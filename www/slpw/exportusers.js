updateForm();

$('.selectpicker').selectpicker({
  style: 'btn-default',
  size: 'auto',
  dropupAuto: false
});

$("#exporttype").change(function(){
    updateForm();
});

// Setup selectall checkbox
$('#selectall').on('ifChecked', function(event){
               selectAll();
});
$('#selectall').on('ifUnchecked', function(event){
               selectAll();
});

$('#filename').focus();

// Validation and saving for add user form
$('#exportuserform').submit(function(event)
{
  // process the form
  // remove the error classes
  $('#filenamediv').removeClass('has-error');
   // remove the error messages
  $('#filenamehelp').remove();
  //Remove form message
  $('#resultexportuser').html('');
  // Validate using JS first
  // Username
  var data={success:true,message:'',errors:{}};
  var filename=$( "#filename" ).val().trim();
  if (filename=="")
    data.errors.filename=ADMINMSG_ENTERFILE;
  if ((filename!="") && (!validChars("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._- ",filename)))
    data.errors.user=ADMINMSG_FILENMIVD;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message=ADMINMSG_FORMERROR;
    //data=JSON.stringify(data);
    // Show validation results from JS in form
    showValidation(data,'resultexportuser');
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
    return;
  }
  $('#act').val('exportselected');
});


function selectAll()
{
  if ($('#selectall').iCheck('update')[0].checked)
    var toggle="check";
  else  
    var toggle="uncheck";
  $('#created').iCheck(toggle);
  $('#username').iCheck(toggle);
  $('#pss').iCheck(toggle);
  $('#enabled').iCheck(toggle);
  $('#name').iCheck(toggle);
  $('#email').iCheck(toggle);
  $('#usergroups').iCheck(toggle);
  for (k=1;k<=50;k++)
  {
    $('#custom'+k).iCheck(toggle);
  }
}

function updateForm()
{
  if ($('#exporttype').val()=="0")
    $('#userdefineddiv').hide();
  else   
    $('#userdefineddiv').show();
}

