<?php
// Sitelok cron handler
require_once("sitelokapi.php");
// Check caller is authorised
$args = array();
foreach ($argv as $arg) {
	if ($pos = strpos($arg,'=')) {
		$key = substr($arg,0,$pos);
		$value = substr($arg,$pos+1);
		$args[$key] = $value;
	}
}
$checkcode=md5($SiteKey."crondfgd4t8wg");
//if (($args['auth']!=$checkcode) && ($_GET['auth']!=$checkcode))
//  die("error 1");
// Get max execution time we have
/*
$maxtime=ini_get('max_execution_time');
if (!is_numeric($maxtime))
	 $maxtime=30;
if ($maxtime==0)
  $maxtime=30;
if ($maxtime>300)
  $maxtime=300;
*/  
$maxtime=30;    	 
$maxtime=$maxtime-5;	 
$scriptstart=time();
$mysql_link=sl_DBconnect();
if ($mysql_link==false)
  die("Can't connect to DB");
// Check we weren't called within 45 seconds
$mysql_result=slcron_mysql_query("SELECT actualts FROM sl_cron WHERE name='croncontrol'",$mysql_link);
if ($mysql_result==false)
  die("Can't perform query");
$row=slcron_mysql_fetch_array_assoc($mysql_result);
//if (($row['actualts']+45)>time())
//  die("error 2");
// Store time of call
$query="UPDATE sl_cron SET actualts=".time()." WHERE name='croncontrol'";
$mysql_result=slcron_mysql_query($query,$mysql_link);
 
 
// Check for cron event (only call one per call)
$mysql_result=slcron_mysql_query("SELECT * FROM sl_cron",$mysql_link);
if ($mysql_result==false)
  die("error 3");
  
while ($row=slcron_mysql_fetch_array_assoc($mysql_result))
{
  $name=$row['name'];
  if ($name=="croncontrol")
    continue;
  $schedule=$row['schedule'];
  $function=$row['function'];
  $actualts=$row['actualts'];
  $plannedts=$row['plannedts']; 
  // First see if any unfinished tasks exist for this plugin (ignore those with an actionts later
  $mysql_result2=slcron_mysql_query("SELECT count(*) FROM sl_crontasks WHERE name='".$name."' AND actionts<=".time(),$mysql_link);
  $row2 = slcron_mysql_fetch_row($mysql_result2);
  if ($row2!=false)
    $matchrows = $row2[0];
  else
    $matchrows=0;
  if ($matchrows>0)
  {
    if (function_exists($function))
    {
      $params['calltype']="crontasks";
      $params['scriptstart']=$scriptstart;    
      $params['maxtime']=$maxtime;    
      $params['mysqllink']=$mysql_link;    
      $params['schedule']=$schedule;
      $params['actualts']=$actualts;
      $params['plannedts']=$plannedts;
      $params['nextts']=$nextts;
      $res=call_user_func($function,$params);
      if ($res==true)
        exit;
    }  
  }
  // Calculate time of next call
  $period=substr($schedule,0,1);
  $period2=substr($schedule,1);
  $nextts=0;
  switch($period)
  {
    case "Y":
      $nextts=strtotime("+".$period2." year",$plannedts);
      break;
    case "M":
      $nextts=strtotime("+".$period2." month",$plannedts);
      break;
    case "W":
      $nextts=strtotime("+".$period2." week",$plannedts);
      break;
    case "D":
      $nextts=strtotime("+".$period2." day",$plannedts);
      break;
    case "H":
      $nextts=strtotime("+".$period2." hour",$plannedts);
      break;
  }
  if (($nextts>0) && (time()>$nextts))  
  {
    if (function_exists($function))
    {
      $params['calltype']="cronevent";
      $params['scriptstart']=$scriptstart;    
      $params['maxtime']=$maxtime;    
      $params['mysqllink']=$mysql_link;    
      $params['schedule']=$schedule;
      $params['actualts']=$actualts;
      $params['plannedts']=$plannedts;
      $params['nextts']=$nextts;
      $res=call_user_func($function,$params);
      exit;
    }  
  }  
} 

function slcron_mysql_query($query,$mysql_link)
{
  global $SitelokVersion;
  if ($SitelokVersion>3.2)
    return mysqli_query($mysql_link,$query);
  return(mysql_query($query,$mysql_link));
}

function slcron_mysql_fetch_array_assoc($mysql_result,$ignore=0)
{ 
  global $SitelokVersion;
  if ($SitelokVersion>3.2)
    return(mysqli_fetch_array($mysql_result,MYSQLI_ASSOC));
  return(mysql_fetch_array($mysql_result,MYSQL_ASSOC));
}

function slcron_mysql_fetch_row($mysql_result)
{
  global $SitelokVersion;
  if ($SitelokVersion>3.2)
    return(mysqli_fetch_row($mysql_result));
  return(mysql_fetch_row($mysql_result));
}


?>