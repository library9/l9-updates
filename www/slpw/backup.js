updateFileList();

// backupModal should not close with escape key or clicking background
$('#backupModal').modal({
    keyboard: false,
    backdrop: 'static',
    'show': false
})

$('#backupform').submit(function(event)
{
  // compress
  var compress="0";
  if ($('#backupcompress').iCheck('update')[0].checked)
    compress="1";
  // migrate
  var migrate="0";
  if ($('#backupmigrate').iCheck('update')[0].checked)
    migrate="1";
  $('#backupModal').modal('show');  
  event.preventDefault();
  startBackupHandler(compress,migrate);
});

function updateFileList()
{
  $('#filelist').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
  var formData = {
      'slcsrf'    : slcsrf
  };

  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'admingetbackups.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      .done(function(data) {
        sessionExpiredReported(data,true);
        if (data.success)
        {
          if (data.filecount>0)
          {
            var htmlstr="";
            htmlstr+='<table>';
            for (k=0;k<data.filecount;k++)
            {
              htmlstr+='<tr>';
              htmlstr+='<td class="date">'+data.times[k]+'</td>';
              htmlstr+='<td class="filename"><a href="'+data.links[k]+'">'+data.files[k]+'</a></td>';
              htmlstr+='<td class="size">'+data.sizes[k]+'</td>';
              htmlstr+='<td class="actions"><a href="'+data.links[k]+'"><span class="glyphicon glyphicon-download-alt actionicon" rel="tooltip" title="'+ADMINBUTTON_DOWNLOAD+'" file"></span></a>&nbsp;<span class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINBUTTON_DELETE+'" onclick="deleteFile(\''+data.files[k]+'\');"></span></td>\n';
              htmlstr+='</tr>';
            }
            htmlstr+='</table>';            
          }
          else
            htmlstr='<p class="filemessage">'+ADMINBACKUP_NOFILES+'</p>';
          $('#filelist').html(htmlstr);
        }
        else
        {
          $('#filelist').html('<p class="fileerror">'+ADMINBACKUP_FILEERROR+'</p>');
        }
      })

    .fail(function(data) {
      $('#filelist').html('<p class="fileerror">'+ADMINBACKUP_FILEERROR+'</p>');
     });
}

function deleteFile(fname)
{
  bootbox.confirm(ADMINBACKUP_DELETEFILE, function(result) {
    if (result)
    {
      var datatosend = { fname : fname, slcsrf : slcsrf };
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'admindeletebackup.php', // the url where we want to POST
            data        : datatosend,
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        .done(function(data) {
          sessionExpiredReported(data,true);
          if (data.success)
            updateFileList();
          else
            bootbox.alert(data.message);
        })
        .fail(function() {
          bootbox.alert(ADMINMSG_NOTDELFILE);
        })
    }  
  });
}

function startBackupHandler(compress,migrate)
{
  $('#backup_cancel').show();  
  $('#backup_cancel').prop('disabled', false);
  $('#backup_close').hide();    
  $('#backup_close').prop('disabled', false);
  $('#backupwarning').html('<p>'+ADMINBACKUP_WARNING+'</p>');
  setProgress(0);
  $('#backupprogress').addClass('active');
  $('#statustext').text(ADMINBACKUP_CREATING);
  // Call ajax handler to backup
  var formData = {
      'slcsrf'    : slcsrf,
      'backupcompress'    : compress,
      'backupmigrate'    : migrate,
      'backupact'       : ''
  };

  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminbackupprocess.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      .done(function(data) {
        sessionExpiredReported(data,true);
        if (data.backupstatus=="finished")
        {  
          finishBackupHandler(data);
          return;
        }
        if (data.backupstatus=="cancelled")
        {
          cancelBackupHandler(data);
          return;
        }
        if (data.backupstatus=="error")
        {  
          // Delay 5 seconds before retrying
          setTimeout(function(){ moreBackupHandler(); }, 5000);
          return;
        }
        moreBackupHandler();
      })

      .fail(function(data) {
        // Delay 5 seconds before retrying
        setTimeout(function(){ moreBackupHandler(); }, 5000);
      })

  progressInterval = setInterval(updateProgress, 2000);

};

function moreBackupHandler()
{
  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminbackupprocess.php', // the url where we want to POST
      data        : {'slcsrf'    : slcsrf,'backupcompress'   : "",'backupmigrate': "",'backupact': "callback" },
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      .done(function(data) {
        sessionExpiredReported(data,true);
        if (data.backupstatus=="finished")
        {  
          finishBackupHandler(data);
          return;
        }
        if (data.backupstatus=="cancelled")
        {  
          cancelBackupHandler(data);
          return;
        }
        if (data.backupstatus=="error")
        {  
          // Delay 5 seconds before retrying
          setTimeout(function(){ moreBackupHandler(); }, 5000);
          return;
        }
        moreBackupHandler();
        return;
      })

      .fail(function(data) {
        // Delay 5 seconds before retrying
        setTimeout(function(){ moreBackupHandler(); }, 5000);
      })

}

function finishBackupHandler(data)
{
  clearInterval(window.progressInterval);
  $('#backupprogress').removeClass('active');
  setProgress(100);
  $('#statustext').text(ADMINMSG_FINISHED);
  $('#backup_cancel').hide();  
  $('#backupwarning').html('<p>&nbsp;</p>');
  $('#backup_close').show();
  updateFileList();  
}

function cancelBackupHandler(data)
{
  clearInterval(window.progressInterval);
  $('#backupprogress').removeClass('active');
  $('#statustext').text(ADMINMSG_CANCELED);
  $('#backup_cancel').hide();  
  $('#backupwarning').html('<p>&nbsp;</p>');
  $('#backup_close').show();  
}

function setProgress(p)
{
  $("#backupprogress").css('width', p + "%");
  $("#backupprogress").text(p + "%");
}

function updateProgress()
{
  $.getJSON(SitelokLocationURL+'writefiles/backupprogress.json',function(data)
  {
    if (data.message.length<4)
      setProgress(data.message);
    else
     $('#statustext').text(data.message); 
  })
  .error(function(data)
  {
  });
}

function cancelClicked()
{
  bootbox.confirm(ADMINMSG_RUSURE, function(result)
  {
    if (result)
    {
      // Call ajax handler to send email
      var formData = {
          'slcsrf'    : slcsrf,
          'comact'   : "cancel",
      };
      $.ajax({
          type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url         : 'adminbackupcontrol.php', // the url where we want to POST
          data        : formData, // our data object
          dataType    : 'json', // what type of data do we expect back from the server
          encode      : true
      })
        .done(function(data) {
          sessionExpiredReported(data,true);
          if (data.success)
          { 
            $('#statustext').text(ADMINMSG_CANCELING);
            $('#backup_cancel').prop('disabled', true);
            return;
          }
        })

    }
  });  
}

function closeClicked()
{
  $('#backupModal').modal('hide');
}


