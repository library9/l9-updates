<?php
  $groupswithaccess="ADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value  
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError(ADMINMSG_CSRFFAILED);
    exit;
  }
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError(ADMINMSG_MYSQLERROR);
    exit;
  }
  $query="SELECT * FROM ".$DbGroupTableName." ORDER BY name ASC"; 
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    returnError(ADMINMSG_MYSQLERROR);
    exit;
  }
  $names=array();
  $descriptions=array();
  $loginactions=array();
  $loginvalues=array();
  $groupids=array();
  $numgroups=mysqli_num_rows($mysql_result);
  while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
  {
    $names[]=$row['name'];
    $descriptions[]=$row['description'];
    $loginactions[]=$row['loginaction'];
    $loginvalues[]=$row['loginvalue'];
    $groupids[]=$row['id'];
  }

  function returnError($msg)
  {
    $data['success'] = false;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

  ?>
{
  "success": true,
  "message": "",
  "groupcount": <?php echo $numgroups; ?>,
  "names": [
<?php
  for ($k=0;$k<$numgroups;$k++)
  {
?>
      <?php echo json_encode($names[$k]); if ($k!=($numgroups-1)) echo ",";?>

<?php    
  }
?>
  ],
  "descriptions": [
<?php
  for ($k=0;$k<$numgroups;$k++)
  {
?>
      <?php echo json_encode(htmlentities($descriptions[$k],ENT_QUOTES,strtoupper($MetaCharSet))); if ($k!=($numgroups-1)) echo ",";?>

<?php    
  }
?>
  ],
  "loginactions": [
<?php
  for ($k=0;$k<$numgroups;$k++)
  {
?>
      <?php echo json_encode($loginactions[$k]); if ($k!=($numgroups-1)) echo ",";?>

<?php    
  }
?>
  ],
  "loginvalues": [
<?php
  for ($k=0;$k<$numgroups;$k++)
  {
?>
      <?php echo json_encode($loginvalues[$k]); if ($k!=($numgroups-1)) echo ",";?>

<?php    
  }
?>
  ],
  "groupids": [
<?php
  for ($k=0;$k<$numgroups;$k++)
  {
?>
      <?php echo json_encode($groupids[$k]); if ($k!=($numgroups-1)) echo ",";?>

<?php    
  }
?>
  ]
}


