<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $startpage="index.php";
  $noaccesspage="";
  require("sitelokpw.php");
  $userid=$_GET['userid'];
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  include("admincommonfunctions.php");
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    die(ADMINMSG_MYSQLERROR);
    exit;
  }
  // Get configuration data
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_adminconfig LIMIT 1");
  if ($mysql_result!=false)
  {
    $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
    if ($row!=false)
    {
      $sendedituseremail=$row["sendedituseremail"];
    }
  }  
  // Get current user data
  $query="SELECT * FROM ".$DbTableName." WHERE ".$IdField."=".sl_quote_smart($userid);
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    print("User doesn't exist");
    mysqli_close($mysql_link);
    exit;
  }
  if ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
  {
    $userid=$row[$IdField];
    $user=$row[$UsernameField];
    $olduser=$user;
    $pass=$row[$PasswordField];
    $en=$row[$EnabledField];
    $nm=$row[$NameField];
    $em=$row[$EmailField];
    $ug=$row[$UsergroupsField];
    if ($slsubadmin)
    {
      if ((trim(strtoupper(substr($ug,0,5)=="ADMIN"))) || (strpos(trim(strtoupper($ug)),"^ADMIN")!==false))
      {
        header("Location: index.php");
        exit;
      }
    }      
    $cu1=$row[$Custom1Field];
    $cu2=$row[$Custom2Field];
    $cu3=$row[$Custom3Field];
    $cu4=$row[$Custom4Field];
    $cu5=$row[$Custom5Field];
    $cu6=$row[$Custom6Field];
    $cu7=$row[$Custom7Field];
    $cu8=$row[$Custom8Field];
    $cu9=$row[$Custom9Field];
    $cu10=$row[$Custom10Field];
    $cu11=$row[$Custom11Field];
    $cu12=$row[$Custom12Field];
    $cu13=$row[$Custom13Field];
    $cu14=$row[$Custom14Field];
    $cu15=$row[$Custom15Field];
    $cu16=$row[$Custom16Field];
    $cu17=$row[$Custom17Field];
    $cu18=$row[$Custom18Field];
    $cu19=$row[$Custom19Field];
    $cu20=$row[$Custom20Field];
    $cu21=$row[$Custom21Field];
    $cu22=$row[$Custom22Field];
    $cu23=$row[$Custom23Field];
    $cu24=$row[$Custom24Field];
    $cu25=$row[$Custom25Field];
    $cu26=$row[$Custom26Field];
    $cu27=$row[$Custom27Field];
    $cu28=$row[$Custom28Field];
    $cu29=$row[$Custom29Field];
    $cu30=$row[$Custom30Field];
    $cu31=$row[$Custom31Field];
    $cu32=$row[$Custom32Field];
    $cu33=$row[$Custom33Field];
    $cu34=$row[$Custom34Field];
    $cu35=$row[$Custom35Field];
    $cu36=$row[$Custom36Field];
    $cu37=$row[$Custom37Field];
    $cu38=$row[$Custom38Field];
    $cu39=$row[$Custom39Field];
    $cu40=$row[$Custom40Field];
    $cu41=$row[$Custom41Field];
    $cu42=$row[$Custom42Field];
    $cu43=$row[$Custom43Field];
    $cu44=$row[$Custom44Field];
    $cu45=$row[$Custom45Field];
    $cu46=$row[$Custom46Field];
    $cu47=$row[$Custom47Field];
    $cu48=$row[$Custom48Field];
    $cu49=$row[$Custom49Field];
    $cu50=$row[$Custom50Field];
    // Convert existing groups and expiry dates into data to be displayed
    $grouparray=explode("^",$ug);
    for ($k=0;$k<count($grouparray);$k++)
    {
      $pvar1="group".($k+1);
      $pvar2="expirydate".($k+1);
      $$pvar1=strtok($grouparray[$k],":");
      $$pvar2=trim(strtok(":"));
    }
    $groupcount=count($grouparray);
  }
  // get field lengths and other info for custom fields
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." LIMIT 1");
  $fcharset=mysqli_get_charset($mysql_link);
  $finfo=mysqli_fetch_fields($mysql_result);
  $fieldlengths=array();
  $fieldtitle=array();
  $fieldvalue=array();
  $download=array();
  $inline=array();
  $downloadicon=false;
  for ($k=1;$k<=50;$k++)
  {
    $fieldlengths[$k] = ($finfo[$k+7]->length)/$fcharset->max_length;
    $titlevar="CustomTitle".$k;
    $valuevar="cu".$k;
    $fieldtitle[$k]=$$titlevar;
    $fieldvalue[$k]=$$valuevar;
    $download[$k]="";
    $inline[$k]="";
    $datatocheck=$fieldvalue[$k];
    if (function_exists("blab_getuploadedimage"))
      $datatocheck=blab_getuploadedimage($datatocheck);    
    if (($fieldvalue[$k]!="") && (@file_exists($FileLocation.$datatocheck)) && (!is_dir($FileLocation.$datatocheck)))
    {
      $download[$k]=sl_siteloklink($datatocheck,1,0,"NOLOG");
      $fileext=sl_fileextension($datatocheck);
      if (($fileext==".jpg") || ($fileext==".jpeg") || ($fileext==".png") || ($fileext==".gif"))
        $inline[$k]=sl_siteloklink($datatocheck,0,0,"NOLOG");
      $downloadicon=true;
    }
  }



?>
<!DOCTYPE html>
<html>
<head>
<?php
$pagename="edituser";
include("adminhead.php");
?>
<title><?php echo ADMINBUTTON_EDITUSER; ?></title>
<link rel="stylesheet" type="text/css" href="maxlength/jquery.maxlength.css">
<link href="featherlight/featherlight.min.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />
<link rel="stylesheet" href="edituser.css">
</head>
<?php include("adminthemeheader.php"); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <span class="glyphicon glyphicon-edit"></span>&nbsp;<?php echo ADMINBUTTON_EDITUSER; ?>
            <small><?php echo $user." (".$userid.")"; ?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><?php echo ADMINBUTTON_EDITUSER; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>

          <div class="row">


                    <form id="edituserform" role="form" class="form-horizontal" method="post">
                      <input type="hidden" name="slcsrf" id="slcsrf" value="<?php echo $slcsrftoken; ?>">
                      <input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>">
                      <input type="hidden" name="olduser" id="olduser" value="<?php echo htmlentities($olduser,ENT_QUOTES,strtoupper($MetaCharSet)); ?>">
                      <input type="hidden" name="existingpss" value="<?php echo htmlentities($pass,ENT_QUOTES,strtoupper($MetaCharSet)); ?>">
                      <input type="hidden" name="existingsendemail" value="<?php echo $sendedituseremail; ?>">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">


                      <div class="box box-default">
                        <div class="box-header">
                          <h3 class="box-title"><?php echo ADMINGENERAL_USERDETAILS; ?></h3>
                        </div>
                        <div class="box-body">


                          <div class="form-group" id="usergroup">
                              <label class="col-xs-12" for="user" id="labeluser"><?php echo ADMINFIELD_USERNAME; ?></label>
                              <div class="col-xs-12" id="userdiv">
                                  <input type="text" class="form-control" name="user" id="user" maxlength="100" value="<?php echo htmlentities($user,ENT_QUOTES,strtoupper($MetaCharSet)); ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                              </div>
                          </div>

                          <div class="form-group" id="pssgroup">
                              <label class="col-xs-12" for="pss" id="labelpss"><?php echo ADMINFIELD_PASSWORD; ?></label>
                              <div class="col-xs-12" id="pssdiv">
                                  <div class="input-group">
                                    <input type="text" class="form-control" name="pss" id="pss" maxlength="255" value="<?php if (!$MD5passwords) echo htmlentities($pass,ENT_QUOTES,strtoupper($MetaCharSet)) ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                    <span class="input-group-btn">
                                      <button class="btn btn-default" type="button" onclick="$('#pss').val(randomPassword());"><?php echo ADMINBUTTON_RANDOM; ?></button>
                                    </span>
                                  </div>
                                  <?php if ($MD5passwords) { ?><span id="helpBlock" class="help-block"><?php echo ADMINEDITUSER_PASSBLANK; ?></span><?php } ?>
                              </div>
                          </div>

                          <div class="form-group" id="nmgroup">
                              <label class="col-xs-12" for="nm" id="labelnm"><?php echo ADMINFIELD_NAME; ?></label>
                              <div class="col-xs-12" id="nmdiv">
                                  <input type="text" class="form-control" name="nm" id="nm" maxlength="100" value="<?php echo htmlentities($nm,ENT_QUOTES,strtoupper($MetaCharSet)); ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                              </div>
                          </div>

                          <div class="form-group" id="emgroup">
                              <label class="col-xs-12" for="em" id="labelem"><?php echo ADMINFIELD_EMAIL; ?></label>
                              <div class="col-xs-12" id="emdiv">
                                  <input type="text" class="form-control" name="em" id="em" maxlength="100" value="<?php echo htmlentities($em,ENT_QUOTES,strtoupper($MetaCharSet)); ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                              </div>
                          </div>

                          <div class="form-group" id="engroup">
                              <div class="col-sm-10" id="endiv">
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" name="en" id="en" value="Yes" <?php if ($en=="Yes") echo "checked"; ?> >&nbsp;&nbsp;<?php echo ADMINFIELD_ENABLED; ?>
                                  </label>
                                </div>
                              </div>
                          </div>

                        </div>
                      </div>  


                      <div class="box box-default">
                        <div class="box-header">
                          <h3 class="box-title"><?php echo ADMINFIELD_USERGROUPS; ?></h3>
                        </div>
                        <div class="box-body">

<?php
for ($k=1;$k<=$groupcount;$k++)
{
?>
                           <div id="groupblock<?php echo $k; ?>"> 
                             <div class="form-group">
                                <div class="col-xs-6 col-sm-5 col-md-5 col-lg-6" id="group<?php echo $k; ?>div">
                                   <select placeholder="<?php echo strtolower(ADMINFILTER_USERGROUP); ?>" class="form-control usergrouplist" id="group<?php echo $k; ?>" name="group<?php echo $k; ?>">
                                   <?php populateUsergroupSelect(); ?>
                                   </select> 
                                </div>

                                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4" id="expiry<?php echo $k; ?>div">
                                  <div class="input-group datepicker" id="dateexpiry<?php echo $k; ?>">
                                     <input type="text" placeholder="<?php echo ADMINEDITUSER_EXPIREPLACE.$DateFormat; ?>" class="form-control" id="expiry<?php echo $k; ?>" name="expiry<?php echo $k; ?>" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                     <div class="input-group-addon"><span class="glyphicon glyphicon-calendar actionicon"></span></div>
                                  </div>   
                                </div>

                                <div class="col-sm-3 col-md-3 col-lg-2 hidden-xs" id="expiry<?php echo $k; ?>div">
                                  <span class="glyphicon glyphicon-triangle-top actionicon" title="<?php echo ADMINBUTTON_MOVEUP; ?>" onclick="groupUp(<?php echo $k; ?>);"></span>
                                  <span class="glyphicon glyphicon-triangle-bottom actionicon" title="<?php echo ADMINBUTTON_MOVEDOWN; ?>" onclick="groupDown(<?php echo $k; ?>);"></span>
                                  <span class="glyphicon glyphicon-remove removegroup actionicon" title="<?php echo ADMINBUTTON_REMOVE; ?>" onclick="removeGroup(<?php echo $k; ?>);"></span>
                                </div>
                            </div>
                          </div>
<?php } ?>


                          <div id="extraGroupTemplate">
<!--                            <div id="groupblockXX">
                              <div class="form-group">
                                <div class="col-xs-6 col-sm-5 col-md-5 col-lg-6" id="groupXXdiv">
                                   <select placeholder="<?php echo strtolower(ADMINFILTER_USERGROUP); ?>" class="form-control usergrouplist" id="groupXX" name="groupXX">
                                   <?php populateUsergroupSelect(); ?>                 
                                   </select> 
                                </div>

                                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4" id="expiryXXdiv">
                                  <div class="input-group datepicker" id="dateexpiryXX">
                                     <input type="text" placeholder="<?php echo ADMINEDITUSER_EXPIREPLACE.$DateFormat; ?>" class="form-control" id="expiryXX"  name="expiryXX" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                     <div class="input-group-addon"><span class="glyphicon glyphicon-calendar actionicon"></span></div>
                                  </div>   
                                </div>

                                <div class="col-sm-3 col-md-3 col-lg-2 hidden-xs" id="expiryXXdiv">
                                  <span class="glyphicon glyphicon-triangle-top actionicon" title="<?php echo ADMINBUTTON_MOVEUP; ?>" onclick="groupUp(XX);"></span>
                                  <span class="glyphicon glyphicon-triangle-bottom actionicon" title="<?php echo ADMINBUTTON_MOVEDOWN; ?>" onclick="groupDown(XX);"></span>
                                  <span class="glyphicon glyphicon-remove removegroup actionicon" title="<?php echo ADMINBUTTON_REMOVE; ?>" onclick="removeGroup(XX);"></span>
                                </div>
                            </div
                          </div>
 -->                     </div>


                        <div id="extraGroupInsert"></div>



                          <div class="form-group">
                              <div class="col-xs-12">
                                <button type="button" class="btn btn-xs btn-default" onclick="addGroup();">
                                  <span class="glyphicon glyphicon-plus actionicon"></span>
                                </button>
                              </div>
                          </div>


                        </div>
                    </div>



                      <div class="box box-default">
                        <div class="box-header">
                          <h3 class="box-title"><?php echo ADMINGENERAL_OTHEROPTIONS; ?></h3>
                        </div>                      
                        <div class="box-body">

                           <div class="form-group">
                              <div class="col-sm-10">
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" id="forcelogout" name="forcelogout" value="1">&nbsp;&nbsp;<?php echo ADMINEDITUSER_FORCELOGOUT; ?>
                                  </label>
                                </div>
                              </div>
                          </div>

                          <div class="form-group" id="sendemailgroup">
                              <div class="col-sm-10">
                                <div class="checkbox" id="sendemaildiv">
                                  <label>
                                    <input type="checkbox" id="sendemail" name="sendemail" value="1">&nbsp;&nbsp;<?php echo ADMINEDITUSER_EMAILUSER; ?>
                                  </label>
                                </div>
                              </div>
                          </div>

                          <div id="templatediv" onclick="openFileManager();">
                          <input type="hidden" id="template" name="template" value="<?php echo $ModifyUserEmail; ?>">
                          <span id="templatename"><?php if ($ModifyUserEmail!="") echo $ModifyUserEmail; else echo ADMINMSG_NOTEMPLATE; ?></span>&nbsp;&nbsp;<span><i class="glyphicon glyphicon-folder-open actionicon"></i></span>
                          </div>

                        </div>
                      </div>  



                    <div class="form-group">
                    <div class="col-sm-11">
                        <div class="btn-toolbar">
                            <button type="submit" id="submitedituser" class="btn btn-primary pull-left"><?php echo ADMINBUTTON_SAVECHANGES; ?></button>
                            <button type="button" id="canceledituser" class="btn btn-default pull-left"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                        </div>   
                    </div>
                    </div>

                      <div id="resultedituser"></div>




</div><!-- /.col -->
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">



                      <div class="box box-default">
                        <div class="box-header">
                          <h3 class="box-title"><?php echo ADMINGENERAL_CUSTOMFIELDS; ?></h3>
                        </div>
                        <div class="box-body">

<?php
$customcols="12";
if ($downloadicon)
  $customcols="10";


for ($k=1;$k<=50;$k++)
{
if ($fieldtitle[$k]!="")
{
  if ($fieldlengths[$k]>255)
  {
?>

                          <div class="form-group" id="<?php echo "cu".$k; ?>group">
                              <label class="col-xs-12" for="cu<?php echo $k; ?>" id="labelcu<?php echo $k; ?>"><?php echo $fieldtitle[$k]; ?></label>
                              <div class="col-xs-<?php echo $customcols; ?>" id="cu<?php echo $k; ?>div">
                                  <textarea class="form-control overlaidLength" name="cu<?php echo $k; ?>" id="cu<?php echo $k; ?>" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"><?php echo htmlentities($fieldvalue[$k],ENT_QUOTES,strtoupper($MetaCharSet)); ?></textarea>
                              </div>
                              <div class="col-sm-2 col-md-2 col-lg-2">
                                <?php if ($download[$k]!="") { ?><a href="<?php echo $download[$k]; ?>"><span class="glyphicon glyphicon-download-alt actionicon" title="<?php echo ADMINBUTTON_DOWNLOAD; ?>"></span></a><?php } ?>
                                <?php if ($inline[$k]!="") { ?><a href="#" data-featherlight="<?php echo $inline[$k]; ?>"><span class="glyphicon glyphicon-eye-open actionicon" title="<?php echo ADMINBUTTON_VIEW; ?>"></span></a><?php } ?>
                              </div>
                          </div>


<?php } else { ?>
                          <div class="form-group" id="<?php echo "cu".$k; ?>group">
                              <label class="col-xs-12" for="cu<?php echo $k; ?>" id="labelcu<?php echo $k; ?>"><?php echo $fieldtitle[$k]; ?></label>
                              <div class="col-xs-<?php echo $customcols; ?>" id="cu<?php echo $k; ?>div">
                                  <input type="text" class="form-control" name="cu<?php echo $k; ?>" id="cu<?php echo $k; ?>" maxlength="<?php echo $fieldlengths[$k]; ?>" value="<?php echo htmlentities($fieldvalue[$k],ENT_QUOTES,strtoupper($MetaCharSet)); ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                              </div>
                              <div class="col-sm-2 col-md-2 col-lg-2">
                                <?php if ($download[$k]!="") { ?><a href="<?php echo $download[$k]; ?>"><span class="glyphicon glyphicon-download-alt actionicon" title="<?php echo ADMINBUTTON_DOWNLOAD; ?>"></span></a><?php } ?>
                                <?php if ($inline[$k]!="") { ?><a href="#" data-featherlight="<?php echo $inline[$k]; ?>"><span class="glyphicon glyphicon-eye-open actionicon" title="<?php echo ADMINBUTTON_VIEW; ?>"></span></a><?php } ?>
                              </div>

                          </div>

<?php } ?>
<?php } ?>
<?php } ?>

                        </div>
                      </div> 
</div><!-- /.col -->                       




</form>









          </div><!-- /.row -->

<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<!-- Image lightbox modal -->
<div class="modal fade" id="ImageLightboxModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <img src="">
      </div>
    </div>
  </div>
</div>


<?php include("adminthemefooter.php"); ?>
<script type="text/javascript" src="maxlength/jquery.plugin.min.js"></script> 
<script type="text/javascript" src="maxlength/jquery.maxlength.min.js"></script>
<script type="text/javascript" src="fancybox/source/jquery.fancybox.js?v=2.1.4"></script>
<script type="text/javascript" src="fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script src="featherlight/featherlight.min.js"></script>
<script type="text/javascript">
  var groupcount=<?php echo ($groupcount+1)."\n"; ?>
</script>
<script src="edituser.js"></script>
<script type="text/javascript">
// Javascript here needs PHP so is not in .js file
<?php for ($k=1;$k<=$groupcount;$k++)
{
  $gvar="group".$k;
  $xvar="expirydate".$k;
?>
  // Make select into combo
  $('#group<?php echo $k; ?>').editableSelect();
  $('#group<?php echo $k; ?>').val('<?php echo $$gvar; ?>');
  // Enable calendar
  $('#dateexpiry<?php echo $k; ?>').datetimepicker({'keepInvalid': true, 'format':DateFormat,'defaultDate':'', 'useStrict': true});
  $('#expiry<?php echo $k; ?>').val('<?php echo $$xvar; ?>');
<?php } ?> 
// add character counter for textareas
<?php for ($k=1;$k<=50;$k++)
{
  if ($fieldlengths[$k]<=255)
    continue;
?>
$('#cu<?php echo $k; ?>').maxlength({max: <?php echo $fieldlengths[$k]; ?>, feedbackText: '{c}/{m}'});
<?php } ?>
// Set iCheck state
<?php if ($sendedituseremail=="1") { ?>
$('#sendemail').iCheck('check');
$('#templatediv').show();
<?php } else { ?>
$('#sendemail').iCheck('uncheck');
$('#templatediv').hide();
<?php } ?>

</script>
  </body>
</html>
