function validUsergroup(val,reqd)
{
  if ((reqd) && (val==""))
    return(false);
  if ((!reqd) && (val==""))
    return(true);  
  if (!validChars('#{}()@.|0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',val))
    return(false);
  return(true);
}

function validChars(allow,str)
{
  for (var k=0;k<str.length;k++)
  {
    if (allow.indexOf(str.charAt(k))==-1)
      return(false);
  }
  return(true);
}

function validInteger(value,min,max,reqd)
{
  if ((reqd) && (value==""))
    return(false);
  if ((!reqd) && (value==""))
    return(true);  
  if (typeof value !== 'string')
    value=value.toString();
  if (value.length==0)
    return(false);
  var validchars="0123456789";
  if ((min<0) || (max<0))
    validchars+="-";
  var reg = new RegExp('^[' + validchars + ']+$');
  if (!reg.test(value))
    return(false);
  value=Number(value);
  if ((value<min) || (value>max))
    return(false);
  return(true);
}

function validateEmail(email)
{
  var ck_email = /^([\w-\'!#$%&\*]+(?:\.[\w-\'!#$%&\*]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,20}(?:\.[a-z]{2})?)$/i
  if (!ck_email.test(email))
    return(false)
  return(true)  
}

function dateValid(dt,fmt)
{
  var monthMax=new Array(31,29,31,30,31,30,31,31,30,31,30,31)
  if (dt.length!=6)
    return(false)
  if (fmt=="DDMMYY")
  {
    var day=dt.substring(0,2)
    var month=dt.substring(2,4)
    var year=dt.substring(4,6)
  }
  else
  {
    var month=dt.substring(0,2)
    var day=dt.substring(2,4)
    var year=dt.substring(4,6)
  }
  if ((isNaN(day)) || (isNaN(month)) || (isNaN(year)))
    return(false)
  var iday=parseInt(day,10)
  var imonth=parseInt(month,10)
  var iyear=parseInt(year,10)
  if ((imonth<1) || (imonth>12))
    return(false)
  if ((iyear<0) || (iyear>37))
    return(false)
  var top=monthMax[imonth-1]
  if ((iday<1) || (iday>top))
    return(false)
  if ((imonth==2) && (iday==29))
  {
    if ((iyear/4)!=(Math.floor(iyear/4)))
      return(false)
  }
  return(true)
}

function isValidColor(color)
{
  color=color.trim();
  if (color.length!=7)
    return(false);
  color=color.toLowerCase();
  if (color.substr(0,1)!='#')
    return(false);
  color=color.substr(1);
  var validchars="0123456789abcdef";
  var reg = new RegExp('^[' + validchars + ']+$');
  if (!reg.test(color))
    return(false);
  return(true);  
}

function populateUsergroupField(objid)
{
  for (var k=0;k<usergroupNames.length;k++)
  {
    $('#'+objid).append('<option value="'+usergroupNames[k]+'">'+usergroupNames[k]+'</option>');
  }
}

function populateUsergroupClass(objclass)
{
  for (var k=0;k<usergroupNames.length;k++)
  {
    $('.'+objclass).append('<option value="'+usergroupNames[k]+'">'+usergroupNames[k]+'</option>');
  }
}

function showValidation(data,messagediv)
{
  if ( ! data.success)
  {
    for (var key in data.errors)
    {
      if (data.errors.hasOwnProperty(key))
      {
        if (key)
        {
          $('#'+key+'div').addClass('has-error'); // add the error class to show red input
          $('#'+key+'div').append('<div class="help-block" id="'+key+'help">' + data.errors[key] + '</div>'); // add the actual error message under our input
        }
      }
    }
    if (messagediv!="")
      $('#'+messagediv).html('<div id="'+messagediv+'message" class="alert alert-danger">' + data.message + '</div>');
  }
  else
  {
     // Show the success message
    if (messagediv!="")
      $('#'+messagediv).html('<div id="'+messagediv+'message" class="alert alert-success">' + data.message + '</div>');
      setTimeout(function() { $('#'+messagediv+'message').slideUp(250, function() {$('#'+messagediv).html('');$('#'+messagediv).show();} );  }, 3000);  // Allow panel to open before focus
  }
}

function showButtonBusy(buttonid,text)
{
  if (typeof(text)==='undefined') text = ADMINMSG_SAVING;
  var label=$('#'+buttonid).html();
  $('#'+buttonid).html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> '+text);
  return(label);
}

function hideButtonBusy(buttonid,label)
{
  $('#'+buttonid).html(label);
}

function randomPassword()
{
  var mask=RandomPasswordMask;
  if (mask=="")
    mask="cccc##"
  var password="";   
  for (k=0;k<mask.length;k++)  
  {
    if (mask.charAt(k)=="c")
      password=password+"abcdefghijklmnopqrstuvwxyz".charAt(Math.round(25*Math.random()));
    if (mask.charAt(k)=="C")
      password=password+"ABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(Math.round(25*Math.random()));
    if (mask.charAt(k)=="#")
      password=password+"0123456789".charAt(Math.round(9*Math.random()));
    if (mask.charAt(k)=="X")
      password=password+"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(Math.round(51*Math.random()));
    if (mask.charAt(k)=="A")
      password=password+"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".charAt(Math.round(61*Math.random()));
    if (mask.charAt(k)=="U")
      password=password+ValidPasswordChars.charAt(Math.round((ValidPasswordChars.length-1)*Math.random()));
  }
  return(password); 
}

function sessionExpiredReported(data,redirect)
{
  if(data.hasOwnProperty("error"))
  { 
    if (data.error.substr(-3,3)=="001")
    {
      if (redirect)
      {
        location.reload(true);
        return(true);
      }
      else
        return(true);
    }
  }    
  return(false); 
}

function pluginPage(pluginid,pluginindex,plugin,v5)
{
  if (v5)
    window.location.href = plugin+"?act=pluginadmin&pluginid="+pluginid+"&pluginindex="+pluginindex+"&numsel="+numsel;
  else
    window.location.href = plugin+"?slcsrf="+slcsrf+"&act=pluginadmin&pluginid="+pluginid+"&pluginindex="+pluginindex+"&numsel="+numsel;
}

function pluginSelected(pluginid,pluginindex,plugin,numsel,v5)
{
  if (v5)
    window.location.href = plugin+"?act=pluginselected&pluginid="+pluginid+"&pluginindex="+pluginindex+"&numsel="+numsel;
  else     
    window.location.href = plugin+"?slcsrf="+slcsrf+"&act=pluginselected&pluginid="+pluginid+"&pluginindex="+pluginindex+"&numsel="+numsel;
}

function validateColorBlur(id,defaultvalue)
{
  if (typeof(defaultvalue)==='undefined')
    var defaultvalue = $(id).prop("defaultValue");
  var col=$(id).val();
  if ((col!='') && (isValidColor(col)))
    return;
  col='#'+col;
  // Try adding # just in case user forgot
  if ((col!='') && (isValidColor(col)))
  {
    $(id).val(col);
    $(id).minicolors('value', col);
    return;
  }
  $(id).val(defaultvalue);
  $(id).minicolors('value', defaultvalue);
}

function validateSizeBlur(id,defaultvalue)
{
  if (typeof(defaultvalue)==='undefined')
    var defaultvalue = $(id).prop("defaultValue");
  var sz=$(id).val();
  if (validInteger(sz,1,99,true))
    return;
  $(id).val(defaultvalue);
}

function validateIntegerBlur(id,min,max,defaultvalue)
{
  if (typeof(defaultvalue)==='undefined')
    var defaultvalue = $(id).prop("defaultValue");
  var sz=$(id).val();
  if (validInteger(sz,min,max,true))
    return;
  $(id).val(defaultvalue);
}

function validateExpiryBlur(id,defaultvalue)
{
  if (typeof(defaultvalue)==='undefined')
    var defaultvalue = $(id).prop("defaultValue");
  var exp=$(id).val();
  if ((exp=="") || (!validChars("0123456789",exp)))
  {
    $(id).val(defaultvalue);
    return;
  }
  if (exp.length==6)
  {
    if (!dateValid(exp,DateFormat))
    {
      $(id).val(defaultvalue);
      return;
    }
  }
}

function validateGroupBlur(id,defaultvalue)
{
  if (typeof(defaultvalue)==='undefined')
    var defaultvalue = $(id).prop("defaultValue");
  var gp=$(id).val();
  if (validUsergroup(gp,true))
    return;
  $(id).val(defaultvalue);
}

function validateReqdBlur(id,defaultvalue)
{
  if (typeof(defaultvalue)==='undefined')  
    var defaultvalue = $(id).prop("defaultValue");
  var vl=$(id).val();
  if (vl.trim()!="")
    return;
  $(id).val(defaultvalue);
}

function checkForUpdate()
{
  $('body').addClass('wait');
  var formData = {
      'slcsrf'    : slcsrf,
  };
  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'admincheckforupdate.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      .done(function(data) {
        $('body').removeClass('wait');
        sessionExpiredReported(data,true);
        if (data.success)
        {
          if (parseFloat(data.version)>parseFloat(SitelokVersion))
          {
            var str=ADMINUPDATE_UPDATE;
            str=str.replace("***1***",data.version);
            str=str.replace("***2***",SitelokVersion);
            str=str.replace("***3***",data.link);
            bootbox.alert(str);
          }
          else
          {
            var str=ADMINUPDATE_LATEST;
            str=str.replace("***1***",SitelokVersion);
            bootbox.alert(str);            
          }
        }
        else
        {
        }
      })

    .fail(function(data) {
        $('body').removeClass('wait');
     });
}

