<?php
$groupswithaccess="ADMIN,SUBADMIN";
$noaccesspage="";
$adminajaxcall=true;
require("sitelokpw.php");
if (!isset($adminlanguage))
  $adminlanguage="en";
require("adminlanguage-".$adminlanguage.".php");  
$slsubadmin=false;
if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
  $slsubadmin=true;
// Check CSRF value
if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
{
  ?>
  {
  "success": false,
  "message": "<?php echo ADMINMSG_CSRFFAILED; ?>"
  }
  <?php
  exit;      
}   
$filename=$_POST['filename'];
// Double check path doesn't start with / or \ or contain .. or :
if ((substr($filename,0,1)=="/") || (substr($filename,0,1)=="\\") || (false!=(strpos($filename, ".."))) || (false!=(strpos($filename, ":"))))
{
?>
{
  "success": false,
  "message": "<?php echo ADMINMSG_TEMPLATENOLOAD; ?>"
}
<?php
exit;
}

$ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
if (($ext!="html") && ($ext!="htm") && ($ext!="txt"))
{
?>
{
  "success": false,
  "message": "<?php echo ADMINMSG_TEMPLATETYPE; ?>"
}
<?php
exit;
}

if (sl_ReadEmailTemplate($filename,$subject,$mailBody,$htmlformat)==false)
{
?>
{
  "success": false,
  "message": "<?php echo ADMINMSG_TEMPLATENOLOAD; ?>"
}
<?php
exit;
}

if ($htmlformat=="")
  $htmlformat="N";

/*
// Standardize newline
$mailBody=str_replace("\r\n", "\n",$mailBody);
$mailBody=str_replace("\r", "\n",$mailBody);
$subject=str_replace("\r\n", "\n",$subject);
$subject=str_replace("\r", "\n",$subject);

// Make newline compatible with JSON
$mailBody=str_replace("\n", "\\n",$mailBody);
$mailBody=str_replace('"', '\\"',$mailBody);
$subject=str_replace("\n", "\\n",$subject);
$subject=str_replace('"', '\\"',$subject);

// Replace tabs with spaces
$mailBody=str_replace("\t", '    ',$mailBody);
$subject=str_replace("\t", '    ',$subject);
*/


// Adjust any !!!sitelokurl!!! or !!!sitelokemailurl!!!
$mailBody=str_replace("!!!sitelokurl!!!",$SitelokLocationURL,$mailBody);
$mailBody=str_replace("!!!sitelokemailurl!!!",$EmailURL,$mailBody);
$subject=str_replace("!!!sitelokurl!!!",$SitelokLocationURL,$subject);
$subject=str_replace("!!!sitelokemailurl!!!",$EmailURL,$subject);

if ($htmlformat=="Y")
{
  // For tinyMCE the body text should only be the part between <body> and </body>
  // We return the other parts for adding back when saving the template
//  preg_match("/<body[^>]*>(.*?)<\/body>/is", $mailBody, $matches);
//  $body=$matches[1];

  // Get start up to <body>
//  preg_match("/^(.*?)<body(.*?)>/is", $mailBody, $matches);
//  $top=$matches[0];

  // End from </body>
//  preg_match("/<\/body.*$/is", $mailBody, $matches);
//  $bottom=$matches[0];

  $content=$mailBody;

}
else
{
  $top="";
  $bottom="";
  $content=$mailBody;
}

$top=json_encode($top);
$body=json_encode($body);
$bottom=json_encode($bottom);
$subject=json_encode($subject);
$content=json_encode($content);


?>
{
  "success": true,
  "message": "",
  "htmlformat": "<?php echo $htmlformat; ?>",
  "subject": <?php echo $subject; ?>,
  "content": <?php echo $content; ?>
}