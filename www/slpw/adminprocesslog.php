<?php
  function slProcessLog($logmanageact,$mysql_link)
  {
    global $LogViewOffset,$DemoMode,$DbTableName,$DbConfigTableName,$DbLogTableName,$LogViewOrder,$LogViewDetails,$DateFormat;
    global $SitelokLocationURLPath,$ExportSeparator,$slplugin_logentrytype;
    $resdata=array();
    // Get input depending on action required
    if (($logmanageact=="export") || ($logmanageact=="view") || ($logmanageact=="clear"))
    {
      $fromdate=$_POST['fromdate'];
      $todate=$_POST['todate'];
      $usernm=$_POST['usernm'];
      $selectedonly=$_POST['selectedonly'];
      $logsortfield=$_POST['logsortfield'];
      $maxentries=$_POST['maxentries'];
      $timeoffset=$_POST['timeoffset'];
      $browsertimeoffset=$_POST['browsertimeoffset'];
      $order=$_POST['order'];    
      for ($k=1;$k<=50;$k++)    
      {
        $var="logentry".$k;
        $$var=$_POST['logentry'.$k];
      }
    }
    if ($logmanageact=="recentactivity")
    {
      $fromdate="";
      $todate="";
      $usernm=$_POST['usernm'];
      $selectedonly="";
      $logsortfield="id";
      $maxentries="25";
      $timeoffset=$LogViewOffset;
      $browsertimeoffset=$_POST['browsertimeoffset'];
      $order="DESC";
      for ($k=1;$k<=50;$k++)    
      {
        $var="logentry".$k;
        $$var="on";
      }      
    }
    if ($logmanageact!="recentactivity")
    {
      // Store time offset preference if changed
      if (($timeoffset!=$LogViewOffset) && (!$DemoMode))
      {
        $query="UPDATE ".$DbConfigTableName." SET logviewoffset=".$timeoffset." WHERE confignum=1";
        $mysql_result=mysqli_query($mysql_link,$query);
        $LogViewOffset=$timeoffset;
        $_SESSION['ses_ConfigReload']="reload";     
      }
      // Store output order if changed
      if (($order!=$LogViewOrder) && (!$DemoMode))
      {
        $query="UPDATE ".$DbConfigTableName." SET logvieworder='".$order."' WHERE confignum=1";
        $mysql_result=mysqli_query($mysql_link,$query);
        $LogViewOrder=$order;
        $_SESSION['ses_ConfigReload']="reload";      
      }
      // Store output details if changed
      $lvdtemp="";
      for ($k=1;$k<=50;$k++)
      {
        $var="logentry".$k;
        if ($$var=="on")
          $lvdtemp.="Y";
        else
          $lvdtemp.="N";      
      }    
      if (($lvdtemp!=$LogViewDetails) && (!$DemoMode))
      {
        $query="UPDATE ".$DbConfigTableName." SET logviewdetails='".$lvdtemp."' WHERE confignum=1";
        $mysql_result=mysqli_query($mysql_link,$query);
        $LogViewDetails=$lvdtemp;
        $_SESSION['ses_ConfigReload']="reload";      
      }
    }    
    $query="";
    if ($logentry1!="on")
    {
      if ($query!="")
        $query.=" AND ";
      $query.=$DbLogTableName.".type<>'login' AND ".$DbLogTableName.".type<>'logout'";
    }
    if ($logentry2!="on")
    {
      if ($query!="")
        $query.=" AND ";
      $query.=$DbLogTableName.".type<>'login problem'";
    }
    if ($logentry3!="on")
    {
      if ($query!="")
        $query.=" AND ";
      $query.=$DbLogTableName.".type<>'password requested'";
    }
    if ($logentry4!="on")
    {
      if ($query!="")
        $query.=" AND ";
      $query.=$DbLogTableName.".type<>'download'";
    }
    if ($logentry5!="on")
    {
      if ($query!="")
        $query.=" AND ";
      $query.=$DbLogTableName.".type<>'download problem'";
    }
    if ($logentry6!="on")
    {
      if ($query!="")
        $query.=" AND ";
      $query.=$DbLogTableName.".type<>'email'";
    }
    if ($logentry7!="on")
    {
      if ($query!="")
        $query.=" AND ";
      $query.=$DbLogTableName.".type<>'membership expired'";
    }
    if ($logentry8!="on")
    {
      if ($query!="")
        $query.=" AND ";
      $query.=$DbLogTableName.".type<>'user modify'";
    }
    if ($logentry9!="on")
    {
      if ($query!="")
        $query.=" AND ";
      $query.=$DbLogTableName.".type<>'api'";
    }
    if ($logentry10!="on")
    {
      if ($query!="")
        $query.=" AND ";
      $query.=$DbLogTableName.".type<>'register'";
    }
    for ($k=11;$k<=50;$k++)
    {
      if ($slplugin_logentrytype[$k]!="")
      {
        $var="logentry".$k;
        if ($$var!="on")
        {
          if ($query!="")
            $query.=" AND ";
          $query.=$DbLogTableName.".type<>'".$slplugin_logentrytype[$k]."'";
        }
      }
    }   
    $fromtimestring="";
    $totimestring="";
    if (($DateFormat=="DDMMYY") && ($fromdate!=""))
    {
      $day=substr($fromdate,0,2);
      $month=substr($fromdate,2,2);
      $year=substr($fromdate,4,2)+2000;
      $fromtimestring=$year."-".$month."-".$day." 00:00:00";
    }
    if (($DateFormat=="MMDDYY") && ($fromdate!=""))
    {
      $month=substr($fromdate,0,2);
      $day=substr($fromdate,2,2);
      $year=substr($fromdate,4,2)+2000;
      $fromtimestring=$year."-".$month."-".$day." 00:00:00";
    }
    if (($DateFormat=="DDMMYY") && ($todate!=""))
    {
      $day=substr($todate,0,2);
      $month=substr($todate,2,2);
      $year=substr($todate,4,2)+2000;
      $totimestring=$year."-".$month."-".$day." 23:59:59";
    }
    if (($DateFormat=="MMDDYY") && ($todate!=""))
    {
      $month=substr($todate,0,2);
      $day=substr($todate,2,2);
      $year=substr($todate,4,2)+2000;
      $totimestring=$year."-".$month."-".$day." 23:59:59";
    }
    if (($totimestring!="") && ($fromtimestring!=""))
    {
      if ($totimestring<$fromtimestring)
      {
        $temp=$fromtimestring;
        $fromtimestring=$totimestring;
        $totimestring=$temp;
      }
    }
    // Adjust for time offset
    if ($timeoffset=="9999")
    {
      if (is_numeric($browsertimeoffset))
        $timeoffset=($browsertimeoffset*-1);
      else
        $timeoffset=0;
      if ($fromtimestring!="")    
        $fromtimestring=dateoffsetmysqlstring($fromtimestring,($timeoffset*-1));
      if ($totimestring!="")    
        $totimestring=dateoffsetmysqlstring($totimestring,($timeoffset*-1));      
    }
    else
    {
      if ($fromtimestring!="")    
        $fromtimestring=dateoffsetmysqlstring($fromtimestring,($timeoffset*-1));
      if ($totimestring!="")    
        $totimestring=dateoffsetmysqlstring($totimestring,($timeoffset*-1));
    }
    if ($fromtimestring!="")
    {
      if ($query!="")
        $query.=" AND ";
      $query.=$DbLogTableName.".time>='".$fromtimestring."'";
    }
    if ($totimestring!="")
    {
      if ($query!="")
        $query.=" AND ";
      $query.=$DbLogTableName.".time<='".$totimestring."'";
    }
    if ($usernm!="")
    {
      if ($query!="")
        $query.=" AND ";
      $query.=$DbLogTableName.".username=".sl_quote_smart($usernm);
    }
    if ($selectedonly=="on")
    {
      if ($logmanageact=="clear")
      {
        if ($query!="")
          $query=$query." AND ";   
        $query="DELETE ".$DbLogTableName." FROM ".$DbLogTableName.", ".$DbTableName." WHERE ".$query.$DbLogTableName.".username=".$DbTableName.".username AND ".$DbTableName.".Selected='Yes'";
      }
      else
      {
        if ($query!="")
          $query=$query." AND ";   
        $query="SELECT ".$DbLogTableName.".* FROM ".$DbLogTableName.",".$DbTableName." WHERE ".$query.$DbLogTableName.".username=".$DbTableName.".username AND ".$DbTableName.".Selected='Yes'";    
      }
    }  
    else
    {
      if ($query!="")
        $query="WHERE ".$query;    
      if ($logmanageact=="clear")
        $query="DELETE FROM ".$DbLogTableName." ".$query;
      else  
        $query="SELECT * FROM ".$DbLogTableName." ".$query;
    }
    if (!(($selectedonly=="on") && ($logmanageact=="clear")))
    {
      if ($logsortfield=="")
        $logsortfield="id";
      if ($logsortfield!="id")
        $logsortfield.=", id";  
      if ($order=="ASC")
        $query.=" ORDER BY ".$logsortfield." ASC";  
      if ($order=="DESC")
        $query.=" ORDER BY ".$logsortfield." DESC";          
      if ($maxentries>0)
        $query.=" LIMIT ".$maxentries;
    }
    if ($logmanageact=="clear")
    {
      if (!$DemoMode)
      {
        $mysql_result=mysqli_query($mysql_link,$query);
        if ($mysql_result===false)
        {
          $resdata['success']=false;
          $resdata['numdeleted']=0;
          $resdata['activity']="";          
          return($resdata);
        }
        $numdeleted=mysqli_affected_rows($mysql_link);
      }
      else
        $numdeleted=0;
      $resdata['success']=true;
      $resdata['numdeleted']=$numdeleted;
      $resdata['activity']="";          
      return($resdata);
    }
    if ($logmanageact=="view")
    {
?>
<!DOCTYPE html>
<html>
<head>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo $SitelokLocationURLPath; ?>bootstrap/css/bootstrap.min.css">
  <script src="<?php echo $SitelokLocationURLPath; ?>jQuery/jQuery-2.1.4.min.js"></script>
  <script src="<?php echo $SitelokLocationURLPath; ?>bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
  <div class="table-responsive">    
    <table class="table table-condensed table-hover">
    <thead>
      <tr>
        <th><nobr><?php echo ADMINGENERAL_DATE; ?></nobr></th>
        <th><nobr><?php echo ADMINGENERAL_TIME; ?></nobr></th>
        <th><nobr><?php echo ADMINFIELD_USERNAME; ?></nobr></th>
        <th><nobr><?php echo ADMINGENERAL_TYPE; ?></nobr></th>
        <th><nobr><?php echo ADMINGENERAL_DETAILS; ?></nobr></th>
        <th><nobr><?php echo ADMINGENERAL_IPADR; ?></nobr></th>
        <th><nobr><?php echo ADMINGENERAL_SESSION; ?></nobr></th>
      </tr>
    </thead>
    <tbody>
<?php  
      if ($DateFormat=="MMDDYY")
        $dtfmt="m/d/y";
      else
        $dtfmt="d/m/y";
      $mysql_result=mysqli_query($mysql_link,$query);
      if ($mysql_result===false)
      {
        $resdata['success']=false;
        $resdata['numdeleted']=0;
        $resdata['activity']="";          
        return($resdata);
      }
      while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
      {
        $tm=$row['time'];
        $tm=dateoffsetmysqltimestamp($tm,$timeoffset);
        $us=$row['username'];
        $tp=$row['type'];
        $ip=$row['ip'];
        $msg=trim($row['details']);
        $ses=$row['session'];
        if ($us=="")
          $us="&nbsp;";
        if ($msg=="")
          $msg="&nbsp;";
        print "<tr>\n";
        print "<td><nobr>".gmdate($dtfmt,$tm)."</nobr></td>";
        print "<td><nobr>".gmdate("H:i:s",$tm)."</nobr></td>";
        if ($act!="recentactivity")
          print "<td><nobr>".sl_cleandata($us)."</nobr></td>";
        print "<td><nobr>".sl_cleandata($tp)."</nobr></td><td><nobr>".sl_cleandata($msg)."</nobr></td>";
        print "<td><nobr>".sl_cleandata($ip)."</nobr></td>";
        if ($act!="recentactivity")
          print "<td><nobr>".sl_cleandata($ses)."</nobr></td>";
        print "</tr>\n";
      }
?>
</tbody>
  </table>
  </div>
</div>
</body>
</html>
<?php
      $resdata['success']=true;
      $resdata['numdeleted']=0;
      $resdata['activity']="";          
      return($resdata);    
    }


    if ($logmanageact=="recentactivity")
    {
      $activity="";
      $activity.="<div class=\"container-fluid\">";
      $activity.="  <div class=\"table-responsive\">";    
      $activity.="    <table class=\"table table-condensed table-hover\">";
      $activity.="    <thead>";
      $activity.="      <tr>";
      $activity.="        <th><nobr>".ADMINGENERAL_DATE."</nobr></th>";
      $activity.="        <th><nobr>".ADMINGENERAL_TIME."</nobr></th>";
      $activity.="        <th><nobr>".ADMINGENERAL_TYPE."</nobr></th>";
      $activity.="        <th><nobr>".ADMINGENERAL_DETAILS."</nobr></th>";
      $activity.="        <th><nobr>".ADMINGENERAL_IPADR."</nobr></th>";
      $activity.="      </tr>";
      $activity.="    </thead>";
      $activity.="    <tbody>";
      if ($DateFormat=="MMDDYY")
        $dtfmt="m/d/y";
      else
        $dtfmt="d/m/y";
      $mysql_result=mysqli_query($mysql_link,$query);
      if ($mysql_result===false)
      {
        $resdata['success']=false;
        $resdata['numdeleted']=0;
        $resdata['activity']="";          
        return($resdata);
      }
      while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
      {
        $tm=$row['time'];
        $tm=dateoffsetmysqltimestamp($tm,$timeoffset);
        $us=$row['username'];
        $tp=$row['type'];
        $ip=$row['ip'];
        $msg=trim($row['details']);
        $ses=$row['session'];
        if ($us=="")
          $us="&nbsp;";
        if ($msg=="")
          $msg="&nbsp;";
        $activity.="<tr>";
        $activity.="<td><nobr>".gmdate($dtfmt,$tm)."</nobr></td>";
        $activity.="<td><nobr>".gmdate("H:i:s",$tm)."</nobr></td>";
        $activity.="<td><nobr>".sl_cleandata($tp)."</nobr></td><td><nobr>".sl_cleandata($msg)."</nobr></td>";
        $activity.="<td><nobr>".sl_cleandata($ip)."</nobr></td>";
        $activity.="</tr>";
      }
      $activity.="</tbody>";
      $activity.="  </table>";
      $activity.="  </div>";
      $activity.="</div>";
      $resdata['success']=true;
      $resdata['numdeleted']=0;
      $resdata['activity']=$activity;
      return($resdata);    
    }



    if ($logmanageact=="export")
    {
      if (!isset($ExportSeparator))
        $ExportSeparator=",";
      header("Last-Modified: ".gmdate('D, d M Y H:i:s T'));
      header("Content-type: application/octet-stream");
      header("Content-disposition: attachment; filename=siteloklog.csv");
      header("Content-transfer-encoding: binary");      
      $mysql_result=mysqli_query($mysql_link,$query);
      if ($mysql_result===false)
      {
        $resdata['success']=false;
        $resdata['numdeleted']=0;
        $resdata['activity']="";          
        return($resdata);
      }
      while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
      {
        $tm=$row['time'];
        if ($timeoffset!=0)
          $tm=dateoffsetmysqlstring($tm,$timeoffset);
        $us=$row['username'];
        $tp=$row['type'];
        $ip=$row['ip'];
        $msg=trim($row['details']);
        $ses=$row['session'];
        print "\"".$tm."\"".$ExportSeparator."\"".$us."\"".$ExportSeparator."\"".$tp."\"".$ExportSeparator."\"".$msg."\"".$ExportSeparator."\"".$ip."\"".$ExportSeparator."\"".$ses."\"\n";
      }
      $resdata['success']=true;
      $resdata['numdeleted']=0;
      $resdata['activity']="";          
      return($resdata);          
    }
}        

function dateoffsetmysqlstring($time,$offset)
{
  $year=substr($time,0,4);
  $month=substr($time,5,2);
  $day=substr($time,8,2);
  $hour=substr($time,11,2);
  $minute=substr($time,14,2);
  $second=substr($time,17,2);
  $timestamp=gmmktime($hour,$minute,$second,$month,$day,$year);
  $timestamp=$timestamp+($offset*60);
  $newtime=gmdate("Y-m-d H:i:s",$timestamp);
  return($newtime);
}

function dateoffsetmysqltimestamp($time,$offset)
{
  $year=substr($time,0,4);
  $month=substr($time,5,2);
  $day=substr($time,8,2);
  $hour=substr($time,11,2);
  $minute=substr($time,14,2);
  $second=substr($time,17,2);
  $timestamp=gmmktime($hour,$minute,$second,$month,$day,$year);
  $timestamp=$timestamp+($offset*60);
  return($timestamp);
}
