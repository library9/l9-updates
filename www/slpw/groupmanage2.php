<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  $act=$_GET['act'];
  $actid=$_GET['actid'];
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    die(ADMINMSG_MYSQLERROR);   
  }
  // If edit or duplicate then get current group details
  if (($act=="editgroup") || ($act=="duplgroup"))
  {
    $query="SELECT * FROM ".$DbGroupTableName." WHERE id=".sl_quote_smart($actid);
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_result!=false)
    {
      $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
      if (!$row)
      {
        print "Could not access usergroup";
        exit;
      }
      $newgroupname=$row['name'];
      if ($act=="duplgroup")
        $newgroupname.="COPY";
      $newdescription=$row['description'];
      $newloginaction=$row['loginaction'];
      $newloginvalue=$row['loginvalue'];
    }
    else
    {
      die(ADMINMSG_MYSQLERROR);   
    }
  }   
?>
<!DOCTYPE html>
<html>
<head>
<?php
$pagename="groupmanage2";
include("adminhead.php");
?>
<title><?php if (($act=="addgroup") || ($act=="duplgroup")) echo ADMINMENU_ADDUSERGROUP; else echo ADMINMENU_EDITUSERGROUP; ?></title>
<link rel="stylesheet" href="groupmanage2.css"></head>
<?php include("adminthemeheader.php"); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php if (($act=="addgroup") || ($act=="duplgroup")) echo ADMINMENU_ADDUSERGROUP; else echo ADMINMENU_EDITUSERGROUP; ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><a href="groupmanage.php"><?php echo ADMINMENU_USERGROUPS; ?></a></li>
            <li class="active"><?php if (($act=="addgroup") || ($act=="duplgroup")) echo ADMINMENU_ADDUSERGROUP; else echo ADMINMENU_EDITUSERGROUP; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>
          <!-- Your Page Content Here -->

          <div class="row">

            <form id="usergroupform" role="form" class="form-horizontal" method="post">
            <input type="hidden" name="slcsrf" id="slcsrf" value="<?php echo $slcsrftoken; ?>">
            <input type="hidden" name="act" id="act" value="<?php echo $act; ?>">
            <input type="hidden" name="actid" id="actid" value="<?php if ($act=="editgroup") echo $actid; ?>">
            <input type="hidden" name="oldgroupname" id="oldgroupname" value="<?php if ($act=="editgroup") echo $newgroupname; ?>">

            <div class="col-xs-12">

              <div class="box">

                <div class="box-body">

                  <div class="form-group" id="newgroupnamegroup">
                      <label class="col-xs-12" for="newgroupname"><?php echo "Usergroup name"; ?></label>
                      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2" id="newgroupnamediv">
                          <input type="text" class="form-control" name="newgroupname" id="newgroupname" maxlength="255" value="<?php echo $newgroupname; ?>" placeholder="GROUPNAME" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                      </div>
                  </div>

                  <div class="form-group" id="newdescriptiongroup">
                      <label class="col-xs-12" for="newdescription"><?php echo "Usergroup description"; ?></label>
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="newdescriptiondiv">
                          <input type="text" class="form-control" name="newdescription" id="newdescription" maxlength="255" value="<?php echo htmlentities($newdescription,ENT_QUOTES,strtoupper($MetaCharSet)); ?>" placeholder="Group description" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                      </div>
                  </div>

                  <div class="form-group" id="newloginactiongroup">
                      <label class="col-xs-12" for="newloginaction"><?php echo "Login action"; ?></label>
                       <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" id="newloginactiondiv">
                          <select id="newloginaction" name="newloginaction" class="form-control selectpicker">
                          <option value="" <?php if ($newloginaction=="") echo "selected=\"selected\""; ?>><?php echo ADMINMU_NONE; ?></option>
                          <option value="URL" <?php if ($newloginaction=="URL") echo "selected=\"selected\""; ?>><?php echo ADMINMU_URL; ?></option>
                          <?php
                            for ($k=1;$k<=50;$k++)
                            {
                              $namevar="custom".$k;
                              $titlevar="CustomTitle".$k;
                              if ($$titlevar=="")
                                $$titlevar="Custom ".$k;
                              ?>  
                              <option value="<?php echo $namevar; ?>" <?php if ($newloginaction==$namevar) echo "selected=\"selected\"";?>><?php echo ADMINMU_CUSTOM; ?><?php echo $$titlevar; ?></option>
                              <?php
                            }
                          ?>
                          </select>  
                      </div>
                  </div> 


                  <div class="form-group" id="newloginvaluegroup">
                      <label class="col-xs-12" for="newloginvalue"><?php echo "URL"; ?></label>
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="newloginvaluediv">
                          <input type="text" class="form-control" name="newloginvalue" id="newloginvalue" maxlength="255" value="<?php echo $newloginvalue; ?>" placeholder="/folder/page.php" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                      </div>
                  </div>


                </div><!-- /.box-body -->
              </div><!-- /.box -->


                    <div class="form-group">
                      <div class="col-xs-12">
                        <div class="btn-toolbar">
                            <button type="submit" id="submit" class="btn btn-primary pull-left"><?php if (($act=="addgroup") || ($act=="duplgroup")) echo ADMINMU_ADD; else echo ADMINBUTTON_SAVECHANGES ?></button>
                            <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='groupmanage.php';"><?php echo ADMINBUTTON_RETURNTOUGLIST; ?></button>
                            <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD; ?></button>
                        </div> 
                      </div>    
                    </div>

              <div id="resultusergroup"></div>



            </div><!-- /.col -->


            </form>

          </div><!-- /.row -->

<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>

      </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("adminthemefooter.php"); ?>
    <script src="groupmanage2.js"></script>

  </body>
</html>
