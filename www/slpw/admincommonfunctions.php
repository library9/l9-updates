<?php
function populateUsergroupSelect($selgrp="")
{
  $usergroupNames=explode(",",$_SESSION['ses_slgroupnames']);
  for ($k=0;$k<count($usergroupNames);$k++)
  {
    if (($selgrp!="") && ($selgrp==$usergroupNames[$k]))
      echo'<option value="'.$usergroupNames[$k].'">'.$usergroupNames[$k].' selected </option>'."\n";
    else
      echo'<option value="'.$usergroupNames[$k].'">'.$usergroupNames[$k].'</option>'."\n";      
  }
}

function dateValid($dt,$fmt)
{
  if ($fmt=="DDMMYY")
  {
    $day=substr($dt,0,2);
    $month=substr($dt,2,2);
    $year=substr($dt,4,2);
  }
  else
  {
    $month=substr($dt,0,2);
    $day=substr($dt,2,2);
    $year=substr($dt,4,2);
  }
  if (($year<0) || ($year>37))
    return(false);
  return(checkdate($month, $day, $year+2000));
}

function isValidColor($color)
{
  if (strlen($color)!=7)
    return(false);
  if (substr($color,0,1)!='#')
    return(false);
  $color=substr($color,1);  
  if ((strspn($color, "0123456789abcdefABCDEF") != strlen($color)))
    return(false);
  return(true);  
}

function isValidSize($size)
{
  if ((strspn($size, "0123456789") != strlen($size)))
    return(false);
  if ($size>99)
    return(false);
  return(true);    
}

function validInteger($value,$min,$max,$reqd)
{
  if (($reqd) && ($value==""))
    return(false);
  if ((!$reqd) && ($value==""))
    return(true);
  if ((strspn($value, "0123456789") != strlen($value)))
    return(false);
  if ((value<$min) || ($value>$max))
    return(false);
  return(true);    
}

function validUsergroup($val,$reqd)
{
  if (($reqd) && ($val==""))
    return(false);
  if ((!$reqd) && ($val==""))
    return(true);  
  if(strspn($val, "#{}()@.|0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") != strlen($val))
    return(false);
  return(true);
}