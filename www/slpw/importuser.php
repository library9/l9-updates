<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  include("admincommonfunctions.php");
  $systemmaxupload=ini_get('upload_max_filesize');
  // Get configuration data
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    die(ADMINMSG_MYSQLERROR);
    exit;
  }
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_adminconfig LIMIT 1");
  if ($mysql_result!=false)
  {
    $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
    if ($row!=false)
    {
      $importheader=$row["importheader"];
      $importuseemailas=$row["importuseemailas"];
      $importaddusers=$row["importaddusers"];
      $importrandpass=$row["importrandpass"];
      $importusergroups=$row["importusergroups"];
      $importslctadded=$row["importslctadded"];
      $importexistusers=$row["importexistusers"];
      $importblank=$row["importblank"];
      $importslctexist=$row["importslctexist"];
    }
    // Split importusergroups into usergroups and expiries
    $grouparray=explode("^",$importusergroups);
    for ($k=0;$k<count($grouparray);$k++)
    {
      $pvar1="group".($k+1);
      $pvar2="expirydate".($k+1);
      $$pvar1=strtok($grouparray[$k],":");
      $$pvar2=trim(strtok(":"));
    }
    $groupcount=count($grouparray);
  }
?>
<!DOCTYPE html>
<html>
<head>
<?php
$pagename="importuser";
include("adminhead.php");
?>
<title>Edit user</title>
<link rel="stylesheet" href="importuser.css"></head>
<?php include("adminthemeheader.php"); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="glyphicon glyphicon-import"></i> <?php echo ADMINMENU_IMPORTUSERS; ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><?php echo ADMINMENU_IMPORTUSERS; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>
          <div class="row">

                    <form id="importuserform" role="form" class="form-horizontal" method="post" action="">
                    <input type="hidden" name="slcsrf" id="slcsrf" value="<?php echo $slcsrftoken; ?>">

            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo ADMINIU_IMPORTFILE; ?></h3>
                </div>
                <div class="box-body">

                  <div class="form-group" id="slimportedusersgroup">
                      <label class="col-xs-12" for="slimportedusers"><?php echo "Select file (max ".$systemmaxupload.")"; ?></label>
                      <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5" id="slimportedusersdiv">
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    <?php echo ADMINBUTTON_BROWSE; ?> <input type="file" name="slimportedusers" id="slimportedusers" style="display: none;">
                                </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                         </div>
                      </div>
                  </div>

                  <div class="form-group" id="firstrowheadergroup">
                      <div class="col-sm-10" id="firstrowheaderdiv">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="firstrowheader" id="firstrowheader" value="on" <?php if ($importheader=="1") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINIU_HEADER; ?>
                          </label>
                        </div>
                      </div>
                  </div>

                  <div class="form-group">
                    <label class="col-xs-12" for="useemailasuser"><?php echo ADMINIU_EMAILCOL; ?></label>
                     <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5" id="useemailasuserdiv">
                        <select id="useemailasuser" name="useemailasuser" class="form-control selectpicker">
                        <option value="emailnever" <?php if ($importuseemailas=="emailnever") echo "selected "?>><?php echo ADMINIU_EMAILCOL1; ?></option>
                        <option value="emailifnouser" <?php if ($importuseemailas=="emailifnouser") echo "selected "?>><?php echo ADMINIU_EMAILCOL2; ?></option>
                        <option value="emailalways" <?php if ($importuseemailas=="emailalways") echo "selected "?>><?php echo ADMINIU_EMAILCOL3; ?></option>
                        </select>  
                    </div>
                   </div> 

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo ADMINIU_NEWUSERS; ?></h3>
                </div>
                <div class="box-body">

                  <div class="form-group" id="addusersgroup">
                      <div class="col-sm-10" id="addusersdiv">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="addusers" id="addusers" value="on" <?php if ($importaddusers=="1") echo "checked "; ?> >&nbsp;&nbsp;<?php echo ADMINIU_ADD; ?>
                          </label>
                        </div>
                      </div>
                  </div>

                  <div class="form-group">
                    <label class="col-xs-12" for="randpass"><?php echo ADMINIU_RND; ?></label>
                     <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5" id="randpassdiv">
                        <select id="randpass" name="randpass" class="form-control selectpicker">
                        <option value="randnewonly" <?php if ($importrandpass=="randnewonly") echo "selected "; ?>><?php echo ADMINIU_RND1; ?></option>
                        <option value="randnew" <?php if ($importrandpass=="randnew") echo "selected "; ?>><?php echo ADMINIU_RND2; ?></option>
                        </select>  
                    </div>
                  </div> 

                    
                  <div class="form-group" style="margin-bottom: 0px;">
                    <label class="col-xs-10"><?php echo ADMINIU_USERGROUPS; ?></label>
                  </div>
<?php
for ($k=1;$k<=$groupcount;$k++)
{
?>
                           <div id="groupblock<?php echo $k; ?>"> 
                             <div class="form-group">
                                <div class="col-xs-6 col-sm-5 col-md-4 col-lg-3" id="group<?php echo $k; ?>div">
                                   <select placeholder="<?php echo strtolower(ADMINFILTER_USERGROUP); ?>" class="form-control usergrouplist" id="group<?php echo $k; ?>" name="group<?php echo $k; ?>">
                                   <?php populateUsergroupSelect(); ?>
                                   </select> 
                                </div>

                                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2" id="expiry<?php echo $k; ?>div">
                                  <div class="input-group datepicker" id="dateexpiry<?php echo $k; ?>">
                                     <input type="text" placeholder="<?php echo ADMINADDUSER_EXPIREPLACE.$DateFormat; ?>" class="form-control" id="expiry<?php echo $k; ?>" name="expiry<?php echo $k; ?>" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                     <div class="input-group-addon"><span class="glyphicon glyphicon-calendar actionicon"></span></div>
                                  </div>   
                                </div>

                                <div class="col-sm-3 col-md-3 col-lg-2 hidden-xs">
                                  <span class="glyphicon glyphicon-triangle-top actionicon" title="<?php echo ADMINBUTTON_MOVEUP; ?>" onclick="groupUp(<?php echo $k; ?>);"></span>
                                  <span class="glyphicon glyphicon-triangle-bottom actionicon" title="<?php echo ADMINBUTTON_MOVEDOWN; ?>" onclick="groupDown(<?php echo $k; ?>);"></span>
                                  <span class="glyphicon glyphicon-remove removegroup actionicon" title="<?php echo ADMINBUTTON_REMOVE; ?>" onclick="removeGroup(<?php echo $k; ?>);"></span>
                                </div>
                            </div>
                          </div>
<?php } ?>


                          <div id="extraGroupTemplate">
<!--                            <div id="groupblockXX">
                              <div class="form-group">
                                <div class="col-xs-6 col-sm-5 col-md-4 col-lg-3" id="groupXXdiv">
                                   <select placeholder="<?php echo strtolower(ADMINFILTER_USERGROUP); ?>" class="form-control usergrouplist" id="groupXX" name="groupXX">
                                   <?php populateUsergroupSelect(); ?>                 
                                   </select> 
                                </div>

                                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2" id="expiryXXdiv">
                                  <div class="input-group datepicker" id="dateexpiryXX">
                                     <input type="text" placeholder="<?php echo ADMINADDUSER_EXPIREPLACE.$DateFormat; ?>" class="form-control" id="expiryXX"  name="expiryXX" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                     <div class="input-group-addon"><span class="glyphicon glyphicon-calendar actionicon"></span></div>
                                  </div>   
                                </div>

                                <div class="col-sm-3 col-md-3 col-lg-2 hidden-xs" id="expiryXXdiv">
                                  <span class="glyphicon glyphicon-triangle-top actionicon" title="<?php echo ADMINBUTTON_MOVEUP; ?>" onclick="groupUp(XX);"></span>
                                  <span class="glyphicon glyphicon-triangle-bottom actionicon" title="<?php echo ADMINBUTTON_MOVEDOWN; ?>" onclick="groupDown(XX);"></span>
                                  <span class="glyphicon glyphicon-remove removegroup actionicon" title="<?php echo ADMINBUTTON_REMOVE; ?>" onclick="removeGroup(XX);"></span>
                                </div>
                            </div
                          </div>
 -->                     </div>


                        <div id="extraGroupInsert"></div>



                          <div class="form-group">
                              <div class="col-xs-12">
                                <button type="button" class="btn btn-xs btn-default" onclick="addGroup();">
                                  <span class="glyphicon glyphicon-plus actionicon"></span>
                                </button>
                              </div>
                          </div>



                  <div class="form-group" id="selectaddedgroup">
                      <div class="col-sm-10" id="selectaddeddiv">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="selectadded" id="selectadded" value="on" <?php if ($importslctadded=="1") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINIU_SELADD; ?>
                          </label>
                        </div>
                      </div>
                  </div>




                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo ADMINIU_UPDUSERS; ?></h3>
                </div>
                <div class="box-body">

                  <div class="form-group" id="existingusersgroup">
                      <div class="col-sm-10" id="existingusersdiv">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="existingusers" id="existingusers" value="on" <?php if ($importexistusers=="1") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINIU_UPDATE; ?>
                          </label>
                        </div>
                      </div>
                  </div>

                  <div class="form-group">
                    <label class="col-xs-12" for="blankdata"><?php echo ADMINIU_BLANK; ?></label>
                     <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5" id="blankdatadiv">
                        <select id="blankdata" name="blankdata" class="form-control selectpicker">
                        <option value="allowblank" <?php if ($importblank=="allowblank") echo "selected "; ?>><?php echo ADMINIU_BLANKUPD; ?></option>
                        <option value="ignoreblank" <?php if ($importblank=="ignoreblank") echo "selected "; ?>><?php echo ADMINIU_BLANKIGN; ?></option>
                        </select>  
                    </div>
                   </div> 

                  <div class="form-group" id="selectupdatedgroup">
                      <div class="col-sm-10" id="selectupdateddiv">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="selectupdated" id="selectupdated" value="on" <?php if ($importslctexist=="1") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINIU_SELUPD; ?>
                          </label>
                        </div>
                      </div>
                  </div>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

 
            <div class="col-xs-12">
                   <div class="form-group">
                    <div class="col-sm-11">
                        <div class="btn-toolbar">
                            <button type="submit" id="submit" class="btn btn-primary pull-left"><?php echo ADMINMENU_IMPORTUSERS; ?></button>
                            <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                        </div>   
                    </div>
                    </div>

                      <div id="resultimportuser"></div>


<div id="importdata">
</div>

<!-- log modal -->
<div class="modal fade bannerformmodal" tabindex="-1" role="dialog" aria-labelledby="logModal" aria-hidden="true" id="logModal">
<div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo ADMINIU_USERSIGN; ?></h4>
                </div>
                <div class="modal-body" id="logcontent">
                </div>
        </div>
        </div>
      </div>
    </div>

            </div><!-- /.col -->




          </div><!-- /.row -->

<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("adminthemefooter.php"); ?>
<script type="text/javascript">
  var groupcount=<?php echo ($groupcount+1)."\n"; ?>
</script>
<script src="importuser.js"></script>
<script type="text/javascript">
// Javascript here needs PHP so is not in .js file
<?php for ($k=1;$k<=$groupcount;$k++)
{
  $gvar="group".$k;
  $xvar="expirydate".$k;
?>
  // Make select into combo
  $('#group<?php echo $k; ?>').editableSelect();
  $('#group<?php echo $k; ?>').val('<?php echo $$gvar; ?>');
  // Enable calendar
  $('#dateexpiry<?php echo $k; ?>').datetimepicker({'keepInvalid': true, 'format':DateFormat,'defaultDate':'', 'useStrict': true});
  $('#expiry<?php echo $k; ?>').val('<?php echo $$xvar; ?>');
<?php } ?> 

</script>    

  </body>
</html>
