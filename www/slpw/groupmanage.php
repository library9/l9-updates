<?php
  $groupswithaccess="ADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
?>
<!DOCTYPE html>
<html>
<head>
<?php 
$pagename="groupmanage";
include("adminhead.php");
?>
<title><?php echo ADMINMENU_USERGROUPS; ?></title>
<link rel="stylesheet" href="groupmanage.css"></head>
<?php include("adminthemeheader.php"); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-group"></i>&nbsp;<?php echo ADMINMENU_USERGROUPS; ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><?php echo ADMINMENU_USERGROUPS; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>
          <!-- Your Page Content Here -->
          <form name="form">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">

                <div class="box-body">

                  <div id="grouplist">
                  </div>

                    <div class="form-group">
 
                            <button type="button" id="cancel" class="btn btn-xs btn-primary pull-left" onclick="window.location.href='groupmanage2.php?act=addgroup';"><?php echo ADMINMU_ADD; ?></button>
                    </div>    

                </div><!-- /.box-body -->
              </div><!-- /.box -->





            </div><!-- /.col -->

                    <div class="form-group">
                      <div class="col-xs-12">
 
                        <div class="btn-toolbar">
                            <button type="button" id="cancel" class="btn btn-primary pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                        </div> 

                      </div>    
                    </div>


          </div><!-- /.row -->
                </form>  
                <br>

<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("adminthemefooter.php"); ?>
    <script src="groupmanage.js"></script>

  </body>
</html>
