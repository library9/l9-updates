updatePluginList(0);

function updatePluginList(checkversions)
{
  //Remove form message
  $('#resultplugin').html('');
  $('#pluginlist').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
  var formData = {
      'slcsrf'    : slcsrf,
      'checkversions' : checkversions
  };
  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'admingetplugins.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      .done(function(data) {
        sessionExpiredReported(data,true);
        if (data.success)
        {
          var updatesfound=false;
          if (data.plugincount>0)
          {
            var htmlstr="";
            htmlstr+='<table>';
            for (k=0;k<data.plugincount;k++)
            {
              htmlstr+='<tr>';
              htmlstr+='<td class="actions">';
              htmlstr+='<img class="plicon" src="'+data.folders[k]+'/'+data.icons[k]+'" rel="tooltip" title="'+data.names[k]+'">';
              if (data.enabled[k]=="Yes")
              {
                htmlstr+='&nbsp;&nbsp;<a href="#" onclick="event.preventDefault();disablePlugin('+data.pluginids[k]+',\''+data.names[k]+'\');"><i class="fa fa-toggle-on enabledicon actionicon" rel="tooltip" title="'+ADMINPL_CLKDIS+'"></i></a>';
                htmlstr+='&nbsp;&nbsp;<a href="#" onclick="event.preventDefault();pluginPageConfig('+data.pluginids[k]+','+data.pluginindexes[k]+',\''+data.folders[k]+'/'+data.configs[k]+'\','+data.v5s[k]+');"><i class="fa fa-cogs actionicon" rel="tooltip" title="'+ADMINPL_CONFIG+'"></i></a>';
                htmlstr+='&nbsp;&nbsp;<span class="glyphicon glyphicon-remove disabledicon uninsticon" rel="tooltip" title="'+ADMINPL_DISTOUNSTL+'"</span>';
              }
              else
              {  
                htmlstr+='&nbsp;&nbsp;<a href="#" onclick="event.preventDefault();enablePlugin('+data.pluginids[k]+',\''+data.names[k]+'\');"><i class="fa fa-toggle-off enabledicon actionicon" rel="tooltip" title="'+ADMINPL_CLKEN+'"></i></a>';                
                htmlstr+='&nbsp;&nbsp;<i class="fa fa-cogs disabledicon"></i>';
                htmlstr+='&nbsp;&nbsp;<a href="#" onclick="event.preventDefault();uninstallPlugin('+data.pluginids[k]+',\''+data.names[k]+'\',\''+data.folders[k]+'/uninstall.php'+'\','+data.v5s[k]+');"><span class="glyphicon glyphicon-remove actionicon uninsticon" rel="tooltip" title="'+ADMINPL_CLKUNSTL+'" onclick="uninstallPlugin(\''+data.pluginids[k]+'\',\''+data.names[k]+'\');"></span>';
              }  
              htmlstr+='</td>\n';
              if (data.enabled[k]=="Yes")
                htmlstr+='<td class="pluginname">'+data.names[k]+' V'+data.versions[k]+'</td>';
              else
                htmlstr+='<td class="pluginnamedisabled">'+data.names[k]+' V'+data.versions[k]+'</td>';                
              if (parseFloat(data.latestversions[k])>parseFloat(data.versions[k]))
              {
                updatesfound=true;
                htmlstr+='</tr>';
                htmlstr+='<tr>';
                var str=ADMINUPDATE_UPDAVAIL;
                str=str.replace("***1***",data.latestversions[k]);
                htmlstr+='<td class="pluginnewversion">&nbsp;</td><td class="pluginnewversion">'+str;
                if (parseFloat(data.latestminslversion[k])>parseFloat(SitelokVersion))
                {
                  str=ADMINUPDATE_PLGREQ;
                  str=str.replace("***1***",data.latestminslversion[k]);
                  htmlstr+=' '+str;
                }
                htmlstr+=' <a href="'+data.latestversionlinks[k]+'" target="_blank"><span class="glyphicon glyphicon-info-sign actionicon updateicon"></span></a></td>';
                htmlstr+='</tr>';
                htmlstr+='</tr>';
              }
            }
            htmlstr+='</table>';            
          }
          else
          {
            htmlstr='<p class="pluginmessage">'+ADMINPL_NOPLUGINS+'</p>';
          }
          $('#pluginlist').html(htmlstr);
          if ((updatesfound) && (checkversions==1) && (!data.versioncheckfailed))
          {
            $('#resultplugin').html('<div id="resultpluginmessage" class="alert alert-warning">' + ADMINUPDATE_UPDSAVAIL + '</div>');
          }
          if ((!updatesfound) && (checkversions==1) && (!data.versioncheckfailed))
          {
            $('#resultplugin').html('<div id="resultpluginmessage" class="alert alert-success">' + ADMINUPDATE_PLGNSOK + '</div>');
          }
          if ((checkversions==1) && (data.versioncheckfailed))
          {
            $('#resultplugin').html('<div id="resultpluginmessage" class="alert alert-danger">' + ADMINUPDATE_CANTUPD + '</div>');
          }
        }
        else
        {
          $('#pluginlist').html('<p class="pluginerror">'+data.message+'</p>');
        }
      })

    .fail(function(data) {
      $('#pluginlist').html('<p class="pluginerror">'+ADMINPL_NOACCESS+'</p>');
     });
}

function pluginPageConfig(pluginid,pluginindex,plugin,v5)
{
  if (v5)
    window.location.href = plugin+"?act=pluginconfig&pluginid="+pluginid+"&pluginindex="+pluginindex+"&numsel="+numsel;
  else
    window.location.href = plugin+"?slcsrf="+slcsrf+"&act=pluginconfig&pluginid="+pluginid+"&pluginindex="+pluginindex+"&numsel="+numsel;
}

function enablePlugin(id,name)
{
  var msg=ADMINPL_ENVER.replace("***1***",name);
  bootbox.confirm(msg, function(result) {
    if (result)
    {
      var datatosend = { pluginid : id, act: 'enable', slcsrf : slcsrf};
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'adminmanageplugins.php', // the url where we want to POST
            data        : datatosend,
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        .done(function(data) {
          sessionExpiredReported(data,true);
          if (data.success)
            updatePluginList(0);
          else
          {
            bootbox.alert(data.message);
            updatePluginList(0);
          }
        })
        .fail(function() {
          bootbox.alert(ADMINPL_NOTEN);
          updatePluginList(0);
        })
    }  
  });
}

function disablePlugin(id,name)
{
  var msg=ADMINPL_DISVER.replace("***1***",name);
  bootbox.confirm(msg, function(result) {
    if (result)
    {
      var datatosend = { pluginid : id, act: 'disable', slcsrf : slcsrf};
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'adminmanageplugins.php', // the url where we want to POST
            data        : datatosend,
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        .done(function(data) {
          sessionExpiredReported(data,true);
          if (data.success)
            updatePluginList(0);
          else
          {
            bootbox.alert(data.message);
            updatePluginList(0);
          }
        })
        .fail(function() {
          bootbox.alert(ADMINPL_NOTDIS);
          updatePluginList(0);
        })
    }  
  });
}

function uninstallPlugin(id,name,p,v5)
{
  var msg=ADMINPL_UNSTLVER.replace("***1***",name);
  bootbox.confirm(msg, function(result) {
    if (result)
    {
      window.location = p;
    }  
  });
}

function checkForPluginUpdates()
{
  updatePluginList(1);
}







