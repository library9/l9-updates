// Fix bug where bootstrap doesn't close panels properly after call to collapse
$('#accordion').on('show.bs.collapse', function () {
    $('#accordion .in').collapse('hide');
});

// Switch + and - icons on accordion panels
$('.collapse').on('shown.bs.collapse', function(){
$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
}).on('hidden.bs.collapse', function(){
$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
});

// Enable select boxes
$('.selectpicker').selectpicker({
  style: 'btn-default',
  size: 4,
  dropupAuto: false  
});

// Convert select to combo
$('#newusergroup').editableSelect();

// Setup user email template checkbox
$('#senduseremail').on('ifChecked', function(event){
               $('#senduseremaildiv').show();
});
$('#senduseremail').on('ifUnchecked', function(event){
               $('#senduseremaildiv').hide();
});

// Setup admin email template checkbox
$('#sendadminemail').on('ifChecked', function(event){
               $('#sendadminemaildiv').show();
});
$('#sendadminemail').on('ifUnchecked', function(event){
               $('#sendadminemaildiv').hide();
});

<!-- Setup minicolor control -->
  var colpick = $('.colorbox').each( function() {
    $(this).minicolors({
      control: $(this).attr('data-control') || 'hue',
      inline: $(this).attr('data-inline') === 'true',
      letterCase: 'uppercase',
      opacity: false,
      changeDelay: 250,
      theme: 'bootstrap'
    });
  });

$('.selectpicker').selectpicker({
  style: 'btn-default',
  size: 'auto',
  dropupAuto: false
});

// Validate fields on blur
$("#newformname").focusout(function(){
  validateReqdBlur('#newformname','untitled');
});

$("#newusergroup").focusout(function(){
  validateGroupBlur('#newusergroup','CLIENT');
});

$("#newexpiry").focusout(function(){
  validateExpiryBlur('#newexpiry','365');
});

$("#newredirect").focusout(function(){
  validateReqdBlur('#newredirect','registerthanks.php');
});

$("#newlabelcolor").focusout(function(){
  validateColorBlur('#newlabelcolor','#1A305E');
  updatePreview();
});

$("#newlabelsize").focusout(function(){
  validateSizeBlur('#newlabelsize','18');
  updatePreview();
});

$("#newinputtextcolor").focusout(function(){
  validateColorBlur('#newinputtextcolor','#1A305E');
  updatePreview();
});

$("#newinputtextsize").focusout(function(){
  validateSizeBlur('#newinputtextsize','16');
  updatePreview();
});

$("#newbordercolor").focusout(function(){
  validateColorBlur('#newbordercolor','#378EE5');
  updatePreview();
});

$("#newrqdfieldcolor").focusout(function(){
  validateColorBlur('#newrqdfieldcolor','#FF0000');
  updatePreview();
});

$("#newrqdfieldsize").focusout(function(){
  validateSizeBlur('#newrqdfieldsize','12');
  updatePreview();
});

$("#newmessagecolor").focusout(function(){
  validateColorBlur('#newmessagecolor','#FF0000');
  updatePreview();
});

$("#newmessagesize").focusout(function(){
  validateSizeBlur('#newmessagesize','12');
  updatePreview();
});

$("#newformerrormsgcolor").focusout(function(){
  validateColorBlur('#newformerrormsgcolor','#FF0000');
  showformerrormessage=false;
  updatePreview();  
});

$("#newformerrormsgsize").focusout(function(){
  validateSizeBlur('#newformerrormsgsize','12');
  showformerrormessage=false;
  updatePreview();
});

$("#newmaxformwidth").focusout(function(){
  validateIntegerBlur('#newmaxformwidth',1,999999,'300');
  updatePreview();
});

$("#newbtnlabelcolor").focusout(function(){
  validateColorBlur('#newbtnlabelcolor','#FFFFFF');
  updatePreview();
});

$("#newbtnlabelsize").focusout(function(){
  validateSizeBlur('#newbtnlabelsize','14');
  updatePreview();
});

$("#newbtncolorfrom").focusout(function(){
  validateColorBlur('#newbtncolorfrom','#1A305E');
  updatePreview();
});

$("#newbtncolorto").focusout(function(){
  validateColorBlur('#newbtncolorto','#378EE5');
  updatePreview();
});

$("#newbtnbordercolor").focusout(function(){
  validateColorBlur('#newbtnbordercolor','#FFFFFF');
  updatePreview();
});

$("#newbtnbordersize").focusout(function(){
  validateSizeBlur('#newbtnbordersize','0');
  updatePreview();
});

// Handle showing error message on exampoe form when related fields are focussed
$('#newformerrormsgstyle').on('show.bs.select', function (e) {
  showformerrormessage=true;
  updatePreview();
});

$('#newformerrormsgstyle').on('hide.bs.select', function (e) {
  showformerrormessage=false;
  updatePreview();
});

$("#newformerrormsgcolor").focusin(function(){
  showformerrormessage=true;
  updatePreview();
});
//focusout handled previously

$("#newformerrormsgsize").focusin(function(){
  showformerrormessage=true;
  updatePreview();
});
//focusout handled previously

$("#newformerrormsg").focusin(function(){
  showformerrormessage=true;
  updatePreview();
});

$("#newformerrormsg").focusout(function(){
  showformerrormessage=false;
  updatePreview();
});


$('#propertiesdiv').hide();


$('#newformname').focus();

function openFileManager(id)
{
    $.fancybox({
        'type'  : 'iframe',
        'width' : '90%',
        'height' : '90%',
        'href' : 'filemanager/dialog.php?type=2&field_id='+id+'&lang='+ADMINRFM_LANG+'&sort_by=name&descending=0'
      });
}

function responsive_filemanager_callback(field_id)
{
  if (-1!=EmailURL.indexOf('://www.'))
  {
    var emailurl1=EmailURL.replace('://www.','://');
    var emailurl2=EmailURL;
  }
  else
  {
    var emailurl1=EmailURL;
    var emailurl2=EmailURL.replace('://','://www.');
  }
  var emailurl3=EmailURL;
  emailurl3=EmailURL.replace('http://','https://');
  var emailurl4=EmailURL;
  emailurl4=EmailURL.replace('https://','http://');
  var url=$('#'+field_id).val();
  url=url.replace(emailurl1,'');
  url=url.replace(emailurl2,'');
  url=url.replace(emailurl3,'');
  url=url.replace(emailurl4,'');
  url=decodeURIComponent(url);
  $('#'+field_id).val(url);
  $('#'+field_id+'name').text(url);
}

function filltype(filltype,togradient)
{
  var filltype=$('#'+filltype).val();
  if (filltype=="solid")
    $('#'+togradient).hide();
  else
    $('#'+togradient).show();
}

function gradientreverse(fromgradient,togradient)
{
  var from=$('#'+fromgradient).val();
  var to=$('#'+togradient).val();
  $('#'+fromgradient).minicolors('value', to);
  $('#'+togradient).minicolors('value', from);
  updatePreview()
}

function updatepropertiesform()
{
  var registertype='normal'
  if (document.getElementById('newregistertype')!=null)
    registertype=document.getElementById('newregistertype').value
  if (registertype=='normal')
  {
    // If form was saved we should get rid of the Paypal redirect and group etc
    if (document.getElementById('newusergroup').value=="SLNOTPAID")
    {
      document.getElementById('newusergroup').value='CLIENT'
      document.getElementById('newexpiry').value='365'
      document.getElementById('newredirect').value='registerthanks.php'
      document.getElementById('newuseremail').value='newuser.htm'
      document.getElementById('newadminemail').value='newuseradmin.htm'
      $('#newenabled').selectpicker('val', 'Yes');
    }   
    document.getElementById('registernormaldiv').style['display']="block"   
    document.getElementById('paypalproductdiv').style['display']="none"
  }
  if (registertype=='paypal')
  {
    document.getElementById('registernormaldiv').style['display']="none"   
    document.getElementById('paypalproductdiv').style['display']="block"   
  }
  if (registertype=='stripe')
  {
    document.getElementById('registernormaldiv').style['display']="none"   
    document.getElementById('paypalproductdiv').style['display']="none"   
  }
}

function updatePreview()
{
  verifyform();
  previewobj=document.getElementById('formpreview');
  slformobj=document.getElementById('slregform');
  fonttype=document.getElementById('newfonttype').value;
  labelcolor=document.getElementById('newlabelcolor').value;
  labelsize=document.getElementById('newlabelsize').value;
  labelstyle=document.getElementById('newlabelstyle').value;
  inputtextcolor=document.getElementById('newinputtextcolor').value;
  inputtextsize=document.getElementById('newinputtextsize').value;
  inputtextstyle=document.getElementById('newinputtextstyle').value;
  inputbackcolor=document.getElementById('newinputbackcolor').value;
  bordersize=document.getElementById('newbordersize').value;
  bordercolor=document.getElementById('newbordercolor').value;
  borderradius=document.getElementById('newborderradius').value;
  inputpaddingv=document.getElementById('newinputpaddingv').value;
  inputpaddingh=document.getElementById('newinputpaddingh').value;
  rqdfieldlabel=document.getElementById('newrqdfieldlabel').value;
  rqdfieldcolor=document.getElementById('newrqdfieldcolor').value;
  rqdfieldsize=document.getElementById('newrqdfieldsize').value;
  rqdfieldstyle=document.getElementById('newrqdfieldstyle').value;
  messagecolor=document.getElementById('newmessagecolor').value;
  messagesize=document.getElementById('newmessagesize').value;
  messagestyle=document.getElementById('newmessagestyle').value;
  btnlabel=document.getElementById('newbtnlabel').value;
  btnlabelfont=document.getElementById('newbtnlabelfont').value;
  btnlabelstyle=document.getElementById('newbtnlabelstyle').value;
  btnlabelcolor=document.getElementById('newbtnlabelcolor').value;
  btnlabelsize=document.getElementById('newbtnlabelsize').value;
  btncolortype=document.getElementById('newbtncolortype').value;
  btncolorfrom=document.getElementById('newbtncolorfrom').value;
  btnborderstyle=document.getElementById('newbtnborderstyle').value;
  btnbordercolor=document.getElementById('newbtnbordercolor').value;
  btnbordersize=document.getElementById('newbtnbordersize').value;
  btnpaddingv=document.getElementById('newbtnpaddingv').value;
  btnpaddingh=document.getElementById('newbtnpaddingh').value;
  btnradius=document.getElementById('newbtnradius').value;
  formerrormsg=document.getElementById('newformerrormsg').value;
  formerrormsgcolor=document.getElementById('newformerrormsgcolor').value;
  formerrormsgsize=document.getElementById('newformerrormsgsize').value;
  formerrormsgstyle=document.getElementById('newformerrormsgstyle').value;
  maxformwidth=document.getElementById('newmaxformwidth').value;
  previewobj.style['backgroundColor']=document.getElementById('newpreviewbackcolor').value;
  if (document.getElementById('newmaxformwidth').value<200)
    previewobj.style['minWidth']=document.getElementById('newmaxformwidth').value+'px';
  else
    previewobj.style['minWidth']='200px';
  border='solid ';
  if (bordersize==0)
    border='none';
  border+=bordercolor+' '+bordersize+'px';
  if (btncolortype=="solid")
    btncolorto=btncolorfrom;
  else
   btncolorto=document.getElementById('newbtncolorto').value;
  if (inputpaddingv=="0.3em") 
    captchaheight=(inputtextsize*1.75)+(bordersize*2);
  else
  {
    var inputpaddingvnum=inputpaddingv.replace('px','');
    captchaheight=(inputtextsize*1.2)+(inputpaddingvnum*2)+(bordersize*2);    
  }
  captchaheight=captchaheight.toFixed(2);
  if (captchaheight<30) 
    captchaheight=30;
  var captchawidth=4;
  if (inputpaddingh!="0.3em")
  {  
    var inputpaddinghnum=inputpaddingh.replace('px','');
    captchawidth=3+((2*inputpaddinghnum)/inputtextsize);
    captchawidth=captchawidth.toFixed(2);
    if (captchawidth<4) 
      captchawidth=4;
  } 
  var textareaheight=5;
  if (inputpaddingv!="0.3em")
  {  
    var inputpaddingvnum=inputpaddingv.replace('px','');
    textareaheight=5+((2*inputpaddingvnum)/inputtextsize);
    textareaheight=textareaheight.toFixed(2);
  } 
  var htmlcode='';
  htmlcode+='<div style="max-width: '+maxformwidth+'px;';
  if (maxformwidth<200)
    htmlcode+=' min-width: '+maxformwidth+'px;';
  else
    htmlcode+=' min-width: 200px;';
  htmlcode+='"';
  htmlcode+=' id="slregform">\n';
  if (showformerrormessage)
  {
  htmlcode+='<div class="slformmsg" style="color:'+formerrormsgcolor+'; font: '+formerrormsgstyle+' '+formerrormsgsize+'px '+fonttype+';">';
  htmlcode+=formerrormsg;
  htmlcode+='</div>\n';
  }
  var options = new Array();
  var optionvalue = new Array();
  for (k=0;k<numberoffields;k++)
  {
    fldname=sitelokfield_array[k]; 
    if (fldname=='')
      fldname='slform_'+k;
    if (inputtype_array[k]=='text')
    {
      htmlcode+='<div class="sltextfield" style="margin-bottom: '+bottommargin_array[k]+'px;">\n';
      htmlcode+='<span style="display: inline; float:left; margin-left: -4em;" class="glyphicon glyphicon-edit actionicon" rel="tooltip" title="'+ADMINET_EDITFLD+'" onclick="editfield('+k+')">&nbsp;</span>';
      htmlcode+='<span style="display: inline; float:left; margin-left: -2.5em;" class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINET_DELFLD+'" onclick="deletefield('+k+')"></span>';
      htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" for="inputid_'+k+'">'+labeltext_array[k];
      if ((rqdfieldlabel!='') && (validation_array[k]!='notrequired') && (showrequired_array[k]!=0))
        htmlcode+='<em style="color:'+rqdfieldcolor+'; font: '+rqdfieldstyle+' '+rqdfieldsize+'px '+fonttype+'; vertical-align: top;">'+rqdfieldlabel+'</em>';
      htmlcode+='</label>\n';
      htmlcode+='<input id="inputid_'+k+'" style="color:'+inputtextcolor+'; font: '+inputtextstyle+' '+inputtextsize+'px '+fonttype+'; background-color: '+inputbackcolor+'; border: '+border+'; border-radius: '+borderradius+'px; padding: '+inputpaddingv+' '+inputpaddingh+'; width: '+fieldwidth_array[k]+'%" type="text" name="'+fldname+'" id="'+fldname+'"';
      if (k==currenteditfield)
        htmlcode+=' value="example data"';
      if (placetext_array[k]!='')
        htmlcode+=' placeholder="'+placetext_array[k]+'"' ;       
      htmlcode+='>\n';
      if ((k==currenteditfield) && (errormsg_array[k]!='') && (validation_array[k]!='notrequired'))
        htmlcode+='<div style="color:'+messagecolor+'; font: '+messagestyle+' '+messagesize+'px '+fonttype+';" id="slformmsg_'+k+'" class="slmsg">'+errormsg_array[k]+'</div>';
      htmlcode+='</div>\n';
      htmlcode+='\n';
    }
    if (inputtype_array[k]=='password')
    {
      htmlcode+='<div class="sltextfield" style="margin-bottom: '+bottommargin_array[k]+'px;">\n'
      htmlcode+='<span style="display: inline; float:left; margin-left: -4em;" class="glyphicon glyphicon-edit actionicon" rel="tooltip" title="'+ADMINET_EDITFLD+'" onclick="editfield('+k+')">&nbsp;</span>';
      htmlcode+='<span style="display: inline; float:left; margin-left: -2.5em;" class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINET_DELFLD+'" onclick="deletefield('+k+')"></span>';
      htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" for="inputid_'+k+'">'+labeltext_array[k]
      if ((rqdfieldlabel!='') && (validation_array[k]!='notrequired') && (showrequired_array[k]!=0))
        htmlcode+='<em style="color:'+rqdfieldcolor+'; font: '+rqdfieldstyle+' '+rqdfieldsize+'px '+fonttype+'; vertical-align: top;">'+rqdfieldlabel+'</em>'
      htmlcode+='</label>\n'
      htmlcode+='<input id="inputid_'+k+'" style="color:'+inputtextcolor+'; font: '+inputtextstyle+' '+inputtextsize+'px '+fonttype+'; background-color: '+inputbackcolor+'; border: '+border+'; border-radius: '+borderradius+'px; padding: '+inputpaddingv+' '+inputpaddingh+'; width: '+fieldwidth_array[k]+'%" type="password" name="'+fldname+'" id="'+fldname+'"'
      if (k==currenteditfield)
        htmlcode+=' value="example data"'
      if (placetext_array[k]!='')
        htmlcode+=' placeholder="'+placetext_array[k]+'"'        
      htmlcode+='>\n'
      if ((k==currenteditfield) && (errormsg_array[k]!='') && (validation_array[k]!='notrequired'))
        htmlcode+='<div style="color:'+messagecolor+'; font: '+messagestyle+' '+messagesize+'px '+fonttype+';" id="slformmsg_'+k+'" class="slmsg">'+errormsg_array[k]+'</div>'
      htmlcode+='</div>\n'
      htmlcode+='\n'
    }    
    if (inputtype_array[k]=='checkbox')
    {
      htmlcode+='<div class="slcbfield" style="margin-bottom: '+bottommargin_array[k]+'px;">\n'
      htmlcode+='<span style="display: inline; float:left; margin-left: -4em;" class="glyphicon glyphicon-edit actionicon" rel="tooltip" title="'+ADMINET_EDITFLD+'" onclick="editfield('+k+')">&nbsp;</span>';
      htmlcode+='<span style="display: inline; float:left; margin-left: -2.5em;" class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINET_DELFLD+'" onclick="deletefield('+k+')"></span>';
      htmlcode+='<input id="inputid_'+k+'" type="checkbox" style="color:'+inputtextcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+'; background-color: '+inputbackcolor+'; border: '+border+'; border-radius: '+borderradius+'px;" name="'+fldname+'" id="'+fldname+'" value="'+value_array[k]+'"'
      if (checked_array[k]=='1')
        htmlcode+=' checked="checked"'    
      htmlcode+='>\n'
      htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" for="inputid_'+k+'">'+labeltext_array[k]
      if ((rqdfieldlabel!='') && (validation_array[k]!='notrequired') && (showrequired_array[k]!=0))
        htmlcode+='<em style="color:'+rqdfieldcolor+'; font: '+rqdfieldstyle+' '+rqdfieldsize+'px '+fonttype+'; vertical-align: top;">'+rqdfieldlabel+'</em>'
      htmlcode+='</label>\n'
      if ((k==currenteditfield) && (errormsg_array[k]!='') && (validation_array[k]!='notrequired'))
        htmlcode+='<div style="color:'+messagecolor+'; font: '+messagestyle+' '+messagesize+'px '+fonttype+';" id="slformmsg_'+k+'" class="slmsg">'+errormsg_array[k]+'</div>'
      htmlcode+='</div>\n'
      htmlcode+='\n'
    }
    if ((inputtype_array[k]=='dropdown') || (inputtype_array[k]=='usergroup'))
    {
      htmlcode+='<div class="slselectfield" style="margin-bottom: '+bottommargin_array[k]+'px;">\n'
      htmlcode+='<span style="display: inline; float:left; margin-left: -4em;" class="glyphicon glyphicon-edit actionicon" rel="tooltip" title="'+ADMINET_EDITFLD+'" onclick="editfield('+k+')">&nbsp;</span>';
      htmlcode+='<span style="display: inline; float:left; margin-left: -2.5em;" class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINET_DELFLD+'" onclick="deletefield('+k+')"></span>';
      htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" for="inputid_'+k+'">'+labeltext_array[k]
      if ((rqdfieldlabel!='') && (validation_array[k]!='notrequired') && (showrequired_array[k]!=0))
        htmlcode+='<em style="color:'+rqdfieldcolor+'; font: '+rqdfieldstyle+' '+rqdfieldsize+'px '+fonttype+'; vertical-align: top;">'+rqdfieldlabel+'</em>'
      htmlcode+='</label>\n'
      htmlcode+='<select id="inputid_'+k+'" style="color:'+inputtextcolor+'; font: '+inputtextstyle+' '+inputtextsize+'px '+fonttype+'; background-color: '+inputbackcolor+'; border: '+border+'; border-radius: '+borderradius+'px; width: '+fieldwidth_array[k]+'%" type="text" name="'+fldname+'" id="'+fldname+'">\n'
      if ((k==currenteditfield) && (errormsg_array[k]!='') && (validation_array[k]!='notrequired'))
        htmlcode+='<div style="color:'+messagecolor+'; font: '+messagestyle+' '+messagesize+'px '+fonttype+';" id="slformmsg_'+k+'" class="slmsg">'+errormsg_array[k]+'</div>'
      options=value_array[k].split("\n")
      for (j=0;j<options.length;j++)
      {
        options[j]=options[j].trim()
        if(options[j]!='')
        {
          // See if separate value
          optionvalue=options[j].split(',')
          optionvalue[0]=optionvalue[0].trim()
          if ((j==0) && (validation_array[k]!='notrequired'))
          {
            htmlcode+='<option value="">'+optionvalue[0]+'</option>\n'
            continue
          }
          if (optionvalue[1])
          {
           optionvalue[1]=optionvalue[1].trim()
           if (optionvalue[1]!='')
             htmlcode+='<option value="'+optionvalue[1]+'">'+optionvalue[0]+'</option>\n'
           continue   
          }    
          htmlcode+='<option value="'+optionvalue[0]+'">'+optionvalue[0]+'</option>\n'            
        }  
      }
      htmlcode+='</select>\n'
      if ((k==currenteditfield) && (errormsg_array[k]!='') && (validation_array[k]!='notrequired'))
        htmlcode+='<div style="color:'+messagecolor+'; font: '+messagestyle+' '+messagesize+'px '+fonttype+';" id="slformmsg_'+k+'" class="slmsg">'+errormsg_array[k]+'</div>'
      htmlcode+='</div>\n'
      htmlcode+='\n'
    }
    if (inputtype_array[k]=='radio')
    {
      htmlcode+='<div class="slradiofield" style="margin-bottom: '+bottommargin_array[k]+'px;">\n'
      htmlcode+='<span style="display: inline; float:left; margin-left: -4em;" class="glyphicon glyphicon-edit actionicon" rel="tooltip" title="'+ADMINET_EDITFLD+'" onclick="editfield('+k+')">&nbsp;</span>';
      htmlcode+='<span style="display: inline; float:left; margin-left: -2.5em;" class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINET_DELFLD+'" onclick="deletefield('+k+')"></span>';
      htmlcode+='<input id="inputid_'+k+'" type="radio" style="color:'+inputtextcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+'; background-color: '+inputbackcolor+'; " name="'+fldname+'" id="'+fldname+'" value="'+value_array[k]+'"'
      if (checked_array[k]=='1')
        htmlcode+=' checked="checked"'    
      htmlcode+='>\n'
      htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" for="inputid_'+k+'">'+labeltext_array[k]
      if ((rqdfieldlabel!='') && (validation_array[k]!='notrequired') && (showrequired_array[k]!=0))
        htmlcode+='<em style="color:'+rqdfieldcolor+'; font: '+rqdfieldstyle+' '+rqdfieldsize+'px '+fonttype+'; vertical-align: top;">'+rqdfieldlabel+'</em>'
      htmlcode+='</label>\n'
      if ((k==currenteditfield) && (errormsg_array[k]!='') && (validation_array[k]!='notrequired'))
        htmlcode+='<div style="color:'+messagecolor+'; font: '+messagestyle+' '+messagesize+'px '+fonttype+';" id="slformmsg_'+k+'" class="slmsg">'+errormsg_array[k]+'</div>'
      htmlcode+='</div>\n'
      htmlcode+='\n'
    }
    if (inputtype_array[k]=='label')
    {
      htmlcode+='<div class="sllabelfield" style="margin-bottom: '+bottommargin_array[k]+'px;">\n'
      htmlcode+='<span style="display: inline; float:left; margin-left: -4em;" class="glyphicon glyphicon-edit actionicon" rel="tooltip" title="'+ADMINET_EDITFLD+'" onclick="editfield('+k+')">&nbsp;</span>';
      htmlcode+='<span style="display: inline; float:left; margin-left: -2.5em;" class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINET_DELFLD+'" onclick="deletefield('+k+')"></span>';
      htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" >'+labeltext_array[k]
      if ((rqdfieldlabel!='') && (showrequired_array[k]!=0))
        htmlcode+='<em style="color:'+rqdfieldcolor+'; font: '+rqdfieldstyle+' '+rqdfieldsize+'px '+fonttype+'; vertical-align: top;">'+rqdfieldlabel+'</em>'
      htmlcode+='</label>\n'
      htmlcode+='</div>\n'
      htmlcode+='\n'
    }
    if (inputtype_array[k]=='captcha')
    {
      htmlcode+='<div class="slcaptchafield" style="margin-bottom: '+bottommargin_array[k]+'px;">\n'
      htmlcode+='<span style="display: inline; float:left; margin-left: -4em;" class="glyphicon glyphicon-edit actionicon" rel="tooltip" title="'+ADMINET_EDITFLD+'" onclick="editfield('+k+')">&nbsp;</span>';
      htmlcode+='<span style="display: inline; float:left; margin-left: -2.5em;" class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINET_DELFLD+'" onclick="deletefield('+k+')"></span>';
      htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" for="inputid_'+k+'">'+labeltext_array[k]
      if ((rqdfieldlabel!='') && (validation_array[k]!='notrequired') && (showrequired_array[k]!=0))
        htmlcode+='<em style="color:'+rqdfieldcolor+'; font: '+rqdfieldstyle+' '+rqdfieldsize+'px '+fonttype+'; vertical-align: top;">'+rqdfieldlabel+'</em>'
      htmlcode+='</label>\n'
      htmlcode+='<input id="inputid_'+k+'" maxlength=5 style="color:'+inputtextcolor+'; font: '+inputtextstyle+' '+inputtextsize+'px '+fonttype+'; background-color: '+inputbackcolor+'; border: '+border+'; border-radius: '+borderradius+'px; padding: '+inputpaddingv+' '+inputpaddingh+'; width: '+captchawidth+'em" type="text" name="'+fldname+'" id="'+fldname+'"'
      if (placetext_array[k]!='')
        htmlcode+=' placeholder="'+placetext_array[k]+'"'        
      htmlcode+='>\n'
      htmlcode+='&nbsp;<img src="turingimage.php" height="'+captchaheight+'">'
      if ((k==currenteditfield) && (errormsg_array[k]!='') && (validation_array[k]!='notrequired'))
        htmlcode+='<div style="color:'+messagecolor+'; font: '+messagestyle+' '+messagesize+'px '+fonttype+';" id="slformmsg_'+k+'" class="slmsg">'+errormsg_array[k]+'</div>'
      htmlcode+='</div>\n'
      htmlcode+='\n'
    }
    if (inputtype_array[k]=='file')
    {
      fileinputsize=inputtextsize
      if (inputtextsize>17)
        fileinputsize=17
      htmlcode+='<div class="slfilefield" style="margin-bottom: '+bottommargin_array[k]+'px;">\n'
      htmlcode+='<span style="display: inline; float:left; margin-left: -4em;" class="glyphicon glyphicon-edit actionicon" rel="tooltip" title="'+ADMINET_EDITFLD+'" onclick="editfield('+k+')">&nbsp;</span>';
      htmlcode+='<span style="display: inline; float:left; margin-left: -2.5em;" class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINET_DELFLD+'" onclick="deletefield('+k+')"></span>';
      htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" for="inputid_'+k+'">'+labeltext_array[k]
      if ((rqdfieldlabel!='') && (validation_array[k]!='notrequired') && (showrequired_array[k]!=0))
        htmlcode+='<em style="color:'+rqdfieldcolor+'; font: '+rqdfieldstyle+' '+rqdfieldsize+'px '+fonttype+'; vertical-align: top;">'+rqdfieldlabel+'</em>'
      htmlcode+='</label>\n'
      htmlcode+='<input id="inputid_'+k+'" style="color:'+inputtextcolor+'; font: '+inputtextstyle+' '+fileinputsize+'px '+fonttype+'; background-color: '+inputbackcolor+';  border: none;" type="file" name="'+fldname+'" id="'+fldname+'"'
      htmlcode+='>\n'
      if ((k==currenteditfield) && (errormsg_array[k]!='') && (validation_array[k]!='notrequired'))
        htmlcode+='<div style="color:'+messagecolor+'; font: '+messagestyle+' '+messagesize+'px '+fonttype+';" id="slformmsg_'+k+'" class="slmsg">'+errormsg_array[k]+'</div>'
      htmlcode+='</div>\n'
      htmlcode+='\n'
    }
    if (inputtype_array[k]=='textarea')
    {
      htmlcode+='<div class="sltextareafield" style="margin-bottom: '+bottommargin_array[k]+'px;">\n'
      htmlcode+='<span style="display: inline; float:left; margin-left: -4em;" class="glyphicon glyphicon-edit actionicon" rel="tooltip" title="'+ADMINET_EDITFLD+'" onclick="editfield('+k+')">&nbsp;</span>';
      htmlcode+='<span style="display: inline; float:left; margin-left: -2.5em;" class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINET_DELFLD+'" onclick="deletefield('+k+')"></span>';
      htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" for="inputid_'+k+'">'+labeltext_array[k]
      if ((rqdfieldlabel!='') && (validation_array[k]!='notrequired') && (showrequired_array[k]!=0))
        htmlcode+='<em style="color:'+rqdfieldcolor+'; font: '+rqdfieldstyle+' '+rqdfieldsize+'px '+fonttype+'; vertical-align: top;">'+rqdfieldlabel+'</em>'
      htmlcode+='</label>\n'
      htmlcode+='<textarea id="inputid_'+k+'" style="color:'+inputtextcolor+'; font: '+inputtextstyle+' '+inputtextsize+'px '+fonttype+'; background-color: '+inputbackcolor+'; border: '+border+'; border-radius: '+borderradius+'px; padding: '+inputpaddingv+' '+inputpaddingh+'; width: '+fieldwidth_array[k]+'%; height: '+textareaheight+'em" type="text" name="'+fldname+'" id="'+fldname+'"'
      if (k==currenteditfield)
        htmlcode+=' value="example data"'
      if (placetext_array[k]!='')
        htmlcode+=' placeholder="'+placetext_array[k]+'"'        
      htmlcode+='></textarea>\n'
      if ((k==currenteditfield) && (errormsg_array[k]!='') && (validation_array[k]!='notrequired'))
        htmlcode+='<div style="color:'+messagecolor+'; font: '+messagestyle+' '+messagesize+'px '+fonttype+';" id="slformmsg_'+k+'" class="slmsg">'+errormsg_array[k]+'</div>'
      htmlcode+='</div>\n'
      htmlcode+='\n'
    }
  }
  htmlcode+='\n'
  htmlcode+='<input style="color: '+btnlabelcolor+'; font: '+btnlabelstyle+' '+btnlabelsize+'px '+btnlabelfont+'; background:linear-gradient(to bottom, '+btncolorfrom+' 5%, '+btncolorto+' 100%); border: '+btnborderstyle+' '+btnbordercolor+' '+btnbordersize+'px; border-radius:'+btnradius+'px; padding: '+btnpaddingv+'px '+btnpaddingh+'px ;" type="button" class="myButton" id="myButton" value="'+btnlabel+'" title="Register" style="cursor: pointer;" onclick="currenteditfield=-1; editfield(-1); updatePreview();">\n'
  htmlcode+='<br><br><span style="display: inline; float:left; margin-left: 0px; "><i class="fa fa-plus actionicon" rel="tooltip" title="'+ADMINET_ADDFLD+'" onclick="addfield();"></i></span>\n'
  htmlcode+='</div>\n'

  previewobj.innerHTML=htmlcode
}

function verifyform()
{
  var captchaenabled=TuringRegister;
  var problems=''
  if (sitelokfield_array.indexOf("name")==-1)
    problems+='<li>'+ADMINET_REGPROB1+'</li>\n'
  if (sitelokfield_array.indexOf("email")==-1)
    problems+='<li>'+ADMINET_REGPROB2+'</li>\n'
  if ((sitelokfield_array.indexOf("captcha")==-1) && (captchaenabled==1))
    problems+='<li>'+ADMINET_CAPTCHAPROB1+'</li>\n'
  if ((sitelokfield_array.indexOf("captcha")!=-1) && (captchaenabled==0))
    problems+='<li>'+ADMINET_CAPTCHAPROB2+'</li>\n'
  if (problems=='')
    $('#formissues').hide();
  else
  {
    problems="<ul>"+problems+"</ul>\n"  
    $('#formissues').html(problems);
    $('#formissues').show();
  }
}

function deletefield(fieldno)
{
  bootbox.confirm(ADMINMSG_RUSURE, function(result) {
    if (result)
    {
      sitelokfield_array.splice(fieldno,1)
      inputtype_array.splice(fieldno,1)
      labeltext_array.splice(fieldno,1)
      placetext_array.splice(fieldno,1)
      value_array.splice(fieldno,1)
      checked_array.splice(fieldno,1)
      validation_array.splice(fieldno,1)
      showrequired_array.splice(fieldno,1)
      errormsg_array.splice(fieldno,1)
      fieldwidth_array.splice(fieldno,1)
      bottommargin_array.splice(fieldno,1)
      numberoffields--;
      currenteditfield=-1
      editfield(-1)
      updatePreview()
    }  
  });
}

function updateposition(newposition)
{
  if (newposition==currenteditfield)
    return
  res=sitelokfield_array.splice(currenteditfield,1)
  sitelokfield_array.splice(newposition,0,res[0])
  res=inputtype_array.splice(currenteditfield,1)
  inputtype_array.splice(newposition,0,res[0])
  res=labeltext_array.splice(currenteditfield,1)
  labeltext_array.splice(newposition,0,res[0])
  res=placetext_array.splice(currenteditfield,1)
  placetext_array.splice(newposition,0,res[0])
  res=value_array.splice(currenteditfield,1)
  value_array.splice(newposition,0,res[0])
  res=checked_array.splice(currenteditfield,1)
  checked_array.splice(newposition,0,res[0])
  res=validation_array.splice(currenteditfield,1)
  validation_array.splice(newposition,0,res[0])
  res=showrequired_array.splice(currenteditfield,1)
  showrequired_array.splice(newposition,0,res[0])
  res=errormsg_array.splice(currenteditfield,1)
  errormsg_array.splice(newposition,0,res[0])
  res=fieldwidth_array.splice(currenteditfield,1)
  fieldwidth_array.splice(newposition,0,res[0])
  res=bottommargin_array.splice(currenteditfield,1)
  bottommargin_array.splice(newposition,0,res[0])
  currenteditfield=newposition
}

function copyinputvalidation()
{
  // If sitelok field changed then if used before then ensure same validation settings
  var fieldtype=sitelokfield_array[currenteditfield]
  for (k=0;k<numberoffields;k++)
  {
    if (k==currenteditfield)
      continue
    if (sitelokfield_array[k]==fieldtype)
    {
      inputtype_array[currenteditfield]=inputtype_array[k]
      validation_array[currenteditfield]=validation_array[k]
      errormsg_array[currenteditfield]=errormsg_array[k]
      showrequired_array[currenteditfield]=showrequired_array[k]
      setfieldproperties(currenteditfield)
      return      
    }
  }
}

function setinputvalidation()
{
  // If validation fields changed then if field used before ensure same validation settings
  var fieldtype=sitelokfield_array[currenteditfield];
  for (k=0;k<numberoffields;k++)
  {
    if (k==currenteditfield)
      continue;
    if (sitelokfield_array[k]==fieldtype)
    {
      validation_array[k]=validation_array[currenteditfield];
      errormsg_array[k]=errormsg_array[currenteditfield];
    }
  }
}

var formFixed=true;

function toggleFixed()
{
  if ($('#previewbox').css('position')=='fixed')
  {
    $('#previewbox').css('position','static');
    $('#previewbox').css('width','100%');
    $('#toggleicon').css('opacity',"0.25");
    formFixed=false;
  }
  else
  {
    $('#previewbox').css('position','fixed');
    $('#previewbox').css('width','40%');
    $('#toggleicon').css('opacity',"0.6");
    formFixed=true;
  } 
}

$(window).on('resize', function(){
  var width=document.documentElement.clientWidth || window.innerWidth || document.getElementsByTagName('body')[0].clientWidth;
  if (width>=1200)
  {
    if (formFixed)
    {
      $('#previewbox').css('position','fixed');
      $('#previewbox').css('width','40%');
    }
    else
    {
      $('#previewbox').css('position','static');
      $('#previewbox').css('width','100%');
    }     
  }
  else
  {
    $('#previewbox').css('position','static');
    $('#previewbox').css('width','100%');    
  }
});

// Validation and saving for form
$('#registerform').submit(function(event) {
        var data={success:true,message:'',errors:{}};
        //Remove form message
        $('#resultregisterform').html('');
       
        var savearray = new Array()  
        savearray[0]=document.getElementById('newformname').value
        var registertype="normal"
        // If Paypal option then check Paypal product selected
        if (document.getElementById('newregistertype')!=null)
          registertype=document.getElementById('newregistertype').value
         
        if (registertype=="normal")
        {
          savearray[1]=document.getElementById('newusergroup').value        
          savearray[2]=document.getElementById('newexpiry').value
          savearray[2]=savearray[2].trim()          
          savearray[3]=document.getElementById('newredirect').value
          if (!$('#senduseremail').iCheck('update')[0].checked)
          {
            document.getElementById('newuseremail').value="";
            $('#newuseremailname').text(ADMINMSG_NOTEMPLATE);
          }
          if (!$('#sendadminemail').iCheck('update')[0].checked)
          {
            document.getElementById('newadminemail').value="";        
            $('#newadminemailname').text(ADMINMSG_NOTEMPLATE);
          }
          savearray[4]=document.getElementById('newuseremail').value
          savearray[5]=document.getElementById('newadminemail').value
          savearray[6]=document.getElementById('newenabled').value
        }
        if (registertype=="paypal")
        {
/*
          document.getElementById('newpaypalproductproblem').style['display']="none"   
          if (document.getElementById('newpaypalproduct').value=='')
          {
            document.getElementById('newpaypalproductproblem').innerHTML="Please select a Paypal product"
            document.getElementById('newpaypalproductproblem').style['display']="block"
            if (!formproblem)   
              document.getElementById('newpaypalproduct').focus()
            formproblem=true;
          }
*/          
          savearray[1]="SLNOTPAID"
          savearray[2]="1"
          var product=document.getElementById('newpaypalproduct').value
          var pos=product.indexOf(",")
          savearray[3]=SitelokLocationURLPath+"pay_paypal/registerpaypal.php?oc=!!!ordercustom!!!&id="+encodeURIComponent(product.substr(0,pos))+"&cy="+product.substr(pos+1)
          savearray[4]=""
          savearray[5]=""
          savearray[6]="Yes"        
        }
        if (registertype=="stripe")
        {
          savearray[1]="SLNOTPAID"
          savearray[2]="1"
          savearray[3]=SitelokLocationURLPath+"pay_stripe/stripecharge.php?username=!!!username!!!&password=!!!passwordhash!!!";
          savearray[4]=""
          savearray[5]=""
          savearray[6]="Yes"        
        }
        
        jsonString = JSON.stringify(savearray);
        document.getElementById('formpropertiesfield').value=jsonString 
        // Form style
        savearray.length=0
        savearray[0]=document.getElementById('newfonttype').value
        savearray[1]=document.getElementById('newlabelcolor').value
        savearray[2]=document.getElementById('newlabelsize').value
        savearray[3]=document.getElementById('newlabelstyle').value
        savearray[4]=document.getElementById('newinputtextcolor').value
        savearray[5]=document.getElementById('newinputtextsize').value
        savearray[6]=document.getElementById('newinputtextstyle').value
        savearray[7]=document.getElementById('newinputbackcolor').value
        savearray[8]=document.getElementById('newbordersize').value
        savearray[9]=document.getElementById('newbordercolor').value
        savearray[10]=document.getElementById('newborderradius').value
        savearray[11]=document.getElementById('newrqdfieldlabel').value
        savearray[12]=document.getElementById('newrqdfieldcolor').value
        savearray[13]=document.getElementById('newrqdfieldsize').value
        savearray[14]=document.getElementById('newrqdfieldstyle').value
        savearray[15]=document.getElementById('newmessagecolor').value
        savearray[16]=document.getElementById('newmessagesize').value
        savearray[17]=document.getElementById('newmessagestyle').value
        savearray[18]=document.getElementById('newbtnlabel').value
        savearray[19]=document.getElementById('newbtnlabelcolor').value
        savearray[20]=document.getElementById('newbtnlabelsize').value
        savearray[21]=document.getElementById('newbtncolortype').value
        savearray[22]=document.getElementById('newbtncolorfrom').value
        savearray[23]=document.getElementById('newbtncolorto').value
        savearray[24]=document.getElementById('newbtnradius').value
        savearray[25]=document.getElementById('newformerrormsg').value
        savearray[26]=document.getElementById('newformerrormsgcolor').value
        savearray[27]=document.getElementById('newformerrormsgsize').value
        savearray[28]=document.getElementById('newformerrormsgstyle').value
        savearray[29]=document.getElementById('newmaxformwidth').value
        savearray[30]=document.getElementById('newpreviewbackcolor').value
        savearray[31]=document.getElementById('newbtnlabelfont').value
        savearray[32]=document.getElementById('newbtnlabelstyle').value
        savearray[33]=document.getElementById('newbtnbordercolor').value
        savearray[34]=document.getElementById('newbtnbordersize').value
        savearray[35]=document.getElementById('newbtnborderstyle').value
        savearray[36]=document.getElementById('newinputpaddingv').value
        savearray[37]=document.getElementById('newinputpaddingh').value
        savearray[38]=document.getElementById('newbtnpaddingv').value
        savearray[39]=document.getElementById('newbtnpaddingh').value
        jsonString = JSON.stringify(savearray);
        document.getElementById('formstylesfield').value=jsonString 
        // Form fields
        document.getElementById('numfields').value=numberoffields   
        for (k=0;k<numberoffields;k++)
        {
          savearray.length=0
          savearray[0]=sitelokfield_array[k]
          savearray[1]=inputtype_array[k]
          savearray[2]=labeltext_array[k]
          savearray[3]=placetext_array[k]
          savearray[4]=value_array[k]
          savearray[5]=checked_array[k]
          savearray[6]=validation_array[k]
          savearray[7]=showrequired_array[k]
          savearray[8]=errormsg_array[k]
          savearray[9]=fieldwidth_array[k]
          savearray[10]=bottommargin_array[k]
          jsonString = JSON.stringify(savearray);
          if (!document.getElementById('formfield'+k))
          {
            newField = document.createElement("input");
            newField.setAttribute("id", "formfield"+k);
            newField.setAttribute("name", "formfield"+k);
            newField.setAttribute("type", "hidden");
            newField.setAttribute("value", jsonString);       
            theForm = document.getElementById("registerform");
            theForm.appendChild(newField);            
          }
          else
            document.getElementById('formfield'+k).value=jsonString;
        }
        // Now call PHP to save form
        var label=showButtonBusy('submit');
        var formData = $('#registerform').serialize();
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'adminsaveregisterform.php', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
            // using the done promise callback
            .done(function(data) {
              sessionExpiredReported(data,true);
              // Show validation results from PHP in form
              showValidation(data,'resultregisterform');
              hideButtonBusy('submit',label);
              if (data.success)
              {
                // If we were duplicating form we need to change actid to editform and set new actid
                // Alos if we were adding form we should switch form to edit
                var act=$('#act').val();
                if ((act=="duplform") || (act=="addform"))
                {
                  $('#act').val('editform');
                  $('#actid').val(data.formid);
                }
              }
            })

// using the fail promise callback
    .fail(function(data) {
    hideButtonBusy('submit',label);  
    $('#resultregisterform').html('<div id="resultregisterformmessage" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
//        console.log(data);
    });


        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });






