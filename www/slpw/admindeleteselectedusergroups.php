<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value  
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError(ADMINMSG_CSRFFAILED);
    exit;      
  }
  $groupids=$_POST['groupids'];
  $groupidsarray=explode(",",$groupids);
  $groupnames=$_POST['groupnames'];
  $groupnamesarray=explode(",",$groupnames);
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError(ADMINMSG_MYSQLERROR);
    exit;
  }
  if (count($groupidsarray)==0)  
  {
    returnError(ADMINMU_NODELETE);
    exit;
  }
  // See if ADMIN or ALL have been selected
  if ((in_array("1", $groupidsarray)) || (in_array("1", $groupidsarray)))
  {  
    returnError(ADMINMU_CANTDELETE);
    exit;
  }
  if ($DemoMode)
  {
    returnSuccess();
    exit;
  }    
  $_SESSION['ses_ConfigReload']="reload";
  for ($k=0;$k<count($groupidsarray);$k++)
  {
    $query="DELETE FROM usergroups WHERE id=".sl_quote_smart($groupidsarray[$k]);
      $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
      continue;
    // Event point
    // Call event handler and plugins
    $paramdata=array();
    $paramdata['oldname']="";
    $paramdata['name']=$groupnamesarray[$k];
    $paramdata['groupid']=$groupidsarray[$k];
    $paramdata['description']="";
    $paramdata['loginaction']="";
    $paramdata['loginvalue']="";
    if (function_exists("sl_onDeleteGroup"))
      sl_onDeleteGroup($paramdata);
    for ($p=0;$p<$slnumplugins;$p++)
    {
      if (function_exists($slplugin_event_onDeleteGroup[$p]))
        call_user_func($slplugin_event_onDeleteGroup[$p],$slpluginid[$p],$paramdata);
    }
  }
  returnSuccess();
  exit;


  function returnSuccess()
  {
    $data['success'] = true;
    $data['message'] = "";
    echo json_encode($data);
    exit;
  }

  function returnError($msg)
  {
    $data['success'] = false;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }
