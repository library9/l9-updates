<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $startpage="index.php";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  include("admincommonfunctions.php");
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    die(ADMINMSG_MYSQLERROR);
    exit;
  }
  // Get configuration data
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_adminconfig LIMIT 1");
  if ($mysql_result!=false)
  {
    $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
    if ($row!=false)
    {
      $emaildedupe=$row["emaildedupe"];
      $emaildeselect=$row["emaildeselect"];
    }
  }  
  $act=$_GET['act'];
  if ($act=="emailuser")
  {
    $userid=$_GET['id'];
    $username=rawurldecode($_GET['un']);
  }
  $title="";
  switch ($act)
  {
    case "managetemplates":
      $title=ADMINMENU_EMAILTEMPLATES;
      break;
    case "emailuser":
      $title=ADMINEMAILUSER_SENDEMAILTO.$username;
      $userid=$_GET['id'];
      $username=rawurldecode($_GET['un']);
      break;
    case "emailselected":
      $title=ADMINEMAILUSER_EMAILSELECTED;
      break;
  }   
?>
<!DOCTYPE html>
<html>
<head>
<?php
$pagename="emailuser";
include("adminhead.php");
?>
<title><?php echo $title; ?></title>
<link rel="stylesheet" href="emailuser.css">
<link rel="stylesheet" type="text/css" href="fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />
</head>
<?php include("adminthemeheader.php"); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1><i class="fa fa-envelope-o"></i>&nbsp;<?php echo $title; ?></h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><?php echo $title; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>

          <div class="row">
          <form method="post" class="form-horizontal" id="emailform">
          <input type="hidden" name="filename" id="filename" value="untitled.html">
          <input type="hidden" name="act" id="act" value="<?php echo $act; ?>">

            <div class="col-xs-12">

              <div class="box box-default">
                <div class="box-body">

                  <div class="btn-group">

                    <div class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <?php echo ADMINBUTTON_NEW; ?>
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu">
                        <li><a href="#" onclick="event.preventDefault();newTemplate('html');"><?php echo ADMINEMAILUSER_HTMLFORMAT; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();newTemplate('text');"><?php echo ADMINEMAILUSER_TEXTFORMAT; ?></a></li>
                      </ul>
                    </div>

                    <button class="btn btn-default" type="button" onclick="loadClicked();" id="loadtemplate"><?php echo ADMINBUTTON_LOAD; ?></button>
                    <button class="btn btn-default" type="button" onclick="saveEmailClicked();" id="savetemplate"><?php echo ADMINBUTTON_SAVE; ?></button>
                    <button class="btn btn-default" type="button" onclick="manageClicked();" id="managetemplate"><?php echo ADMINBUTTON_MNG; ?></button>




                  </div>

                  <div id="resultsendemail1"></div>


                  <div id="filenamelabel">untitled.html</div>



                </div>
              </div>


              <div class="box box-default">
                <div class="box-body">



                  <div class="form-group" id="subjectgroup">
                  <label class="col-xs-12" for="subject" id="labelsubject"><?php echo ADMINEMAILUSER_SUBJECT; ?></label>
                  <div class="col-xs-12" id="subjectdiv">
                  <input type="text" class="form-control" name="subject" id="subject" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  </div>




                  <div class="form-group" id="modegroup">
                      <div class="col-xs-12">
                        <div class="checkbox" id="editormodediv">
                          <label>
                            <input type="radio" id="visualeditor" name="editormode" value="visual" checked ><label for="visualeditor">&nbsp;<?php echo ADMINEMAILUSER_VISUALEDITOR; ?></label>&nbsp;&nbsp;
                            <input type="radio" id="sourceeditor" name="editormode" value="source"><label for="sourceeditor">&nbsp;<?php echo ADMINEMAILUSER_SOURCEEDITOR; ?></label>
                          </label>
                        </div>
                      </div>
                  </div>


                  <div class="form-group">
                  <div class="col-xs-12">

                    <div class="btn-group" role="group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
                        <?php echo ADMINEMAILUSER_INSVARS; ?>
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu scrollable123-menu" role="menu">
                        <li class="dropdown-header"><?php echo ADMINEMAILUSER_USERDATA; ?></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!username!!!');"><?php echo ADMINFIELD_USERNAME; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!password!!!');"><?php echo ADMINFIELD_PASSWORD; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!passwordifknown!!!');"><?php echo ADMINEMAILUSER_PASSIFKNOWN; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!passwordclue!!!');"><?php echo ADMINEMAILUSER_PASSCLUE; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!passwordhash!!!');"><?php echo ADMINEMAILUSER_PASSHASH; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!name!!!');"><?php echo ADMINEMAILUSER_FULLNAME; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!firstname!!!');"><?php echo ADMINEMAILUSER_FIRSTNAME; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!lastname!!!');"><?php echo ADMINEMAILUSER_LASTNAME; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!email!!!');"><?php echo ADMINFIELD_EMAIL; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!ip!!!');"><?php echo ADMINEMAILUSER_IP; ?></a></li>
                        <li class="dropdown-header"><?php echo ADMINGENERAL_CUSTOMFIELDS; ?></li>
<?php
for ($k=1;$k<=50;$k++)
{
  $titlevar="CustomTitle".$k;
  if ($$titlevar!="")
  {
    ?>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!custom<?php echo $k; ?>!!!');"><?php echo htmlentities($$titlevar,ENT_QUOTES,strtoupper($MetaCharSet)); ?></a></li>
    <?php                        
  }
}
?>
                        <li class="dropdown-header"><?php echo ADMINMENU_USERGROUPS; ?></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!groups!!!');"><?php echo ADMINMENU_USERGROUPS; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!eachgroupstart!!!');"><?php echo ADMINEMAILUSER_STARTGROUPLOOP; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!groupname!!!');"><?php echo ADMINEMAILUSER_GROUPNAME; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!groupdesc!!!');"><?php echo ADMINEMAILUSER_GROUPDESC; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!groupexpiry!!!');"><?php echo ADMINEMAILUSER_GROUPEXPIRY; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!eachgroupend!!!');"><?php echo ADMINEMAILUSER_ENDGROUPLOOP; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();$('#addGroupModal').modal();"><?php echo ADMINEMAILUSER_ADDGROUP; ?>...</a></li>
                        <li><a href="#" onclick="event.preventDefault();$('#removeGroupModal').modal();"><?php echo ADMINEMAILUSER_REMGROUP; ?>...</a></li>
                        <li><a href="#" onclick="event.preventDefault();$('#replaceGroupModal').modal();"><?php echo ADMINEMAILUSER_REPGROUP; ?>...</a></li>
                        <li><a href="#" onclick="event.preventDefault();$('#extendGroupModal').modal();"><?php echo ADMINEMAILUSER_EXTGROUP; ?>...</a></li>
                        <li class="dropdown-header"><?php echo ADMINEMAILUSER_RESETPASS; ?></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!newpassword!!!');"><?php echo ADMINEMAILUSER_NEWPASS; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!activatepassword!!!');"><?php echo ADMINEMAILUSER_ACTPASS; ?></a></li>
                        <li class="dropdown-header"><?php echo ADMINEMAILUSER_CHGEMAIL; ?></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!requestedemail!!!');"><?php echo ADMINEMAILUSER_REQEMAIL; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!verifyemail!!!');"><?php echo ADMINEMAILUSER_VEREMAIL; ?></a></li>
                        <li class="dropdown-header"><?php echo ADMINEMAILUSER_LOGINLINK; ?></li>
                        <li><a href="#" onclick="event.preventDefault();$('#loginLinkModal').modal();"><?php echo ADMINEMAILUSER_LOGINLINK; ?>...</a></li>
                        <li class="dropdown-header"><?php echo ADMINEMAILUSER_DOWNLINK; ?></li>
                        <li><a href="#" onclick="event.preventDefault();$('#downloadLinkModal').modal();"><?php echo ADMINEMAILUSER_DOWNLINK; ?>...</a></li>
                        <li><a href="#" onclick="event.preventDefault();$('#fileSizeModal').modal();"><?php echo ADMINEMAILUSER_FILESIZE; ?>...</a></li>
                        <li class="dropdown-header"><?php echo ADMINEMAILUSER_ACTPROC; ?></li>
                        <li><a href="#" onclick="event.preventDefault();$('#approveModal').modal();"><?php echo ADMINEMAILUSER_CONACT; ?>...</a></li>
                        <li><a href="#" onclick="event.preventDefault();$('#disableModal').modal();"><?php echo ADMINEMAILUSER_DISACT; ?>...</a></li>
                        <li><a href="#" onclick="event.preventDefault();$('#deleteModal').modal();"><?php echo ADMINEMAILUSER_DELACT; ?>...</a></li>
                        <li class="dropdown-header"><?php echo ADMINGENERAL_CONTACTFORM; ?></li>                        
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!contactform!!!');"><?php echo ADMINEMAILUSER_CONDATA; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();$('#contactLabelModal').modal();""><?php echo ADMINEMAILUSER_CONLAB; ?>...</a></li>
                        <li><a href="#" onclick="event.preventDefault();$('#contactValueModal').modal();""><?php echo ADMINEMAILUSER_CONVAL; ?>...</a></li>
                        <li class="dropdown-header"><?php echo ADMINGENERAL_OTHER; ?></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!sitename!!!');"><?php echo ADMINGENERAL_SITENAME; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!siteemail!!!');"><?php echo ADMINGENERAL_SITEEMAIL; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!siteemail2!!!');"><?php echo ADMINGENERAL_SITEEMAIL2; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!date!!!');"><?php echo ADMINGENERAL_DATE; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!datedmy!!!');"><?php echo ADMINEMAILUSER_DATEDD; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('!!!datemdy!!!');"><?php echo ADMINEMAILUSER_DATEMM; ?></a></li>
                        <li><a href="#" onclick="event.preventDefault();modalobj=$('#viewEmailModal').modal();"><?php echo ADMINEMAILUSER_VIEWEMAIL; ?>...</a></li>
                        <li class="dropdown-header"><?php echo ADMINEMAILUSER_PLUGIN; ?></li>


<?php
  // Get any plugin related template variables (such as !!!unsubscribe!!!)
  for ($k=0;$k<count($slplugin_templatevariablename);$k++)
  {
    if ($slplugin_templatevariabletype[$k]=="link")
    {
?>
                        <li><a href="#" onclick="event.preventDefault();editorInsertLink('<?php echo $slplugin_templatevariablelink[$k]; ?>','<?php echo $slplugin_templatevariablecode[$k]; ?>');  focusEditor();"><?php echo $slplugin_templatevariabledescription[$k]; ?></a></li>
<?php } 
    if ($slplugin_templatevariabletype[$k]=="html")
    {
?>
                        <li><a href="#" onclick="event.preventDefault();editorInsertStr('<?php echo $slplugin_templatevariablecode[$k]; ?>');"><?php echo $slplugin_templatevariabledescription[$k]; ?></a></li>

<?php } ?>
<?php } ?>





                     </ul>
                    </div>
                  </div> 
                  </div> 




                  <div id="htmlmodediv">
                    <div class="form-group" id="">
                    <div class="col-xs-12" id="htmleditordiv">
                    <div id="templateeditordiv"><textarea id="templateeditor" class="form-control" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></textarea></div>
                    </div>
                    </div>
                  </div>



                  <div id="textmodediv">
                    <div class="form-group">
                    <label class="col-xs-12" for="templateeditortext" id="labelbody"><?php ADMINEMAILUSER_BODY; ?></label>
                    <div class="col-xs-12" id="texteditordiv">
                    <div id="templateeditortextdiv"><textarea id="templateeditortext" class="form-control" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></textarea></div>
                    </div>
                    </div>
                  </div>

                  </div>
                  </div>


                  



                  <div class="form-group">
                  <div class="col-sm-11">
                      <div class="btn-toolbar">
                          <button type="button" class="btn btn-primary pull-left" onclick="<?php if ($act=="managetemplates") echo "previewClicked();" ?><?php if ($act=="emailuser") echo "previewForUser('$userid');"; ?><?php if ($act=="emailselected") echo "previewSelected();" ?>"><?php echo ADMINBUTTON_PREVIEW; ?></button>
                          <button type="button" class="btn btn-primary pull-left" id="sendemail" onclick="<?php if ($act=="managetemplates") echo "sendEmailClicked();" ?><?php if ($act=="emailuser") echo "sendEmailToUser('$userid');"; ?><?php if ($act=="emailselected") echo "sendEmailSelected();" ?>"><?php if ($act=="emailselected") echo ADMINBUTTON_SENDEMAILS; else echo ADMINBUTTON_SENDEMAIL; ?></button>
                          <button type="button" class="btn btn-secondary pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                      </div>   
                  </div>
                  </div>

                  <div id="resultsendemail2"></div>

            </div>
            </form>



          </div><!-- /.row -->

<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<!-- Modal dialog boxes -->
<!-- Add group link -->
<form action="#" id="addgroupform" class="form-horizontal" role="form">
  <div class="modal fade" id="addGroupModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_ADDGROUP; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="addgroup_linkexpiry" ><?php echo ADMINEMAILUSER_LINKEXPIRY; ?></label>
                  <div class="col-xs-5 col-sm-3 col-md-3 col-lg-3" id="addgroup_linkexpirydiv">
                      <input type="number" class="form-control" id="addgroup_linkexpiry" maxlength="6" value="1440" min="0" max="999999" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <span id="helpBlock" class="col-xs-12 help-block"><?php echo ADMINEMAILUSER_NOEXPIRY; ?></span>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="addgroup_usergroup" ><?php echo ADMINFIELD_USERGROUP; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="addgroup_usergroupdiv">
                      <input type="text" class="form-control" id="addgroup_usergroup" maxlength="100" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="addgroup_groupexpiry" ><?php echo ADMINEMAILUSER_GROUPEXPIRY.$DateFormat; ?></label>
                  <div class="col-xs-5 col-sm-5 col-md-4 col-lg-3" id="addgroup_groupexpirydiv">
                      <input type="text" class="form-control" id="addgroup_groupexpiry" maxlength="6" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="addgroup_useremail" ><?php echo ADMINGENERAL_USEREMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="addgroup_useremaildiv">
                      <input type="text" class="form-control" id="addgroup_useremail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="addgroup_adminemail" ><?php echo ADMINGENERAL_ADMINEMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="addgroup_adminemaildiv">
                      <input type="text" class="form-control" id="addgroup_adminemail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="addgroup_redirect" ><?php echo ADMINGENERAL_REDIRECTPAGE ?></label>
                  <div class="col-xs-12 col-sm-9 col-md-8 col-lg-9" id="addgroup_redirectdiv">
                      <input type="text" class="form-control" id="addgroup_redirect" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_INSERT; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- Remove group link -->
<form action="#" id="removegroupform" class="form-horizontal" role="form">
  <div class="modal fade" id="removeGroupModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_REMGROUP; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="removegroup_linkexpiry" ><?php echo ADMINEMAILUSER_LINKEXPIRY; ?></label>
                  <div class="col-xs-5 col-sm-3 col-md-3 col-lg-3" id="removegroup_linkexpirydiv">
                      <input type="number" class="form-control" id="removegroup_linkexpiry" maxlength="6" value="1440" min="0" max="999999" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <span id="helpBlock" class="col-xs-12 help-block"><?php echo ADMINEMAILUSER_NOEXPIRY; ?></span>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="removegroup_usergroup" ><?php echo ADMINFIELD_USERGROUP; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="removegroup_usergroupdiv">
                      <input type="text" class="form-control" id="removegroup_usergroup" maxlength="100" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="removegroup_useremail" ><?php echo ADMINGENERAL_USEREMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="removegroup_useremaildiv">
                      <input type="text" class="form-control" id="removegroup_useremail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="removegroup_adminemail" ><?php echo ADMINGENERAL_ADMINEMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="removegroup_adminemaildiv">
                      <input type="text" class="form-control" id="removegroup_adminemail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="removegroup_redirect" ><?php echo ADMINGENERAL_REDIRECTPAGE ?></label>
                  <div class="col-xs-12 col-sm-9 col-md-8 col-lg-9" id="removegroup_redirectdiv">
                      <input type="text" class="form-control" id="removegroup_redirect" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_INSERT; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- replace group link -->
<form action="#" id="replacegroupform" class="form-horizontal" role="form">
  <div class="modal fade" id="replaceGroupModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_REPGROUP; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="replacegroup_linkexpiry" ><?php echo ADMINEMAILUSER_LINKEXPIRY; ?></label>
                  <div class="col-xs-5 col-sm-3 col-md-3 col-lg-3" id="replacegroup_linkexpirydiv">
                      <input type="number" class="form-control" id="replacegroup_linkexpiry" maxlength="6" value="1440" min="0" max="999999" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <span id="helpBlock" class="col-xs-12 help-block"><?php echo ADMINEMAILUSER_NOEXPIRY; ?></span>
             </div>
              <div class="form-group">
                  <label class="col-xs-12" for="replacegroup_usergroup" ><?php echo ADMINFIELD_USERGROUP; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="replacegroup_usergroupdiv">
                      <input type="text" class="form-control" id="replacegroup_usergroup" maxlength="100" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="replacegroup_newusergroup" ><?php echo ADMINEMAILUSER_NEWUSERGROUP; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="replacegroup_newusergroupdiv">
                      <input type="text" class="form-control" id="replacegroup_newusergroup" maxlength="100" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="replacegroup_groupexpiry" ><?php echo ADMINEMAILUSER_NEWGROUPEXPIRY.$DateFormat; ?><br><?php echo ADMINEMAILUSER_LEAVEEXPIRYBLANK; ?></label>
                  <div class="col-xs-5 col-sm-5 col-md-4 col-lg-3" id="replacegroup_groupexpirydiv">
                      <input type="text" class="form-control" id="replacegroup_groupexpiry" maxlength="6" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="replacegroup_useremail" ><?php echo ADMINGENERAL_USEREMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="replacegroup_useremaildiv">
                      <input type="text" class="form-control" id="replacegroup_useremail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="replacegroup_adminemail" ><?php echo ADMINGENERAL_ADMINEMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="replacegroup_adminemaildiv">
                      <input type="text" class="form-control" id="replacegroup_adminemail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="replacegroup_redirect" ><?php echo ADMINGENERAL_REDIRECTPAGE ?></label>
                  <div class="col-xs-12 col-sm-9 col-md-8 col-lg-9" id="replacegroup_redirectdiv">
                      <input type="text" class="form-control" id="replacegroup_redirect" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_INSERT; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- extend group link -->
<form action="#" id="extendgroupform" class="form-horizontal" role="form">
  <div class="modal fade" id="extendGroupModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_EXTGROUP; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="extendgroup_linkexpiry" ><?php echo ADMINEMAILUSER_LINKEXPIRY; ?></label>
                  <div class="col-xs-5 col-sm-3 col-md-3 col-lg-3" id="extendgroup_linkexpirydiv">
                      <input type="number" class="form-control" id="extendgroup_linkexpiry" maxlength="6" value="1440" min="0" max="999999" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <span id="helpBlock" class="col-xs-12 help-block"><?php echo ADMINEMAILUSER_NOEXPIRY; ?></span>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="extendgroup_usergroup" ><?php echo ADMINFIELD_USERGROUP; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="extendgroup_usergroupdiv">
                      <input type="text" class="form-control" id="extendgroup_usergroup" maxlength="100" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="extendgroup_groupexpiry" ><?php echo ADMINEMAILUSER_GROUPEXPIRY.$DateFormat; ?></label>
                  <div class="col-xs-5 col-sm-5 col-md-4 col-lg-3" id="extendgroup_groupexpirydiv">
                      <input type="text" class="form-control" id="extendgroup_groupexpiry" maxlength="6" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="extendgroup_expirytype" ><?php echo ADMINEMAILUSER_EXPIRYTYPE; ?></label>
                  <div class="col-xs-5 col-sm-5 col-md-4 col-lg-3" id="extendgroup_expirytypediv">
                      <input type="text" class="form-control" id="extendgroup_expirytype" maxlength="6" value="0" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="extendgroup_useremail" ><?php echo ADMINGENERAL_USEREMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="extendgroup_useremaildiv">
                      <input type="text" class="form-control" id="extendgroup_useremail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="extendgroup_adminemail" ><?php echo ADMINGENERAL_ADMINEMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="extendgroup_adminemaildiv">
                      <input type="text" class="form-control" id="extendgroup_adminemail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="extendgroup_redirect" ><?php echo ADMINGENERAL_REDIRECTPAGE ?></label>
                  <div class="col-xs-12 col-sm-9 col-md-8 col-lg-9" id="extendgroup_redirectdiv">
                      <input type="text" class="form-control" id="extendgroup_redirect" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_INSERT; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- Login link -->
<form action="#" id="loginlinkform" class="form-horizontal" role="form">
  <div class="modal fade" id="loginLinkModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_LOGINLINK; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="loginlink_linkexpiry" ><?php echo ADMINEMAILUSER_LINKEXPIRY; ?></label>
                  <div class="col-xs-5 col-sm-3 col-md-3 col-lg-3" id="loginlink_linkexpirydiv">
                      <input type="number" class="form-control" id="loginlink_linkexpiry" maxlength="6" value="1440" min="0" max="999999" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <span id="helpBlock" class="col-xs-12 help-block"><?php echo ADMINEMAILUSER_NOEXPIRY; ?></span>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="loginlink_page" ><?php echo ADMINEMAILUSER_LOGINURL ?></label>
                  <div class="col-xs-12 col-sm-9 col-md-8 col-lg-9" id="loginlink_pagediv">
                      <input type="text" class="form-control" id="loginlink_page" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_INSERT; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- Download link -->
<form action="#" id="downloadlinkform" class="form-horizontal" role="form">
  <div class="modal fade" id="downloadLinkModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_DOWNLINK; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="downloadlink_linkexpiry" ><?php echo ADMINEMAILUSER_LINKEXPIRY; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="downloadlink_linkexpirydiv">
                      <input type="text" class="form-control" id="downloadlink_linkexpiry" maxlength="12" value="1440" min="0" max="999999" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              <span id="helpBlock" class="col-xs-12 help-block"><?php echo ADMINEMAILUSER_EXPIRY12; ?></span>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="downloadlink_file" ><?php echo ADMINEMAILUSER_FILENAME ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="downloadlink_filediv">
                      <input type="text" class="form-control" id="downloadlink_file" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="downloadlink_location" ><?php echo ADMINEMAILUSER_FILELOCATION ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="downloadlink_locationdiv">
                      <select type="text" class="form-control selectpicker" id="downloadlink_location">
                      <option value="">Default</option>
                      <?php foreach($FileLocations as $key => $value) { ?>
                      <option value="<?php echo $key; ?>"><?php echo $key; ?></option>
                      <?php } ?>
                      </select>
                  </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_INSERT; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- File size -->
<form action="#" id="filesizeform" class="form-horizontal" role="form">
  <div class="modal fade" id="fileSizeModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_FILESIZE; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="filesize_file" ><?php echo ADMINEMAILUSER_FILENAME ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="filesize_filediv">
                      <input type="text" class="form-control" id="filesize_file" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="filesize_location" ><?php echo ADMINEMAILUSER_FILELOCATION ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="filesize_locationdiv">
                      <select type="text" class="form-control selectpicker" id="filesize_location">
                      <option value="">Default</option>
                      <?php foreach($FileLocations as $key => $value) { ?>
                      <option value="<?php echo $key; ?>"><?php echo $key; ?></option>
                      <?php } ?>
                      </select>
                  </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_INSERT; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- Approve / confirm link -->
<form action="#" id="approveform" class="form-horizontal" role="form">
  <div class="modal fade" id="approveModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_CONACT; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="approve_linkexpiry" ><?php echo ADMINEMAILUSER_LINKEXPIRY; ?></label>
                  <div class="col-xs-5 col-sm-3 col-md-3 col-lg-3" id="approve_linkexpirydiv">
                      <input type="number" class="form-control" id="approve_linkexpiry" maxlength="6" value="1440" min="0" max="999999" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <span id="helpBlock" class="col-xs-12 help-block"><?php echo ADMINEMAILUSER_NOEXPIRY; ?></span>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="approve_useremail" ><?php echo ADMINGENERAL_USEREMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="approve_useremaildiv">
                      <input type="text" class="form-control" id="approve_useremail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="approve_adminemail" ><?php echo ADMINGENERAL_ADMINEMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="approve_adminemaildiv">
                      <input type="text" class="form-control" id="approve_adminemail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="approve_redirect" ><?php echo ADMINGENERAL_REDIRECTPAGE ?></label>
                  <div class="col-xs-12 col-sm-9 col-md-8 col-lg-9" id="approve_redirectdiv">
                      <input type="text" class="form-control" id="approve_redirect" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_INSERT; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- Disable link -->
<form action="#" id="disableform" class="form-horizontal" role="form">
  <div class="modal fade" id="disableModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_DISACT; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="disable_linkexpiry" ><?php echo ADMINEMAILUSER_LINKEXPIRY; ?></label>
                  <div class="col-xs-5 col-sm-3 col-md-3 col-lg-3" id="disable_linkexpirydiv">
                      <input type="number" class="form-control" id="disable_linkexpiry" maxlength="6" value="1440" min="0" max="999999" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <span id="helpBlock" class="col-xs-12 help-block"><?php echo ADMINEMAILUSER_NOEXPIRY; ?></span>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="disable_useremail" ><?php echo ADMINGENERAL_USEREMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="disable_useremaildiv">
                      <input type="text" class="form-control" id="disable_useremail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="disable_adminemail" ><?php echo ADMINGENERAL_ADMINEMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="disable_adminemaildiv">
                      <input type="text" class="form-control" id="disable_adminemail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="disable_redirect" ><?php echo ADMINGENERAL_REDIRECTPAGE ?></label>
                  <div class="col-xs-12 col-sm-9 col-md-8 col-lg-9" id="disable_redirectdiv">
                      <input type="text" class="form-control" id="disable_redirect" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_INSERT; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- Delete link -->
<form action="#" id="deleteform" class="form-horizontal" role="form">
  <div class="modal fade" id="deleteModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_DELACT; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="delete_linkexpiry" ><?php echo ADMINEMAILUSER_LINKEXPIRY; ?></label>
                  <div class="col-xs-5 col-sm-3 col-md-3 col-lg-3" id="delete_linkexpirydiv">
                      <input type="number" class="form-control" id="delete_linkexpiry" maxlength="6" value="1440" min="0" max="999999" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <span id="helpBlock" class="col-xs-12 help-block"><?php echo ADMINEMAILUSER_NOEXPIRY; ?></span>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="delete_useremail" ><?php echo ADMINGENERAL_USEREMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="delete_useremaildiv">
                      <input type="text" class="form-control" id="delete_useremail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="delete_adminemail" ><?php echo ADMINGENERAL_ADMINEMAIL; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="delete_adminemaildiv">
                      <input type="text" class="form-control" id="delete_adminemail" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="delete_redirect" ><?php echo ADMINGENERAL_REDIRECTPAGE ?></label>
                  <div class="col-xs-12 col-sm-9 col-md-8 col-lg-9" id="delete_redirectdiv">
                      <input type="text" class="form-control" id="delete_redirect" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_INSERT; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- View email link -->
<form action="#" id="viewemailform" class="form-horizontal" role="form">
  <div class="modal fade" id="viewEmailModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_VIEWEMAIL; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="viewemail_linkexpiry" ><?php echo ADMINEMAILUSER_LINKEXPIRY; ?></label>
                  <div class="col-xs-5 col-sm-3 col-md-3 col-lg-3" id="viewemail_linkexpirydiv">
                      <input type="number" class="form-control" id="viewemail_linkexpiry" maxlength="6" value="1440" min="0" max="999999" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <span id="helpBlock" class="col-xs-12 help-block"><?php echo ADMINEMAILUSER_NOEXPIRY; ?></span>
              </div>
              <div class="form-group">
                  <label class="col-xs-12" for="viewemail_email" ><?php echo ADMINEMAILUSER_EMAILTEMPLATE; ?></label>
                  <div class="col-xs-10 col-sm-7 col-md-6 col-lg-5" id="viewemail_emaildiv">
                      <input type="text" class="form-control" id="viewemail_email" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_INSERT; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- Contact form label -->
<form action="#" id="contactlabelform" class="form-horizontal" role="form">
  <div class="modal fade" id="contactLabelModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_CONLAB; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="contactlabel_num" ><?php echo ADMINEMAILUSER_FIELDNUM; ?></label>
                  <div class="col-xs-5 col-sm-3 col-md-3 col-lg-3" id="contactlabel_numdiv">
                      <input type="number" class="form-control" id="contactlabel_num" maxlength="2" value="0" min="0" max="99" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <span id="helpBlock" class="col-xs-12 help-block"><?php echo ADMINEMAILUSER_FIELDNUMHELP; ?></span>
              </div>
            </div>  
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_INSERT; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- Contact form field -->
<form action="#" id="contactvalueform" class="form-horizontal" role="form">
  <div class="modal fade" id="contactValueModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_CONVAL; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="contactvalue_num" ><?php echo ADMINEMAILUSER_FIELDNUM; ?></label>
                  <div class="col-xs-5 col-sm-3 col-md-3 col-lg-3" id="contactvalue_numdiv">
                      <input type="number" class="form-control" id="contactvalue_num" maxlength="2" value="0" min="0" max="99" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <span id="helpBlock" class="col-xs-12 help-block"><?php echo ADMINEMAILUSER_FIELDNUMHELP; ?></span>
              </div>
            </div>  
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_INSERT; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- Save email -->
<form action="#" id="saveemailform" class="form-horizontal" role="form">
  <div class="modal fade" id="saveEmailModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_SAVEEMAIL; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="saveemail_filename" ><?php echo ADMINEMAILUSER_EMAILTEMPLATE; ?></label>
                  <div class="col-xs-12 col-sm-9 col-md-8 col-lg-9" id="saveemail_filenamediv">
                      <input type="text" class="form-control" id="saveemail_filename" maxlength="100" value="<?php echo $slusername; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
            </div>  
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary"><?php echo ADMINBUTTON_SAVE; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- Preview email -->
<form action="#" id="previewemailform" class="form-horizontal" role="form">
  <div class="modal fade" id="previewEmailModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINBUTTON_PREVIEW; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="previewemail_username" ><?php echo ADMINEMAILUSER_PREVIEWUSER; ?></label>
                  <div class="col-xs-12 col-sm-9 col-md-8 col-lg-9" id="previewemail_usernamediv">
                      <input type="text" class="form-control" id="previewemail_username" maxlength="100" value="<?php echo $slusername; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
            </div>  
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary" id="previewemail"><?php echo ADMINBUTTON_PREVIEW; ?></button>
            </div>
            <div id="previewemailresult"></div>
        </div>
      </div>
  </div>
</form>

<!-- Send email -->
<form action="#" id="sendemailform" class="form-horizontal" role="form">
  <div class="modal fade" id="sendEmailModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINBUTTON_SENDEMAIL; ?></h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="col-xs-12" for="sendemail_username" ><?php echo ADMINEMAILUSER_SENDUSER; ?></label>
                  <div class="col-xs-12 col-sm-9 col-md-8 col-lg-9" id="sendemail_usernamediv">
                      <input type="text" class="form-control" id="sendemail_username" maxlength="100" value="<?php echo $slusername; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
              </div>
            </div>  
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary" id="sendemail_button"><?php echo ADMINBUTTON_SENDEMAIL; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>

<!-- Send email to selected modal 1-->
<form action="#" id="sendselected1form" class="form-horizontal" role="form">
  <div class="modal fade" id="sendSelected1Modal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_EMAILSELECTED; ?></h4>
            </div>
            <div class="modal-body">


              <div class="form-group">
                  <div class="col-sm-10" id="sendselected1_dedupediv">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="sendselected1_dedupe" value="<?php echo $emaildedupe; ?>" checked >&nbsp;&nbsp;<?php echo ADMINEMAILUSER_DEDUPE; ?>
                      </label>
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="col-sm-10" id="sendselected1_deselectdiv">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="sendselected1_deselect" value="<?php echo $emaildeselect; ?>" checked >&nbsp;&nbsp;<?php echo ADMINEMAILUSER_DESELECT; ?>
                      </label>
                    </div>
                  </div>
              </div>


            </div>  
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="submit" class="btn btn-primary" id="sendselected1_button"><?php echo ADMINBUTTON_SENDEMAILS; ?></button>
            </div>
        </div>
      </div>
  </div>
</form>


<!-- Send email to selected modal 2-->
  <div class="modal fade" id="sendSelected2Modal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"> 
              <h4 class="modal-title"><?php echo ADMINEMAILUSER_EMAILSELECTED; ?></h4>
            </div>
            <div class="modal-body">
              <table class="info">
              <tr>
              <th><?php echo ADMINEMAILUSER_SENT; ?></td><td><span id="numsent"></span></td>
              </tr>
              <tr>
              <th><?php echo ADMINEMAILUSER_FAILED; ?></td><td><span id="numfail"></span></td>
              </tr>
              <tr>
              <th><?php echo ADMINEMAILUSER_BLOCKED; ?></td><td><span id="numblocked"></span></td>
              </tr>
              </table>
              <div class="progress" id="sentprogressdiv">
                <div id="sentprogress" class="progress-bar progress-bar-striped" role="progressbar">
                  0%
                </div>
              </div>
              <div id="statustext"></div>

            </div>  
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" id="sendselected2_pause" onclick="pauseClicked();"><?php echo ADMINBUTTON_PAUSE; ?></button>
              <button type="button" class="btn btn-primary" id="sendselected2_continue" onclick="continueClicked();"><?php echo ADMINBUTTON_CONTINUE; ?></button>
              <button type="button" class="btn btn-default" id="sendselected2_cancel" onclick="cancelClicked();"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="button" class="btn btn-primary" id="sendselected2_close" onclick="closeClicked();"><?php echo ADMINBUTTON_CLOSE; ?></button>
            </div>
        </div>
      </div>
  </div>



<?php include("adminthemefooter.php"); ?>
<script type="text/javascript">
  var groupcount=2;
</script>
<script src='tinymce/tinymce.min.js'></script>
<script type="text/javascript" src="fancybox/source/jquery.fancybox.js?v=2.1.4"></script>
<script type="text/javascript" src="fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="rangyinputs/rangyinputs-jquery.js"></script>
<script src="emailuser.js"></script>
<script type="text/javascript">
// Javascript here needs PHP so is not in .js file


</script>
  </body>
</html>
