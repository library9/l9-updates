<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  require("admincommonfunctions.php");  

  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  $errors = array();    // array to hold validation errors
  $data = array();      // array to pass back data 
  $emailsenderror=false; // email send error is handled separately
  // Check CSRF value
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError($data,$errors,ADMINMSG_NOTUPDATEUSER.". ".ADMINMSG_CSRFFAILED.".");
    exit;
  }   
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError($data,$errors,ADMINMSG_NOTUPDATEUSER.". ".ADMINMSG_MYSQLERROR.".");
    exit;
  }
  $userid=$_POST['userid'];
  if (!is_numeric($userid))
  {
    returnError($data,$errors,ADMINMSG_NOTUPDATEUSER.". ".ADMINMSG_MYSQLERROR.".");
    exit;    
  }
  $groupcount=$_POST['groupcount']; // This isn't number of groups but maximum group number. e.g. 23 for group23.
  $newuser=$_POST['user'];
  $user=$_POST['olduser'];
  $pass=$_POST['pss'];
  $existingpass=$_POST['existingpss'];
  $nm=$_POST['nm'];
  $em=$_POST['em'];
  $en=$_POST['en'];
  if ($en=="")
    $en="No";    
  $cu1=$_POST['cu1'];
  $cu2=$_POST['cu2'];
  $cu3=$_POST['cu3'];
  $cu4=$_POST['cu4'];
  $cu5=$_POST['cu5'];
  $cu6=$_POST['cu6'];
  $cu7=$_POST['cu7'];
  $cu8=$_POST['cu8'];
  $cu9=$_POST['cu9'];
  $cu10=$_POST['cu10']; 
  $cu11=$_POST['cu11'];
  $cu12=$_POST['cu12'];
  $cu13=$_POST['cu13'];
  $cu14=$_POST['cu14'];
  $cu15=$_POST['cu15'];
  $cu16=$_POST['cu16'];
  $cu17=$_POST['cu17'];
  $cu18=$_POST['cu18'];
  $cu19=$_POST['cu19'];
  $cu20=$_POST['cu20'];
  $cu21=$_POST['cu21'];
  $cu22=$_POST['cu22'];
  $cu23=$_POST['cu23'];
  $cu24=$_POST['cu24'];
  $cu25=$_POST['cu25'];
  $cu26=$_POST['cu26'];
  $cu27=$_POST['cu27'];
  $cu28=$_POST['cu28'];
  $cu29=$_POST['cu29'];
  $cu30=$_POST['cu30'];
  $cu31=$_POST['cu31'];
  $cu32=$_POST['cu32'];
  $cu33=$_POST['cu33'];
  $cu34=$_POST['cu34'];
  $cu35=$_POST['cu35'];
  $cu36=$_POST['cu36'];
  $cu37=$_POST['cu37'];
  $cu38=$_POST['cu38'];
  $cu39=$_POST['cu39'];
  $cu40=$_POST['cu40'];
  $cu41=$_POST['cu41'];
  $cu42=$_POST['cu42'];
  $cu43=$_POST['cu43'];
  $cu44=$_POST['cu44'];
  $cu45=$_POST['cu45'];
  $cu46=$_POST['cu46'];
  $cu47=$_POST['cu47'];
  $cu48=$_POST['cu48'];
  $cu49=$_POST['cu49'];
  $cu50=$_POST['cu50'];
  $template=$_POST['template'];
  $forcelogout=$_POST['forcelogout'];
  if ($forcelogout!=1)
    $forcelogout=0;
  $sendemail=$_POST['sendemail'];
  if ($sendemail!=1)
    $sendemail=0;
  $existingsendemail=$_POST['existingsendemail'];
  if ($existingsendemail!=1)
    $existingsendemail=0;

  if (get_magic_quotes_gpc())
  {
    $newuser=stripslashes($newuser);
    $nm=stripslashes($nm);
    $em=stripslashes($em);      
    $cu1=stripslashes($cu1);
    $cu2=stripslashes($cu2);
    $cu3=stripslashes($cu3);
    $cu4=stripslashes($cu4);
    $cu5=stripslashes($cu5);
    $cu6=stripslashes($cu6);
    $cu7=stripslashes($cu7);
    $cu8=stripslashes($cu8);
    $cu9=stripslashes($cu9);
    $cu10=stripslashes($cu10);      
    $cu11=stripslashes($cu11);
    $cu12=stripslashes($cu12);
    $cu13=stripslashes($cu13);
    $cu14=stripslashes($cu14);
    $cu15=stripslashes($cu15);
    $cu16=stripslashes($cu16);
    $cu17=stripslashes($cu17);
    $cu18=stripslashes($cu18);
    $cu19=stripslashes($cu19);
    $cu20=stripslashes($cu20);      
    $cu21=stripslashes($cu21);
    $cu22=stripslashes($cu22);
    $cu23=stripslashes($cu23);
    $cu24=stripslashes($cu24);
    $cu25=stripslashes($cu25);
    $cu26=stripslashes($cu26);
    $cu27=stripslashes($cu27);
    $cu28=stripslashes($cu28);
    $cu29=stripslashes($cu29);
    $cu30=stripslashes($cu30);      
    $cu31=stripslashes($cu31);
    $cu32=stripslashes($cu32);
    $cu33=stripslashes($cu33);
    $cu34=stripslashes($cu34);
    $cu35=stripslashes($cu35);
    $cu36=stripslashes($cu36);
    $cu37=stripslashes($cu37);
    $cu38=stripslashes($cu38);
    $cu39=stripslashes($cu39);
    $cu40=stripslashes($cu40);      
    $cu41=stripslashes($cu41);
    $cu42=stripslashes($cu42);
    $cu43=stripslashes($cu43);
    $cu44=stripslashes($cu44);
    $cu45=stripslashes($cu45);
    $cu46=stripslashes($cu46);
    $cu47=stripslashes($cu47);
    $cu48=stripslashes($cu48);
    $cu49=stripslashes($cu49);
    $cu50=stripslashes($cu50);      
    $template=stripslashes($template);      
  }
  // Get usergroups. These can be group1 group3 etc with gaps.
  for ($k=1;$k<=$groupcount;$k++)
  {
    if ((isset($_POST['group'.$k])) || (isset($_POST['expiry'.$k])))
    {
      $var="group".$k;
      $$var=$_POST['group'.$k];
      $var="expiry".$k;
      $$var=$_POST['expiry'.$k];
    }
  }
  // Validate username
  $usererror="";
  if ($newuser=="")
    $usererror = ADMINMSG_ENTERUSER;
  // Call plugin and eventhandler validation function
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if (($usererror=="") && (function_exists($slplugin_event_onUsernameValidate[$p])))
      $usererror=call_user_func($slplugin_event_onUsernameValidate[$p],$slpluginid[$p],$newuser,2);
  }
  if ($usererror=="")
  {
    if (function_exists("sl_onUsernameValidate"))
      $usererror=sl_onUsernameValidate($newuser,2);
  }    
  // check username doesn't contain invalid characters
  if (($usererror=="") && (strspn($newuser, $ValidUsernameChars) != strlen($newuser)))
    $usererror=ADMINMSG_USERINVALID;
  if ($usererror!="")
    $errors['user']=$usererror;

  // Validate password
  $passerror="";
  if ((!$MD5passwords) && ($passerror=="") && ($pass==""))
    $passerror=ADMINMSG_PASSSHORT;
  if ($pass!="")
  {        
    // Call plugin and eventhandler validation function
    for ($p=0;$p<$slnumplugins;$p++)
    {
      if (($passerror=="") && (function_exists($slplugin_event_onPasswordValidate[$p])))
        $passerror=call_user_func($slplugin_event_onPasswordValidate[$p],$slpluginid[$p],$pass,2);
    }
    if ($passerror=="")
    {
      if (function_exists("sl_onPasswordValidate"))
        $passerror=sl_onPasswordValidate($pass,2);
    }
  }
  if (($MD5passwords) && ($pass!=""))
  {
    // check password is at least 5 characters long
    if (($passerror=="") && (strlen($pass)<5))
      $passerror=ADMINMSG_PASSSHORT; 
    // Check for valid characters
    if (($passerror=="") && (strspn($pass, $ValidPasswordChars) != strlen($pass)))
      $passerror=ADMINMSG_PASSINVALID;
  }
  if (!$MD5passwords)
  {
    // check password is at least 5 characters long
    if (($passerror=="") && (strlen($pass)<5))
      $passerror=ADMINMSG_PASSSHORT; 
    // Check for valid characters
    if (($passerror=="") && (strspn($pass, $ValidPasswordChars) != strlen($pass)))
      $passerror=ADMINMSG_PASSINVALID;
  }
  if ($passerror!="")
    $errors['pss']=$passerror;

  // Validate name
  $nmerror="";
  if (($nmerror=="") && ($nm==""))
    $nmerror=ADMINMSG_ENTERNAME;     
  // Call plugin and eventhandler validation function
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if (($nmerror=="") && (function_exists($slplugin_event_onNameValidate[$p])))
      $nmerror=call_user_func($slplugin_event_onNameValidate[$p],$slpluginid[$p],$nm,2);
  }
  if ($nmerror=="")
  {
    if (function_exists("sl_onNameValidate"))
    $nmerror=sl_onNameValidate($nm,2);
  }
  if ($nmerror!="")
    $errors['nm']=$nmerror;

  // Validate email field
  $emerror="";
  if (($emerror=="") && ($em==""))
    $emerror=ADMINMSG_ENTEREMAIL;     
  // Check email is in valid format
  if (($emerror=="") && (!sl_validate_email($em)))
    $emerror=ADMINMSG_EMAILINVALID;    
  // Check if email address already used (by a different user if required                        
  if (($emerror=="") && ($EmailUnique==2))
  {
    $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." WHERE ".$EmailField."=".sl_quote_smart($em)." AND ".$UsernameField."!=".sl_quote_smart($user));
    if ($mysql_result===false)
    {
      returnError($data,$errors,ADMINMSG_NOTUPDATEUSER.". ".ADMINMSG_MYSQLERROR.".");
      exit;
    }
    $num = mysqli_num_rows($mysql_result);
    if ($num>0)
      $emerror=ADMINMSG_EMAILINUSE;
  }
  // Call plugin and eventhandler validation function
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if (($emerror=="") && (function_exists($slplugin_event_onEmailValidate[$p])))
      $emerror=call_user_func($slplugin_event_onEmailValidate[$p],$slpluginid[$p],$em,2);
  }
  if ($emerror=="")
  {
    if (function_exists("sl_onEmailValidate"))
      $emerror=sl_onEmailValidate($em,2);
  }
  if ($emerror!="")
    $errors['em']=$emerror;

  // Validate custom fields if required
  for ($k=1;$k<51;$k++)
  {
    $customerror="";
    $cusvar="cu".$k;
    $cusvar2="Custom".$k."Validate";
    $cusvar3="CustomTitle".$k;
    $cusvar4="sl_onCustom".$k."Validate";
    $cusvar5="slplugin_event_onCustom".$k."Validate";
    // Validate for plugins  
    for ($p=0;$p<$slnumplugins;$p++)   
    {
      if (($customerror=="") && (function_exists(${$cusvar5}[$p])))
      {
        $customerror=call_user_func(${$cusvar5}[$p],$slpluginid[$p],$$cusvar,$$cusvar3,2);
      }
    }
    // Validate using eventhandlers
    if (($customerror=="") && (($$cusvar2==2) || ($$cusvar2==3)))
    {
      $customerror=call_user_func($cusvar4,$$cusvar,$$cusvar3,2);
    }
    if ($customerror!="")
      $errors[$cusvar]=$customerror;
  }
  // Validate usergroups and expiries
  for ($k=1;$k<=$groupcount;$k++)
  {
    $gvar="group".$k;
    $evar="expiry".$k;
    if ((!isset($$gvar)) && (!isset($$evar)))
      continue;
    $grouperror="";
    $expiryerror="";
    if (($$gvar!="") && (!validUsergroup($$gvar,true)))
      $grouperror=ADMINMSG_GROUPINVALID;
    if (($grouperror=="") && ($slsubadmin) && (trim(strtoupper($$gvar))=="ADMIN"))
      $grouperror=ADMINMSG_ADMINNOTALLOWED;
    if (($$evar!="") && (strspn($$evar, "0123456789") != strlen($$evar)))
      $expiryerror=ADMINMSG_EXPIRYINVALID;
    if (strlen($$evar)==6)
    {
      if (!dateValid($$evar,$DateFormat))
        $expiryerror=ADMINMSG_EXPIRYDATEINVALID;
    }
    if (($$gvar=="") && ($$evar!=""))
      $grouperror=ADMINMSG_GROUPINVALID;      
    if ($grouperror!="")
      $errors[$gvar]=$grouperror;
    if ($expiryerror!="")
      $errors[$evar]=$expiryerror;
  }

  // If sending email check email template has correct extension and exists
  if ($sendemail=="1")
  {
    $ext = strtolower(pathinfo($template, PATHINFO_EXTENSION));
    if (($template!="") && ($ext!="html") && ($ext!="htm") && ($ext!="txt"))
      $errors['sendemail']=ADMINMSG_TEMPLATETYPE;      
    else
    {
      if (($template!="") && ($sendemail=="1")) 
      {
        if (!sl_ReadEmailTemplate($template,$subject,$mailBody,$htmlformat))
          $errors['sendemail']=ADMINMSG_TEMPLATENOLOAD;      
      }
    }
    // Double check path doesn't start with / or \ or contain .. or :
    if ((substr($template,0,1)=="/") || (substr($template,0,1)=="\\") || (false!=(strpos($template, ".."))) || (false!=(strpos($template, ":"))))
      $errors['sendemail']=ADMINMSG_TEMPLATENOLOAD;      
  }
  // Concatenate groups and expiry times in table format
  $ug="";
  for ($k=1;$k<=$groupcount;$k++)
  {
    $pvar1="group".$k;
    $pvar2="expiry".$k;
    if (!isset($$pvar1))
      continue;
    if ($$pvar1!="")
    {
      if ($ug!="")
        $ug.="^";
      $ug.=$$pvar1;  
      if ($$pvar2!="")
      {
        // If expiry is number of days then convert to date
        if (strlen($$pvar2)<6)
        {
          if ($DateFormat=="DDMMYY")
            $expirystr=gmdate("dmy",time()+$$pvar2*86400);
          if ($DateFormat=="MMDDYY")
            $expirystr=gmdate("mdy",time()+$$pvar2*86400);            
        }
        else
          $expirystr=$$pvar2;
        $ug.=":";
        $ug.=$expirystr;
      }         
    }
  }
  if (!empty($errors))
  {
    returnError($data,$errors,ADMINMSG_FORMERROR);
    exit;
  }
 
  // Give last chance to plugins and event handler to block changes
  $paramdata['userid']=$userid;  // previous versions left this blank
  $paramdata['oldusername']=$user;
  $paramdata['username']=$newuser;
  $paramdata['password']=$pass;
  $paramdata['enabled']=$en;
  $paramdata['name']=$nm;
  $paramdata['email']=$em;
  $paramdata['usergroups']=$ug;
  $paramdata['custom1']=$cu1;
  $paramdata['custom2']=$cu2;
  $paramdata['custom3']=$cu3;
  $paramdata['custom4']=$cu4;
  $paramdata['custom5']=$cu5;
  $paramdata['custom6']=$cu6;
  $paramdata['custom7']=$cu7;
  $paramdata['custom8']=$cu8;
  $paramdata['custom9']=$cu9;
  $paramdata['custom10']=$cu10;
  $paramdata['custom11']=$cu11;
  $paramdata['custom12']=$cu12;
  $paramdata['custom13']=$cu13;
  $paramdata['custom14']=$cu14;
  $paramdata['custom15']=$cu15;
  $paramdata['custom16']=$cu16;
  $paramdata['custom17']=$cu17;
  $paramdata['custom18']=$cu18;
  $paramdata['custom19']=$cu19;
  $paramdata['custom20']=$cu20;
  $paramdata['custom21']=$cu21;
  $paramdata['custom22']=$cu22;
  $paramdata['custom23']=$cu23;
  $paramdata['custom24']=$cu24;
  $paramdata['custom25']=$cu25;
  $paramdata['custom26']=$cu26;
  $paramdata['custom27']=$cu27;
  $paramdata['custom28']=$cu28;
  $paramdata['custom29']=$cu29;
  $paramdata['custom30']=$cu30;
  $paramdata['custom31']=$cu31;
  $paramdata['custom32']=$cu32;
  $paramdata['custom33']=$cu33;
  $paramdata['custom34']=$cu34;
  $paramdata['custom35']=$cu35;
  $paramdata['custom36']=$cu36;
  $paramdata['custom37']=$cu37;
  $paramdata['custom38']=$cu38;
  $paramdata['custom39']=$cu39;
  $paramdata['custom40']=$cu40;
  $paramdata['custom41']=$cu41;
  $paramdata['custom42']=$cu42;
  $paramdata['custom43']=$cu43;
  $paramdata['custom44']=$cu44;
  $paramdata['custom45']=$cu45;
  $paramdata['custom46']=$cu46;
  $paramdata['custom47']=$cu47;
  $paramdata['custom48']=$cu48;
  $paramdata['custom49']=$cu49;
  $paramdata['custom50']=$cu50;
  $paramdata['from']=1;                      
  // Call plugin event
  // Currently plugins only return a form error (not for fields)
  $editmsg="";
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if ($editmsg=="")
    {
      if (function_exists($slplugin_event_onCheckModifyUser[$p]))
      {
        $res=call_user_func($slplugin_event_onCheckModifyUser[$p],$slpluginid[$p],$paramdata);
        if ($res['ok']==false)
          $editmsg=$res['message'];
      } 
    }  
  }
  if ($editmsg!="")
  {
    returnError($data,$errors,$editmsg);
    exit;
  }

  // Call eventhandler
  if (function_exists("sl_onCheckModifyUser"))
  {
    $res=sl_onCheckModifyUser($paramdata);
    if ($res['ok']==false)
      $editmsg=$res['message'];
  }  
  if ($editmsg!="")
  {
    returnError($data,$errors,$editmsg);
    exit;
  }

  if (($MD5passwords!=true) || ($pass!=""))
  {
    $passtowrite=$pass;
    if ($MD5passwords)
      $passtowrite=password_hash($pass,PASSWORD_DEFAULT);
    $Query="UPDATE ".$DbTableName." SET ".$UsernameField."=".sl_quote_smart($newuser).", ".$PasswordField.
    "=".sl_quote_smart($passtowrite).", ".$EnabledField."=".sl_quote_smart($en).", ".$NameField."=".sl_quote_smart($nm).", ".$EmailField."=".sl_quote_smart($em).", ".$UsergroupsField.
    "=".sl_quote_smart($ug).", ".$Custom1Field."=".sl_quote_smart($cu1).", ".$Custom2Field."=".sl_quote_smart($cu2).", ".$Custom3Field."=".sl_quote_smart($cu3).", ".$Custom4Field.
    "=".sl_quote_smart($cu4).", ".$Custom5Field."=".sl_quote_smart($cu5).", ".$Custom6Field."=".sl_quote_smart($cu6).", ".$Custom7Field."=".sl_quote_smart($cu7).", ".$Custom8Field.
    "=".sl_quote_smart($cu8).", ".$Custom9Field."=".sl_quote_smart($cu9).", ".$Custom10Field."=".sl_quote_smart($cu10).", ".$Custom11Field."=".sl_quote_smart($cu11).", ".$Custom12Field.
    "=".sl_quote_smart($cu12).", ".$Custom13Field."=".sl_quote_smart($cu13).", ".$Custom14Field."=".sl_quote_smart($cu14).", ".$Custom15Field."=".sl_quote_smart($cu15).", ".$Custom16Field.
    "=".sl_quote_smart($cu16).", ".$Custom17Field."=".sl_quote_smart($cu17).", ".$Custom18Field."=".sl_quote_smart($cu18).", ".$Custom19Field."=".sl_quote_smart($cu19).", ".$Custom20Field.
    "=".sl_quote_smart($cu20).", ".$Custom21Field."=".sl_quote_smart($cu21).", ".$Custom22Field."=".sl_quote_smart($cu22).", ".$Custom23Field."=".sl_quote_smart($cu23).", ".$Custom24Field.
    "=".sl_quote_smart($cu24).", ".$Custom25Field."=".sl_quote_smart($cu25).", ".$Custom26Field."=".sl_quote_smart($cu26).", ".$Custom27Field."=".sl_quote_smart($cu27).", ".$Custom28Field.
    "=".sl_quote_smart($cu28).", ".$Custom29Field."=".sl_quote_smart($cu29).", ".$Custom30Field."=".sl_quote_smart($cu30).", ".$Custom31Field."=".sl_quote_smart($cu31).", ".$Custom32Field.
    "=".sl_quote_smart($cu32).", ".$Custom33Field."=".sl_quote_smart($cu33).", ".$Custom34Field."=".sl_quote_smart($cu34).", ".$Custom35Field."=".sl_quote_smart($cu35).", ".$Custom36Field.
    "=".sl_quote_smart($cu36).", ".$Custom37Field."=".sl_quote_smart($cu37).", ".$Custom38Field."=".sl_quote_smart($cu38).", ".$Custom39Field."=".sl_quote_smart($cu39).", ".$Custom40Field.
    "=".sl_quote_smart($cu40).", ".$Custom41Field."=".sl_quote_smart($cu41).", ".$Custom42Field."=".sl_quote_smart($cu42).", ".$Custom43Field."=".sl_quote_smart($cu43).", ".$Custom44Field.
    "=".sl_quote_smart($cu44).", ".$Custom45Field."=".sl_quote_smart($cu45).", ".$Custom46Field."=".sl_quote_smart($cu46).", ".$Custom47Field."=".sl_quote_smart($cu47).", ".$Custom48Field.
    "=".sl_quote_smart($cu48).", ".$Custom49Field."=".sl_quote_smart($cu49).", ".$Custom50Field."=".sl_quote_smart($cu50)." WHERE ".$UsernameField."=".sl_quote_smart($user);
  }
  else
  {
    $Query="UPDATE ".$DbTableName." SET ".$UsernameField."=".sl_quote_smart($newuser).", ".$EnabledField."=".sl_quote_smart($en).", ".$NameField."=".sl_quote_smart($nm).", ".$EmailField."=".sl_quote_smart($em).", ".$UsergroupsField.
    "=".sl_quote_smart($ug).", ".$Custom1Field."=".sl_quote_smart($cu1).", ".$Custom2Field."=".sl_quote_smart($cu2).", ".$Custom3Field."=".sl_quote_smart($cu3).", ".$Custom4Field.
    "=".sl_quote_smart($cu4).", ".$Custom5Field."=".sl_quote_smart($cu5).", ".$Custom6Field."=".sl_quote_smart($cu6).", ".$Custom7Field."=".sl_quote_smart($cu7).", ".$Custom8Field.
    "=".sl_quote_smart($cu8).", ".$Custom9Field."=".sl_quote_smart($cu9).", ".$Custom10Field."=".sl_quote_smart($cu10).", ".$Custom11Field."=".sl_quote_smart($cu11).", ".$Custom12Field.
    "=".sl_quote_smart($cu12).", ".$Custom13Field."=".sl_quote_smart($cu13).", ".$Custom14Field."=".sl_quote_smart($cu14).", ".$Custom15Field."=".sl_quote_smart($cu15).", ".$Custom16Field.
    "=".sl_quote_smart($cu16).", ".$Custom17Field."=".sl_quote_smart($cu17).", ".$Custom18Field."=".sl_quote_smart($cu18).", ".$Custom19Field."=".sl_quote_smart($cu19).", ".$Custom20Field.
    "=".sl_quote_smart($cu20).", ".$Custom21Field."=".sl_quote_smart($cu21).", ".$Custom22Field."=".sl_quote_smart($cu22).", ".$Custom23Field."=".sl_quote_smart($cu23).", ".$Custom24Field.
    "=".sl_quote_smart($cu24).", ".$Custom25Field."=".sl_quote_smart($cu25).", ".$Custom26Field."=".sl_quote_smart($cu26).", ".$Custom27Field."=".sl_quote_smart($cu27).", ".$Custom28Field.
    "=".sl_quote_smart($cu28).", ".$Custom29Field."=".sl_quote_smart($cu29).", ".$Custom30Field."=".sl_quote_smart($cu30).", ".$Custom31Field."=".sl_quote_smart($cu31).", ".$Custom32Field.
    "=".sl_quote_smart($cu32).", ".$Custom33Field."=".sl_quote_smart($cu33).", ".$Custom34Field."=".sl_quote_smart($cu34).", ".$Custom35Field."=".sl_quote_smart($cu35).", ".$Custom36Field.
    "=".sl_quote_smart($cu36).", ".$Custom37Field."=".sl_quote_smart($cu37).", ".$Custom38Field."=".sl_quote_smart($cu38).", ".$Custom39Field."=".sl_quote_smart($cu39).", ".$Custom40Field.
    "=".sl_quote_smart($cu40).", ".$Custom41Field."=".sl_quote_smart($cu41).", ".$Custom42Field."=".sl_quote_smart($cu42).", ".$Custom43Field."=".sl_quote_smart($cu43).", ".$Custom44Field.
    "=".sl_quote_smart($cu44).", ".$Custom45Field."=".sl_quote_smart($cu45).", ".$Custom46Field."=".sl_quote_smart($cu46).", ".$Custom47Field."=".sl_quote_smart($cu47).", ".$Custom48Field.
    "=".sl_quote_smart($cu48).", ".$Custom49Field."=".sl_quote_smart($cu49).", ".$Custom50Field."=".sl_quote_smart($cu50)." WHERE ".$UsernameField."=".sl_quote_smart($user);
  }
  if ($DemoMode)
    $mysql_result=true;
  else  
    $mysql_result=mysqli_query($mysql_link,$Query);
  if ($mysql_result==false)
  {
    $errors['user']=ADMINMSG_USERNAMEINUSE;
    returnError($data,$errors,ADMINMSG_FORMERROR);
    exit;
  }  
  if (($mysql_result) && ($user!=$newuser))
    sl_usernamechanged($user,$newuser);
  // If user account has been disabled or force logout requested then destroy existing session if we can
  if (($en=="No") || ($forcelogout=="1"))
  {
    $Query="SELECT ".$SessionField." FROM ".$DbTableName." WHERE ".$UsernameField."=".sl_quote_smart($user);
    $mysql_result=mysqli_query($mysql_link,$Query);
    if ($mysql_result!=false)
    {
      if ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
      {
        $usersess=$row[$SessionField];        
        $ThisSession=session_id();
        session_id($usersess);
        @session_destroy();
        if ($SessionName!="")
          session_name($SessionName);
        session_id($ThisSession);
        session_start();     
      }
    } 
  } 
  if (($sendemail!=$existingsendemail) && (!$DemoMode))
  {
    $query="UPDATE sl_adminconfig SET sendedituseremail=".sl_quote_smart($sendemail)." WHERE confignum=1";
    $mysql_result=mysqli_query($mysql_link,$query);
    $_SESSION['ses_ConfigReload']="reload";     
  }         
  if (($sendemail=="1") && ($template!=$ModifyUserEmail) && (!$DemoMode))
  {
    $query="UPDATE ".$DbConfigTableName." SET modifyuseremail=".sl_quote_smart($template)." WHERE confignum=1";
    $mysql_result=mysqli_query($mysql_link,$query);
    $ModifyUserEmail=$template;
    $_SESSION['ses_ConfigReload']="reload"; 
  }         

  if (($template!="") && ($sendemail=="1"))
  {
    if (sl_ReadEmailTemplate($template,$subject,$mailBody,$htmlformat))
    {
      $passtoemail=$pass;
      if ($pass=="")
        $passtoemail=$existingpass;
      if (!sl_SendEmail($em,$mailBody,$subject,$htmlformat,$newuser,$passtoemail,$nm,$em,$ug,$cu1,$cu2,$cu3,$cu4,$cu5,$cu6,$cu7,$cu8,$cu9,$cu10,
      $cu11,$cu12,$cu13,$cu14,$cu15,$cu16,$cu17,$cu18,$cu19,$cu20,$cu21,$cu22,$cu23,$cu24,$cu25,$cu26,$cu27,$cu28,$cu29,$cu30,
      $cu31,$cu32,$cu33,$cu34,$cu35,$cu36,$cu37,$cu38,$cu39,$cu40,$cu41,$cu42,$cu43,$cu44,$cu45,$cu46,$cu47,$cu48,$cu49,$cu50)==1)
        $emailsenderror=true;
    }
  }
  // Event point
  // Get auto increment id of user modified
  //  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." WHERE ".$UsernameField."=".sl_quote_smart($user));
  //  $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
  $paramdata['userid']=$userid;    
  $paramdata['oldusername']=$user;
  $paramdata['username']=$newuser;
  $paramdata['password']=$pass;
  $paramdata['enabled']=$en;
  $paramdata['name']=$nm;
  $paramdata['email']=$em;
  $paramdata['usergroups']=$ug;
  $paramdata['custom1']=$cu1;
  $paramdata['custom2']=$cu2;
  $paramdata['custom3']=$cu3;
  $paramdata['custom4']=$cu4;
  $paramdata['custom5']=$cu5;
  $paramdata['custom6']=$cu6;
  $paramdata['custom7']=$cu7;
  $paramdata['custom8']=$cu8;
  $paramdata['custom9']=$cu9;
  $paramdata['custom10']=$cu10;
  $paramdata['custom11']=$cu11;
  $paramdata['custom12']=$cu12;
  $paramdata['custom13']=$cu13;
  $paramdata['custom14']=$cu14;
  $paramdata['custom15']=$cu15;
  $paramdata['custom16']=$cu16;
  $paramdata['custom17']=$cu17;
  $paramdata['custom18']=$cu18;
  $paramdata['custom19']=$cu19;
  $paramdata['custom20']=$cu20;
  $paramdata['custom21']=$cu21;
  $paramdata['custom22']=$cu22;
  $paramdata['custom23']=$cu23;
  $paramdata['custom24']=$cu24;
  $paramdata['custom25']=$cu25;
  $paramdata['custom26']=$cu26;
  $paramdata['custom27']=$cu27;
  $paramdata['custom28']=$cu28;
  $paramdata['custom29']=$cu29;
  $paramdata['custom30']=$cu30;
  $paramdata['custom31']=$cu31;
  $paramdata['custom32']=$cu32;
  $paramdata['custom33']=$cu33;
  $paramdata['custom34']=$cu34;
  $paramdata['custom35']=$cu35;
  $paramdata['custom36']=$cu36;
  $paramdata['custom37']=$cu37;
  $paramdata['custom38']=$cu38;
  $paramdata['custom39']=$cu39;
  $paramdata['custom40']=$cu40;
  $paramdata['custom41']=$cu41;
  $paramdata['custom42']=$cu42;
  $paramdata['custom43']=$cu43;
  $paramdata['custom44']=$cu44;
  $paramdata['custom45']=$cu45;
  $paramdata['custom46']=$cu46;
  $paramdata['custom47']=$cu47;
  $paramdata['custom48']=$cu48;
  $paramdata['custom49']=$cu49;
  $paramdata['custom50']=$cu50;
  $paramdata['from']=1;                      
  // Call plugin event
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if (function_exists($slplugin_event_onModifyUser[$p]))
      call_user_func($slplugin_event_onModifyUser[$p],$slpluginid[$p],$paramdata);
  }
  // Call user event handler        
  if (function_exists("sl_onModifyUser"))
    sl_onModifyUser($paramdata);
  if (!$emailsenderror)
    returnSuccess($data,ADMINMSG_UPDATEDUSER);
  else
    returnSuccess($data,ADMINMSG_UPDATEDUSER.". ".ADMINMSG_EMAILNOTSENT);

  function returnSuccess($data,$msg)
  {
    $data['success'] = true;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

  function returnError($data,$errors,$msg)
  {
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

?>
