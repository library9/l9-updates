<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Get current values
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    die(ADMINMSG_MYSQLERROR);
  }
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_logintemplate WHERE id=1");
  if ($mysql_result===false)
  {
    die(ADMINMSG_MYSQLERROR);
  }
  if ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
  {
    $newbackgroundcolor=$row['backcolor'];    
    $newbackgroundimage=$row['backimage'];    
    $newbackgroundimagerp=$row['backimagerp']; 
    $newloginfont=$row['mainfont']; 
    $newloginboxfilltype=$row['boxcolortype'];    
    $newloginboxcolorfrom=$row['boxcolorfrom'];    
    $newloginboxcolorto=$row['boxcolorto'];    
    $newloginboxshape=$row['boxradius'];    
    $newloginboxshadow=$row['boxshadow'];    
    $newtitletext=$row['title'];    
    $newtitlecolor=$row['titlecolor'];    
    $newtitlesize=$row['titlesize'];    
    $newtitlealign=$row['titlealign'];    
    $newtitlefont=$row['titlefont'];    
    $newmessagecolor=$row['msgcolor'];    
    $newmessagesize=$row['msgsize'];    
    $newmessagealign=$row['msgalign'];    
    $newusernamelabel=$row['username'];    
    $newpasswordlabel=$row['password'];    
    $newcaptchalabel=$row['captcha'];    
    $newrememberlabel=$row['remember'];    
    $newautologinlabel=$row['autologin'];    
    $newlabelcolor=$row['labelcolor'];    
    $newlabelsize=$row['labelsize'];    
    $newinputtextcolor=$row['inputcolor'];    
    $newinputtextsize=$row['inputsize'];    
    $newinputbackcolor=$row['inputbackcolor'];
    $newinputshowicons=$row['showicons'];
    $newlogintext=$row['btnlbltext'];    
    $newlogintextfont=$row['btnlblfont'];    
    $newlogintextcolor=$row['btnlblcolor'];    
    $newlogintextsize=$row['btnlblsize'];    
    $newlogintextstyle=$row['btnlblstyle'];    
    $newloginbuttonfilltype=$row['btncolortype'];    
    $newloginbuttoncolorfrom=$row['btncolorfrom'];    
    $newloginbuttoncolorto=$row['btncolorto'];    
    $newloginbuttonshape=$row['btnradius'];    
    $newbtnborderstyle=$row['btnborderstyle'];    
    $newbtnbordercolor=$row['btnbordercolor'];    
    $newbtnbordersize=$row['btnbordersize'];    
    $newforgottext=$row['forgottxt'];    
    $newforgotcolor=$row['forgotcolor'];    
    $newforgotsize=$row['forgotsize'];    
    $newsignuptext=$row['signuptext'];    
    $newsignupurl=$row['signupurl'];    
    $newsignupcolor=$row['signupcolor'];    
    $newsignupsize=$row['signupsize'];    
    $newsignupalign=$row['signupalign'];    
  }
  else
  {
    die(ADMINMSG_MYSQLERROR);
  }


?>
<!DOCTYPE html>

<html>
<head>
    <?php
    $pagename="editlogin";
    include("adminhead.php");
    ?>
    <title><?php echo ADMINMENU_DEFAULTLOGINSTYLE; ?></title>
    <link rel="stylesheet" href="jquery-minicolors-master/jquery.minicolors.css" type="text/css">
    <link rel="stylesheet" href="editlogin.css" type="text/css">


 <!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
    <!--[if lte IE 6]>
  <style type="text/css">
    div#loginbox {
    width: 420px;
    }
<![endif]-->
</head>
<?php include("adminthemeheader.php"); ?>
<!-- Content Wrapper. Contains page content -->

<body>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo ADMINMENU_DEFAULTLOGINSTYLE; ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><?php echo ADMINMENU_DEFAULTLOGINSTYLE; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Your Page Content Here -->

          <div class="row">


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
              <div class="box" id="previewbox">
             <div id="togglefixed" class="hidden-xs hidden-sm hidden-md"><span id="toggleicon" class="glyphicon glyphicon-pushpin" rel="tooltip" title="<?php echo ADMINET_TOGFIX; ?>" onclick="toggleFixed();"></span></div>
                <div class="box-body" id="loginpreview" style="cursor: pointer;" onclick="previewclick('newbackgroundcolor','collapse1')">
                  <!-- Simulate the login form in a div here -->
                  <!-- <div id="loginpreview" style="cursor: pointer;" onclick="previewclick('newbackgroundcolor')"> -->
                    <div id="loginbox" class="gradient" style="cursor: pointer;" onclick="previewclick('newloginboxcolorfrom','collapse2')">
                      <div id="innerbox">
                        <h1 id="title" style="cursor: pointer;" onclick="previewclick('newtitletext','collapse2')">Login</h1>

                        <p id="message" style="cursor: pointer;" onclick="previewclick('newmessagesize','collapse3')">This is the message area</p>

                        <label id="usernamelabel" style="cursor: pointer;" onclick="previewclick('newusernamelabel','collapse3')">Username</label>
                        <input type="text" name="username" id="username" value="username" placeholder="username" maxlength="50" tabindex="1"  style="cursor: pointer;" onclick="previewclick('newusernamelabel','collapse3')" autocomplete="off">

                        <label id="passwordlabel" style="cursor: pointer;" onclick="previewclick('newpasswordlabel','collapse3')">Password</label>
                        <input type="password" name="password" id="password" value="password" placeholder="password" maxlength="50" tabindex="2" style="cursor: pointer;" onclick="previewclick('newpasswordlabel','collapse3')"  autocomplete="off">

                        <?php if ($TuringLogin==1) { ?>
                        <label id="captchalabel" style="cursor: pointer;" onclick="previewclick('newcaptchalabel','collapse3')">Captcha</label>
                        <input type="text" name="turing" id="turing" value="" placeholder="captcha" maxlength="5" tabindex="3" style="cursor: pointer;" onclick="previewclick('newcaptchalabel','collapse3')">
                        <?php } ?>

                        <?php if ($CookieLogin==1) { ?>
                        <label id="rememberlabel" class="labelcb" style="cursor: pointer;" onclick="previewclick('newrememberlabel','collapse3')">Remember Me</label>
                        <input type="checkbox" name="remember" id="remember"  value="1" style="cursor: pointer;" onclick="previewclick('newrememberlabel','collapse3')">
                        <?php } ?>

                        <?php if ($CookieLogin==2) { ?>
                        <label id="rememberlabel" class="labelcb" for="newautologinlabel" style="cursor: pointer;" onclick="previewclick('newautologinlabel','collapse3')">Auto Login</label>
                        <input type="checkbox" name="remember" id="remember"  value="2" onclick="previewclick('newautologinlabel','collapse3')">
                        <?php } ?>

                        <div style="display: block;clear: both;margin-top: 15px; line-height:10px;padding:0;margin:0;"><br></div>
                        <input type="button" class="myButton" id="myButton" value="Login" tabindex="5" title="Login" style="cursor: pointer;" onclick="previewclick('newlogintext','collapse4')">
                        <a id="forgot" href="javaScript:void(0);" title="Forgot your password? Enter username or email &amp; click link" style="cursor: pointer;" onclick="previewclick('newforgottext','collapse5')">Forgot Password</a>
                        <p id="signup"><a id="signuplink" href="javaScript:void(0);" style="cursor: pointer;" onclick="previewclick('newsignuptext','collapse6')">Not signed up?</a></p>
                      </div>
                    </div>
                  <!-- </div> -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

              <form id="editloginform" role="form" class="form-horizontal" method="post">
              <input type="hidden" name="slcsrf" id="slcsrf" value="<?php echo $slcsrftoken; ?>">
              <div class="box">
                <div class="box-body">

                  <div class="panel-group" id="accordion" role="tablist">

                
                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse2">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse2" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_LGBOXSTY; ?></a>
                        </h4>
                      </div>
                      <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="form-group">
                            <label class="col-xs-12" for="newloginboxfilltype"><?php echo ADMINET_LGBOXCOL; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newloginboxfilltypediv">
                                <select id="newloginboxfilltype" name="newloginboxfilltype" class="form-control selectpicker" onchange="filltype('newloginboxfilltype','newloginboxcolortodiv'); updatePreview()">
                                <option value="solid" <?php if ($newloginboxfilltype=="solid") print "selected=\"selected\"";?>><?php echo ADMINET_SOLID; ?></option>
                                <option value="gradient" <?php if ($newloginboxfilltype=="gradient") print "selected=\"selected\"";?>><?php echo ADMINET_GRADIENT; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newloginboxcolorfromdiv">
                                <input type="text" id="newloginboxcolorfrom" name="newloginboxcolorfrom" class="form-control colorbox" maxlength="7" value="<?php echo $newloginboxcolorfrom; ?>" onchange="updatePreview()">
                            </div> 
                            <div class="col-xs-6 col-sm-5 col-md-4 col-lg-5" id="newloginboxcolortodiv">
                              <div class="input-group">
                                <input type="text" id="newloginboxcolorto" name="newloginboxcolorto" class="form-control colorbox" maxlength="7" value="<?php echo $newloginboxcolorto; ?>" onchange="updatePreview()">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button" onclick="gradientreverse('newloginboxcolorfrom','newloginboxcolorto')"><span class="glyphicon glyphicon-transfer actionicon" title="reverse"></span></button>
                                </span>
                              </div>                                                        
                            </div>                           
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newloginboxshape"><?php echo ADMINET_LGBOXCOR; ?></label>
                             <div class="col-xs-12 col-sm-6 col-md-3 col-lg-5" id="newloginboxshapediv">
                                <select id="newloginboxshape" name="newloginboxshape" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="0" <?php if ($newloginboxshape=="0") print "selected=\"selected\"";?>><?php echo ADMINET_SQUARE; ?></option>
                                <option value="10" <?php if ($newloginboxshape=="10") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 10px</option>
                                <option value="20" <?php if ($newloginboxshape=="20") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 20px</option>
                                <option value="30" <?php if ($newloginboxshape=="30") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 30px</option>
                                <option value="40" <?php if ($newloginboxshape=="40") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 40px</option>
                                <option value="50" <?php if ($newloginboxshape=="50") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 50px</option>
                                </select>  
                            </div> 
                             <div class="col-xs-12 col-sm-6 col-md-3 col-lg-5" id="newloginboxshadowdiv">
                                <select id="newloginboxshadow" name="newloginboxshadow" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="0" <?php if ($newloginboxshadow=="0") print "selected=\"selected\"";?>><?php echo ADMINET_NOSHADOW; ?></option>
                                <option value="6" <?php if ($newloginboxshadow=="6") print "selected=\"selected\"";?>><?php echo ADMINET_SHADOW; ?></option>
                                </select>  
                            </div> 
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newtitletext"><?php echo ADMINET_TITLE; ?></label>
                              <div class="col-xs-12" id="newtitletextdiv">
                                  <input type="text" id="newtitletext" name="newtitletext" class="form-control" maxlength="100" value="<?php echo $newtitletext; ?>" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newtitlealign"><?php echo ADMINET_TITLESTY; ?></label>
                             <div class="col-xs-12 col-sm-4 col-md-3 col-lg-4" id="newtitlealigndiv">
                                <select id="newtitlealign" name="newtitlealign" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="left" <?php if ($newtitlealign=="left") print "selected=\"selected\"";?>><?php echo ADMINET_LEFT; ?></option>
                                <option value="center" <?php if ($newtitlealign=="center") print "selected=\"selected\"";?>><?php echo ADMINET_CENTER; ?></option>
                                <option value="right" <?php if ($newtitlealign=="right") print "selected=\"selected\"";?>><?php echo ADMINET_RIGHT; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newtitlecolordiv">
                                <input type="text" id="newtitlecolor" name="newtitlecolor" class="form-control colorbox" maxlength="7" value="<?php echo $newtitlecolor; ?>" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newtitlesizediv">
                                <div class="input-group">
                                  <input type="number" id="newtitlesize" name="newtitlesize" class="form-control" maxlength="2" min="1" max="99" value="<?php echo $newtitlesize; ?>" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newtitlefont"><?php echo ADMINET_TITLEFONT; ?></label>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newtitlefontdiv">
                                <select id="newtitlefont" name="newtitlefont" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="Arial, Helvetica, sans-serif" <?php if ($newtitlefont=="Arial, Helvetica, sans-serif") print "selected=\"selected\"";?>>Arial</option>
                                <option value="Arial Narrow, sans-serif" <?php if ($newtitlefont=="Arial Narrow, sans-serif") print "selected=\"selected\"";?>>Arial Narrow</option>
                                <option value="Century Gothic, sans-serif" <?php if ($newtitlefont=="Century Gothic, sans-serif") print "selected=\"selected\"";?>>Century Gothic</option>
                                <option value="Copperplate Gothic Light, sans-serif" <?php if ($newtitlefont=="Copperplate Gothic Light, sans-serif") print "selected=\"selected\"";?>>Copperplate Gothic Light</option>
                                <option value="Courier New, Courier, monospace" <?php if ($newtitlefont=="Courier New, Courier, monospace") print "selected=\"selected\"";?>>Courier New</option>
                                <option value="Garamond, Serif" <?php if ($newtitlefont=="Garamond, Serif") print "selected=\"selected\"";?>>Garamond</option>
                                <option value="Georgia, Serif" <?php if ($newtitlefont=="Georgia, Serif") print "selected=\"selected\"";?>>Georgia</option>
                                <option value="Gill Sans MT, sans-serif" <?php if ($newtitlefont=="Gill Sans MT, sans-serif") print "selected=\"selected\"";?>>Gill Sans MT</option>
                                <option value="Impact, Charcoal, sans-serif" <?php if ($newtitlefont=="Impact, Charcoal, sans-serif") print "selected=\"selected\"";?>>Impact</option>
                                <option value="Lucida Console, Monaco, monospace" <?php if ($newtitlefont=="Lucida Console, Monaco, monospace") print "selected=\"selected\"";?>>Lucida Console</option>
                                <option value="Lucida Sans Unicode, Lucida Grande, sans-serif" <?php if ($newtitlefont=="Lucida Sans Unicode, Lucida Grande, sans-serif") print "selected=\"selected\"";?>>Lucida Sans Unicode</option>
                                <option value="Palatino Linotype, Book Antiqua, Palatino, serif" <?php if ($newtitlefont=="Palatino Linotype, Book Antiqua, Palatino, serif") print "selected=\"selected\"";?>>Palatino Linotype</option>
                                <option value="Tahoma, Geneva, sans-serif" <?php if ($newtitlefont=="Tahoma, Geneva, sans-serif") print "selected=\"selected\"";?>>Tahoma</option>
                                <option value="Times New Roman, Times, serif" <?php if ($newtitlefont=="Times New Roman, Times, serif") print "selected=\"selected\"";?>>Times New Roman</option>
                                <option value="Verdana, Geneva, sans-serif" <?php if ($newtitlefont=="Verdana, Geneva, sans-serif") print "selected=\"selected\"";?>>Verdana</option>
                                <option value="Trebuchet MS, Helvetica, sans-serif" <?php if ($newtitlefont=="Trebuchet MS, Helvetica, sans-serif") print "selected=\"selected\"";?>>Trebuchet MS</option>
                                </select>  
                            </div>    
                          </div>
                        </div>
                      </div>
                    </div>  

                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse3">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse3" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_INPFLDS; ?></a>
                        </h4>
                      </div>
                      <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="form-group">
                              <label class="col-xs-12" for="newusernamelabel"><?php echo ADMINET_USERLBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newusernamelabeldiv">
                                  <input type="text" id="newusernamelabel" name="newusernamelabel" class="form-control" maxlength="50" value="<?php echo $newusernamelabel; ?>" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newpasswordlabel"><?php echo ADMINET_PASSLBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newpasswordlabeldiv">
                                  <input type="text" id="newpasswordlabel" name="newpasswordlabel" class="form-control" maxlength="50" value="<?php echo $newpasswordlabel; ?>" onchange="updatePreview()">
                              </div>
                          </div>
                          <?php if ($TuringLogin==1) { ?>
                          <div class="form-group">
                              <label class="col-xs-12" for="newcaptchalabel"><?php echo ADMINET_CAPTCHALBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newcaptchalabeldiv">
                                  <input type="text" id="newcaptchalabel" name="newcaptchalabel" class="form-control" maxlength="50" value="<?php echo $newcaptchalabel; ?>" onchange="updatePreview()">
                              </div>
                          </div>
                          <?php } ?>
                          <?php if ($CookieLogin==1) { ?>
                          <div class="form-group">
                              <label class="col-xs-12" for="newrememberlabel"><?php echo ADMINET_REMLBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newrememberlabeldiv">
                                  <input type="text" id="newrememberlabel" name="newrememberlabel" class="form-control" maxlength="50" value="<?php echo $newrememberlabel; ?>" onchange="updatePreview()">
                              </div>
                          </div>
                          <?php } ?>
                          <?php if ($CookieLogin==2) { ?>
                          <div class="form-group">
                              <label class="col-xs-12" for="newautologinlabel"><?php echo ADMINET_AUTOLBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newautologinlabeldiv">
                                  <input type="text" id="newautologinlabel" name="newautologinlabel" class="form-control" maxlength="50" value="<?php echo $newautologinlabel; ?>" onchange="updatePreview()">
                              </div>
                          </div>
                          <?php } ?>
                          <div class="form-group">
                            <label class="col-xs-12" for="newloginfont"><?php echo ADMINET_FONTTYP; ?></label>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newloginfontdiv">
                                <select id="newloginfont" name="newloginfont" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="Arial, Helvetica, sans-serif" <?php if ($newloginfont=="Arial, Helvetica, sans-serif") print "selected=\"selected\"";?>>Arial</option>
                                <option value="Times New Roman, Times, serif" <?php if ($newloginfont=="Times New Roman, Times, serif") print "selected=\"selected\"";?>>Times New Roman</option>
                                <option value="Verdana, Geneva, sans-serif" <?php if ($newloginfont=="Verdana, Geneva, sans-serif") print "selected=\"selected\"";?>>Verdana</option>
                                <option value="Impact, Charcoal, sans-serif" <?php if ($newloginfont=="Impact, Charcoal, sans-serif") print "selected=\"selected\"";?>>Impact</option>
                                <option value="Palatino Linotype, Book Antiqua, Palatino, serif" <?php if ($newloginfont=="Palatino Linotype, Book Antiqua, Palatino, serif") print "selected=\"selected\"";?>>Palatino Linotype</option>
                                <option value="Tahoma, Geneva, sans-serif" <?php if ($newloginfont=="Tahoma, Geneva, sans-serif") print "selected=\"selected\"";?>>Tahoma</option>
                                <option value="Century Gothic, sans-serif" <?php if ($newloginfont=="Century Gothic, sans-serif") print "selected=\"selected\"";?>>Century Gothic</option>
                                <option value="Lucida Sans Unicode, Lucida Grande, sans-serif" <?php if ($newloginfont=="Lucida Sans Unicode, Lucida Grande, sans-serif") print "selected=\"selected\"";?>>Lucida Sans Unicode</option>
                                <option value="Arial Narrow, sans-serif" <?php if ($newloginfont=="Arial Narrow, sans-serif") print "selected=\"selected\"";?>>Arial Narrow</option>
                                <option value="Copperplate Gothic Light, sans-serif" <?php if ($newloginfont=="Copperplate Gothic Light, sans-serif") print "selected=\"selected\"";?>>Copperplate Gothic Light</option>
                                <option value="Lucida Console, Monaco, monospace" <?php if ($newloginfont=="Lucida Console, Monaco, monospace") print "selected=\"selected\"";?>>Lucida Console</option>
                                <option value="Gill Sans MT, sans-serif" <?php if ($newloginfont=="Gill Sans MT, sans-serif") print "selected=\"selected\"";?>>Gill Sans MT</option>
                                <option value="Trebuchet MS, Helvetica, sans-serif" <?php if ($newloginfont=="Trebuchet MS, Helvetica, sans-serif") print "selected=\"selected\"";?>>Trebuchet MS</option>
                                <option value="Courier New, Courier, monospace" <?php if ($newloginfont=="Courier New, Courier, monospace") print "selected=\"selected\"";?>>Courier New</option>
                                <option value="Georgia, Serif" <?php if ($newloginfont=="Georgia, Serif") print "selected=\"selected\"";?>>Georgia</option>
                                <option value="Garamond, Serif" <?php if ($newloginfont=="Garamond, Serif") print "selected=\"selected\"";?>>Garamond</option>
                                </select>  
                            </div>    
                          </div>                      
                          <div class="form-group">
                            <label class="col-xs-12" for="newlabelcolor"><?php echo ADMINET_INPLBLSTY; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newlabelcolordiv">
                                <input type="text" id="newlabelcolor" name="newlabelcolor" class="form-control colorbox" maxlength="7" value="<?php echo $newlabelcolor; ?>" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newlabelsizediv">
                                <div class="input-group">
                                  <input type="number" id="newlabelsize" name="newlabelsize" class="form-control" maxlength="2" min="1" max="99" value="<?php echo $newlabelsize; ?>" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newinputtextcolor"><?php echo ADMINET_INPTXTSTY; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newinputtextcolordiv">
                                <input type="text" id="newinputtextcolor" name="newinputtextcolor" class="form-control colorbox" maxlength="7" value="<?php echo $newinputtextcolor; ?>" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newinputtextsizediv">
                                <div class="input-group">
                                  <input type="number" id="newinputtextsize" name="newinputtextsize" class="form-control" maxlength="2" min="1" max="99" value="<?php echo $newinputtextsize; ?>" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div>
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newinputbackcolor"><?php echo ADMINET_INPBCKCOL; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newinputbackcolordiv">
                              <input type="text" id="newinputbackcolor" name="newinputbackcolor" class="form-control colorbox" maxlength="7" value="<?php echo $newinputbackcolor; ?>" onchange="updatePreview()">
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newinputshowiconsdiv">
                              <select id="newinputshowicons" name="newinputshowicons" class="form-control selectpicker" onchange="updatePreview()">
                              <option value="0" <?php if ($newinputshowicons==0) print "selected=\"selected\"";?>>hide icons</option>
                              <option value="1" <?php if ($newinputshowicons==1) print "selected=\"selected\"";?>>show icons</option>
                              </select>  
                            </div> 
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newtitlealign"><?php echo ADMINET_MSGSTY; ?></label>
                             <div class="col-xs-12 col-sm-4 col-md-3 col-lg-4" id="newmessagealigndiv" onchange="updatePreview()">
                                <select id="newmessagealign" name="newmessagealign" class="form-control selectpicker">
                                <option value="left" <?php if ($newmessagealign=="left") print "selected=\"selected\"";?>><?php echo ADMINET_LEFT; ?></option>
                                <option value="center" <?php if ($newmessagealign=="center") print "selected=\"selected\"";?>><?php echo ADMINET_CENTER; ?></option>
                                <option value="right" <?php if ($newmessagealign=="right") print "selected=\"selected\"";?>><?php echo ADMINET_RIGHT; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newmessagecolordiv">
                                <input type="text" id="newmessagecolor" name="newmessagecolor" class="form-control colorbox" maxlength="7" value="<?php echo $newmessagecolor; ?>" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newmessagesizediv">
                                <div class="input-group">
                                  <input type="number" id="newmessagesize" name="newmessagesize" class="form-control" maxlength="2" min="1" max="99" value="<?php echo $newmessagesize; ?>" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>  

                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse4">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse4" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_LOGINBTN; ?></a>
                        </h4>
                      </div>
                      <div id="collapse4" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="form-group">
                              <label class="col-xs-12" for="newlogintext"><?php echo ADMINET_BTNLBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newlogintextdiv">
                                  <input type="text" id="newlogintext" name="newlogintext" class="form-control" maxlength="50" value="<?php echo $newlogintext; ?>" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newlogintextfont"><?php echo ADMINET_BTNFONT; ?></label>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newlogintextfontdiv">
                                <select id="newlogintextfont" name="newlogintextfont" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="Arial, Helvetica, sans-serif" <?php if ($newlogintextfont=="Arial, Helvetica, sans-serif") print "selected=\"selected\"";?>>Arial</option>
                                <option value="Times New Roman, Times, serif" <?php if ($newlogintextfont=="Times New Roman, Times, serif") print "selected=\"selected\"";?>>Times New Roman</option>
                                <option value="Verdana, Geneva, sans-serif" <?php if ($newlogintextfont=="Verdana, Geneva, sans-serif") print "selected=\"selected\"";?>>Verdana</option>
                                <option value="Impact, Charcoal, sans-serif" <?php if ($newlogintextfont=="Impact, Charcoal, sans-serif") print "selected=\"selected\"";?>>Impact</option>
                                <option value="Palatino Linotype, Book Antiqua, Palatino, serif" <?php if ($newlogintextfont=="Palatino Linotype, Book Antiqua, Palatino, serif") print "selected=\"selected\"";?>>Palatino Linotype</option>
                                <option value="Tahoma, Geneva, sans-serif" <?php if ($newlogintextfont=="Tahoma, Geneva, sans-serif") print "selected=\"selected\"";?>>Tahoma</option>
                                <option value="Century Gothic, sans-serif" <?php if ($newlogintextfont=="Century Gothic, sans-serif") print "selected=\"selected\"";?>>Century Gothic</option>
                                <option value="Lucida Sans Unicode, Lucida Grande, sans-serif" <?php if ($newlogintextfont=="Lucida Sans Unicode, Lucida Grande, sans-serif") print "selected=\"selected\"";?>>Lucida Sans Unicode</option>
                                <option value="Arial Narrow, sans-serif" <?php if ($newlogintextfont=="Arial Narrow, sans-serif") print "selected=\"selected\"";?>>Arial Narrow</option>
                                <option value="Copperplate Gothic Light, sans-serif" <?php if ($newlogintextfont=="Copperplate Gothic Light, sans-serif") print "selected=\"selected\"";?>>Copperplate Gothic Light</option>
                                <option value="Lucida Console, Monaco, monospace" <?php if ($newlogintextfont=="Lucida Console, Monaco, monospace") print "selected=\"selected\"";?>>Lucida Console</option>
                                <option value="Gill Sans MT, sans-serif" <?php if ($newlogintextfont=="Gill Sans MT, sans-serif") print "selected=\"selected\"";?>>Gill Sans MT</option>
                                <option value="Trebuchet MS, Helvetica, sans-serif" <?php if ($newlogintextfont=="Trebuchet MS, Helvetica, sans-serif") print "selected=\"selected\"";?>>Trebuchet MS</option>
                                <option value="Courier New, Courier, monospace" <?php if ($newlogintextfont=="Courier New, Courier, monospace") print "selected=\"selected\"";?>>Courier New</option>
                                <option value="Georgia, Serif" <?php if ($newlogintextfont=="Georgia, Serif") print "selected=\"selected\"";?>>Georgia</option>
                                <option value="Garamond, Serif" <?php if ($newlogintextfont=="Garamond, Serif") print "selected=\"selected\"";?>>Garamond</option>
                                </select>  
                            </div>    
                          </div>                      
                          <div class="form-group">
                            <label class="col-xs-12" for="newlogintextstyle"><?php echo ADMINET_BTNLBLSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newlogintextstylediv">
                                <select id="newlogintextstyle" name="newlogintextstyle" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="normal normal normal" <?php if ($newlogintextstyle=="normal normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold" <?php if ($newlogintextstyle=="normal normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal" <?php if ($newlogintextstyle=="italic normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold" <?php if ($newlogintextstyle=="italic normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newlogintextcolordiv">
                                <input type="text" id="newlogintextcolor" name="newlogintextcolor" class="form-control colorbox" maxlength="7" value="<?php echo $newlogintextcolor; ?>" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newlogintextsizediv">
                                <div class="input-group">
                                  <input type="number" id="newlogintextsize" name="newlogintextsize" class="form-control" maxlength="2" min="1" max="99" value="<?php echo $newlogintextsize; ?>" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newloginbuttonfilltype"><?php echo ADMINET_BTNCOL; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newloginbuttonfilltypediv">
                                <select id="newloginbuttonfilltype" name="newloginbuttonfilltype" class="form-control selectpicker" onchange="filltype('newloginbuttonfilltype','newloginbuttoncolortodiv'); updatePreview()">
                                <option value="solid" <?php if ($newloginbuttonfilltype=="solid") print "selected=\"selected\"";?>><?php echo ADMINET_SOLID; ?></option>
                                <option value="gradient" <?php if ($newloginbuttonfilltype=="gradient") print "selected=\"selected\"";?>><?php echo ADMINET_GRADIENT; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newloginbuttoncolorfromdiv">
                                <input type="text" id="newloginbuttoncolorfrom" name="newloginbuttoncolorfrom" class="form-control colorbox" maxlength="7" value="<?php echo $newloginbuttoncolorfrom; ?>" onchange="updatePreview()">
                            </div> 
                            <div class="col-xs-6 col-sm-5 col-md-4 col-lg-5" id="newloginbuttoncolortodiv">
                              <div class="input-group">
                                <input type="text" id="newloginbuttoncolorto" name="newloginbuttoncolorto" class="form-control colorbox" maxlength="7" value="<?php echo $newloginbuttoncolorto; ?>" onchange="updatePreview()">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button" id="loginbuttoncolorreverse" onclick="gradientreverse('newloginbuttoncolorfrom','newloginbuttoncolorto')"><span class="glyphicon glyphicon-transfer actionicon" title="reverse"></span></button>
                                </span>
                              </div>                                                        
                            </div>                           
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtnborderstyle"><?php echo ADMINET_BTNBRDSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtnborderstylediv">
                                <select id="newbtnborderstyle" name="newbtnborderstyle" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="solid" <?php if ($newbtnborderstyle=="solid") print "selected=\"selected\"";?>><?php echo ADMINET_SOLID; ?></option>
                                <option value="dotted" <?php if ($newbtnborderstyle=="dotted") print "selected=\"selected\"";?>><?php echo ADMINET_DOTTED; ?></option>
                                <option value="dashed" <?php if ($newbtnborderstyle=="dashed") print "selected=\"selected\"";?>><?php echo ADMINET_DASHED; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbtnbordercolordiv">
                                <input type="text" id="newbtnbordercolor" name="newbtnbordercolor" class="form-control colorbox" maxlength="7" value="<?php echo $newbtnbordercolor; ?>" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newbtnbordersizediv">
                                <div class="input-group">
                                  <input type="number" id="newbtnbordersize" name="newbtnbordersize" class="form-control" maxlength="2" min="0" max="99" value="<?php echo $newbtnbordersize; ?>" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newloginbuttonshape"><?php echo ADMINET_BTNSHAPE; ?></label>
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newloginbuttonshapediv">
                                <select id="newloginbuttonshape" name="newloginbuttonshape" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="0" <?php if ($newloginbuttonshape=="0") print "selected=\"selected\"";?>><?php echo ADMINET_SQUARE; ?></option>
                                <option value="5" <?php if ($newloginbuttonshape=="5") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 5px</option>
                                <option value="10" <?php if ($newloginbuttonshape=="10") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 10px</option>
                                <option value="15" <?php if ($newloginbuttonshape=="15") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 15px</option>
                                <option value="20" <?php if ($newloginbuttonshape=="20") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 20px</option>
                                </select>  
                            </div>    
                          </div>                      
                        </div>
                      </div>
                    </div>  

                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse5">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse5" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_FRGLNK; ?></a>
                        </h4>
                      </div>
                      <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="form-group">
                              <label class="col-xs-12" for="newforgottext"><?php echo ADMINET_FRGTXT; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newforgottextdiv">
                                  <input type="text" id="newforgottext" name="newforgottext" class="form-control" maxlength="50" value="<?php echo $newforgottext; ?>" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newlabelcolor"><?php echo ADMINET_FRGSTY; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newforgotcolordiv">
                                <input type="text" id="newforgotcolor" name="newforgotcolor" class="form-control colorbox" maxlength="7" value="<?php echo $newforgotcolor; ?>" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newforgotsizediv">
                                <div class="input-group">
                                  <input type="number" id="newforgotsize" name="newforgotsize" class="form-control" maxlength="2" min="1" max="99" value="<?php echo $newforgotsize; ?>" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>  

                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse6">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse6" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_SIGNLNK; ?></a>
                        </h4>
                      </div>
                      <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="form-group">
                              <label class="col-xs-12" for="newforgottext"><?php echo ADMINET_SIGNTXT; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newsignuptextdiv">
                                  <input type="text" id="newsignuptext" name="newsignuptext" class="form-control" maxlength="100" value="<?php echo $newsignuptext; ?>" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newforgottext"><?php echo ADMINET_SIGNURL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newsignupurldiv">
                                  <input type="text" id="newsignupurl" name="newsignupurl" class="form-control" maxlength="100" value="<?php echo $newsignupurl; ?>" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newsignupalign"><?php echo ADMINET_SIGNTXT; ?></label>
                             <div class="col-xs-12 col-sm-4 col-md-3 col-lg-4" id="newsignupaligndiv">
                                <select id="newsignupalign" name="newsignupalign" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="left" <?php if ($newsignupalign=="left") print "selected=\"selected\"";?>><?php echo ADMINET_LEFT; ?></option>
                                <option value="center" <?php if ($newsignupalign=="center") print "selected=\"selected\"";?>><?php echo ADMINET_CENTER; ?></option>
                                <option value="right" <?php if ($newsignupalign=="right") print "selected=\"selected\"";?>><?php echo ADMINET_RIGHT; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newsignupcolordiv">
                                <input type="text" id="newsignupcolor" name="newsignupcolor" class="form-control colorbox" maxlength="7" value="<?php echo $newsignupcolor; ?>" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newsignupsizediv">
                                <div class="input-group">
                                  <input type="number" id="newsignupsize" name="newsignupsize" class="form-control" maxlength="2" min="1" max="99" value="<?php echo $newsignupsize; ?>" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse1">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse1" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_PGBACK; ?></a>
                        </h4>
                      </div>
                      <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="form-group">
                              <label class="col-xs-12" for="newbackgroundcolor"><?php echo ADMINET_PGBACKCOL; ?></label>
                              <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbackgroundcolordiv">
                                  <input type="text" id="newbackgroundcolor" name="newbackgroundcolor" class="form-control colorbox" maxlength="7" value="<?php echo $newbackgroundcolor; ?>" onchange="updatePreview();">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newbackgroundimage"><?php echo ADMINET_PGBACKURL; ?></label>
                              <div class="col-xs-12" id="newbackgroundimagediv">
                                  <input type="text" id="newbackgroundimage" name="newbackgroundimage" class="form-control" maxlength="100" value="<?php echo $newbackgroundimage; ?>" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbackgroundimagerp"><?php echo ADMINET_PGBACKIMG; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbackgroundimagerpdiv">
                                <select id="newbackgroundimagerp" name="newbackgroundimagerp" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="no-repeat" <?php if ($newbackgroundimagerp=="no-repeat") print "selected=\"selected\"";?>>no-repeat</option>
                                <option value="repeat" <?php if ($newbackgroundimagerp=="repeat") print "selected=\"selected\"";?>>repeat</option>
                                <option value="repeat-x" <?php if ($newbackgroundimagerp=="repeat-x") print "selected=\"selected\"";?>>repeat-x</option>
                                <option value="repeat-y" <?php if ($newbackgroundimagerp=="repeat-y") print "selected=\"selected\"";?>>repeat-y</option>
                                <option value="cover" <?php if ($newbackgroundimagerp=="cover") print "selected=\"selected\"";?>>cover</option>
                                <option value="100%" <?php if ($newbackgroundimagerp=="100%") print "selected=\"selected\"";?>>100%</option>
                                <option value="100% 100%" <?php if ($newbackgroundimagerp=="100% 100%") print "selected=\"selected\"";?>>100% 100%</option>
                                </select>  
                            </div>    
                          </div>
                        </div>
                      </div>
                    </div>  



                  </div> <!-- panel-group -->

                </div> <!-- box body -->
              </div> <!-- box --> 


                    <div class="form-group">
                      <div class="col-xs-12">
 
                        <div class="btn-toolbar">
                            <button type="submit" id="submit" class="btn btn-primary pull-left"><?php echo ADMINBUTTON_SAVECHANGES; ?></button>
                            <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                            <button type="button" id="reset" class="btn btn-default pull-right" onclick="revertToDefaults();"><?php echo ADMINBUTTON_RESETDEFAULTS ?></button>
                        </div> 
                      </div>    
                    </div>

              <div id="resulteditlogin"></div>



              </form>




           </div> <!-- col -->


          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include("adminthemefooter.php"); ?>
    <script src="jquery-minicolors-master/jquery.minicolors.js" type="text/javascript"></script>
    <script src="editlogin.js" type="text/javascript">
</script>
</body>
</html>
