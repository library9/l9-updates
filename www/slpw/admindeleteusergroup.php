<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value  
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError(ADMINMSG_CSRFFAILED);
    exit;      
  }
  $groupid=$_POST['groupid'];
  $groupname=$_POST['groupname'];
  if (($groupname=="ADMIN") || ($groupname=="ALL"))
  {
    returnError(ADMINMU_CANTDELETE);
    exit;      
  }
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError(ADMINMSG_MYSQLERROR);
    exit;
  }
  if (!is_numeric($groupid))
  {
    returnError(ADMINMU_NODELETE);
    exit;
  }
  if ($DemoMode)
  {
    returnSuccess();
    exit;
  }    
  $_SESSION['ses_ConfigReload']="reload";
  $query="DELETE FROM usergroups WHERE id=".sl_quote_smart($groupid);
    $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    returnError(ADMINMU_NODELETE);
    exit;
  }
  // Event point
  // Call event handler and plugins
  $paramdata=array();
  $paramdata['oldname']="";
  $paramdata['name']=$groupname;
  $paramdata['groupid']=$groupid;
  $paramdata['description']="";
  $paramdata['loginaction']="";
  $paramdata['loginvalue']="";
  if (function_exists("sl_onDeleteGroup"))
    sl_onDeleteGroup($paramdata);
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if (function_exists($slplugin_event_onDeleteGroup[$p]))
      call_user_func($slplugin_event_onDeleteGroup[$p],$slpluginid[$p],$paramdata);
  }
  returnSuccess();
  exit;

  function returnSuccess()
  {
    $data['success'] = true;
    $data['message'] = "";
    echo json_encode($data);
    exit;
  }

  function returnError($msg)
  {
    $data['success'] = false;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }


