<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  require_once("adminprocesslog.php");  
  // Check CSRF value  
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError(ADMINMSG_CSRFFAILED);
    exit;
  }
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError(ADMINMSG_MYSQLERROR);
    exit;
  }
  $res=slProcessLog("clear",$mysql_link);
  if ($res['success'])
  {
    returnSuccess($res['numdeleted'].ADMINLG_DELETED);
    exit;
  }
  else
  {
    returnError(ADMINLG_NODELETE);
    exit;
  }    

  function returnError($msg)
  {
    $data['success'] = false;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

  function returnSuccess($msg)
  {
      $data['success'] = true;
      $data['message'] = $msg;
      echo json_encode($data);
      exit;
  }


