<?php
  $groupswithaccess="ADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  $dbupdate=true;  
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value  
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError(ADMINMSG_CSRFFAILED);
    exit;
  }
  $checkversions=$_POST['checkversions'];
  $versioncheckfailed="false"; // Use string for json output
  if ($checkversions==1)
  {
    $str = url_get_contents('http://www.vibralogix.com/appdata/slpluginversions.php');
    if ($str===false)
    {  
      $versioncheckfailed="true";
      $checkversions=0;
    }  
    else
      $updateinfo = json_decode($str, true);
  }
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError(ADMINMSG_MYSQLERROR);
    exit;
  }
  if (!sl_tableexists($mysql_link,$DbPluginsTableName))
  {
    returnError(ADMINPL_NOPLUGINS);
    exit;
  }
  $query="SELECT * FROM ".$DbPluginsTableName." ORDER BY id ASC"; 
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    returnError(ADMINMSG_MYSQLERROR);
    exit;
  }
  $ids=array();
  $names=array();
  $icons=array();
  $enabled=array();
  $versions=array();
  $folders=array();
  $configs=array();
  $pluginindexes=array();
  $v5s=array();
  $latestversions[$c]=array();
  $latestminslversion[$c]=array();
  $latestversionlinks[$c]=array();

  $numplugins=mysqli_num_rows($mysql_result);
  $c=0;
  while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
  {
    $pluginids[$c]=$row['id'];
    $names[$c]=$row['name'];
    $enabled[$c]=$row['enabled'];
    $versions[$c]=$row['version'];
    $folders[$c]=$row['folder'];
    if (is_file($row['folder']."/pluginv5.png"))
      $icons[$c]="pluginv5.png";
    else
      $icons[$c]="plugin.png";
    // If plugin is enabled get plugin page, plugin index and check if V5 
    if ($row['enabled']=="Yes")
    {
      $ind=array_search($row['id'],$slpluginid);
      if ($ind!==false)
      {  
        $pluginindexes[$c]=$ind;
        if (isset($slplugin_adminv5[$ind]))
          $v5s[$c]=$slplugin_adminv5[$ind];
        else
          $v5s[$c]=false;
        $configs[$c]=$slplugin_adminpluginpage[$ind];
      }
      else
      {
        $pluginindexes[$c]=-1;
        $v5s[$c]=false;
        $configs[$c]="";
      }  
    }
    else
    {
      $pluginindexes[$c]=-1;
      $v5s[$c]=false;
      $configs[$c]="";
    }
    if ($v5s[$c])
      $configs[$c]=str_replace(".php", "v5.php",$configs[$c]);
    // If required check if later version
    $latestversions[$c]="0";
    $latestminslversion[$c]="0";
    $latestversionlinks[$c]="";
    if ($checkversions==1)
    {
      for ($j=0;$j<count($updateinfo);$j++)
      {
        if ($updateinfo[$j]['name']==$names[$c])
        {
            $latestversions[$c]=$updateinfo[$j]['version'];
            $latestminslversion[$c]=$updateinfo[$j]['slmin'];
            $latestversionlinks[$c]=$updateinfo[$j]['link'];
        }
      }
    }
    $c++;
  }

  function returnError($msg)
  {
    $data['success'] = false;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

  function url_get_contents($Url)
  {
      if (!function_exists('curl_init'))
      { 
          return(false);
      }
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $Url);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $output = curl_exec($ch);
      curl_close($ch);
      return $output;
  }  

  ?>
{
  "success": true,
  "message": "",
  "versioncheckfailed": <?php echo $versioncheckfailed; ?>,
  "plugincount": <?php echo $numplugins; ?>,
  "pluginids": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$pluginids[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "names": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$names[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "icons": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$icons[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "enabled": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$enabled[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "versions": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$versions[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "latestversions": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$latestversions[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "latestminslversion": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$latestminslversion[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "latestversionlinks": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$latestversionlinks[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "configs": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$configs[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "pluginindexes": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$pluginindexes[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "v5s": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo ($v5s[$k]?'true':'false'); if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "folders": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$folders[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ]
}


