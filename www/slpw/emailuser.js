var templateMode="html";
var htmlMode="visual";
var defaultTemplateTop="<!DOCTYPE html>\n<html>\n<head>\n<title></title>\n</head>\n<body>\n";
var defaultTemplateBottom="</body>\n</html>\n";
var templateTop=defaultTemplateTop;
var templateBottom=defaultTemplateBottom;
var modalobj=null;
var progressInterval;
var paused=false;
var currentpath=window.location.href;
currentpath = currentpath.substring(0, currentpath.lastIndexOf('/'));
currentpath +='/';

  tinymce.init({
    selector: '#templateeditor',
    height: 400,
    relative_urls: false,
    remove_script_host: false,
    language: ADMINTINYMCE_LANG,
    external_filemanager_path: SitelokLocationURLPath+"filemanager/",
    filemanager_title:"File Manager" ,
    external_plugins: { "filemanager" : SitelokLocationURL+"filemanager/plugin.min.js"},
    menu: {
    edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall searchreplace'},
    insert: {title: 'Insert', items: 'image media link | charmap hr anchor pagebreak insertdatetime nonbreaking'},
    view: {title: 'View', items: 'visualchars visualblocks visualaid'},
    format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats removeformat'},
    table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
    variables: {title : 'Variables', items : 'username password downloadlink'}
    },
    plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools responsivefilemanager'
      ],
    toolbar: 'insertfile undo redo | styleselect | bold italic | fontselect fontsizeselect | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink image media | print | emoticons',

    image_advtab: true,

  });

// Setup email template checkbox
$('#visualeditor').on('ifChecked', function(event){
               switchHtmlMode("visual");
});
$('#sourceeditor').on('ifChecked', function(event){
                switchHtmlMode("source");
});

$('.selectpicker').selectpicker({
  style: 'btn-default',
  size: 4,
  dropupAuto: false
});

// Stop main form being submitted
$('#emailform').submit(function(event)
{
  event.preventDefault();  
});

$('#subject').focus();

// handle add group link submit
$('#addGroupModal').on('shown.bs.modal', function () {
    $('#addgroup_linkexpiry').focus();
})

$('#addgroupform').submit(function(event)
{
  // remove the error classes
  $('#addgroup_linkexpiry').removeClass('has-error');
  $('#addgroup_usergroup').removeClass('has-error');
  $('#addgroup_groupexpiry').removeClass('has-error');
  $('#addgroup_useremail').removeClass('has-error');
  $('#addgroup_adminemail').removeClass('has-error');
  $('#addgroup_redirect').removeClass('has-error');
   // remove the error messages
  $('#addgroup_linkexpiryhelp').remove();
  $('#addgroup_usergrouphelp').remove();
  $('#addgroup_groupexpiryhelp').remove();
  $('#addgroup_useremailhelp').remove();
  $('#addgroup_adminemailhelp').remove();
  $('#addgroup_redirecthelp').remove();
  var data={success:true,message:'',errors:{}};
  // link expiry
  var linkexpiry=$( "#addgroup_linkexpiry" ).val().trim();
  if (!validInteger(linkexpiry,0,999999,true))
    data.errors.addgroup_linkexpiry=ADMINMSG_LINKEXPIRYINVALID;
  // Usergroup
  var gstr=$( "#addgroup_usergroup" ).val().trim();
  if (gstr=="")
    data.errors.addgroup_usergroup=ADMINMSG_INVALIDGROUP;    
  if (!validUsergroup(gstr,true))
    data.errors.addgroup_usergroup=ADMINMSG_GROUPINVALID;
  // Expiry
  estr=$("#addgroup_groupexpiry").val().trim();
  if ((estr!="") && (!validChars("0123456789",estr)))
    data.errors.addgroup_groupexpiry=ADMINMSG_EXPIRYINVALID;
  if (estr.length==6)
  {
    if (!dateValid(estr,DateFormat))
      data.errors.addgroup_groupexpiry=ADMINMSG_EXPIRYDATEINVALID;
  }
  // User template
  var utemplate=$( "#addgroup_useremail" ).val().trim();
  if (utemplate!="")
  {  
    var fnext = utemplate.substr((utemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.addgroup_useremail=ADMINMSG_TEMPLATETYPE;
  }               
  // Admin template
  var atemplate=$( "#addgroup_adminemail" ).val().trim();
  if (atemplate!="")
  {  
    var fnext = atemplate.substr((atemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.addgroup_adminemail=ADMINMSG_TEMPLATETYPE;
  }
  // redirect
  var redirect=$( "#addgroup_redirect" ).val().trim();
  redirectlower=redirect.toLowerCase();
  if ((redirectlower!="") && (redirectlower.substring(0,7)!='http://') && (redirectlower.substring(0,8)!='https://'))
    data.errors.addgroup_redirect=ADMINMSG_URLINVALID;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  var str='!!!addgroup('+linkexpiry+','+gstr+','+estr+','+utemplate+','+atemplate+','+redirect+')!!!';
  editorInsertLink('addgroup',str);
  $('#addGroupModal').modal('hide')  
  focusEditor();
  event.preventDefault();
});

// handle remove group link submit
$('#removeGroupModal').on('shown.bs.modal', function () {
    $('#removegroup_linkexpiry').focus();
})

$('#removegroupform').submit(function(event)
{
  // remove the error classes
  $('#removegroup_linkexpiry').removeClass('has-error');
  $('#removegroup_usergroup').removeClass('has-error');
  $('#removegroup_useremail').removeClass('has-error');
  $('#removegroup_adminemail').removeClass('has-error');
  $('#removegroup_redirect').removeClass('has-error');
   // remove the error messages
  $('#removegroup_linkexpiryhelp').remove();
  $('#removegroup_usergrouphelp').remove();
  $('#removegroup_useremailhelp').remove();
  $('#removegroup_adminemailhelp').remove();
  $('#removegroup_redirecthelp').remove();
  var data={success:true,message:'',errors:{}};
  // link expiry
  var linkexpiry=$( "#removegroup_linkexpiry" ).val().trim();
  if (!validInteger(linkexpiry,0,999999,true))
    data.errors.removegroup_linkexpiry=ADMINMSG_LINKEXPIRYINVALID;
  // Usergroup
  var gstr=$( "#removegroup_usergroup" ).val().trim();
  if (gstr=="")
    data.errors.removegroup_usergroup=ADMINMSG_INVALIDGROUP;    
  if (!validUsergroup(gstr,true))
    data.errors.removegroup_usergroup=ADMINMSG_GROUPINVALID;
  // User template
  var utemplate=$( "#removegroup_useremail" ).val().trim();
  if (utemplate!="")
  {  
    var fnext = utemplate.substr((utemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.removegroup_useremail=ADMINMSG_TEMPLATETYPE;
  }               
  // Admin template
  var atemplate=$( "#removegroup_adminemail" ).val().trim();
  if (atemplate!="")
  {  
    var fnext = atemplate.substr((atemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.removegroup_adminemail=ADMINMSG_TEMPLATETYPE;
  }
  // redirect
  var redirect=$( "#removegroup_redirect" ).val().trim();
  redirectlower=redirect.toLowerCase();
  if ((redirectlower!="") && (redirectlower.substring(0,7)!='http://') && (redirectlower.substring(0,8)!='https://'))
    data.errors.removegroup_redirect=ADMINMSG_URLINVALID;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  var str='!!!removegroup('+linkexpiry+','+gstr+','+utemplate+','+atemplate+','+redirect+')!!!';
  editorInsertLink('removegroup',str);
  $('#removeGroupModal').modal('hide')
  focusEditor();
  event.preventDefault();
});

// handle replace group link submit
$('#replaceGroupModal').on('shown.bs.modal', function () {
    $('#replacegroup_linkexpiry').focus();
})

$('#replacegroupform').submit(function(event)
{
  // remove the error classes
  $('#replacegroup_linkexpiry').removeClass('has-error');
  $('#replacegroup_usergroup').removeClass('has-error');
  $('#replacegroup_newusergroup').removeClass('has-error');
  $('#replacegroup_groupexpiry').removeClass('has-error');
  $('#replacegroup_useremail').removeClass('has-error');
  $('#replacegroup_adminemail').removeClass('has-error');
  $('#replacegroup_redirect').removeClass('has-error');
   // remove the error messages
  $('#replacegroup_linkexpiryhelp').remove();
  $('#replacegroup_usergrouphelp').remove();
  $('#replacegroup_newusergrouphelp').remove();
  $('#replacegroup_groupexpiryhelp').remove();
  $('#replacegroup_useremailhelp').remove();
  $('#replacegroup_adminemailhelp').remove();
  $('#replacegroup_redirecthelp').remove();
  var data={success:true,message:'',errors:{}};
  // link expiry
  var linkexpiry=$( "#replacegroup_linkexpiry" ).val().trim();
  if (!validInteger(linkexpiry,0,999999,true))
    data.errors.replacegroup_linkexpiry=ADMINMSG_LINKEXPIRYINVALID;
  // Usergroup
  var gstr=$( "#replacegroup_usergroup" ).val().trim();
  if (gstr=="")
    data.errors.replacegroup_usergroup=ADMINMSG_INVALIDGROUP;    
  if (!validUsergroup(gstr,true))
    data.errors.replacegroup_usergroup=ADMINMSG_GROUPINVALID;
  // New usergroup
  var ngstr=$( "#replacegroup_newusergroup" ).val().trim();
  if (ngstr=="")
    data.errors.replacegroup_newusergroup=ADMINMSG_INVALIDGROUP;    
  if (!validUsergroup(ngstr,true))
    data.errors.replacegroup_newusergroup=ADMINMSG_GROUPINVALID;
  // Expiry
  estr=$("#replacegroup_groupexpiry").val().trim();
  if ((estr!="") && (!validChars("0123456789+",estr)))
    data.errors.replacegroup_groupexpiry=ADMINMSG_EXPIRYINVALID;
  if (estr.length==6)
  {
    if (!dateValid(estr,DateFormat))
      data.errors.replacegroup_groupexpiry=ADMINMSG_EXPIRYDATEINVALID;
  }
  // User template
  var utemplate=$( "#replacegroup_useremail" ).val().trim();
  if (utemplate!="")
  {  
    var fnext = utemplate.substr((utemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.replacegroup_useremail=ADMINMSG_TEMPLATETYPE;
  }               
  // Admin template
  var atemplate=$( "#replacegroup_adminemail" ).val().trim();
  if (atemplate!="")
  {  
    var fnext = atemplate.substr((atemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.replacegroup_adminemail=ADMINMSG_TEMPLATETYPE;
  }
  // redirect
  var redirect=$( "#replacegroup_redirect" ).val().trim();
  redirectlower=redirect.toLowerCase();
  if ((redirectlower!="") && (redirectlower.substring(0,7)!='http://') && (redirectlower.substring(0,8)!='https://'))
    data.errors.replacegroup_redirect=ADMINMSG_URLINVALID;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  var str='!!!replacegroup('+linkexpiry+','+gstr+','+ngstr+','+estr+','+utemplate+','+atemplate+','+redirect+')!!!';
  editorInsertLink('replacegroup',str);
  $('#replaceGroupModal').modal('hide')  
  focusEditor();
  event.preventDefault();
});

// handle extend group link submit
$('#extendGroupModal').on('shown.bs.modal', function () {
    $('#extendgroup_linkexpiry').focus();
})

$('#extendgroupform').submit(function(event)
{
  // remove the error classes
  $('#extendroup_linkexpiry').removeClass('has-error');
  $('#extendroup_usergroup').removeClass('has-error');
  $('#extendroup_groupexpiry').removeClass('has-error');
  $('#extendroup_expirytype').removeClass('has-error');
  $('#extendroup_useremail').removeClass('has-error');
  $('#extendroup_adminemail').removeClass('has-error');
  $('#extendroup_redirect').removeClass('has-error');
   // remove the error messages
  $('#extendroup_linkexpiryhelp').remove();
  $('#extendroup_usergrouphelp').remove();
  $('#extendroup_groupexpiryhelp').remove();
  $('#extendroup_useremailhelp').remove();
  $('#extendroup_expirytypehelp').remove();
  $('#extendroup_adminemailhelp').remove();
  $('#extendroup_redirecthelp').remove();
  var data={success:true,message:'',errors:{}};
  // link expiry
  var linkexpiry=$( "#extendgroup_linkexpiry" ).val().trim();
  if (!validInteger(linkexpiry,0,999999,true))
    data.errors.extendgroup_linkexpiry=ADMINMSG_LINKEXPIRYINVALID;
  // Usergroup
  var gstr=$( "#extendgroup_usergroup" ).val().trim();
  if (gstr=="")
    data.errors.extendgroup_usergroup=ADMINMSG_INVALIDGROUP;    
  if (!validUsergroup(gstr,true))
    data.errors.extendgroup_usergroup=ADMINMSG_GROUPINVALID;
  // Expiry
  estr=$("#extendgroup_groupexpiry").val().trim();
  if ((estr!="") && (!validChars("0123456789+",estr)))
    data.errors.extendgroup_groupexpiry=ADMINMSG_EXPIRYINVALID;
  if (estr.length==6)
  {
    if (!dateValid(estr,DateFormat))
      data.errors.extendgroup_groupexpiry=ADMINMSG_EXPIRYDATEINVALID;
  }
  // Expiry type
  etstr=$("#extendgroup_expirytype").val().trim();
  if ((etstr=="") || (!validChars("0123456789+",etstr)))
    data.errors.extendgroup_expirytype=ADMINMSG_EXPIRYINVALID;
  // User template
  var utemplate=$( "#extendgroup_useremail" ).val().trim();
  if (utemplate!="")
  {  
    var fnext = utemplate.substr((utemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.extendgroup_useremail=ADMINMSG_TEMPLATETYPE;
  }               
  // Admin template
  var atemplate=$( "#extendgroup_adminemail" ).val().trim();
  if (atemplate!="")
  {  
    var fnext = atemplate.substr((atemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.extendgroup_adminemail=ADMINMSG_TEMPLATETYPE;
  }
  // redirect
  var redirect=$( "#extendgroup_redirect" ).val().trim();
  redirectlower=redirect.toLowerCase();
  if ((redirectlower!="") && (redirectlower.substring(0,7)!='http://') && (redirectlower.substring(0,8)!='https://'))
    data.errors.extendgroup_redirect=ADMINMSG_URLINVALID;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  var str='!!!extendgroup('+linkexpiry+','+gstr+','+estr+','+etstr+','+utemplate+','+atemplate+','+redirect+')!!!';
  editorInsertLink('extendgroup',str);
  $('#extendGroupModal').modal('hide')  
  focusEditor();
  event.preventDefault();
});

// handle login link submit
$('#loginLinkModal').on('shown.bs.modal', function () {
    $('#loginlink_linkexpiry').focus();
})

$('#loginlinkform').submit(function(event)
{
  // remove the error classes
  $('#loginlink_linkexpiry').removeClass('has-error');
  $('#loginlink_page').removeClass('has-error');
   // remove the error messages
  $('#loginlink_linkexpiryhelp').remove();
  $('#loginlink_pagehelp').remove();
  var data={success:true,message:'',errors:{}};
  // link expiry
  var linkexpiry=$( "#loginlink_linkexpiry" ).val().trim();
  if (!validInteger(linkexpiry,0,999999,true))
    data.errors.loginlink_linkexpiry=ADMINMSG_LINKEXPIRYINVALID;
  // page
  var page=$( "#loginlink_page" ).val().trim();
  pagelower=page.toLowerCase();
  if ((pagelower!="") && (pagelower.substring(0,7)!='http://') && (pagelower.substring(0,8)!='https://'))
    data.errors.loginlink_page=ADMINMSG_URLINVALID;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  if (page.indexOf('?')==-1)
    var str=page+'?username=!!!username!!!&password=!!!passwordhash('+linkexpiry+')!!!';
  else
    var str=page+'&username=!!!username!!!&password=!!!passwordhash('+linkexpiry+')!!!';    
  editorInsertLink('login',str);
  $('#loginLinkModal').modal('hide')  
  focusEditor();
  event.preventDefault();
});

// handle download link submit
$('#downloadLinkModal').on('shown.bs.modal', function () {
    $('#downloadlink_linkexpiry').focus();
})

$('#downloadlinkform').submit(function(event)
{
  // remove the error classes
  $('#downloadlink_linkexpiry').removeClass('has-error');
  $('#downloadlink_file').removeClass('has-error');
  $('#downloadlink_location').removeClass('has-error');
   // remove the error messages
  $('#downloadlink_linkexpiryhelp').remove();
  $('#downloadlink_filehelp').remove();
  $('#downloadlink_locationhelp').remove();
  var data={success:true,message:'',errors:{}};
  // link expiry
  var linkexpiry=$( "#downloadlink_linkexpiry" ).val().trim();
  if (linkexpiry.length!=12)
  {
    if (!validInteger(linkexpiry,0,999999,true))
      data.errors.downloadlink_linkexpiry=ADMINMSG_LINKEXPIRYINVALID;
  }
  else
  {
    if (!validChars("0123456789",linkexpiry))
      data.errors.downloadlink_linkexpiry=ADMINMSG_LINKEXPIRYINVALID;      
  }
  // file
  var file=$( "#downloadlink_file" ).val().trim();
  if (file=="")
    data.errors.downloadlink_file=ADMINMSG_ENTERFILE;
  // location
  var location=$( "#downloadlink_location" ).val().trim();  
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  if (location=='')
    var str='!!!link('+file+','+linkexpiry+')!!!';
  else
    var str='!!!link('+file+':'+location+','+linkexpiry+')!!!';
  editorInsertLink(file,str);
  $('#downloadLinkModal').modal('hide')  
  focusEditor();
  event.preventDefault();
});

// handle file size submit
$('#fileSizeModal').on('shown.bs.modal', function () {
    $('#filesize_file').focus();
})

$('#filesizeform').submit(function(event)
{
  // remove the error classes
  $('#filesize_file').removeClass('has-error');
  $('#filesize_location').removeClass('has-error');
   // remove the error messages
  $('#filesize_filehelp').remove();
  $('#filesize_locationhelp').remove();
  var data={success:true,message:'',errors:{}};
  // file
  var file=$( "#filesize_file" ).val().trim();
  if (file=="")
    data.errors.filesize_file=ADMINMSG_ENTERFILE;
  // location
  var location=$( "#filesize_location" ).val().trim();  
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  if (location=='')
    var str='!!!size('+file+')!!!';
  else
    var str='!!!size('+file+':'+location+')!!!';
  editorInsertStr(str);
  $('#fileSizeModal').modal('hide')  
  focusEditor();
  event.preventDefault();
});

// handle approve/confirm link submit
$('#approveModal').on('shown.bs.modal', function () {
    $('#approve_linkexpiry').focus();
})

$('#approveform').submit(function(event)
{
  // remove the error classes
  $('#approve_linkexpiry').removeClass('has-error');
  $('#approve_useremail').removeClass('has-error');
  $('#approve_adminemail').removeClass('has-error');
  $('#approve_redirect').removeClass('has-error');
   // remove the error messages
  $('#approve_linkexpiryhelp').remove();
  $('#approve_useremailhelp').remove();
  $('#approve_adminemailhelp').remove();
  $('#approve_redirecthelp').remove();
  var data={success:true,message:'',errors:{}};
  // link expiry
  var linkexpiry=$( "#approve_linkexpiry" ).val().trim();
  if (!validInteger(linkexpiry,0,999999,true))
    data.errors.approve_linkexpiry=ADMINMSG_LINKEXPIRYINVALID;
  // User template
  var utemplate=$( "#approve_useremail" ).val().trim();
  if (utemplate!="")
  {  
    var fnext = utemplate.substr((utemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.approve_useremail=ADMINMSG_TEMPLATETYPE;
  }               
  // Admin template
  var atemplate=$( "#approve_adminemail" ).val().trim();
  if (atemplate!="")
  {  
    var fnext = atemplate.substr((atemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.approve_adminemail=ADMINMSG_TEMPLATETYPE;
  }
  // redirect
  var redirect=$( "#approve_redirect" ).val().trim();
  redirectlower=redirect.toLowerCase();
  if ((redirectlower!="") && (redirectlower.substring(0,7)!='http://') && (redirectlower.substring(0,8)!='https://'))
    data.errors.approve_redirect=ADMINMSG_URLINVALID;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  var str='!!!approve('+linkexpiry+','+utemplate+','+atemplate+','+redirect+')!!!';
  editorInsertLink('confirm account',str);
  $('#approveModal').modal('hide')  
  focusEditor();
  event.preventDefault();
});

// handle disable link submit
$('#disableModal').on('shown.bs.modal', function () {
    $('#disable_linkexpirydiv').focus();
})

$('#disableform').submit(function(event)
{
  // remove the error classes
  $('#disable_linkexpiry').removeClass('has-error');
  $('#disable_useremail').removeClass('has-error');
  $('#disable_adminemail').removeClass('has-error');
  $('#disable_redirect').removeClass('has-error');
   // remove the error messages
  $('#disable_linkexpiryhelp').remove();
  $('#disable_useremailhelp').remove();
  $('#disable_adminemailhelp').remove();
  $('#disable_redirecthelp').remove();
  var data={success:true,message:'',errors:{}};
  // link expiry
  var linkexpiry=$( "#disable_linkexpiry" ).val().trim();
  if (!validInteger(linkexpiry,0,999999,true))
    data.errors.disable_linkexpiry=ADMINMSG_LINKEXPIRYINVALID;
  // User template
  var utemplate=$( "#disable_useremail" ).val().trim();
  if (utemplate!="")
  {  
    var fnext = utemplate.substr((utemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.disable_useremail=ADMINMSG_TEMPLATETYPE;
  }  
  // Admin template
  var atemplate=$( "#disable_adminemail" ).val().trim();
  if (atemplate!="")
  {  
    var fnext = atemplate.substr((atemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.disable_adminemail=ADMINMSG_TEMPLATETYPE;
  }
  // redirect
  var redirect=$( "#disable_redirect" ).val().trim();
  redirectlower=redirect.toLowerCase();
  if ((redirectlower!="") && (redirectlower.substring(0,7)!='http://') && (redirectlower.substring(0,8)!='https://'))
    data.errors.disable_redirect=ADMINMSG_URLINVALID;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  var str='!!!disable('+linkexpiry+','+utemplate+','+atemplate+','+redirect+')!!!';
  editorInsertLink('disable account',str);
  $('#disableModal').modal('hide')  
  focusEditor();
  event.preventDefault();
});

// handle delete link submit
$('#deleteModal').on('shown.bs.modal', function () {
    $('#delete_linkexpiry').focus();
})

$('#deleteform').submit(function(event)
{
  // remove the error classes
  $('#delete_linkexpiry').removeClass('has-error');
  $('#delete_useremail').removeClass('has-error');
  $('#delete_adminemail').removeClass('has-error');
  $('#delete_redirect').removeClass('has-error');
   // remove the error messages
  $('#delete_linkexpiryhelp').remove();
  $('#delete_useremailhelp').remove();
  $('#delete_adminemailhelp').remove();
  $('#delete_redirecthelp').remove();
  var data={success:true,message:'',errors:{}};
  // link expiry
  var linkexpiry=$( "#delete_linkexpiry" ).val().trim();
  if (!validInteger(linkexpiry,0,999999,true))
    data.errors.delete_linkexpiry=ADMINMSG_LINKEXPIRYINVALID;
  // User template
  var utemplate=$( "#delete_useremail" ).val().trim();
  if (utemplate!="")
  {  
    var fnext = utemplate.substr((utemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.delete_useremail=ADMINMSG_TEMPLATETYPE;
  }               
  // Admin template
  var atemplate=$( "#delete_adminemail" ).val().trim();
  if (atemplate!="")
  {  
    var fnext = atemplate.substr((atemplate.lastIndexOf('.') + 1));
    fnext=fnext.toLowerCase();
    if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
      data.errors.delete_adminemail=ADMINMSG_TEMPLATETYPE;
  }
  // redirect
  var redirect=$( "#delete_redirect" ).val().trim();
  redirectlower=redirect.toLowerCase();
  if ((redirectlower!="") && (redirectlower.substring(0,7)!='http://') && (redirectlower.substring(0,8)!='https://'))
    data.errors.delete_redirect=ADMINMSG_URLINVALID;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  var str='!!!delete('+linkexpiry+','+utemplate+','+atemplate+','+redirect+')!!!';
  editorInsertLink('delete account',str);
  $('#deleteModal').modal('hide')  
  focusEditor();
  event.preventDefault();
});

// handle view email link submit
$('#viewEmailModal').on('shown.bs.modal', function () {
    $('#viewemail_linkexpiry').focus();
})

$('#viewemailform').submit(function(event)
{
  // remove the error classes
  $('#viewemail_linkexpiry').removeClass('has-error');
  $('#viewemail_email').removeClass('has-error');
   // remove the error messages
  $('#viewemail_linkexpiryhelp').remove();
  $('#viewemail_emailhelp').remove();
  var data={success:true,message:'',errors:{}};
  // link expiry
  var linkexpiry=$( "#viewemail_linkexpiry" ).val().trim();
  if (!validInteger(linkexpiry,0,999999,true))
    data.errors.viewemail_linkexpiry=ADMINMSG_LINKEXPIRYINVALID;
  // User template
  var template=$( "#viewemail_email" ).val().trim();
  var fnext = template.substr((template.lastIndexOf('.') + 1));
  fnext=fnext.toLowerCase();
  if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
    data.errors.viewemail_email=ADMINMSG_TEMPLATETYPE;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  var str='!!!viewemail('+linkexpiry+','+template+')!!!';
  editorInsertLink('view email in browser',str);
  $('#viewEmailModal').modal('hide');
  focusEditor();
  event.preventDefault();
});

// handle contact field label submit
$('#contactLabelModal').on('shown.bs.modal', function () {
    $('#contactlabel_num').focus();
})

$('#contactlabelform').submit(function(event)
{
  // remove the error classes
  $('#contactlabel_num').removeClass('has-error');
   // remove the error messages
  $('#contactlabel_numhelp').remove();
  var data={success:true,message:'',errors:{}};
  // Field number
  var num=$( "#contactlabel_num" ).val().trim();
  if (!validInteger(num,0,99,true))
    data.errors.contactlabel_num=ADMINMSG_LINKEXPIRYINVALID;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  var str='!!!contactlabel_'+num+'!!!';
  editorInsertStr(str);
  $('#contactLabelModal').modal('hide')  
  focusEditor();
  event.preventDefault();
});

// handle contact field value submit
$('#contactValueModal').on('shown.bs.modal', function () {
    $('#contactvalue_num').focus();
})

$('#contactvalueform').submit(function(event)
{
  // remove the error classes
  $('#contactvalue_num').removeClass('has-error');
   // remove the error messages
  $('#contactvalue_numhelp').remove();
  var data={success:true,message:'',errors:{}};
  // Field number
  var num=$( "#contactvalue_num" ).val().trim();
  if (!validInteger(num,0,99,true))
    data.errors.contactvalue_num=ADMINMSG_LINKEXPIRYINVALID;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  var str='!!!contactvalue_'+num+'!!!';
  editorInsertStr(str);
  $('#contactValueModal').modal('hide');
  focusEditor();
  event.preventDefault();
});

// handle save email submit
$('#saveEmailModal').on('shown.bs.modal', function () {
    $('#saveemail_filename').focus();
})

$('#saveemailform').submit(function(event)
{
  // remove the error classes
  $('#saveemail_filename').removeClass('has-error');
   // remove the error messages
  $('#saveemail_filenamehelp').remove();
  //Remove form message
  $('#resultsendemail1').html('');
  $('#resultsendemail2').html('');
  var data={success:true,message:'',errors:{}};
  // Filename
  var fn=($('#saveemail_filename').val());
  var fnext = fn.substr((fn.lastIndexOf('.') + 1));
  fnext=fnext.toLowerCase();
  if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
    data.errors.saveemail_filename=ADMINMSG_TEMPLATETYPE;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  var content="";
  if (templateMode=="html")
  {
    if (htmlMode=="visual")
      content=templateTop+tinymce.activeEditor.getContent({format: 'code'})+templateBottom;
    else
      content=$('#templateeditortext').val();
  }
  else
    content=$('#templateeditortext').val();
  // Call ajax handler to get file contents
  var label=showButtonBusy('savetemplate',ADMINMSG_SAVING);
  var formData = {
      'slcsrf'    : slcsrf,
      'filename'  : fn,
      'subject'   : $('#subject').val(),
      'htmlformat': templateMode,
      'content'   : fixContent(content)
  };
  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminsaveemailtemplate.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      // using the done promise callback
      .done(function(data) {
        sessionExpiredReported(data,true);
        hideButtonBusy('savetemplate',label);
        if(data.hasOwnProperty("error"))
        { 
          switch (data.error.substr(-3,3))
          {
            case "001":
              location.reload(true);
              break;
            case "002":
              bootbox.alert("Could not connect to Mysql");
              break;
          }   
        }         
        if (data.success)
        {
          setFilename(fn);
        }
        else
        {
          hideButtonBusy('savetemplate',label);
          $('#resultsendemail1').html('<div id="resultsendemail1message" class="alert alert-danger">' + data.message + '</div>');
        }
      })

// using the fail promise callback
    .fail(function(data) {
    hideButtonBusy('savetemplate',label);  
    $('#resultsendemail1').html('<div id="resultsendemail1message" class="alert alert-danger">' + ADMINMSG_SAVETEMPLATEERROR + '</div>');
    event.preventDefault();
    });
    $('#saveEmailModal').modal('hide')
    event.preventDefault();
  });

// handle preview email submit
$('#previewEmailModal').on('shown.bs.modal', function () {
    $('#previewemail_username').focus();
})

$('#previewemailform').submit(function(event)
{
  // remove the error classes
  $('#previewemail_username').removeClass('has-error');
   // remove the error messages
  $('#previewemail_usernamehelp').remove();
  //Remove form message
  $('#resultsendemail1').html('');
  $('#resultsendemail2').html('');

  var data={success:true,message:'',errors:{}};
  // username
  var username=$( "#previewemail_username" ).val().trim();
  // subject
  var subject=$('#subject').val().trim();
  if (subject=="")
    subject=ADMINMSG_ENTERSUBJECT;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  var content="";
  if (templateMode=="html")
  {
    if (htmlMode=="visual")
      content=templateTop+tinymce.activeEditor.getContent({format: 'code'})+templateBottom;
    else
      content=$('#templateeditortext').val();
  }
  else
    content=$('#templateeditortext').val();
  // Call ajax handler to get file contents
  var formData = {
      'slcsrf'    : slcsrf,
      'subject'   : subject,
      'htmlformat': templateMode,
      'content'   : fixContent(content),
      'act'       : $('#act').val(),
      'user'      : username
  };

  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminpreviewemailsetup.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      // using the done promise callback
      .done(function(data) {
        sessionExpiredReported(data,true);
        if (data.success)
        {
          data.content=data.content.replace(/\\n/g,'\n');
          data.content=data.content.replace(/\"/g,'"');
          $.fancybox({
                  'title': data.subject,
                  'content' : "<iframe id=\"previewiframe\" src=\"adminpreviewemail.php?slcsrf="+slcsrf+"\" scrolling=\"yes\" frameborder=\"0\" width=\"100%\" height=\"100%\"></iframe>",
                  'minWidth': 260,
                  'minHeight': 400,
                  'autoSize' : false,
                  'width'    : "auto",
                  'height'   : "80%",
                  'width'    : "80%",
          });
        }
        else
        {
          $('#resultsendemail2').html('<div id="resultsendemail2message" class="alert alert-danger">' + data.message + '</div>');
        }
      })

// using the fail promise callback
    .fail(function(data) {
    $('#resultsendemail2').html('<div id="resultsendemail2message" class="alert alert-danger">' + ADMINEMAILUSER_PREVIEWISSUE + '</div>');
    });
  $('#previewEmailModal').modal('hide')  
  event.preventDefault();
});

// handle send email submit
$('#sendEmailModal').on('shown.bs.modal', function () {
    $('#sendemail_username').focus();
})

$('#sendemailform').submit(function(event)
{
  // remove the error classes
  $('#sendemail_usernamediv').removeClass('has-error');
   // remove the error messages
  $('#sendemail_usernamehelp').remove();
  //Remove form message
  $('#resultsendemail1').html('');
  $('#resultsendemail2').html('');  
  var data={success:true,message:'',errors:{}};
  // username
  var username=$( "#sendemail_username" ).val().trim();
  // subject
  var subject=$('#subject').val().trim();
  if (subject=="")
    subject=ADMINMSG_ENTERSUBJECT;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message="";
    showValidation(data,'');
    event.preventDefault();
    return;
  }
  var content="";
  if (templateMode=="html")
  {
    if (htmlMode=="visual")
      content=templateTop+tinymce.activeEditor.getContent({format: 'code'})+templateBottom;
    else
      content=$('#templateeditortext').val();
  }
  else
    content=$('#templateeditortext').val();
  // Call ajax handler to send email
  var label=showButtonBusy('sendemail_button',ADMINMSG_SENDING);
  var formData = {
      'slcsrf'    : slcsrf,
      'subject'   : subject,
      'htmlformat': templateMode,
      'content'   : fixContent(content),
      'act'       : $('#act').val(),
      'user'      : username,
  };
  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminsendemail.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      // using the done promise callback
      .done(function(data) {
        sessionExpiredReported(data,true); 
        if (data.success)
        {
          hideButtonBusy('sendemail_button',label);
          var successstr=data.email;
          if ((data.user!="") && (data.user!=data.email))
            successstr+=" ("+data.user+")";
          $('#resultsendemail2').html('<div id="resultsendemail2message" class="alert alert-success">' + ADMINMSG_EMAILSENTTO + successstr + '</div>');
          $('#sendEmailModal').modal('hide')  
        }
        else
        {
          hideButtonBusy('sendemail_button',label);
          if ((data.message==ADMINMSG_EMAILBLOCKEDPLUG) || (data.message==ADMINMSG_USEROREMAILNOTVALID))
          {
            $('#sendemail_usernamediv').addClass('has-error'); // add the error class to show red input
            $('#sendemail_usernamediv').append('<div class="help-block" id="sendemail_usernamehelp">' + data.message + '</div>');
          }
          else
          {
            $('#resultsendemail2').html('<div id="resultsendemail2message" class="alert alert-danger">' + data.message + '</div>');
            $('#sendEmailModal').modal('hide')  
          }

        }
      })

// using the fail promise callback
    .fail(function(data) {
    hideButtonBusy('sendemail_button',label);
    $('#resultsendemail2').html('<div id="resultsendemail2message" class="alert alert-danger">' + ADMINMSG_SENDEMAILISSUE + '</div>');
    $('#sendEmailModal').modal('hide')  
    });
  event.preventDefault();
});


// sendSelected2Modal should not close with escape key or clicking background
$('#sendSelected2Modal').modal({
    keyboard: false,
    backdrop: 'static',
    'show': false
})

$('#sendSelected1Modal').on('shown.bs.modal', function () {
    $('#sendselected1_dedupe').focus();
})

$('#sendselected1form').submit(function(event)
{
  //Remove form message
  $('#resultsendemail1').html('');
  $('#resultsendemail2').html('');  
  // dedupe
  var dedupe="0";
  if ($('#sendselected1_dedupe').iCheck('update')[0].checked)
    dedupe="1";
  // deselect
  var deselect="0";
  if ($('#sendselected1_deselect').iCheck('update')[0].checked)
    deselect="1";
  // If deselect option then disable selected user actions in main menu
  if (deselect=="1")
    $('.selectactionmenu').hide();
  // subject
  var subject=$('#subject').val().trim();
  if (subject=="")
    subject=ADMINMSG_ENTERSUBJECT;
  var content="";
  if (templateMode=="html")
  {
    if (htmlMode=="visual")
      content=templateTop+tinymce.activeEditor.getContent({format: 'code'})+templateBottom;
    else
      content=$('#templateeditortext').val();
  }
  else
    content=$('#templateeditortext').val();
  // Hide sendSelected1Modal and show sendSelected2Modal
  $('#sendSelected1Modal').modal('hide');
  setSentProgress(0);
  $('#sendSelected2Modal').modal('show');  
  event.preventDefault();
  startEmailHandler(subject,content,templateMode,dedupe,deselect);
});

function responsive_filemanager_callback(field_id)
{
  if (-1!=EmailURL.indexOf('://www.'))
  {
    var emailurl1=EmailURL.replace('://www.','://');
    var emailurl2=EmailURL;
  }
  else
  {
    var emailurl1=EmailURL;
    var emailurl2=EmailURL.replace('://','://www.');
  }
  var emailurl3=EmailURL;
  emailurl3=EmailURL.replace('http://','https://');
  var emailurl4=EmailURL;
  emailurl4=EmailURL.replace('https://','http://');
  var url=$('#'+field_id).val();
  url=url.replace(emailurl1,'');
  url=url.replace(emailurl2,'');
  url=url.replace(emailurl3,'');
  url=url.replace(emailurl4,'');
  url=decodeURIComponent(url);
  $('#'+field_id).val(url);
  loadTemplate();
}

function newTemplate(type)
{
  //Remove form message
  $('#resultsendemail1').html('');
  $('#resultsendemail2').html('');
  if (type=="html")
  {
    setTemplateMode('html',defaultTemplateTop,defaultTemplateBottom)
    setFilename('untitled.html');
  }
  else
  {
    setTemplateMode('text','','')
    setFilename('untitled.txt');
  }
}

function setTemplateMode(type,top,bottom)
{
  if (type=="html")
  {
    $('#visualeditor').iCheck('check');
    templateMode="html";
    templateTop=top;
    templateBottom=bottom;
    $('#subject').val("");
    $('#templateeditortext').val("");
    $('#htmlmodediv').show();
    $('#modegroup').show();
    $('#textmodediv').hide();

    tinyMCE.activeEditor.setContent('');
  }
  else
  {
    templateMode="text";
    templateTop="";
    templateBottom="";
    $('#subject').val("");
    tinyMCE.activeEditor.setContent('');
    $('#templateeditortext').val("");
    $('#htmlmodediv').hide();
    $('#modegroup').hide();
    $('#textmodediv').show();
  }
}

function setFilename(file)
{
  $('#filename').val(file);
  $('#filenamelabel').text(file);
  if (file.substring(0,9)!=='untitled')
    $('#viewemail_email').val(file);
}

function switchHtmlMode(type)
{
  if (type=="visual")
  {
    var content=$('#templateeditortext').val();
    var body=getHtmlBody(content);
    templateTop=getHtmlTop(content);
    templateBottom=getHtmlBottom(content);    
    tinymce.activeEditor.setContent(body, {format: 'code'});
    $('#htmlmodediv').show();
    $('#textmodediv').hide();
    htmlMode="visual";
  }
  else
  {
    var content=tinymce.activeEditor.getContent({format: 'code'});
    content=fixContent(content);
    $('#templateeditortext').val(templateTop+content+templateBottom);
    $('#htmlmodediv').hide();
    $('#textmodediv').show();
    htmlMode="source";
  }
}

function loadClicked()
{
    //Remove form message
    $('#resultsendemail1').html('');
    $('#resultsendemail2').html('');
    $.fancybox({
        'type'  : 'iframe',
        'width' : '90%',
        'height' : '90%',
        'href' : 'filemanager/dialog.php?type=2&field_id=filename&lang='+ADMINRFM_LANG+'&sort_by=name&descending=0'
      });
}

function manageClicked()
{
    //Remove form message
    $('#resultsendemail1').html('');
    $('#resultsendemail2').html('');
    $.fancybox({
        'type'  : 'iframe',
        'width' : '90%',
        'height' : '90%',
        'href' : 'filemanager/dialog.php?type=2&lang='+ADMINRFM_LANG+'&sort_by=name&descending=0'
      });
}

function loadTemplate()
{
  var fn=$('#filename').val();
  var fnext = fn.substr((fn.lastIndexOf('.') + 1));
  fnext=fnext.toLowerCase();
  // Call ajax handler to get file contents
  var label=showButtonBusy('loadtemplate',ADMINMSG_LOADING);
  var formData = {
      'slcsrf'    : slcsrf,
      'filename'  : $('#filename').val()
  };

  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminloademailtemplate.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      // using the done promise callback
      .done(function(data) {
        sessionExpiredReported(data,true);
       // hideButtonBusy('loadtemplate',label);
        hideButtonBusy('loadtemplate',ADMINBUTTON_LOAD);
        if (data.success)
        {
          // TinyMCE needs just the body of the template

          data.subject=data.subject.replace(/\\n/g,'\n');
          data.subject=data.subject.replace(/\"/g,'"');
          data.content=data.content.replace(/\\n/g,'\n');
          data.content=data.content.replace(/\"/g,'"');
          if (data.htmlformat=="Y")
          { 
            // Store top and bottom of template far saving later
            templateTop=getHtmlTop(data.content);
            templateBottom=getHtmlBottom(data.content);
            body=getHtmlBody(data.content);
            setTemplateMode('html',templateTop,templateBottom);
            htmlMode="visual";
            tinymce.activeEditor.setContent(body, {format: 'code'});
            $('#templateeditortext').val(data.content);
          }
          else
          {  
            setTemplateMode('text','','');
            $('#templateeditortext').val(data.content);
          }  
          $('#subject').val(data.subject);
          setFilename($('#filename').val());
        }
        else
        {
          // hideButtonBusy('loadtemplate',label);
          hideButtonBusy('loadtemplate',ADMINBUTTON_LOAD);
          newTemplate('html');
           $('#resultsendemail1').html('<div id="resultsendemail1message" class="alert alert-danger">' + data.message + '</div>');
        }
      })

// using the fail promise callback
    .fail(function(data) {
    // hideButtonBusy('loadtemplate',label);
    hideButtonBusy('loadtemplate',ADMINBUTTON_LOAD);
    newTemplate('html');
    $('#resultsendemail1').html('<div id="resultsendemail1message" class="alert alert-danger">' + ADMINMSG_TEMPLATENOLOAD + '</div>');
    });
}

function previewClicked()
{
//  subject=$('#subject').val();
//  if (subject=="")
//  {
//    var box=bootbox.alert(ADMINMSG_ENTERSUBJECT);
//    box.on('hidden.bs.modal',function(){
//      $('#subject').focus(); 
//    });
//    return;
//  }
  $('#previewEmailModal').modal();  
}

function saveEmailClicked()
{
  // remove the error classes
  $('#saveemail_filenamediv').removeClass('has-error');
   // remove the error messages
  $('#saveemail_filenamehelp').remove();
  //Remove form message
  $('#resultsendemail1').html('');
  $('#resultsendemail2').html('');
  subject=$('#subject').val();
  if (subject=="")
  {
   var box=bootbox.alert(ADMINMSG_ENTERSUBJECT);
   box.on('hidden.bs.modal',function(){
     $('#subject').focus(); 
   });
   return;
  }
  // Prefill filename
  var fn=$('#filename').val();
  $('#saveemail_filename').val(fn);
  $('#saveEmailModal').modal();  
}

function previewForUser(id)
{
  var subject=$('#subject').val()
  var content="";
  if (templateMode=="html")
  {
    if (htmlMode=="visual")
      content=templateTop+tinymce.activeEditor.getContent({format: 'code'})+templateBottom;
    else
      content=$('#templateeditortext').val();
  }
  else
    content=$('#templateeditortext').val();
  // Call ajax handler to get file contents
  var formData = {
      'slcsrf'    : slcsrf,
      'subject'   : subject,
      'htmlformat': templateMode,
      'content'   : fixContent(content),
      'act'       : 'emailuser',
      'user'      : id
  };

  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminpreviewemailsetup.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      // using the done promise callback
      .done(function(data) {
        sessionExpiredReported(data,true);
        if (data.success)
        {
          data.content=data.content.replace(/\\n/g,'\n');
          data.content=data.content.replace(/\"/g,'"');
          $.fancybox({
                  'title': data.subject,
                  'content' : "<iframe id=\"previewiframe\" src=\"adminpreviewemail.php?slcsrf="+slcsrf+"\" scrolling=\"yes\" frameborder=\"0\" width=\"100%\" height=\"100%\"></iframe>",
                  'minWidth': 260,
                  'minHeight': 400,
                  'autoSize' : false,
                  'width'    : "auto",
                  'height'   : "80%",
                  'width'    : "80%",
          });        }
        else
        {
          $('#resultsendemail2').html('<div id="resultsendemail2message" class="alert alert-danger">' + data.message + '</div>');
        }
      })

// using the fail promise callback
    .fail(function(data) {
    $('#resultsendemail2').html('<div id="resultsendemail2message" class="alert alert-danger">' + ADMINEMAILUSER_PREVIEWISSUE + '</div>');
    });
  $('#previewEmailModal').modal('hide');  
  event.preventDefault();
}

function previewSelected(id)
{
  var subject=$('#subject').val()
  var content="";
  if (templateMode=="html")
  {
    if (htmlMode=="visual")
      content=templateTop+tinymce.activeEditor.getContent({format: 'code'})+templateBottom;
    else
      content=$('#templateeditortext').val();
  }
  else
    content=$('#templateeditortext').val();
  // Call ajax handler to get file contents
  var formData = {
      'slcsrf'    : slcsrf,
      'subject'   : subject,
      'htmlformat': templateMode,
      'content'   : fixContent(content),
      'act'       : 'emailselected',
  };

  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminpreviewemailsetup.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      // using the done promise callback
      .done(function(data) {
        sessionExpiredReported(data,true);
        if (data.success)
        {
          data.content=data.content.replace(/\\n/g,'\n');
          data.content=data.content.replace(/\"/g,'"');
          $.fancybox({
                  'title': data.subject,
                  'content' : "<iframe id=\"previewiframe\" src=\"adminpreviewemail.php?slcsrf="+slcsrf+"\" scrolling=\"yes\" frameborder=\"0\" width=\"100%\" height=\"100%\"></iframe>",
                  'minWidth': 260,
                  'minHeight': 400,
                  'autoSize' : false,
                  'width'    : "auto",
                  'height'   : "80%",
                  'width'    : "80%",
          });        }
        else
        {
          $('#resultsendemail2').html('<div id="resultsendemail2message" class="alert alert-danger">' + data.message + '</div>');
        }
      })

// using the fail promise callback
    .fail(function(data) {
    $('#resultsendemail2').html('<div id="resultsendemail2message" class="alert alert-danger">' + ADMINEMAILUSER_PREVIEWISSUE + '</div>');
    });
  $('#previewEmailModal').modal('hide');  
  event.preventDefault();
}

function sendEmailClicked()
{
  // remove the error classes
  $('#sendemail_username').removeClass('has-error');
   // remove the error messages
  $('#sendemail_usernamehelp').remove();
  //Remove form message
  $('#resultsendemail1').html('');
  $('#resultsendemail2').html('');  
  subject=$('#subject').val();
  if (subject=="")
  {
   var box=bootbox.alert(ADMINMSG_ENTERSUBJECT);
   box.on('hidden.bs.modal',function(){
     $('#subject').focus(); 
   });
   return;
  }
  $('#sendEmailModal').modal();  
}

function sendEmailToUser(userid)
{
  subject=$('#subject').val();
  if (subject=="")
  {
   var box=bootbox.alert(ADMINMSG_ENTERSUBJECT);
   box.on('hidden.bs.modal',function(){
     $('#subject').focus(); 
   });
   return;
  }
  var content="";
  if (templateMode=="html")
  {
    if (htmlMode=="visual")
      content=templateTop+tinymce.activeEditor.getContent({format: 'code'})+templateBottom;
    else
      content=$('#templateeditortext').val();
  }
  else
    content=$('#templateeditortext').val();
  // Call ajax handler to get file contents
  var label=showButtonBusy('sendemail',ADMINMSG_SENDING);
  var formData = {
      'slcsrf'    : slcsrf,
      'subject'   : subject,
      'htmlformat': templateMode,
      'content'   : fixContent(content),
      'act'       : 'emailuser',
      'user'      : userid,
  };
  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminsendemail.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      // using the done promise callback
      .done(function(data) {
        sessionExpiredReported(data,true);
        if (data.success)
        {
          hideButtonBusy('sendemail',label);
          var successstr=data.user;
          if (data.user!=data.email)
            successstr+=" ("+data.email+")";
          $('#resultsendemail2').html('<div id="resultsendemail2message" class="alert alert-success">' + ADMINMSG_EMAILSENTTO + successstr + '</div>');
        }
        else
        {
          hideButtonBusy('sendemail',label);
          $('#resultsendemail2').html('<div id="resultsendemail2message" class="alert alert-danger">' + data.message + '</div>');
        }
      })

// using the fail promise callback
    .fail(function(data) {
    hideButtonBusy('sendemail',label);
    $('#resultsendemail2').html('<div id="resultsendemail2message" class="alert alert-danger">' + ADMINMSG_SENDEMAILISSUE + '</div>');
    });
  event.preventDefault();
}

function sendEmailSelected()
{
  // remove the error classes
  $('#sendselected1_username').removeClass('has-error');
   // remove the error messages
  $('#sendselected1_usernamehelp').remove();
  //Remove form message
  $('#resultsendemail1').html('');
  $('#resultsendemail2').html('');  
  subject=$('#subject').val();
  if (subject=="")
  {
   var box=bootbox.alert(ADMINMSG_ENTERSUBJECT);
   box.on('hidden.bs.modal',function(){
     $('#subject').focus(); 
   });
   return;
  }
  $('#sendSelected1Modal').modal();  
}

function startEmailHandler(subject,content,templateMode,dedupe,deselect)
{
  $('#sendselected2_pause').show();  
  $('#sendselected2_pause').prop('disabled', false);
  $('#sendselected2_continue').hide();  
  $('#sendselected2_continue').prop('disabled', false);
  $('#sendselected2_cancel').show();  
  $('#sendselected2_cancel').prop('disabled', false);
  $('#sendselected2_close').hide();    
  $('#sendselected2_close').prop('disabled', false);
  $('#statustext').text('Sending emails...');
  $('#numsent').text('');
  $('#numfail').text('');
  $('#numblocked').text('');
  setSentProgress(0);
  $('#sentprogress').addClass('active');
  paused=false;

  // Call ajax handler to send email
  var formData = {
      'slcsrf'    : slcsrf,
      'subject'   : subject,
      'htmlformat': templateMode,
      'content'   : fixContent(content),
      'dedupe'    : dedupe,
      'deselect'    : deselect,
      'esact'       : 'init'
  };

  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminemailselected.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      .done(function(data) {
        sessionExpiredReported(data,true);
        if (data.esstatus=="finished")
        {  
          finishEmailHandler(data);
          return;
        }
        if (data.esstatus=="paused")
        {
          processPaused(data);
          return;
        }
        if (data.esstatus=="cancelled")
        {  
          cancelEmailHandler(data);
          return;
        }
        if (data.esstatus=="error")
        {  
          // Delay 5 seconds before retrying
          setTimeout(function(){ moreEmailHandler(); }, 5000);
          return;
        }
        moreEmailHandler();
      })

      .fail(function(data) {
        // Delay 5 seconds before retrying
        setTimeout(function(){ moreEmailHandler(); }, 5000);
      })

  progressInterval = setInterval(updateProgress, 2000);

};

function moreEmailHandler()
{
  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminemailselected.php', // the url where we want to POST
      data        : {'slcsrf'    : slcsrf,'subject'   : "",'htmlformat': "",'content'   : "",'dedupe'    : "",'deselect'    : "",'esact': "" },
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      .done(function(data) {
        sessionExpiredReported(data,true);
        if (data.esstatus=="finished")
        {  
          finishEmailHandler(data);
          return;
        }
        if (data.esstatus=="paused")
        {
          processPaused(data);  
          return;
        }
        if (data.esstatus=="cancelled")
        {  
          cancelEmailHandler(data);
          return;
        }
        if (data.esstatus=="error")
        {  
          // Delay 5 seconds before retrying
          setTimeout(function(){ moreEmailHandler(); }, 5000);
          return;
        }
        moreEmailHandler();
        return;
      })

      .fail(function(data) {
        // Delay 5 seconds before retrying
        setTimeout(function(){ moreEmailHandler(); }, 5000);
      })

}

function finishEmailHandler(data)
{
  clearInterval(window.progressInterval);
  if (data.numtoemail>0)
  {
    var p=(data.emailcount/data.numtoemail)*100;
    p=Math.round(p);
  }
  else
    p=100;
  $('#sentprogress').removeClass('active');
  setSentProgress(p);
  $('#numsent').text(data.emailsent+'/'+data.numtoemail);
  $('#numfail').text(data.emailfail);
  $('#numblocked').text(data.emailblocked);
  $('#statustext').text(ADMINMSG_FINISHED);
  $('#sendselected2_pause').hide();  
  $('#sendselected2_continue').hide();  
  $('#sendselected2_cancel').hide();  
  $('#sendselected2_close').show();  
}

function updateProgress()
{
  $.getJSON(SitelokLocationURL+'writefiles/emailselectedprogress.json',function(data)
  {
    if (data.numtoemail>0)
    {
      var p=(data.emailcount/data.numtoemail)*100;
      p=Math.round(p);
    }
    else
      p=100;
    setSentProgress(p);
    $('#numsent').text(data.emailsent+'/'+data.numtoemail);
    $('#numfail').text(data.emailfail);
    $('#numblocked').text(data.emailblocked);
  })
  .error(function(data)
  {
  });
}

function setSentProgress(p)
{
  $("#sentprogress").css('width', p + "%");
  $("#sentprogress").text(p + "%");
}

function pauseClicked()
{
  // Call ajax handler to send email
  var formData = {
      'slcsrf'    : slcsrf,
      'comact'   : "pause",
  };

  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminselectedemailcontrol.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
    .done(function(data) {
      sessionExpiredReported(data,true);
      if (data.success)
      { 
        $('#statustext').text(ADMINMSG_PAUSING);
        $('#sendselected2_pause').prop('disabled', true);
        return;
      }
    })
}

function processPaused(data)
{
  $('#sendselected2_pause').prop('disabled', false);
  $('#sendselected2_pause').hide();  
  $('#sendselected2_continue').show();  
  clearInterval(window.progressInterval);
  if (data.numtoemail>0)
  {
    var p=(data.emailcount/data.numtoemail)*100;
    p=Math.round(p);
  }
  else
    p=100;
  $('#sentprogress').removeClass('active');
  setSentProgress(p);
  $('#numsent').text(data.emailsent+'/'+data.numtoemail);
  $('#numfail').text(data.emailfail);
  $('#numblocked').text(data.emailblocked);
  $('#statustext').text(ADMINMSG_PAUSED);
  paused=true;
}

function continueClicked()
{
  // Call ajax handler to send email
  var formData = {
      'slcsrf'    : slcsrf,
      'comact'   : ""
  };

  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminselectedemailcontrol.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
    .done(function(data) {
      sessionExpiredReported(data,true);
      if (data.success)
      { 
        processContinue();
        return;
      }
    })
}

function processContinue()
{
  progressInterval=setInterval(updateProgress, 2000);
  $('#sendselected2_continue').hide();
  $('#sendselected2_pause').show();
  $('#sentprogress').addClass('active');
  $('#statustext').text(ADMINMSG_SENDINGEMAIL);
  paused=false;
  moreEmailHandler();
}

function cancelClicked()
{
  bootbox.confirm(ADMINMSG_RUSURE, function(result)
  {
    if (result)
    {
      if (paused)
      {
        $('#sendselected2_pause').hide();  
        $('#sendselected2_continue').hide();  
        $('#sendselected2_cancel').prop('disabled', false);
        $('#sendselected2_cancel').hide();  
        $('#sendselected2_close').show(); 
        $('#statustext').text(ADMINMSG_CANCELED);         
      } 
      else
      { 
        // Call ajax handler to send email
        var formData = {
            'slcsrf'    : slcsrf,
            'comact'   : "cancel",
        };
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'adminselectedemailcontrol.php', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
          .done(function(data) {
            sessionExpiredReported(data,true);
            if (data.success)
            { 
              $('#statustext').text(ADMINMSG_CANCELING);
              $('#sendselected2_cancel').prop('disabled', true);
              return;
            }
          })
      }
    }
  });  
}

function closeClicked()
{
  $('#sendSelected2Modal').modal('hide');
}

function cancelEmailHandler(data)
{
  clearInterval(window.progressInterval);
  if (data.numtoemail>0)
  {
    var p=(data.emailcount/data.numtoemail)*100;
    p=Math.round(p);
  }
  else
    p=100;
  $('#sentprogress').removeClass('active');
  setSentProgress(p);
  $('#numsent').text(data.emailsent+'/'+data.numtoemail);
  $('#numfail').text(data.emailfail);
  $('#numblocked').text(data.emailblocked);
  $('#statustext').text(ADMINMSG_CANCELED);
  $('#sendselected2_pause').hide();  
  $('#sendselected2_continue').hide();  
  $('#sendselected2_cancel').prop('disabled', false);
  $('#sendselected2_cancel').hide();  
  $('#sendselected2_close').show();  
}

function getHtmlTop(txt)
{
  var matches=txt.match(/<body.*?>([\s\S]*)<\/body>/i);
  if (matches==null)
    return('');
  bodyHtml=matches[1];
  posstart=txt.indexOf(bodyHtml);
  posend=posstart+bodyHtml.length;
  return(txt.substr(0,posstart));
}

function getHtmlBody(txt)
{
  var matches=txt.match(/<body.*?>([\s\S]*)<\/body>/i);
  if (matches==null)
    bodyHtml='';
  else
    bodyHtml=matches[1];
  return(bodyHtml);
}

function getHtmlBottom(txt)
{
  var matches=txt.match(/<body.*?>([\s\S]*)<\/body>/i);
  if (matches==null)
    return('');
  posstart=txt.indexOf(bodyHtml);
  posend=posstart+bodyHtml.length;
  return(txt.substr(posend));
}

function editorInsertStr(str)
{
  // replace old <!-- and --> with !!!
  str = str.replace('<!--' ,'!!!');
  str = str.replace('-->' ,'!!!');
  if ((templateMode=="html") && (htmlMode=="visual"))
    tinymce.activeEditor.insertContent(str);
  else
  {
    $('#templateeditortext').focus();
    var sel = $("#templateeditortext").getSelection();
    if (sel.text!="")
      $("#templateeditortext").replaceSelectedText(str, "collapseToEnd");
    else
      $("#templateeditortext").insertText(str, sel.end, "collapseToEnd");
  }
}

function editorInsertLink(label,code)
{
  if ((templateMode=="html") && (htmlMode=="visual"))
  {
    var seltxt=tinymce.activeEditor.selection.getContent({format: code});
    if (seltxt=="")
      seltxt=label;
    tinymce.activeEditor.selection.setContent('<a href="'+code+'">'+seltxt+'</a>',{format : 'raw'});
  }
  else
  {
    if (templateMode=="html")
    { 
      $('#templateeditortext').focus();
      var sel = $("#templateeditortext").getSelection();
      if (sel.text!="")
        $("#templateeditortext").replaceSelectedText('<a href="'+code+'">'+sel.text+'</a>', "collapseToEnd");
      else
        $("#templateeditortext").insertText('<a href="'+code+'">'+label+'</a>', sel.end, "collapseToEnd");
    }
    else
    {
      $('#templateeditortext').focus();
      var sel = $("#templateeditortext").getSelection();
      if (sel.text!="")
        $("#templateeditortext").replaceSelectedText(code, "collapseToEnd");
      else
        $("#templateeditortext").insertText(code, sel.end, "collapseToEnd");
    }  
  }
}

function focusEditor()
{
  if ((templateMode=="html") && (htmlMode=="visual"))
    tinymce.activeEditor.focus();
  else  
    $('#templateeditortext').focus();
}

function fixContent(content)
{
  // Fix links containing currentpath!!! by replacing with just !!!
  content=content.split(currentpath+'!!!').join('!!!')
  return(content);
}




