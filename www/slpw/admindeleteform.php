<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value  
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError(ADMINMSG_CSRFFAILED);
    exit;      
  }
  $formid=$_POST['formid'];
  $formtype="register";
  if ($_POST['formtype']=="register")
    $formtype="register";
  if ($_POST['formtype']=="update")
    $formtype="update";
  if ($_POST['formtype']=="login")
    $formtype="login";
  if ($_POST['formtype']=="contact")
    $formtype="contact";
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError(ADMINMSG_MYSQLERROR);
    exit;
  }
  if (!is_numeric($formid))
  {
    returnError(ADMINET_NODELETE);
    exit;
  }
  $query="DELETE FROM sl_forms WHERE id=".sl_quote_smart($formid);
  if (!$DemoMode)
    $mysql_result=mysqli_query($mysql_link,$query);
  else
    $mysql_result=true;
  if ($mysql_result==false)
  {
    returnError(ADMINET_NODELETE);
    exit;
  }
  if ($formtype=="register")
    $query="DELETE FROM sl_registerforms WHERE id=".sl_quote_smart($formid);
  if ($formtype=="update")
    $query="DELETE FROM sl_updateforms WHERE id=".sl_quote_smart($formid);
  if ($formtype=="login")
    $query="DELETE FROM sl_loginforms WHERE id=".sl_quote_smart($formid);
  if ($formtype=="contact")
    $query="DELETE FROM sl_contactforms WHERE id=".sl_quote_smart($formid);
  if (!$DemoMode)
    $mysql_result=mysqli_query($mysql_link,$query);
  else
    $mysql_result=true;
  if ($mysql_result==false)
  {
    returnError(ADMINET_NODELETE);
    exit;
  }

  function returnError($msg)
  {
    $data['success'] = false;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }


?>  
{
  "success": true,
  "message": ""
}


