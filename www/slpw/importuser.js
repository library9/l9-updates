// Enable select boxes
$('.selectpicker').selectpicker({
  style: 'btn-default',
  size: 4,
  dropupAuto: false  
});


// Handle file browser
// Thanks to https://www.abeautifulsite.net/whipping-file-inputs-into-shape-with-bootstrap-3
$(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }

      });
  });
  
});

// Validation and saving for add user form
$('#importuserform').submit(function(event) {
        // process the form
        // remove the error classes
        $('#slimportedusersdiv').removeClass('has-error');
        for (k=1;k<groupcount;k++)
        {
          if ( !$( "#group"+k ).length )
            continue;
          $('#group'+k+'div').removeClass('has-error');
          $('#expiry'+k+'div').removeClass('has-error');
        }
         // remove the error messages
        $('#slimportedusershelp').remove();
        for (k=1;k<groupcount;k++)
        {
          if ( !$( "#group"+k ).length )
            continue;
          $('#group'+k+'help').remove();
          $('#expiry'+k+'help').remove();
        }
        //Remove form message
        $('#resultimportuser').html('');
        // Remove results
        $('#importdata').hide();
        // Validate using JS first
        var data={success:true,message:'',errors:{}};
        var slimportedusers=$( "#slimportedusers" ).val().trim();
        if (slimportedusers=="")
          data.errors.slimportedusers=ADMINIU_NOFILE;
        for (k=1;k<groupcount;k++)
        {
          // Ignore if field not present
          if ( !$( "#group"+k ).length )
            continue;
          var gstr=$( "#group"+k ).val();
          if (!validUsergroup(gstr,false))
            eval("data.errors.group"+k+"=ADMINMSG_GROUPINVALID");
          estr=$( "#expiry"+k ).val();
          if ((estr!="") && (!validChars("0123456789",estr)))
            eval("data.errors.expiry"+k+"=ADMINMSG_EXPIRYINVALID");
          if (estr.length==6)
          {
            if (!dateValid(estr,DateFormat))
              eval("data.errors.expiry"+k+"=ADMINMSG_EXPIRYDATEINVALID");
          }
          if ((gstr=="") && (estr!=""))
            eval("data.errors.group"+k+"=ADMINMSG_GROUPINVALID");
        }
        if (Object.keys(data.errors).length>0)
        {
          data.success=false;
          data.message=ADMINMSG_FORMERROR;
          //data=JSON.stringify(data);
          // Show validation results from JS in form
          showValidation(data,'resultimportuser');
          // stop the form from submitting the normal way and refreshing the page
          event.preventDefault();
          return;
        }      
        // Now call PHP for final validation and processing
        var label=showButtonBusy('submit');

        var form = $('form')[0]; // You need to use standart javascript object here
        var formData = new FormData(form);
        formData.append('groupcount', groupcount-1);
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'adminimportuser.php', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            contentType: false,
            processData: false
        })
            // using the done promise callback
            .done(function(data) {
              sessionExpiredReported(data,true);
              // Show validation results from PHP in form
              if (!data.success)
                showValidation(data,'resultimportuser');
              hideButtonBusy('submit',label);
              if (data.success)
              {
                // Display results and log
                var importdata="";
                importdata+="<p>"+data.totalrecords+" "+ADMINIU_USERSREAD+"</p>";
                importdata+="<p>"+data.usersadded+" "+ADMINIU_USERSADD+"</p>";
                if (data.usersignored>0)
                  importdata+="<p class=\"importdatawarn\">"+data.usersignored+" "+ADMINIU_USERSIGN+" &nbsp;&nbsp;<a href=\"#\" data-toggle=\"modal\" data-target=\"#logModal\">"+ADMINIU_VIEW+"</a></p>";
                importdata+="<p>"+data.usersupdated+" "+ADMINIU_USERUPD+"</p>";
                $('#importdata').html(importdata);
                if (data.log.length>0)
                {
                  var logcontent="";
                  for (k=0;k<data.log.length;k++)
                  {
                    logcontent+="<p>"+data.log[k]+"</p>\n";
                  }
                  $('#logcontent').html(logcontent);
                }                  
                $('#importdata').show();
              }
            })

// using the fail promise callback
    .fail(function(data) {
    hideButtonBusy('submit',label);  
    $('#resultimportuser').html('<div id="resultimportusermessage" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
//        console.log(data);
    });


        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });



function addGroup()
{
  var code=$('#extraGroupTemplate').html();
  code=code.replace(/XX/g, groupcount);
  code=code.replace(/<!--/g, '');
  code=code.replace(/-->/g, '');
  $('#extraGroupInsert').before(code);
  $('#group'+groupcount).editableSelect();
  // Enable calendar
  $('#dateexpiry'+groupcount).datetimepicker({'keepInvalid': true, 'format':DateFormat,'defaultDate':'', 'useStrict': true});
  groupcount++;
}

function removeGroup(num)
{
  $('#groupblock'+num).remove();
}

function groupUp(num)
{
  if (num==1)
    return;
  var prev=0;
  for (var k=(num-1);k>=1;k--)
  {
    if ($("#groupblock"+k).length)
    {
      prev=k;
      break;
    }
  }
  if (prev==0)
    return;
  var tmp=$("#group"+prev).val()
  $("#group"+prev).val($("#group"+num).val())
  $("#group"+num).val(tmp)
  var tmp=$("#expiry"+prev).val()
  $("#expiry"+prev).val($("#expiry"+num).val())
  $("#expiry"+num).val(tmp)
}

function groupDown(num)
{
  if (num==(groupcount-1))
    return;
  var next=0;
  for (var k=(num+1);k<groupcount;k++)
  {
    if ($("#groupblock"+k).length)
    {
      next=k;
      break;
    }
  }
  if (next==0)
    return;
  var tmp=$("#group"+next).val()
  $("#group"+next).val($("#group"+num).val())
  $("#group"+num).val(tmp)
  var tmp=$("#expiry"+next).val()
  $("#expiry"+next).val($("#expiry"+num).val())
  $("#expiry"+num).val(tmp)
}
