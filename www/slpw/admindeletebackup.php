<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value  
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    ?>
    {
    "success": false,
    "message": "<?php echo ADMINMSG_CSRFFAILED; ?>"
    }
    <?php
    exit;      
  }
  $filename=$_POST['fname'];
  // Double check path doesn't start with / or \ or contain .. or :
  if ((substr($filename,0,1)=="/") || (substr($filename,0,1)=="\\") || (false!=(strpos($filename, ".."))) || (false!=(strpos($filename, ":"))))
  {
  ?>
  {
    "success": false,
    "message": "<?php echo ADMINMSG_TEMPLATENOLOAD; ?>"
  }
  <?php
  exit;
  }
  // Check valid file extension
  $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
  if (($ext!="sql") && ($ext!="gz"))
  {
  ?>
  {
    "success": false,
    "message": "<?php echo ADMINBACKUP_NODELETE.$ext; ?>"
  }

  <?php
  exit;
  }
  if (file_exists($BackupLocation.$filename))
  {
    if (false===@unlink($BackupLocation.$filename))
    {
    ?>
    {
      "success": false,
      "message": "<?php echo ADMINBACKUP_NODELETE; ?>"
    }

    <?php
    exit;
    }
  }    
  ?>
{
"success": true,
"message": ""
}
