<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-type" content="text/html;charset=<?php echo $MetaCharSet; ?>">
<title>Please Login</title>
<?php
$usedbsettings=false;
if (basename(__FILE__)=="logintemplatedefault.php")
{
  require("dblogindefaults.php");
  return;
}  
else   
{
?>
<style type="text/css">

html, body { margin: 0; padding: 0; }

body{
  background-color: white;
}

div#topspace {
  width: 100%;
  height: 0px;
}

div#loginbox {
  width: 99%;
  max-width: 420px;
  min-width: 320px;
  margin-left: auto;
  margin-right: auto;
  padding-top: 10px;
  padding-bottom: 20px;
  padding-left: 0;
  padding-right: 0;
  
  /* Background gradient from http://www.colorzilla.com/gradient-editor/ */
  background: #1a305e; /* Old browsers */
  /* IE9 SVG, needs conditional override of 'filter' to 'none' */
  background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzFhMzA1ZSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMyZjVkYWEiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
  background: -moz-linear-gradient(top,  #1a305e 0%, #2f5daa 100%); /* FF3.6+ */
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#1a305e), color-stop(100%,#2f5daa)); /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top,  #1a305e 0%,#2f5daa 100%); /* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(top,  #1a305e 0%,#2f5daa 100%); /* Opera 11.10+ */
  background: -ms-linear-gradient(top,  #1a305e 0%,#2f5daa 100%); /* IE10+ */
  background: linear-gradient(to bottom,  #1a305e 0%,#2f5daa 100%); /* W3C */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a305e', endColorstr='#2f5daa',GradientType=0 ); /* IE6-8 */
  /* End of gradient */

  -webkit-box-shadow: 5px 5px 7px 0px rgba(50, 50, 50, 0.75);
  -moz-box-shadow:    5px 5px 7px 0px rgba(50, 50, 50, 0.75);
  box-shadow:         5px 5px 7px 0px rgba(50, 50, 50, 0.75);

  -webkit-border-radius: 10px;
  -moz-border-radius: 10px;
  border-radius: 10px;
}

div#innerbox {
  width: 80%;
  margin-left: auto;
  margin-right: auto;
  padding: 0;
}

div#loginbox h1{
  color: white;
	font: bold 44px Arial, Helvetica, sans-serif;
	text-align: center;
	padding: 0;
	margin: 10px 0 0 0;
}

input#username{
  background: url('<?php echo $SitelokLocationURLPath; ?>username.png') 5px 3px no-repeat;
  background-color: white;
  background-size: 29px 80%;
  width: 85%;
  padding-top: 10px;
  padding-bottom: 10px;  
  padding-left: 45px;
  color: #1a305e;
	font: bold 16px Arial, Helvetica, sans-serif;
	border: none;
  }

input#password{
  background: url('<?php echo $SitelokLocationURLPath; ?>password.png') 5px 3px no-repeat;
  background-color: white;
  background-size: 29px 80%;
  width: 85%;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 45px;
  color: #1a305e;
	font: bold 16px Arial, Helvetica, sans-serif;
	border: none;
}

input#turing{
  background: url('<?php echo $SitelokLocationURLPath; ?>turingimage.php?w=80&h=40') 0px 0px no-repeat;
  background-size: 80px 100%;
  background-color: white;
  width: 72%;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 90px;
  color: #1a305e;
	font: bold 16px Arial, Helvetica, sans-serif;
	border: none;
}

input#remember{
  width: 20px;
  height : 20px;
  border: none;
  margin: 17px 0 0 10px;
}

div#loginbox label{
  display: block;
  clear: both;
  color: white;
	font: bold 18px Arial, Helvetica, sans-serif;
  margin: 15px 0 2px 0 ;
  padding: 0;
  float: left;
}

div#loginbox label.labelcb{
  display: inline;
  clear: none;
}

a#forgot{
  color: white;
	font: bold 14px Arial, Helvetica, sans-serif;
	text-align: left;
	vertical-align: middle;
	padding: 0;
	margin-left: 20px;
  margin-top: -20px;
}

p#loginmessage{
  color: red;
	font: bold 16px Arial, Helvetica, sans-serif;
	text-align: left;
	padding: 0;
	margin: 15px 0 0 0;
}

p#signup{
	text-align: center;
  padding: 0;
	margin: 15px 0 0 0;
}

p#signup a{
  color: orange;
	font: bold 16px Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
}

/* Button from http://www.bestcssbuttongenerator.com */
.myButton {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #378ee5), color-stop(1, #79bcff));
	background:-moz-linear-gradient(top, #378ee5 5%, #79bcff 100%);
	background:-webkit-linear-gradient(top, #378ee5 5%, #79bcff 100%);
	background:-o-linear-gradient(top, #378ee5 5%, #79bcff 100%);
	background:-ms-linear-gradient(top, #378ee5 5%, #79bcff 100%);
	background:linear-gradient(to bottom, #378ee5 5%, #79bcff 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#378ee5', endColorstr='#79bcff',GradientType=0);
	background-color:#378ee5;
	-moz-border-radius:10px;
	-webkit-border-radius:10px;
	border-radius:10px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
  font: normal normal bold 17px Arial, Helvetica, sans-serif;
	padding:8px 31px;
	text-decoration:none;
	border: none;
/*
	text-shadow:0px 1px 0px #ffffff;
*/
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #79bcff), color-stop(1, #378ee5));
	background:-moz-linear-gradient(top, #79bcff 5%, #378ee5 100%);
	background:-webkit-linear-gradient(top, #79bcff 5%, #378ee5 100%);
	background:-o-linear-gradient(top, #79bcff 5%, #378ee5 100%);
	background:-ms-linear-gradient(top, #79bcff 5%, #378ee5 100%);
	background:linear-gradient(to bottom, #79bcff 5%, #378ee5 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#79bcff', endColorstr='#378ee5',GradientType=0);
	background-color:#79bcff;
}
.myButton:active {
	position:relative;
	top:1px;
}
/* End of button */

/* busy spinner */
div#slspinner {
  display: none;
  box-sizing: content-box;  
  float: right;
  height: 12px;
  width: 12px;
  margin-left: 0px;
  margin-right: -22px;
  margin-top: 2px;
  position: relative;
  -webkit-animation: rotation .6s infinite linear;
  -moz-animation: rotation .6s infinite linear;
  -o-animation: rotation .6s infinite linear;
  animation: rotation .6s infinite linear;
  border-left: 2px solid rgba(255,255,255, .3);
  border-right: 2px solid rgba(255,255,255, .3);
  border-bottom: 2px solid rgba(255,255,255, .3);
  border-top: 2px solid rgba(255,255,255, 1);
  border-radius: 100%;
}

@-webkit-keyframes rotation {
  from {
    -webkit-transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(359deg);
  }
}

@-moz-keyframes rotation {
  from {
    -moz-transform: rotate(0deg);
  }
  to {
    -moz-transform: rotate(359deg);
  }
}

@-o-keyframes rotation {
  from {
    -o-transform: rotate(0deg);
  }
  to {
    -o-transform: rotate(359deg);
  }
}

@keyframes rotation {
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(359deg);
  }
}

</style>
<?php } ?>
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->

<!--[if lte IE 6]>
  <style type="text/css">
    div#loginbox {
    width: 420px;
    }
  </style>
<![endif]-->
<?php
if (($TuringLogin==1) && (function_exists('slcaptchahead')))
  echo slcaptchahead();
?>
</head>
<body>
<div id="topspace"></div>
<div id="loginbox" class="gradient">
<form name="siteloklogin" action="<?php echo $startpage; ?>" method="post" <?php if ($LoginType=="SECURE") echo " autocomplete=\"off\""; ?>  onSubmit="return validatelogin()" >
<?php siteloklogin(); ?>
<div id="innerbox">
<h1>Login</h1>
<p id="loginmessage"><?php if ($msg!="") echo $msg; ?></p>
<label for="username">Username</label>
<input type="text" name="username" id="username" value="<?php echo $slcookieusername; ?>" placeholder="username" maxlength="50" tabindex="1" autocorrect="off" autocapitalize="off"  spellcheck="off" autofocus="autofocus">

<label for="password">Password</label>
<input type="password" name="password" id="password" value="<?php echo $slcookiepassword; ?>" placeholder="password" maxlength="50" tabindex="2" autocorrect="off" autocapitalize="off" spellcheck="off">

<?php if ($TuringLogin==1) { ?>
<label for="turing"><?php echo $row['captcha']; ?></label>
<?php
if (function_exists('slshowcaptcha'))
{
  print "<div style=\"display: block;clear: both;\">\n";
  echo slshowcaptcha();
  print "</div>\n";
}  
else
{
?>
<input type="text" name="turing" id="turing" value="" placeholder="captcha" maxlength="5" tabindex="3" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="off">
<?php
}
}
?>
<?php if ($CookieLogin==1) { ?>
<label class="labelcb" for="remember">Remember Me</label>
<input type="checkbox" name="remember" id="remember"  value="1" <?php if ($slcookielogin=="1") echo "checked"; ?> tabindex="4">
<?php } ?>

<?php if ($CookieLogin==2) { ?>
<label class="labelcb" for="remember">Auto Login</label>
<input type="checkbox" name="remember" id="remember"  value="2" <?php if ($slcookielogin=="2") echo "checked"; ?> tabindex="4">
<?php } ?>

<div style="display: block;clear: both;margin-top: 15px;"></div>
<button type="submit" class="myButton" id="myButton" tabindex="5" >Login<div id="slspinner"></div></button>
<a id="forgot" href="javascript: void forgotpw()" title="Forgot your password? Enter username or email &amp; click link">Forgot Password</a>

<?php if (($signupurl!="") && ($signuptext!="")) { ?>
<p id="signup"><a href="<?php echo $signupurl; ?>"><?php echo $signuptext; ?></a></p>
<?php } ?>
</div>
</form>
</div>
<script type="text/javascript">
centerLogin();
var obj=document.getElementById("username")
obj.focus()
window.onresize=centerLogin;
function centerLogin()
{
  var dsocheight = document.documentElement.clientHeight || window.innerHeight || document.getElementsByTagName('body')[0].clientHeight;
  var loginboxheight = document.getElementById('loginbox').clientHeight;
  var newtop=0;
  newtop=Math.floor((dsocheight-loginboxheight)/2);
  document.getElementById('topspace').style.height=newtop+'px';
}  
</script>
</body>
</html>
