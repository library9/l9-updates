<?php
@error_reporting (E_ERROR);
require("slconfig.php");
// Clear any existing session exists
if ($SessionName!="")
  session_name($SessionName);
session_start();
@session_destroy();
if ($sl_cookiehttponly)
  setcookie(session_name(), '', time()-42000, '/',$sl_cookiedomain,$sl_cookiesecure,true);
else
  setcookie(session_name(), '', time()-42000, '/',$sl_cookiedomain,$sl_cookiesecure);      
$installversion="5.1";
if(get_magic_quotes_runtime())
  set_magic_quotes_runtime (0);
// Get full server path to slpw folder
$slpwpath=str_replace(basename(__FILE__), '', realpath(__FILE__));
$slpwpath=str_replace("\\","/",$slpwpath);
if ((!isset($DbLogTableName)) || ($DbLogTableName==""))
  $TempLogTableName="log";
else  
  $TempLogTableName=$DbLogTableName;

if ((!isset($DbGroupTableName)) || ($DbGroupTableName==""))
  $TempGroupTableName="usergroups";
else
  $TempGroupTableName=$DbGroupTableName;

if ((!isset($DbConfigTableName)) || ($DbConfigTableName==""))
  $TempConfigTableName="slconfig";
else
  $TempConfigTableName=$DbConfigTableName;
  
if ((!isset($DbPluginsTableName)) || ($DbPluginsTableName==""))
  $TempPluginsTableName="sl_plugins";

else
  $TempPluginsTableName=$DbPluginsTableName;

if ((!isset($UsernameField)) || ($UsernameField==""))
  $TempUsernameField="Username";
else
  $TempUsernameField=$UsernameField;

if ((!isset($PasswordField)) || ($PasswordField==""))
  $TempPasswordField="Passphrase";
else
  $TempPasswordField=$PasswordField;

if ((!isset($EmailField)) || ($EmailField==""))
  $TempEmailField="Email";
else
  $TempEmailField=$EmailField;

if ((!isset($NameField)) || ($NameField==""))
  $TempNameField="Name";
else
  $TempNameField=$NameField;


$comments="";
// First check that PHP version is 5.3.7 or above and mysqli library is available
if (version_compare(phpversion(), '5.3.7', '<'))
{
  $comments.="This version of Sitelok requires PHP 5.3.7 or greater<br>\n";
  DisplayComments($comments);
  exit;
}
if (!function_exists('mysqli_query'))
{
  $comments.="This version of Sitelok requires the mysqli library which should be included by default. Check with your hosting company.<br>\n";
  DisplayComments($comments);
  exit;
}
// Connect to database
$pos=strpos($DbHost,":");
if ($pos!==false)
  $mysql_link=mysqli_connect(substr($DbHost,0,$pos),$DbUser,$DbPassword,$DbName,substr($DbHost,$pos+1));
else
  $mysql_link=mysqli_connect($DbHost,$DbUser,$DbPassword,$DbName);    
if ($mysql_link===false)
{
  $comments.="Can't connect to MySQL server. Please check the settings in slconfig.php.<br>\n";
  DisplayComments($comments);
  exit;
}
/*
$db=mysqli_select_db($mysql_link,$DbName);
if ($db===false)
{
  mysqli_close($mysql_link);
  $comments.="Can't open database. Please check the settings in slconfig.php.<br>\n";
  DisplayComments($comments);
  exit;
}
*/
// Make sure that existing Sitelok table is there
if (!tableexists($mysql_link, $DbTableName, "Selected"))
{
  mysqli_close($mysql_link);
  $comments.="Can't find existing Sitelok table. Please double check current installation.<br>\n";
  DisplayComments($comments);
  exit;  
}

// Add extra 40 custom fields if early version with just 10
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName. " LIMIT 1");
if (mysqli_num_fields($mysql_result)<20)
{
  // Add the extra custom fields
  $query ="ALTER TABLE ".$DbTableName." ADD Custom11 VARCHAR(255) NOT NULL default '' AFTER Custom10, ";
  $query.="ADD Custom12 VARCHAR(255) NOT NULL default '' AFTER Custom11, ";
  $query.="ADD Custom13 VARCHAR(255) NOT NULL default '' AFTER Custom12, ";
  $query.="ADD Custom14 VARCHAR(255) NOT NULL default '' AFTER Custom13, ";
  $query.="ADD Custom15 VARCHAR(255) NOT NULL default '' AFTER Custom14, ";
  $query.="ADD Custom16 VARCHAR(255) NOT NULL default '' AFTER Custom15, ";
  $query.="ADD Custom17 VARCHAR(255) NOT NULL default '' AFTER Custom16, ";
  $query.="ADD Custom18 VARCHAR(255) NOT NULL default '' AFTER Custom17, ";
  $query.="ADD Custom19 VARCHAR(255) NOT NULL default '' AFTER Custom18, ";
  $query.="ADD Custom20 VARCHAR(255) NOT NULL default '' AFTER Custom19, ";
  $query.="ADD Custom21 VARCHAR(255) NOT NULL default '' AFTER Custom20, ";
  $query.="ADD Custom22 VARCHAR(255) NOT NULL default '' AFTER Custom21, ";
  $query.="ADD Custom23 VARCHAR(255) NOT NULL default '' AFTER Custom22, ";
  $query.="ADD Custom24 VARCHAR(255) NOT NULL default '' AFTER Custom23, ";
  $query.="ADD Custom25 VARCHAR(255) NOT NULL default '' AFTER Custom24, ";
  $query.="ADD Custom26 VARCHAR(255) NOT NULL default '' AFTER Custom25, ";
  $query.="ADD Custom27 VARCHAR(255) NOT NULL default '' AFTER Custom26, ";
  $query.="ADD Custom28 VARCHAR(255) NOT NULL default '' AFTER Custom27, ";
  $query.="ADD Custom29 VARCHAR(255) NOT NULL default '' AFTER Custom28, ";
  $query.="ADD Custom30 VARCHAR(255) NOT NULL default '' AFTER Custom29, ";
  $query.="ADD Custom31 VARCHAR(255) NOT NULL default '' AFTER Custom30, ";
  $query.="ADD Custom32 VARCHAR(255) NOT NULL default '' AFTER Custom31, ";
  $query.="ADD Custom33 VARCHAR(255) NOT NULL default '' AFTER Custom32, ";
  $query.="ADD Custom34 VARCHAR(255) NOT NULL default '' AFTER Custom33, ";
  $query.="ADD Custom35 VARCHAR(255) NOT NULL default '' AFTER Custom34, ";
  $query.="ADD Custom36 VARCHAR(255) NOT NULL default '' AFTER Custom35, ";
  $query.="ADD Custom37 VARCHAR(255) NOT NULL default '' AFTER Custom36, ";
  $query.="ADD Custom38 VARCHAR(255) NOT NULL default '' AFTER Custom37, ";
  $query.="ADD Custom39 VARCHAR(255) NOT NULL default '' AFTER Custom38, ";
  $query.="ADD Custom40 VARCHAR(255) NOT NULL default '' AFTER Custom39, ";
  $query.="ADD Custom41 VARCHAR(255) NOT NULL default '' AFTER Custom40, ";
  $query.="ADD Custom42 VARCHAR(255) NOT NULL default '' AFTER Custom41, ";
  $query.="ADD Custom43 VARCHAR(255) NOT NULL default '' AFTER Custom42, ";
  $query.="ADD Custom44 VARCHAR(255) NOT NULL default '' AFTER Custom43, ";
  $query.="ADD Custom45 VARCHAR(255) NOT NULL default '' AFTER Custom44, ";
  $query.="ADD Custom46 VARCHAR(255) NOT NULL default '' AFTER Custom45, ";
  $query.="ADD Custom47 VARCHAR(255) NOT NULL default '' AFTER Custom46, ";
  $query.="ADD Custom48 VARCHAR(255) NOT NULL default '' AFTER Custom47, ";
  $query.="ADD Custom49 VARCHAR(255) NOT NULL default '' AFTER Custom48, ";
  $query.="ADD Custom50 VARCHAR(255) NOT NULL default '' AFTER Custom49";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add extra custom fields to the table '$DbTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }    
}
// Add Session field if it doesn't exist already
if (!fieldexists($mysql_link,$DbTableName,"Session"))
{
  $query ="ALTER TABLE ".$DbTableName." ADD Session VARCHAR(255)";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'Session' to the table '$DbTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
}
// Create log table
if (!tableexists($mysql_link,$TempLogTableName, "id"))
{
  $query="CREATE TABLE `".$TempLogTableName."` (
  `id` int(11) NOT NULL auto_increment,
  `time` datetime NOT NULL,
  `username` varchar(100) NOT NULL default '',
  `type` varchar(30) NOT NULL default '',
  `details` varchar(255) NOT NULL default '',
  `ip` varchar(46) NOT NULL default '',
  `session` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create the table '$TempLogTableName'. The following error was reported by MySql<br><br>\n";
    $comments.="$err"."<br><br>\n";
    DisplayComments($comments);
    exit;
  }
}

// create configuration table
if (!tableexists($mysql_link,$TempConfigTableName,"confignum"))
{
  $query="CREATE TABLE `".$TempConfigTableName."` (
  `confignum` int(11) NOT NULL default '1',
  `version` varchar(5) NOT NULL default '',
  `sitename` varchar(255) NOT NULL default 'Members Area',
  `siteemail` varchar(255) NOT NULL default 'you@yoursite.com',
  `dateformat` varchar(6) NOT NULL default 'DDMMYY',
  `logoutpage` varchar(255) NOT NULL default 'http://www.yoursite.com/index.html',
  `siteloklocation` varchar(255) NOT NULL default '',
  `siteloklocationurl` varchar(255) NOT NULL default '',
  `emaillocation` varchar(255) NOT NULL default '',
  `emailurl` varchar(255) NOT NULL default '',
  `filelocation` mediumtext,
  `siteloklog` varchar(255) NOT NULL default '',
  `logdetails` varchar(50) NOT NULL default 'YYYYYYYYYYYYYYYY',
  `sitekey` varchar(50) NOT NULL default '',
  `logintype` varchar(10) NOT NULL default 'NORMAL',
  `turinglogin` int(11) NOT NULL default '0',
  `turingregister` int(11) NOT NULL default '0',
  `maxsessiontime` int(11) NOT NULL default '0',
  `maxinactivitytime` int(11) NOT NULL default '0',
  `cookielogin` int(11) NOT NULL default '0',
  `logintemplate` varchar(255) NOT NULL default '',
  `expiredpage` varchar(255) NOT NULL default '',
  `wronggrouppage` varchar(255) NOT NULL default '',
  `messagetemplate` varchar(255) NOT NULL default '',
  `forgottenemail` varchar(255) NOT NULL default '',
  `newuseremail` varchar(255) NOT NULL default 'newuser.htm',
  `showrows` int(11) NOT NULL default '10',
  `customintable` int(11) NOT NULL default '1',
  `randompasswordmask` varchar(50) NOT NULL default 'cccc##',
  `md5passwords` int(11) NOT NULL default '0',
  `concurrentlogin` int(11) NOT NULL default '1',
  `logviewoffset` int(11) NOT NULL default '0',
  `logvieworder` varchar(4) NOT NULL default 'ASC',
  `logviewdetails` varchar(50) NOT NULL default 'YYYYYYYYYYYYYYYY',
  `sortfield` varchar(100) NOT NULL default '',
  `sortdirection` varchar(4) NOT NULL default '',
  `customtitle1` varchar(255) NOT NULL default 'Custom1',
  `customtitle2` varchar(255) NOT NULL default 'Custom2',
  `customtitle3` varchar(255) NOT NULL default 'Custom3',
  `customtitle4` varchar(255) NOT NULL default '',
  `customtitle5` varchar(255) NOT NULL default '',
  `customtitle6` varchar(255) NOT NULL default '',
  `customtitle7` varchar(255) NOT NULL default '',
  `customtitle8` varchar(255) NOT NULL default '',
  `customtitle9` varchar(255) NOT NULL default '',
  `customtitle10` varchar(255) NOT NULL default '',
  `customtitle11` varchar(255) NOT NULL default '',
  `customtitle12` varchar(255) NOT NULL default '',
  `customtitle13` varchar(255) NOT NULL default '',
  `customtitle14` varchar(255) NOT NULL default '',
  `customtitle15` varchar(255) NOT NULL default '',
  `customtitle16` varchar(255) NOT NULL default '',
  `customtitle17` varchar(255) NOT NULL default '',
  `customtitle18` varchar(255) NOT NULL default '',
  `customtitle19` varchar(255) NOT NULL default '',
  `customtitle20` varchar(255) NOT NULL default '',
  `customtitle21` varchar(255) NOT NULL default '',
  `customtitle22` varchar(255) NOT NULL default '',
  `customtitle23` varchar(255) NOT NULL default '',
  `customtitle24` varchar(255) NOT NULL default '',
  `customtitle25` varchar(255) NOT NULL default '',
  `customtitle26` varchar(255) NOT NULL default '',
  `customtitle27` varchar(255) NOT NULL default '',
  `customtitle28` varchar(255) NOT NULL default '',
  `customtitle29` varchar(255) NOT NULL default '',
  `customtitle30` varchar(255) NOT NULL default '',
  `customtitle31` varchar(255) NOT NULL default '',
  `customtitle32` varchar(255) NOT NULL default '',
  `customtitle33` varchar(255) NOT NULL default '',
  `customtitle34` varchar(255) NOT NULL default '',
  `customtitle35` varchar(255) NOT NULL default '',
  `customtitle36` varchar(255) NOT NULL default '',
  `customtitle37` varchar(255) NOT NULL default '',
  `customtitle38` varchar(255) NOT NULL default '',
  `customtitle39` varchar(255) NOT NULL default '',
  `customtitle40` varchar(255) NOT NULL default '',
  `customtitle41` varchar(255) NOT NULL default '',
  `customtitle42` varchar(255) NOT NULL default '',
  `customtitle43` varchar(255) NOT NULL default '',
  `customtitle44` varchar(255) NOT NULL default '',
  `customtitle45` varchar(255) NOT NULL default '',
  `customtitle46` varchar(255) NOT NULL default '',
  `customtitle47` varchar(255) NOT NULL default '',
  `customtitle48` varchar(255) NOT NULL default '',
  `customtitle49` varchar(255) NOT NULL default '',
  `customtitle50` varchar(255) NOT NULL default '',
  `custom1validate` int(11) NOT NULL default '0',
  `custom2validate` int(11) NOT NULL default '0',
  `custom3validate` int(11) NOT NULL default '0',
  `custom4validate` int(11) NOT NULL default '0',
  `custom5validate` int(11) NOT NULL default '0',
  `custom6validate` int(11) NOT NULL default '0',
  `custom7validate` int(11) NOT NULL default '0',
  `custom8validate` int(11) NOT NULL default '0',
  `custom9validate` int(11) NOT NULL default '0',
  `custom10validate` int(11) NOT NULL default '0',
  `custom11validate` int(11) NOT NULL default '0',
  `custom12validate` int(11) NOT NULL default '0',
  `custom13validate` int(11) NOT NULL default '0',
  `custom14validate` int(11) NOT NULL default '0',
  `custom15validate` int(11) NOT NULL default '0',
  `custom16validate` int(11) NOT NULL default '0',
  `custom17validate` int(11) NOT NULL default '0',
  `custom18validate` int(11) NOT NULL default '0',
  `custom19validate` int(11) NOT NULL default '0',
  `custom20validate` int(11) NOT NULL default '0',
  `custom21validate` int(11) NOT NULL default '0',
  `custom22validate` int(11) NOT NULL default '0',
  `custom23validate` int(11) NOT NULL default '0',
  `custom24validate` int(11) NOT NULL default '0',
  `custom25validate` int(11) NOT NULL default '0',
  `custom26validate` int(11) NOT NULL default '0',
  `custom27validate` int(11) NOT NULL default '0',
  `custom28validate` int(11) NOT NULL default '0',
  `custom29validate` int(11) NOT NULL default '0',
  `custom30validate` int(11) NOT NULL default '0',
  `custom31validate` int(11) NOT NULL default '0',
  `custom32validate` int(11) NOT NULL default '0',
  `custom33validate` int(11) NOT NULL default '0',
  `custom34validate` int(11) NOT NULL default '0',
  `custom35validate` int(11) NOT NULL default '0',
  `custom36validate` int(11) NOT NULL default '0',
  `custom37validate` int(11) NOT NULL default '0',
  `custom38validate` int(11) NOT NULL default '0',
  `custom39validate` int(11) NOT NULL default '0',
  `custom40validate` int(11) NOT NULL default '0',
  `custom41validate` int(11) NOT NULL default '0',
  `custom42validate` int(11) NOT NULL default '0',
  `custom43validate` int(11) NOT NULL default '0',
  `custom44validate` int(11) NOT NULL default '0',
  `custom45validate` int(11) NOT NULL default '0',
  `custom46validate` int(11) NOT NULL default '0',
  `custom47validate` int(11) NOT NULL default '0',
  `custom48validate` int(11) NOT NULL default '0',
  `custom49validate` int(11) NOT NULL default '0',
  `custom50validate` int(11) NOT NULL default '0',
  `emailtype` int(11) NOT NULL default '0',
  `emailusername` varchar(255) NOT NULL default '',
  `emailpassword` varchar(255) NOT NULL default '',
  `emailserver` varchar(255) NOT NULL default '',
  `emaildelay` int(11) NOT NULL default '0',
  `modifyuseremail` varchar(255) NOT NULL default 'updateuser.htm',
  `noaccesspage` varchar(255) NOT NULL default '',
  `dbupdate` int(11) NOT NULL default '0',
  `siteemail2` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`confignum`)) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.="$err"."<br><br>\n";
    DisplayComments($comments);
    exit;
  }
}

// Create usergroups table
if (!tableexists($mysql_link,$TempGroupTableName,"name"))
{
  $query="CREATE TABLE `".$TempGroupTableName."` (
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL default '',
  `loginaction` varchar(255) NOT NULL default '',
  `loginvalue` varchar(255) NOT NULL default '',
  UNIQUE KEY `name` (`name`)) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create the table '$TempGroupTableName'. The following error was reported by MySql<br><br>\n";
    $comments.="$err"."<br><br>\n";
    DisplayComments($comments);
    exit;
  }
}

// Add usergroups to table if not already created
if (numberofentries($mysql_link,$TempGroupTableName)<1)
{
  // Use data from slconfig.php as current version is <V2.00
  if (!array_key_exists("ADMIN",$GroupNames))
    $GroupNames['ADMIN']="Administration";
  if (!array_key_exists("ALL",$GroupNames))
    $GroupNames['ADMIN']="All Areas";
  if (!empty($GroupNames))
  {
    while(list($name, $value) = each($GroupNames)) 
    {
      $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$TempGroupTableName." WHERE name='$name'");
      if ($mysql_result==false)
      {
        $err=mysqli_error($mysql_link);
        mysqli_close($mysql_link);
        $comments.= "Could not query the table '$TempGroupTableName'. The following error was reported by MySql<br><br>\n";
        $comments.="$err"."<br><br>\n";
        DisplayComments($comments);
        exit;
      }
      if (mysqli_num_rows($mysql_result)==0)
      {
        $query="INSERT INTO `".$TempGroupTableName."` (
      `name` ,
      `description` ,
      `loginaction` ,
      `loginvalue`
      )
      VALUES (
      '$name',  '$value', '' , ''
      )";
        $mysql_result=mysqli_query($mysql_link,$query);
        if ($mysql_result==false)
        {
          $err=mysqli_error($mysql_link);
          mysqli_close($mysql_link);
          $comments.= "Could not insert data in table '$TempGroupTableName'. The following error was reported by MySql<br><br>\n";
          $comments.="$err"."<br><br>\n";
          DisplayComments($comments);
          exit;
        }
      }
    }
  }
}

if (numberofentries($mysql_link,$TempConfigTableName)<1)
{
  // Use data from slconfig.php as current version is <V2.00
  // Add config settings to table
  // Determine settings for configuration table
  
  // Get full server path to slpw folder
  $slpwpath=str_replace(basename(__FILE__), '', realpath(__FILE__));
  $slpwpath=str_replace("\\","/",$slpwpath);
  
  // Get URL to slpw folder
  $slpwurl="http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
  $slpwurl=str_replace("upgrade.php","",$slpwurl);
  
  $install_version=$installversion;
  
  $install_sitename=$SiteName;
  
  $install_siteemail=$SiteEmail;
  
  $install_siteemail2="";
  
  $install_dateformat=$DateFormat;
  
  $install_logoutpage=$LogoutPage;
  
  $install_siteloklocation=$SitelokLocation;
  
  if (isset($SitelokLocationURL))
    $install_siteloklocationurl=$SitelokLocationURL;
  else
    $install_siteloklocationurl=$slpwurl;
  
  if (isset($EmailLocation))  
    $install_emaillocation=$EmailLocation;
  else
    $install_emaillocation=$slpwpath."email/";
  
  if (isset($EmailURL))  
    $install_emailurl=$EmailURL;
  else
    $install_emailurl=$slpwurl."email/";
  
  $install_filelocation="default=".$FileLocation;
  
  if (!empty($FileLocations))
  {
    $install_filelocation.="|";  
    while(list($name, $value) = each($FileLocations)) 
    {
      $install_filelocation.="|".$name."=".$value;      
    }
  }
  
  $install_siteloklog=$SitelokLog;
  
  $install_logdetails=$LogDetails;
  
  $install_sitekey=$SiteKey;
  
  $install_logintype=$LoginType;
  
  if (isset($TuringLogin))
    $install_turinglogin=$TuringLogin;
  else 
    $install_turinglogin=0;
    
  if (isset($TuringRegister))
    $install_turingregister=$TuringRegister;
  else 
    $install_turingregister=0;
  
  $install_maxsessiontime=$MaxSessionTime;
  
  $install_maxinactivitytime=$MaxInactivityTime;
  
  $install_cookielogin=$CookieLogin;
  
  $install_logintemplate=$LoginPage;
  
  $install_expiredpage=$ExpiredPage;
  
  $install_wronggrouppage=$WrongGroupPage;
  
  $install_messagetemplate=$MessagePage;
  
  $install_forgottenemail=$ForgottenEmail;
  
  $install_newuseremail=$NewUserEmail;
  
  if (isset($ShowRows))
    $install_showrows=$ShowRows;
  else
    $install_showrows=10;
  
  if ($CustomInTable==true)  
    $install_customintable=1;
  else
    $install_customintable=0;
  
  if (isset($RandomPasswordMask))
    $install_randompasswordmask=$RandomPasswordMask;
  else
    $install_randompasswordmask="cccc##";
  
  if ((!isset($MD5passwords)) || ($MD5passwords==false))
    $install_md5passwords=0;
  else
    $install_md5passwords=1;
  
  $install_concurrentlogin=1;
  
  $install_logviewoffset=0;
  
  $install_logvieworder="ASC";
  
  $install_logviewdetails="YYYYYYYYYYYYYYYY";
  
  $install_sortfield="";
  
  $install_sortdirection="";
  
  
  $install_customtitle1=$CustomTitle1;
  $install_customtitle2=$CustomTitle2;
  $install_customtitle3=$CustomTitle3;
  $install_customtitle4=$CustomTitle4;
  $install_customtitle5=$CustomTitle5;
  $install_customtitle6=$CustomTitle6;
  $install_customtitle7=$CustomTitle7;
  $install_customtitle8=$CustomTitle8;
  $install_customtitle9=$CustomTitle9;
  $install_customtitle10=$CustomTitle10;
  $install_customtitle11=$CustomTitle11;
  $install_customtitle12=$CustomTitle12;
  $install_customtitle13=$CustomTitle13;
  $install_customtitle14=$CustomTitle14;
  $install_customtitle15=$CustomTitle15;
  $install_customtitle16=$CustomTitle16;
  $install_customtitle17=$CustomTitle17;
  $install_customtitle18=$CustomTitle18;
  $install_customtitle19=$CustomTitle19;
  $install_customtitle20=$CustomTitle20;
  $install_customtitle21=$CustomTitle21;
  $install_customtitle22=$CustomTitle22;
  $install_customtitle23=$CustomTitle23;
  $install_customtitle24=$CustomTitle24;
  $install_customtitle25=$CustomTitle25;
  $install_customtitle26=$CustomTitle26;
  $install_customtitle27=$CustomTitle27;
  $install_customtitle28=$CustomTitle28;
  $install_customtitle29=$CustomTitle29;
  $install_customtitle30=$CustomTitle30;
  $install_customtitle31=$CustomTitle31;
  $install_customtitle32=$CustomTitle32;
  $install_customtitle33=$CustomTitle33;
  $install_customtitle34=$CustomTitle34;
  $install_customtitle35=$CustomTitle35;
  $install_customtitle36=$CustomTitle36;
  $install_customtitle37=$CustomTitle37;
  $install_customtitle38=$CustomTitle38;
  $install_customtitle39=$CustomTitle39;
  $install_customtitle40=$CustomTitle40;
  $install_customtitle41=$CustomTitle41;
  $install_customtitle42=$CustomTitle42;
  $install_customtitle43=$CustomTitle43;
  $install_customtitle44=$CustomTitle44;
  $install_customtitle45=$CustomTitle45;
  $install_customtitle46=$CustomTitle46;
  $install_customtitle47=$CustomTitle47;
  $install_customtitle48=$CustomTitle48;
  $install_customtitle49=$CustomTitle49;
  $install_customtitle50=$CustomTitle50;
  
  $install_custom1validate=0;
  $install_custom2validate=0;
  $install_custom3validate=0;
  $install_custom4validate=0;
  $install_custom5validate=0;
  $install_custom6validate=0;
  $install_custom7validate=0;
  $install_custom8validate=0;
  $install_custom9validate=0;
  $install_custom10validate=0;
  $install_custom11validate=0;
  $install_custom12validate=0;
  $install_custom13validate=0;
  $install_custom14validate=0;
  $install_custom15validate=0;
  $install_custom16validate=0;
  $install_custom17validate=0;
  $install_custom18validate=0;
  $install_custom19validate=0;
  $install_custom20validate=0;
  $install_custom21validate=0;
  $install_custom22validate=0;
  $install_custom23validate=0;
  $install_custom24validate=0;
  $install_custom25validate=0;
  $install_custom26validate=0;
  $install_custom27validate=0;
  $install_custom28validate=0;
  $install_custom29validate=0;
  $install_custom30validate=0;
  $install_custom31validate=0;
  $install_custom32validate=0;
  $install_custom33validate=0;
  $install_custom34validate=0;
  $install_custom35validate=0;
  $install_custom36validate=0;
  $install_custom37validate=0;
  $install_custom38validate=0;
  $install_custom39validate=0;
  $install_custom40validate=0;
  $install_custom41validate=0;
  $install_custom42validate=0;
  $install_custom43validate=0;
  $install_custom44validate=0;
  $install_custom45validate=0;
  $install_custom46validate=0;
  $install_custom47validate=0;
  $install_custom48validate=0;
  $install_custom49validate=0;
  $install_custom50validate=0;
  
  if ($UsePHPmailer==1)
    $install_emailtype=1;
  else
    $install_emailtype=0;
  
  if (isset($EmailUsername))  
    $install_emailusername=$EmailUsername;
  else
    $install_emailusername="";    
    
  if (isset($EmailPassword))  
    $install_emailpassword=$EmailPassword;
  else
    $install_emailpassword="";    
  
  if (isset($EmailServer))  
    $install_emailserver=$EmailServer;
  else
    $install_emailserver="";    
  
  $install_emaildelay=0;
  
  if (isset($ModifyUserEmail))  
    $install_modifyuseremail=$ModifyUserEmail;
  else
    $install_modifyuseremail="updateuser.htm";
  
  if (isset($NoAccessPage))  
    $install_noaccesspage=$NoAccessPage;
  else
    $install_noaccesspage="";
    
  if ((!isset($DBupdate)) || ($DBupdate==false)) 
    $install_dbupdate=0;
  else
    $install_dbupdate=1;
    
  // Create settings in table
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$TempConfigTableName." WHERE confignum='1'");
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.= "Could not query the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.="$err"."<br><br>\n";
    DisplayComments($comments);
    exit;
  }
  if (mysqli_num_rows($mysql_result)==0)
  {
    $query="INSERT INTO `slconfig` (
  `confignum` ,
  `version` ,
  `sitename` ,
  `siteemail` ,
  `dateformat` ,
  `logoutpage` ,
  `siteloklocation` ,
  `siteloklocationurl` ,
  `emaillocation` ,
  `emailurl` ,
  `filelocation` ,
  `siteloklog` ,
  `logdetails` ,
  `sitekey` ,
  `logintype` ,
  `turinglogin` ,
  `turingregister` ,
  `maxsessiontime` ,
  `maxinactivitytime` ,
  `cookielogin` ,
  `logintemplate` ,
  `expiredpage` ,
  `wronggrouppage` ,
  `messagetemplate` ,
  `forgottenemail` ,
  `newuseremail` ,
  `showrows` ,
  `customintable` ,
  `randompasswordmask` ,
  `md5passwords` ,
  `concurrentlogin` ,
  `logviewoffset` ,
  `logvieworder` ,
  `logviewdetails` ,
  `sortfield` ,
  `sortdirection` ,
  `customtitle1` ,
  `customtitle2` ,
  `customtitle3` ,
  `customtitle4` ,
  `customtitle5` ,
  `customtitle6` ,
  `customtitle7` ,
  `customtitle8` ,
  `customtitle9` ,
  `customtitle10` ,
  `customtitle11` ,
  `customtitle12` ,
  `customtitle13` ,
  `customtitle14` ,
  `customtitle15` ,
  `customtitle16` ,
  `customtitle17` ,
  `customtitle18` ,
  `customtitle19` ,
  `customtitle20` ,
  `customtitle21` ,
  `customtitle22` ,
  `customtitle23` ,
  `customtitle24` ,
  `customtitle25` ,
  `customtitle26` ,
  `customtitle27` ,
  `customtitle28` ,
  `customtitle29` ,
  `customtitle30` ,
  `customtitle31` ,
  `customtitle32` ,
  `customtitle33` ,
  `customtitle34` ,
  `customtitle35` ,
  `customtitle36` ,
  `customtitle37` ,
  `customtitle38` ,
  `customtitle39` ,
  `customtitle40` ,
  `customtitle41` ,
  `customtitle42` ,
  `customtitle43` ,
  `customtitle44` ,
  `customtitle45` ,
  `customtitle46` ,
  `customtitle47` ,
  `customtitle48` ,
  `customtitle49` ,
  `customtitle50` ,
  `custom1validate` ,
  `custom2validate` ,
  `custom3validate` ,
  `custom4validate` ,
  `custom5validate` ,
  `custom6validate` ,
  `custom7validate` ,
  `custom8validate` ,
  `custom9validate` ,
  `custom10validate` ,
  `custom11validate` ,
  `custom12validate` ,
  `custom13validate` ,
  `custom14validate` ,
  `custom15validate` ,
  `custom16validate` ,
  `custom17validate` ,
  `custom18validate` ,
  `custom19validate` ,
  `custom20validate` ,
  `custom21validate` ,
  `custom22validate` ,
  `custom23validate` ,
  `custom24validate` ,
  `custom25validate` ,
  `custom26validate` ,
  `custom27validate` ,
  `custom28validate` ,
  `custom29validate` ,
  `custom30validate` ,
  `custom31validate` ,
  `custom32validate` ,
  `custom33validate` ,
  `custom34validate` ,
  `custom35validate` ,
  `custom36validate` ,
  `custom37validate` ,
  `custom38validate` ,
  `custom39validate` ,
  `custom40validate` ,
  `custom41validate` ,
  `custom42validate` ,
  `custom43validate` ,
  `custom44validate` ,
  `custom45validate` ,
  `custom46validate` ,
  `custom47validate` ,
  `custom48validate` ,
  `custom49validate` ,
  `custom50validate` ,
  `emailtype` ,
  `emailusername` ,
  `emailpassword` ,
  `emailserver` ,
  `emaildelay` ,
  `modifyuseremail` ,
  `noaccesspage` ,
  `siteemail2`, 
  `dbupdate` 
  )
  VALUES (
  '1',
  '$install_version',
  '$install_sitename',
  '$install_siteemail',
  '$install_dateformat',
  '$install_logoutpage',
  '$install_siteloklocation',
  '$install_siteloklocationurl',
  '$install_emaillocation',
  '$install_emailurl',
  '$install_filelocation',
  '$install_siteloklog',
  '$install_logdetails',
  '$install_sitekey',
  '$install_logintype',
  '$install_turinglogin',
  '$install_turingregister',
  '$install_maxsessiontime',
  '$install_maxinactivitytime',
  '$install_cookielogin',
  '$install_logintemplate',
  '$install_expiredpage',
  '$install_wronggrouppage',
  '$install_messagetemplate',
  '$install_forgottenemail',
  '$install_newuseremail',
  '$install_showrows',
  '$install_customintable',
  '$install_randompasswordmask',
  '$install_md5passwords',
  '$install_concurrentlogin',
  '$install_logviewoffset',
  '$install_logvieworder',
  '$install_logviewdetails',
  '$install_sortfield',
  '$install_sortdirection',
  '$install_customtitle1',
  '$install_customtitle2',
  '$install_customtitle3',
  '$install_customtitle4',
  '$install_customtitle5',
  '$install_customtitle6',
  '$install_customtitle7',
  '$install_customtitle8',
  '$install_customtitle9',
  '$install_customtitle10',
  '$install_customtitle11',
  '$install_customtitle12',
  '$install_customtitle13',
  '$install_customtitle14',
  '$install_customtitle15',
  '$install_customtitle16',
  '$install_customtitle17',
  '$install_customtitle18',
  '$install_customtitle19',
  '$install_customtitle20',
  '$install_customtitle21',
  '$install_customtitle22',
  '$install_customtitle23',
  '$install_customtitle24',
  '$install_customtitle25',
  '$install_customtitle26',
  '$install_customtitle27',
  '$install_customtitle28',
  '$install_customtitle29',
  '$install_customtitle30',
  '$install_customtitle31',
  '$install_customtitle32',
  '$install_customtitle33',
  '$install_customtitle34',
  '$install_customtitle35',
  '$install_customtitle36',
  '$install_customtitle37',
  '$install_customtitle38',
  '$install_customtitle39',
  '$install_customtitle40',
  '$install_customtitle41',
  '$install_customtitle42',
  '$install_customtitle43',
  '$install_customtitle44',
  '$install_customtitle45',
  '$install_customtitle46',
  '$install_customtitle47',
  '$install_customtitle48',
  '$install_customtitle49',
  '$install_customtitle50',
  '$install_custom1validate',
  '$install_custom2validate',
  '$install_custom3validate',
  '$install_custom4validate',
  '$install_custom5validate',
  '$install_custom6validate',
  '$install_custom7validate',
  '$install_custom8validate',
  '$install_custom9validate',
  '$install_custom10validate',
  '$install_custom11validate',
  '$install_custom12validate',
  '$install_custom13validate',
  '$install_custom14validate',
  '$install_custom15validate',
  '$install_custom16validate',
  '$install_custom17validate',
  '$install_custom18validate',
  '$install_custom19validate',
  '$install_custom20validate',
  '$install_custom21validate',
  '$install_custom22validate',
  '$install_custom23validate',
  '$install_custom24validate',
  '$install_custom25validate',
  '$install_custom26validate',
  '$install_custom27validate',
  '$install_custom28validate',
  '$install_custom29validate',
  '$install_custom30validate',
  '$install_custom31validate',
  '$install_custom32validate',
  '$install_custom33validate',
  '$install_custom34validate',
  '$install_custom35validate',
  '$install_custom36validate',
  '$install_custom37validate',
  '$install_custom38validate',
  '$install_custom39validate',
  '$install_custom40validate',
  '$install_custom41validate',
  '$install_custom42validate',
  '$install_custom43validate',
  '$install_custom44validate',
  '$install_custom45validate',
  '$install_custom46validate',
  '$install_custom47validate',
  '$install_custom48validate',
  '$install_custom49validate',
  '$install_custom50validate',
  '$install_emailtype',
  '$install_emailusername',
  '$install_emailpassword',
  '$install_emailserver',
  '$install_emaildelay',
  '$install_modifyuseremail',
  '$install_noaccesspage',
  '$install_siteemail2',
  '$install_dbupdate')";
  
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      $err=mysqli_error($mysql_link);
      mysqli_close($mysql_link);
      $comments.= "Could not insert data in table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
      $comments.="$err"."<br><br>\n";
      DisplayComments($comments);
      exit;
    }
  }
}

// Process changes to V2.10
// Add Search engine fields if they don't exist already
if (!fieldexists($mysql_link,$TempConfigTableName,"allowsearchengine"))
{
  $query ="ALTER TABLE ".$TempConfigTableName." ADD allowsearchengine INT(11) NOT NULL default '0'";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'allowsearchengine' to the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  $query ="ALTER TABLE ".$TempConfigTableName." ADD searchenginegroup VARCHAR(255) NOT NULL default ''";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'searchenginegroup' to the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Set default values
  $query="UPDATE ".$TempConfigTableName." SET allowsearchengine=0, searchenginegroup='ALL' WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set 'allowsearchengine' or 'searchenginegroup' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  } 
}

// Process changes to V2.20
// Create sl_ordercontrol table
if (!tableexists($mysql_link,"sl_ordercontrol","orderno"))
{
  $query="CREATE TABLE `sl_ordercontrol` (
  `orderno` varchar(255) NOT NULL,
  `timest` int(11) NOT NULL default '0',
  `status` varchar(255) NOT NULL default '',
  `username` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`orderno`)) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create the table 'sl_ordercontrol'. The following error was reported by MySql<br><br>\n";
    $comments.="$err"."<br><br>\n";
    DisplayComments($comments);
    exit;
  }
}

// Process changes to V2.30
// Add emailport field if it doesn't exist already
if (!fieldexists($mysql_link,$TempConfigTableName,"emailport"))
{
  $query ="ALTER TABLE ".$TempConfigTableName." ADD emailport VARCHAR(6) NOT NULL default '25' AFTER emailserver";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'emailport' to the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Set default value
  $query="UPDATE ".$TempConfigTableName." SET emailport='25' WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set 'emailport' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  } 
}

// Process changes to V2.40
// Add emailauth field if it doesn't exist already
if (!fieldexists($mysql_link,$TempConfigTableName,"emailauth"))
{
  $query ="ALTER TABLE ".$TempConfigTableName." ADD emailauth INT(11) NOT NULL default '1' AFTER emailport";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'emailauth' to the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Set default value
  $query="UPDATE ".$TempConfigTableName." SET emailauth='1' WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set 'emailauth' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  } 
}
// Add emailserversecurity field if it doesn't exist already
if (!fieldexists($mysql_link,$TempConfigTableName,"emailserversecurity"))
{
  $query ="ALTER TABLE ".$TempConfigTableName." ADD emailserversecurity VARCHAR(6) NOT NULL default '' AFTER emailauth";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'emailserversecurity' to the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Set default value
  $query="UPDATE ".$TempConfigTableName." SET emailserversecurity='' WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set 'emailserversecurity' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  } 
}

// Process changes to V2.50
// No DB changes required for V2.5

// Process changes to V3.00
if (!fieldexists($mysql_link,$DbTableName,"id"))
{
  // Remove primary key from username field
  $query="ALTER TABLE `".$DbTableName."` DROP PRIMARY KEY";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not drop PRIMARY KEY in the table '$DbTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }  
  // Make username field unique
  $query="ALTER TABLE `".$DbTableName."` ADD UNIQUE (`".$TempUsernameField."`)";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not make $TempUsernameField UNIQUE in the table '$DbTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }  
  // Add auto increment and primary field id to sitelok table
  $query="ALTER TABLE `".$DbTableName."` ADD `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add id field to the table '$DbTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  } 
}
if (!fieldexists($mysql_link,$TempGroupTableName,"id"))
{
  // Add auto increment and primary field id to usergroups table
  $query="ALTER TABLE `".$TempGroupTableName."` ADD `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add id field to the table '$TempGroupTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
}

// Process changes to V3.1
// Add profilepassrequired field if it doesn't already exist
if (!fieldexists($mysql_link,$TempConfigTableName,"profilepassrequired"))
{
  $query ="ALTER TABLE ".$TempConfigTableName." ADD profilepassrequired INT(11) NOT NULL default '0' AFTER searchenginegroup";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'profilepassrequired' to the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Set default value
  $query="UPDATE ".$TempConfigTableName." SET profilepassrequired='0' WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set 'profilepassrequired' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  } 
}
// Add emailconfirmrequired field if it doesn't already exist
if (!fieldexists($mysql_link,$TempConfigTableName,"emailconfirmrequired"))
{
  $query ="ALTER TABLE ".$TempConfigTableName." ADD emailconfirmrequired INT(11) NOT NULL default '0' AFTER profilepassrequired";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'emailconfirmrequired' to the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Set default value
  $query="UPDATE ".$TempConfigTableName." SET emailconfirmrequired='0' WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set 'emailconfirmrequired' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  } 
}

// If emailconfirmtemplate exists but is INT type then remove it (bug in earlier upgrade.php caused this)
if (fieldexists($mysql_link,$TempConfigTableName,"emailconfirmtemplate"))
{
  $query ="SELECT emailconfirmtemplate FROM ".$TempConfigTableName." WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  $finfo = mysqli_fetch_field($mysql_result);
  if ($finfo->type==3)
  {
    $query ="ALTER TABLE `".$TempConfigTableName."` DROP `emailconfirmtemplate` ";
    $mysql_result=mysqli_query($mysql_link,$query);
  }  
}

// Add emailconfirmtemplate field if it doesn't already exist
if (!fieldexists($mysql_link,$TempConfigTableName,"emailconfirmtemplate"))
{
  $query ="ALTER TABLE ".$TempConfigTableName." ADD emailconfirmtemplate VARCHAR(255) NOT NULL default '' AFTER emailconfirmrequired";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'emailconfirmtemplate' to the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Set default value
  $query="UPDATE ".$TempConfigTableName." SET emailconfirmtemplate='verifyemail.htm' WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set 'emailconfirmtemplate' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  } 
}
// Add emailunique field if it doesn't already exist
if (!fieldexists($mysql_link,$TempConfigTableName,"emailunique"))
{
  $query ="ALTER TABLE ".$TempConfigTableName." ADD emailunique INT(11) NOT NULL default '0' AFTER emailconfirmtemplate";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'emailunique' to the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Set default value
  $query="UPDATE ".$TempConfigTableName." SET emailunique=0 WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set 'emailunique' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  } 
}
// Add loginwithemail field if it doesn't already exist
if (!fieldexists($mysql_link,$TempConfigTableName,"loginwithemail"))
{
  $query ="ALTER TABLE ".$TempConfigTableName." ADD loginwithemail INT(11) NOT NULL default '0' AFTER emailunique";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'loginwithemail' to the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Set default value
  $query="UPDATE ".$TempConfigTableName." SET loginwithemail=0 WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set 'loginwithemail' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  } 
}

// Process changes to V3.2
// None required

// Process changes to V4.0
// Increase size of IP address field in log to be 46 characters for IPv6
$query ="ALTER TABLE  `".$TempLogTableName."` CHANGE  `ip`  `ip` VARCHAR(46) NOT NULL default ''";
$mysql_result=mysqli_query($mysql_link,$query);

// Add columnorder field if it doesn't exist already
if (!fieldexists($mysql_link,$TempConfigTableName,"columnorder"))
{
  $query ="ALTER TABLE ".$TempConfigTableName." ADD columnorder VARCHAR(120) NOT NULL default '' AFTER loginwithemail";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'columnorder' to the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Set value of columnorder
  $columnorder="ACSLCRUSPWENNMEMUG010203";
  // If current setting of customintable is 0 then set to ACSLCRUSPWENNMEMUG
  $query="SELECT * FROM ".$TempConfigTableName." WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result!=false)
  {
    $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
    if ($row['customintable']==0)
      $columnorder="ACSLCRUSPWENNMEMUG";
    else
    {
      // Otherwise show all columns plus any custom fields with a title
      $columnorder="ACSLCRUSPWENNMEMUG";
      for ($k=1;$k<=50;$k++)
      {
        if ($k<10) $val="0".$k; else $val=$k;
        if ($row['customtitle'.$k]!="")
          $columnorder.=$val;
      }
    }  
  }     
  $query="UPDATE ".$TempConfigTableName." SET columnorder='".$columnorder."' WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set 'columnorder' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  } 
}

// Rename customintable to be actionitems and set to 0
if (fieldexists($mysql_link,$TempConfigTableName,"customintable"))
{
  $query ="ALTER TABLE  `".$TempConfigTableName."` CHANGE  `customintable`  `actionitems` INT(11) NOT NULL default '0'";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'rename customintable to actionitems' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Set value
  $query="UPDATE ".$TempConfigTableName." SET actionitems='0' WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set 'actionitems' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }  
}

if (!fieldexists($mysql_link,$TempConfigTableName,"backuplocation"))
{
  $query ="ALTER TABLE ".$TempConfigTableName." ADD backuplocation VARCHAR(255) NOT NULL default '' AFTER columnorder";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'backuplocation' to the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }  
  // Set default value
  $validchars  = "abcdefghijklmnopqrstuvwxyz0123456789";
  $randomstr="slbackups_";
  for ($k=1;$k<mt_rand(10,16);$k++)
    $randomstr.=substr($validchars,mt_rand(0,35),1);
  $pos=strrpos(substr($slpwpath,0,strlen($slpwpath)-1),"/");
  if (is_integer($pos))
    $install_backuplocation=substr($slpwpath,0,$pos);
  $install_backuplocation=$install_backuplocation."/".$randomstr."/";
  $query="UPDATE ".$TempConfigTableName." SET backuplocation='".$install_backuplocation."' WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set 'backuplocation' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
}
// Read backuplocation from DB in case set previously
$query="SELECT * FROM ".$TempConfigTableName." WHERE confignum=1";
$mysql_result=mysqli_query($mysql_link,$query);
if ($mysql_result!=false)
{
  $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
  $install_backuplocation=$row['backuplocation'];
}
// Creat backup folder
if (!file_exists($install_backuplocation))
{
  if (!@mkdir($install_backuplocation, 0777))
  {
    $backupfolderproblem=true;
  }
  else
  {
    if (!file_exists($install_backuplocation.".htaccess"))
    {
      if (is_writeable($install_backuplocation))
      {
        $fh=@fopen($install_backuplocation.".htaccess","w");
        fwrite($fh,"order allow,deny\ndeny from *\n");
        fclose($fh);
      }
    }  
  }
}

// No changes needed for V4.1

// Process changes to V4.2

if (!tableexists($mysql_link,"sl_logintemplate","id"))
{
  $query="CREATE TABLE `sl_logintemplate` (
  `id` int(11) NOT NULL  default 0,
  `backcolor` varchar(6) NOT NULL default '',
  `backimage` varchar(100) NOT NULL default '',
  `backimagerp` varchar(9) NOT NULL default '',
  `mainfont` varchar(100) NOT NULL default '',
  `boxcolortype` varchar(8) NOT NULL default '',
  `boxcolorfrom` varchar(6) NOT NULL default '',
  `boxcolorto` varchar(6) NOT NULL default '',
  `boxradius` tinyint(4) NOT NULL default 0,
  `boxshadow` tinyint(4) NOT NULL default 0,
  `title` varchar(100) NOT NULL default '',
  `titlecolor` varchar(6) NOT NULL default '',
  `titlesize` tinyint(4) NOT NULL default 0,
  `titlealign` varchar(6) NOT NULL default '',
  `titlefont` varchar(100) NOT NULL default '',
  `msgcolor` varchar(6) NOT NULL default '',
  `msgsize` tinyint(4) NOT NULL default 0,
  `msgalign` varchar(6) NOT NULL default '',
  `username` varchar(50) NOT NULL default '',
  `password` varchar(50) NOT NULL default '',
  `captcha` varchar(50) NOT NULL default '',
  `remember` varchar(50) NOT NULL default '',
  `autologin` varchar(50) NOT NULL default '',
  `labelcolor` varchar(6) NOT NULL default '',
  `labelsize` tinyint(4) NOT NULL default 0,
  `inputcolor` varchar(6) NOT NULL default '',
  `inputsize` tinyint(4) NOT NULL default 0,
  `inputbackcolor` varchar(6) NOT NULL default '',
  `showicons` tinyint(4) NOT NULL default 0,
  `btnlbltext` varchar(50) NOT NULL default '',
  `btnlblcolor` varchar(6) NOT NULL default '',
  `btnlblsize` tinyint(4) NOT NULL default 0,
  `btncolortype` varchar(8) NOT NULL default '',
  `btncolorfrom` varchar(6) NOT NULL default '',
  `btncolorto` varchar(6) NOT NULL default '',
  `btnradius` tinyint(4) NOT NULL default 0,
  `forgottxt` varchar(50) NOT NULL default '',
  `forgotcolor` varchar(6) NOT NULL default '',
  `forgotsize` tinyint(4) NOT NULL default 0,
  `signuptext` varchar(100) NOT NULL default '',
  `signupurl` varchar(100) NOT NULL default '',
  `signupcolor` varchar(6) NOT NULL default '',
  `signupsize` tinyint(4) NOT NULL default 0,
  `signupalign` varchar(6) NOT NULL default '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create the table 'sl_logintemplate'. The following error was reported by MySql<br><br>\n";
    $comments.="$err"."<br><br>\n";
    DisplayComments($comments);
    exit;
  }
}

// Add default login template settings if not already created
if (numberofentries($mysql_link,"sl_logintemplate")<1)
{
  $query="INSERT INTO `sl_logintemplate` VALUES(1, 'FFFFFF', '', 'no-repeat', 'Arial, Helvetica, sans-serif', 'gradient', '1A305E', '2F5DAA', 10, 6, 'Login', 'FFFFFF', 44, 'center', 'Arial, Helvetica, sans-serif', 'FF0000', 16, 'left', 'Username', 'Password', 'Captcha', 'Remember Me', 'Auto Login', 'FFFFFF', 18, '1A305E', 16, 'FFFFFF', 1, 'Login', 'FFFFFF', 17, 'gradient', '79BCFF', '378EE5', 10, 'Forgot Password', 'FFFFFF', 14, '', '', 'FFA500', 16, 'center');";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not insert values in the table 'sl_logintemplate'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  } 
}

// Process changes to V4.3

// Create sl_forms table
if (!tableexists($mysql_link, "sl_forms", "id"))
{
  $query="CREATE TABLE `sl_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create the table 'sl_forms'. The following error was reported by MySql<br><br>\n";
    $comments.="$err"."<br><br>\n";
    DisplayComments($comments);
    exit;
  }
  // Add example tables (only do this if we create the table to not overwrite customies forms
  $query ="INSERT INTO `sl_forms` VALUES(1, 'Example for register.php', 'register'),";
  $query.="(2, 'Example for registerapprove.php', 'register'),";
  $query.="(3, 'Example for registerturing.php', 'register'),";
  $query.="(4, 'Example for update.php', 'update'),";
  $query.="(5, 'Example for pagewithlogin.php', 'login')";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create example forms in the table 'sl_forms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  $query="ALTER TABLE `sl_forms` AUTO_INCREMENT=10;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set auto increment in the table 'sl_forms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }   
}

// Create sl_registerforms table
if (!tableexists($mysql_link, "sl_registerforms", "id"))
{
  $query="CREATE TABLE `sl_registerforms` (
 `id` int(11) NOT NULL default 0,
  `position` tinyint(4) NOT NULL default 0,
  `fonttype` varchar(255) NOT NULL default '',
  `labelcolor` varchar(6) NOT NULL default '',
  `labelsize` tinyint(4) NOT NULL default 0,
  `labelstyle` varchar(25) NOT NULL default '',
  `inputtextcolor` varchar(6) NOT NULL default '',
  `inputtextsize` tinyint(4) NOT NULL  default 0,
  `inputtextstyle` varchar(25) NOT NULL default '',
  `inputbackcolor` varchar(6) NOT NULL default '',
  `bordersize` tinyint(4) NOT NULL default 0,
  `bordercolor` varchar(6) NOT NULL default '',
  `borderradius` tinyint(4) NOT NULL default 0,
  `rqdfieldlabel` varchar(255) NOT NULL default '',
  `rqdfieldcolor` varchar(6) NOT NULL default '',
  `rqdfieldsize` tinyint(4) NOT NULL default 0,
  `rqdfieldstyle` varchar(25) NOT NULL default '',
  `messagecolor` varchar(6) NOT NULL default '',
  `messagesize` tinyint(4) NOT NULL default 0,
  `messagestyle` varchar(25) NOT NULL default '',
  `usergroup` varchar(255) NOT NULL default '',
  `expiry` varchar(255) NOT NULL default '',
  `redirect` varchar(255) NOT NULL default '',
  `useremail` varchar(255) NOT NULL default '',
  `adminemail` varchar(255) NOT NULL default '',
  `enabled` varchar(3) NOT NULL default '',
  `sitelokfield` varchar(10) NOT NULL default '',
  `inputtype` varchar(10) NOT NULL default '',
  `labeltext` varchar(255) NOT NULL default '',
  `placetext` varchar(255) NOT NULL default '',
  `validation` varchar(20) NOT NULL default '',
  `errormsg` varchar(255) NOT NULL default '',
  `fieldwidth` tinyint(4) NOT NULL default 0,
  `value` text NOT NULL,
  `checked` tinyint(4) NOT NULL default 0,
  `bottommargin` tinyint(4) NOT NULL default 0,
  `showrequired` varchar(1) NOT NULL default '',
  `btncolortype` varchar(8) NOT NULL default '',
  `btncolorfrom` varchar(6) NOT NULL default '',
  `btncolorto` varchar(6) NOT NULL default '',
  `btnradius` tinyint(4) NOT NULL default 0,
  `btnlabel` varchar(100) NOT NULL default '',
  `btnlabelcolor` varchar(6) NOT NULL default '',
  `btnlabelsize` tinyint(4) NOT NULL default 0,
  `formerrormsg` varchar(255) NOT NULL default '',
  `formerrormsgcolor` varchar(6) NOT NULL default '',
  `formerrormsgsize` tinyint(4) NOT NULL default 0,
  `formerrormsgstyle` varchar(25) NOT NULL default '',
  `maxformwidth` int(11) NOT NULL default 0,
  `backcolor` varchar(6) NOT NULL default '',
  UNIQUE KEY `id` (`id`,`position`)  
) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create example forms in the table 'sl_registerforms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Add example tables (only do this if we create the table to not overwrite customies forms
  $query ="INSERT INTO `sl_registerforms` VALUES(1, 0, 'Arial, Helvetica, sans-serif', '1A305E', 16, 'normal normal bold', '1A305E', 14, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', 'CLIENT', '0', '/members/registerthanks.php', 'newuser.htm', 'newuseradmin.htm', 'Yes', '', '', '', '', '', '', 0, '', 0, 0, '', 'gradient', '1A305E', '378EE5', 10, 'Register', 'FFFFFF', 14, 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 300, 'FFFFFF'),";
  $query.="(1, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'name', 'text', 'Full name', '', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(1, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'email', 'text', 'Email', '', 'required', 'Please enter your email', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(1, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom1', 'text', 'What type of camera do you use', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(1, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom2', 'text', 'Where did you hear about us', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(2, 0, 'Arial, Helvetica, sans-serif', '1A305E', 16, 'normal normal bold', '1A305E', 14, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', 'CLIENT', '0', '/members/registerthanks.php', 'pending.htm', 'pendingadmin.htm', 'No', '', '', '', '', '', '', 0, '', 0, 0, '', 'gradient', '1A305E', '378EE5', 10, 'Register', 'FFFFFF', 14, 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 300, 'FFFFFF'),";
  $query.="(2, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'name', 'text', 'Full name', '', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(2, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'email', 'text', 'Email', '', 'required', 'Please enter your email', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(2, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom1', 'text', 'What type of camera do you use', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(2, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom2', 'text', 'Where did you hear about us', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(3, 0, 'Arial, Helvetica, sans-serif', '1A305E', 16, 'normal normal bold', '1A305E', 14, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', 'CLIENT', '0', '/members/registerthanks.php', 'newuser.htm', 'newuseradmin.htm', 'Yes', '', '', '', '', '', '', 0, '', 0, 0, '', 'gradient', '1A305E', '378EE5', 10, 'Register', 'FFFFFF', 14, 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 300, 'FFFFFF'),";
  $query.="(3, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'name', 'text', 'Full name', '', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(3, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'email', 'text', 'Email', '', 'required', 'Please enter your email', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(3, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom1', 'text', 'What type of camera do you use', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(3, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom2', 'text', 'Where did you hear about us', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(3, 5, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'captcha', 'captcha', 'Captcha', '', 'required', 'Please enter the letters shown', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', 0, '', 0, '');";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create example forms in the table 'sl_registerforms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }    
}

// Create sl_forms table
if (!tableexists($mysql_link, "sl_updateforms", "id"))
{
  $query="CREATE TABLE `sl_updateforms` (
  `id` int(11) NOT NULL,
  `position` tinyint(4) NOT NULL default 0,
  `fonttype` varchar(255) NOT NULL default '',
  `labelcolor` varchar(6) NOT NULL default '',
  `labelsize` tinyint(4) NOT NULL default 0,
  `labelstyle` varchar(25) NOT NULL default '',
  `inputtextcolor` varchar(6) NOT NULL default '',
  `inputtextsize` tinyint(4) NOT NULL default 0,
  `inputtextstyle` varchar(25) NOT NULL default '',
  `inputbackcolor` varchar(6) NOT NULL default '',
  `bordersize` tinyint(4) NOT NULL default 0,
  `bordercolor` varchar(6) NOT NULL default '',
  `borderradius` tinyint(4) NOT NULL default 0,
  `rqdfieldlabel` varchar(255) NOT NULL default '',
  `rqdfieldcolor` varchar(6) NOT NULL default '',
  `rqdfieldsize` tinyint(4) NOT NULL default 0,
  `rqdfieldstyle` varchar(25) NOT NULL default '',
  `messagecolor` varchar(6) NOT NULL default '',
  `messagesize` tinyint(4) NOT NULL default 0,
  `messagestyle` varchar(25) NOT NULL default '',
  `redirect` varchar(255) NOT NULL default '',
  `useremail` varchar(255) NOT NULL default '',
  `adminemail` varchar(255) NOT NULL default '',
  `sitelokfield` varchar(10) NOT NULL default '',
  `inputtype` varchar(10) NOT NULL default '',
  `labeltext` varchar(255) NOT NULL default '',
  `placetext` varchar(255) NOT NULL default '',
  `validation` varchar(20) NOT NULL default '',
  `errormsg` varchar(255) NOT NULL default '',
  `fieldwidth` tinyint(4) NOT NULL default 0,
  `value` text NOT NULL,
  `emailaction` tinyint(4) NOT NULL default 0,
  `bottommargin` tinyint(4) NOT NULL default 0,
  `showrequired` varchar(1) NOT NULL default '',
  `btncolortype` varchar(8) NOT NULL default '',
  `btncolorfrom` varchar(6) NOT NULL default '',
  `btncolorto` varchar(6) NOT NULL default '',
  `btnradius` tinyint(4) NOT NULL default 0,
  `btnlabel` varchar(100) NOT NULL default '',
  `btnlabelcolor` varchar(6) NOT NULL default '',
  `btnlabelsize` tinyint(4) NOT NULL default 0,
  `formerrormsg` varchar(255) NOT NULL default '',
  `formerrormsgcolor` varchar(6) NOT NULL default '',
  `formerrormsgsize` tinyint(4) NOT NULL default 0,
  `formerrormsgstyle` varchar(25) NOT NULL default '',
  `maxformwidth` int(11) NOT NULL default 0,
  `backcolor` varchar(6) NOT NULL default '',
  UNIQUE KEY `id` (`id`,`position`)  
) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create the table 'sl_updateforms'. The following error was reported by MySql<br><br>\n";
    $comments.="$err"."<br><br>\n";
    DisplayComments($comments);
    exit;
  }
  // Add example tables (only do this if we create the table to not overwrite customies forms
  $query ="INSERT INTO `sl_updateforms` VALUES(4, 0, 'Arial, Helvetica, sans-serif', '1A305E', 14, 'normal normal bold', '1A305E', 16, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', '', 'updateuser.htm', 'updateuseradmin.htm', '', '', '', '', '', '', 0, '', 0, 0, '', 'gradient', '1A305E', '378EE5', 10, 'Update', 'FFFFFF', 14, 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 400, 'FFFFFF'),";
  $query.="(4, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', 'password', 'password', 'New password (leave blank to keep the same)', '', 'notrequired', '', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(4, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', 'vpassword', 'password', 'Type password again', '', 'required', 'The passwords don''t match', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(4, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', 'name', 'text', 'Name', '', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(4, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', 'email', 'text', 'Email', '', 'required', 'Please enter your email', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(4, 5, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', 'custom1', 'text', 'What type of camera do you use', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', 0, '', 0, ''),";
  $query.="(4, 6, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', 'custom2', 'text', 'Where did you hear about us', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', 0, '', 0, '');";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create example forms in the table 'sl_updateforms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }    
}

// Create sl_loginforms table
if (!tableexists($mysql_link, "sl_loginforms", "id"))
{
  $query="CREATE TABLE `sl_loginforms` (
  `id` int(11) NOT NULL,
  `mainfont` varchar(255) NOT NULL default '',
  `messagecolor` varchar(6) NOT NULL default '',
  `messagesize` tinyint(4) NOT NULL default 0,
  `messagestyle` varchar(25) NOT NULL default '',
  `usernamelabel` varchar(255) NOT NULL default '',
  `usernameplaceholder` varchar(255) NOT NULL,
  `passwordlabel` varchar(255) NOT NULL default '',
  `passwordplaceholder` varchar(255) NOT NULL,
  `includecaptcha` varchar(1) NOT NULL default '',
  `captchalabel` varchar(255) NOT NULL default '',
  `captchaplaceholder` varchar(255) NOT NULL,
  `includeremember` varchar(1) NOT NULL default '',
  `rememberlabel` varchar(255) NOT NULL default '',
  `includeautologin` varchar(1) NOT NULL default '',
  `autologinlabel` varchar(255) NOT NULL default '',
  `labelcolor` varchar(6) NOT NULL default '',
  `labelsize` tinyint(4) NOT NULL default 0,
  `labelstyle` varchar(25) NOT NULL default '',
  `inputtextcolor` varchar(6) NOT NULL default '',
  `inputtextsize` tinyint(4) NOT NULL default 0,
  `inputtextstyle` varchar(25) NOT NULL default '',
  `inputbackcolor` varchar(6) NOT NULL default '',
  `bordersize` tinyint(4) NOT NULL default 0,
  `bordercolor` varchar(6) NOT NULL default '',
  `borderradius` tinyint(4) NOT NULL default 0,
  `logintext` varchar(100) NOT NULL default '',
  `logintextcolor` varchar(6) NOT NULL default '',
  `logintextsize` tinyint(4) NOT NULL default 0,
  `loginfilltype` varchar(8) NOT NULL default '',
  `logincolorfrom` varchar(6) NOT NULL default '',
  `logincolorto` varchar(6) NOT NULL default '',
  `loginshape` tinyint(4) NOT NULL default 0,
  `includeforgot` varchar(1) NOT NULL default '',
  `forgottext` varchar(255) NOT NULL default '',
  `forgotcolor` varchar(6) NOT NULL default '',
  `forgotsize` tinyint(4) NOT NULL default 0,
  `forgotstyle` varchar(25) NOT NULL default '',
  `includesignup` varchar(1) NOT NULL default '',
  `signuptext` varchar(255) NOT NULL default '',
  `signupurl` varchar(255) NOT NULL default '',
  `signupcolor` varchar(6) NOT NULL default '',
  `signupsize` tinyint(4) NOT NULL default 0,
  `signupstyle` varchar(25) NOT NULL default '',
  `bottommargin` tinyint(4) NOT NULL default 0,
  `maxformwidth` int(11) NOT NULL default 0,
  `backgroundcolor` varchar(6) NOT NULL default '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)  
) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create the table 'sl_loginforms'. The following error was reported by MySql<br><br>\n";
    $comments.="$err"."<br><br>\n";
    DisplayComments($comments);
    exit;
  }
  // Add example tables (only do this if we create the table to not overwrite customies forms
  $query ="INSERT INTO `sl_loginforms` VALUES(5, 'Arial, Helvetica, sans-serif', 'FF0000', 12, 'normal normal bold', 'Username', '', 'Password', '', '0', 'Captcha', 'captcha', '0', 'Remember me', '0', 'Auto Login', '1A305E', 16, 'normal normal normal', '1A305E', 14, 'normal normal normal', 'FFFFFF', 1, '378EE5', 5, 'Login', 'FFFFFF', 14, 'gradient', '1A305E', '378EE5', 10, '1', 'Forgotten?', '1A305E', 14, 'normal normal normal', '0', 'Not signed up yet?', '/register.php', '000000', 12, 'normal normal normal', 15, 200, 'FFFFFF');";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create example tables in sl_loginforms. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }      
}

// Process changes to V4.4
// No changes needed

// Process changes to V4.5

// Add new fields to sl_logintemplate
if (!fieldexists($mysql_link,"sl_logintemplate","btnlblfont"))
{
  $query ="ALTER TABLE `sl_logintemplate`
  ADD `btnlblfont` VARCHAR(255) NOT NULL DEFAULT 'Arial, Helvetica, sans-serif' AFTER `btnlbltext`,
  ADD `btnlblstyle` VARCHAR(25) NOT NULL DEFAULT 'normal normal bold' AFTER `btnlblfont`,
  ADD `btnbordercolor` VARCHAR(6) NOT NULL DEFAULT 'FFFFFF' AFTER `btnradius`,
  ADD `btnbordersize` TINYINT(4) NOT NULL DEFAULT '0' AFTER `btnbordercolor`,
  ADD `btnborderstyle` VARCHAR(8) NOT NULL DEFAULT 'solid' AFTER `btnbordersize`
  ";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add fields 'btnlblfont,btnlblstyle,btnbordercolor,btnbordersize,btnborderstyle' to the table 'sl_logintemplate'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // If we are adding the fields update existing entries with defaults
  $query ="UPDATE `sl_logintemplate` SET btnlblfont='Arial, Helvetica, sans-serif', btnlblstyle='normal normal bold', btnbordercolor='FFFFFF', btnbordersize=0, btnborderstyle='solid' WHERE id=1";
  $mysql_result=mysqli_query($mysql_link,$query);  
}

// Add new fields to sl_loginforms
if (!fieldexists($mysql_link,"sl_loginforms","logintextfont"))
{
  $query ="ALTER TABLE `sl_loginforms`
  ADD `logintextfont` VARCHAR(255) NOT NULL DEFAULT 'Arial, Helvetica, sans-serif' AFTER `logintext`,
  ADD `logintextstyle` VARCHAR(25) NOT NULL DEFAULT 'normal normal bold' AFTER `logintextfont`,
  ADD `btnbordercolor` VARCHAR(6) NOT NULL DEFAULT 'FFFFFF' AFTER `loginshape`,
  ADD `btnbordersize` TINYINT(4) NOT NULL DEFAULT '0' AFTER `btnbordercolor`,
  ADD `btnborderstyle` VARCHAR(8) NOT NULL DEFAULT 'solid' AFTER `btnbordersize`
  ";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add fields 'logintextfont,logintextstyle,btnbordercolor,btnbordersize,btnborderstyle' to the table 'sl_loginforms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // If we are adding the fields update existing entries with defaults
  $query ="UPDATE `sl_loginforms` SET logintextfont='Arial, Helvetica, sans-serif', logintextstyle='normal normal bold', btnbordercolor='FFFFFF', btnbordersize=0, btnborderstyle='solid'";
  $mysql_result=mysqli_query($mysql_link,$query);  
}

// Add new fields to sl_registerforms
if (!fieldexists($mysql_link,"sl_registerforms","btnlabelfont"))
{
  $query ="ALTER TABLE `sl_registerforms`
  ADD `btnlabelfont` VARCHAR(255) NOT NULL DEFAULT '' AFTER `btnlabelsize`,
  ADD `btnlabelstyle` VARCHAR(25) NOT NULL DEFAULT '' AFTER `btnlabelfont`,
  ADD `btnbordercolor` VARCHAR(6) NOT NULL DEFAULT '' AFTER `btnlabelstyle`,
  ADD `btnbordersize` TINYINT(4) NOT NULL DEFAULT '0' AFTER `btnbordercolor`,
  ADD `btnborderstyle` VARCHAR(8) NOT NULL DEFAULT '' AFTER `btnbordersize`
  ";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add fields 'btnlabelfont,btnlabelstyle,btnbordercolor,btnbordersize,btnborderstyle' to the table 'sl_registerforms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // If we are adding the fields update existing entries with defaults (only for position 0)
  $query ="UPDATE `sl_registerforms` SET btnlabelfont='Arial, Helvetica, sans-serif', btnlabelstyle='normal normal bold', btnbordercolor='FFFFFF', btnbordersize=0, btnborderstyle='solid' WHERE position=0";
  $mysql_result=mysqli_query($mysql_link,$query);  
  $query ="UPDATE `sl_registerforms` SET btnlabelfont='', btnlabelstyle='', btnbordercolor='', btnbordersize=0, btnborderstyle='' WHERE position>0";
  $mysql_result=mysqli_query($mysql_link,$query);  
}

// Add new fields to sl_updateforms
if (!fieldexists($mysql_link,"sl_updateforms","btnlabelfont"))
{
  $query ="ALTER TABLE `sl_updateforms`
  ADD `btnlabelfont` VARCHAR(255) NOT NULL DEFAULT '' AFTER `btnlabelsize`,
  ADD `btnlabelstyle` VARCHAR(25) NOT NULL DEFAULT '' AFTER `btnlabelfont`,
  ADD `btnbordercolor` VARCHAR(6) NOT NULL DEFAULT '' AFTER `btnlabelstyle`,
  ADD `btnbordersize` TINYINT(4) NOT NULL DEFAULT '0' AFTER `btnbordercolor`,
  ADD `btnborderstyle` VARCHAR(8) NOT NULL DEFAULT '' AFTER `btnbordersize`
  ";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add fields 'btnlabelfont,btnlabelstyle,btnbordercolor,btnbordersize,btnborderstyle' to the table 'sl_updateforms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // If we are adding the fields update existing entries with defaults (only for position 0)
  $query ="UPDATE `sl_updateforms` SET btnlabelfont='Arial, Helvetica, sans-serif', btnlabelstyle='normal normal bold', btnbordercolor='FFFFFF', btnbordersize=0, btnborderstyle='solid' WHERE position=0";
  $mysql_result=mysqli_query($mysql_link,$query);  
  $query ="UPDATE `sl_updateforms` SET btnlabelfont='', btnlabelstyle='', btnbordercolor='', btnbordersize=0, btnborderstyle='' WHERE position>0";
  $mysql_result=mysqli_query($mysql_link,$query);  
}


// Create sl_contactforms table
if (!tableexists($mysql_link, "sl_contactforms", "id"))
{
  $query="CREATE TABLE `sl_contactforms` (
  `id` int(11) NOT NULL DEFAULT '0',
  `position` tinyint(4) NOT NULL DEFAULT '0',
  `fonttype` varchar(255) NOT NULL DEFAULT '',
  `labelcolor` varchar(6) NOT NULL DEFAULT '',
  `labelsize` tinyint(4) NOT NULL DEFAULT '0',
  `labelstyle` varchar(25) NOT NULL DEFAULT '',
  `inputtextcolor` varchar(6) NOT NULL DEFAULT '',
  `inputtextsize` tinyint(4) NOT NULL DEFAULT '0',
  `inputtextstyle` varchar(25) NOT NULL DEFAULT '',
  `inputbackcolor` varchar(6) NOT NULL DEFAULT '',
  `bordersize` tinyint(4) NOT NULL DEFAULT '0',
  `bordercolor` varchar(6) NOT NULL DEFAULT '',
  `borderradius` tinyint(4) NOT NULL DEFAULT '0',
  `rqdfieldlabel` varchar(255) NOT NULL DEFAULT '',
  `rqdfieldcolor` varchar(6) NOT NULL DEFAULT '',
  `rqdfieldsize` tinyint(4) NOT NULL DEFAULT '0',
  `rqdfieldstyle` varchar(25) NOT NULL DEFAULT '',
  `messagecolor` varchar(6) NOT NULL DEFAULT '',
  `messagesize` tinyint(4) NOT NULL DEFAULT '0',
  `messagestyle` varchar(25) NOT NULL DEFAULT '',
  `sendemail` varchar(255) NOT NULL DEFAULT '',
  `redirect` varchar(255) NOT NULL DEFAULT '',
  `useremailvisitor` varchar(255) NOT NULL DEFAULT '',
  `adminemailvisitor` varchar(255) NOT NULL DEFAULT '',
  `useremailmember` varchar(255) NOT NULL DEFAULT '',
  `adminemailmember` varchar(255) NOT NULL DEFAULT '',
  `fromnameuser` varchar(255) NOT NULL DEFAULT '',
  `replytouser` varchar(255) NOT NULL DEFAULT '',
  `attachmenttypes` varchar(255) NOT NULL DEFAULT '',
  `sendasuser` varchar(1) NOT NULL DEFAULT '',
  `attachmentsize` int(11) NOT NULL,
  `sitelokfield` varchar(10) NOT NULL DEFAULT '',
  `inputtype` varchar(10) NOT NULL DEFAULT '',
  `labeltext` varchar(255) NOT NULL DEFAULT '',
  `placetext` varchar(255) NOT NULL DEFAULT '',
  `validation` varchar(20) NOT NULL DEFAULT '',
  `errormsg` varchar(255) NOT NULL DEFAULT '',
  `fieldwidth` tinyint(4) NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  `checked` tinyint(4) NOT NULL DEFAULT '0',
  `bottommargin` tinyint(4) NOT NULL DEFAULT '0',
  `showrequired` varchar(1) NOT NULL DEFAULT '',
  `useas` varchar(1) NOT NULL DEFAULT '',
  `showfieldfor` varchar(1) NOT NULL DEFAULT '',
  `btncolortype` varchar(8) NOT NULL DEFAULT '',
  `btncolorfrom` varchar(6) NOT NULL DEFAULT '',
  `btncolorto` varchar(6) NOT NULL DEFAULT '',
  `btnradius` tinyint(4) NOT NULL DEFAULT '0',
  `btnlabel` varchar(100) NOT NULL DEFAULT '',
  `btnlabelcolor` varchar(6) NOT NULL DEFAULT '',
  `btnlabelsize` tinyint(4) NOT NULL DEFAULT '0',
  `btnlabelfont` varchar(255) NOT NULL DEFAULT '',
  `btnlabelstyle` varchar(25) NOT NULL DEFAULT '',
  `btnbordercolor` varchar(6) NOT NULL DEFAULT '',
  `btnbordersize` tinyint(4) NOT NULL DEFAULT '0',
  `btnborderstyle` varchar(8) NOT NULL DEFAULT '',
  `formerrormsg` varchar(255) NOT NULL DEFAULT '',
  `formerrormsgcolor` varchar(6) NOT NULL DEFAULT '',
  `formerrormsgsize` tinyint(4) NOT NULL DEFAULT '0',
  `formerrormsgstyle` varchar(25) NOT NULL DEFAULT '',
  `maxformwidth` int(11) NOT NULL DEFAULT '0',
  `backcolor` varchar(6) NOT NULL DEFAULT '',
  UNIQUE KEY `id` (`id`,`position`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create the table 'sl_contactforms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Add example contact table to sl_forms
  $query ="INSERT INTO `sl_forms` VALUES(6, 'Example for contactpage.php', 'contact');";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create exampl contact form in the table 'sl_forms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }

  // Add example tables (only do this if we create the table to not overwrite custom forms
  $query ="INSERT INTO `sl_contactforms` (`id`, `position`, `fonttype`, `labelcolor`, `labelsize`, `labelstyle`, `inputtextcolor`, `inputtextsize`, `inputtextstyle`, `inputbackcolor`, `bordersize`, `bordercolor`, `borderradius`, `rqdfieldlabel`, `rqdfieldcolor`, `rqdfieldsize`, `rqdfieldstyle`, `messagecolor`, `messagesize`, `messagestyle`, `sendemail`, `redirect`, `useremailvisitor`, `adminemailvisitor`, `useremailmember`, `adminemailmember`, `fromnameuser`, `replytouser`, `attachmenttypes`, `sendasuser`, `attachmentsize`, `sitelokfield`, `inputtype`, `labeltext`, `placetext`, `validation`, `errormsg`, `fieldwidth`, `value`, `checked`, `bottommargin`, `showrequired`, `useas`, `showfieldfor`, `btncolortype`, `btncolorfrom`, `btncolorto`, `btnradius`, `btnlabel`, `btnlabelcolor`, `btnlabelsize`, `btnlabelfont`, `btnlabelstyle`, `btnbordercolor`, `btnbordersize`, `btnborderstyle`, `formerrormsg`, `formerrormsgcolor`, `formerrormsgsize`, `formerrormsgstyle`, `maxformwidth`, `backcolor`) VALUES";
  $query.="(6, 0, 'Arial, Helvetica, sans-serif', '1A305E', 18, 'normal normal bold', '1A305E', 16, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', '', 'contactthanks.php', 'contactemailuser.htm', 'contactemailadmin.htm', 'contactemailuser.htm', 'contactemailadmin.htm', '', '', '.jpg .gif .png .zip .txt .pdf', '0', 2000000, '', '', '', '', '', '', 0, '', 0, 0, '', '', '', 'gradient', '1A305E', '378EE5', 10, 'Send', 'FFFFFF', 14, 'Arial, Helvetica, sans-serif', 'normal normal bold', 'FFFFFF', 0, 'solid', 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 300, 'FFFFFF'),";
  $query.="(6, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, 'fullname', 'text', 'Full name', 'full name', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '2', '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
  $query.="(6, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, 'email', 'text', 'Email', 'email', 'requiredemail', 'Please enter your email', 100, '', 0, 20, '1', '1', '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
  $query.="(6, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, '', 'textarea', 'Message', 'message', 'required', 'Please enter your message', 100, '', 0, 20, '1', '', '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
  $query.="(6, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, '', 'captcha', 'Captcha', 'captcha', 'required', 'Please enter the captcha code', 100, '', 0, 20, '1', '', '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, '');";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create example forms in the table 'sl_contactforms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }    
}


// Process changes to V4.6
// Increase size of password field to be 255 characters (for password hashing)
$query ="ALTER TABLE  `".$DbTableName."` CHANGE  `".$TempPasswordField."`  `".$TempPasswordField."` VARCHAR(255) NULL default NULL";
$mysql_result=mysqli_query($mysql_link,$query);

// Increase size of username field to be 100 characters
$query ="ALTER TABLE  `".$DbTableName."` CHANGE  `".$TempUsernameField."`  `".$TempUsernameField."` VARCHAR(100) NULL default NULL";
$mysql_result=mysqli_query($mysql_link,$query);

// Increase size of email field to be 100 characters
$query ="ALTER TABLE  `".$DbTableName."` CHANGE  `".$TempEmailField."`  `".$TempEmailField."` VARCHAR(100) NULL default NULL";
$mysql_result=mysqli_query($mysql_link,$query);

// Increase size of name field to be 100 characters
$query ="ALTER TABLE  `".$DbTableName."` CHANGE  `".$TempNameField."`  `".$TempNameField."` VARCHAR(100) NULL default NULL";
$mysql_result=mysqli_query($mysql_link,$query);

// Increase size of username field in log table to be 100 characters
$query ="ALTER TABLE  `".$TempLogTableName."` CHANGE  `username`  `username` VARCHAR(100) NOT NULL default ''";
$mysql_result=mysqli_query($mysql_link,$query);

// Process changes to V5.0
// Create sl_adminconfig table
if (!tableexists($mysql_link, "sl_adminconfig", "confignum"))
{
  $query="CREATE TABLE `sl_adminconfig` (
  `confignum` int(11) NOT NULL DEFAULT '1',
  `sendnewuseremail` tinyint(4) NOT NULL DEFAULT '0',
  `sendedituseremail` tinyint(4) NOT NULL DEFAULT '0',
  `emaildedupe` tinyint(4) NOT NULL DEFAULT '1',
  `emaildeselect` tinyint(4) NOT NULL DEFAULT '1',
  `exportfilename` varchar(100) NOT NULL DEFAULT 'sitelokusers.csv',
  `exportuseheader` tinyint(4) NOT NULL DEFAULT '0',
  `exporttype` tinyint(4) NOT NULL DEFAULT '0',
  `exportfields` varchar(120) NOT NULL DEFAULT 'CRUSPWENNMEMUG0102030405060708091011121314151617181920212223242526272829303132333435363738394041424344454647484950',
  `importheader` tinyint(4) NOT NULL DEFAULT '0',
  `importuseemailas` varchar(15) NOT NULL DEFAULT 'emailnever',
  `importaddusers` tinyint(4) NOT NULL DEFAULT '1',
  `importrandpass` varchar(15) NOT NULL DEFAULT 'randnewonly',
  `importusergroups` text NOT NULL,
  `importslctadded` tinyint(4) NOT NULL DEFAULT '0',
  `importexistusers` tinyint(4) NOT NULL DEFAULT '1',
  `importblank` varchar(15) NOT NULL DEFAULT 'allowblank',
  `importslctexist` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY  (`confignum`)) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not create the table 'sl_admonconfig'. The following error was reported by MySql<br><br>\n";
    $comments.="$err"."<br><br>\n";
    DisplayComments($comments);
    exit;
  }
}
// Add setting to table if not already created
if (numberofentries($mysql_link,"sl_adminconfig")<1)
{
  // Create settings in table
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$TempConfigTableName." WHERE confignum='1'");
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.= "Could not query the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.="$err"."<br><br>\n";
    DisplayComments($comments);
    exit;
  }
  if (mysqli_num_rows($mysql_result)==0)
  {
    $query="INSERT INTO `sl_adminconfig` (
`confignum` ,
`sendnewuseremail` ,
`sendedituseremail` ,
`emaildedupe` ,
`emaildeselect` ,
`exportfilename` ,
`exportuseheader` ,
`exporttype` ,
`exportfields` ,
`importheader` ,
`importuseemailas` ,
`importaddusers` ,
`importrandpass` ,
`importusergroups` ,
`importslctadded` ,
`importexistusers` ,
`importblank` ,
`importslctexist`
)
VALUES (
'1',
'0',
'0',
'0',
'1',
'sitelokusers.csv',
'0',
'0',
'CRUSPWENNMEMUG0102030405060708091011121314151617181920212223242526272829303132333435363738394041424344454647484950',
'0',
'emailnever',
'1',
'randnewonly',
'',
'0',
'1',
'allowblank',
'0')";
  
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      $err=mysqli_error($mysql_link);
      mysqli_close($mysql_link);
      $comments.= "Could not insert data in table sl_adminconfig. The following error was reported by MySql<br><br>\n";
      $comments.="$err"."<br><br>\n";
      DisplayComments($comments);
      exit;
    }
  }  
}

// Remove actionitems field from slconfig table
  $query ="ALTER TABLE `slconfig` DROP `actionitems` ";
  $mysql_result=mysqli_query($mysql_link,$query);

// Add username and type indexes to log table if not already
// Check if indexes exists
$logusernameindex=false;
$logtypeindex=false;
$mysql_result=mysqli_query($mysql_link,"SHOW INDEX FROM ".$DbLogTableName);
if ($mysql_result!=false)
{
  while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
  {
    if ($row['Column_name']=="username")
      $logusernameindex=true;
    if ($row['Column_name']=="type")
      $logtypeindex=true;
  }
}
if (!$logusernameindex)
  $mysql_result=mysqli_query($mysql_link,"ALTER TABLE `log` ADD KEY `username` (`username`)");
if (!$logtypeindex)
  $mysql_result=mysqli_query($mysql_link,"ALTER TABLE `log` ADD KEY `type` (`type`)");

// Set permission on writefiles folder
$install_writefileslocation=$slpwpath."writefiles/";
if (!is_writeable($install_writefileslocation))
{
  @chmod($install_writefileslocation,0777);
  if (!is_writeable($install_writefileslocation))
  {
    $writefilesfolderproblem=true;
  }
}

// Set permission on emailthumbs folder
$install_emailthumbslocation=$slpwpath."emailthumbs/";
if (!is_writeable($install_emailthumbslocation))
{
  @chmod($install_emailthumbslocation,0777);
  if (!is_writeable($install_emailthumbslocation))
  {
    $emailthumbsfolderproblem=true;
  }
}

// Process changes to V5.1
// Add new fields to sl_registerforms
if (!fieldexists($mysql_link,"sl_registerforms","inputpaddingv"))
{
  $query ="ALTER TABLE `sl_registerforms`
  ADD `inputpaddingv` VARCHAR(5) NOT NULL DEFAULT '' AFTER `borderradius`,
  ADD `inputpaddingh` VARCHAR(5) NOT NULL DEFAULT '' AFTER `inputpaddingv`,
  ADD `btnpaddingv` TINYINT(4) NOT NULL DEFAULT 0 AFTER `btnborderstyle`,
  ADD `btnpaddingh` TINYINT(4) NOT NULL DEFAULT 0 AFTER `btnpaddingv`
  ";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add fields 'inputpaddingv,inputpaddingh,btnpaddingv,btnpaddingh' to the table 'sl_registerforms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // If we are adding the fields update existing entries with defaults (only for position 0)
  $query ="UPDATE `sl_registerforms` SET inputpaddingv='0.3em', inputpaddingh='0.3em', btnpaddingv=6, btnpaddingh=24 WHERE position=0";
  $mysql_result=mysqli_query($mysql_link,$query);  
  $query ="UPDATE `sl_registerforms` SET inputpaddingv='', inputpaddingh='', btnpaddingv=0, btnpaddingh=0 WHERE position>0";
  $mysql_result=mysqli_query($mysql_link,$query);  
}
// Add new fields to sl_loginforms
if (!fieldexists($mysql_link,"sl_loginforms","inputpaddingv"))
{
  $query ="ALTER TABLE `sl_loginforms`
  ADD `inputpaddingv` VARCHAR(5) NOT NULL DEFAULT '' AFTER `borderradius`,
  ADD `inputpaddingh` VARCHAR(5) NOT NULL DEFAULT '' AFTER `inputpaddingv`,
  ADD `btnpaddingv` TINYINT(4) NOT NULL DEFAULT 0 AFTER `btnborderstyle`,
  ADD `btnpaddingh` TINYINT(4) NOT NULL DEFAULT 0 AFTER `btnpaddingv`
  ";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add fields 'inputpaddingv,inputpaddingh,btnpaddingv,btnpaddingh' to the table 'sl_loginforms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // If we are adding the fields update existing entries with defaults
  $query ="UPDATE `sl_loginforms` SET inputpaddingv='0.3em', inputpaddingh='0.3em', btnpaddingv=6, btnpaddingh=24";
  $mysql_result=mysqli_query($mysql_link,$query);  
  $query ="UPDATE `sl_loginforms` SET inputpaddingv='', inputpaddingh='', btnpaddingv=0, btnpaddingh=0 WHERE";
  $mysql_result=mysqli_query($mysql_link,$query);  
}
// Add new fields to sl_updateforms
if (!fieldexists($mysql_link,"sl_updateforms","inputpaddingv"))
{
  $query ="ALTER TABLE `sl_updateforms`
  ADD `inputpaddingv` VARCHAR(5) NOT NULL DEFAULT '' AFTER `borderradius`,
  ADD `inputpaddingh` VARCHAR(5) NOT NULL DEFAULT '' AFTER `inputpaddingv`,
  ADD `btnpaddingv` TINYINT(4) NOT NULL DEFAULT 0 AFTER `btnborderstyle`,
  ADD `btnpaddingh` TINYINT(4) NOT NULL DEFAULT 0 AFTER `btnpaddingv`
  ";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add fields 'inputpaddingv,inputpaddingh,btnpaddingv,btnpaddingh' to the table 'sl_updateforms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // If we are adding the fields update existing entries with defaults (only for position 0)
  $query ="UPDATE `sl_updateforms` SET inputpaddingv='0.3em', inputpaddingh='0.3em', btnpaddingv=6, btnpaddingh=24 WHERE position=0";
  $mysql_result=mysqli_query($mysql_link,$query);  
  $query ="UPDATE `sl_updateforms` SET inputpaddingv='', inputpaddingh='', btnpaddingv=0, btnpaddingh=0 WHERE position>0";
  $mysql_result=mysqli_query($mysql_link,$query);  
}
// Add new fields to sl_contactforms
if (!fieldexists($mysql_link,"sl_contactforms","inputpaddingv"))
{
  $query ="ALTER TABLE `sl_contactforms`
  ADD `inputpaddingv` VARCHAR(5) NOT NULL DEFAULT '' AFTER `borderradius`,
  ADD `inputpaddingh` VARCHAR(5) NOT NULL DEFAULT '' AFTER `inputpaddingv`,
  ADD `btnpaddingv` TINYINT(4) NOT NULL DEFAULT 0 AFTER `btnborderstyle`,
  ADD `btnpaddingh` TINYINT(4) NOT NULL DEFAULT 0 AFTER `btnpaddingv`
  ";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add fields 'inputpaddingv,inputpaddingh,btnpaddingv,btnpaddingh' to the table 'sl_contactforms'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // If we are adding the fields update existing entries with defaults (only for position 0)
  $query ="UPDATE `sl_contactforms` SET inputpaddingv='0.3em', inputpaddingh='0.3em', btnpaddingv=6, btnpaddingh=24 WHERE position=0";
  $mysql_result=mysqli_query($mysql_link,$query);  
  $query ="UPDATE `sl_contactforms` SET inputpaddingv='', inputpaddingh='', btnpaddingv=0, btnpaddingh=0 WHERE position>0";
  $mysql_result=mysqli_query($mysql_link,$query);  
}
// Add emailreplyto field if it doesn't exist already
if (!fieldexists($mysql_link,$TempConfigTableName,"emailreplyto"))
{
  $query ="ALTER TABLE ".$TempConfigTableName." ADD emailreplyto VARCHAR(255) NOT NULL default '' AFTER emailtype";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not add field 'emailreplyto' to the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  }
  // Set default value
  $query="UPDATE ".$TempConfigTableName." SET emailreplyto='' WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    $comments.="Could not set 'emailreplyto' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
    $comments.=$err."<br><br>\n";
    DisplayComments($comments);
    exit;        
  } 
}

// Update version number in slconfig table.
$query="UPDATE ".$TempConfigTableName." SET version='".$installversion."' WHERE confignum=1";
$mysql_result=mysqli_query($mysql_link,$query);
if ($mysql_result==false)
{
  $err=mysqli_error($mysql_link);
  mysqli_close($mysql_link);
  $comments.="Could not set 'version' in the table '$TempConfigTableName'. The following error was reported by MySql<br><br>\n";
  $comments.=$err."<br><br>\n";
  DisplayComments($comments);
  exit;        
} 

$comments.= "The upgrade was successful.<br>\n";  
$settingstoadd="";
if ((!isset($DbLogTableName)) || ($DbLogTableName==""))
  $settingstoadd.="\$DbLogTableName=\"$TempLogTableName\";<br>\n";
if ((!isset($DbConfigTableName)) || ($DbConfigTableName==""))
  $settingstoadd.="\$DbConfigTableName=\"$TempConfigTableName\";<br>\n";
if ((!isset($DbGroupTableName)) || ($DbGroupTableName==""))
  $settingstoadd.="\$DbGroupTableName=\"$TempGroupTableName\";<br>\n";
if ($settingstoadd!="")
  $comments.= "<br>\nPlease add the following lines to slconfig.php (near the other mysql settings) before using Sitelok.<br><br>\n".$settingstoadd; 
 
if ($backupfolderproblem)
{
  $comments.="<br>Please create a folder called <b>".basename($install_backuplocation)."</b> in the root of your site using your filemanager or FTP client<br>\n";
  $comments.="and set the permissions to allow write (usually 666 or 777).<br>\n";
} 
  
if ($writefilesfolderproblem)
{
  $comments.="<br>Set write permission (usually 666 or 777) on the <b>".basename($install_writefileslocation)."</b> folder (in the slpw folder) using your filemanager or FTP client<br>\n";
}   

if ($emailthumbsfolderproblem)
{
  $comments.="<br>Set write permission (usually 666 or 777) on the <b>".basename($install_emailthumbslocation)."</b> folder (in the slpw folder) using your filemanager or FTP client<br>\n";
}   

// Check for incompatible plugins
if (tableexists($mysql_link,$TempPluginsTableName,"id"))
{
  $v=GetPluginVersion($mysql_link,"2CO");
  if ($v!==false)
  {
    if ((float)$v<1.5)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#2co' target='plugins'>update</a> the plugin '2CO'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Paypal");
  if ($v!==false)
  {
    if ((float)$v<2.0)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#paypal' target='plugins'>update</a> the plugin 'Paypal'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Cartloom");
  if ($v!==false)
  {
    if ((float)$v<1.1)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#cartloom' target='plugins'>update</a> the plugin 'AWeber'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Blab!");
  if ($v!==false)
  {
    if ((float)$v<1.7)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#blab' target='plugins'>update</a> the plugin 'Blab!'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Custom Field Login");
  if ($v!==false)
  {
    if ((float)$v<1.4)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#logincustom' target='plugins'>update</a> the plugin 'Custom Field Login'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Facebook Connect");
  if ($v!==false)
  {
    if ((float)$v<1.7)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#facebook' target='plugins'>update</a> the plugin 'Facebook Connect'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Linklok Orders");
  if ($v!==false)
  {
    if ((float)$v<1.4)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#linklokorders' target='plugins'>update</a> the plugin 'Linklok Orders'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"OTP Token");
  if ($v!==false)
  {
    if ((float)$v<1.4)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#otptoken' target='plugins'>update</a> the plugin 'OTP Token'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Password Sentry");
  if ($v!==false)
  {
    if ((float)$v<1.2)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#passwordsentry' target='plugins'>update</a> the plugin 'Password Sentry'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Password Security");
  if ($v!==false)
  {
    if ((float)$v<1.3)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#passwordsecurity' target='plugins'>update</a> the plugin 'Password Security'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Amazon SES");
  if ($v!==false)
  {
    if ((float)$v<1.5)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#amazonses' target='plugins'>update</a> the plugin 'Amazon SES'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Email Unsubscribe");
  if ($v!==false)
  {
    if ((float)$v<1.4)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#emailunsubscribe' target='plugins'>update</a> the plugin 'Email Unsubscribe'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"User files");
  if ($v!==false)
  {
    if ((float)$v<1.5)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#userfiles' target='plugins'>update</a> the plugin 'User files'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Vanilla forum");
  if ($v!==false)
  {
    if ((float)$v<1.2)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#vanillaforum' target='plugins'>update</a> the plugin 'Vanilla forum'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Vanilla forum (jsConnect)");
  if ($v!==false)
  {
    if ((float)$v<1.3)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#vanillaforum' target='plugins'>update</a> the plugin 'Vanilla forum (jsConnect)'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"WebYep");
  if ($v!==false)
  {
    if ((float)$v<1.2)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#webyep' target='plugins'>update</a> the plugin 'WebYep'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Who is online");
  if ($v!==false)
  {
    if ((float)$v<1.6)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#whoisonline' target='plugins'>update</a> the plugin 'Who is online'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Zendesk");
  if ($v!==false)
  {
    if ((float)$v<1.3)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#zendesk' target='plugins'>update</a> the plugin 'Zendesk'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Stripe");
  if ($v!==false)
  {
    if ((float)$v<1.2)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#stripe' target='plugins'>update</a> the plugin 'Stripe'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"AWeber");
  if ($v!==false)
  {
    if ((float)$v<1.3)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#aweber' target='plugins'>update</a> the plugin 'AWeber'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Credits");
  if ($v!==false)
  {
    if ((float)$v<1.3)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#credits' target='plugins'>update</a> the plugin 'Credits'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Disqus");
  if ($v!==false)
  {
    if ((float)$v<1.1)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#disqus' target='plugins'>update</a> the plugin 'Disqus'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Expiry Email");
  if ($v!==false)
  {
    if ((float)$v<1.1)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#expiryemail' target='plugins'>update</a> the plugin 'Expiry Email'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Follow Up Email");
  if ($v!==false)
  {
    if ((float)$v<1.1)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#followupemail' target='plugins'>update</a> the plugin 'Follow Up Email'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Mailchimp");
  if ($v!==false)
  {
    if ((float)$v<1.1)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#mailchimp' target='plugins'>update</a> the plugin 'Mailchimp'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"PDF watermark");
  if ($v!==false)
  {
    if ((float)$v<1.2)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#pdfwatermark' target='plugins'>update</a> the plugin 'PDF watermark'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Register Code");
  if ($v!==false)
  {
    if ((float)$v<1.3)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#registercode' target='plugins'>update</a> the plugin 'Register Code'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
  $v=GetPluginVersion($mysql_link,"Google ReCAPTCHA");
  if ($v!==false)
  {
    if ((float)$v<1.3)
      $comments.="<br>\nPlease <a href='http://www.vibralogix.com/sitelokpw/plugins.php#recaptcha2' target='plugins'>update</a> the plugin 'Recaptcha'. The version installed is not compatible with this version of Sitleok.<br>\n";
  }  
} 
DisplayComments($comments);
print "</body>\n";
print "</html>\n";

function tableexists($mysql_link, $tablename, $firstfield)
{
  // See if table exists
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$tablename." LIMIT 1");
  if ($mysql_result==false)
    return(false);
  // If it does check firstname field matches
  $finfo = mysqli_fetch_field($mysql_result);
  $fieldname=$finfo->name;
  if (strtolower($fieldname)==strtolower($firstfield))
    return(true);
  return(false);  
}

function fieldexists($mysql_link,$tablename,$fieldname)
{
  $fieldname=strtolower($fieldname);
  $mysql_result=mysqli_query($mysql_link,"SHOW COLUMNS FROM ".$tablename);
  while ($row = mysqli_fetch_array($mysql_result,MYSQLI_BOTH)) $col_names[]=strtolower($row[0]);
  if (!in_array($fieldname, $col_names))
    return(false);
  return (true);    
}

function numberofentries($mysql_link,$tablename)
{
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$tablename);
  $num=mysqli_num_rows($mysql_result); 
  return ($num); 
}

function GetPluginVersion($mysql_link,$name)
{
  global $TempPluginsTableName;
  $query="SELECT * FROM ".$TempPluginsTableName." WHERE name='".$name."'";
  $mysql_result=mysqli_query($mysql_link,$query);
 	$row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);  
  if ($row!=NULL)
    return($row['version']);
  return(false);
}

function DisplayComments($comments)
{
  print "<html>\n";
  print "<head><title>Sitelok Upgrade</title></head>\n";
  print "<body>\n";
  print $comments;
  print "</body>\n";
  print "</html>\n";

}
?>