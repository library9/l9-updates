<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  require("admincommonfunctions.php");  

  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  $errors = array();    // array to hold validation errors
  $data = array();      // array to pass back data 
  $emailsenderror=false; // email send error is handled separately
  // Check CSRF value
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_CSRFFAILED.".");
    exit;
  }   
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_MYSQLERROR.".");
    exit;
  }
  $act=$_POST['act'];
  $actid=$_POST['actid'];
  $formpropertiesfield=$_POST['formpropertiesfield'];
  $formstylesfield=$_POST['formstylesfield'];
  $numfields=$_POST['numfields'];
  $formid=$actid;
  if (!$DemoMode)
  {
    $dataarray=json_decode($formpropertiesfield);
    // If editform then get number of existing fields
    $numexistingfields=0;
    if ($act=="editform")
    {
      $mysql_result = mysqli_query($mysql_link,"SELECT id FROM sl_updateforms WHERE position>0 AND id=".sl_quote_smart($actid));
      if ($mysql_result==false)
      {
        returnError($data,$errors,ADMINMSG_MYSQLERROR);
        exit;
      }
      $numexistingfields=mysqli_num_rows($mysql_result);
    }      
    // Create or update form in sl_forms table
    $query="sl_forms SET ";         
    $query.="name=".sl_quote_smart($dataarray[0]).",";
    $query.="type=".sl_quote_smart("update");
    if (($act=="addform") || ($act=="duplform"))
      $query = "INSERT INTO ".$query;
    else
      $query = "UPDATE ".$query." WHERE id=".sl_quote_smart($actid); 
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_MYSQLERROR);
      exit;
    }
    if (($act=="addform") || ($act=="duplform"))
      $formid=mysqli_insert_id($mysql_link);
    else
      $formid=$actid;
    $query ="sl_updateforms SET ";
    $query.="id=".sl_quote_smart($formid).",";
    $query.="position=0".",";
    $query.="redirect=".sl_quote_smart($dataarray[1]).",";
    $query.="useremail=".sl_quote_smart($dataarray[2]).",";
    $query.="adminemail=".sl_quote_smart($dataarray[3]).",";
    $dataarray=json_decode($formstylesfield);
    $query.="fonttype=".sl_quote_smart($dataarray[0]).",";
    $query.="labelcolor=".sl_quote_smart(substr($dataarray[1],1)).",";
    $query.="labelsize=".sl_quote_smart($dataarray[2]).",";
    $query.="labelstyle=".sl_quote_smart($dataarray[3]).",";
    $query.="inputtextcolor=".sl_quote_smart(substr($dataarray[4],1)).",";
    $query.="inputtextsize=".sl_quote_smart($dataarray[5]).",";
    $query.="inputtextstyle=".sl_quote_smart($dataarray[6]).",";
    $query.="inputbackcolor=".sl_quote_smart(substr($dataarray[7],1)).",";
    $query.="bordersize=".sl_quote_smart($dataarray[8]).",";
    $query.="bordercolor=".sl_quote_smart(substr($dataarray[9],1)).",";
    $query.="borderradius=".sl_quote_smart($dataarray[10]).",";
    $query.="rqdfieldlabel=".sl_quote_smart($dataarray[11]).",";
    $query.="rqdfieldcolor=".sl_quote_smart(substr($dataarray[12],1)).",";
    $query.="rqdfieldsize=".sl_quote_smart($dataarray[13]).",";
    $query.="rqdfieldstyle=".sl_quote_smart($dataarray[14]).",";
    $query.="messagecolor=".sl_quote_smart(substr($dataarray[15],1)).",";
    $query.="messagesize=".sl_quote_smart($dataarray[16]).",";
    $query.="messagestyle=".sl_quote_smart($dataarray[17]).",";
    $query.="btnlabel=".sl_quote_smart($dataarray[18]).",";
    $query.="btnlabelcolor=".sl_quote_smart(substr($dataarray[19],1)).",";
    $query.="btnlabelsize=".sl_quote_smart($dataarray[20]).",";
    $query.="btncolortype=".sl_quote_smart($dataarray[21]).",";
    $query.="btncolorfrom=".sl_quote_smart(substr($dataarray[22],1)).",";
    $query.="btncolorto=".sl_quote_smart(substr($dataarray[23],1)).",";
    $query.="btnradius=".sl_quote_smart($dataarray[24]).",";
    $query.="formerrormsg=".sl_quote_smart($dataarray[25]).",";
    $query.="formerrormsgcolor=".sl_quote_smart(substr($dataarray[26],1)).",";
    $query.="formerrormsgsize=".sl_quote_smart($dataarray[27]).",";
    $query.="formerrormsgstyle=".sl_quote_smart($dataarray[28]).",";
    $query.="maxformwidth=".sl_quote_smart($dataarray[29]).",";
    $query.="backcolor=".sl_quote_smart(substr($dataarray[30],1)).",";
    $query.="btnlabelfont=".sl_quote_smart($dataarray[31]).",";
    $query.="btnlabelstyle=".sl_quote_smart($dataarray[32]).",";
    $query.="btnbordercolor=".sl_quote_smart(substr($dataarray[33],1)).",";
    $query.="btnbordersize=".sl_quote_smart($dataarray[34]).",";
    $query.="btnborderstyle=".sl_quote_smart($dataarray[35]).",";
    $query.="inputpaddingv=".sl_quote_smart($dataarray[36]).",";
    $query.="inputpaddingh=".sl_quote_smart($dataarray[37]).",";
    $query.="btnpaddingv=".sl_quote_smart($dataarray[38]).",";
    $query.="btnpaddingh=".sl_quote_smart($dataarray[39]).",";
    $query.="value=''";   // This line needed as Mysql doesn't allow default for TEXT fields
    if (($act=="addform") || ($act=="duplform"))
    {
      $query = "INSERT INTO ".$query;
      $formid=mysqli_insert_id($mysql_link);
    }  
    else
    {
      $query = "UPDATE ".$query." WHERE position=0 AND id=".sl_quote_smart($actid);;        
      $formid=$actid;
    } 
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_MYSQLERROR);
      exit;
    }
    for ($k=0;$k<$numfields;$k++)
    {
      $dataarray=json_decode($_POST['formfield'.$k]);
      $query="sl_updateforms SET ";
      $query.="id=".sl_quote_smart($formid).",";
      $query.="position=".($k+1).",";
      $query.="sitelokfield=".sl_quote_smart($dataarray[0]).",";
      $query.="inputtype=".sl_quote_smart($dataarray[1]).",";
      $query.="labeltext=".sl_quote_smart($dataarray[2]).",";
      $query.="placetext=".sl_quote_smart($dataarray[3]).",";
      $dataarray[4]=str_replace(chr(13),"",$dataarray[4]);
      $query.="value=".sl_quote_smart($dataarray[4]).",";
      $query.="emailaction=".sl_quote_smart($dataarray[5]).",";
      $query.="validation=".sl_quote_smart($dataarray[6]).",";
      $query.="showrequired=".sl_quote_smart($dataarray[7]).",";
      $query.="errormsg=".sl_quote_smart($dataarray[8]).",";
      $query.="fieldwidth=".sl_quote_smart($dataarray[9]).",";
      $query.="bottommargin=".sl_quote_smart($dataarray[10]);
      if ((($act=="addform") || ($act=="duplform")) || (($act=="editform") && ($k>=$numexistingfields)))
        $query = "INSERT INTO ".$query;
      else
        $query = "UPDATE ".$query." WHERE position=".($k+1)." AND id=".sl_quote_smart($actid);
      $mysql_result = mysqli_query($mysql_link,$query);
      if ($mysql_result==false)
      {
        returnError($data,$errors,ADMINMSG_MYSQLERROR);
        exit;
      }
    }
    // Remove any fields >numfields (some fields may have been deleted since last time)
    $mysql_result = mysqli_query($mysql_link,"DELETE FROM sl_updateforms WHERE id=".sl_quote_smart($actid)." AND position>".$numfields);
  }
  returnSuccess($data,ADMINET_FRMSAVED,$formid);
  exit;     

  function returnSuccess($data,$msg,$formid)
  {
    $data['success'] = true;
    $data['message'] = $msg;
    $data['formid'] = $formid;    
    echo json_encode($data);
    exit;
  }

  function returnError($data,$errors,$msg)
  {
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }


  ?>