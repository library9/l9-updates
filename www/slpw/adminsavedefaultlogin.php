<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  require("admincommonfunctions.php");  
  $slsubadmin=false;
  $errors = array();    // array to hold validation errors
  $data = array();      // array to pass back data   
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_CSRFFAILED.".");
    exit;
  }   
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_MYSQLERROR.".");
    exit;
  }
  $newbackgroundcolor=$_POST['newbackgroundcolor'];    
  $newbackgroundimage=$_POST['newbackgroundimage'];    
  $newbackgroundimagerp=$_POST['newbackgroundimagerp']; 
  $newloginfont=$_POST['newloginfont']; 
  $newloginboxfilltype=$_POST['newloginboxfilltype'];    
  $newloginboxcolorfrom=$_POST['newloginboxcolorfrom'];    
  $newloginboxcolorto=$_POST['newloginboxcolorto'];    
  $newloginboxshape=$_POST['newloginboxshape'];    
  $newloginboxshadow=$_POST['newloginboxshadow'];    
  $newtitletext=$_POST['newtitletext'];    
  $newtitlecolor=$_POST['newtitlecolor'];    
  $newtitlesize=$_POST['newtitlesize'];    
  $newtitlealign=$_POST['newtitlealign'];    
  $newtitlefont=$_POST['newtitlefont'];    
  $newmessagecolor=$_POST['newmessagecolor'];    
  $newmessagesize=$_POST['newmessagesize'];    
  $newmessagealign=$_POST['newmessagealign'];    
  $newusernamelabel=$_POST['newusernamelabel'];    
  $newpasswordlabel=$_POST['newpasswordlabel'];    
  $newcaptchalabel=$_POST['newcaptchalabel'];    
  $newrememberlabel=$_POST['newrememberlabel'];    
  $newautologinlabel=$_POST['newautologinlabel'];    
  $newlabelcolor=$_POST['newlabelcolor'];    
  $newlabelsize=$_POST['newlabelsize'];    
  $newinputtextcolor=$_POST['newinputtextcolor'];    
  $newinputtextsize=$_POST['newinputtextsize'];    
  $newinputbackcolor=$_POST['newinputbackcolor'];
  $newinputshowicons=$_POST['newinputshowicons'];
  $newlogintext=$_POST['newlogintext'];    
  $newlogintextfont=$_POST['newlogintextfont'];    
  $newlogintextcolor=$_POST['newlogintextcolor'];    
  $newlogintextsize=$_POST['newlogintextsize'];    
  $newlogintextstyle=$_POST['newlogintextstyle'];    
  $newloginbuttonfilltype=$_POST['newloginbuttonfilltype'];    
  $newloginbuttoncolorfrom=$_POST['newloginbuttoncolorfrom'];    
  $newloginbuttoncolorto=$_POST['newloginbuttoncolorto'];    
  $newloginbuttonshape=$_POST['newloginbuttonshape'];    
  $newbtnborderstyle=$_POST['newbtnborderstyle'];    
  $newbtnbordercolor=$_POST['newbtnbordercolor'];    
  $newbtnbordersize=$_POST['newbtnbordersize'];    
  $newforgottext=$_POST['newforgottext'];    
  $newforgotcolor=$_POST['newforgotcolor'];    
  $newforgotsize=$_POST['newforgotsize'];    
  $newsignuptext=$_POST['newsignuptext'];    
  $newsignupurl=$_POST['newsignupurl'];    
  $newsignupcolor=$_POST['newsignupcolor'];    
  $newsignupsize=$_POST['newsignupsize'];    
  $newsignupalign=$_POST['newsignupalign'];    
  // Validate input
  if (!isValidColor($newbackgroundcolor))
    $newbackgroundcolor="#FFFFFF";
  if (!isValidColor($newloginboxcolorfrom))
    $newloginboxcolorfrom="#1A305E";
  if (!isValidColor($newloginboxcolorto))
    $newloginboxcolorto="#2F5DAA";
  if (!isValidColor($newtitlecolor))
    $newtitlecolor="#FFFFFF";
  if (!isValidColor($newlabelcolor))
    $newlabelcolor="#FFFFFF";
  if (!isValidColor($newinputtextcolor))
    $newinputtextcolor="#1A305E";
  if (!isValidColor($newinputbackcolor))
    $newinputbackcolor="#FFFFFF";
  if (!isValidColor($newmessagecolor))
    $newmessagecolor="#FF0000";
  if (!isValidColor($newlogintextcolor))
    $newlogintextcolor="#FFFFFF";
  if (!isValidColor($newloginbuttoncolorfrom))
    $newloginbuttoncolorfrom="#79BCFF";
  if (!isValidColor($newloginbuttoncolorto))
    $newloginbuttoncolorto="#378EE5";
  if (!isValidColor($newbtnbordercolor))
    $newbtnbordercolor="#FFFFFF";
  if (!isValidColor($newforgotcolor))
    $newforgotcolor="#FFFFFF";
  if (!isValidColor($newsignupcolor))
    $newsignupcolor="#FFA500";
  if (!isValidSize($newtitlesize))
    $newtitlesize="44";
  if (!isValidSize($newlabelsize))
    $newlabelsize="18";
  if (!isValidSize($newinputtextsize))
    $newinputtextsize="16";
  if (!isValidSize($newmessagesize))
    $newmessagesize="16";
  if (!isValidSize($newlogintextsize))
    $newlogintextsize="17";
  if (!isValidSize($newbtnbordersize))
    $newbtnbordersize="0";
  if (!isValidSize($newforgotsize))
    $newforgotsize="14";
  if (!isValidSize($newsignupsize))
    $newsignupsize="16";
  // Remove # from colors
  $newbackgroundcolor=substr($newbackgroundcolor,1);
  $newloginboxcolorfrom=substr($newloginboxcolorfrom,1);
  $newloginboxcolorto=substr($newloginboxcolorto,1);
  $newtitlecolor=substr($newtitlecolor,1);
  $newlabelcolor=substr($newlabelcolor,1);
  $newinputtextcolor=substr($newinputtextcolor,1);
  $newinputbackcolor=substr($newinputbackcolor,1);
  $newmessagecolor=substr($newmessagecolor,1);
  $newlogintextcolor=substr($newlogintextcolor,1);
  $newloginbuttoncolorfrom=substr($newloginbuttoncolorfrom,1);
  $newloginbuttoncolorto=substr($newloginbuttoncolorto,1);
  $newbtnbordercolor=substr($newbtnbordercolor,1);
  $newforgotcolor=substr($newforgotcolor,1);
  $newsignupcolor=substr($newsignupcolor,1);
  if (!$DemoMode)
  {
    // Save settings to table and set current settings
    $query = "UPDATE sl_logintemplate SET ";
    $query.="backcolor=".sl_quote_smart($newbackgroundcolor).",";
    $query.="backimage=".sl_quote_smart($newbackgroundimage).",";
    $query.="backimagerp=".sl_quote_smart($newbackgroundimagerp).",";
    $query.="mainfont=".sl_quote_smart($newloginfont).",";
    $query.="boxcolortype=".sl_quote_smart($newloginboxfilltype).",";
    $query.="boxcolorfrom=".sl_quote_smart($newloginboxcolorfrom).",";
    $query.="boxcolorto=".sl_quote_smart($newloginboxcolorto).",";
    $query.="boxradius=".sl_quote_smart($newloginboxshape).",";
    $query.="boxshadow=".sl_quote_smart($newloginboxshadow).",";
    $query.="title=".sl_quote_smart($newtitletext).",";
    $query.="titlecolor=".sl_quote_smart($newtitlecolor).",";
    $query.="titlesize=".sl_quote_smart($newtitlesize).",";
    $query.="titlealign=".sl_quote_smart($newtitlealign).",";
    $query.="titlefont=".sl_quote_smart($newtitlefont).",";
    $query.="msgcolor=".sl_quote_smart($newmessagecolor).",";
    $query.="msgsize=".sl_quote_smart($newmessagesize).",";
    $query.="msgalign=".sl_quote_smart($newmessagealign).",";
    $query.="username=".sl_quote_smart($newusernamelabel).",";
    $query.="password=".sl_quote_smart($newpasswordlabel).",";
    if ($TuringLogin==1)
      $query.="captcha=".sl_quote_smart($newcaptchalabel).",";
    if ($CookieLogin==1)
      $query.="remember=".sl_quote_smart($newrememberlabel).",";
    if ($CookieLogin==2)
      $query.="autologin=".sl_quote_smart($newautologinlabel).",";
    $query.="labelcolor=".sl_quote_smart($newlabelcolor).",";
    $query.="labelsize=".sl_quote_smart($newlabelsize).",";
    $query.="inputcolor=".sl_quote_smart($newinputtextcolor).",";
    $query.="inputsize=".sl_quote_smart($newinputtextsize).",";
    $query.="inputbackcolor=".sl_quote_smart($newinputbackcolor).",";
    $query.="showicons=".sl_quote_smart($newinputshowicons).",";
    $query.="btnlbltext=".sl_quote_smart($newlogintext).",";
    $query.="btnlblfont=".sl_quote_smart($newlogintextfont).",";
    $query.="btnlblcolor=".sl_quote_smart($newlogintextcolor).",";
    $query.="btnlblsize=".sl_quote_smart($newlogintextsize).",";
    $query.="btnlblstyle=".sl_quote_smart($newlogintextstyle).",";
    $query.="btncolortype=".sl_quote_smart($newloginbuttonfilltype).",";
    $query.="btncolorfrom=".sl_quote_smart($newloginbuttoncolorfrom).",";
    $query.="btncolorto=".sl_quote_smart($newloginbuttoncolorto).",";
    $query.="btnradius=".sl_quote_smart($newloginbuttonshape).",";
    $query.="btnborderstyle=".sl_quote_smart($newbtnborderstyle).",";
    $query.="btnbordercolor=".sl_quote_smart($newbtnbordercolor).",";
    $query.="btnbordersize=".sl_quote_smart($newbtnbordersize).",";
    $query.="forgottxt=".sl_quote_smart($newforgottext).",";
    $query.="forgotcolor=".sl_quote_smart($newforgotcolor).",";
    $query.="forgotsize=".sl_quote_smart($newforgotsize).",";
    $query.="signuptext=".sl_quote_smart($newsignuptext).",";
    $query.="signupurl=".sl_quote_smart($newsignupurl).",";
    $query.="signupcolor=".sl_quote_smart($newsignupcolor).",";
    $query.="signupsize=".sl_quote_smart($newsignupsize).",";
    $query.="signupalign=".sl_quote_smart($newsignupalign);
    $query.=" WHERE id=1";
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_MYSQLERROR);
      exit;
    }
  } 
  returnSuccess($data,ADMINET_FRMSAVED);

  function returnSuccess($data,$msg)
  {
    $data['success'] = true;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

  function returnError($data,$errors,$msg)
  {
    $data['success'] = false;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

?>
