<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  if (!defined('ADMINET_INPPADDING'))
    define("ADMINET_INPPADDING","Input Padding (Vert Horz)");
  if (!defined('ADMINET_BTNPADDING'))
    define("ADMINET_BTNPADDING","Button Padding (Vert Horz)");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  include("admincommonfunctions.php");  
  // Get current values
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    die(ADMINMSG_MYSQLERROR);
  }
  if (isset($_GET['actid']))
    $actid=$_GET['actid'];
  if (isset($_GET['act']))
    $act=$_GET['act'];  
  // Get custom field lengths (to see if textarea type is allowed)
  $customfieldlengths=array();
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." LIMIT 1");
  $fcharset=mysqli_get_charset($mysql_link);
  $finfo=mysqli_fetch_fields($mysql_result);
  for($k=1;$k<=50;$k++)
      $customfieldlengths[$k]=($finfo[$k+7]->length)/$fcharset->max_length;  
  // Check for compatible payment plugins
  $paymentoption=false;
  $paypal=false;
  $stripe=false;
  // Check for Paypal Plugin V1.8 or above
  $v=GetPluginVersion($mysql_link,"Paypal");
  if ($v!==false)
  {
    if ((float)$v>=1.8)
    {
      $paymentoption=true;
      $paypal=true;
    }
  }  
  // Check for Stripe Plugin V1.0 or above
  $v=GetPluginVersion($mysql_link,"Stripe");
  if ($v!==false)
  {
    if ((float)$v>=1.0)
    {
      $paymentoption=true;
      $stripe=true;
    }
  }  

  function GetPluginVersion($mysql_link,$name)
  {
    global $DbPluginsTableName;
    if ((!isset($DbPluginsTableName)) || ($DbPluginsTableName==""))
      $TempPluginsTableName="sl_plugins";
    else
      $TempPluginsTableName=$DbPluginsTableName;
    $query="SELECT * FROM ".$TempPluginsTableName." WHERE name='".$name."'";
    $mysql_result=mysqli_query($mysql_link,$query);
    $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);  
    if ($row!=NULL)
      return($row['version']);
    return(false);
  }
  
  function GetPaypalProducts($mysql_link,$prefill)
  {
    global $DbPaypalProductsTableName;
    if ($DbPaypalProductsTableName=="")
      $DbPaypalProductsTableName="sl_paypalproducts";  
    $query="SELECT * FROM ".$DbPaypalProductsTableName." ORDER BY id ASC";
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_result===false)
    {
      echo "<option value=\"\">".ADMINET_NOPAYPAL."</option>\n";
      return;
    } 
    $prodfound=false; 
    while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
    {
      $id=$row['id'];      
      $enabled=$row['enabled'];      
      $description=$row['description'];
      $type=$row['type'];
      $multicurrency=$row['multicurrency'];
      if ($multicurrency==1)
      {
        $price1=$row['price1'];
        $currency1=$row['currency1'];
        if (is_numeric($price1))
        {
          echo "<option value=\"".$id.",".$currency1."\"";
          if ($prefill==rawurlencode($id).",".$currency1)
            echo " selected=\"selected\" ";          
          echo">".$id."(".$currency1.") - ".$description."</option>\n";        
        }  
        $price2=$row['price2'];
        $currency2=$row['currency2'];
        if (is_numeric($price2))
        {
          echo "<option value=\"".$id.",".$currency2."\"";
          if ($prefill==rawurlencode($id).",".$currency2)
            echo " selected=\"selected\" ";          
          echo">".$id."(".$currency2.") - ".$description."</option>\n";        
        }  
        $price3=$row['price3'];
        $currency3=$row['currency3'];
        if (is_numeric($price3))
        {
          echo "<option value=\"".$id.",".$currency3."\"";
          if ($prefill==rawurlencode($id).",".$currency3)
            echo " selected=\"selected\" ";          
          echo">".$id."(".$currency3.") - ".$description."</option>\n";        
        }  
        $price4=$row['price4'];
        $currency4=$row['currency4'];
        if (is_numeric($price4))
        {
          echo "<option value=\"".$id.",".$currency4."\"";
          if ($prefill==rawurlencode($id).",".$currency4)
            echo " selected=\"selected\" ";          
          echo">".$id."(".$currency4.") - ".$description."</option>\n";        
        }  
        $prodfound=true;
      }
      else
      {
        $price1=$row['price1'];
        $currency1=$row['currency1'];
        echo "<option value=\"".$id.",".$currency1."\"";
        if ($prefill==rawurlencode($id).",".$currency1)
          echo " selected=\"selected\" ";
        echo ">".$id." - ".$description."</option>\n";
        $prodfound=true;        
      }
    } 
    if (!$prodfound)
      echo "<option value=\"\">".ADMINET_NOPAYPAL."</option>\n";       
  }


?>
<!DOCTYPE html>

<html>
<head>
    <?php
    $pagename="registerformedit";
    include("adminhead.php");
    ?>
    <title><?php echo ADMINMENU_REGISTERFORMDESIGN; ?></title>
    <link rel="stylesheet" href="jquery-minicolors-master/jquery.minicolors.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />
    <link rel="stylesheet" href="registerformedit.css" type="text/css">


 <!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
    <!--[if lte IE 6]>
  <style type="text/css">
    div#loginbox {
    width: 420px;
    }
<![endif]-->
<script type="text/javascript">
// Run this when the page first loads
var gradientPrefix = getCssValuePrefix('background','linear-gradient(to bottom, #79bcff 5%, #378ee5 100%)');
var radiusPrefix = getCssValuePrefix('border-radius','10px');
currenteditfield=-1;
// Setup global form field arrays
var sitelokfield_array = new Array();
var inputtype_array = new Array();
var labeltext_array = new Array();
var placetext_array = new Array();
var value_array = new Array();
var checked_array = new Array();
var validation_array = new Array();
var showrequired_array = new Array();
var errormsg_array = new Array();
var fieldwidth_array = new Array();
var bottommargin_array = new Array();

var showformerrormessage=false;
var numberoffields;

// Detect which browser prefix to use for the specified CSS value
// (e.g., background-image: -moz-linear-gradient(...);
//        background-image:   -o-linear-gradient(...); etc).
//
// http://stackoverflow.com/questions/15071062/using-javascript-to-edit-css-gradient
function getCssValuePrefix(name, value) {
    var prefixes = ['', '-o-', '-ms-', '-moz-', '-webkit-'];

    // Create a temporary DOM object for testing
    var dom = document.createElement('div');

    for (var i = 0; i < prefixes.length; i++) {
        // Attempt to set the style
        dom.style[name] = prefixes[i] + value;

        // Detect if the style was successfully set
        if (dom.style[name]) {
            return prefixes[i];
        }
        dom.style[name] = '';   // Reset the style
    }
}

function startvalues()
{
  <?php if ($act=="addform") { ?>
  // Set form style variables
  document.getElementById('newformname').value='Untitled';
  document.getElementById('newusergroup').value='CLIENT';
  document.getElementById('newexpiry').value='365';
  document.getElementById('newredirect').value='registerthanks.php';
  $('#newuseremail').val('');
  $('#senduseremaildiv').hide();
  $('#newuseremailname').text(ADMINMSG_NOTEMPLATE);
  $('#senduseremail').iCheck('uncheck');
  $('#newadminemail').val('');
  $('#sendadminemaildiv').hide();
  $('#newadminemailname').text(ADMINMSG_NOTEMPLATE);
  $('#sendadminemail').iCheck('uncheck');
  $('#newenabled').selectpicker('val', 'Yes');    
  $('#newfonttype').selectpicker('val', 'Arial, Helvetica, sans-serif');    
  $('#newlabelcolor').minicolors('value', '#1A305E');
  document.getElementById('newlabelsize').value='18';
  $('#newlabelstyle').selectpicker('val', 'normal normal bold');    
  $('#newinputtextcolor').minicolors('value', '#1A305E');
  document.getElementById('newinputtextsize').value='16';
  $('#newinputtextstyle').selectpicker('val', 'normal normal normal');    
  $('#newinputbackcolor').minicolors('value', '#FFFFFF');  
  document.getElementById('newbordersize').value='1';
  $('#newbordersize').selectpicker('val', '1');    
  $('#newbordercolor').minicolors('value', '#378EE5');  
  $('#newborderradius').selectpicker('val', '0');    
  $('#newinputpaddingv').selectpicker('val', '0.3em');    
  $('#newinputpaddingh').selectpicker('val', '0.3em');    
  document.getElementById('newrqdfieldlabel').value='*';
  $('#newrqdfieldcolor').minicolors('value', '#FF0000');  
  document.getElementById('newrqdfieldsize').value='12';
  $('#newrqdfieldstyle').selectpicker('val', 'normal normal normal');      
  $('#newmessagecolor').minicolors('value', '#FF0000');  
  document.getElementById('newmessagesize').value='12';
  $('#newmessagestyle').selectpicker('val', 'normal normal normal');    
  document.getElementById('newbtnlabel').value='Register';
  $('#newbtnlabelfont').selectpicker('val', 'Arial, Helvetica, sans-serif');    
  $('#newbtnlabelstyle').selectpicker('val', 'normal normal bold');    
  $('#newbtnlabelcolor').minicolors('value', '#FFFFFF');  
  document.getElementById('newbtnlabelsize').value='14';
  $('#newbtncolortype').selectpicker('val', 'gradient');    
  $('#newbtncolorfrom').minicolors('value', '#1A305E');    
  $('#newbtncolorto').minicolors('value', '#378EE5');    
  $('#newbtnborderstyle').selectpicker('val', 'solid');    
  $('#newbtnbordercolor').minicolors('value', '#FFFFFF');
  $('#newbtnpaddingv').selectpicker('val', '6');    
  $('#newbtnpaddingh').selectpicker('val', '24');        
  document.getElementById('newbtnbordersize').value='0';
  $('#newbtnradius').selectpicker('val', '10');    
  document.getElementById('newformerrormsg').value='Please correct the errors below';
  $('#newformerrormsgcolor').minicolors('value', '#FF0000');    
  document.getElementById('newformerrormsgsize').value='14';
  $('#newformerrormsgstyle').selectpicker('val', 'normal normal bold');    
  $('#newpreviewbackcolor').minicolors('value', '#FFFFFF');    
  document.getElementById('newmaxformwidth').value='300';

  // Set field properties
  numberoffields=2;
  
  sitelokfield_array[0]='name';
  inputtype_array[0]='text';
  labeltext_array[0]='Full name';
  placetext_array[0]='full name';
  value_array[0]='';
  checked_array[0]='0';
  validation_array[0]='required';
  showrequired_array[0]='1';
  errormsg_array[0]='Please enter your full name';
  fieldwidth_array[0]='100';
  bottommargin_array[0]='20';
  
  inputtype_array[1]='text';
  sitelokfield_array[1]='email';
  labeltext_array[1]='Email';
  placetext_array[1]='email';
  value_array[1]='';
  checked_array[1]='0';
  validation_array[1]='required';
  showrequired_array[1]='1';
  errormsg_array[1]='Please enter your email';
  fieldwidth_array[1]='100';
  bottommargin_array[1]='20';
  
  <?php if ($TuringRegister==1) { ?>
  numberoffields++;
  inputtype_array[2]='captcha';
  sitelokfield_array[2]='captcha';
  labeltext_array[2]='Captcha';
  placetext_array[2]='captcha';
  value_array[2]='';
  checked_array[2]='0';
  validation_array[2]='required';
  showrequired_array[2]='1';
  errormsg_array[2]='Please enter the captcha code';
  fieldwidth_array[2]='100';
  bottommargin_array[2]='20';
<?php } ?>
  <?php } ?>
  
  <?php if (($act=="editform") || ($act=="duplform")){
  $mysql_result=mysqli_query($mysql_link,"SELECT name FROM sl_forms WHERE id=".sl_quote_smart($actid));
  if ($mysql_result===false)
  {
    print("Can't read form template");
    exit;      
  } 
  $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
  $formname=$row['name'];
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_registerforms WHERE id=".sl_quote_smart($actid)." AND position=0");
  if ($mysql_result===false)
  {
    print("Can't read form template");
    exit;      
  }
  $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
  $registertype="normal";  
  if (($row['usergroup']=="SLNOTPAID") && (false!==strpos($row['redirect'],"registerpaypal.php")))
  {
    $registertype="paypal";
    // Get product details for product prefill
    $pos=strpos($row['redirect'],"&id=");
    $productval=substr($row['redirect'],$pos+4);
    $productval=str_replace("&cy=",",",$productval);
  }  
  if (($row['usergroup']=="SLNOTPAID") && (false!==strpos($row['redirect'],"stripecharge.php")))
  {
    $registertype="stripe";
  } 
  ?>
  // Set form style variables
  document.getElementById('newformname').value='<?php if ($act=="duplform") echo str_replace("'","\'",$formname)." copy"; else echo str_replace("'","\'",$formname); ?>';
  document.getElementById('newusergroup').value='<?php echo $row['usergroup']; ?>';
  document.getElementById('newexpiry').value='<?php echo $row['expiry']; ?>';
  document.getElementById('newredirect').value='<?php echo $row['redirect']; ?>';
  document.getElementById('newuseremail').value='<?php echo $row['useremail']; ?>';
  document.getElementById('newadminemail').value='<?php echo $row['adminemail']; ?>';
  <?php if ($row['useremail']=="") { ?>
  $('#newuseremail').val('');
  $('#senduseremaildiv').hide();
  $('#newuseremailname').text(ADMINMSG_NOTEMPLATE);
  $('#senduseremail').iCheck('uncheck');
  <?php } else { ?>
  $('#newuseremail').val('<?php echo $row['useremail']; ?>');
  $('#senduseremaildiv').show();
  $('#newuseremailname').text('<?php echo $row['useremail']; ?>');
  $('#senduseremail').iCheck('check');
  <?php } ?>
  <?php if ($row['adminemail']=="") { ?>
  $('#newadminemail').val('');
  $('#sendadminemaildiv').hide();
  $('#newadminemailname').text(ADMINMSG_NOTEMPLATE);
  $('#sendadminemail').iCheck('uncheck');
  <?php } else { ?>
  $('#newadminemail').val('<?php echo $row['adminemail']; ?>');
  $('#sendadminemaildiv').show();
  $('#newadminemailname').text('<?php echo $row['adminemail']; ?>');
  $('#sendadminemail').iCheck('check');
  <?php } ?>
  //document.getElementById('newenabled').value='<?php echo $row['enabled']; ?>';
  $('#newenabled').selectpicker('val', '<?php echo $row['enabled']; ?>');
  $('#newfonttype').selectpicker('val', '<?php echo $row['fonttype']; ?>');  
  $('#newlabelcolor').minicolors('value', '#<?php echo $row['labelcolor']; ?>');    
  document.getElementById('newlabelsize').value='<?php echo $row['labelsize']; ?>';
  $('#newlabelstyle').selectpicker('val', '<?php echo $row['labelstyle']; ?>');  
  $('#newinputtextcolor').minicolors('value', '#<?php echo $row['inputtextcolor']; ?>');      
  document.getElementById('newinputtextsize').value='<?php echo $row['inputtextsize']; ?>';
  $('#newinputtextstyle').selectpicker('val', '<?php echo $row['inputtextstyle']; ?>');  
  $('#newinputbackcolor').minicolors('value', '#<?php echo $row['inputbackcolor']; ?>');      
  $('#newbordersize').selectpicker('val', '<?php echo $row['bordersize']; ?>');  
  $('#newbordercolor').minicolors('value', '#<?php echo $row['bordercolor']; ?>');      
  $('#newborderradius').selectpicker('val', '<?php echo $row['borderradius']; ?>');  
  $('#newinputpaddingv').selectpicker('val', '<?php echo $row['inputpaddingv']; ?>');  
  $('#newinputpaddingh').selectpicker('val', '<?php echo $row['inputpaddingh']; ?>');  
  document.getElementById('newrqdfieldlabel').value='<?php echo $row['rqdfieldlabel']; ?>';
  $('#newrqdfieldcolor').minicolors('value', '#<?php echo $row['rqdfieldcolor']; ?>');      
  document.getElementById('newrqdfieldsize').value='<?php echo $row['rqdfieldsize']; ?>';
  $('#newrqdfieldstyle').selectpicker('val', '<?php echo $row['rqdfieldstyle']; ?>');    
  $('#newmessagecolor').minicolors('value', '#<?php echo $row['messagecolor']; ?>');      
  document.getElementById('newmessagesize').value='<?php echo $row['messagesize']; ?>';
  $('#newmessagestyle').selectpicker('val', '<?php echo $row['messagestyle']; ?>');    
  document.getElementById('newbtnlabel').value='<?php echo $row['btnlabel']; ?>';
  $('#newbtnlabelfont').selectpicker('val', '<?php echo $row['btnlabelfont']; ?>');    
  $('#newbtnlabelstyle').selectpicker('val', '<?php echo $row['btnlabelstyle']; ?>');    
  $('#newbtnlabelcolor').minicolors('value', '#<?php echo $row['btnlabelcolor']; ?>');      
  document.getElementById('newbtnlabelsize').value='<?php echo $row['btnlabelsize']; ?>';
  $('#newbtncolortype').selectpicker('val', '<?php echo $row['btncolortype']; ?>');    
  $('#newbtncolorfrom').minicolors('value', '#<?php echo $row['btncolorfrom']; ?>');      
  $('#newbtncolorto').minicolors('value', '#<?php echo $row['btncolorto']; ?>');      
  $('#newbtnborderstyle').selectpicker('val', '<?php echo $row['btnborderstyle']; ?>');    
  $('#newbtnbordercolor').minicolors('value', '#<?php echo $row['btnbordercolor']; ?>');      
  document.getElementById('newbtnbordersize').value='<?php echo $row['btnbordersize']; ?>';
  $('#newbtnpaddingv').selectpicker('val', '<?php echo $row['btnpaddingv']; ?>');  
  $('#newbtnpaddingh').selectpicker('val', '<?php echo $row['btnpaddingh']; ?>');  
  $('#newbtnradius').selectpicker('val', '<?php echo $row['btnradius']; ?>');    
  document.getElementById('newformerrormsg').value='<?php echo str_replace("'","\'",$row['formerrormsg']); ?>';
  $('#newformerrormsgcolor').minicolors('value', '#<?php echo $row['formerrormsgcolor']; ?>');      
  document.getElementById('newformerrormsgsize').value='<?php echo $row['formerrormsgsize']; ?>';
  $('#newformerrormsgstyle').selectpicker('val', '<?php echo $row['formerrormsgstyle']; ?>');    
  $('#newpreviewbackcolor').minicolors('value', '#<?php echo $row['backcolor']; ?>');      
  document.getElementById('newmaxformwidth').value='<?php echo $row['maxformwidth']; ?>';
  <?php
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_registerforms WHERE id=".sl_quote_smart($actid)." AND position>0 ORDER BY position ASC");
  if ($mysql_result===false)
  {
    print("Can't read form template");
    exit;      
  }
  $numfields=0;
  while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
  {
  $val=str_replace("\n","\\n",$row['value']);
  $val=str_replace("'","\'",$val);
  ?>  
  sitelokfield_array[<?php echo $numfields; ?>]='<?php echo $row['sitelokfield']; ?>'
  inputtype_array[<?php echo $numfields; ?>]='<?php echo $row['inputtype']; ?>'
  labeltext_array[<?php echo $numfields; ?>]='<?php echo str_replace("'","\'",$row['labeltext']); ?>'
  placetext_array[<?php echo $numfields; ?>]='<?php echo str_replace("'","\'",$row['placetext']); ?>'
  value_array[<?php echo $numfields; ?>]='<?php echo $val; ?>'
  checked_array[<?php echo $numfields; ?>]='<?php echo $row['checked']; ?>'
  validation_array[<?php echo $numfields; ?>]='<?php echo $row['validation']; ?>'
  showrequired_array[<?php echo $numfields; ?>]='<?php echo $row['showrequired']; ?>'
  errormsg_array[<?php echo $numfields; ?>]='<?php echo str_replace("'","\'",$row['errormsg']); ?>'
  fieldwidth_array[<?php echo $numfields; ?>]='<?php echo $row['fieldwidth']; ?>'
  bottommargin_array[<?php echo $numfields; ?>]='<?php echo $row['bottommargin']; ?>'
  <?php
  $numfields++;
  } ?>
  numberoffields=<?php echo $numfields; ?>  
<?php } ?>
  filltype('newbtncolortype','newbtncolortodiv')
  updatepropertiesform()
  updatePreview()
}

function addfield()
{
  customtitles = new Array();
  // Get custom field titles from PHP
  <?php for ($k=1;$k<=50;$k++)
  {
    $var="CustomTitle".$k;
    if ($$var!="")
      echo "  customtitles[$k]='".str_replace("'","\s",$$var)."'\n";
    else
      echo "  customtitles[$k]='Custom $k'\n";      
  }
  ?>
  numberoffields++;
  currenteditfield=numberoffields-1;
  // If no name field then add it next
  if (sitelokfield_array.indexOf("name")==-1)
  {
    sitelokfield_array[currenteditfield]='name';  
    inputtype_array[currenteditfield]='text'; 
    labeltext_array[currenteditfield]='Full name';
    placetext_array[currenteditfield]='full name';
    value_array[currenteditfield]='';
    checked_array[currenteditfield]='0';
    validation_array[currenteditfield]='required';
    showrequired_array[currenteditfield]='1';
    errormsg_array[currenteditfield]='Please enter your full name';
    fieldwidth_array[currenteditfield]='100';
    bottommargin_array[currenteditfield]=20;
    editfield(currenteditfield)
    updatePreview()
    return
  } 
  // If no email then add it next
  if (sitelokfield_array.indexOf("email")==-1)
  {
    sitelokfield_array[currenteditfield]='email';  
    inputtype_array[currenteditfield]='text'; 
    labeltext_array[currenteditfield]='Email';
    placetext_array[currenteditfield]='email';
    value_array[currenteditfield]='';
    checked_array[currenteditfield]='0';
    validation_array[currenteditfield]='required';
    showrequired_array[currenteditfield]='1';
    errormsg_array[currenteditfield]='Please enter your email';
    fieldwidth_array[currenteditfield]='100';
    bottommargin_array[currenteditfield]=20;
    editfield(currenteditfield)
    updatePreview()
    return
  }
  var k
  for (k=1;k<=50;k++)
  {
    if (sitelokfield_array.indexOf("custom"+k)==-1)
    {
      sitelokfield_array[currenteditfield]='custom'+k;  
      inputtype_array[currenteditfield]='text'; 
//      labeltext_array[currenteditfield]=customtitles[k];
//      placetext_array[currenteditfield]=customtitles[k].toLowerCase();
      labeltext_array[currenteditfield]='Label text';
      placetext_array[currenteditfield]='placeholder';
      value_array[currenteditfield]='';
      checked_array[currenteditfield]='0';
      validation_array[currenteditfield]='notrequired';
      showrequired_array[currenteditfield]='0';
      errormsg_array[currenteditfield]='This field is required';
      fieldwidth_array[currenteditfield]='100';
      bottommargin_array[currenteditfield]=20;
      editfield(currenteditfield)
      updatePreview()
      return
    }
  }
  // If we are still here then add custom 1 again
  sitelokfield_array[currenteditfield]='custom1';  
  inputtype_array[currenteditfield]='text'; 
//  labeltext_array[currenteditfield]=customtitles[1];
//  placetext_array[currenteditfield]=customtitles[1].toLowerCase();
  labeltext_array[currenteditfield]='Label text';
  placetext_array[currenteditfield]='placeholder';
  value_array[currenteditfield]='';
  checked_array[currenteditfield]='0';
  validation_array[currenteditfield]='notrequired';
  showrequired_array[currenteditfield]='0';
  errormsg_array[currenteditfield]='This field is required';
  fieldwidth_array[currenteditfield]='100';
  bottommargin_array[currenteditfield]=20;
  editfield(currenteditfield)
  updatePreview()   
}

function editfield(fieldno)
{
  currenteditfield=fieldno
  if (currenteditfield==-1)
  {
    document.getElementById('nopropertiesdiv').style['display']="block"; 
    document.getElementById('propertiesdiv').style['display']="none"; 
  }
  else
  {
    document.getElementById('nopropertiesdiv').style['display']="none"; 
    document.getElementById('propertiesdiv').style['display']="block"; 
    // Get current field details and enter in form
    setfieldproperties(fieldno);
    fieldpropertiesform(inputtype_array[fieldno])
    // Remove options from position dropdown
    var selectobj = document.getElementById('newposition');
    var k;
    for(k=selectobj.options.length-1;k>=0;k--)
      selectobj.remove(k);
    // Add new options to position drop down
    var newpos=0; 
    for (k=0;k<=numberoffields;k++)
    {
      if (k==(currenteditfield+1))
        continue
      opt = document.createElement('option');
      opt.value = newpos;
      newpos++
      if (k==0)
        opt.innerHTML = 'top'; 
      else
      {  
        opt.innerHTML = 'after '+labeltext_array[k-1];
      }
      selectobj.appendChild(opt);  
    }
    selectobj.value=fieldno
    $('#newposition').selectpicker('refresh');

    // document.getElementById('newsitelokfield').focus()
//    setTimeout(function() { document.getElementById('newsitelokfield').focus();$("#newsitelokfield").get(0).scrollIntoView(); }, 300);  // Allow panel to open before focus
  }
  $('#collapse1').collapse('hide');
  $('#collapse2').collapse('hide');
  $('#collapse3').collapse('hide');
  $('#collapse4').collapse('show');
  setTimeout(function() { document.getElementById('newsitelokfield').focus();$("#collapse3").get(0).scrollIntoView(); }, 300);  // Allow panel to open before focus
  updatePreview()
}

function setfieldproperties(fieldno)
{
  $('#newsitelokfield').selectpicker('val', sitelokfield_array[fieldno]);   
  $('#newinputtype').selectpicker('val', inputtype_array[fieldno]);   
  document.getElementById('newlabeltext').value=labeltext_array[fieldno]; 
  document.getElementById('newplacetext').value=placetext_array[fieldno]; 
  document.getElementById('newvalue').value=value_array[fieldno]; 
  document.getElementById('newvaluedropdown').value=value_array[fieldno]; 
  $('#newchecked').selectpicker('val', checked_array[fieldno]);   
  $('#newvalidation').selectpicker('val', validation_array[fieldno]);   
  $('#newvalidationdropdown').selectpicker('val', validation_array[fieldno]);   
  $('#newshowrequired').selectpicker('val', showrequired_array[fieldno]);   
  document.getElementById('newerrormsg').value=errormsg_array[fieldno] 
  $('#newfieldwidth').selectpicker('val', fieldwidth_array[fieldno]);   
  $('#newbottommargin').selectpicker('val', bottommargin_array[fieldno]);   
}

function fieldpropertiesform()
{
  var fieldtype=inputtype_array[currenteditfield]
  if ((fieldtype=='text') || (fieldtype=="password"))
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="none"; 
    document.getElementById('valuedropdowndiv').style['display']="none";
    document.getElementById('placeholderdiv').style['display']="block";
    document.getElementById('errormsgdiv').style['display']="block";     
    document.getElementById('fieldwidthdiv').style['display']="block";     
    document.getElementById('validationdiv').style['display']="block";     
    document.getElementById('validationdropdowndiv').style['display']="none";
    document.getElementById('checkeddiv').style['display']="none"; 
    if (document.getElementById('newvalidation').value=='notrequired')
    {
      document.getElementById('errormsgdiv').style['display']='none'
      document.getElementById('showrequireddiv').style['display']='none'
    }  
    else
    {
      document.getElementById('errormsgdiv').style['display']='block'
      document.getElementById('showrequireddiv').style['display']='block'
    }      
  }
  if ((fieldtype=='checkbox') || (fieldtype=="radio"))
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="block"; 
    document.getElementById('valuedropdowndiv').style['display']="none"; 
    document.getElementById('checkeddiv').style['display']="block"; 
    document.getElementById('placeholderdiv').style['display']="none";
    document.getElementById('errormsgdiv').style['display']="block";     
    document.getElementById('fieldwidthdiv').style['display']="none";     
    document.getElementById('validationdiv').style['display']="block";     
    document.getElementById('validationdropdowndiv').style['display']="none";     
//    if (document.getElementById('newvalidation').value=='notrequired')
    if (validation_array[currenteditfield]=='notrequired')
    {
      document.getElementById('errormsgdiv').style['display']='none'
      document.getElementById('showrequireddiv').style['display']='none'
    }  
    else
    {
      document.getElementById('errormsgdiv').style['display']='block'
      document.getElementById('showrequireddiv').style['display']='block'
    }  
  }
  if (fieldtype=='dropdown')
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="none"; 
    document.getElementById('valuedropdowndiv').style['display']="block"; 
    document.getElementById('checkeddiv').style['display']="none"; 
    document.getElementById('placeholderdiv').style['display']="none";
    document.getElementById('errormsgdiv').style['display']="block";     
    document.getElementById('fieldwidthdiv').style['display']="block";     
    document.getElementById('validationdiv').style['display']="none";     
    document.getElementById('validationdropdowndiv').style['display']="block";     
//    if (document.getElementById('newvalidationdropdown').value=='notrequired')
    if (validation_array[currenteditfield]=='notrequired')
    {
      document.getElementById('errormsgdiv').style['display']='none'
      document.getElementById('showrequireddiv').style['display']='none'
    }  
    else
    {
      document.getElementById('errormsgdiv').style['display']='block'
      document.getElementById('showrequireddiv').style['display']='block'
    }  
  }
  if (fieldtype=='label')
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="none"; 
    document.getElementById('valuedropdowndiv').style['display']="none"; 
    document.getElementById('checkeddiv').style['display']="none"; 
    document.getElementById('placeholderdiv').style['display']="none";
    document.getElementById('errormsgdiv').style['display']="none";     
    document.getElementById('fieldwidthdiv').style['display']="none";     
    document.getElementById('validationdiv').style['display']="none";     
    document.getElementById('validationdropdowndiv').style['display']="none";     
    document.getElementById('showrequireddiv').style['display']='block'
  }
  if (fieldtype=='captcha')
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="none"; 
    document.getElementById('valuedropdowndiv').style['display']="none"; 
    document.getElementById('checkeddiv').style['display']="none"; 
    document.getElementById('placeholderdiv').style['display']="block";
    document.getElementById('errormsgdiv').style['display']="block";     
    document.getElementById('fieldwidthdiv').style['display']="none";     
    document.getElementById('validationdiv').style['display']="block";     
    document.getElementById('validationdropdowndiv').style['display']="none";     
//    if (document.getElementById('newvalidation').value=='notrequired')
    if (validation_array[currenteditfield]=='notrequired')
    {
      document.getElementById('errormsgdiv').style['display']='none'
      document.getElementById('showrequireddiv').style['display']='none'
    }  
    else
    {
      document.getElementById('errormsgdiv').style['display']='block'
      document.getElementById('showrequireddiv').style['display']='block'
    }  
  }
  if (fieldtype=='file')
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="none"; 
    document.getElementById('valuedropdowndiv').style['display']="none";
    document.getElementById('checkeddiv').style['display']="none"; 
    document.getElementById('placeholderdiv').style['display']="none";
    document.getElementById('errormsgdiv').style['display']="block";     
    document.getElementById('fieldwidthdiv').style['display']="none";     
    document.getElementById('validationdiv').style['display']="block";     
    document.getElementById('validationdropdowndiv').style['display']="none";
//    if (document.getElementById('newvalidation').value=='notrequired')
    if (validation_array[currenteditfield]=='notrequired')
    {
      document.getElementById('errormsgdiv').style['display']='none'
      document.getElementById('showrequireddiv').style['display']='none'
    }  
    else
    {
      document.getElementById('errormsgdiv').style['display']='block'
      document.getElementById('showrequireddiv').style['display']='block'
    }      
  }
  if (fieldtype=='textarea')
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="none"; 
    document.getElementById('valuedropdowndiv').style['display']="none";
    document.getElementById('placeholderdiv').style['display']="block";
    document.getElementById('errormsgdiv').style['display']="block";     
    document.getElementById('fieldwidthdiv').style['display']="block";     
    document.getElementById('validationdiv').style['display']="block";     
    document.getElementById('validationdropdowndiv').style['display']="none";
    document.getElementById('checkeddiv').style['display']="none"; 
    if (document.getElementById('newvalidation').value=='notrequired')
    {
      document.getElementById('errormsgdiv').style['display']='none'
      document.getElementById('showrequireddiv').style['display']='none'
    }  
    else
    {
      document.getElementById('errormsgdiv').style['display']='block'
      document.getElementById('showrequireddiv').style['display']='block'
    }      
  }
  if (fieldtype=='usergroup')
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="none"; 
    document.getElementById('valuedropdowndiv').style['display']="block"; 
    document.getElementById('checkeddiv').style['display']="none"; 
    document.getElementById('placeholderdiv').style['display']="none";
    document.getElementById('errormsgdiv').style['display']="block";     
    document.getElementById('fieldwidthdiv').style['display']="none";     
    document.getElementById('validationdiv').style['display']="none";     
    document.getElementById('validationdropdowndiv').style['display']="block";
    // Get list of groups used
    groups=document.getElementById('newusergroup').value.split(",")
    groupoptions=''
    for (j=0;j<groups.length;j++)
    {
      groups[j]=groups[j].trim()
      if (groups[j]!='')
        groupoptions+='Join '+groups[j]+','+groups[j]+'\n'
    }
//    if (document.getElementById('newvalidationdropdown').value=='notrequired')
    if (validation_array[currenteditfield]=='notrequired')
    {
      document.getElementById('errormsgdiv').style['display']='none'
      document.getElementById('showrequireddiv').style['display']='none'
      // If value is blank then insert default
      if (document.getElementById('newvaluedropdown').value.trim()=='')
      {
        document.getElementById('newvaluedropdown').value=groupoptions
        value_array[currenteditfield]=groupoptions
      }  
    }  
    else
    {
      document.getElementById('errormsgdiv').style['display']='block'
      document.getElementById('showrequireddiv').style['display']='block'
      // If value is blank then insert default
      groupoptions=',Select usergroup\n'+groupoptions
      if (document.getElementById('newvaluedropdown').value.trim()=='')
      {
        document.getElementById('newvaluedropdown').value=groupoptions
        value_array[currenteditfield]=groupoptions
      }  
    }      
  }
  var customfieldlengths = new Array()
  <?php for ($k=1;$k<=50;$k++) { ?>
  customfieldlengths[<?php echo $k; ?>]=<?php echo $customfieldlengths[$k]."\n"; ?>
  <?php } ?>
  var fieldname=sitelokfield_array[currenteditfield]
  // Remove options from inputtype dropdown
  var selectobj = document.getElementById('newinputtype');
  var k;
  for(k=selectobj.options.length-1;k>=0;k--)
    selectobj.remove(k);

  if ((fieldname=='username') || (fieldname=='password') || (fieldname=='vpassword') || (fieldname=='email') || (fieldname=='vemail') || (fieldname=='name'))
  {
    var optionname = ["text","password"];
    var optionvalue = ["text","password"];
  }
  if (fieldname.substring(0,6)=="custom")
  {
    var cusnum=fieldname.substring(6)
    if (customfieldlengths[cusnum]>255)
    {
      var optionname = ["text","password","dropdown menu","checkbox","radio","file upload","text area"];
      var optionvalue = ["text","password","dropdown","checkbox","radio","file","textarea"];
    }
    else
    {
      var optionname = ["text","password","dropdown menu","checkbox","radio","file upload"];
      var optionvalue = ["text","password","dropdown","checkbox","radio","file"];
    }    
  }    
  if (fieldname=='captcha')
  {
    var optionname = ["captcha"];
    var optionvalue = ["captcha"];
  }
  if (fieldname=='label')
  {
    var optionname = ["label"];
    var optionvalue = ["label"];
  }
  if (fieldname=='usergroup')
  {
    var optionname = ["usergroup"];
    var optionvalue = ["usergroup"];
  }
  // Add new options to inputtype drop down
  for (k=0;k<optionname.length;k++)
  {
    opt = document.createElement('option');
    opt.value = optionvalue[k];
    opt.innerHTML = optionname[k]; 
    selectobj.appendChild(opt);  
  }
  // If current input type not allowed 
  if (optionvalue.indexOf(inputtype_array[currenteditfield])==-1)
  {
    selectobj.value=optionvalue[0]
    inputtype_array[currenteditfield]=optionvalue[0]
    fieldpropertiesform()
  }
  else
    selectobj.value=inputtype_array[currenteditfield]
  $('#newinputtype').selectpicker('refresh');

} 
</script>
</head>
<?php include("adminthemeheader.php"); ?>
<!-- Content Wrapper. Contains page content -->

<body>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-list-alt"></i>&nbsp;<?php echo ADMINMENU_REGISTERFORMDESIGN; ?>
            <?php if ($act=="editform") { ?><small>form id: <?php echo $actid; ?></small><?php } ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><a href="registerforms.php"><?php echo ADMINMENU_REGISTERFORMS; ?></a></li>
            <li class="active"><?php echo ADMINMENU_REGISTERFORMDESIGN; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Your Page Content Here -->

          <div class="row">


         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
         <div class="box" id="previewbox">
         <div id="togglefixed" class="hidden-xs hidden-sm hidden-md"><span id="toggleicon" class="glyphicon glyphicon-pushpin" rel="tooltip" title="<?php echo ADMINET_TOGFIX; ?>" onclick="toggleFixed();"></span></div>
         <div class="box-body" id="formpreview">
            <!-- Simulate the login form in a div here -->
         </div><!-- /.box -->
         </div><!-- /.box-body -->
         </div><!-- /.col -->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

              <form id="registerform" role="form" class="form-horizontal" method="post">
              <input type="hidden" name="slcsrf" id="slcsrf" value="<?php echo $slcsrftoken; ?>">
              <input name="act" id="act" type="hidden" value="<?php echo $act; ?>">
              <input name="actid" id="actid" type="hidden" value="<?php if ($act=="editform") echo $actid; ?>">
              <input name="formpropertiesfield" id="formpropertiesfield" type="hidden" value="">
              <input name="formstylesfield" id="formstylesfield" type="hidden" value="">
              <input name="numfields" id="numfields" type="hidden" value="">
              <div class="box">
                <div class="box-body">



                  <div class="form-group">
                      <label class="col-xs-12" for="newformname"><?php echo ADMINET_FRMNM; ?></label>
                      <div class="col-xs-12" id="newformnamediv">
                          <input type="text" id="newformname" name="newformname" class="form-control" maxlength="100" value="<?php echo $newformname; ?>" onchange="updatepropertiesform()">
                      </div>
                  </div>

                  <div class="panel-group" id="accordion" role="tablist">

                
                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse1">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse1" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_FORMPROP; ?></a>
                        </h4>
                      </div>
                      <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-body">
                          <?php if ($paymentoption) { ?>
                          <div class="form-group">
                            <label class="col-xs-12" for="newregistertype"><?php echo ADMINET_REGTYPE; ?></label>
                             <div class="col-xs-12 col-sm-6 col-md-3 col-lg-5" id="newregistertypediv">
                                <select id="newregistertype" name="newregistertype" class="form-control selectpicker" onchange="updatepropertiesform()"">
                                <option value="normal" <?php if (($registertype=="normal") || ($registertype=="")) print "selected=\"selected\"";?>><?php echo ADMINET_REGTYPEFREE; ?></option>
                                <?php if ($paypal) { ?>
                                <option value="paypal" <?php if ($registertype=="paypal") print "selected=\"selected\"";?>><?php echo ADMINET_REGTYPEPPL; ?></option>
                                <?php } ?>
                                <?php if ($stripe) { ?>
                                <option value="stripe" <?php if ($registertype=="stripe") print "selected=\"selected\"";?>><?php echo ADMINET_REGTYPESTRP; ?></option>
                                <?php } ?>
                                </select>  
                            </div>
                           </div>
                           <?php } ?> 

                           <div id="registernormaldiv">


                           <div class="form-group">
                              <label class="col-xs-12" for="newusergroup"><?php echo ADMINET_GRPADD; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-5" id="newusergroupdiv">
                                 <select class="form-control usergrouplist" id="newusergroup" name="newusergroup" value="" onchange="updatepropertiesform()">
                                 <?php populateUsergroupSelect(); ?>
                                 </select> 
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newexpiry"><?php echo ADMINET_EXPDAYS; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-5" id="newexpirydiv">
                                  <input type="text" id="newexpiry" name="newexpiry" class="form-control" maxlength="255" value="<?php echo $newexpiry; ?>" onchange="updatepropertiesform()">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newredirect"><?php echo ADMINET_THANKPAGE; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-5" id="newredirectdiv">
                                  <input type="text" id="newredirect" name="newredirect" class="form-control" maxlength="255" value="<?php echo $newredirect; ?>" onchange="updatepropertiesform()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newenabled"><?php echo ADMINET_INITSTAT; ?></label>
                             <div class="col-xs-12 col-sm-6 col-md-3 col-lg-5" id="newenableddiv">
                                <select id="newenabled" name="newenabled" class="form-control selectpicker" onchange="updatepropertiesform()">
                                <option value="Yes" <?php if ($newenabled=="Yes") print "selected=\"selected\"";?>><?php echo ADMINET_ENABLED; ?></option>
                                <option value="No" <?php if ($newenabled=="No") print "selected=\"selected\"";?>><?php echo ADMINET_DISABLED; ?></option>
                                </select>  
                            </div>
                           </div> 

                          <div class="form-group">
                            <div class="col-sm-10">
                              <div class="checkbox">
                                <label>
                                <input type="checkbox" id="senduseremail" name="senduseremail" value="1">&nbsp;&nbsp;<?php echo ADMINET_USEREMAIL; ?>
                                </label>
                              </div>
                              <div class="selectemailtemplate" id="senduseremaildiv" onclick="openFileManager('newuseremail');">
                              <input type="hidden" id="newuseremail" name="newuseremail" value="<?php echo $newuseremail; ?>">
                              <span id="newuseremailname"><?php if ($newuseremail!="") echo $newuseremail; else echo ADMINMSG_NOTEMPLATE; ?></span>&nbsp;&nbsp;<span><i class="glyphicon glyphicon-folder-open actionicon"></i></span>
                              </div>
                            </div>
                          </div>


                          <div class="form-group">
                            <div class="col-sm-10">
                              <div class="checkbox">
                                <label>
                                <input type="checkbox" id="sendadminemail" name="sendadminemail" value="1">&nbsp;&nbsp;<?php echo ADMINET_ADMINEMAIL; ?>
                                </label>
                              </div>
                              <div class="selectemailtemplate" id="sendadminemaildiv" onclick="openFileManager('newadminemail');">
                              <input type="hidden" id="newadminemail" name="newadminemail" value="<?php echo $newadminemail; ?>">
                              <span id="newadminemailname"><?php if ($newadminemail!="") echo $newadminemail; else echo ADMINMSG_NOTEMPLATE; ?></span>&nbsp;&nbsp;<span><i class="glyphicon glyphicon-folder-open actionicon"></i></span>
                              </div>
                            </div>
                          </div>

                          </div> <!-- registernormaldiv -->

                          <div id="paypalproductdiv">
                          <?php if ($paypal) { ?>
                          <div class="form-group">
                            <label class="col-xs-12" for="newpaypalproduct"><?php echo "Select Paypal product"; ?></label>
                             <div class="col-xs-12 col-sm-6 col-md-3 col-lg-5" id="newpaypalproductdiv">
                                <select id="newpaypalproduct" name="newpaypalproduct" class="form-control selectpicker" onchange="updatepropertiesform()"">
                                <?php GetPaypalProducts($mysql_link,$productval); ?>
                                </select>  
                            </div>
                          </div> 
                           <?php } ?>
                           </div>


                          </div>
                        <div>    
                      </div>
                    </div>  
                    </div>

                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse2">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse2" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_FORMSTYLE; ?></a>
                        </h4>
                      </div>
                      <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">

                          <div class="form-group">
                            <label class="col-xs-12" for="newfonttype"><?php echo ADMINET_FONTTYP; ?></label>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newfonttypediv">
                                <select id="newfonttype" name="newfonttype" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="Arial, Helvetica, sans-serif" <?php if ($newfonttype=="Arial, Helvetica, sans-serif") print "selected=\"selected\"";?>>Arial</option>
                                <option value="Arial Narrow, sans-serif" <?php if ($newfonttype=="Arial Narrow, sans-serif") print "selected=\"selected\"";?>>Arial Narrow</option>
                                <option value="Century Gothic, sans-serif" <?php if ($newfonttype=="Century Gothic, sans-serif") print "selected=\"selected\"";?>>Century Gothic</option>
                                <option value="Copperplate Gothic Light, sans-serif" <?php if ($newfonttype=="Copperplate Gothic Light, sans-serif") print "selected=\"selected\"";?>>Copperplate Gothic Light</option>
                                <option value="Courier New, Courier, monospace" <?php if ($newfonttype=="Courier New, Courier, monospace") print "selected=\"selected\"";?>>Courier New</option>
                                <option value="Garamond, Serif" <?php if ($newfonttype=="Garamond, Serif") print "selected=\"selected\"";?>>Garamond</option>
                                <option value="Georgia, Serif" <?php if ($newfonttype=="Georgia, Serif") print "selected=\"selected\"";?>>Georgia</option>
                                <option value="Gill Sans MT, sans-serif" <?php if ($newfonttype=="Gill Sans MT, sans-serif") print "selected=\"selected\"";?>>Gill Sans MT</option>
                                <option value="Impact, Charcoal, sans-serif" <?php if ($newfonttype=="Impact, Charcoal, sans-serif") print "selected=\"selected\"";?>>Impact</option>
                                <option value="Lucida Console, Monaco, monospace" <?php if ($newfonttype=="Lucida Console, Monaco, monospace") print "selected=\"selected\"";?>>Lucida Console</option>
                                <option value="Lucida Sans Unicode, Lucida Grande, sans-serif" <?php if ($newfonttype=="Lucida Sans Unicode, Lucida Grande, sans-serif") print "selected=\"selected\"";?>>Lucida Sans Unicode</option>
                                <option value="Palatino Linotype, Book Antiqua, Palatino, serif" <?php if ($newfonttype=="Palatino Linotype, Book Antiqua, Palatino, serif") print "selected=\"selected\"";?>>Palatino Linotype</option>
                                <option value="Tahoma, Geneva, sans-serif" <?php if ($newfonttype=="Tahoma, Geneva, sans-serif") print "selected=\"selected\"";?>>Tahoma</option>
                                <option value="Times New Roman, Times, serif" <?php if ($newfonttype=="Times New Roman, Times, serif") print "selected=\"selected\"";?>>Times New Roman</option>
                                <option value="Verdana, Geneva, sans-serif" <?php if ($newfonttype=="Verdana, Geneva, sans-serif") print "selected=\"selected\"";?>>Verdana</option>
                                <option value="Trebuchet MS, Helvetica, sans-serif" <?php if ($newfonttype=="Trebuchet MS, Helvetica, sans-serif") print "selected=\"selected\"";?>>Trebuchet MS</option>
                                </select>  
                            </div>    
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newlabelstyle"><?php echo ADMINET_INPLBLSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newlabelstylediv" onchange="updatePreview()">
                                <select id="newlabelstyle" name="newlabelstyle" class="form-control selectpicker">
                                <option value="normal normal normal" <?php if ($newlabelstyle=="normal normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold" <?php if ($newlabelstyle=="normal normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal" <?php if ($newlabelstyle=="italic normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold" <?php if ($newlabelstyle=="italic normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newlabelcolordiv">
                                <input type="text" id="newlabelcolor" name="newlabelcolor" class="form-control colorbox" maxlength="7" value="<?php echo $newlabelcolor; ?>" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newlabelsizediv">
                                <div class="input-group">
                                  <input type="number" id="newlabelsize" name="newlabelsize" class="form-control" maxlength="2" min="1" max="99" value="<?php echo $newlabelsize; ?>" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newinputtextstyle"><?php echo ADMINET_INPTXTSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newinputtextstylediv" onchange="updatePreview()">
                                <select id="newinputtextstyle" name="newinputtextstyle" class="form-control selectpicker">
                                <option value="normal normal normal" <?php if ($newinputtextstyle=="normal normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold" <?php if ($newinputtextstyle=="normal normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal" <?php if ($newinputtextstyle=="italic normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold" <?php if ($newinputtextstyle=="italic normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newinputtextcolordiv">
                                <input type="text" id="newinputtextcolor" name="newinputtextcolor" class="form-control colorbox" maxlength="7" value="<?php echo $newinputtextcolor; ?>" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newinputtextsizediv">
                                <div class="input-group">
                                  <input type="number" id="newinputtextsize" name="newinputtextsize" class="form-control" maxlength="2" min="1" max="99" value="<?php echo $newinputtextsize; ?>" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newinputbackcolor"><?php echo ADMINET_INPBCKCOL; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newinputbackcolordiv">
                              <input type="text" id="newinputbackcolor" name="newinputbackcolor" class="form-control colorbox" maxlength="7" value="<?php echo $newinputbackcolor; ?>" onchange="updatePreview()">
                            </div> 
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbordersize"><?php echo ADMINET_INPTXTBRDSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbordersizediv" onchange="updatePreview()">
                                <select id="newbordersize" name="newbordersize" class="form-control selectpicker">
                                <option value="0"><?php echo ADMINET_NONE; ?></option>
                                <option value="1">1px</option>
                                <option value="2">2px</option>
                                <option value="3">3px</option>
                                <option value="4">4px</option>
                                <option value="5">5px</option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbordercolordiv">
                                <input type="text" id="newbordercolor" name="newbordercolor" class="form-control colorbox" maxlength="7" value="<?php echo $newbordercolor; ?>" onchange="updatePreview()">
                            </div> 
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newborderradiusdiv" onchange="updatePreview()">
                                <select id="newborderradius" name="newborderradius" class="form-control selectpicker">
                                <option value="0"><?php echo ADMINET_SQUARE; ?></option>
                                <option value="5"><?php echo ADMINET_ROUNDED; ?> 5px</option>
                                <option value="10"><?php echo ADMINET_ROUNDED; ?> 10px</option>
                                <option value="15"><?php echo ADMINET_ROUNDED; ?> 15px</option>
                                <option value="20"><?php echo ADMINET_ROUNDED; ?> 20px</option>
                                </select>  
                            </div> 
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newinputpaddingv"><?php echo ADMINET_INPPADDING; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newinputpaddingvdiv" onchange="updatePreview()">
                                <select id="newinputpaddingv" name="newinputpaddingv" class="form-control selectpicker">
                                <option value="0.3em"><?php echo "default"; ?></option>
                                <option value="0px">0px</option>
                                <option value="5px">5px</option>
                                <option value="10px">10px</option>
                                <option value="15px">15px</option>
                                <option value="20px">20px</option>
                                <option value="25px">25px</option>
                                <option value="30px">30px</option>
                                </select>  
                            </div> 
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newinputpaddinghdiv" onchange="updatePreview()">
                                <select id="newinputpaddingh" name="newinputpaddingh" class="form-control selectpicker">
                                <option value="0.3em"><?php echo "default"; ?></option>
                                <option value="0px">0px</option>
                                <option value="5px">5px</option>
                                <option value="10px">10px</option>
                                <option value="15px">15px</option>
                                <option value="20px">20px</option>
                                <option value="25px">25px</option>
                                <option value="30px">30px</option>
                                </select>  
                            </div> 
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newrqdfieldlabel"><?php echo ADMINET_RQDFLDLAB; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-5" id="newrqdfieldlabeldiv">
                                  <input type="text" id="newrqdfieldlabel" name="newrqdfieldlabel" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newrqdfieldstyle"><?php echo ADMINET_RQDFLDSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newrqdfieldstylediv" onchange="updatePreview()">
                                <select id="newrqdfieldstyle" name="newrqdfieldstyle" class="form-control selectpicker">
                                <option value="normal normal normal" <?php if ($newrqdfieldstyle=="normal normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold" <?php if ($newrqdfieldstyle=="normal normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal" <?php if ($newrqdfieldstyle=="italic normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold" <?php if ($newrqdfieldstyle=="italic normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newrqdfieldcolordiv">
                                <input type="text" id="newrqdfieldcolor" name="newrqdfieldcolor" class="form-control colorbox" maxlength="7"  onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newrqdfieldsizediv">
                                <div class="input-group">
                                  <input type="number" id="newrqdfieldsize" name="newrqdfieldsize" class="form-control" maxlength="2" min="1" max="99"  onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newmessagestyle"><?php echo ADMINET_MSGSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newmessagestylediv">
                                <select id="newmessagestyle" name="newmessagestyle" class="form-control selectpicker" onchange="updatePreview();">
                                <option value="normal normal normal" <?php if ($newmessagestyle=="normal normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold" <?php if ($newmessagestyle=="normal normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal" <?php if ($newmessagestyle=="italic normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold" <?php if ($newmessagestyle=="italic normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newmessagecolordiv">
                                <input type="text" id="newmessagecolor" name="newmessagecolor" class="form-control colorbox" maxlength="7"  onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newmessagesizediv">
                                <div class="input-group">
                                  <input type="number" id="newmessagesize" name="newmessagesize" class="form-control" maxlength="2" min="1" max="99" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newformerrormsg"><?php echo ADMINET_FRMERRMSG; ?></label>
                              <div class="col-xs-12" id="newformerrormsgdiv">
                                  <input type="text" id="newformerrormsg" name="newformerrormsg" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newformerrormsgstyle"><?php echo ADMINET_FRMERRMSGSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newformerrormsgstylediv">
                                <select id="newformerrormsgstyle" name="newformerrormsgstyle" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="normal normal normal" <?php if ($newformerrormsgstyle=="normal normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold" <?php if ($newformerrormsgstyle=="normal normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal" <?php if ($newformerrormsgstyle=="italic normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold" <?php if ($newformerrormsgstyle=="italic normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newformerrormsgcolordiv">
                                <input type="text" id="newformerrormsgcolor" name="newformerrormsgcolor" class="form-control colorbox" maxlength="7"  onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newformerrormsgsizediv">
                                <div class="input-group">
                                  <input type="number" id="newformerrormsgsize" name="newformerrormsgsize" class="form-control" maxlength="2" min="1" max="99"  onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newmaxformwidth"><?php echo ADMINET_FRMMAXWID; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newmaxformwidthdiv">
                                <div class="input-group">
                                  <input type="number" id="newmaxformwidth" name="newmaxformwidth" class="form-control" maxlength="6" min="0" max="999999" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                            </div> 
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newpreviewbackcolor"><?php echo ADMINET_PREVCOL; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newpreviewbackcolordiv">
                              <input type="text" id="newpreviewbackcolor" name="newpreviewbackcolor" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                            </div> 
                          </div>

                         </div>
                      </div>
                    </div>  


                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse3">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse3" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_BTNSTYLE; ?></a>
                        </h4>
                      </div>
                      <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">

                            <div class="form-group">
                              <label class="col-xs-12" for="newbtnlabel"><?php echo ADMINET_BTNLBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newbtnlabeldiv">
                                  <input type="text" id="newbtnlabel" name="newbtnlabel" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtnlabelfont"><?php echo ADMINET_BTNFONT; ?></label>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newbtnlabelfontdiv">
                                <select id="newbtnlabelfont" name="newbtnlabelfont" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="Arial, Helvetica, sans-serif" <?php if ($newbtnlabelfont=="Arial, Helvetica, sans-serif") print "selected=\"selected\"";?>>Arial</option>
                                <option value="Times New Roman, Times, serif" <?php if ($newbtnlabelfont=="Times New Roman, Times, serif") print "selected=\"selected\"";?>>Times New Roman</option>
                                <option value="Verdana, Geneva, sans-serif" <?php if ($newbtnlabelfont=="Verdana, Geneva, sans-serif") print "selected=\"selected\"";?>>Verdana</option>
                                <option value="Impact, Charcoal, sans-serif" <?php if ($newbtnlabelfont=="Impact, Charcoal, sans-serif") print "selected=\"selected\"";?>>Impact</option>
                                <option value="Palatino Linotype, Book Antiqua, Palatino, serif" <?php if ($newbtnlabelfont=="Palatino Linotype, Book Antiqua, Palatino, serif") print "selected=\"selected\"";?>>Palatino Linotype</option>
                                <option value="Tahoma, Geneva, sans-serif" <?php if ($newbtnlabelfont=="Tahoma, Geneva, sans-serif") print "selected=\"selected\"";?>>Tahoma</option>
                                <option value="Century Gothic, sans-serif" <?php if ($newbtnlabelfont=="Century Gothic, sans-serif") print "selected=\"selected\"";?>>Century Gothic</option>
                                <option value="Lucida Sans Unicode, Lucida Grande, sans-serif" <?php if ($newbtnlabelfont=="Lucida Sans Unicode, Lucida Grande, sans-serif") print "selected=\"selected\"";?>>Lucida Sans Unicode</option>
                                <option value="Arial Narrow, sans-serif" <?php if ($newbtnlabelfont=="Arial Narrow, sans-serif") print "selected=\"selected\"";?>>Arial Narrow</option>
                                <option value="Copperplate Gothic Light, sans-serif" <?php if ($newbtnlabelfont=="Copperplate Gothic Light, sans-serif") print "selected=\"selected\"";?>>Copperplate Gothic Light</option>
                                <option value="Lucida Console, Monaco, monospace" <?php if ($newbtnlabelfont=="Lucida Console, Monaco, monospace") print "selected=\"selected\"";?>>Lucida Console</option>
                                <option value="Gill Sans MT, sans-serif" <?php if ($newbtnlabelfont=="Gill Sans MT, sans-serif") print "selected=\"selected\"";?>>Gill Sans MT</option>
                                <option value="Trebuchet MS, Helvetica, sans-serif" <?php if ($newbtnlabelfont=="Trebuchet MS, Helvetica, sans-serif") print "selected=\"selected\"";?>>Trebuchet MS</option>
                                <option value="Courier New, Courier, monospace" <?php if ($newbtnlabelfont=="Courier New, Courier, monospace") print "selected=\"selected\"";?>>Courier New</option>
                                <option value="Georgia, Serif" <?php if ($newbtnlabelfont=="Georgia, Serif") print "selected=\"selected\"";?>>Georgia</option>
                                <option value="Garamond, Serif" <?php if ($newbtnlabelfont=="Garamond, Serif") print "selected=\"selected\"";?>>Garamond</option>
                                </select>  
                            </div>    
                          </div>                      
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtnlabelstyle"><?php echo ADMINET_BTNLBLSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtnlabelstylediv">
                                <select id="newbtnlabelstyle" name="newbtnlabelstyle" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="normal normal normal" <?php if ($newbtnlabelstyle=="normal normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold" <?php if ($newbtnlabelstyle=="normal normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal" <?php if ($newbtnlabelstyle=="italic normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold" <?php if ($newbtnlabelstyle=="italic normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbtnlabelcolordiv">
                                <input type="text" id="newbtnlabelcolor" name="newbtnlabelcolor" class="form-control colorbox" maxlength="7"  onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newbtnlabelsizediv">
                                <div class="input-group">
                                  <input type="number" id="newbtnlabelsize" name="newbtnlabelsize" class="form-control" maxlength="2" min="1" max="99" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtncolortype"><?php echo ADMINET_BTNCOL; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtncolortypediv">
                                <select id="newbtncolortype" name="newbtncolortype" class="form-control selectpicker" onchange="filltype('newbtncolortype','newbtncolortodiv'); updatePreview()">
                                <option value="solid" <?php if ($newbtncolortype=="solid") print "selected=\"selected\"";?>><?php echo ADMINET_SOLID; ?></option>
                                <option value="gradient" <?php if ($newbtncolortype=="gradient") print "selected=\"selected\"";?>><?php echo ADMINET_GRADIENT; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbtncolorfromdiv">
                                <input type="text" id="newbtncolorfrom" name="newbtncolorfrom" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                            </div> 
                            <div class="col-xs-6 col-sm-5 col-md-4 col-lg-5" id="newbtncolortodiv">
                              <div class="input-group">
                                <input type="text" id="newbtncolorto" name="newbtncolorto" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button" id="loginbuttoncolorreverse" onclick="gradientreverse('newbtncolorfrom','newbtncolorto')"><span class="glyphicon glyphicon-transfer actionicon" title="reverse"></span></button>
                                </span>
                              </div>                                                        
                            </div>                           
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtnborderstyle"><?php echo ADMINET_BTNBRDSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtnborderstylediv">
                                <select id="newbtnborderstyle" name="newbtnborderstyle" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="solid" <?php if ($newbtnborderstyle=="solid") print "selected=\"selected\"";?>><?php echo ADMINET_SOLID; ?></option>
                                <option value="dotted" <?php if ($newbtnborderstyle=="dotted") print "selected=\"selected\"";?>><?php echo ADMINET_DOTTED; ?></option>
                                <option value="dashed" <?php if ($newbtnborderstyle=="dashed") print "selected=\"selected\"";?>><?php echo ADMINET_DASHED; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbtnbordercolordiv">
                                <input type="text" id="newbtnbordercolor" name="newbtnbordercolor" class="form-control colorbox" maxlength="7"  onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newbtnbordersizediv">
                                <div class="input-group">
                                  <input type="number" id="newbtnbordersize" name="newbtnbordersize" class="form-control" maxlength="2" min="0" max="99" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtnpaddingv"><?php echo ADMINET_BTNPADDING; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtnpaddingvdiv" onchange="updatePreview()">
                                <select id="newbtnpaddingv" name="newbtnpaddingv" class="form-control selectpicker">
                                <option value="6"><?php echo "default"; ?></option>
                                <option value="0">0px</option>
                                <option value="5">5px</option>
                                <option value="10">10px</option>
                                <option value="15">15px</option>
                                <option value="20">20px</option>
                                <option value="25">25px</option>
                                <option value="30">30px</option>
                                <option value="40">40px</option>
                                <option value="50">50px</option>
                                <option value="60">60px</option>
                                <option value="70">70px</option>
                                <option value="80">80px</option>
                                <option value="90">90px</option>
                                <option value="100">100px</option>
                                </select>  
                            </div> 
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtnpaddinghdiv" onchange="updatePreview()">
                                <select id="newbtnpaddingh" name="newbtnpaddingh" class="form-control selectpicker">
                                <option value="24"><?php echo "default"; ?></option>
                                <option value="0">0px</option>
                                <option value="5">5px</option>
                                <option value="10">10px</option>
                                <option value="15">15px</option>
                                <option value="20">20px</option>
                                <option value="25">25px</option>
                                <option value="30">30px</option>
                                <option value="40">40px</option>
                                <option value="50">50px</option>
                                <option value="60">60px</option>
                                <option value="70">70px</option>
                                <option value="80">80px</option>
                                <option value="90">90px</option>
                                <option value="100">100px</option>
                                </select>  
                            </div> 
                          </div>                          
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtnradius"><?php echo ADMINET_BTNSHAPE; ?></label>
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newbtnradiusdiv">
                                <select id="newbtnradius" name="newbtnradius" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="0" <?php if ($newbtnradius=="0") print "selected=\"selected\"";?>><?php echo ADMINET_SQUARE; ?></option>
                                <option value="5" <?php if ($newbtnradius=="5") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 5px</option>
                                <option value="10" <?php if ($newbtnradius=="10") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 10px</option>
                                <option value="15" <?php if ($newbtnradius=="15") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 15px</option>
                                <option value="20" <?php if ($newbtnradius=="20") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 20px</option>
                                </select>  
                            </div>    
                          </div>                      

                         </div>
                      </div>
                    </div>  



                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse4">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse4" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_FLDPROP; ?></a>
                        </h4>
                      </div>
                      <div id="collapse4" class="panel-collapse collapse">
                        <div class="panel-body">

<div id="nopropertiesdiv">
<p id="nopropertiesp">
<?php
$str=ADMINET_FLDNOTE;
$str=str_replace("***1***", "<span class=\"glyphicon glyphicon-edit actionicon\"></span>", $str);
$str=str_replace("***2***", "<i class=\"fa fa-plus actionicon\" rel=\"tooltip\" title=\"".ADMINET_ADDFLD."\" onclick=\"addfield();\"></i>", $str);
echo $str;
?>
</p>
</div>

<div id="propertiesdiv">

                        <div id="sitelokfielddiv">                        
                          <div class="form-group">
                            <label class="col-xs-12" for="newsitelokfield"><?php echo ADMINET_SLFIELD; ?></label>
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newsitelokfielddiv">
                                <select id="newsitelokfield" name="newsitelokfield" class="form-control selectpicker" onchange="sitelokfield_array[currenteditfield]=this.value; copyinputvalidation(); fieldpropertiesform(); updatePreview()">
                                <option value="username"><?php echo ADMINFIELD_USERNAME; ?></option>
                                <option value="password"><?php echo ADMINFIELD_PASSWORD; ?></option>
                                <option value="vpassword"><?php echo ADMINFIELD_VERPASS; ?></option>
                                <option value="name"><?php echo ADMINFIELD_NAME; ?></option>
                                <option value="email"><?php echo ADMINFIELD_EMAIL; ?></option>
                                <option value="vemail"><?php echo ADMINFIELD_VEREMAIL; ?></option>
                                <option value="captcha"><?php echo ADMINFIELD_CAPTCHA; ?></option>
                                <option value="usergroup"><?php echo ADMINFIELD_USERGROUP; ?></option>
                                <option value="label"><?php echo ADMINET_FRMLAB; ?></option>
                                <?php for ($k=1;$k<51;$k++)
                                {
                                  $custitle="CustomTitle".$k;  
                                ?>
                                <option value="custom<?php echo $k; ?>"><?php if ($$custitle!="") echo $$custitle." (custom ".$k.")"; else echo "custom ".$k; ?></option>
                                <?php } ?>
                                </select>  
                            </div>    
                          </div>                      
                        </div> <!-- sitelokfielddiv -->

                        <div class="form-group">
                          <label class="col-xs-12" for="newinputtype"><?php echo ADMINET_INPTYP; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newinputtypediv">
                              <select id="newinputtype" name="newinputtype" class="form-control selectpicker" onchange="inputtype_array[currenteditfield]=this.value; fieldpropertiesform(this.value); updatePreview()">
                              <option value="text"><?php echo ADMINET_TEXT; ?></option>
                              <option value="password"><?php echo ADMINET_PASS; ?></option>
                              <option value="dropdown"><?php echo ADMINET_DRPDWN; ?></option>
                              <option value="checkbox"><?php echo ADMINET_CHECKBOX; ?></option>
                              <option value="radio"><?php echo ADMINET_RADIO; ?></option>
                              <option value="label"><?php echo ADMINET_LABEL; ?></option>
                              <option value="captcha"><?php echo ADMINET_CAPTCHA; ?></option>
                              <option value="file"><?php echo ADMINET_FILEUPL; ?></option>
                              <option value="textarea"><?php echo ADMINET_TXTAREA; ?></option>
                              </select>  
                          </div>    
                        </div>                      

                        <div class="form-group">
                          <label class="col-xs-12" for="newlabeltext"><?php echo ADMINET_LBLTXT; ?></label>
                          <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newlabeltextdiv">
                              <input type="text" id="newlabeltext" name="newlabeltext" class="form-control" maxlength="255" onchange="labeltext_array[currenteditfield]=this.value; updatePreview()">
                          </div>
                        </div>

                        <div id="valuediv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newvalue"><?php echo ADMINET_VALUE; ?></label>
                          <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newvaluediv">
                              <input type="text" id="newvalue" name="newvalue" class="form-control" maxlength="255" onchange="value_array[currenteditfield]=this.value; updatePreview()">
                          </div>
                        </div>
                        </div> <!-- valuediv -->

                        <div id="valuedropdowndiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newvaluedropdown"><?php echo ADMINET_OPTIONS; ?></label>
                          <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newvaluedropdowndiv">
                              <textarea id="newvaluedropdown" name="newvaluedropdown" wrap="off" class="form-control" maxlength="255" onchange="value_array[currenteditfield]=this.value; updatePreview()"></textarea>
                          </div>
                        </div>
                        </div> <!-- valuedropdowndiv -->

                        <div id="checkeddiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newchecked"><?php echo ADMINET_INICHKD; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newcheckeddiv">
                              <select id="newchecked" name="newchecked" class="form-control selectpicker" onchange="checked_array[currenteditfield]=this.value; updatePreview()">
                              <option value="0"><?php echo ADMINET_NOTCHKD; ?></option>
                              <option value="1"><?php echo ADMINET_CHKD; ?></option>
                              </select>  
                          </div>    
                        </div>                      
                        </div> <!-- checkeddiv -->

                        <div id="placeholderdiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newplacetext"><?php echo ADMINET_PLACETXT; ?></label>
                          <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newplacetextdiv">
                              <input type="text" id="newplacetext" name="newplacetext" class="form-control" maxlength="255" onchange="placetext_array[currenteditfield]=this.value; updatePreview()">
                          </div>
                        </div>
                        </div> <!-- placeholderdiv -->

                        <div id="validationdiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newvalidation"><?php echo ADMINET_REQD; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newvalidationdiv">
                              <select id="newvalidation" name="newvalidation" class="form-control selectpicker" onchange="validation_array[currenteditfield]=this.value; if (this.value!='notrequired') showrequired_array[currenteditfield]='1'; else showrequired_array[currenteditfield]='0'; document.getElementById('newshowrequired').value=showrequired_array[currenteditfield]; setinputvalidation(); fieldpropertiesform();  updatePreview()">
                              <option value="notrequired"><?php echo ADMINET_NOTREQD; ?></option>
                              <option value="required"><?php echo ADMINET_REQD; ?></option>
                              </select>  
                          </div>    
                        </div>                      
                        </div> <!-- validationdiv -->

                        <div id="validationdropdowndiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newvalidationdropdown"><?php echo ADMINET_REQD; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newvalidationdropdowndiv">
                              <select id="newvalidationdropdown" name="newvalidationdropdown" class="form-control selectpicker" onchange="validation_array[currenteditfield]=this.value; if (this.value!='notrequired') showrequired_array[currenteditfield]='1'; else showrequired_array[currenteditfield]='0'; document.getElementById('newshowrequired').value=showrequired_array[currenteditfield]; setinputvalidation(); fieldpropertiesform(); updatePreview()">
                              <option value="notrequired"><?php echo ADMINET_NOTREQD; ?></option>
                              <option value="required"><?php echo ADMINET_REQD." ".ADMINET_NOTFIRST; ?></option>
                              </select>  
                          </div>    
                        </div>                      
                        </div> <!-- validationdropdowndiv -->

                        <div id="showrequireddiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newshowrequired"><?php echo ADMINET_SHWREQ; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newshowrequireddiv">
                              <select id="newshowrequired" name="newshowrequired" class="form-control selectpicker" onchange="showrequired_array[currenteditfield]=this.value; updatePreview()">
                              <option value="1"><?php echo ADMINET_SHWREQLAB; ?></option>
                              <option value="0"><?php echo ADMINET_NOREQLAB; ?></option>
                              </select>  
                          </div>    
                        </div>                      
                        </div> <!-- showrequireddiv -->

                        <div id="errormsgdiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newerrormsg"><?php echo ADMINET_ERRMSG; ?></label>
                          <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newerrormsgdiv">
                              <input type="text" id="newerrormsg" name="newerrormsg" class="form-control" maxlength="255" onchange="errormsg_array[currenteditfield]=this.value; setinputvalidation(); updatePreview()">
                          </div>
                        </div>
                        </div> <!-- errormsgdiv -->

                        <div id="fieldwidthdiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newfieldwidth"><?php echo ADMINET_FLDWID; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newfieldwidthdiv">
                              <select id="newfieldwidth" name="newfieldwidth" class="form-control selectpicker" onchange="fieldwidth_array[currenteditfield]=this.value; updatePreview()">
                              <option value="100">100%</option>
                              <option value="75">75%</option>
                              <option value="66">66%</option>
                              <option value="50">50%</option>
                              <option value="33">33%</option>
                              <option value="25">25%</option>
                              <option value="20">20%</option>
                              <option value="10">10%</option>
                              </select>  
                          </div>    
                        </div>                      
                        </div> <!-- fieldwidthdiv -->

                        <div class="form-group">
                          <label class="col-xs-12" for="newbottommargin"><?php echo ADMINET_BOTMGN; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newfieldwidthdiv">
                              <select id="newbottommargin" name="newbottommargin" class="form-control selectpicker" onchange="bottommargin_array[currenteditfield]=this.value; updatePreview()">
                              <option value="50">50px</option>
                              <option value="40">40px</option>
                              <option value="30">30px</option>
                              <option value="25">25px</option>
                              <option value="20">20px</option>
                              <option value="15">15px</option>
                              <option value="10">10px</option>
                              <option value="5">5px</option>
                              <option value="1">1px</option>
                              </select>  
                          </div>    
                        </div>                      

                        <div class="form-group">
                          <label class="col-xs-12" for="newposition"><?php echo ADMINET_FRMPOS; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newpositiondiv">
                              <select id="newposition" name="newposition" class="form-control selectpicker" onchange="updateposition(this.value); updatePreview()">
                              </select>  
                          </div>    
                        </div>                      

</div> <!-- propertiesdiv -->



                        </div>
                      </div>
                    </div>  



                  </div> <!-- panel-group -->

                </div> <!-- box body -->
              </div> <!-- box --> 

              <div class="alert alert-warning" role="alert" id="formissues"></div>


                    <div class="form-group">
                      <div class="col-xs-12">
                        <div class="btn-toolbar">
                            <button type="submit" id="submit" class="btn btn-primary pull-left"><?php echo ADMINBUTTON_SAVECHANGES; ?></button>
                            <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='registerforms.php';"><?php echo ADMINBUTTON_RETURNTOFORMS ?></button>
                            <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                        </div> 
                      </div>    
                    </div>

              <div id="resultregisterform"></div>


              </form>




           </div> <!-- col -->


          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include("adminthemefooter.php"); ?>
    <script src="jquery-minicolors-master/jquery.minicolors.js" type="text/javascript"></script>
    <script type="text/javascript" src="fancybox/source/jquery.fancybox.js?v=2.1.4"></script>
    <script src="registerformedit.js" type="text/javascript"></script>
    <script type="text/javascript">
      startvalues();
    </script>
</body>
</html>
