updateUsergroupList();

function updateUsergroupList()
{
  $('#grouplist').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
  var formData = {
      'slcsrf'    : slcsrf
  };
  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'admingetusergroups.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      .done(function(data) {
        sessionExpiredReported(data,true);
        if (data.success)
        {
          if (data.groupcount>0)
          {
            // Store longest usergroup name for table colum size
            var len=0;
            var htmlstr="";
            htmlstr+='<table>';
            for (k=0;k<data.groupcount;k++)
            {
              if (data.names[k].length>len)
                len=data.names[k].length;
              htmlstr+='<tr>';
              htmlstr+='<td class="actions">';
              if ((data.names[k]!='ADMIN') && (data.names[k]!='ALL'))
                htmlstr+='<input class="delcb" type="checkbox" name="sel[]" value="'+data.groupids[k]+','+data.names[k]+'">';
              else
                htmlstr+='<input class="delcb" type="checkbox" name="sel[]" disabled value="">';
              htmlstr+='&nbsp;&nbsp;<a href="groupmanage2.php?act=editgroup&actid='+data.groupids[k]+'"><span class="glyphicon glyphicon-edit actionicon" rel="tooltip" title="'+ADMINBUTTON_EDIT+'"></span></a>';
              htmlstr+='&nbsp;&nbsp;<a href="groupmanage2.php?act=duplgroup&actid='+data.groupids[k]+'"><span class="glyphicon glyphicon-duplicate actionicon" rel="tooltip" title="'+ADMINBUTTON_DUPLICATE+'"></span></a>';
              if ((data.names[k]!='ADMIN') && (data.names[k]!='ALL'))
                htmlstr+='&nbsp;&nbsp;<span class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINBUTTON_DELETE+'" onclick="deleteUsergroup(\''+data.groupids[k]+'\',\''+data.names[k]+'\');"></span>';
              else
                htmlstr+='&nbsp;&nbsp;<span class="glyphicon glyphicon-remove disabledicon" rel="tooltip" title="'+ADMINBUTTON_DELETE+'" onclick="deleteUsergroup(\''+data.groupids[k]+'\',\''+data.names[k]+'\');"></span>';                
              htmlstr+='</td>\n';
              htmlstr+='<td class="groupname">'+data.names[k]+'</td>';
              htmlstr+='<td class="groupdescription">'+data.descriptions[k]+'</td>';
              htmlstr+='</tr>';
            }
            htmlstr+='<tr><td colspan="3"><p>&nbsp;</p></td></tr>';
            htmlstr+='<tr>';
            htmlstr+='<td class="actions" colspan="3">';
            htmlstr+='<input class="delcb" type="checkbox" id="selectall" value="on" onclick="checkAll();">';
            htmlstr+='&nbsp;&nbsp;<span class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINMENU_DELETESELECTED+'" onclick="deleteSelectedUsergroups();"></span>';
            htmlstr+='</td>\n';              
            htmlstr+='</tr>';              
            htmlstr+='</table>';            
          }
          else
          {
            htmlstr='<p class="groupmessage">'+ADMINMU_NOGROUPS+'</p>';
          }
          $('#grouplist').html(htmlstr);
          // Adjust column size for largest usergroup name
          if(data.groupcount>0)
          {
            len=Math.ceil(len*0.6);
            $("#grouplist td.groupname").css("width",len+"em");
          }
        }
        else
        {
          $('#grouplist').html('<p class="grouperror">'+data.message+'</p>');
        }
      })

    .fail(function(data) {
      $('#grouplist').html('<p class="grouperror">'+ADMINMU_GROUPERROR+'</p>');
     });
}

function deleteUsergroup(id,name)
{
  if ((name=="ADMIN") || (name=="ALL"))
  {
    bootbox.alert(ADMINMU_NODELETE);
    return;
  }  
  bootbox.confirm(ADMINMU_DELETEGROUP, function(result) {
    if (result)
    {
      var datatosend = { groupid : id, groupname: name, slcsrf : slcsrf};
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'admindeleteusergroup.php', // the url where we want to POST
            data        : datatosend,
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        .done(function(data) {
          sessionExpiredReported(data,true);
          if (data.success)
            updateUsergroupList();
          else
          {
            bootbox.alert(data.message);
            updateUsergroupList();
          }
        })
        .fail(function() {
          bootbox.alert(ADMINMU_CANTDELETE);
          updateUsergroupList();
        })
    }  
  });
}

function deleteSelectedUsergroups()
{
  // Get id's of selected usergroups
  formname="form";
  var checkboxes = new Array();
  var groupids = new Array();
  var groupnames =  new Array();
  checkboxes = document[formname].getElementsByTagName('input');
  var ct=0;
  for (var i=0; i<checkboxes.length; i++)  {
    if (checkboxes[i].type == 'checkbox')   {
      if (checkboxes[i].checked)
      {
        var info=checkboxes[i].value.split(",");
        groupids[ct]=info[0];
        groupnames[ct]=info[1];
        ct++;
      }
    }
  }
  if (ct==0)
    return;    
  bootbox.confirm(ADMINMU_DELETESELGRP, function(result) {
    if (result)
    {
      var datatosend = { slcsrf : slcsrf, groupids : groupids.join(), groupnames : groupnames.join()};
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'admindeleteselectedusergroups.php', // the url where we want to POST
            data        : datatosend,
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        .done(function(data) {
          sessionExpiredReported(data,true);
          if (data.success)
            updateUsergroupList();
          else
          {
            bootbox.alert(data.message);
            updateUsergroupList();
          }
        })
        .fail(function() {
          bootbox.alert(ADMINMU_NODELETE);
          updateUsergroupList();
        })
    }  
  });
}

function checkAll()
{
  formname="form";
  checktoggle=document.getElementById('selectall').checked;
  var checkboxes = new Array(); 
  checkboxes = document[formname].getElementsByTagName('input');
  for (var i=0; i<checkboxes.length; i++)  {
    if ((checkboxes[i].type == 'checkbox') && (!checkboxes[i].disabled))   {
      checkboxes[i].checked = checktoggle;
    }
  }
}


