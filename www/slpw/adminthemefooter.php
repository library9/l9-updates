      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          <?php echo ADMINGENERAL_FOOTERRIGHT; ?>
        </div>
        <!-- Default to the left -->
        <?php echo ADMINGENERAL_FOOTERLEFT; ?>
      </footer>
    </div><!-- ./wrapper -->
    <!-- REQUIRED JS SCRIPTS -->
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo $SitelokLocationURLPath; ?>jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo $SitelokLocationURLPath; ?>bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $SitelokLocationURLPath; ?>adminlte/js/app.min.js"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
    <script src="<?php echo $SitelokLocationURLPath; ?>adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo $SitelokLocationURLPath; ?>bootbox/bootbox.min.js"></script>

    <script src="<?php echo $SitelokLocationURLPath; ?>editable-select/jquery.editable-select.min.js"></script>
    <script src="<?php echo $SitelokLocationURLPath; ?>bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="<?php echo $SitelokLocationURLPath; ?>adminlte/plugins/iCheck/icheck.min.js"></script>
    <script src="<?php echo $SitelokLocationURLPath; ?>bootstrap-datetimepicker/moment.min.js"></script>
    <?php if (ADMINDTTMPICK_LANG!="") { ?>
    <script src="<?php echo $SitelokLocationURLPath; ?>bootstrap-datetimepicker/locale/<?php echo ADMINDTTMPICK_LANG; ?>.js"></script>
    <?php } ?>
    <script src="<?php echo $SitelokLocationURLPath; ?>bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>

    <script src="<?php echo $SitelokLocationURLPath; ?>admincommonfunctions.js"></script>

    <?php
    if(isset($slplugin_adminfooter))
    {
      asort($slplugin_adminfooter);
      foreach ($slplugin_adminfooter as $key => $value)
      {
        if ($value>0)
          include($SitelokLocation.$slpluginfolder[$key]."/pluginadminfooter.php");
      } 
    }
    ?>

    <script type="text/javascript">
        $(document).ready(function(){
    });
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });

    // Set language for bootbox. We use en but replace words
    bootbox.addLocale('en', {
    OK : ADMINBUTTON_OK,
    CANCEL : ADMINBUTTON_CANCEL,
    CONFIRM : ADMINBUTTON_CONFIRM
    });
    bootbox.setDefaults({ locale: 'en' });

    // Show selectactionmenu items if any users selected
    var numsel=<?php echo $numsel."\n";?>
    if (numsel>0)
      $('.selectactionmenu').show();
    // Make sure server doesn't cache any ajax calls
    $.ajaxSetup({ cache: false });    
    </script>

   
