<?php
if (!defined('MSG_UPLOADERROR'))
  define("MSG_UPLOADERROR","File upload failed");
if (!defined('MSG_UPLOADTYPE'))
  define("MSG_UPLOADTYPE","Files of this type cannot be uploaded");
reset($_GET);
reset($_POST);
if (!empty($_GET)) while(list($name, $value) = each($_GET)) $$name = $value;
if (!empty($_POST)) while(list($name, $value) = each($_POST)) $$name = $value;
if ((isset($_POST['contact_id'])) &&  ($_POST['contact_id']!=""))
{
  $hashedid=$_POST['contact_id'];
  $id=strtok($hashedid,"|");
  $hash=strtok("|");
  if ($hash!=md5($id."contactid".$SiteKey))
  {
    echo "<strong>Invalid form id</strong>";
    exit;
  }
  $contactmsg="contactmsg_".$id;
  $$contactmsg="";
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    echo "<strong>Can't connect to database</strong>";
    exit;
  }
  // If form is not contact type then return
  $mysql_result=mysqli_query($mysql_link,"SELECT type FROM sl_forms WHERE id=".sl_quote_smart($id));
  if ($mysql_result===false)
  {
    echo "<strong>Can't connect to database</strong>";
    exit;
  }  
  $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
  if ($row===false)
  {
    echo "<strong>Can't connect to database</strong>";
    exit;
  }
  if ($row['type']!="contact")
  {
    echo "<strong>Contact form not found</strong>";
    exit;
  }  
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_contactforms WHERE id=".sl_quote_smart($id)." AND position=0");
  if ($mysql_result===false)
  {
    echo "<strong>Contact form not found</strong>";
    exit;
  }  
  $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
  $sendemail=$row['sendemail'];
  $redirect=$row['redirect'];
  $useremailvisitor=$row['useremailvisitor'];
  $adminemailvisitor=$row['adminemailvisitor'];
  $useremailmember=$row['useremailmember'];
  $adminemailmember=$row['adminemailmember'];
  $fromnameuser=$row['fromnameuser'];
  $replytouser=$row['replytouser'];
  $attachmenttypes=$row['attachmenttypes'];
  $attachmentsize=$row['attachmentsize'];
  $sendasuser=$row['sendasuser'];
  // If user using example form without setting email use Sitelok defaults
  if ($sendemail=="")
    $sendemail=$SiteEmail;
  if ($fromnameuser=="")
    $fromnameuser=$SiteName;
  if ($replytouser=="")
    $replytouser=$SiteEmail;
  // Get field details
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_contactforms WHERE id=".sl_quote_smart($id)." AND position>0 ORDER BY position ASC");
  if ($mysql_result===false)
  {
    echo "<strong>Contact form fields not found</strong>";
    exit;
  }
  $captcharequired=-1;
  $emailfield=-1;
  $namefield=-1;
  $uploadfields=array();    
  $numfields=0;
  while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
  { 
    $sitelokfield_array[$numfields]=$row['sitelokfield'];
    $inputtype_array[$numfields]=$row['inputtype'];
    $labeltext_array[$numfields]=$row['labeltext'];
    $placetext_array[$numfields]=$row['placetext'];
    $value_array[$numfields]=$row['value'];
    $checked_array[$numfields]=$row['checked'];
    $validation_array[$numfields]=$row['validation'];
    $showrequired_array[$numfields]=$row['showrequired'];
    $errormsg_array[$numfields]=$row['errormsg'];
    $useas_array[$numfields]=$row['useas'];
    $showfieldfor_array[$numfields]=$row['showfieldfor'];
    $fieldwidth_array[$numfields]=$row['fieldwidth'];
    $bottommargin_array[$numfields]=$row['bottommargin'];
    // flag if captcha field
    if (($inputtype_array[$numfields]=="captcha") && ($validation_array[$numfields]!="notrequired") && ($showfieldfor_array[$numfields]==0))
      $captcharequired=$numfields;
    if (($inputtype_array[$numfields]=="captcha") && ($validation_array[$numfields]!="notrequired") && ($showfieldfor_array[$numfields]==1) && ($slpublicaccess))
      $captcharequired=$numfields;
    if (($inputtype_array[$numfields]=="captcha") && ($validation_array[$numfields]!="notrequired") && ($showfieldfor_array[$numfields]==2) && (!$slpublicaccess))
      $captcharequired=$numfields;
    // flag email field  
    if (($useas_array[$numfields]==1) && ($emailfield==-1))
      $emailfield=$numfields;
    // flag name field  
    if (($useas_array[$numfields]==2) && ($namefield==-1))
      $namefield=$numfields;
    // flag upload fields
    if (($inputtype_array[$numfields]=="file") && ($_FILES['contact_'.$id."_".$k]['name'])!="")
      $uploadfields[]=$numfields;
    $numfields++;
  }

  // Get entered values from form fields
  $inputvalues=array();
  for ($k=0;$k<$numfields;$k++)
  {
    if ($inputtype_array[$k]=="file")
    {
      $inputvalues[$k]=$_FILES['contact_'.$id."_".$k]['name'];
    }
    else
    {
      if (isset($_POST['contact_'.$id."_".$k]))
        $inputvalues[$k]=sl_uncleandata($_POST['contact_'.$id."_".$k]);
      else
        $inputvalues[$k]="";
    }
  }
  // Verify CAPTCHA if required
  if ($captcharequired>-1)
  {
    if ($SessionName!="")
      session_name($SessionName);    
    session_start();
    $turingmatch=false;
    $turingtested=false;
    // First see if plugin will verify code 
    // Call event handlers
    $paramdata['username']=$slusername;
    // Call plugin event
    for ($p=0;$p<$slnumplugins;$p++)
    {
      if (function_exists($slplugin_event_onCaptchaVerify[$p]))
      {
        $turingtested=true;
        if (call_user_func($slplugin_event_onCaptchaVerify[$p],$slpluginid[$p],$paramdata))
          $turingmatch=true;
        break;
      }
    }
    // Call user event handler
    if ((!$turingtested) && (function_exists("sl_onCaptchaVerify")))
    {
      $turingtested=true;
      if (sl_onCaptchaVerify($paramdata))
        $turingmatch=true; 
    }
    if (($turingtested) && (!$turingmatch))
    	$$contactmsg=$errormsg_array[$captcharequired];
    if (!$turingtested)
    {
      if ((strtolower($_SESSION['ses_slturingcode'])==strtolower(trim($_POST['contact_'.$id."_".$captcharequired]))) && ($_SESSION['ses_slturingcode']!=""))
      {
        $turingmatch=true;
        if (!$slajaxform)
          $_SESSION['ses_slturingcode']="";
      }
      else if ((strtolower($_SESSION['ses_slpreviousturingcode'])==strtolower(trim($_POST['contact_'.$id."_".$captcharequired]))) && ($_SESSION['ses_slpreviousturingcode']!=""))
      {
        $turingmatch=true;
        if (!$slajaxform)        
          $_SESSION['ses_slpreviousturingcode']="";
      }
      if (!$turingmatch)
      	$$contactmsg=$errormsg_array[$captcharequired];
    }	
  }
  $attachments=false;
  $attachmentpaths=array();
  $attachmentfilenames=array();
  $extsarray=explode(" ",$attachmenttypes);
  for ($k=0;$k<count($extsarray);$k++)
    $extsarray[$k]=trim($extsarray[$k]);
  if ($$contactmsg=="")
  {
    // Validate other fields
    for ($k=0;$k<$numfields;$k++)
    {
      // if field is file type then check extensions and upload size even if not required
      if (($inputtype_array[$k]=="file") && (trim($inputvalues[$k])!=""))
      {
        $ext=sl_fileextension(trim($inputvalues[$k]));
        $ext=trim(strtolower($ext));
        if (!is_integer(array_search($ext,$extsarray)))
        {
          $$contactmsg=MSG_UPLOADTYPE;
          break;
        }
        if ($_FILES['contact_'.$id."_".$k]['size']>$attachmentsize)
        {
          $$contactmsg=MSG_UPLOADERROR;
          break;
        }
        $attachments=true;
        $attachmentpaths[]=$_FILES['contact_'.$id."_".$k]['tmp_name'];
        $attachmentfilenames[]=$_FILES['contact_'.$id."_".$k]['name'];        
      }
      // If field doesn't require validation in this case then skip
      if ($validation_array[$k]=="notrequired")
        continue;
      if (($validation_array[$k]!="notrequired") && ($showfieldfor_array[$k]==1) && (!$slpublicaccess))
        continue;
      if (($validation_array[$k]!="notrequired") && ($showfieldfor_array[$k]==2) && ($slpublicaccess))
        continue;
      // If captcha field then ignore as checked earlier
      if ($inputtype_array[$k]=="captcha")
        continue;        
      // If email field check value entered is valid email address
      if ($validation_array[$k]=="requiredemail")  
      {
        if (!sl_validate_email($inputvalues[$k]))
        {
        	$$contactmsg=$errormsg_array[$k];
          break;
        }
      }  
      if ($validation_array[$k]=="required")  
      {
        if (trim($inputvalues[$k])=="")
        {
        	$$contactmsg=$errormsg_array[$k];
          break;
        }
      }  
    }
  }
  if ($$contactmsg=="")
  {  
    // Allow plugins to reject contact form submission. Could also be used to handle submit.
    $paramdata=array();
    $paramdata['formid']=$id;
    $paramdata['sendemail']=$sendemail;
    $paramdata['redirect']=$sendemail;
    $paramdata['useremailvisitor']=$useremailvisitor;
    $paramdata['adminemailvisitor']=$adminemailvisitor;
    $paramdata['useremailmember']=$useremailmember;
    $paramdata['adminemailmember']=$adminemailmember;
    $paramdata['fromnameuser']=$fromnameuser;
    $paramdata['attachmenttypes']=$attachmenttypes;
    $paramdata['attachmentsize']=$attachmentsize;
    $paramdata['replytouser']=$replytouser;
    $paramdata['sendasuser']=$sendasuser;
    $paramdata['inputvalues']=$inputvalues;
    $paramdata['sitelokfield_array']=$sitelokfield_array;
    $paramdata['inputtype_array']=$inputtype_array;
    $paramdata['labeltext_array']=$labeltext_array;
    $paramdata['value_array']=$value_array;
    $paramdata['validation_array']=$validation_array;
    $paramdata['errormsg_array']=$errormsg_array;
    $paramdata['useas_array']=$useas_array;
    $paramdata['showfieldfor_array']=$showfieldfor_array;
    if ($namefield>-1)
      $paramdata['contactname']=$inputvalues[$namefield];
    else
      $paramdata['contactname']="";
    if ($emailfield>-1)
      $paramdata['contactemail']=$inputvalues[$emailfield];
    else
      $paramdata['contactemail']="";
    $paramdata['attachmentpaths']=$attachmentpaths;
    $paramdata['attachmentfilenames']=$attachmentfilenames;
    $paramdata['publicaccess']=$slpublicaccess;
    $paramdata['username']=$slusername;
    $paramdata['userid']=$sluserid;    
    $paramdata['password']=$slpassword;
    $paramdata['enabled']=$slenabled;
    $paramdata['name']=$slname;
    $paramdata['email']=$slemail;
    $paramdata['usergroups']=$slusergroups;
    $paramdata['custom1']=$slcustom1;
    $paramdata['custom2']=$slcustom2;
    $paramdata['custom3']=$slcustom3;
    $paramdata['custom4']=$slcustom4;
    $paramdata['custom5']=$slcustom5;
    $paramdata['custom6']=$slcustom6;
    $paramdata['custom7']=$slcustom7;
    $paramdata['custom8']=$slcustom8;
    $paramdata['custom9']=$slcustom9;
    $paramdata['custom10']=$slcustom10;
    $paramdata['custom11']=$slcustom11;
    $paramdata['custom12']=$slcustom12;
    $paramdata['custom13']=$slcustom13;
    $paramdata['custom14']=$slcustom14;
    $paramdata['custom15']=$slcustom15;
    $paramdata['custom16']=$slcustom16;
    $paramdata['custom17']=$slcustom17;
    $paramdata['custom18']=$slcustom18;
    $paramdata['custom19']=$slcustom19;
    $paramdata['custom20']=$slcustom20;
    $paramdata['custom21']=$slcustom21;
    $paramdata['custom22']=$slcustom22;
    $paramdata['custom23']=$slcustom23;
    $paramdata['custom24']=$slcustom24;
    $paramdata['custom25']=$slcustom25;
    $paramdata['custom26']=$slcustom26;
    $paramdata['custom27']=$slcustom27;
    $paramdata['custom28']=$slcustom28;
    $paramdata['custom29']=$slcustom29;
    $paramdata['custom30']=$slcustom30;
    $paramdata['custom31']=$slcustom31;
    $paramdata['custom32']=$slcustom32;
    $paramdata['custom33']=$slcustom33;
    $paramdata['custom34']=$slcustom34;
    $paramdata['custom35']=$slcustom35;
    $paramdata['custom36']=$slcustom36;
    $paramdata['custom37']=$slcustom37;
    $paramdata['custom38']=$slcustom38;
    $paramdata['custom39']=$slcustom39;
    $paramdata['custom40']=$slcustom40;
    $paramdata['custom41']=$slcustom41;
    $paramdata['custom42']=$slcustom42;
    $paramdata['custom43']=$slcustom43;
    $paramdata['custom44']=$slcustom44;
    $paramdata['custom45']=$slcustom45;
    $paramdata['custom46']=$slcustom46;
    $paramdata['custom47']=$slcustom47;
    $paramdata['custom48']=$slcustom48;
    $paramdata['custom49']=$slcustom49;
    $paramdata['custom50']=$slcustom50;              
    // Call plugin event
    for ($p=0;$p<$slnumplugins;$p++)
    {
      if (function_exists($slplugin_event_onCheckContact[$p]))
      {
        $$contactmsg=call_user_func($slplugin_event_onCheckContact[$p],$slpluginid[$p],$paramdata);
        if ($$contactmsg!="")
          break;
      }  
    }
    if ($$contactmsg=="")
    {
      // Call user event handler
      if (function_exists("sl_onCheckContact"))
        $$contactmsg=sl_onCheckContact($paramdata);      
    }  
  }
  if ($$contactmsg=="")
  {  
    if (!$slpublicaccess)
    {
      // Make email data for !!!contactform!!!
      $contactform="";
      for ($k=0;$k<$numfields;$k++)
      {
        // Skip if field not for logged in users, captcha  or label
        if (($showfieldfor_array[$k]!=1) && ($inputtype_array[$k]!="captcha") && ($inputtype_array[$k]!="label"))
        {
          $data=sl_cleandata($inputvalues[$k]);
          if ($inputtype_array[$k]=="textarea")
            $data=str_replace("\n","<br>\n",$data);
          $contactform.="<strong>".$labeltext_array[$k]."</strong><br>\n";
          $contactform.=$data."<br>\n";
          $contactform.="<br>\n";       
        }
      }
      // Send email out to recipient(s) from a logged in user
      if ($adminemailmember!="")
      {
        if (sl_ReadEmailTemplate($adminemailmember,$subject,$mailBody,$htmlformat))
        {
          if ($htmlformat!="Y")
          {
            $contactform=str_replace("<br>","",$contactform);
            $contactform=str_replace("<strong>","",$contactform);          
            $contactform=str_replace("</strong>","",$contactform);          
          }
          $subject=str_replace("!!!contactform!!!",$contactform,$subject);
          $mailBody=str_replace("!!!contactform!!!",$contactform,$mailBody);
          for ($k=0;$k<$numfields;$k++)
          {
            $data=sl_cleandata($inputvalues[$k]);
            if ($inputtype_array[$k]=="textarea")
              $data=str_replace("\n","<br>\n",$data);
            $subject=str_replace("!!!contactlabel_".$k."!!!",$labeltext_array[$k],$subject);
            $subject=str_replace("!!!contactvalue_".$k."!!!",$data,$subject);
            $mailBody=str_replace("!!!contactlabel_".$k."!!!",$labeltext_array[$k],$mailBody);
            $mailBody=str_replace("!!!contactvalue_".$k."!!!",$data,$mailBody);
          }
          $fromemail=$slemail;
          if ($emailfield>-1)
            $fromemail=$inputvalues[$emailfield];
          $fromname=$slname;
          if ($namefield>-1)
            $fromname=$inputvalues[$namefield];
          $sendemailarray=explode("\n",$sendemail);
          for ($k=0;$k<count($sendemailarray);$k++)
          {
            $sendemailarray[$k]=str_replace(chr(13),"",$sendemailarray[$k]);
            if ($sendemailarray[$k]=="")
              continue;
            if ($sendasuser==1)
              $extraparams=array("fromname"=>$fromname,"replyto"=>"","fromemail"=>$fromemail);
            else
              $extraparams=array("fromname"=>$fromname,"replyto"=>$fromemail,"fromemail"=>"");
            if ($attachments)
            {
              $extraparams['attachmentpaths']=$attachmentpaths;
              $extraparams['attachmentfilenames']=$attachmentfilenames;
            }
            sl_SendEmail($sendemailarray[$k],$mailBody,$subject,$htmlformat,$slusername,$slpassword,$slname,$slemail,$slusergroups,$slcustom1, $slcustom2, $slcustom3, $slcustom4, $slcustom5, $slcustom6, $slcustom7, $slcustom8, $slcustom9, $slcustom10,
                         $slcustom11, $slcustom12, $slcustom13, $slcustom14, $slcustom15, $slcustom16, $slcustom17, $slcustom18, $slcustom19, $slcustom20,$slcustom21, $slcustom22, $slcustom23, $slcustom24, $slcustom25, $slcustom26, $slcustom27, $slcustom28, $slcustom29, $slcustom30,
                         $slcustom31, $slcustom32, $slcustom33, $slcustom34, $slcustom35, $slcustom36, $slcustom37, $slcustom38, $slcustom39, $slcustom40,$slcustom41, $slcustom42, $slcustom43, $slcustom44, $slcustom45, $slcustom46, $slcustom47, $slcustom48, $slcustom49, $slcustom50,$extraparams);
          }
        }              
      }
      // Send email out to logged in user
      if ($useremailmember!="")
      {
        if (sl_ReadEmailTemplate($useremailmember,$subject,$mailBody,$htmlformat))
        {
          if ($htmlformat!="Y")
          {
            $contactform=str_replace("<br>","",$contactform);
            $contactform=str_replace("<strong>","",$contactform);          
            $contactform=str_replace("</strong>","",$contactform);          
          }
          $subject=str_replace("!!!contactform!!!",$contactform,$subject);
          $mailBody=str_replace("!!!contactform!!!",$contactform,$mailBody);
          for ($k=0;$k<$numfields;$k++)
          {
            $data=sl_cleandata($inputvalues[$k]);
            if ($inputtype_array[$k]=="textarea")
              $data=str_replace("\n","<br>\n",$data);
            $subject=str_replace("!!!contactlabel_".$k."!!!",$labeltext_array[$k],$subject);
            $subject=str_replace("!!!contactvalue_".$k."!!!",$data,$subject);
            $mailBody=str_replace("!!!contactlabel_".$k."!!!",$labeltext_array[$k],$mailBody);
            $mailBody=str_replace("!!!contactvalue_".$k."!!!",$data,$mailBody);
          }
          $toemail=$slemail;
          if ($emailfield>-1)
            $toemail=$inputvalues[$emailfield];
          $toname=$slname;
          if ($namefield>-1)
            $toname=$inputvalues[$namefield];
          sl_SendEmail($toemail,$mailBody,$subject,$htmlformat,$slusername,$slpassword,$slname,$slemail,$slusergroups,$slcustom1, $slcustom2, $slcustom3, $slcustom4, $slcustom5, $slcustom6, $slcustom7, $slcustom8, $slcustom9, $slcustom10,
                         $slcustom11, $slcustom12, $slcustom13, $slcustom14, $slcustom15, $slcustom16, $slcustom17, $slcustom18, $slcustom19, $slcustom20,$slcustom21, $slcustom22, $slcustom23, $slcustom24, $slcustom25, $slcustom26, $slcustom27, $slcustom28, $slcustom29, $slcustom30,
                         $slcustom31, $slcustom32, $slcustom33, $slcustom34, $slcustom35, $slcustom36, $slcustom37, $slcustom38, $slcustom39, $slcustom40,$slcustom41, $slcustom42, $slcustom43, $slcustom44, $slcustom45, $slcustom46, $slcustom47, $slcustom48, $slcustom49, $slcustom50,array("fromname"=>$fromnameuser,"replyto"=>$replytouser)); 
        }              
      }
    }
    if (($slpublicaccess) && ($emailfield!=-1))
    {
      // Make email data for !!!contactform!!!
      $contactform="";
      for ($k=0;$k<$numfields;$k++)
      {
        // Skip if field not for visitors, captcha  or label
        if (($showfieldfor_array[$k]!=2) && ($inputtype_array[$k]!="captcha") && ($inputtype_array[$k]!="label"))
        {
          $data=sl_cleandata($inputvalues[$k]);
          if ($inputtype_array[$k]=="textarea")
            $data=str_replace("\n","<br>\n",$data);
          $contactform.="<strong>".$labeltext_array[$k]."</strong><br>\n";
          $contactform.=$data."<br>\n";
          $contactform.="<br>\n";       
        }
      }    
      // Send email out to recipient(s) from a logged in user
      if ($adminemailvisitor!="")
      {
        if (sl_ReadEmailTemplate($adminemailvisitor,$subject,$mailBody,$htmlformat))
        {
          if ($htmlformat!="Y")
          {
            $contactform=str_replace("<br>","",$contactform);
            $contactform=str_replace("<strong>","",$contactform);          
            $contactform=str_replace("</strong>","",$contactform);          
          }
          $subject=str_replace("!!!contactform!!!",$contactform,$subject);
          $mailBody=str_replace("!!!contactform!!!",$contactform,$mailBody);
          for ($k=0;$k<$numfields;$k++)
          {
            $data=sl_cleandata($inputvalues[$k]);
            if ($inputtype_array[$k]=="textarea")
              $data=str_replace("\n","<br>\n",$data);
            $subject=str_replace("!!!contactlabel_".$k."!!!",$labeltext_array[$k],$subject);
            $subject=str_replace("!!!contactvalue_".$k."!!!",$data,$subject);
            $mailBody=str_replace("!!!contactlabel_".$k."!!!",$labeltext_array[$k],$mailBody);
            $mailBody=str_replace("!!!contactvalue_".$k."!!!",$data,$mailBody);
          }
          $fromemail="";
          if ($emailfield>-1)
            $fromemail=$inputvalues[$emailfield];
          $fromname="";
          if ($namefield>-1)
            $fromname=$inputvalues[$namefield];        
          $sendemailarray=explode("\n",$sendemail);
          for ($k=0;$k<count($sendemailarray);$k++)
          {
            $sendemailarray[$k]=str_replace(chr(13),"",$sendemailarray[$k]);
            if ($sendemailarray[$k]=="")
              continue;
            if ($sendasuser==1)
              $extraparams=array("fromname"=>$fromname,"replyto"=>"","fromemail"=>$fromemail);
            else
              $extraparams=array("fromname"=>$fromname,"replyto"=>$fromemail,"fromemail"=>"");
            if ($attachments)
            {
              $extraparams['attachmentpaths']=$attachmentpaths;
              $extraparams['attachmentfilenames']=$attachmentfilenames;
            }
            sl_SendEmail($sendemailarray[$k],$mailBody,$subject,$htmlformat,"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",$extraparams);
          }
        }              
      }
      // Send email out to a logged in user
      if ($useremailvisitor!="")
      {
        if (sl_ReadEmailTemplate($useremailvisitor,$subject,$mailBody,$htmlformat))
        {
          if ($htmlformat!="Y")
          {
            $contactform=str_replace("<br>","",$contactform);
            $contactform=str_replace("<strong>","",$contactform);          
            $contactform=str_replace("</strong>","",$contactform);          
          }
          $subject=str_replace("!!!contactform!!!",$contactform,$subject);
          $mailBody=str_replace("!!!contactform!!!",$contactform,$mailBody);
          for ($k=0;$k<$numfields;$k++)
          {
            $data=sl_cleandata($inputvalues[$k]);
            if ($inputtype_array[$k]=="textarea")
              $data=str_replace("\n","<br>\n",$data);
            $subject=str_replace("!!!contactlabel_".$k."!!!",$labeltext_array[$k],$subject);
            $subject=str_replace("!!!contactvalue_".$k."!!!",$data,$subject);
            $mailBody=str_replace("!!!contactlabel_".$k."!!!",$labeltext_array[$k],$mailBody);
            $mailBody=str_replace("!!!contactvalue_".$k."!!!",$data,$mailBody);
          }
          $toemail=$slemail;
          if ($emailfield>-1)
            $toemail=$inputvalues[$emailfield];
          $toname=$slname;
          if ($namefield>-1)
            $toname=$inputvalues[$namefield];
          sl_SendEmail($toemail,$mailBody,$subject,$htmlformat,$slusername,$$password,$nm,$em,$ugs,"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",array("fromname"=>$fromnameuser,"replyto"=>$replytouser)); 
        }              
      }
    }
  }
  // Delete any uploaded files
  for ($k=0;$k<count($attachmentpaths);$k++)
    unlink($attachmentpaths[$k]);
  if ($$contactmsg=="")
  {
    if (!$slpublicaccess)
      $fromemail=$slemail;
    else
      $fromemail="";      
    if ($emailfield>-1)
      $fromemail=$inputvalues[$emailfield];
    if (!$slpublicaccess)
      $fromname=$slname;
    else
      $fromname="";      
    if ($namefield>-1)
      $fromname=$inputvalues[$namefield];
    $redirect=str_replace("!!!email!!!",urlencode($fromemail),$redirect);
    $redirect=str_replace("!!!name!!!",urlencode($fromname),$redirect);
    // Now call onContact for plugins and eventhandlers
    // Call plugin event
    for ($p=0;$p<$slnumplugins;$p++)
    {
      if (function_exists($slplugin_event_onContact[$p]))
        call_user_func($slplugin_event_onContact[$p],$slpluginid[$p],$paramdata);
    }
    // Call user event handler
    if (function_exists("sl_onContact"))
      sl_onContact($paramdata);
    if ($slajaxform)
    {
      ajaxContactReturn(true,"",$redirect);
      exit;
    }            
    header("Location: ".$redirect);
    exit;
  }
  else
  {
    if ($slajaxform)
    {
      ajaxLoginReturn(false,$$contactmsg,"");
      exit;
    }    
  }
  // Prefill data
  for ($k=0;$k<$numfields;$k++)
  {
    $var="custom_".$id."_".$k;
    $$var=sl_uncleandata($inputvalues[$k]);
  }
}



  
function sl_contactformhead($id,$simplestyle)
{
  $res=sl_get_contact_formcode(false,$id,false,$simplestyle,$css,$js,$html);
  if ($res)
    echo $css."\n".$js;  
}
function sl_contactformbody($id,$simplestyle)
{
  $res=sl_get_contact_formcode(false,$id,false,$simplestyle,$css,$js,$html);
  if ($res)
    echo $html;
  else
  {
    echo "<strong>Contact form not found</strong>";
  }    
}
function sl_get_contact_formcode($mysql_link,$id,$fullsource,$simplestyle,&$css,&$js,&$html)
{
global $contactmsg,$SitelokLocationURL,$SitelokLocationURLPath,$SiteKey,$slpublicaccess;
global $slusername,$slname,$slfirstname,$sllastname,$slemail,$slusergroups;
global $slcustom1,$slcustom2,$slcustom3,$slcustom4,$slcustom5,$slcustom6,$slcustom7,$slcustom8,$slcustom9,$slcustom10;
global $slcustom11,$slcustom12,$slcustom13,$slcustom14,$slcustom15,$slcustom16,$slcustom17,$slcustom18,$slcustom19,$slcustom20;
global $slcustom21,$slcustom22,$slcustom23,$slcustom24,$slcustom25,$slcustom26,$slcustom27,$slcustom28,$slcustom29,$slcustom30;
global $slcustom31,$slcustom32,$slcustom33,$slcustom34,$slcustom35,$slcustom36,$slcustom37,$slcustom38,$slcustom39,$slcustom40;
global $slcustom41,$slcustom42,$slcustom43,$slcustom44,$slcustom45,$slcustom46,$slcustom47,$slcustom48,$slcustom49,$slcustom50;
global $sl_ajaxforms;

if ($mysql_link===false)
{
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
    return(false);
}
// If form is not contact type then return
$mysql_result=mysqli_query($mysql_link,"SELECT type FROM sl_forms WHERE id=".sl_quote_smart($id));
if ($mysql_result===false)
  return(false);
$row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
if ($row===false)
  return(false);
if ($row['type']!="contact")
  return(false);
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_contactforms WHERE id=".sl_quote_smart($id)." AND position=0");
if ($mysql_result===false)
  return(false);
$row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
$sendemail=$row['sendemail'];
$redirect=$row['redirect'];
$useremailvisitor=$row['useremailvisitor'];
$adminemailvisitor=$row['adminemailvisitor'];
$useremailmember=$row['useremailmember'];
$adminemailmember=$row['adminemailmember'];
$attachmenttypes=$row['attachmenttypes'];
$attachmentsize=$row['attachmentsize'];
$fonttype=$row['fonttype'];
$labelcolor=$row['labelcolor'];
$labelsize=$row['labelsize'];
$labelstyle=$row['labelstyle'];
$inputtextcolor=$row['inputtextcolor'];
$inputtextsize=$row['inputtextsize'];
$inputtextstyle=$row['inputtextstyle'];
$inputbackcolor=$row['inputbackcolor'];
$bordersize=$row['bordersize'];
$bordercolor=$row['bordercolor'];
$borderradius=$row['borderradius'];
$inputpaddingv=$row['inputpaddingv'];
$inputpaddingh=$row['inputpaddingh'];
$rqdfieldlabel=$row['rqdfieldlabel'];
$rqdfieldcolor=$row['rqdfieldcolor'];
$rqdfieldsize=$row['rqdfieldsize'];
$rqdfieldstyle=$row['rqdfieldstyle'];
$messagecolor=$row['messagecolor'];
$messagesize=$row['messagesize'];
$messagestyle=$row['messagestyle'];
$btnlabel=$row['btnlabel'];
$btnlabelfont=$row['btnlabelfont'];
$btnlabelstyle=$row['btnlabelstyle'];
$btnlabelcolor=$row['btnlabelcolor'];
$btnlabelsize=$row['btnlabelsize'];
$btncolortype=$row['btncolortype'];
$btncolorfrom=$row['btncolorfrom'];
$btncolorto=$row['btncolorto'];
$btnborderstyle=$row['btnborderstyle'];
$btnbordercolor=$row['btnbordercolor'];
$btnbordersize=$row['btnbordersize'];
$btnpaddingv=$row['btnpaddingv'];
$btnpaddingh=$row['btnpaddingh'];
$btnradius=$row['btnradius'];
$formerrormsg=$row['formerrormsg'];
$formerrormsgcolor=$row['formerrormsgcolor'];
$formerrormsgsize=$row['formerrormsgsize'];
$formerrormsgstyle=$row['formerrormsgstyle'];
$backcolor=$row['backcolor'];
$maxformwidth=$row['maxformwidth'];

$border='solid';
if ($bordersize==0)
  $border='none';
$border.=' #'.$bordercolor.' '.$bordersize.'px'; 
if ($btncolortype=="solid")
  $btncolorto=$btncolorfrom;
if ($simplestyle)
  $captchaheight=30;
else
{
  if ($inputpaddingv=="0.3em")
    $captchaheight=($inputtextsize*1.75)+($bordersize*2);
  else
  {
    $inputpaddingvnum=str_replace('px','',$inputpaddingv);
    $captchaheight=($inputtextsize*1.2)+($inputpaddingvnum*2)+($bordersize*2);        
  }
  $captchaheight=round($captchaheight,2);
  if ($captchaheight<30) 
    $captchaheight=30;
}
$captchawidth=4;
if ($inputpaddingh!="0.3em")
{  
  $inputpaddinghnum=str_replace('px','',$inputpaddingh);
  $captchawidth=3+((2*$inputpaddinghnum)/$inputtextsize);
  $captchawidth=round($captchawidth,2);
  if ($captchawidth<4) 
    $captchawidth=4;
}
$textareaheight=8;
if ($inputpaddingv!="0.3em")
{  
  $inputpaddingvnum=str_replace('px','',$inputpaddingv);
  $textareaheight=8+((2*$inputpaddingvnum)/$inputtextsize);
  $textareaheight=round($textareaheight,2);
} 
$fileinputsize=$inputtextsize;
if ($inputtextsize>17)
  $fileinputsize=17;
// Get field details
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_contactforms WHERE id=".sl_quote_smart($id)." AND position>0 ORDER BY position ASC");
if ($mysql_result===false)
  return(false);
$numfields=0;
while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
{ 
  $sitelokfield_array[$numfields]=$row['sitelokfield'];
  $inputtype_array[$numfields]=$row['inputtype'];
  $labeltext_array[$numfields]=$row['labeltext'];
  $placetext_array[$numfields]=$row['placetext'];
  $value_array[$numfields]=$row['value'];
  $checked_array[$numfields]=$row['checked'];
  $validation_array[$numfields]=$row['validation'];
  $showrequired_array[$numfields]=$row['showrequired'];
  $errormsg_array[$numfields]=$row['errormsg'];
  $showfieldfor_array[$numfields]=$row['showfieldfor'];
  $fieldwidth_array[$numfields]=$row['fieldwidth'];
  $bottommargin_array[$numfields]=$row['bottommargin'];
  $numfields++;
}

// Adjust settings for spinner
if ($simplestyle)
  $spinnerposition="form";
else
  $spinnerposition="button";

if ($spinnerposition=="button")
{
  if ($btnlabelsize>=14)
  {
    $spinnermargintop=intval((($btnlabelsize*1.16)-16)/2);
    $spinnersize=12;
  }
  else
  {
    $spinnermargintop=0;
    $spinnersize=intval($btnlabelsize/1.4);
  }
  $spinnermarginright=-19;
  $spinnercolorR=hexdec(substr($btnlabelcolor,0,2));
  $spinnercolorG=hexdec(substr($btnlabelcolor,2,2));
  $spinnercolorB=hexdec(substr($btnlabelcolor,4,2));
}
if ($spinnerposition=="form")
{
  $spinnersize=$btnlabelsize;
  $spinnercolorR=0;
  $spinnercolorG=179;
  $spinnercolorB=234;
  $spinnermargintop=0;
  $spinnermarginright=0;
}


if (!$simplestyle)
{
$css = <<<EOT
<style type="text/css">

div#slform_{$id}{
  border: none;
  padding: 0;
  margin: 0;
  width: 100%;
  max-width: {$maxformwidth}px;
}

div#slform_{$id} label em{
  color: #{$rqdfieldcolor};
  font: {$rqdfieldstyle} {$rqdfieldsize}px {$fonttype};
  margin: 0 0 0 2px;
  padding:0;
  vertical-align: top;
  text-transform: none;
}

/* Text fields */
div.sltextfield_{$id} {
}

div#slform_{$id} div.sltextfield_{$id} label {
  display: block;
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding:0;
  text-transform: none;
}

div#slform_{$id} div.sltextfield_{$id} input[type="text"], input[type="email"], input[type="password"] {
	border: {$border};
	background-image: none;
	background-repeat: no-repeat;
	background-color:#{$inputbackcolor};
	font: {$inputtextstyle} {$inputtextsize}px {$fonttype};
	color: #{$inputtextcolor};
  margin: 0 0 0 0px;
	width:100%;
	-moz-border-radius: {$borderradius}px;
	-khtml-border-radius: {$borderradius}px;
	border-radius: {$borderradius}px;	
  padding: {$inputpaddingv} {$inputpaddingh};
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

/* Radio fields */
div.slradiofield_{$id} {
}

div#slform_{$id} div.slradiofield_{$id} label {  
  display: inline;
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
	vertical-align: middle;
	margin: 0;
	padding: 0;
  text-transform: none;	
}

div#slform_{$id} div.slradiofield_{$id} input[type="radio"] {
	margin: 0;
	padding: 0;
	background-image: none;
	background-repeat: no-repeat;
	color: #{$inputtextcolor};
  font: {$labelstyle} {$labelsize}px {$fonttype};
	vertical-align: middle;
}

/* Checkbox fields */
div.slcbfield_{$id} {
}

div#slform_{$id} div.slcbfield_{$id} label {  
  display: inline;
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
	vertical-align: middle;
	margin: 0;
	padding:0;
  text-transform: none;
}

div#slform_{$id} div.slcbfield_{$id} input[type="checkbox"] {
	margin-left: 0;
	background-image: none;
	background-repeat: no-repeat;
	color: #{$inputtextcolor};
  font: {$labelstyle} {$labelsize}px {$fonttype};
	vertical-align: middle;
	margin: 0;
	padding: 0;
}

/* Select fields */
div.slselectfield_{$id} {
}

div#slform_{$id} div.slselectfield_{$id} label {
  display: block;  
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding: 0;	
  text-transform: none;
}

div#slform_{$id} div.slselectfield_{$id} select {
	border: {$border};
	background-image: none;
	background-repeat: no-repeat;
	background-color: #{$inputbackcolor};
	font: {$inputtextstyle} {$inputtextsize}px {$fonttype};
	color: #{$inputtextcolor};
	width: 100%;
  -moz-border-radius: {$borderradius}px;
	-khtml-border-radius:{$borderradius}px;
  border-radius: {$borderradius}px;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  height: 1.8em;
}

/* Simple label_{$id} fields */
div.sllabelfield {
}

div#slform_{$id} div.sllabelfield_{$id} label {
  display: block;
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding: 0;	
  text-transform: none;
}

/* Captcha fields */
div.slcaptchafield_{$id} {
}

div#slform_{$id} div.slcaptchafield_{$id} input[type="text"]{
	border: {$border};
	background-image: none;
	background-repeat: no-repeat;
	background-color:#{$inputbackcolor};
	font: {$inputtextstyle} {$inputtextsize}px {$fonttype};
	color: #{$inputtextcolor};
  width: {$captchawidth}em;
	margin: 0;
  padding: {$inputpaddingv} {$inputpaddingh};
	-moz-border-radius: {$borderradius}px;
	-khtml-border-radius: {$borderradius}px;
	border-radius: {$borderradius}px;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  max-width: {$maxformwidth}px;
}

div#slform_{$id} div.slcaptchafield_{$id} label {
  display: block;  
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding:0;
}

div#slform_{$id} div.slcaptchafield_{$id} img{
  vertical-align: top;
  height: {$captchaheight}px;
}

/* File fields */
div.slfilefield_{$id} {
  margin: 0;
  padding:0;
}

div#slform_{$id} div.slfilefield_{$id} label {
  display: block;  
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding: 0;	
  text-transform: none;
}

div#slform_{$id} div.slfilefield_{$id} input[type="file"] {
	border: none;
	background-image: none;
	background-repeat: no-repeat;
	background-color:#{$inputbackcolor};
	font: {$inputtextstyle} {$fileinputsize}px {$fonttype};
	color: #{$inputtextcolor};
	margin: 0;
	padding: 2px;
	-moz-border-radius: {$borderradius}px;
	-khtml-border-radius: {$borderradius}px;
	border-radius: {$borderradius}px;
	width: 100%;
  padding: 0.3em;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

/* Textarea fields */
div.sltextareafield_{$id} {
}

div#slform_{$id} div.sltextareafield_{$id} label {
  display: block;
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding:0;
  text-transform: none;
}

div#slform_{$id} div.sltextareafield_{$id} textarea{
	border: {$border};
	background-image: none;
	background-repeat: no-repeat;
	background-color:#{$inputbackcolor};
	font: {$inputtextstyle} {$inputtextsize}px {$fonttype};
	color: #{$inputtextcolor};
  margin: 0 0 0 0px;
	width:100%;
  height: {$textareaheight}em;
	resize: none;
	-moz-border-radius: {$borderradius}px;
	-khtml-border-radius: {$borderradius}px;
	border-radius: {$borderradius}px;	
  padding: {$inputpaddingv} {$inputpaddingh};
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

div#slform_{$id} div.slmsg_{$id} {  
  padding: 0;
  margin: 1px 0 0 0;
  text-align: left;
  color: #{$messagecolor};
	font: {$messagestyle} {$messagesize}px {$fonttype};
/*
	border: solid 1px white;
*/
}

div#slform_{$id} div.slformmsg_{$id} {
  padding: 0;
  margin: 0 0 10px 0;
  text-align: left;
  color: #{$formerrormsgcolor};
	font: {$formerrormsgstyle} {$formerrormsgsize}px {$fonttype};  
}

/* Button from http://www.bestcssbuttongenerator.com */
div#slform_{$id} #myButton_{$id} {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #{$btncolorfrom}), color-stop(1, #{$btncolorto}));
	background:-moz-linear-gradient(top, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
	background:-webkit-linear-gradient(top, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
	background:-o-linear-gradient(top, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
	background:-ms-linear-gradient(top, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
	background:linear-gradient(to bottom, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#{$btncolorfrom}', endColorstr='#{$btncolorto}',GradientType=0);
	background-color:#{$btncolorfrom};
	-moz-border-radius: {$btnradius}px;
	-webkit-border-radius: {$btnradius}px;
	border-radius: {$btnradius}px;
	display:inline-block;
	cursor:pointer;
	color: #{$btnlabelcolor};
  font: $btnlabelstyle {$btnlabelsize}px {$btnlabelfont};
  padding: {$btnpaddingv}px {$btnpaddingh}px;
	margin: 0;
	text-decoration:none;
  border: $btnborderstyle #{$btnbordercolor} {$btnbordersize}px;
/*
	text-shadow:0px 1px 0px #ffffff;
*/

}
div#slform_{$id} #myButton_{$id}:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #{$btncolorto}), color-stop(1, #{$btncolorfrom}));
	background:-moz-linear-gradient(top, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
	background:-webkit-linear-gradient(top, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
	background:-o-linear-gradient(top, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
	background:-ms-linear-gradient(top, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
	background:linear-gradient(to bottom, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#{$btncolorto}', endColorstr='#{$btncolorfrom}',GradientType=0);
	background-color:#{$btncolorto};
}
div#slform_{$id} #myButton_{$id}:active {
	position:relative;
	top:1px;
}
/* End of button */

EOT;
if ($sl_ajaxforms)
{
$css .= <<<EOT
/* busy spinner */
div#slspinner_{$id} {
  display: none;
  box-sizing: content-box;  
  float: right;
  height: {$spinnersize}px;
  width: {$spinnersize}px;
  margin-left: 0px;
  margin-right: {$spinnermarginright}px;
  margin-top: {$spinnermargintop}px;
  position: relative;
  -webkit-animation: rotation .6s infinite linear;
  -moz-animation: rotation .6s infinite linear;
  -o-animation: rotation .6s infinite linear;
  animation: rotation .6s infinite linear;
  border-left: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-right: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-bottom: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-top: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, 1);
  border-radius: 100%;
}

@-webkit-keyframes rotation {
  from {
    -webkit-transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(359deg);
  }
}

@-moz-keyframes rotation {
  from {
    -moz-transform: rotate(0deg);
  }
  to {
    -moz-transform: rotate(359deg);
  }
}

@-o-keyframes rotation {
  from {
    -o-transform: rotate(0deg);
  }
  to {
    -o-transform: rotate(359deg);
  }
}

@keyframes rotation {
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(359deg);
  }
}

EOT;
}

$css .="\n/* Field specific styles */\n";
for ($k=0;$k<$numfields;$k++)
{
      
$css .= <<<EOT

#slfielddiv_{$id}_{$k}{
  width: {$fieldwidth_array[$k]}%;
  margin-bottom: {$bottommargin_array[$k]}px;
  padding:0;
  max-width: {$maxformwidth}px;
}

EOT;

}

$css .= <<<EOT
</style>
<!--[if lt IE 9]>
<style type="text/css">
div#slform_{$id} {
  width: {$maxformwidth}px;
}
</style>

<![endif]-->

<!--[if lte IE 7]>
<style type="text/css">
div#slform_{$id} div.sltextfield_{$id} input[type="text"], input[type="email"], input[type="password"] {
  width: 95%;
}
div#slform_{$id} div.sltextareafield_{$id} textarea{
  width: 95%;
}

div#slform_{$id} #myButton_{$id} {
	padding: 0px 0px;
}
</style>
<![endif]-->


EOT;
}
if ($simplestyle)
{
$css = <<<EOT
<style type="text/css">

div#slform_{$id}{
  border: none;
  padding: 0;
  margin: 0;
  width: 100%;
  max-width: {$maxformwidth}px;
}

div#slform_{$id} label em{
  color: #{$rqdfieldcolor};
  margin: 0 0 0 2px;
  padding:0;
  vertical-align: top;
}

/* Text fields */
div.sltextfield_{$id} {
}

div#slform_{$id} div.sltextfield_{$id} label {
  display: block;
  margin: 0 0 2px 0;
  padding:0;
}

div#slform_{$id} div.sltextfield_{$id} input[type="text"], input[type="email"], input[type="password"] {
  margin: 0 0 0 0px;
	width:100%;
  /* box-sizing: border-box; */
}

/* Radio fields */
div.slradiofield_{$id} {
}

div#slform_{$id} div.slradiofield_{$id} label {  
  display: inline;
	vertical-align: middle;
	margin: 0;
	padding: 0;
	
}

div#slform_{$id} div.slradiofield_{$id} input[type="radio"] {
	margin: 0;
	padding: 0;
	vertical-align: middle;
}

/* Checkbox fields */
div.slcbfield_{$id} {
}

div#slform_{$id} div.slcbfield_{$id} label {  
  display: inline;
	vertical-align: middle;
	margin: 0;
	padding:0;
}

div#slform_{$id} div.slcbfield_{$id} input[type="checkbox"] {
	margin-left: 0;
	vertical-align: middle;
	margin: 0;
	padding: 0;
}

/* Select fields */
div.slselectfield_{$id} {
}

div#slform_{$id} div.slselectfield_{$id} label {
  display: block;  
  margin: 0 0 2px 0;
  padding: 0;	
}

div#slform_{$id} div.slselectfield_{$id} select {
	margin: 0;
  /* box-sizing: border-box; */
}

/* Simple label_{$id} fields */
div.sllabelfield {
}

div#slform_{$id} div.sllabelfield_{$id} label {
  display: block;
  margin: 0 0 2px 0;
  padding: 0;	
}

/* Captcha fields */
div.slcaptchafield_{$id} {
}

div#slform_{$id} div.slcaptchafield_{$id} input[type="text"]{
	margin: 0;
  max-width: {$maxformwidth}px;
}

div#slform_{$id} div.slcaptchafield_{$id} label {
  display: block;  
  margin: 0 0 2px 0;
  padding:0;
}

div#slform_{$id} div.slcaptchafield_{$id} img{
  vertical-align: top;
  height: {$captchaheight}px;
}

/* File fields */
div.slfilefield_{$id} {
  margin: 0;
  padding:0;
}

div#slform_{$id} div.slfilefield_{$id} label {
  display: block;  
  margin: 0 0 2px 0;
  padding: 0;	
}

div#slform_{$id} div.slfilefield_{$id} input[type="file"] {
	margin: 0;
	padding: 2px;
	width: 100%;
  /* box-sizing: border-box; */
}

/* Textarea fields */
div.sltextareafield_{$id} {
}

div#slform_{$id} div.sltextareafield_{$id} label {
  display: block;
  margin: 0 0 2px 0;
  padding:0;
}

div#slform_{$id} div.sltextareafield_{$id} textarea{
  margin: 0 0 0 0px;
	width:100%;
	height: 4em;
	resize: none;
  /* box-sizing: border-box; */
}

div#slform_{$id} div.slmsg_{$id} {  
  padding: 0;
  margin: 1px 0 0 0;
  text-align: left;
  color: #{$messagecolor};
	font: {$messagestyle} {$messagesize}px {$fonttype};
}

div#slform_{$id} div.slformmsg_{$id} {
  padding: 0;
  margin: 0 0 10px 0;
  text-align: left;
  color: #{$formerrormsgcolor};
	font: {$formerrormsgstyle} {$formerrormsgsize}px {$fonttype};  
}

div#slform_{$id} #myButton_{$id} {
}
div#slform_{$id} #myButton_{$id}:hover {
}
div#slform_{$id} #myButton_{$id}:active {
}

EOT;
if ($sl_ajaxforms)
{
$css .= <<<EOT
/* busy spinner */
div#slspinner_{$id} {
  display: none;
  box-sizing: content-box;  
  float: right;
  height: {$spinnersize}px;
  width: {$spinnersize}px;
  margin-left: 0px;
  margin-right: {$spinnermarginright}px;
  margin-top: {$spinnermargintop}px;
  position: relative;
  -webkit-animation: rotation .6s infinite linear;
  -moz-animation: rotation .6s infinite linear;
  -o-animation: rotation .6s infinite linear;
  animation: rotation .6s infinite linear;
  border-left: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-right: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-bottom: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-top: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, 1);
  border-radius: 100%;
}

@-webkit-keyframes rotation {
  from {
    -webkit-transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(359deg);
  }
}

@-moz-keyframes rotation {
  from {
    -moz-transform: rotate(0deg);
  }
  to {
    -moz-transform: rotate(359deg);
  }
}

@-o-keyframes rotation {
  from {
    -o-transform: rotate(0deg);
  }
  to {
    -o-transform: rotate(359deg);
  }
}

@keyframes rotation {
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(359deg);
  }
}

EOT;
}
$css .="\n/* Field specific styles */\n";
for ($k=0;$k<$numfields;$k++)
{
 
$css .= <<<EOT

#slfielddiv_{$id}_{$k}{
  width: {$fieldwidth_array[$k]}%;
  margin-bottom: {$bottommargin_array[$k]}px;
  padding:0;
  max-width: {$maxformwidth}px;
}

EOT;

}

$css .= <<<EOT
</style>
<!--[if lt IE 9]>
<style type="text/css">
div#slform_{$id} {
  width: {$maxformwidth}px;
}
</style>

<![endif]-->

<!--[if lte IE 7]>
<style type="text/css">
div#slform_{$id} div.sltextfield_{$id} input[type="text"], input[type="email"], input[type="password"] {
  width: 95%;
}
div#slform_{$id} div.sltextareafield_{$id} textarea{
  width: 95%;
}

div#slform_{$id} #myButton_{$id} {
	padding: 0px 0px;
}
</style>
<![endif]-->


EOT;
}

$html="";
if ($fullsource)
{
  $html.="<?php\n";
  $html.="\$contactmsg=\"\";\n";
  $html.="if (isset(\$GLOBALS['contactmsg_{$id}']))\n";
  $html.="  \$contactmsg=\$GLOBALS['contactmsg_{$id}'];\n";
  $html.="//Set prefill values as required\n";
  for ($k=0;$k<$numfields;$k++)
  {
    $var="contact_".$id."_".$k;
    $html.="\$$var=\"\";\n";
  }  
  $html.="if ((!\$slpublicaccess) && ((!isset(\$_POST['contact_submit_$id'])) || (\$_POST['contact_submit_$id']==\"\")))\n";
  $html.="{\n";
  for ($k=0;$k<$numfields;$k++)
  {
    if ($sitelokfield_array[$k]=="username")
      $html.="  \$contact_".$id."_".$k."=sl_cleandata(\$slusername);\n";
    if ($sitelokfield_array[$k]=="fullname")
      $html.="  \$contact_".$id."_".$k."=sl_cleandata(\$slname);\n";
    if ($sitelokfield_array[$k]=="firstname")
      $html.="  \$contact_".$id."_".$k."=sl_cleandata(\$slfirstname);\n";
    if ($sitelokfield_array[$k]=="lastname")
      $html.="  \$contact_".$id."_".$k."=sl_cleandata(\$sllastname);\n";
    if ($sitelokfield_array[$k]=="email")
      $html.="  \$contact_".$id."_".$k."=sl_cleandata(\$slemail);\n";
    if ($sitelokfield_array[$k]=="usergroup")
      $html.="  \$contact_".$id."_".$k."=sl_cleandata(\$slusergroups);\n";
    if (substr($sitelokfield_array[$k],0,6)=="custom")
      $html.="  \$contact_".$id."_".$k."=sl_cleandata(\$slcustom".substr($sitelokfield_array[$k],6).");\n";

  }  
  $html.="}\n";
  $html.="if ((isset(\$_POST['contact_submit_$id'])) && (\$_POST['contact_submit_$id']!=\"\"))\n";
  $html.="{\n";
  for ($k=0;$k<$numfields;$k++)
  {
    $var="contact_".$id."_".$k;
    $html.="  \$$var=sl_cleandata(\$GLOBALS['$var']);\n";
  }  
  $html.="}\n";  
  $html.="?>\n";
}
else
{
  $contactmsg="";
  if (isset($GLOBALS['contactmsg_'.$id]))
  $contactmsg=$GLOBALS['contactmsg_'.$id];
  //Set prefill values as required
  for ($k=0;$k<$numfields;$k++)
  {
    $var="contact_".$id."_".$k;
    $$var="";
    if ((!$slpublicaccess) && ((!isset($_POST['contact_submit_'.$id])) || ($_POST['contact_submit_'.$id]=="")))
    {
      if ($sitelokfield_array[$k]=="username")
        $$var=sl_cleandata($slusername);
      if ($sitelokfield_array[$k]=="fullname")
        $$var=sl_cleandata($slname);
      if ($sitelokfield_array[$k]=="firstname")
        $$var=sl_cleandata($slfirstname);
      if ($sitelokfield_array[$k]=="lastname")
        $$var=sl_cleandata($sllastname);
      if ($sitelokfield_array[$k]=="email")
        $$var=sl_cleandata($slemail);
      if ($sitelokfield_array[$k]=="usergroup")
        $$var=sl_cleandata($slusergroups);
      if (substr($sitelokfield_array[$k],0,6)=="custom")
      {
        $var2="slcustom".substr($sitelokfield_array[$k],6);
        $$var=sl_cleandata($$var2);
      }
    }
    if ((isset($_POST['contact_submit_'.$id])) && ($_POST['contact_submit_'.$id]!=""))
      $$var=sl_cleandata($GLOBALS[$var]);
  }
}



// If file type in form then add enctype="multipart/form-data" for form
$enctype="";
if (false!==array_search("file", $inputtype_array))
  $enctype="enctype=\"multipart/form-data\" ";
$html .= <<<EOT
<div id="slform_{$id}">
<form action="" method="post" {$enctype}onSubmit="return slvalidateform_{$id}(this)">

EOT;
if ($sl_ajaxforms)
{
$html.= <<<EOT
    <input type="hidden" name="slajaxform" value="0">

EOT;
}
$html.="<div id=\"slformmsg_{$id}\" class=\"slformmsg_{$id}\">";
if ($fullsource)
  $html.="<?php if (\$contactmsg!=\"\") echo \$contactmsg; ?>";
else
{
  if ($contactmsg!="")
    $html.="$contactmsg";
}  
$html.="</div>\n";
for ($k=0;$k<$numfields;$k++)
{

if (!$fullsource)
{ 
  // Skip if we don't need to show this field as user is logged in etc
  if (($showfieldfor_array[$k]==1) && (!$slpublicaccess))
    continue;
  if (($showfieldfor_array[$k]==2) && ($slpublicaccess))
    continue;
}
else
{
  if ($showfieldfor_array[$k]!=0)
  {
    if ($showfieldfor_array[$k]==1)
    {  
  $html .= <<<EOT

    <?php
    if (\$slpublicaccess)
    {
    ?>
EOT;
    }
    if ($showfieldfor_array[$k]==2)
    {  
  $html .= <<<EOT

<?php if (!\$slpublicaccess) { ?>
EOT;
    }
  }
}
  
// Set field name to use. Just use a number name
$fieldname="contact_".$id."_".$k;

if ($inputtype_array[$k]=="text")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="sltextfield_{$id}">
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$type="text";
if ($validation_array[$k]=="requiredemail")
  $type="email";
$maxlength="255";
$html .= <<<EOT
</label>
<input type="{$type}" name="{$fieldname}" id="slfieldinput_{$id}_{$k}"
EOT;
if ($placetext_array[$k]!='')
  $html.=" placeholder=\"{$placetext_array[$k]}\"";
$html.=" maxlength=\"".$maxlength."\"";
if ($fullsource)
  $html.=" value=\"<?php echo \${$fieldname}; ?>\"";  
else
  $html.=" value=\"".$$fieldname."\"";  
    
$html.= ">\n";
if ($validation_array[$k]!="notrequired")
  $html .="<div id=\"slmsg_{$id}_{$k}\" class=\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


if ($inputtype_array[$k]=="password")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="sltextfield_{$id}">
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .= <<<EOT
</label>
<input type="password" name="{$fieldname}" id="slfieldinput_{$id}_{$k}"
EOT;
if ($placetext_array[$k]!='')
  $html.=" placeholder=\"{$placetext_array[$k]}\"";        
$html .= ">\n";
if ($validation_array[$k]!="notrequired")
  $html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


if ($inputtype_array[$k]=="dropdown")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="slselectfield_{$id}">
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .= <<<EOT
</label>
<select name="{$fieldname}" id="slfieldinput_{$id}_{$k}">
EOT;
$options=explode("\n",$value_array[$k]);
for ($j=0;$j<count($options);$j++)
{
  if($options[$j]!="")
  {
    $optionname=strtok($options[$j],",");
    $optionvalue=trim(strtok(","));
    // See if separate value
    if (($j==0) && ($validation_array[$k]!='notrequired'))
    {
      $html.="<option value=\"\">".$optionname."</option>\n";
      continue;
    }
    if ($optionvalue!="")
    {
      if ($fullsource)
        $html.="<option value=\"".$optionvalue."\" <?php if (\${$fieldname}==\"{$optionvalue}\") echo \"selected=selected\"; ?>>".$optionname."</option>\n";
      else
      {
        $html.="<option value=\"".$optionvalue."\" ";
        if ($$fieldname==$optionvalue) $html.= "selected=selected";
        $html.=">".$optionname."</option>\n";        
      }  
      continue;   
    } 
    if ($fullsource) 
      $html.="<option value=\"".$optionname."\" <?php if (\${$fieldname}==\"{$optionname}\") echo \"selected=selected\"; ?>>".$optionname."</option>\n";            
    else
    {
      $html.="<option value=\"".$optionname."\" ";
      if ($$fieldname==$optionname) $html.="selected=selected";
      $html.=">".$optionname."</option>\n";            
    }   
  }  
}
$html .= <<<EOT
</select>

EOT;
if ($validation_array[$k]!="notrequired")
  $html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


if ($inputtype_array[$k]=="checkbox")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="slcbfield_{$id}">
<input type="checkbox" name="{$fieldname}" id="slfieldinput_{$id}_{$k}" value="{$value_array[$k]}"
EOT;
if ($checked_array[$k]=="1")
{
  if ($fullsource)
    $html.=" <?php if ((\${$fieldname}==\"{$value_array[$k]}\") || (\${$fieldname}==\"\")) echo \" checked=checked\"; ?>";    
  else
    if (($$fieldname==$value_array[$k]) || ($$fieldname=="")) $html.= " checked=checked";
}
else
{
  if ($fullsource)
    $html.=" <?php if (\${$fieldname}==\"{$value_array[$k]}\") echo \" checked=checked\"; ?>";    
  else
    if ($$fieldname==$value_array[$k]) $html.= " checked=checked";
}  
$html .= <<<EOT
>
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .= <<<EOT
</label>

EOT;
if ($validation_array[$k]!="notrequired")
  $html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


if ($inputtype_array[$k]=="captcha")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="slcaptchafield_{$id}">
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .="</label>\n";


if ($fullsource)
{
  $html .="<?php\n";
  $html .="if (function_exists('slshowcaptcha'))\n";
  $html .="{\n";
  $html.="echo slshowcaptcha();\n";
  $html .="}\n";
  $html .="else\n";
  $html .="{\n";
  $html .="?>\n";  
  $html .= <<<EOT
<input type="text" name="{$fieldname}" id="slfieldinput_{$id}_{$k}" placeholder="{$placetext_array[$k]}" maxlength="5" autocorrect="off" autocapitalize="off" spellcheck="off" autocomplete="off">
&nbsp;
EOT;
  $html.="<img src=\"<?php echo \$SitelokLocationURLPath; ?>turingimage.php\" height=\"{$captchaheight}\" title=\"{$placetext_array[$k]}\" alt=\"{$placetext_array[$k]}\">\n";
  $html .="<?php\n";  
  $html .="}\n";
  $html .="?>\n";    
}
else
{
  if (function_exists('slshowcaptcha'))
    $html.=slshowcaptcha();
  else
  {    
$html .= <<<EOT
<input type="text" name="{$fieldname}" id="slfieldinput_{$id}_{$k}" placeholder="{$placetext_array[$k]}" maxlength="5" autocorrect="off" autocapitalize="off" spellcheck="off" autocomplete="off">
&nbsp;
EOT;
$html.="<img src=\"{$SitelokLocationURLPath}turingimage.php\" height=\"{$captchaheight}\" title=\"{$placetext_array[$k]}\" alt=\"{$placetext_array[$k]}\">\n";
  }
} 
  
if ($validation_array[$k]!="notrequired")
  $html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


if ($inputtype_array[$k]=="label")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="sllabelfield_{$id}">
<label>{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .= <<<EOT
</label>
<div id="slmsg_{$id}_{$k}" class ="slmsg_{$id}"></div>
</div>

EOT;
}


if ($inputtype_array[$k]=="file")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="slfilefield_{$id}">
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .= <<<EOT
</label>
<input type="file" name="{$fieldname}" id="slfieldinput_{$id}_{$k}">

EOT;

// Always have message field for file type even if not required as extension are checked
$html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


if ($inputtype_array[$k]=="textarea")
{
$cusnum=substr($fieldname,6);
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="sltextareafield_{$id}">
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .= <<<EOT
</label>
<textarea name="{$fieldname}" id="slfieldinput_{$id}_{$k}"
EOT;
if ($placetext_array[$k]!='')
  $html.=" placeholder=\"{$placetext_array[$k]}\"";
$html.=" maxlength=\"{$customfieldlengths[$cusnum]}\">";
if ($fullsource)          
  $html .= "<?php echo \${$fieldname}; ?></textarea>\n";
else
  $html .= "{$$fieldname}</textarea>\n";
  
if ($validation_array[$k]!="notrequired")
  $html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}

if (($fullsource) && ($showfieldfor_array[$k]!=0))
{
$html .= <<<EOT
<?php } ?>

EOT;
}

}
$hashedid=$id."|".md5($id."contactid".$SiteKey);
$html .= <<<EOT

<input name="contact_id" type="hidden" value="{$hashedid}">

EOT;
// Button
if ($sl_ajaxforms)
{  
  if ($spinnerposition=="button")
    $html.="<button id=\"myButton_".$id."\" type=\"submit\">".$btnlabel."<div id=\"slspinner_".$id."\"></div></button>\n";
  if ($spinnerposition=="form")
    $html.="<button id=\"myButton_{$id}\" type=\"submit\">".$btnlabel."</button><div id=\"slspinner_".$id."\"></div>\n";
}
else
{  
  $html.="<button id=\"myButton_{$id}\" type=\"submit\">".$btnlabel."</button>\n";
}

$html .= <<<EOT
</form>
</div>

EOT;
$js="";
$js .= <<<EOT
<script type="text/javascript">
function slvalidateform_{$id}(form)
{
  var errorfound=false
  document.getElementById('slformmsg_{$id}').innerHTML=''
  //document.getElementById('slformmsg_{$id}').style['display']="none"

EOT;
for ($k=0;$k<$numfields;$k++)
{
  if (($inputtype_array[$k]=="captcha") && (function_exists("slcaptchahead")))
    continue;
  if (!$fullsource)
  {   
    // Skip if we don't need to show this field as user is logged in etc
    if (($showfieldfor_array[$k]==1) && (!$slpublicaccess))
      continue;
    if (($showfieldfor_array[$k]==2) && ($slpublicaccess))
      continue;
  }
  $errormsg=str_replace("'","\'",$errormsg_array[$k]);
  $errormsg=str_replace("\n","\\n",$errormsg);
  if ($fullsource)  
  {
    if ($showfieldfor_array[$k]!=0)
    {
      if ($showfieldfor_array[$k]==1)
      {  
    $js .= <<<EOT

      <?php
      if (\$slpublicaccess)
      {
      ?>
EOT;
      }
      if ($showfieldfor_array[$k]==2)
      {  
    $js .= <<<EOT

  <?php if (!\$slpublicaccess) { ?>
EOT;
      }
    }
}



  if ($validation_array[$k]!='notrequired')
  {  
    // email field validation
    if ($validation_array[$k]=="requiredemail")
    {
$js .= <<<EOT

  // Validate slfieldinput_{$id}_{$k}
  document.getElementById('slmsg_{$id}_{$k}').innerHTML=''
  document.getElementById('slmsg_{$id}_{$k}').style['display']="none"  
  var email=document.getElementById('slfieldinput_{$id}_{$k}').value
  email=sltrim_{$id}(email)
  if ((email=='') || (!slvalidateemail_{$id}(email)))
  {
    document.getElementById('slmsg_{$id}_{$k}').innerHTML='{$errormsg}'
    document.getElementById('slmsg_{$id}_{$k}').style['display']="block"  
    if (!errorfound)
      document.getElementById('slfieldinput_{$id}_{$k}').focus()
    errorfound=true
  }
  

EOT;
    }    



    // standard field validation  
    if ($validation_array[$k]!="requiredemail")
    {  
      $js .= "  // Validate slfieldinput_{$id}_{$k}";
      if (($inputtype_array[$k]!="radio") && ($inputtype_array[$k]!="checkbox"))
      {
$js .= <<<EOT

  document.getElementById('slmsg_{$id}_{$k}').innerHTML=''
  document.getElementById('slmsg_{$id}_{$k}').style['display']="none"
  var value=document.getElementById('slfieldinput_{$id}_{$k}').value
  value=sltrim_{$id}(value)
  if (value=='')
  {
    document.getElementById('slmsg_{$id}_{$k}').innerHTML='{$errormsg}'
    document.getElementById('slmsg_{$id}_{$k}').style['display']="block"  
    if (!errorfound)
      document.getElementById('slfieldinput_{$id}_{$k}').focus()
    errorfound=true
  }


EOT;
      }
      else
      {
$js .= <<<EOT

  document.getElementById('slmsg_{$id}_{$k}').innerHTML=''
  document.getElementById('slmsg_{$id}_{$k}').style['display']="none"
  var checked=slseeifchecked_{$id}('slfieldinput_{$id}_{$k}')
  if (!checked)
  {
    document.getElementById('slmsg_{$id}_{$k}').innerHTML='{$errormsg}'
    document.getElementById('slmsg_{$id}_{$k}').style['display']="block"  
    if (!errorfound)
      document.getElementById('slfieldinput_{$id}_{$k}').focus()
    errorfound=true
  }


EOT;
      }        
    }
  }
  // If file field check for allowed extensions even if not required field
  if ($inputtype_array[$k]=="file")
  {
$msgtype=MSG_UPLOADTYPE;
$msgsize=MSG_UPLOADERROR;   
$js .= <<<EOT

  // If file uploaded check extensions are allowed for slfieldinput_{$id}_{$k} 
  var filename=document.getElementById('slfieldinput_{$id}_{$k}').value
  filename=sltrim_{$id}(filename)
  if (filename!='')
  {
    document.getElementById('slmsg_{$id}_{$k}').innerHTML=''
    document.getElementById('slmsg_{$id}_{$k}').style['display']="none"  
    var exts='{$attachmenttypes}'
    var extsarray=exts.split(' ')
    for (k=0;k<extsarray.length;k++)
      extsarray[k]=extsarray[k].trim()
    if ((filename!='') && (!slvalidatefiletype_{$id}(filename,extsarray)))
    {
      document.getElementById('slmsg_{$id}_{$k}').innerHTML='{$msgtype}'
      document.getElementById('slmsg_{$id}_{$k}').style['display']="block"  
      if (!errorfound)
        document.getElementById('slfieldinput_{$id}_{$k}').focus()
      errorfound=true
    }
    else if (filename!='')
    {
      if (slgetfilesize_{$id}('slfieldinput_{$id}_{$k}')>{$attachmentsize})
      {
        document.getElementById('slmsg_{$id}_{$k}').innerHTML='{$msgsize}'
        document.getElementById('slmsg_{$id}_{$k}').style['display']="block"  
        if (!errorfound)
          document.getElementById('slfieldinput_{$id}_{$k}').focus()
        errorfound=true        
      }
    }
  }
  

EOT;
    
  }

if (($fullsource) && ($showfieldfor_array[$k]!=0))
{
$js .= <<<EOT
<?php } ?>

EOT;
}


}  // for numfields

  $errormsg=str_replace("'","\'",$formerrormsg);
  $errormsg=str_replace("\n","\\n",$errormsg);

$js .= <<<EOT

  if (errorfound)
  {
    document.getElementById('slformmsg_{$id}').innerHTML='{$errormsg}'
    //document.getElementById('slformmsg_{$id}').style['display']="block"    
    return(false)
  }
  document.getElementById('slformmsg_{$id}').innerHTML=''
  //document.getElementById('slformmsg_{$id}').style['display']="none"  

EOT;
if ($sl_ajaxforms)
{
$js .= <<<EOT
  document.getElementById('slformmsg_{$id}').innerHTML='';
  //document.getElementById('slformmsg_{$id}').style['display']="none";
  //See if any file fields in form by checking enctype
  var slfilefields=false;
  var slajaxavailable=false;
  var slformdataavailable=false
  form.slajaxform.value="0";
  if (form.enctype=="multipart/form-data")
    slfilefields=true;
  if ((window.XMLHttpRequest) && (typeof JSON === "object"))
  {
    slajaxavailable=true;
    var xhr = new XMLHttpRequest();    
    slformdataavailable=(xhr && ('upload' in xhr));
  }
  // If ajax supported but no FormData then only use if no file fields
  if ((!slformdataavailable) && (slfilefields))
    slajaxavailable=false;
  if (slajaxavailable)
  {
    form.slajaxform.value="1";
    xhr.onreadystatechange = function()
    {
      if (xhr.readyState == 4 && xhr.status == 200)
      {
        // Handle callback
        document.getElementById('myButton_{$id}').disabled=false
        document.getElementById('slspinner_{$id}').style['display']="none"
        var data = JSON.parse(xhr.responseText);
        if(data.hasOwnProperty("error"))
        { 
          if (data.error.substr(-3,3)=="001")
          {
              location.reload(true);
              return(false);
          }
          return(false);
        }    
        if (data.success)
        {
          window.location=data.redirect;
          return(false);
        }
        else  
        {  
          document.getElementById('slformmsg_{$id}').innerHTML=data.message;
          //document.getElementById('slformmsg_{$id}').style['display']="block"
          return(false);
        }
      }
    };
    // Serialize form
    if (slformdataavailable)
      var formData = new FormData(form);
    else
      var formData=sl_serialize(form);
    var slfrmact=window.location.href;
    document.getElementById('myButton_{$id}').disabled=true
    document.getElementById('slspinner_{$id}').style['display']="block"
    xhr.open("POST", slfrmact, true);
    if (!slformdataavailable)
      xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(formData);
    return(false);
  }

EOT;
}
$js .= <<<EOT
  return(true)
}

function slvalidateemail_{$id}(email)
{
  var ck_email = /^([\w-\'!#$%&\*]+(?:\.[\w-\'!#$%&\*]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,9}(?:\.[a-z]{2})?)$/i
  if (!ck_email.test(email))
    return(false)
  return(true)  
}

function sltrim_{$id}(x)
{
    return x.replace(/^\s+|\s+$/gm,'');
}

function slseeifchecked_{$id}(id)
{
  if (document.getElementById(id).checked)
    return(true)
  return(false)
}

function slvalidatefiletype_{$id}(filename, exts)
{
  filename=filename.toLowerCase();
  return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(filename);
}

function slgetfilesize_{$id}(elementId)
{
  if (!window.File)
    return(0);
  var nBytes = 0,
  oFiles = document.getElementById(elementId).files,
  nFiles = oFiles.length;
  for (var nFileId = 0; nFileId < nFiles; nFileId++) {
      nBytes += oFiles[nFileId].size;
  }
  var sOutput = nBytes + " bytes";
  // optional code for multiples approximation
  for (var aMultiples = ["K", "M", "G", "T", "P", "E", "Z", "Y"], nMultiple = 0, nApprox = nBytes / 1024; nApprox > 1; nApprox /= 1024, nMultiple++) {
      sOutput = " (" + nApprox.toFixed(3) + aMultiples[nMultiple] + ")";
  }
  return nBytes;
}

EOT;

if ($sl_ajaxforms)
{
$js .= <<<EOT
function sl_serialize(form)
{
 if (!form || form.nodeName !== "FORM") {
   return;
 }
 var i, j, q = [];
 for (i = form.elements.length - 1; i >= 0; i = i - 1) {
   if (form.elements[i].name === "") {
     continue;
   }
   switch (form.elements[i].nodeName) {
   case 'INPUT':
     switch (form.elements[i].type) {
     case 'text':
     case 'email':
     case 'number':
     case 'hidden':
     case 'password':
     case 'button':
     case 'reset':
     case 'submit':
       q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
       break;
     case 'checkbox':
     case 'radio':
       if (form.elements[i].checked) {
         q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
       }           
       break;
     case 'file':
       break;
     }
     break;       
   case 'TEXTAREA':
     q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
     break;
   case 'SELECT':
     switch (form.elements[i].type) {
     case 'select-one':
       q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
       break;
     case 'select-multiple':
       for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
         if (form.elements[i].options[j].selected) {
           q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].options[j].value));
         }
       }
       break;
     }
     break;
   case 'BUTTON':
     switch (form.elements[i].type) {
     case 'reset':
     case 'submit':
     case 'button':
       q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
       break;
     }
     break;
   }
 }
 return q.join("&");
}

EOT;
}
$js .= <<<EOT
</script>

EOT;


if ($fullsource)
{
  $js .="<?php\n";
  $js .="if (function_exists('slcaptchahead'))\n";
  $js .="  echo slcaptchahead();\n";
  $js .="?>\n";    
}
else
{
  if (function_exists('slcaptchahead'))
    $js.=slcaptchahead();  
}
// Adjust for RW compatibilty
$html=sl_rwadjust($html);
return(true);
}

function ajaxContactReturn($success,$msg,$redirect)
{
  $data=array();
  $data['success']=$success;
  $data['message']=$msg;
  $data['redirect']=$redirect;
  echo json_encode($data);
}
?>