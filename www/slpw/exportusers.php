<?php
  $groupswithaccess="ADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    die(ADMINMSG_MYSQLERROR);
    exit;
  }  
  $act=$_POST['act'];
  if ($act=="")
  {
    // Get configuration data
    $mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_adminconfig LIMIT 1");
    if ($mysql_result!=false)
    {
      $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
      if ($row!=false)
      {
        $exportfilename=$row["exportfilename"];
        $exportuseheader=$row["exportuseheader"];
        $exporttype=$row["exporttype"];
        $exportfields=$row["exportfields"];
        $exportfieldsarray=array();
        for ($k=0;$k<strlen($exportfields);$k=$k+2)
        {
          $exportfieldsarray[$k/2]=substr($exportfields,$k,2);
        }  
      }
    }
  }  
  if (($act=="exportselected") && ($_POST['slcsrf']==$_SESSION['ses_slcsrf']))
  {
    $exportfilename=$_POST["filename"];
    $exportuseheader=$_POST["useheader"];
    $exporttype=$_POST["exporttype"];    
    if ($exporttype=="1")
    {
      $exportfields="";
      if ($_POST['created']=="1")
        $exportfields.="CR";
      if ($_POST['username']=="1")
        $exportfields.="US";
      if ($_POST['pss']=="1")
        $exportfields.="PW";
      if ($_POST['enabled']=="1")
        $exportfields.="EN";
      if ($_POST['name']=="1")
        $exportfields.="NM";
      if ($_POST['email']=="1")
        $exportfields.="EM";
      if ($_POST['usergroups']=="1")
        $exportfields.="UG";
      for ($k=1;$k<=50;$k++)
      {
        if ($_POST['custom'.$k]=="1")
        {
          if ($k<10)
            $val="0".$k;
          else
            $val=$k;
          $val=(string)$val;
          $exportfields.=$val;
        }
      }
    }
    else
    {
      $exportfields="CRUSPWENNMEMUG";
      for ($k=1;$k<=50;$k++)
      {
        $pvar1="CustomTitle".$k;
        $pvar2="Cus".$k;
        if ($$pvar1!="")
        {
          if ($k<10)
            $val="0".$k;
          else
            $val=$k;
          $val=(string)$val;
          $exportfields.=$val;
        }
      }    
    }
    // Save settings
    if ($exporttype=="1")
      $query="UPDATE sl_adminconfig SET exportfilename=".sl_quote_smart($exportfilename).", exportuseheader=".sl_quote_smart($exportuseheader).", exporttype=".sl_quote_smart($exporttype).", exportfields=".sl_quote_smart($exportfields)." WHERE confignum=1";
    else
      $query="UPDATE sl_adminconfig SET exportfilename=".sl_quote_smart($exportfilename).", exportuseheader=".sl_quote_smart($exportuseheader).", exporttype=".sl_quote_smart($exporttype)." WHERE confignum=1";      
    $mysql_result=mysqli_query($mysql_link,$query);
    $_SESSION['ses_ConfigReload']="reload";     
    $exportfieldsarray=array();
    for ($k=0;$k<strlen($exportfields);$k=$k+2)
    {
      $exportfieldsarray[$k/2]=substr($exportfields,$k,2);
    }  
    if (!isset($ExportSeparator))
      $ExportSeparator=",";
    header("Last-Modified: ".gmdate('D, d M Y H:i:s T'));
    header("Content-type: application/octet-stream");
    header("Content-disposition: attachment; filename=\"".$exportfilename."\"");
    header("Content-transfer-encoding: binary");
    if (($sortf!="") && ($sortf!=""))
      $sortquery=" ORDER BY ".mysqli_real_escape_string($mysql_link,$sortf)." ".mysqli_real_escape_string($mysql_link,$sortd);
    else
      $sortquery="";      
    $mysql_result=mysqli_query($mysql_link,"SELECT count(*) FROM ".$DbTableName." WHERE ".$SelectedField."='Yes'");
    $row = mysqli_fetch_row($mysql_result);
    if ($row!=false)
      $matchrows = $row[0];
    $userdata=array();
    $fieldcount=count($exportfieldsarray);
    $lastfield=$fieldcount-1;
    // Output header row
    if ($exportuseheader=="1")
    {
      $headertitles['CR']="created";
      $headertitles['US']="username";
      $headertitles['PW']="password";
      $headertitles['EN']="enabled";
      $headertitles['NM']="name";
      $headertitles['EM']="email";
      $headertitles['UG']="usergroups";
      for ($k=1;$k<=50;$k++)
      {
        if ($k<10)
          $val="0".$k;
        else
          $val=$k;
        $val=(string)$val;
        $headertitles[$val]="custom".$k;
      }
      for ($k=0;$k<$fieldcount;$k++)
      {
        echo "\"".$headertitles[$exportfieldsarray[$k]]."\"";
        if ($k!=$lastfield)
          echo $ExportSeparator;
      }
      print "\r\n";
    }
    for ($l=0;$l<$matchrows;$l=$l+$sl_dbblocksize)
    {
      $limit=" LIMIT ".$l.",".$sl_dbblocksize;
      $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." WHERE ".$SelectedField."='Yes'".$sortquery.$limit);
      while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
      {
        $userdata['SL']=$row[$SelectedField];
        $userdata['CR']=$row[$CreatedField];
        $userdata['US']=$row[$UsernameField];
        $userdata['PW']=$row[$PasswordField];
        $userdata['EN']=$row[$EnabledField];
        $userdata['NM']=$row[$NameField];
        $userdata['EM']=$row[$EmailField];
        $userdata['UG']=$row[$UsergroupsField];
        $userdata['01']=$row[$Custom1Field];
        $userdata['02']=$row[$Custom2Field];
        $userdata['03']=$row[$Custom3Field];
        $userdata['04']=$row[$Custom4Field];
        $userdata['05']=$row[$Custom5Field];
        $userdata['06']=$row[$Custom6Field];
        $userdata['07']=$row[$Custom7Field];
        $userdata['08']=$row[$Custom8Field];
        $userdata['09']=$row[$Custom9Field];
        $userdata['10']=$row[$Custom10Field];
        $userdata['11']=$row[$Custom11Field];
        $userdata['12']=$row[$Custom12Field];
        $userdata['13']=$row[$Custom13Field];
        $userdata['14']=$row[$Custom14Field];
        $userdata['15']=$row[$Custom15Field];
        $userdata['16']=$row[$Custom16Field];
        $userdata['17']=$row[$Custom17Field];
        $userdata['18']=$row[$Custom18Field];
        $userdata['19']=$row[$Custom19Field];
        $userdata['20']=$row[$Custom20Field];
        $userdata['21']=$row[$Custom21Field];
        $userdata['22']=$row[$Custom22Field];
        $userdata['23']=$row[$Custom23Field];
        $userdata['24']=$row[$Custom24Field];
        $userdata['25']=$row[$Custom25Field];
        $userdata['26']=$row[$Custom26Field];
        $userdata['27']=$row[$Custom27Field];
        $userdata['28']=$row[$Custom28Field];
        $userdata['29']=$row[$Custom29Field];
        $userdata['30']=$row[$Custom30Field];
        $userdata['31']=$row[$Custom31Field];
        $userdata['32']=$row[$Custom32Field];
        $userdata['33']=$row[$Custom33Field];
        $userdata['34']=$row[$Custom34Field];
        $userdata['35']=$row[$Custom35Field];
        $userdata['36']=$row[$Custom36Field];
        $userdata['37']=$row[$Custom37Field];
        $userdata['38']=$row[$Custom38Field];
        $userdata['39']=$row[$Custom39Field];
        $userdata['40']=$row[$Custom40Field];
        $userdata['41']=$row[$Custom41Field];
        $userdata['42']=$row[$Custom42Field];
        $userdata['43']=$row[$Custom43Field];
        $userdata['44']=$row[$Custom44Field];
        $userdata['45']=$row[$Custom45Field];
        $userdata['46']=$row[$Custom46Field];
        $userdata['47']=$row[$Custom47Field];
        $userdata['48']=$row[$Custom48Field];
        $userdata['49']=$row[$Custom49Field];
        $userdata['50']=$row[$Custom50Field];
        if (($DateFormat=="DDMMYY") && (strlen($userdata['CR'])==6))
          $cdate=substr($userdata['CR'],4,2)."/".substr($userdata['CR'],2,2)."/".substr($userdata['CR'],0,2);
        else
          $cdate=substr($userdata['CR'],2,2)."/".substr($userdata['CR'],4,2)."/".substr($userdata['CR'],0,2);
        $userdata['CR']=$cdate;
        for ($k=0;$k<$fieldcount;$k++)
        {
          echo "\"".str_replace('"','""',$userdata[$exportfieldsarray[$k]])."\"";
          if ($k!=$lastfield)
            echo $ExportSeparator;
        }
        print "\r\n";
      }
    }
  exit;
  }

?>
<!DOCTYPE html>
<html>
<head>
<?php
$pagename="exportusers";
include("adminhead.php");
?>
<title><?php echo ADMINMENU_EXPORTSELECTED; ?></title>
<link rel="stylesheet" href="exportusers.css"></head>
<?php include("adminthemeheader.php"); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="glyphicon glyphicon-export"></i> <?php echo ADMINMENU_EXPORTSELECTED; ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><?php echo ADMINMENU_EXPORTSELECTED; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>

          <div class="row">

          <form id="exportuserform" name="exportuserform" role="form" class="form-horizontal" method="post" action="exportusers.php">
          <input type="hidden" name="slcsrf" id="slcsrf" value="<?php echo $slcsrftoken; ?>">
          <input type="hidden" name="act" id="act" value="">



            <div class="col-xs-12">

              <div class="box">

                <div class="box-body">

                  <div class="form-group">
                    <label class="col-xs-12" for="filename"><?php echo ADMINEU_FILE; ?></label>
                     <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3" id="filenamediv">
                        <input type="text" id="filename" name="filename" class="form-control" value="<?php echo $exportfilename; ?>" maxlength="100">
                    </div>
                  </div> 

                  <div class="form-group">
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-control" name="useheader" id="useheader" value="1" <?php if ($exportuseheader=="1") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINEU_EXPHEAD; ?>
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-xs-12" for="exporttype"><?php echo ADMINEU_EXPTYPE; ?></label>
                     <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3" id="exporttypediv">
                        <select id="exporttype" name="exporttype" class="form-control selectpicker">
                        <option value="0" <?php if ($exporttype=="0") echo "selected"; ?>><?php echo ADMINEU_EXPTYPE1; ?></option>
                        <option value="1" <?php if ($exporttype=="1") echo "selected"; ?>><?php echo ADMINEU_EXPTYPE2; ?></option>
                        </select>  
                    </div>
                  </div> 

                  <div id="userdefineddiv">

                  <div class="form-group">
                    <div class="col-xs-12">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-control" name="selectall" id="selectall" value="1">&nbsp;&nbsp;<?php echo ADMINBUTTON_SELECTALL; ?>
                        </label>
                      </div>
                    </div>
                  </div>    

                  <div class="form-group" id="exportfieldsgroup">

                      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="created" id="created" value="1" <?php if (in_array("CR",$exportfieldsarray)) echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINFIELD_CREATED; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="username" id="username" value="1" <?php if (in_array("US",$exportfieldsarray)) echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINFIELD_USERNAME; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="pss" id="pss" value="1" <?php if (in_array("PW",$exportfieldsarray)) echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINFIELD_PASSWORD; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="enabled" id="enabled" value="1" <?php if (in_array("EN",$exportfieldsarray)) echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINFIELD_ENABLED; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="name" id="name" value="1" <?php if (in_array("NM",$exportfieldsarray)) echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINFIELD_NAME; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="email" id="email" value="1" <?php if (in_array("EM",$exportfieldsarray)) echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINFIELD_EMAIL; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="usergroups" id="usergroups" value="1" <?php if (in_array("UG",$exportfieldsarray)) echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINFIELD_USERGROUPS; ?>
                          </label>
                        </div>
                      </div>

                      <?php for ($k=1; $k<=50; $k++)
                      {
                        if ($k<10)
                          $val="0".$k;
                        else
                          $val=$k;
                        $val=(string)$val;
                      ?>
                      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="custom<?php echo $k; ?>" id="custom<?php echo $k; ?>" value="1" <?php if (in_array($val,$exportfieldsarray)) echo "checked "; ?>>&nbsp;&nbsp;<?php echo "Custom ".$k; ?>
                          </label>
                        </div>
                      </div>
                      <?php } ?>  

                  </div>

                  </div> <!--  userdefineddiv  -->




                </div><!-- /.box-body -->
              </div><!-- /.box -->

               <div class="form-group">
                <div class="col-xs-12">
                    <div class="btn-toolbar">
                        <button type="submit" id="submit" class="btn btn-primary pull-left"><?php echo ADMINMENU_EXPORTSELECTED; ?></button>
                        <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                    </div>   
                </div>
              </div>
                

            <div id="resultexportuser"></div>



            </div><!-- /.col -->


          </form>


          </div><!-- /.row -->


<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("adminthemefooter.php"); ?>
    <script src="exportusers.js"></script>

  </body>
</html>
