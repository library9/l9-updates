$('.selectpicker').selectpicker({
  style: 'btn-default',
  size: 'auto',
  dropupAuto: false
});

// Enable calendar
$('#fromdatecal').datetimepicker({'keepInvalid': true, 'format':DateFormat,'defaultDate':'', 'useStrict': true});
$('#todatecal').datetimepicker({'keepInvalid': true, 'format':DateFormat,'defaultDate':'', 'useStrict': true});

// Setup selectall checkbox
$('#selectall').on('ifChecked', function(event){
               selectAll();
});
$('#selectall').on('ifUnchecked', function(event){
               selectAll();
});


function selectAll()
{
  formname="logform";
  if ($('#selectall').iCheck('update')[0].checked)
    var toggle="check";
  else  
    var toggle="uncheck";
  var checkboxes = new Array(); 
  checkboxes = document[formname].getElementsByTagName('input');
  for (var i=0; i<checkboxes.length; i++) 
  {
    if ((checkboxes[i].type == 'checkbox') && (!checkboxes[i].disabled))
    {
      if (checkboxes[i].id.substr(0,8)=='logentry')
      {
        $('#'+checkboxes[i].id).iCheck(toggle);
      }
    }
  }
}

function exportLog()
{
  // process the form
  // remove the error classes
  $('#fromdatediv').removeClass('has-error');
  $('#todatediv').removeClass('has-error');
  $('#logentrydiv').removeClass('has-error');
   // remove the error messages
  $('#fromdatehelp').remove();
  $('#todatehelp').remove();
  $('#logentryhelp').remove();
  //Remove form message
  $('#resultlog').html('');
  // Validate using JS first
  // Username
  var data={success:true,message:'',errors:{}};
  var fromdate=$("#fromdate").val().trim();
  if ((fromdate!="") && (!dateValid(fromdate,DateFormat)))
    data.errors.fromdate=ADMINLG_DATENV;
  var todate=$("#todate").val().trim();
  if ((todate!="") && (!dateValid(todate,DateFormat)))
    data.errors.todate=ADMINLG_DATENV
  var atleastoneentry=false;
  if ($('#logentry1').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry2').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry3').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry4').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry5').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry6').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry7').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry8').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry9').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry10').iCheck('update')[0].checked) atleastoneentry=true;
  for (k=11;k<=50;k++)
  {
    if ($('#logentry'+k).is(':checkbox'))
    {
      if ($('#logentry'+k).iCheck('update')[0].checked) atleastoneentry=true;
    }
  }  
  if (!atleastoneentry)
    data.errors.logentry=ADMINLG_ONEENTRY;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message=ADMINMSG_FORMERROR;
    //data=JSON.stringify(data);
    // Show validation results from JS in form
    showValidation(data,'resultlog');
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
    return;
  }
  $('#browsertimeoffset').val(adminLocalTzOffset); 
  $("#logform").prop("target", "");
  $("#logform").prop("action", "logmanage.php");
  $('#logmanageact').val("export"); 
  $("#logform").submit();
}

function viewLog()
{
  // process the form
  // remove the error classes
  $('#fromdatediv').removeClass('has-error');
  $('#todatediv').removeClass('has-error');
  $('#logentrydiv').removeClass('has-error');
   // remove the error messages
  $('#fromdatehelp').remove();
  $('#todatehelp').remove();
  $('#logentryhelp').remove();
  //Remove form message
  $('#resultlog').html('');
  // Validate using JS first
  // Username
  var data={success:true,message:'',errors:{}};
  var fromdate=$("#fromdate").val().trim();
  if ((fromdate!="") && (!dateValid(fromdate,DateFormat)))
    data.errors.fromdate=ADMINLG_DATENV;
  var todate=$("#todate").val().trim();
  if ((todate!="") && (!dateValid(todate,DateFormat)))
    data.errors.todate=ADMINLG_DATENV
  var atleastoneentry=false;
  if ($('#logentry1').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry2').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry3').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry4').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry5').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry6').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry7').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry8').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry9').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry10').iCheck('update')[0].checked) atleastoneentry=true;
  for (k=11;k<=50;k++)
  {
    if ($('#logentry'+k).is(':checkbox'))
    {
      if ($('#logentry'+k).iCheck('update')[0].checked) atleastoneentry=true;
    }
  }  
  if (!atleastoneentry)
    data.errors.logentry=ADMINLG_ONEENTRY;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message=ADMINMSG_FORMERROR;
    //data=JSON.stringify(data);
    // Show validation results from JS in form
    showValidation(data,'resultlog');
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
    return;
  }
  $('#browsertimeoffset').val(adminLocalTzOffset); 
  $("#logform").prop("target", "_blank");
  $("#logform").prop("action", "logmanage.php");
  $('#logmanageact').val("view"); 
  $("#logform").submit();
}

function clearLog()
{
  bootbox.confirm(ADMINLG_VERDEL, function(result) {
      if (result)
      {
        clearLogProcess();
      } 
});
}

function clearLogProcess()
{
  // process the form
  // remove the error classes
  $('#fromdatediv').removeClass('has-error');
  $('#todatediv').removeClass('has-error');
  $('#logentrydiv').removeClass('has-error');
   // remove the error messages
  $('#fromdatehelp').remove();
  $('#todatehelp').remove();
  $('#logentryhelp').remove();
  //Remove form message
  $('#resultlog').html('');
  // Validate using JS first
  // Username
  var data={success:true,message:'',errors:{}};
  var fromdate=$("#fromdate").val().trim();
  if ((fromdate!="") && (!dateValid(fromdate,DateFormat)))
    data.errors.fromdate=ADMINLG_DATENV;
  var todate=$("#todate").val().trim();
  if ((todate!="") && (!dateValid(todate,DateFormat)))
    data.errors.todate=ADMINLG_DATENV
  var atleastoneentry=false;
  if ($('#logentry1').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry2').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry3').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry4').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry5').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry6').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry7').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry8').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry9').iCheck('update')[0].checked) atleastoneentry=true;
  if ($('#logentry10').iCheck('update')[0].checked) atleastoneentry=true;
  for (k=11;k<=50;k++)
  {
    if ($('#logentry'+k).is(':checkbox'))
    {
      if ($('#logentry'+k).iCheck('update')[0].checked) atleastoneentry=true;
    }
  }  
  if (!atleastoneentry)
    data.errors.logentry=ADMINLG_ONEENTRY;
  if (Object.keys(data.errors).length>0)
  {
    data.success=false;
    data.message=ADMINMSG_FORMERROR;
    //data=JSON.stringify(data);
    // Show validation results from JS in form
    showValidation(data,'resultlog');
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
    return;
  }
  var label=showButtonBusy('delete');
  var formData = $('#logform').serialize();
  //var formData = {
  //    'text1'              : $('#text1').val(),
  //    'hpos1'             : $('#hpos1').val(),
  //};  
  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'adminclearlog.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      // using the done promise callback
      .done(function(data) {
        sessionExpiredReported(data,true);
        // Show validation results from PHP in form
        showValidation(data,'resultlog');
        hideButtonBusy('delete',label);
        if (data.success)
        {
        }
      })

// using the fail promise callback
.fail(function(data) {
hideButtonBusy('delete',label);  
$('#resultlog').html('<div id="resultlogmessage" class="alert alert-danger">'+ADMINLG_NODELETE+'</div>');
//        console.log(data);
});


  // stop the form from submitting the normal way and refreshing the page
  event.preventDefault();
}

