<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  require_once("slupdateform.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  include("admincommonfunctions.php");
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    die(ADMINMSG_MYSQLERROR);
    exit;
  }  
  $id=$_GET['actid'];
?>
<!DOCTYPE html>
<html>
<head>
<?php
$pagename="updateformcode";
include("adminhead.php");
?>
<title><?php echo ADMINMENU_PROFILEFORMCODE; ?></title>
<link rel="stylesheet" href="formcode.css"></head>
<?php include("adminthemeheader.php"); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-list-alt"></i>&nbsp;<?php echo ADMINMENU_PROFILEFORMCODE; ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><a href="updateforms.php"><?php echo ADMINMENU_PROFILEFORMS; ?></a></li>
            <li class="active"><?php echo ADMINMENU_PROFILEFORMCODE; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>
          <!-- Your Page Content Here -->
          <div class="row">

            <div class="col-xs-12">
<form class="form-horizontal">          
              <div class="box">

                <div class="box-body">

<h3><?php echo ADMINCG_EMBEDTYP; ?></h3>
<div class="form-group">
    <div class="col-xs-12">
      <div class="radio">
        <label>
          <p class="hanging-indent"><input type="radio" name="embedmethod" id="embed" value="embed" checked="checked">&nbsp;&nbsp;<?php echo ADMINCG_EMBED; ?></p>
        </label>
      </div>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12">
      <div class="radio">
        <label>
          <p class="hanging-indent"><input type="radio" name="embedmethod" id="source" value="source">&nbsp;&nbsp;<?php echo ADMINCG_SOURCE; ?></p>
        </label>
      </div>
    </div>
</div>

<h3><?php echo ADMINCG_STYLETYP; ?></h3>
<div class="form-group">
    <div class="col-xs-12">
      <div class="radio">
        <label>
          <p class="hanging-indent"><input type="radio" name="styletype" id="style" value="style" checked="checked">&nbsp;&nbsp;<?php echo ADMINCG_FORMSTYLE; ?></p>
        </label>
      </div>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12">
      <div class="radio">
        <label>
          <p class="hanging-indent"><input type="radio" name="styletype" id="simple" value="simple">&nbsp;&nbsp;<?php echo ADMINCG_SIMPLESTYLE; ?></p>
        </label>
      </div>
    </div>
</div>




<div id="sourcecodenormal">
  <?php
  sl_get_update_formcode($mysql_link,$id,true,false,$css,$js,$html);
  $prefix ="<?php\n";
  $prefix.="\$groupswithaccess=\"ALL\";\n";
  $prefix.="require_once(\"".$SitelokLocation."sitelokpw.php\");\n";
  $prefix.="?>";  
  $prefix = str_replace("&", "&amp;",$prefix);
  $prefix = str_replace("<", "&lt;",$prefix);
  $css = str_replace("&", "&amp;",$css);
  $css = str_replace("<", "&lt;",$css);
  $js = str_replace("&", "&amp;",$js);
  $js = str_replace("<", "&lt;",$js);
  $html = str_replace("&", "&amp;",$html);
  $html = str_replace("<", "&lt;",$html);
  ?>
  <h3><?php echo ADMINCG_STEP1; ?></h3>
  <p><?php echo ADMINCG_PRECODE; ?></p>
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="5" class="form-control" readonly name="prefixcodesourcestyle" id="prefixcodesourcestyle"><?php echo $prefix; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('prefixcodesourcestyle').focus(); document.getElementById('prefixcodesourcestyle').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
  <h3><?php echo ADMINCG_STEP2; ?></h3>
  <p><?php echo ADMINCG_HEADCODE; ?></p>
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="16" class="form-control" readonly name="headcodesourcestyle" id="headcodesourcestyle"><?php echo $css."\n\n".$js; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('headcodesourcestyle').focus(); document.getElementById('headcodesourcestyle').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
  <h3><?php echo ADMINCG_STEP3; ?></h3>
  <p><?php echo ADMINCG_BODYCODE; ?></p>
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="16" class="form-control" readonly name="bodycodesourcestyle" id="bodycodesourcestyle"><?php echo $html; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('bodycodesourcestyle').focus(); document.getElementById('bodycodesourcestyle').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>

<div id="sourcecodesimple">
  <?php
  sl_get_update_formcode($mysql_link,$id,true,true,$css,$js,$html);
  $prefix ="<?php\n";
  $prefix.="\$groupswithaccess=\"ALL\";\n";
  $prefix.="require_once(\"".$SitelokLocation."sitelokpw.php\");\n";
  $prefix.="?>";  
  $prefix = str_replace("&", "&amp;",$prefix);
  $prefix = str_replace("<", "&lt;",$prefix);
  $css = str_replace("&", "&amp;",$css);
  $css = str_replace("<", "&lt;",$css);
  $js = str_replace("&", "&amp;",$js);
  $js = str_replace("<", "&lt;",$js);
  $html = str_replace("&", "&amp;",$html);
  $html = str_replace("<", "&lt;",$html);
  ?>
  <h3><?php echo ADMINCG_STEP1; ?></h3>
  <p><?php echo ADMINCG_PRECODE; ?></p>
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="5" class="form-control" readonly name="prefixcodesourcesimple" id="prefixcodesourcesimple"><?php echo $prefix; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('prefixcodesourcesimple').focus(); document.getElementById('prefixcodesourcesimple').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
  <h3><?php echo ADMINCG_STEP2; ?></h3>
  <p><?php echo ADMINCG_HEADCODE; ?></p>
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="16" class="form-control" readonly name="headcodesourcesimple" id="headcodesourcesimple"><?php echo $css."\n\n".$js; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('headcodesourcesimple').focus(); document.getElementById('headcodesourcesimple').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
  <h3><?php echo ADMINCG_STEP3; ?></h3>
  <p><?php echo ADMINCG_BODYCODE; ?></p>
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="16" class="form-control" readonly name="bodycodesourcesimple" id="bodycodesourcesimple"><?php echo $html; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('bodycodesourcesimple').focus(); document.getElementById('bodycodesourcesimple').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>

<div id="linkednormal">
  <?php
  $prefix ="<?php\n";
  $prefix.="\$groupswithaccess=\"ALL\";\n";
  $prefix.="require_once(\"".$SitelokLocation."sitelokpw.php\");\n";
  $prefix.="require_once(\"".$SitelokLocation."slupdateform.php\");\n";  
  $prefix.="?>";  
  $prefix = str_replace("&", "&amp;",$prefix);
  $prefix = str_replace("<", "&lt;",$prefix);
  $head = "<?php sl_updateformhead({$id},false); ?>";
  $head = sl_rwadjust($head);
  $head = str_replace("&", "&amp;",$head);
  $head = str_replace("<", "&lt;",$head);
  $body = "<?php sl_updateformbody({$id},false); ?>";
  $body = sl_rwadjust($body);
  $body = str_replace("&", "&amp;",$body);
  $body = str_replace("<", "&lt;",$body);
  ?>
  <h3><?php echo ADMINCG_STEP1; ?></h3>
  <p><?php echo ADMINCG_PRECODE; ?></p>
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea  rows="5" class="form-control" readonly name="prefixcodeembedstyle" id="prefixcodeembedstyle"><?php echo $prefix; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('prefixcodeembedstyle').focus(); document.getElementById('prefixcodeembedstyle').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
  <h3><?php echo ADMINCG_STEP2; ?></h3>
  <p><?php echo ADMINCG_HEADCODE; ?></p>
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="4" class="form-control" readonly name="headcodeembedstyle" id="headcodeembedstyle"><?php echo $head; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('headcodeembedstyle').focus(); document.getElementById('headcodeembedstyle').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
  <h3><?php echo ADMINCG_STEP3; ?></h3>
  <p><?php echo ADMINCG_BODYCODE; ?></p>
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="4" class="form-control" readonly name="bodycodeembedstyle" id="bodycodeembedstyle"><?php echo $body; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('bodycodeembedstyle').focus(); document.getElementById('bodycodeembedstyle').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>

<div id="linkedsimple">
  <?php
  $prefix ="<?php\n";
  $prefix.="\$groupswithaccess=\"ALL\";\n";
  $prefix.="require_once(\"".$SitelokLocation."sitelokpw.php\");\n";
  $prefix.="require_once(\"".$SitelokLocation."slupdateform.php\");\n";  
  $prefix.="?>";  
  $prefix = str_replace("&", "&amp;",$prefix);
  $prefix = str_replace("<", "&lt;",$prefix);
  $head = "<?php sl_updateformhead({$id},true); ?>";
  $head = sl_rwadjust($head);  
  $head = str_replace("&", "&amp;",$head);
  $head = str_replace("<", "&lt;",$head);
  $body = "<?php sl_updateformbody({$id},true); ?>";
  $body = sl_rwadjust($body);
  $body = str_replace("&", "&amp;",$body);
  $body = str_replace("<", "&lt;",$body);
  ?>
  <h3><?php echo ADMINCG_STEP1; ?></h3>
  <p><?php echo ADMINCG_PRECODE; ?></p>
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="5" class="form-control" readonly name="prefixcodeembedsimple" id="prefixcodeembedsimple"><?php echo $prefix; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('prefixcodeembedsimple').focus(); document.getElementById('prefixcodeembedsimple').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
  <h3><?php echo ADMINCG_STEP2; ?></h3>
  <p><?php echo ADMINCG_HEADCODE; ?></p>
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="4" class="form-control" readonly name="headcodeembedsimple" id="headcodeembedsimple"><?php echo $head; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('headcodeembedsimple').focus(); document.getElementById('headcodeembedsimple').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
  <h3><?php echo ADMINCG_STEP3; ?></h3>
  <p><?php echo ADMINCG_BODYCODE; ?></p>
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="4" class="form-control" readonly name="bodycodeembedsimple" id="bodycodeembedsimple"><?php echo $body; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('bodycodeembedsimple').focus(); document.getElementById('bodycodeembedsimple').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>

                </div><!-- /.box-body -->
              </div><!-- /.box -->

<div class="form-group">
  <div class="col-xs-12">
    <div class="btn-toolbar">
        <button type="button" id="cancel" class="btn btn-primary pull-left" onclick="window.location.href='updateforms.php';"><?php echo ADMINBUTTON_RETURNTOFORMS ?></button>
        <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
    </div> 
  </div>    
</div>

</form>
            </div><!-- /.col -->





          </div><!-- /.row -->

<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>


        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("adminthemefooter.php"); ?>
    <script src="updateformcode.js"></script>

  </body>
</html>
