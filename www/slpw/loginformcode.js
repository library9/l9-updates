$('#page').on('ifChecked', function(event){
               updatepage();
});

$('#template').on('ifChecked', function(event){
               updatepage();
});

$('#embed').on('ifChecked', function(event){
               updatepage();
});

$('#source').on('ifChecked', function(event){
               updatepage();
});

$('#style').on('ifChecked', function(event){
               updatepage();
});

$('#simple').on('ifChecked', function(event){
               updatepage();
});

$('#redirect').on('ifChecked', function(event){
               updatepage();
});

$('#stay').on('ifChecked', function(event){
               updatepage();
});

$('#show').on('ifChecked', function(event){
               updatepage();
});

$('#hide').on('ifChecked', function(event){
               updatepage();
});

updatepage();

function updatepage()
{
  var embed=$('#embed').iCheck('embed')[0].checked;
  var source=$('#source').iCheck('source')[0].checked;
  var style=$('#style').iCheck('style')[0].checked;
  var simple=$('#simple').iCheck('simple')[0].checked;
  var page=$('#page').iCheck('page')[0].checked;
  var template=$('#template').iCheck('template')[0].checked;
  var redirect=$('#redirect').iCheck('redirect')[0].checked;
  var stay=$('#stay').iCheck('stay')[0].checked;
  var show=$('#show').iCheck('show')[0].checked;
  var hide=$('#hide').iCheck('hide')[0].checked;

  if (template)
    document.getElementById('templatesettings').style['display']="none";
  if (page)
    document.getElementById('templatesettings').style['display']="block";    

  // Prefix
  if (embed)
  {
    if (page)
    {
      ta=document.getElementById('prefixcodepage');
      if (document.getElementById('redirect').checked)
        ta.value=ta.value.replace('loginredirect=0;', 'loginredirect=2;');
      if (document.getElementById('stay').checked)
        ta.value=ta.value.replace('loginredirect=2;', 'loginredirect=0;');
       document.getElementById('prefixpage').style['display']="block";
       document.getElementById('prefixtemplate').style['display']="none";
       document.getElementById('prefixblock').style['display']="block";           
    }
    if (template)
    {
      ta=document.getElementById('prefixcodetemplate');
      if (document.getElementById('redirect').checked)
        ta.value=ta.value.replace('loginredirect=0;', 'loginredirect=2;');
      if (document.getElementById('stay').checked)
        ta.value=ta.value.replace('loginredirect=2;', 'loginredirect=0;');
      document.getElementById('prefixpage').style['display']="none";
      document.getElementById('prefixtemplate').style['display']="block";
      document.getElementById('prefixblock').style['display']="block";       
    } 
  }
  if (source)
  {
     if (page)
     {
       document.getElementById('prefixblock').style['display']="block";       
       document.getElementById('prefixpage').style['display']="block";
       document.getElementById('prefixtemplate').style['display']="none";
     } 
     if (template)
     {
       document.getElementById('prefixpage').style['display']="none";
       document.getElementById('prefixtemplate').style['display']="none";
       document.getElementById('prefixblock').style['display']="none";       
     } 
  }
  
  // Head
  if ((template) && (source))
    document.getElementById('headstep').innerHTML=ADMINCG_STEP1;
  else  
    document.getElementById('headstep').innerHTML=ADMINCG_STEP2;
  if (embed)
  {
    if (style)
    {
      document.getElementById('headsourcenormal').style['display']="none";
      document.getElementById('headsourcesimple').style['display']="none";
      document.getElementById('headembednormal').style['display']="block";
      document.getElementById('headembedsimple').style['display']="none";
    } 
    if (simple)
    {
      document.getElementById('headsourcenormal').style['display']="none";
      document.getElementById('headsourcesimple').style['display']="none";
      document.getElementById('headembednormal').style['display']="none";
      document.getElementById('headembedsimple').style['display']="block";
    } 
  }
  if (source)
  {
    if (style)
    {
      document.getElementById('headsourcenormal').style['display']="block";
      document.getElementById('headsourcesimple').style['display']="none";
      document.getElementById('headembednormal').style['display']="none";
      document.getElementById('headembedsimple').style['display']="none";
    } 
    if (simple)
    {
      document.getElementById('headsourcenormal').style['display']="none";
      document.getElementById('headsourcesimple').style['display']="block";
      document.getElementById('headembednormal').style['display']="none";
      document.getElementById('headembedsimple').style['display']="none";
    } 
  }
  
  // Body
  if ((template) && (source))
    document.getElementById('bodystep').innerHTML=ADMINCG_STEP2;
  else  
    document.getElementById('bodystep').innerHTML=ADMINCG_STEP3;
  var ta=document.getElementById('prefixcodepage');
  if (document.getElementById('redirect').checked)
    ta.value=ta.value.replace('loginredirect=0;', 'loginredirect=2;');
  if (document.getElementById('stay').checked)
    ta.value=ta.value.replace('loginredirect=2;', 'loginredirect=0;');
  ta=document.getElementById('prefixcodetemplate');
  if (document.getElementById('redirect').checked)
    ta.value=ta.value.replace('loginredirect=0;', 'loginredirect=2;');
  if (document.getElementById('stay').checked)
    ta.value=ta.value.replace('loginredirect=2;', 'loginredirect=0;');
  if (embed)
  {
    if (style)
    {
      var ta=document.getElementById('bodycodeembednormal');
      ta.value=ta.value.replace('<'+'?php if ($slpublicaccess) { ?>\n', '');
      if ((document.getElementById('hide').checked) && (page))
        ta.value='<'+'?php if ($slpublicaccess) { ?>\n'+ta.value;
      ta.value=ta.value.replace('<'+'?php } // Hide ?>\n', '');
      if ((document.getElementById('hide').checked) && (page))
        ta.value=ta.value+'<'+'?php } // Hide ?>\n';
      document.getElementById('bodysourcenormal').style['display']="none";
      document.getElementById('bodysourcesimple').style['display']="none";
      document.getElementById('bodyembednormal').style['display']="block";
      document.getElementById('bodyembedsimple').style['display']="none";
    } 
    if (simple)
    {
      var ta=document.getElementById('bodycodeembedsimple');
      ta.value=ta.value.replace('<'+'?php if ($slpublicaccess) { ?>\n', '');
      if ((document.getElementById('hide').checked) && (page))
        ta.value='<'+'?php if ($slpublicaccess) { ?>\n'+ta.value;
      ta.value=ta.value.replace('<'+'?php } // Hide ?>\n', '');
      if ((document.getElementById('hide').checked) && (page))
        ta.value=ta.value+'<'+'?php } // Hide ?>\n';
      document.getElementById('bodysourcenormal').style['display']="none";
      document.getElementById('bodysourcesimple').style['display']="none";
      document.getElementById('bodyembednormal').style['display']="none";
      document.getElementById('bodyembedsimple').style['display']="block";
     } 
  }
  if (source)
  {
    if (style)
    {
      var ta=document.getElementById('bodycodesourcenormal');
      ta.value=ta.value.replace('<'+'?php if ($slpublicaccess) { ?>\n', '');
      if ((document.getElementById('hide').checked) && (page))
        ta.value='<'+'?php if ($slpublicaccess) { ?>\n'+ta.value;
      ta.value=ta.value.replace('<'+'?php } // Hide ?>\n', '');
      if ((document.getElementById('hide').checked) && (page))
        ta.value=ta.value+'<'+'?php } // Hide ?>\n';
      document.getElementById('bodysourcenormal').style['display']="block";
      document.getElementById('bodysourcesimple').style['display']="none";
      document.getElementById('bodyembednormal').style['display']="none";
      document.getElementById('bodyembedsimple').style['display']="none";
    } 
    if (simple)
    {
      var ta=document.getElementById('bodycodesourcesimple');
      ta.value=ta.value.replace('<'+'?php if ($slpublicaccess) { ?>\n', '');
      if ((document.getElementById('hide').checked) && (page))
        ta.value='<'+'?php if ($slpublicaccess) { ?>\n'+ta.value;
      ta.value=ta.value.replace('<'+'?php } // Hide ?>\n', '');
      if ((document.getElementById('hide').checked) && (page))
        ta.value=ta.value+'<'+'?php } // Hide ?>\n';
      document.getElementById('bodysourcenormal').style['display']="none";
      document.getElementById('bodysourcesimple').style['display']="block";
      document.getElementById('bodyembednormal').style['display']="none";
      document.getElementById('bodyembedsimple').style['display']="none";
    } 
  }
}
