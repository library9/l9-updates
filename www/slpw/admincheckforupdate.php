<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  $dbupdate=true;  
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value  
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError(ADMINMSG_CSRFFAILED);
    exit;
  }
  $str = url_get_contents('http://www.vibralogix.com/appdata/slversion.php');
  if ($str===false)
  {
    returnError(ADMINGENERAL_CANTUPD);
    exit;
  }
  $updateinfo = json_decode($str, true);
  returnSuccess("",$updateinfo['version'],$updateinfo['link']);
  exit;

  function returnError($msg)
  {
    $data['success'] = false;
    $data['message'] = $msg;
    $data['version'] = "";
    $data['link'] = "";    
    echo json_encode($data);
    exit;
  }

  function returnSuccess($msg,$version,$link)
  {
    $data['success'] = true;
    $data['message'] = $msg;
    $data['version'] = $version;
    $data['link'] = $link;    
    echo json_encode($data);
    exit;
  }

  function url_get_contents($Url)
  {
      if (!function_exists('curl_init'))
      { 
          return(false);
      }
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $Url);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $output = curl_exec($ch);
      curl_close($ch);
      return $output;
  }  

  ?>
{
  "success": true,
  "message": "",
  "versioncheckfailed": <?php echo $versioncheckfailed; ?>,
  "plugincount": <?php echo $numplugins; ?>,
  "pluginids": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$pluginids[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "names": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$names[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "icons": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$icons[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "enabled": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$enabled[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "versions": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$versions[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "latestversions": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$latestversions[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "latestminslversion": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$latestminslversion[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "latestversionlinks": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$latestversionlinks[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "configs": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$configs[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "pluginindexes": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$pluginindexes[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "v5s": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo ($v5s[$k]?'true':'false'); if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ],
  "folders": [
<?php
  for ($k=0;$k<$numplugins;$k++)
  {
?>
      <?php echo "\"".$folders[$k]."\""; if ($k!=($numplugins-1)) echo ",";?>

<?php    
  }
?>
  ]
}


