<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  require("admincommonfunctions.php");  

  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  $errors = array();    // array to hold validation errors
  $data = array();      // array to pass back data 
  $emailsenderror=false; // email send error is handled separately
  // Check CSRF value
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_CSRFFAILED.".");
    exit;
  }   
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_MYSQLERROR.".");
    exit;
  }
  $groupcount=$_POST['groupcount']; // This isn't number of groups but maximum group number. e.g. 23 for group23.
  $user=$_POST['user'];
  $pass=$_POST['pss'];
  $nm=$_POST['nm'];
  $em=$_POST['em'];
  $en=$_POST['en'];
  if ($en=="")
    $en="No";    
  $cu1=$_POST['cu1'];
  $cu2=$_POST['cu2'];
  $cu3=$_POST['cu3'];
  $cu4=$_POST['cu4'];
  $cu5=$_POST['cu5'];
  $cu6=$_POST['cu6'];
  $cu7=$_POST['cu7'];
  $cu8=$_POST['cu8'];
  $cu9=$_POST['cu9'];
  $cu10=$_POST['cu10']; 
  $cu11=$_POST['cu11'];
  $cu12=$_POST['cu12'];
  $cu13=$_POST['cu13'];
  $cu14=$_POST['cu14'];
  $cu15=$_POST['cu15'];
  $cu16=$_POST['cu16'];
  $cu17=$_POST['cu17'];
  $cu18=$_POST['cu18'];
  $cu19=$_POST['cu19'];
  $cu20=$_POST['cu20'];
  $cu21=$_POST['cu21'];
  $cu22=$_POST['cu22'];
  $cu23=$_POST['cu23'];
  $cu24=$_POST['cu24'];
  $cu25=$_POST['cu25'];
  $cu26=$_POST['cu26'];
  $cu27=$_POST['cu27'];
  $cu28=$_POST['cu28'];
  $cu29=$_POST['cu29'];
  $cu30=$_POST['cu30'];
  $cu31=$_POST['cu31'];
  $cu32=$_POST['cu32'];
  $cu33=$_POST['cu33'];
  $cu34=$_POST['cu34'];
  $cu35=$_POST['cu35'];
  $cu36=$_POST['cu36'];
  $cu37=$_POST['cu37'];
  $cu38=$_POST['cu38'];
  $cu39=$_POST['cu39'];
  $cu40=$_POST['cu40'];
  $cu41=$_POST['cu41'];
  $cu42=$_POST['cu42'];
  $cu43=$_POST['cu43'];
  $cu44=$_POST['cu44'];
  $cu45=$_POST['cu45'];
  $cu46=$_POST['cu46'];
  $cu47=$_POST['cu47'];
  $cu48=$_POST['cu48'];
  $cu49=$_POST['cu49'];
  $cu50=$_POST['cu50'];
  $template=$_POST['template'];
  $sendemail=$_POST['sendemail'];
  if ($sendemail!=1)
    $sendemail=0;
  $existingsendemail=$_POST['existingsendemail'];
  if ($existingsendemail!=1)
    $existingsendemail=0;

  if (get_magic_quotes_gpc())
  {
    $newuser=stripslashes($newuser);
    $nm=stripslashes($nm);
    $em=stripslashes($em);      
    $cu1=stripslashes($cu1);
    $cu2=stripslashes($cu2);
    $cu3=stripslashes($cu3);
    $cu4=stripslashes($cu4);
    $cu5=stripslashes($cu5);
    $cu6=stripslashes($cu6);
    $cu7=stripslashes($cu7);
    $cu8=stripslashes($cu8);
    $cu9=stripslashes($cu9);
    $cu10=stripslashes($cu10);      
    $cu11=stripslashes($cu11);
    $cu12=stripslashes($cu12);
    $cu13=stripslashes($cu13);
    $cu14=stripslashes($cu14);
    $cu15=stripslashes($cu15);
    $cu16=stripslashes($cu16);
    $cu17=stripslashes($cu17);
    $cu18=stripslashes($cu18);
    $cu19=stripslashes($cu19);
    $cu20=stripslashes($cu20);      
    $cu21=stripslashes($cu21);
    $cu22=stripslashes($cu22);
    $cu23=stripslashes($cu23);
    $cu24=stripslashes($cu24);
    $cu25=stripslashes($cu25);
    $cu26=stripslashes($cu26);
    $cu27=stripslashes($cu27);
    $cu28=stripslashes($cu28);
    $cu29=stripslashes($cu29);
    $cu30=stripslashes($cu30);      
    $cu31=stripslashes($cu31);
    $cu32=stripslashes($cu32);
    $cu33=stripslashes($cu33);
    $cu34=stripslashes($cu34);
    $cu35=stripslashes($cu35);
    $cu36=stripslashes($cu36);
    $cu37=stripslashes($cu37);
    $cu38=stripslashes($cu38);
    $cu39=stripslashes($cu39);
    $cu40=stripslashes($cu40);      
    $cu41=stripslashes($cu41);
    $cu42=stripslashes($cu42);
    $cu43=stripslashes($cu43);
    $cu44=stripslashes($cu44);
    $cu45=stripslashes($cu45);
    $cu46=stripslashes($cu46);
    $cu47=stripslashes($cu47);
    $cu48=stripslashes($cu48);
    $cu49=stripslashes($cu49);
    $cu50=stripslashes($cu50);      
    $template=stripslashes($template);      
  }
  // Get usergroups. These can be group1 group3 etc with gaps.
  for ($k=1;$k<=$groupcount;$k++)
  {
    if ((isset($_POST['group'.$k])) || (isset($_POST['expiry'.$k])))
    {
      $var="group".$k;
      $$var=$_POST['group'.$k];
      $var="expiry".$k;
      $$var=$_POST['expiry'.$k];
    }
  }
  // Validate username
  $usererror="";
  if ($user=="")
    $usererror = ADMINMSG_ENTERUSER;
  // Call plugin and eventhandler validation function
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if (($usererror=="") && (function_exists($slplugin_event_onUsernameValidate[$p])))
      $usererror=call_user_func($slplugin_event_onUsernameValidate[$p],$slpluginid[$p],$user,2);
  }
  if ($usererror=="")
  {
    if (function_exists("sl_onUsernameValidate"))
      $usererror=sl_onUsernameValidate($user,2);
  }    
  // check username doesn't contain invalid characters
  if (($usererror=="") && (strspn($user, $ValidUsernameChars) != strlen($user)))
    $usererror=ADMINMSG_USERINVALID;
  if ($usererror!="")
    $errors['user']=$usererror;

  // Validate password
  $passerror="";
  if ((!$MD5passwords) && ($passerror=="") && ($pass==""))
    $passerror=ADMINMSG_PASSSHORT;
  if ($pass!="")
  {        
    // Call plugin and eventhandler validation function
    for ($p=0;$p<$slnumplugins;$p++)
    {
      if (($passerror=="") && (function_exists($slplugin_event_onPasswordValidate[$p])))
        $passerror=call_user_func($slplugin_event_onPasswordValidate[$p],$slpluginid[$p],$pass,2);
    }
    if ($passerror=="")
    {
      if (function_exists("sl_onPasswordValidate"))
        $passerror=sl_onPasswordValidate($pass,2);
    }
  }
  if (($MD5passwords) && ($pass!=""))
  {
    // check password is at least 5 characters long
    if (($passerror=="") && (strlen($pass)<5))
      $passerror=ADMINMSG_PASSSHORT; 
    // Check for valid characters
    if (($passerror=="") && (strspn($pass, $ValidPasswordChars) != strlen($pass)))
      $passerror=ADMINMSG_PASSINVALID;
  }
  // check password is at least 5 characters long
  if (($passerror=="") && (strlen($pass)<5))
    $passerror=ADMINMSG_PASSSHORT; 
  // Check for valid characters
  if (($passerror=="") && (strspn($pass, $ValidPasswordChars) != strlen($pass)))
    $passerror=ADMINMSG_PASSINVALID;
  if ($passerror!="")
    $errors['pss']=$passerror;

  // Validate name
  $nmerror="";
  if (($nmerror=="") && ($nm==""))
    $nmerror=ADMINMSG_ENTERNAME;     
  // Call plugin and eventhandler validation function
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if (($nmerror=="") && (function_exists($slplugin_event_onNameValidate[$p])))
      $nmerror=call_user_func($slplugin_event_onNameValidate[$p],$slpluginid[$p],$nm,2);
  }
  if ($nmerror=="")
  {
    if (function_exists("sl_onNameValidate"))
    $nmerror=sl_onNameValidate($nm,2);
  }
  if ($nmerror!="")
    $errors['nm']=$nmerror;

  // Validate email field
  $emerror="";
  if (($emerror=="") && ($em==""))
    $emerror=ADMINMSG_ENTEREMAIL;     
  // Check email is in valid format
  if (($emerror=="") && (!sl_validate_email($em)))
    $emerror=ADMINMSG_EMAILINVALID;    
  // Check if email address already used (by a different user if required                        
  if (($emerror=="") && ($EmailUnique==2))
  {
    $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." WHERE ".$EmailField."=".sl_quote_smart($em)." AND ".$UsernameField."!=".sl_quote_smart($user));
    if ($mysql_result===false)
    {
      returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_MYSQLERROR.".");
      exit;
    }
    $num = mysqli_num_rows($mysql_result);
    if ($num>0)
      $emerror=ADMINMSG_EMAILINUSE;
  }
  // Call plugin and eventhandler validation function
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if (($emerror=="") && (function_exists($slplugin_event_onEmailValidate[$p])))
      $emerror=call_user_func($slplugin_event_onEmailValidate[$p],$slpluginid[$p],$em,2);
  }
  if ($emerror=="")
  {
    if (function_exists("sl_onEmailValidate"))
      $emerror=sl_onEmailValidate($em,2);
  }
  if ($emerror!="")
    $errors['em']=$emerror;

  // Validate custom fields if required
  for ($k=1;$k<51;$k++)
  {
    $customerror="";
    $cusvar="cu".$k;
    $cusvar2="Custom".$k."Validate";
    $cusvar3="CustomTitle".$k;
    $cusvar4="sl_onCustom".$k."Validate";
    $cusvar5="slplugin_event_onCustom".$k."Validate";
    // Validate for plugins  
    for ($p=0;$p<$slnumplugins;$p++)   
    {
      if (($customerror=="") && (function_exists(${$cusvar5}[$p])))
      {
        $customerror=call_user_func(${$cusvar5}[$p],$slpluginid[$p],$$cusvar,$$cusvar3,2);
      }
    }
    // Validate using eventhandlers
    if (($customerror=="") && (($$cusvar2==2) || ($$cusvar2==3)))
    {
      $customerror=call_user_func($cusvar4,$$cusvar,$$cusvar3,2);
    }
    if ($customerror!="")
      $errors[$cusvar]=$customerror;
  }
  // Validate usergroups and expiries
  for ($k=1;$k<=$groupcount;$k++)
  {
    $gvar="group".$k;
    $evar="expiry".$k;
    if ((!isset($$gvar)) && (!isset($$evar)))
      continue;
    $grouperror="";
    $expiryerror="";
    if (($$gvar!="") && (!validUsergroup($$gvar,true)))
      $grouperror=ADMINMSG_GROUPINVALID;
    if (($grouperror=="") && ($slsubadmin) && (trim(strtoupper($$gvar))=="ADMIN"))
      $grouperror=ADMINMSG_ADMINNOTALLOWED;
    if (($$evar!="") && (strspn($$evar, "0123456789") != strlen($$evar)))
      $expiryerror=ADMINMSG_EXPIRYINVALID;
    if (strlen($$evar)==6)
    {
      if (!dateValid($$evar,$DateFormat))
        $expiryerror=ADMINMSG_EXPIRYDATEINVALID;
    }
    if (($$gvar=="") && ($$evar!=""))
      $grouperror=ADMINMSG_GROUPINVALID;      
    if ($grouperror!="")
      $errors[$gvar]=$grouperror;
    if ($expiryerror!="")
      $errors[$evar]=$expiryerror;
  }

  // If sending email check email template has correct extension and exists
  if ($sendemail=="1")
  {
    $ext = strtolower(pathinfo($template, PATHINFO_EXTENSION));
    if (($template!="") && ($ext!="html") && ($ext!="htm") && ($ext!="txt"))
      $errors['sendemail']=ADMINMSG_TEMPLATETYPE;      
    else
    {
      if (($template!="") && ($sendemail=="1")) 
      {
        if (!sl_ReadEmailTemplate($template,$subject,$mailBody,$htmlformat))
          $errors['sendemail']=ADMINMSG_TEMPLATENOLOAD;      
      }
    }
    // Double check path doesn't start with / or \ or contain .. or :
    if ((substr($template,0,1)=="/") || (substr($template,0,1)=="\\") || (false!=(strpos($template, ".."))) || (false!=(strpos($template, ":"))))
      $errors['sendemail']=ADMINMSG_TEMPLATENOLOAD;
  }
  // Concatenate groups and expiry times in table format
  $ug="";
  for ($k=1;$k<=$groupcount;$k++)
  {
    $pvar1="group".$k;
    $pvar2="expiry".$k;
    if (!isset($$pvar1))
      continue;
    if ($$pvar1!="")
    {
      if ($ug!="")
        $ug.="^";
      $ug.=$$pvar1;  
      if ($$pvar2!="")
      {
        // If expiry is number of days then convert to date
        if (strlen($$pvar2)<6)
        {
          if ($DateFormat=="DDMMYY")
            $expirystr=gmdate("dmy",time()+$$pvar2*86400);
          if ($DateFormat=="MMDDYY")
            $expirystr=gmdate("mdy",time()+$$pvar2*86400);            
        }
        else
          $expirystr=$$pvar2;
        $ug.=":";
        $ug.=$expirystr;
      }         
    }
  }
  if (!empty($errors))
  {
    returnError($data,$errors,ADMINMSG_FORMERROR);
    exit;
  }
 
  // Give last chance to plugins and event handler to block changes
  $paramdata['username']=$user;
  $paramdata['userid']="";   // Not set yet        
  $paramdata['password']=$pass;
  $paramdata['enabled']=$en;
  $paramdata['name']=$nm;
  $paramdata['email']=$em;
  $paramdata['usergroups']=$ug;
  $paramdata['custom1']=$cu1;
  $paramdata['custom2']=$cu2;
  $paramdata['custom3']=$cu3;
  $paramdata['custom4']=$cu4;
  $paramdata['custom5']=$cu5;
  $paramdata['custom6']=$cu6;
  $paramdata['custom7']=$cu7;
  $paramdata['custom8']=$cu8;
  $paramdata['custom9']=$cu9;
  $paramdata['custom10']=$cu10;
  $paramdata['custom11']=$cu11;
  $paramdata['custom12']=$cu12;
  $paramdata['custom13']=$cu13;
  $paramdata['custom14']=$cu14;
  $paramdata['custom15']=$cu15;
  $paramdata['custom16']=$cu16;
  $paramdata['custom17']=$cu17;
  $paramdata['custom18']=$cu18;
  $paramdata['custom19']=$cu19;
  $paramdata['custom20']=$cu20;
  $paramdata['custom21']=$cu21;
  $paramdata['custom22']=$cu22;
  $paramdata['custom23']=$cu23;
  $paramdata['custom24']=$cu24;
  $paramdata['custom25']=$cu25;
  $paramdata['custom26']=$cu26;
  $paramdata['custom27']=$cu27;
  $paramdata['custom28']=$cu28;
  $paramdata['custom29']=$cu29;
  $paramdata['custom30']=$cu30;
  $paramdata['custom31']=$cu31;
  $paramdata['custom32']=$cu32;
  $paramdata['custom33']=$cu33;
  $paramdata['custom34']=$cu34;
  $paramdata['custom35']=$cu35;
  $paramdata['custom36']=$cu36;
  $paramdata['custom37']=$cu37;
  $paramdata['custom38']=$cu38;
  $paramdata['custom39']=$cu39;
  $paramdata['custom40']=$cu40;
  $paramdata['custom41']=$cu41;
  $paramdata['custom42']=$cu42;
  $paramdata['custom43']=$cu43;
  $paramdata['custom44']=$cu44;
  $paramdata['custom45']=$cu45;
  $paramdata['custom46']=$cu46;
  $paramdata['custom47']=$cu47;
  $paramdata['custom48']=$cu48;
  $paramdata['custom49']=$cu49;
  $paramdata['custom50']=$cu50;
  $paramdata['from']=1;                
  // Call plugin event
  // Currently plugins only return a form error (not for fields)
  $addmsg="";
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if ($addmsg=="")
    {
      if (function_exists($slplugin_event_onCheckAddUser[$p]))
      {
        $res=call_user_func($slplugin_event_onCheckAddUser[$p],$slpluginid[$p],$paramdata);
        if ($res['ok']==false)
          $addmsg=$res['message'];
      } 
    }  
  }
  if ($addmsg!="")
  {
    returnError($data,$errors,$addmsg);
    exit;
  }
  // Call eventhandler
  if (function_exists("sl_onCheckAddUser"))
  {
    $res=sl_onCheckAddUser($paramdata);
    if ($res['ok']==false)
      $addmsg=$res['message'];
  }  
  if ($addmsg!="")
  {
    returnError($data,$errors,$addmsg);
    exit;
  }

  $passtowrite=$pass;
  if ($MD5passwords)
   $passtowrite=password_hash($pass,PASSWORD_DEFAULT);
  $Query="INSERT INTO ".$DbTableName." (".$SelectedField.",".$CreatedField.",".$UsernameField.",".$PasswordField.",".$EnabledField.
  ",".$NameField.",".$EmailField.",".$UsergroupsField.",".$Custom1Field.",".$Custom2Field.",".$Custom3Field.",".$Custom4Field.",".$Custom5Field.
  ",".$Custom6Field.",".$Custom7Field.",".$Custom8Field.",".$Custom9Field.",".$Custom10Field.
  ",".$Custom11Field.",".$Custom12Field.",".$Custom13Field.",".$Custom14Field.",".$Custom15Field.
  ",".$Custom16Field.",".$Custom17Field.",".$Custom18Field.",".$Custom19Field.",".$Custom20Field.
  ",".$Custom21Field.",".$Custom22Field.",".$Custom23Field.",".$Custom24Field.",".$Custom25Field.
  ",".$Custom26Field.",".$Custom27Field.",".$Custom28Field.",".$Custom29Field.",".$Custom30Field.
  ",".$Custom31Field.",".$Custom32Field.",".$Custom33Field.",".$Custom34Field.",".$Custom35Field.
  ",".$Custom36Field.",".$Custom37Field.",".$Custom38Field.",".$Custom39Field.",".$Custom40Field.
  ",".$Custom41Field.",".$Custom42Field.",".$Custom43Field.",".$Custom44Field.",".$Custom45Field.
  ",".$Custom46Field.",".$Custom47Field.",".$Custom48Field.",".$Custom49Field.",".$Custom50Field.
  ") VALUES('No','".gmdate("ymd")."',".sl_quote_smart($user).",".sl_quote_smart($passtowrite).",".sl_quote_smart($en).",".sl_quote_smart($nm).",".sl_quote_smart($em).",".sl_quote_smart($ug).",".sl_quote_smart($cu1).",".sl_quote_smart($cu2).",".sl_quote_smart($cu3).",".sl_quote_smart($cu4).",".sl_quote_smart($cu5).",".sl_quote_smart($cu6).",".sl_quote_smart($cu7).",".sl_quote_smart($cu8).",".sl_quote_smart($cu9).",".sl_quote_smart($cu10).",".
  sl_quote_smart($cu11).",".sl_quote_smart($cu12).",".sl_quote_smart($cu13).",".sl_quote_smart($cu14).",".sl_quote_smart($cu15).",".sl_quote_smart($cu16).",".sl_quote_smart($cu17).",".sl_quote_smart($cu18).",".sl_quote_smart($cu19).",".sl_quote_smart($cu20).",".
  sl_quote_smart($cu21).",".sl_quote_smart($cu22).",".sl_quote_smart($cu23).",".sl_quote_smart($cu24).",".sl_quote_smart($cu25).",".sl_quote_smart($cu26).",".sl_quote_smart($cu27).",".sl_quote_smart($cu28).",".sl_quote_smart($cu29).",".sl_quote_smart($cu30).",".
  sl_quote_smart($cu31).",".sl_quote_smart($cu32).",".sl_quote_smart($cu33).",".sl_quote_smart($cu34).",".sl_quote_smart($cu35).",".sl_quote_smart($cu36).",".sl_quote_smart($cu37).",".sl_quote_smart($cu38).",".sl_quote_smart($cu39).",".sl_quote_smart($cu40).",".
  sl_quote_smart($cu41).",".sl_quote_smart($cu42).",".sl_quote_smart($cu43).",".sl_quote_smart($cu44).",".sl_quote_smart($cu45).",".sl_quote_smart($cu46).",".sl_quote_smart($cu47).",".sl_quote_smart($cu48).",".sl_quote_smart($cu49).",".sl_quote_smart($cu50).")";
  if ($DemoMode)
    $mysql_result=true;
  else  
    $mysql_result=mysqli_query($mysql_link,$Query);
  if ($mysql_result==false)
  {
    $errors['user']=ADMINMSG_USERNAMEINUSE;
    returnError($data,$errors,ADMINMSG_FORMERROR);
    exit;
  } 
  $userid=mysqli_insert_id($mysql_link);

  if (($sendemail!=$existingsendemail) && (!$DemoMode))
  {
    $query="UPDATE sl_adminconfig SET sendnewuseremail=".sl_quote_smart($sendemail)." WHERE confignum=1";
    $mysql_result=mysqli_query($mysql_link,$query);
    $_SESSION['ses_ConfigReload']="reload";     
  }         

  if (($sendemail=="1") && ($template!=$NewUserEmail) && (!$DemoMode))
  {
    $query="UPDATE ".$DbConfigTableName." SET newuseremail=".sl_quote_smart($template)." WHERE confignum=1";
    $mysql_result=mysqli_query($mysql_link,$query);
    $ModifyUserEmail=$template;
    $_SESSION['ses_ConfigReload']="reload"; 
  }         

  if (($template!="") && ($sendemail=="1"))
  {
    if (sl_ReadEmailTemplate($template,$subject,$mailBody,$htmlformat))
    {
      $passtoemail=$pass;
      if (!sl_SendEmail($em,$mailBody,$subject,$htmlformat,$user,$passtoemail,$nm,$em,$ug,$cu1,$cu2,$cu3,$cu4,$cu5,$cu6,$cu7,$cu8,$cu9,$cu10,
      $cu11,$cu12,$cu13,$cu14,$cu15,$cu16,$cu17,$cu18,$cu19,$cu20,$cu21,$cu22,$cu23,$cu24,$cu25,$cu26,$cu27,$cu28,$cu29,$cu30,
      $cu31,$cu32,$cu33,$cu34,$cu35,$cu36,$cu37,$cu38,$cu39,$cu40,$cu41,$cu42,$cu43,$cu44,$cu45,$cu46,$cu47,$cu48,$cu49,$cu50)==1)
        $emailsenderror=true;
    }
  }
  // Event point
  $paramdata['username']=$user;
  $paramdata['userid']=$userid;        
  $paramdata['password']=$pass;
  $paramdata['enabled']=$en;
  $paramdata['name']=$nm;
  $paramdata['email']=$em;
  $paramdata['usergroups']=$ug;
  $paramdata['custom1']=$cu1;
  $paramdata['custom2']=$cu2;
  $paramdata['custom3']=$cu3;
  $paramdata['custom4']=$cu4;
  $paramdata['custom5']=$cu5;
  $paramdata['custom6']=$cu6;
  $paramdata['custom7']=$cu7;
  $paramdata['custom8']=$cu8;
  $paramdata['custom9']=$cu9;
  $paramdata['custom10']=$cu10;
  $paramdata['custom11']=$cu11;
  $paramdata['custom12']=$cu12;
  $paramdata['custom13']=$cu13;
  $paramdata['custom14']=$cu14;
  $paramdata['custom15']=$cu15;
  $paramdata['custom16']=$cu16;
  $paramdata['custom17']=$cu17;
  $paramdata['custom18']=$cu18;
  $paramdata['custom19']=$cu19;
  $paramdata['custom20']=$cu20;
  $paramdata['custom21']=$cu21;
  $paramdata['custom22']=$cu22;
  $paramdata['custom23']=$cu23;
  $paramdata['custom24']=$cu24;
  $paramdata['custom25']=$cu25;
  $paramdata['custom26']=$cu26;
  $paramdata['custom27']=$cu27;
  $paramdata['custom28']=$cu28;
  $paramdata['custom29']=$cu29;
  $paramdata['custom30']=$cu30;
  $paramdata['custom31']=$cu31;
  $paramdata['custom32']=$cu32;
  $paramdata['custom33']=$cu33;
  $paramdata['custom34']=$cu34;
  $paramdata['custom35']=$cu35;
  $paramdata['custom36']=$cu36;
  $paramdata['custom37']=$cu37;
  $paramdata['custom38']=$cu38;
  $paramdata['custom39']=$cu39;
  $paramdata['custom40']=$cu40;
  $paramdata['custom41']=$cu41;
  $paramdata['custom42']=$cu42;
  $paramdata['custom43']=$cu43;
  $paramdata['custom44']=$cu44;
  $paramdata['custom45']=$cu45;
  $paramdata['custom46']=$cu46;
  $paramdata['custom47']=$cu47;
  $paramdata['custom48']=$cu48;
  $paramdata['custom49']=$cu49;
  $paramdata['custom50']=$cu50;
  $paramdata['from']=1; 
  // Call plugin event
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if (function_exists($slplugin_event_onAddUser[$p]))
      call_user_func($slplugin_event_onAddUser[$p],$slpluginid[$p],$paramdata);
  }
  // Call eventhandler
  if (function_exists("sl_onAddUser"))
    sl_onAddUser($paramdata);
   if (!$emailsenderror)
    returnSuccess($data,ADMINMSG_ADDEDUSER);
  else
    returnSuccess($data,ADMINMSG_ADDEDUSER.". ".ADMINMSG_EMAILNOTSENT);

  function returnSuccess($data,$msg)
  {
    $data['success'] = true;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

  function returnError($data,$errors,$msg)
  {
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

?>
