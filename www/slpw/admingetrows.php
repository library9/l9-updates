<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php"); 
  if (!isset($sl_noqueryoption))
  $sl_noqueryoptio=false; 
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
  {
    $slsubadmin=true;
    $sl_noqueryoption=true;
  }
  if ($DemoMode)
    $sl_noqueryoption=true;

  // Get filter settings
  $act=$_POST['act'];

  if (isset($_SESSION['filteron'])) 
    $filteron=$_SESSION['filteron'];
 
  // If showall then clear filter session variables
  if (($act=="showall") || ($act=="clearall"))
  {
    clearFilters();
    if ($act=="clearall")
      $act="deselectall";
    else
      $act="";
    $_SESSION['filtertype']="";
  }

  // Quick search settings
  $quicksearch="";
  if (isset($_SESSION['quicksearch'])) 
    $quicksearch=$_SESSION['quicksearch'];
  if ($act=="quicksearch")
  {
    clearFilters();
    $quicksearch=$_POST['quicksearch'];
    $_SESSION['quicksearch']=$quicksearch;
    $_SESSION['filtertype']="quicksearch";
  }

  // Members of usergroup
  $memberof="";
  if (isset($_SESSION['memberof'])) 
    $memberof=$_SESSION['memberof'];
  if ($act=="memberof")
  {
    clearFilters();
    $memberof=$_POST['memberof'];
    $_SESSION['memberof']=$memberof;
    $_SESSION['filtertype']="memberof";
  }

  // Unexpired members of usergroup
  $unexpmemberof="";
  if (isset($_SESSION['unexpmemberof'])) 
    $unexpmemberof=$_SESSION['unexpmemberof'];
  if ($act=="unexpmemberof")
  {
    clearFilters();
    $unexpmemberof=$_POST['unexpmemberof'];
    $_SESSION['unexpmemberof']=$unexpmemberof;
    $_SESSION['filtertype']="unexpmemberof";
  }

  // Expired members of usergroup
  $expmemberof="";
  if (isset($_SESSION['expmemberof'])) 
    $expmemberof=$_SESSION['expmemberof'];
  if ($act=="expmemberof")
  {
    clearFilters();
    $expmemberof=$_POST['expmemberof'];
    $_SESSION['expmemberof']=$expmemberof;
    $_SESSION['filtertype']="expmemberof";
  }

  // Expired within days
  $expwithin="";
  $expwithindays="";
  if (isset($_SESSION['expwithin'])) 
    $expwithin=$_SESSION['expwithin'];
  if (isset($_SESSION['expwithindays'])) 
    $expwithindays=$_SESSION['expwithindays'];
  if ($act=="expwithin")
  {
    clearFilters();
    $expwithin=$_POST['expwithin'];
    $_SESSION['expwithin']=$expwithin;
    $expwithindays=$_POST['expwithindays'];
    $_SESSION['expwithindays']=$expwithindays;
    $_SESSION['filtertype']="expwithin";
  }

  // Joined within
  $joinwithin="";
  if (isset($_SESSION['joinwithin'])) 
    $joinwithin=$_SESSION['joinwithin'];
  if ($act=="joinwithin")
  {
    clearFilters();
    $joinwithin=$_POST['joinwithin'];
    $_SESSION['joinwithin']=$joinwithin;
    $_SESSION['filtertype']="joinwithin";
  }

  // Only Selected
  $onlyselected="";
  if (isset($_SESSION['onlyselected'])) 
    $onlyselected=$_SESSION['onlyselected'];
  if ($act=="onlyselected")
  {
    clearFilters();
    $onlyselected=$_POST['onlyselected'];
    $_SESSION['onlyselected']=$onlyselected;
    $_SESSION['filtertype']="onlyselected";
  }

  // Advanced filter
  $filfield1="";
  $filfield2="";
  $filfield3="";
  $filfield4="";
  $filcond1="";
  $filcond2="";
  $filcond3="";
  $filcond4="";
  $fildata1="";
  $fildata2="";
  $fildata3="";
  $fildata4="";
  $filbool1="";
  $filbool2="";
  $filbool3="";
  if (isset($_SESSION['filfield1'])) 
    $filfield1=$_SESSION['filfield1'];
  if (isset($_SESSION['filcond1'])) 
    $filcond1=$_SESSION['filcond1'];
  if (isset($_SESSION['fildata1'])) 
    $fildata1=$_SESSION['fildata1'];
  if (isset($_SESSION['filbool1'])) 
    $filbool1=$_SESSION['filbool1'];
  if (isset($_SESSION['filfield2'])) 
    $filfield2=$_SESSION['filfield2'];
  if (isset($_SESSION['filcond2'])) 
    $filcond2=$_SESSION['filcond2'];
  if (isset($_SESSION['fildata2'])) 
    $fildata2=$_SESSION['fildata2'];
  if (isset($_SESSION['filbool2'])) 
    $filbool2=$_SESSION['filbool2'];
  if (isset($_SESSION['filfield3'])) 
    $filfield3=$_SESSION['filfield3'];
  if (isset($_SESSION['filcond3'])) 
    $filcond3=$_SESSION['filcond3'];
  if (isset($_SESSION['fildata3'])) 
    $fildata3=$_SESSION['fildata3'];
  if (isset($_SESSION['filbool3'])) 
    $filbool3=$_SESSION['filbool3'];
  if (isset($_SESSION['filfield4'])) 
    $filfield4=$_SESSION['filfield4'];
  if (isset($_SESSION['filcond4'])) 
    $filcond4=$_SESSION['filcond4'];
  if (isset($_SESSION['fildata4'])) 
    $fildata4=$_SESSION['fildata4'];
  if ($act=="filter")
  {
    clearFilters();
    $filfield1=$_POST['filfield1'];
    $filcond1=$_POST['filcond1'];
    $fildata1=$_POST['fildata1'];
    $filbool1=$_POST['filbool1'];
    $filfield2=$_POST['filfield2'];
    $filcond2=$_POST['filcond2'];
    $fildata2=$_POST['fildata2'];
    $filbool2=$_POST['filbool2'];
    $filfield3=$_POST['filfield3'];
    $filcond3=$_POST['filcond3'];
    $fildata3=$_POST['fildata3'];
    $filbool3=$_POST['filbool3'];
    $filfield4=$_POST['filfield4'];
    $filcond4=$_POST['filcond4'];
    $fildata4=$_POST['fildata4'];
    $filteron=1;
    $_SESSION['filfield1']=$filfield1;
    $_SESSION['filcond1']=$filcond1;
    $_SESSION['fildata1']=$fildata1;
    $_SESSION['filbool1']=$filbool1;
    $_SESSION['filfield2']=$filfield2;
    $_SESSION['filcond2']=$filcond2;
    $_SESSION['fildata2']=$fildata2;
    $_SESSION['filbool2']=$filbool2;
    $_SESSION['filfield3']=$filfield3;
    $_SESSION['filcond3']=$filcond3;
    $_SESSION['fildata3']=$fildata3;
    $_SESSION['filbool3']=$filbool3;
    $_SESSION['filfield4']=$filfield4;
    $_SESSION['filcond44']=$filcond4;
    $_SESSION['fildata4']=$fildata4;
    $_SESSION['filtertype']="filter";
    $_SESSION['filteron']=1;

  }

  // SQL Input
  $sqlquery="";
  if (isset($_SESSION['sqlinput'])) 
    $sqlquery=$_SESSION['sqlinput'];
  if ($act=="query")
  {
    clearFilters();
    $sqlquery=$_POST['sqlinput'];
    $_SESSION['sqlinput']=$sqlquery;
    $_SESSION['filtertype']="query";
  }

  if ($sl_noqueryoption)
  {
    if ($act=="query")
      $act="";
    $sqlinput="";
  }  

  //determine sort field and sort direction based on table column number
  $order=$_POST['order'];
  $ordercolumn=$order[0]['column'];
  $orderdir=$order[0]['dir'];
  $sortd=strtoupper($orderdir);
  $coltag=substr($ColumnOrder,$ordercolumn*2,2);
  switch ($coltag)
  {
    case "AC":
      $sortf="";
      $sortd="";
      break;  
    case "SL":
      $sortf=$SelectedField;
      break;
    case "CR":
      $sortf=$CreatedField;
      break;
    case "US":
      $sortf=$UsernameField;
      break;
    case "PW":
      $sortf=$PasswordField;
      break;
    case "EN":
      $sortf=$EnabledField;
      break;
    case "NM":
      $sortf=$NameField;
      break;
    case "EM":
      $sortf=$EmailField;
      break;
    case "UG":
      $sortf=$UsergroupsField;
      break;
    case "ID":
      $sortf=$IdField;
      break;
    default:
      $sortf="";
      if (is_numeric($coltag))
      {
        // Use (int) to remove first 0
        $var="Custom".(int)$coltag."Field";
        $sortf=$$var;
      }  
      break;
  }

  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    ?>
    {
    "error": "002"
    }
    <?php
    exit;  
  }
  // Save if sort field or direction changed
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbConfigTableName." LIMIT 1");
  if ($mysql_result!=false)
  {
    $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
    if ($row!=false)
    {
      $SortField=$row["sortfield"];
      $SortDirection=$row["sortdirection"];
      $ShowRows=$row["showrows"];
    }
  }  
  if (($sortf!=$SortField) || ($sortd!=$SortDirection))
  {
    $query="UPDATE ".$DbConfigTableName." SET sortfield='".$sortf."', sortdirection='".$sortd."' WHERE confignum=1";
    $mysql_result=mysqli_query($mysql_link,$query);
    $_SESSION['ses_ConfigReload']="reload";
  }
  // Save if ShowRows has changed
  if (($ShowRows!=$_POST['length']) && (is_numeric($_POST['length'])))
  {
    $query="UPDATE ".$DbConfigTableName." SET showrows='".$_POST['length']."' WHERE confignum=1";
    $mysql_result=mysqli_query($mysql_link,$query);
    $ShowRows=$_POST['length'];
    $_SESSION['ses_ConfigReload']="reload";
  }
  $tablestart=$_POST['start'];
  // Store tablestart in session
  $_SESSION['ses_admin_tablestart']=$tablestart;






  if (($sortf!=$SortField) || ($sortd!=$SortDirection))
  {
    $query="UPDATE ".$DbConfigTableName." SET sortfield='".$sortf."', sortdirection='".$sortd."' WHERE confignum=1";
    $mysql_result=mysqli_query($mysql_link,$query);
  }
  // Count total number of records
  $mysql_result = mysqli_query($mysql_link,"SELECT count(*) from $DbTableName");
  $row = mysqli_fetch_row($mysql_result);
  if ($row!=false)
    $totalrows = $row[0];
  else  
    $totalrows = 0;
  // If tablestart>totalrows thenadjust
  if ($tablestart>=$totalrows)
    $tablestart=0;    
//  $sqlinput="";
  $rowsaffected=-1;
  $queryerror=false;
  $jsonmessage="";
  // If no filter or sql query then set query to show all records
  if (($filteron!="1") && ($sqlquery==""))
    $sqlquerytodo="SELECT * FROM ".$DbTableName;
  // If filtering required and groupexpiry not selected we can use SQL
  if (($filteron=="1") && ($filfield1!="groupexpiry") && ($filfield2!="groupexpiry") && ($filfield3!="groupexpiry") && ($filfield4!="groupexpiry"))
  {
    // If filtering with created date then convert user entered data to YYMMDD
    if (($filfield1==$CreatedField) && (strlen($fildata1)==6))
    {
      $enteredfildata1=$fildata1;
      if ($DateFormat=="DDMMYY")
        $fildata1=substr($fildata1,4,2).substr($fildata1,2,2).substr($fildata1,0,2);
      else
        $fildata1=substr($fildata1,2,2).substr($fildata1,4,2).substr($fildata1,0,2);
    }
    if (($filfield2==$CreatedField) && (strlen($fildata2)==6))
    {
      $enteredfildata2=$fildata2;
      if ($DateFormat=="DDMMYY")
        $fildata2=substr($fildata2,4,2).substr($fildata2,2,2).substr($fildata2,0,2);
      else
        $fildata2=substr($fildata2,2,2).substr($fildata2,4,2).substr($fildata2,0,2);
    }
    if (($filfield3==$CreatedField) && (strlen($fildata3)==6))
    {
      $enteredfildata3=$fildata3;
      if ($DateFormat=="DDMMYY")
        $fildata3=substr($fildata3,4,2).substr($fildata3,2,2).substr($fildata3,0,2);
      else
        $fildata3=substr($fildata3,2,2).substr($fildata3,4,2).substr($fildata3,0,2);
    }
    if (($filfield4==$CreatedField) && (strlen($fildata4)==6))
    {
      $enteredfildata4=$fildata4;
      if ($DateFormat=="DDMMYY")
        $fildata4=substr($fildata4,4,2).substr($fildata4,2,2).substr($fildata4,0,2);
      else
        $fildata4=substr($fildata4,2,2).substr($fildata4,4,2).substr($fildata4,0,2);
    }
    $sqlquerytodo="SELECT * FROM ".$DbTableName." WHERE ";
    if ($filcond1=="equals")
      $query1=mysqli_real_escape_string($mysql_link,$filfield1)." = ".sl_quote_smart($fildata1);
    if ($filcond1=="notequal")
      $query1=mysqli_real_escape_string($mysql_link,$filfield1)." != ".sl_quote_smart($fildata1);
    if ($filcond1=="contains")
      $query1=mysqli_real_escape_string($mysql_link,$filfield1)." LIKE ".sl_quote_smart("%".$fildata1."%");
    if ($filcond1=="notcontain")
      $query1=mysqli_real_escape_string($mysql_link,$filfield1)." NOT LIKE ".sl_quote_smart("%".$fildata1."%");
    if ($filcond1=="less")
      $query1=mysqli_real_escape_string($mysql_link,$filfield1)." < ".sl_quote_smart($fildata1);
    if ($filcond1=="greater")
      $query1=mysqli_real_escape_string($mysql_link,$filfield1)." > ".sl_quote_smart($fildata1);
    if ($filcond1=="starts")
      $query1=mysqli_real_escape_string($mysql_link,$filfield1)." LIKE ".sl_quote_smart($fildata1."%");
    if ($filcond1=="ends")
      $query1=mysqli_real_escape_string($mysql_link,$filfield1)." LIKE ".sl_quote_smart("%".$fildata1);
    if ($filcond1=="lessnum")
      $query1=mysqli_real_escape_string($mysql_link,$filfield1)." < ".mysqli_real_escape_string($mysql_link,$fildata1);
    if ($filcond1=="greaternum")
      $query1=mysqli_real_escape_string($mysql_link,$filfield1)." > ".mysqli_real_escape_string($mysql_link,$fildata1);
    $sqlquerytodo=$sqlquerytodo.$query1;
    if (($filfield2!="") && ($filfield2!="groupexpiry"))
    {
      if ($filcond2=="equals")
        $query2=mysqli_real_escape_string($mysql_link,$filfield2)." = ".sl_quote_smart($fildata2);
      if ($filcond2=="notequal")
        $query2=mysqli_real_escape_string($mysql_link,$filfield2)." != ".sl_quote_smart($fildata2);
      if ($filcond2=="contains")
        $query2=mysqli_real_escape_string($mysql_link,$filfield2)." LIKE ".sl_quote_smart("%".$fildata2."%");
      if ($filcond2=="notcontain")
        $query2=mysqli_real_escape_string($mysql_link,$filfield2)." NOT LIKE ".sl_quote_smart("%".$fildata2."%");
      if ($filcond2=="less")
        $query2=mysqli_real_escape_string($mysql_link,$filfield2)." < ".sl_quote_smart($fildata2);
      if ($filcond2=="greater")
        $query2=mysqli_real_escape_string($mysql_link,$filfield2)." > ".sl_quote_smart($fildata2);
      if ($filcond2=="starts")
        $query2=mysqli_real_escape_string($mysql_link,$filfield2)." LIKE ".sl_quote_smart($fildata2."%");
      if ($filcond2=="ends")
        $query2=mysqli_real_escape_string($mysql_link,$filfield2)." LIKE ".sl_quote_smart("%".$fildata2);
      if ($filcond2=="lessnum")
        $query2=mysqli_real_escape_string($mysql_link,$filfield2)." < ".mysqli_real_escape_string($mysql_link,$fildata2);
      if ($filcond2=="greaternum")
        $query2=mysqli_real_escape_string($mysql_link,$filfield2)." > ".mysqli_real_escape_string($mysql_link,$fildata2);
      $sqlquerytodo=$sqlquerytodo." ".$filbool1." ".$query2;
      if (($filfield3!="") && ($filfield3!="groupexpiry"))
      {
        if ($filcond3=="equals")
          $query3=mysqli_real_escape_string($mysql_link,$filfield3)." = ".sl_quote_smart($fildata3);
        if ($filcond3=="notequal")
          $query3=mysqli_real_escape_string($mysql_link,$filfield3)." != ".sl_quote_smart($fildata3);
        if ($filcond3=="contains")
          $query3=mysqli_real_escape_string($mysql_link,$filfield3)." LIKE ".sl_quote_smart("%".$fildata3."%");
        if ($filcond3=="notcontain")
          $query3=mysqli_real_escape_string($mysql_link,$filfield3)." NOT LIKE ".sl_quote_smart("%".$fildata3."%");
        if ($filcond3=="less")
          $query3=mysqli_real_escape_string($mysql_link,$filfield3)." < ".sl_quote_smart($fildata3);
        if ($filcond3=="greater")
          $query3=mysqli_real_escape_string($mysql_link,$filfield3)." > ".sl_quote_smart($fildata3);
        if ($filcond3=="starts")
          $query3=mysqli_real_escape_string($mysql_link,$filfield3)." LIKE ".sl_quote_smart($fildata3."%");
        if ($filcond3=="ends")
          $query3=mysqli_real_escape_string($mysql_link,$filfield3)." LIKE ".sl_quote_smart("%".$fildata3);
        if ($filcond3=="lessnum")
          $query3=mysqli_real_escape_string($mysql_link,$filfield3)." < ".mysqli_real_escape_string($mysql_link,$fildata3);
        if ($filcond3=="greaternum")
          $query3=mysqli_real_escape_string($mysql_link,$filfield3)." > ".mysqli_real_escape_string($mysql_link,$fildata3);
          $sqlquerytodo=$sqlquerytodo." ".$filbool2." ".$query3;
      if (($filfield4!="") && ($filfield4!="groupexpiry"))
      {
        if ($filcond4=="equals")
          $query4=mysqli_real_escape_string($mysql_link,$filfield4)." = ".sl_quote_smart($fildata4);
        if ($filcond4=="notequal")
          $query4=mysqli_real_escape_string($mysql_link,$filfield4)." != ".sl_quote_smart($fildata4);
        if ($filcond4=="contains")
          $query4=mysqli_real_escape_string($mysql_link,$filfield4)." LIKE ".sl_quote_smart("%".$fildata4."%");
        if ($filcond4=="notcontain")
          $query4=mysqli_real_escape_string($mysql_link,$filfield4)." NOT LIKE ".sl_quote_smart("%".$fildata4."%");
        if ($filcond4=="less")
          $query4=mysqli_real_escape_string($mysql_link,$filfield4)." < ".sl_quote_smart($fildata4);
        if ($filcond4=="greater")
          $query4=mysqli_real_escape_string($mysql_link,$filfield4)." > ".sl_quote_smart($fildata4);
        if ($filcond4=="starts")
          $query4=mysqli_real_escape_string($mysql_link,$filfield4)." LIKE ".sl_quote_smart($fildata4."%");
        if ($filcond4=="ends")
          $query4=mysqli_real_escape_string($mysql_link,$filfield4)." LIKE ".sl_quote_smart("%".$fildata4);
        if ($filcond4=="lessnum")
          $query4=mysqli_real_escape_string($mysql_link,$filfield4)." < ".mysqli_real_escape_string($mysql_link,$fildata4);
        if ($filcond4=="greaternum")
          $query4=mysqli_real_escape_string($mysql_link,$filfield4)." > ".mysqli_real_escape_string($mysql_link,$fildata4);
          $sqlquerytodo=$sqlquerytodo." ".$filbool3." ".$query4;
      }       
      }
    }
    // Put back the date in user entered format if we filtered using created date
    if (($filfield1==$CreatedField) && (strlen($fildata1)==6))
    {
      $fildata1=$enteredfildata1;
    }
    if (($filfield2==$CreatedField) && (strlen($fildata2)==6))
    {
      $fildata2=$enteredfildata2;
    }
    if (($filfield3==$CreatedField) && (strlen($fildata3)==6))
    {
      $fildata3=$enteredfildata3;
    }
    if (($filfield4==$CreatedField) && (strlen($fildata4)==6))
    {
      $fildata4=$enteredfildata4;
    }
  }
  if (($filteron=="1") && (($filfield1=="groupexpiry") || ($filfield2=="groupexpiry") || ($filfield3=="groupexpiry") || ($filfield4=="groupexpiry")))
  {
    // We will filter manually so set query for all records
    $sqlquerytodo="SELECT * FROM ".$DbTableName;
  }
  if ($sqlquery!="")
    $sqlquerytodo=$sqlquery;
    
  if ($act=="query")
  {
    $tablestart=0;
    if ((strlen(trim($sqlquerytodo))>=6) && (strcasecmp(substr(trim($sqlquerytodo),0,6),"SELECT")!=0))
    {
      $mysql_result=mysqli_query($mysql_link,$sqlquerytodo);
      if ($mysql_result!=false)
      {
        $rowsaffected=mysqli_affected_rows($mysql_link);
        $sqlquery="SELECT * FROM ".$DbTableName;
        $sqlquerytodo=$sqlquery;
          $sqlinput="";
      }
      else
      {
        $queryerror=true;
        $sqlinput=$sqlquery;
        $sqlquery="SELECT * FROM ".$DbTableName;
        $sqlquerytodo=$sqlquery;
      }        
    }
    else
    {
      $sqlinput=$sqlquery;
      $sqlquerytodo=$sqlquery;
    }    
  }
  
  if (($act=="filter") || ($act=="quicksearch") || ($act=="memberof") || ($act=="joinwithin") || ($act=="unexpmemberof") || ($act=="expmemberof") || ($act=="expwithin") || ($act=="onlyselected"))
    $tablestart=0;
  
  $queryrun=false;
  // ********************************
  if (($filteron=="1") && (($filfield1=="groupexpiry") || ($filfield2=="groupexpiry") || ($filfield3=="groupexpiry") || ($filfield4=="groupexpiry")))
  {
    $queryrun=true;
    // As groupexpiry field selected we need to filter manually
    if (($sortf!="") && ($sortf!=""))
      $sortquery=" ORDER BY ".mysqli_real_escape_string($mysql_link,$sortf)." ".mysqli_real_escape_string($mysql_link,$sortd);
      $numrows=0;
      $numselected=0;
      $tfildata1=strtolower($fildata1);
      $tfildata2=strtolower($fildata2);
      $tfildata3=strtolower($fildata3);
      $tfildata4=strtolower($fildata4);
    // If checking Created field then convert date to YYMMDD for each of the 4 filters
    if (($filfield1==$CreatedField) && (strlen($tfildata1)==6))
    {
      if ($DateFormat=="DDMMYY")
        $tfildata1=substr($tfildata1,4,2).substr($tfildata1,2,2).substr($tfildata1,0,2);
      else
        $tfildata1=substr($tfildata1,4,2).substr($tfildata1,0,2).substr($tfildata1,2,2);
      }
    if (($filfield2==$CreatedField) && (strlen($tfildata2)==6))
    {
      if ($DateFormat=="DDMMYY")
        $tfildata2=substr($tfildata2,4,2).substr($tfildata2,2,2).substr($tfildata2,0,2);
      else
        $tfildata2=substr($tfildata2,4,2).substr($tfildata2,0,2).substr($tfildata2,2,2);
      }
    if (($filfield3==$CreatedField) && (strlen($tfildata3)==6))
    {
      if ($DateFormat=="DDMMYY")
        $tfildata3=substr($tfildata3,4,2).substr($tfildata3,2,2).substr($tfildata3,0,2);
      else
        $tfildata3=substr($tfildata3,4,2).substr($tfildata3,0,2).substr($tfildata3,2,2);
      }
    if (($filfield4==$CreatedField) && (strlen($tfildata4)==6))
    {
      if ($DateFormat=="DDMMYY")
        $tfildata4=substr($tfildata4,4,2).substr($tfildata4,2,2).substr($tfildata4,0,2);
      else
        $tfildata4=substr($tfildata4,4,2).substr($tfildata4,0,2).substr($tfildata4,2,2);
      }

    if (($filfield1=="groupexpiry") && (strlen($tfildata1)==6))
    {
      if ($DateFormat=="DDMMYY")
        $fitime1=substr($tfildata1,4,2).substr($tfildata1,2,2).substr($tfildata1,0,2);
      else
        $fitime1=substr($tfildata1,4,2).substr($tfildata1,0,2).substr($tfildata1,2,2);
    }
    else
      $fitime1=0;

    if (($filfield2=="groupexpiry") && (strlen($tfildata2)==6))
    {
      if ($DateFormat=="DDMMYY")
        $fitime2=substr($tfildata2,4,2).substr($tfildata2,2,2).substr($tfildata2,0,2);
      else
        $fitime2=substr($tfildata2,4,2).substr($tfildata2,0,2).substr($tfildata2,2,2);
    }
    else
      $fitime2=0;

    if (($filfield3=="groupexpiry") && (strlen($tfildata3)==6))
    {
      if ($DateFormat=="DDMMYY")
        $fitime3=substr($tfildata3,4,2).substr($tfildata3,2,2).substr($tfildata3,0,2);
      else
        $fitime3=substr($tfildata3,4,2).substr($tfildata3,0,2).substr($tfildata3,2,2);
    }
    else
      $fitime3=0;

    if (($filfield4=="groupexpiry") && (strlen($tfildata4)==6))
    {
      if ($DateFormat=="DDMMYY")
        $fitime4=substr($tfildata4,4,2).substr($tfildata4,2,2).substr($tfildata4,0,2);
      else
        $fitime4=substr($tfildata4,4,2).substr($tfildata4,0,2).substr($tfildata4,2,2);
    }
    else
      $fitime4=0;


    for ($l=0;$l<$totalrows;$l=$l+$sl_dbblocksize)
    {
      $limit=" LIMIT ".$l.",".$sl_dbblocksize;
      $mysql_result=mysqli_query($mysql_link,$sqlquerytodo.$sortquery.$limit);
      if ($mysql_result!=false)
      {
        for ($k=$l;$k<($l+$sl_dbblocksize);$k++)
        {
          if ($k>=$totalrows)
            break;
          $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
          if ($row!=false)
          {
            // Now see if matches filter 1
            if ($filfield1!="groupexpiry")
            {
              $datatocheck=strtolower($row[$filfield1]);
              switch ($filcond1)
              {
                case "equals":
                  $comp1=($tfildata1==$datatocheck);
                  break;
                case "notequal":
                  $comp1=($tfildata1!=$datatocheck);
                  break;
                case "contains":
                  $comp1=is_integer(strpos($datatocheck,$tfildata1));
                  break;
                case "notcontain":
                  $comp1=(strpos($datatocheck,$tfildata1)===false);
                  break;
                case "less":
                  $comp1=($datatocheck<$tfildata1);
                  break;
                case "greater":
                  $comp1=($datatocheck>$tfildata1);
                  break;
                case "starts":
                  $comp1=(strpos($datatocheck,$tfildata1)===0);
                  break;
                case "ends":
                  $comp1=(strpos($datatocheck,$tfildata1)===(strlen($datatocheck)-strlen($tfildata1)));
                  break;
              }
            }
            else
            {
              // Compare the expiry dates. If any match then set comp to true
              $allgroups=explode("^",$row[$UsergroupsField]);
              for ($j=0;$j<count($allgroups);$j++)
              {
                $exdate=strtok($allgroups[$j],":");
                $exdate=trim(strtok(":"));
                if (strlen($exdate)!=6)
                {
                  if (($filcond1=="notequal") || ($filcond1=="greater"))
                    $comp1=true;
                  else
                    $comp1=false;
                    continue;
                }
                switch ($filcond1)
                {
                    case "equals":
                      $comp1=($tfildata1==$exdate);
                      break;
                    case "notequal":
                      $comp1=($tfildata1!=$exdate);
                      break;
                    case "contains":
                      $comp1=is_integer(strpos($exdate,$tfildata1));
                      break;
                    case "greater":
                      $comp1=($datatocheck>$tfildata1);
                        if ($fitime1==0)
                            $comp1=false;
                        else
                        {
                        if ($DateFormat=="DDMMYY")
                                   $extime=substr($exdate,4,2).substr($exdate,2,2).substr($exdate,0,2);
                        else
                                   $extime=substr($exdate,4,2).substr($exdate,0,2).substr($exdate,2,2);
                        $comp1=false;
                        if (($filcond1=="greater") && ($extime>$fitime1))
                                            $comp1=true;
                    }                     
                      break;
                    case "starts":
                      $comp1=(strpos($exdate,$tfildata1)===0);
                      break;
                    case "ends":
                      $comp1=(strpos($exdate,$tfildata1)===(strlen($exdate)-strlen($tfildata1)));
                      break;
                    case "less":
                        if ($fitime1==0)
                            $comp1=false;
                        else
                        {
                        if ($DateFormat=="DDMMYY")
                                   $extime=substr($exdate,4,2).substr($exdate,2,2).substr($exdate,0,2);
                        else
                                   $extime=substr($exdate,4,2).substr($exdate,0,2).substr($exdate,2,2);
                        $comp1=false;
                        if (($filcond1=="less") && ($extime<$fitime1))
                                            $comp1=true;
                    }
                    break;
                }
                if ($comp1)
                  break; // We only need one match
              }
            }
            $finalcomp="\$comp1";
            // Now see if matches filter 2
            if ($filfield2!="")
            {
              if ($filfield2!="groupexpiry")
              {
                $datatocheck=strtolower($row[$filfield2]);
                switch ($filcond2)
                {
                  case "equals":
                    $comp2=($tfildata2==$datatocheck);
                    break;
                  case "notequal":
                    $comp2=($tfildata2!=$datatocheck);
                    break;
                  case "contains":
                    $comp2=is_integer(strpos($datatocheck,$tfildata2));
                    break;
                  case "notcontain":
                    $comp2=(strpos($datatocheck,$tfildata2)===false);
                    break;
                  case "less":
                    $comp2=($datatocheck<$tfildata2);
                    break;
                  case "greater":
                    $comp2=($datatocheck>$tfildata2);
                    break;
                  case "starts":
                    $comp2=(strpos($datatocheck,$tfildata2)===0);
                    break;
                  case "ends":
                    $comp2=(strpos($datatocheck,$tfildata2)===(strlen($datatocheck)-strlen($tfildata2)));
                    break;
                }
              }
              else
              {
                // Compare the expiry dates. If any match then set comp to true
                $allgroups=explode("^",$row[$UsergroupsField]);
                for ($j=0;$j<count($allgroups);$j++)
                {
                  $exdate=strtok($allgroups[$j],":");
                  $exdate=trim(strtok(":"));
                  if (strlen($exdate)!=6)
                  {
                    if (($filcond2=="notequal") || ($filcond2=="greater"))
                        $comp2=true;
                    else
                        $comp2=false;
                    continue;
                  }
                  switch ($filcond2)
                  {
                    case "equals":
                      $comp2=($tfildata2==$exdate);
                      break;
                    case "notequal":
                      $comp2=($tfildata2!=$exdate);
                      break;
                    case "contains":
                      $comp2=is_integer(strpos($exdate,$tfildata2));
                      break;
                    case "greater":
                      $comp2=($datatocheck>$tfildata2);
                        if ($fitime2==0)
                            $comp2=false;
                        else
                        {
                        if ($DateFormat=="DDMMYY")
                                   $extime=substr($exdate,4,2).substr($exdate,2,2).substr($exdate,0,2);
                        else
                                   $extime=substr($exdate,4,2).substr($exdate,0,2).substr($exdate,2,2);
                        $comp2=false;
                        if (($filcond2=="greater") && ($extime>$fitime2))
                                            $comp2=true;
                      }                   
                      break;
                    case "starts":
                      $comp2=(strpos($exdate,$tfildata2)===0);
                      break;
                    case "ends":
                      $comp2=(strpos($exdate,$tfildata2)===(strlen($exdate)-strlen($tfildata2)));
                      break;
                    case "less":
                        if ($fitime2==0)
                            $comp2=false;
                        else
                        {
                        if ($DateFormat=="DDMMYY")
                                   $extime=substr($exdate,4,2).substr($exdate,2,2).substr($exdate,0,2);
                        else
                                   $extime=substr($exdate,4,2).substr($exdate,0,2).substr($exdate,2,2);
                        $comp2=false;
                        if (($filcond2=="less") && ($extime<$fitime2))
                                            $comp2=true;
                      }
                      break;
                    }
                    if ($comp2)
                      break; // We only need one match
                }
              }
              if ($filbool1=="AND")
                  $finalcomp=$finalcomp." & "."\$comp2";
              else
                  $finalcomp=$finalcomp." | "."\$comp2";
            }
            // Now see if matches filter 3
            if ($filfield3!="")
            {
              if ($filfield3!="groupexpiry")
              {
                $datatocheck=strtolower($row[$filfield3]);
                switch ($filcond3)
                {
                  case "equals":
                    $comp3=($tfildata3==$datatocheck);
                    break;
                  case "notequal":
                    $comp3=($tfildata3!=$datatocheck);
                    break;
                  case "contains":
                    $comp3=is_integer(strpos($datatocheck,$tfildata3));
                    break;
                  case "notcontain":
                    $comp3=(strpos($datatocheck,$tfildata3)===false);
                    break;
                  case "less":
                    $comp3=($datatocheck<$tfildata3);
                    break;
                  case "greater":
                    $comp3=($datatocheck>$tfildata3);
                    break;
                  case "starts":
                    $comp3=(strpos($datatocheck,$tfildata3)===0);
                    break;
                  case "ends":
                    $comp3=(strpos($datatocheck,$tfildata3)===(strlen($datatocheck)-strlen($tfildata3)));
                    break;
                }
              }
              else
              {
                // Compare the expiry dates. If any match then set comp to true
                $allgroups=explode("^",$row[$UsergroupsField]);
                for ($j=0;$j<count($allgroups);$j++)
                {
                  $exdate=strtok($allgroups[$j],":");
                  $exdate=trim(strtok(":"));
                  if (strlen($exdate)!=6)
                  {
                    if (($filcond3=="notequal") || ($filcond3=="greater"))
                        $comp3=true;
                    else
                        $comp3=false;
                    continue;
                  }
                  switch ($filcond3)
                  {
                    case "equals":
                      $comp3=($tfildata3==$exdate);
                      break;
                    case "notequal":
                      $comp3=($tfildata3!=$exdate);
                      break;
                    case "contains":
                      $comp3=is_integer(strpos($exdate,$tfildata3));
                      break;
                    case "greater":
                      $comp3=($datatocheck>$tfildata3);
                        if ($fitime3==0)
                            $comp3=false;
                        else
                        {
                        if ($DateFormat=="DDMMYY")
                                   $extime=substr($exdate,4,2).substr($exdate,2,2).substr($exdate,0,2);
                        else
                                   $extime=substr($exdate,4,2).substr($exdate,0,2).substr($exdate,2,2);
                        $comp3=false;
                        if (($filcond3=="greater") && ($extime>$fitime3))
                                            $comp3=true;
                      }                   
                      break;
                    case "starts":
                      $comp3=(strpos($exdate,$tfildata3)===0);
                      break;
                    case "ends":
                      $comp3=(strpos($exdate,$tfildata3)===(strlen($exdate)-strlen($tfildata3)));
                      break;
                    case "less":
                        if ($fitime3==0)
                            $comp3=false;
                        else
                        {
                        if ($DateFormat=="DDMMYY")
                                   $extime=substr($exdate,4,2).substr($exdate,2,2).substr($exdate,0,2);
                        else
                                   $extime=substr($exdate,4,2).substr($exdate,0,2).substr($exdate,2,2);
                        $comp3=false;
                        if (($filcond3=="less") && ($extime<$fitime3))
                                            $comp3=true;
                      }
                      break;
                    }
                    if ($comp3)
                      break; // We only need one match
                }
              }
              if ($filbool2=="AND")
                  $finalcomp=$finalcomp." & "."\$comp3";
                        else
                  $finalcomp=$finalcomp." | "."\$comp3";
            }  
            
            // Now see if matches filter 4
            if ($filfield4!="")
            {
              if ($filfield4!="groupexpiry")
              {
                $datatocheck=strtolower($row[$filfield4]);
                switch ($filcond4)
                {
                  case "equals":
                    $comp4=($tfildata4==$datatocheck);
                    break;
                  case "notequal":
                    $comp4=($tfildata4!=$datatocheck);
                    break;
                  case "contains":
                    $comp4=is_integer(strpos($datatocheck,$tfildata4));
                    break;
                  case "notcontain":
                    $comp4=(strpos($datatocheck,$tfildata4)===false);
                    break;
                  case "less":
                    $comp4=($datatocheck<$tfildata4);
                    break;
                  case "greater":
                    $comp4=($datatocheck>$tfildata4);
                    break;
                  case "starts":
                    $comp4=(strpos($datatocheck,$tfildata4)===0);
                    break;
                  case "ends":
                    $comp4=(strpos($datatocheck,$tfildata4)===(strlen($datatocheck)-strlen($tfildata4)));
                    break;
                }
              }
              else
              {
                // Compare the expiry dates. If any match then set comp to true
                $allgroups=explode("^",$row[$UsergroupsField]);
                for ($j=0;$j<count($allgroups);$j++)
                {
                  $exdate=strtok($allgroups[$j],":");
                  $exdate=trim(strtok(":"));
                  if (strlen($exdate)!=6)
                  {
                    if (($filcond4=="notequal") || ($filcond4=="greater"))
                        $comp4=true;
                    else
                        $comp4=false;
                    continue;
                  }
                  switch ($filcond4)
                  {
                    case "equals":
                      $comp4=($tfildata4==$exdate);
                      break;
                    case "notequal":
                      $comp4=($tfildata4!=$exdate);
                      break;
                    case "contains":
                      $comp4=is_integer(strpos($exdate,$tfildata4));
                      break;
                    case "greater":
                      $comp4=($datatocheck>$tfildata4);
                        if ($fitime4==0)
                            $comp4=false;
                        else
                        {
                        if ($DateFormat=="DDMMYY")
                                   $extime=substr($exdate,4,2).substr($exdate,2,2).substr($exdate,0,2);
                        else
                                   $extime=substr($exdate,4,2).substr($exdate,0,2).substr($exdate,2,2);
                        $comp4=false;
                        if (($filcond4=="greater") && ($extime>$fitime4))
                                            $comp4=true;
                      }                   
                      break;
                    case "starts":
                      $comp4=(strpos($exdate,$tfildata4)===0);
                      break;
                    case "ends":
                      $comp4=(strpos($exdate,$tfildata4)===(strlen($exdate)-strlen($tfildata4)));
                      break;
                    case "less":
                        if ($fitime4==0)
                            $comp4=false;
                        else
                        {
                        if ($DateFormat=="DDMMYY")
                                   $extime=substr($exdate,4,2).substr($exdate,2,2).substr($exdate,0,2);
                        else
                                   $extime=substr($exdate,4,2).substr($exdate,0,2).substr($exdate,2,2);
                        $comp4=false;
                        if (($filcond4=="less") && ($extime<$fitime4))
                                            $comp4=true;
                      }
                      break;
                    }
                    if ($comp4)
                      break; // We only need one match
                }
              }
              if ($filbool3=="AND")
                  $finalcomp=$finalcomp." & "."\$comp4";
                        else
                  $finalcomp=$finalcomp." | "."\$comp4";
            }
            // Now combine filter results together using OR and AND
            eval("\$comp=(".$finalcomp.");");
            if ($comp)
            {
                $select=$row[$SelectedField];
              if ($act=="selectall")
              {
                $mysql_selresult=mysqli_query($mysql_link,"UPDATE ".$DbTableName." SET ".$SelectedField."='Yes' WHERE ".$UsernameField."=".sl_quote_smart($row[$UsernameField]));
                $select="Yes";
              }
              if ($act=="deselectall")
              {
                $mysql_selresult=mysqli_query($mysql_link,"UPDATE ".$DbTableName." SET ".$SelectedField."='No' WHERE ".$UsernameField."=".sl_quote_smart($row[$UsernameField]));
                $select="No";
              }
              $numrows++;
              if ((($numrows-1)>=$tablestart) && (($numrows-1)<($tablestart+$ShowRows)))
              {
                $rtu=$numrows-$tablestart-1;
                $Selectedarray[$rtu]=$select;
                $Createdarray[$rtu]=$row[$CreatedField];
                $Usernamearray[$rtu]=$row[$UsernameField];
                $Passwordarray[$rtu]=$row[$PasswordField];
                $Enabledarray[$rtu]=$row[$EnabledField];
                $Namearray[$rtu]=$row[$NameField];
                $Emailarray[$rtu]=$row[$EmailField];
                $Usergroupsarray[$rtu]=$row[$UsergroupsField];
                $Cus1array[$rtu]=$row[$Custom1Field];
                $Cus2array[$rtu]=$row[$Custom2Field];
                $Cus3array[$rtu]=$row[$Custom3Field];
                $Cus4array[$rtu]=$row[$Custom4Field];
                $Cus5array[$rtu]=$row[$Custom5Field];
                $Cus6array[$rtu]=$row[$Custom6Field];
                $Cus7array[$rtu]=$row[$Custom7Field];
                $Cus8array[$rtu]=$row[$Custom8Field];
                $Cus9array[$rtu]=$row[$Custom9Field];
                $Cus10array[$rtu]=$row[$Custom10Field];
                $Cus11array[$rtu]=$row[$Custom11Field];
                $Cus12array[$rtu]=$row[$Custom12Field];
                $Cus13array[$rtu]=$row[$Custom13Field];
                $Cus14array[$rtu]=$row[$Custom14Field];
                $Cus15array[$rtu]=$row[$Custom15Field];
                $Cus16array[$rtu]=$row[$Custom16Field];
                $Cus17array[$rtu]=$row[$Custom17Field];
                $Cus18array[$rtu]=$row[$Custom18Field];
                $Cus19array[$rtu]=$row[$Custom19Field];
                $Cus20array[$rtu]=$row[$Custom20Field];
                $Cus21array[$rtu]=$row[$Custom21Field];
                $Cus22array[$rtu]=$row[$Custom22Field];
                $Cus23array[$rtu]=$row[$Custom23Field];
                $Cus24array[$rtu]=$row[$Custom24Field];
                $Cus25array[$rtu]=$row[$Custom25Field];
                $Cus26array[$rtu]=$row[$Custom26Field];
                $Cus27array[$rtu]=$row[$Custom27Field];
                $Cus28array[$rtu]=$row[$Custom28Field];
                $Cus29array[$rtu]=$row[$Custom29Field];
                $Cus30array[$rtu]=$row[$Custom30Field];
                $Cus31array[$rtu]=$row[$Custom31Field];
                $Cus32array[$rtu]=$row[$Custom32Field];
                $Cus33array[$rtu]=$row[$Custom33Field];
                $Cus34array[$rtu]=$row[$Custom34Field];
                $Cus35array[$rtu]=$row[$Custom35Field];
                $Cus36array[$rtu]=$row[$Custom36Field];
                $Cus37array[$rtu]=$row[$Custom37Field];
                $Cus38array[$rtu]=$row[$Custom38Field];
                $Cus39array[$rtu]=$row[$Custom39Field];
                $Cus40array[$rtu]=$row[$Custom40Field];
                $Cus41array[$rtu]=$row[$Custom41Field];
                $Cus42array[$rtu]=$row[$Custom42Field];
                $Cus43array[$rtu]=$row[$Custom43Field];
                $Cus44array[$rtu]=$row[$Custom44Field];
                $Cus45array[$rtu]=$row[$Custom45Field];
                $Cus46array[$rtu]=$row[$Custom46Field];
                $Cus47array[$rtu]=$row[$Custom47Field];
                $Cus48array[$rtu]=$row[$Custom48Field];
                $Cus49array[$rtu]=$row[$Custom49Field];
                $Cus50array[$rtu]=$row[$Custom50Field];              
                $Useridarray[$rtu]=$row[$IdField];              
              }
            }
          }
        }
      }       
    }
  }
  if ($quicksearch!="")
  {
    $tmpdata=sl_quote_smart("%".$quicksearch."%");
    $sqlquerytodo= "SELECT * FROM ".$DbTableName." WHERE ".$UsernameField." LIKE ".$tmpdata." OR ";
    $sqlquerytodo.=$PasswordField." LIKE ".$tmpdata." OR ";
    $sqlquerytodo.=$NameField." LIKE ".$tmpdata." OR ";
    $sqlquerytodo.=$EmailField." LIKE ".$tmpdata." OR ";
    $sqlquerytodo.=$UsergroupsField." LIKE ".$tmpdata;
    for ($k=1;$k<=50;$k++)
    {
      $sqlquerytodo.=" OR ";
      $var="Custom".$k."Field";
      $sqlquerytodo.=$$var." LIKE ".$tmpdata;
    }
  }
  
  if ($memberof!="")
  {
    $sqlquerytodo= "SELECT * FROM ".$DbTableName." WHERE ".$UsergroupsField." REGEXP '(^|\\\^)".$memberof."(:|\\\^|$)'";
  }
  if ($joinwithin!="")
  {
    $comparedate=gmdate("ymd",strtotime("-".($joinwithin-1)." days") );
    $sqlquerytodo= "SELECT * FROM ".$DbTableName." WHERE ".$CreatedField." >= ".$comparedate;
  }
  if ($onlyselected!="")
  {
    $sqlquerytodo= "SELECT * FROM ".$DbTableName." WHERE ".$SelectedField."=".sl_quote_smart($onlyselected);
  }
  if (($unexpmemberof!="") || ($expmemberof!="") || ($expwithin!=""))
  { 
    $queryrun=true;
    if (($sortf!="") && ($sortf!=""))
      $sortquery=" ORDER BY ".mysqli_real_escape_string($mysql_link,$sortf)." ".mysqli_real_escape_string($mysql_link,$sortd);
    if ($unexpmemberof!="")
    {
      $comparedate=gmdate("ymd");
      $qry="SELECT * FROM ".$DbTableName." WHERE ".$UsergroupsField." REGEXP '(^|\\\^)".$unexpmemberof."(:|\\\^|$)'";
    }  
    if ($expmemberof!="")
    {
      $comparedate=gmdate("ymd");
      $qry="SELECT * FROM ".$DbTableName." WHERE ".$UsergroupsField." REGEXP '(^|\\\^)".$expmemberof."(:)'";
    }  
    if ($expwithin!="")
    {
      $comparedate=gmdate("ymd");
      $comparedate2=gmdate("ymd",strtotime("+".$expwithindays." days") );
      $qry="SELECT * FROM ".$DbTableName." WHERE ".$UsergroupsField." REGEXP '(^|\\\^)".$expwithin."(:)'";
    }
      $numrows=0;
      $numselected=0;
    for ($l=0;$l<$totalrows;$l=$l+$sl_dbblocksize)
    {
      $limit=" LIMIT ".$l.",".$sl_dbblocksize;
      $mysql_result=mysqli_query($mysql_link,$qry.$sortquery.$limit);
      if ($mysql_result!=false)
      {
        for ($k=$l;$k<($l+$sl_dbblocksize);$k++)
        {
          if ($k>=$totalrows)
            break;
          $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
          if ($row!=false)
          {
            $comp=false;
            if ($unexpmemberof!="")
              {  
                // Check that group selected is not expired
              $allgroups=explode("^",$row[$UsergroupsField]);
              for ($j=0;$j<count($allgroups);$j++)
              {
                $grpname=strtok($allgroups[$j],":");
                if (trim($grpname)!=$unexpmemberof)
                  continue;
                $exdate=trim(strtok(":"));
                if ($exdate=="")
                {
                  $comp=true;
                  break;
                }  
                if (strlen($exdate)==6)
                {
                  if ($DateFormat=="DDMMYY")
                          $exp=substr($exdate,4,2).substr($exdate,2,2).substr($exdate,0,2);
                  else
                          $exp=substr($exdate,4,2).substr($exdate,0,2).substr($exdate,2,2);
                        if ($exp>=$comparedate)
                        {
                          $comp=true;
                          break;
                        }  
                }
              }
            }
            if ($expmemberof!="")
              {  
                // Check that group selected is expired
              $allgroups=explode("^",$row[$UsergroupsField]);
              for ($j=0;$j<count($allgroups);$j++)
              {
                $grpname=strtok($allgroups[$j],":");
                if (trim($grpname)!=$expmemberof)
                  continue;
                $exdate=trim(strtok(":"));
                if ($exdate=="")
                {
                  $comp=false;
                  break;
                }  
                if (strlen($exdate)==6)
                {
                  if ($DateFormat=="DDMMYY")
                          $exp=substr($exdate,4,2).substr($exdate,2,2).substr($exdate,0,2);
                  else
                          $exp=substr($exdate,4,2).substr($exdate,0,2).substr($exdate,2,2);
                        if ($exp<$comparedate)
                        {
                          $comp=true;
                          break;
                        }  
                }
              }              
            }
            if ($expwithin!="")
              { 
                // see if group expires within entered days
              $allgroups=explode("^",$row[$UsergroupsField]);
              for ($j=0;$j<count($allgroups);$j++)
              {
                $grpname=strtok($allgroups[$j],":");
                if (trim($grpname)!=$expwithin)
                  continue;
                $exdate=trim(strtok(":"));
                if ($exdate=="")
                {
                  $comp=false;
                  break;
                }  
                if (strlen($exdate)==6)
                {
                  if ($DateFormat=="DDMMYY")
                          $exp=substr($exdate,4,2).substr($exdate,2,2).substr($exdate,0,2);
                  else
                          $exp=substr($exdate,4,2).substr($exdate,0,2).substr($exdate,2,2);
                        if (($exp<$comparedate2) && ($exp>=$comparedate))
                        {
                          $comp=true;
                          break;
                        }  
                }
              }
            }
            if ($comp)
            {
                $select=$row[$SelectedField];
              if ($act=="selectall")
              {
                $mysql_selresult=mysqli_query($mysql_link,"UPDATE ".$DbTableName." SET ".$SelectedField."='Yes' WHERE ".$UsernameField."=".sl_quote_smart($row[$UsernameField]));
                $select="Yes";
              }
              if ($act=="deselectall")
              {
                $mysql_selresult=mysqli_query($mysql_link,"UPDATE ".$DbTableName." SET ".$SelectedField."='No' WHERE ".$UsernameField."=".sl_quote_smart($row[$UsernameField]));
                $select="No";
              }
              $numrows++;
              if ((($numrows-1)>=$tablestart) && (($numrows-1)<($tablestart+$ShowRows)))
              {
                $rtu=$numrows-$tablestart-1;
                $Selectedarray[$rtu]=$select;
                $Createdarray[$rtu]=$row[$CreatedField];
                $Usernamearray[$rtu]=$row[$UsernameField];
                $Passwordarray[$rtu]=$row[$PasswordField];
                $Enabledarray[$rtu]=$row[$EnabledField];
                $Namearray[$rtu]=$row[$NameField];
                $Emailarray[$rtu]=$row[$EmailField];
                $Usergroupsarray[$rtu]=$row[$UsergroupsField];
                $Cus1array[$rtu]=$row[$Custom1Field];
                $Cus2array[$rtu]=$row[$Custom2Field];
                $Cus3array[$rtu]=$row[$Custom3Field];
                $Cus4array[$rtu]=$row[$Custom4Field];
                $Cus5array[$rtu]=$row[$Custom5Field];
                $Cus6array[$rtu]=$row[$Custom6Field];
                $Cus7array[$rtu]=$row[$Custom7Field];
                $Cus8array[$rtu]=$row[$Custom8Field];
                $Cus9array[$rtu]=$row[$Custom9Field];
                $Cus10array[$rtu]=$row[$Custom10Field];
                $Cus11array[$rtu]=$row[$Custom11Field];
                $Cus12array[$rtu]=$row[$Custom12Field];
                $Cus13array[$rtu]=$row[$Custom13Field];
                $Cus14array[$rtu]=$row[$Custom14Field];
                $Cus15array[$rtu]=$row[$Custom15Field];
                $Cus16array[$rtu]=$row[$Custom16Field];
                $Cus17array[$rtu]=$row[$Custom17Field];
                $Cus18array[$rtu]=$row[$Custom18Field];
                $Cus19array[$rtu]=$row[$Custom19Field];
                $Cus20array[$rtu]=$row[$Custom20Field];
                $Cus21array[$rtu]=$row[$Custom21Field];
                $Cus22array[$rtu]=$row[$Custom22Field];
                $Cus23array[$rtu]=$row[$Custom23Field];
                $Cus24array[$rtu]=$row[$Custom24Field];
                $Cus25array[$rtu]=$row[$Custom25Field];
                $Cus26array[$rtu]=$row[$Custom26Field];
                $Cus27array[$rtu]=$row[$Custom27Field];
                $Cus28array[$rtu]=$row[$Custom28Field];
                $Cus29array[$rtu]=$row[$Custom29Field];
                $Cus30array[$rtu]=$row[$Custom30Field];
                $Cus31array[$rtu]=$row[$Custom31Field];
                $Cus32array[$rtu]=$row[$Custom32Field];
                $Cus33array[$rtu]=$row[$Custom33Field];
                $Cus34array[$rtu]=$row[$Custom34Field];
                $Cus35array[$rtu]=$row[$Custom35Field];
                $Cus36array[$rtu]=$row[$Custom36Field];
                $Cus37array[$rtu]=$row[$Custom37Field];
                $Cus38array[$rtu]=$row[$Custom38Field];
                $Cus39array[$rtu]=$row[$Custom39Field];
                $Cus40array[$rtu]=$row[$Custom40Field];
                $Cus41array[$rtu]=$row[$Custom41Field];
                $Cus42array[$rtu]=$row[$Custom42Field];
                $Cus43array[$rtu]=$row[$Custom43Field];
                $Cus44array[$rtu]=$row[$Custom44Field];
                $Cus45array[$rtu]=$row[$Custom45Field];
                $Cus46array[$rtu]=$row[$Custom46Field];
                $Cus47array[$rtu]=$row[$Custom47Field];
                $Cus48array[$rtu]=$row[$Custom48Field];
                $Cus49array[$rtu]=$row[$Custom49Field];
                $Cus50array[$rtu]=$row[$Custom50Field];              
                $Useridarray[$rtu]=$row[$IdField];
              }
            }
          }
        }
      }
    }
  }
  if (!$queryrun)
  {
      if ($mysql_result!=false)
      {
        if ($act=="selectall")
        {
        $pos=strpos(strtoupper($sqlquerytodo),"WHERE");
        if (is_integer($pos))
        {
          $selquery="UPDATE ".$DbTableName." SET ".$SelectedField."='Yes' ".substr($sqlquerytodo,$pos,strlen($sqlquerytodo)-$pos);
          $mysql_result=mysqli_query($mysql_link,$selquery);
        }
        else
        {
          $selquery="UPDATE ".$DbTableName." SET ".$SelectedField."='Yes' ";
          $mysql_result=mysqli_query($mysql_link,$selquery);
        }
      }
        if ($act=="deselectall")
        {
          $pos=strpos(strtoupper($sqlquerytodo),"WHERE");
          if (is_integer($pos))
          {
                  $selquery="UPDATE ".$DbTableName." SET ".$SelectedField."='No' ".substr($sqlquerytodo,$pos,strlen($sqlquerytodo)-$pos);
                    $mysql_result=mysqli_query($mysql_link,$selquery);
          }
          else
          {
                  $selquery="UPDATE ".$DbTableName." SET ".$SelectedField."='No' ";
                    $mysql_result=mysqli_query($mysql_link,$selquery);
          }
      }
        if (($sortf!="") && ($sortf!=""))
        $sortquery=" ORDER BY ".$sortf." ".$sortd;
        $sqllimit=" LIMIT ".$tablestart.",".$ShowRows;
        $mysql_result=mysqli_query($mysql_link,str_replace("SELECT","SELECT SQL_CALC_FOUND_ROWS",$sqlquerytodo).$sortquery.$sqllimit);
        if ($mysql_result!=false)
        {
        $sqlnum=mysqli_query($mysql_link,"SELECT FOUND_ROWS() AS `found_rows`;");
        $rows = mysqli_fetch_array($sqlnum,MYSQLI_ASSOC);
        $numrows = $rows['found_rows'];
      }  
        else
        {
          $numrows=0;
        $queryerror=true;
        } 
          if ($tablestart>=$numrows)
            $tablestart=$numrows-$ShowRows;
        for ($k=0;$k<$ShowRows;$k++)
        {
          $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
          if ($row!=false)
          {
          $Selectedarray[$k]=$row[$SelectedField];
          $Createdarray[$k]=$row[$CreatedField];
          $Usernamearray[$k]=$row[$UsernameField];
          $Passwordarray[$k]=$row[$PasswordField];
          $Enabledarray[$k]=$row[$EnabledField];
          $Namearray[$k]=$row[$NameField];
          $Emailarray[$k]=$row[$EmailField];
          $Usergroupsarray[$k]=$row[$UsergroupsField];
          $Cus1array[$k]=$row[$Custom1Field];
          $Cus2array[$k]=$row[$Custom2Field];
          $Cus3array[$k]=$row[$Custom3Field];
          $Cus4array[$k]=$row[$Custom4Field];
          $Cus5array[$k]=$row[$Custom5Field];
          $Cus6array[$k]=$row[$Custom6Field];
          $Cus7array[$k]=$row[$Custom7Field];
          $Cus8array[$k]=$row[$Custom8Field];
          $Cus9array[$k]=$row[$Custom9Field];
          $Cus10array[$k]=$row[$Custom10Field];
          $Cus11array[$k]=$row[$Custom11Field];
          $Cus12array[$k]=$row[$Custom12Field];
          $Cus13array[$k]=$row[$Custom13Field];
          $Cus14array[$k]=$row[$Custom14Field];
          $Cus15array[$k]=$row[$Custom15Field];
          $Cus16array[$k]=$row[$Custom16Field];
          $Cus17array[$k]=$row[$Custom17Field];
          $Cus18array[$k]=$row[$Custom18Field];
          $Cus19array[$k]=$row[$Custom19Field];
          $Cus20array[$k]=$row[$Custom20Field];
          $Cus21array[$k]=$row[$Custom21Field];
          $Cus22array[$k]=$row[$Custom22Field];
          $Cus23array[$k]=$row[$Custom23Field];
          $Cus24array[$k]=$row[$Custom24Field];
          $Cus25array[$k]=$row[$Custom25Field];
          $Cus26array[$k]=$row[$Custom26Field];
          $Cus27array[$k]=$row[$Custom27Field];
          $Cus28array[$k]=$row[$Custom28Field];
          $Cus29array[$k]=$row[$Custom29Field];
          $Cus30array[$k]=$row[$Custom30Field];
          $Cus31array[$k]=$row[$Custom31Field];
          $Cus32array[$k]=$row[$Custom32Field];
          $Cus33array[$k]=$row[$Custom33Field];
          $Cus34array[$k]=$row[$Custom34Field];
          $Cus35array[$k]=$row[$Custom35Field];
          $Cus36array[$k]=$row[$Custom36Field];
          $Cus37array[$k]=$row[$Custom37Field];
          $Cus38array[$k]=$row[$Custom38Field];
          $Cus39array[$k]=$row[$Custom39Field];
          $Cus40array[$k]=$row[$Custom40Field];
          $Cus41array[$k]=$row[$Custom41Field];
          $Cus42array[$k]=$row[$Custom42Field];
          $Cus43array[$k]=$row[$Custom43Field];
          $Cus44array[$k]=$row[$Custom44Field];
          $Cus45array[$k]=$row[$Custom45Field];
          $Cus46array[$k]=$row[$Custom46Field];
          $Cus47array[$k]=$row[$Custom47Field];
          $Cus48array[$k]=$row[$Custom48Field];
          $Cus49array[$k]=$row[$Custom49Field];
          $Cus50array[$k]=$row[$Custom50Field];
          $Useridarray[$k]=$row[$IdField];         
          }
        }
      }
      else
      {
        for ($k=0;$k<$ShowRows;$k++)
        {
          $Selectedarray[$k]="";
          $Createdarray[$k]="";
          $Usernamearray[$k]="";
          $Passwordarray[$k]="";
          $Enabledarray[$k]="";
          $Namearray[$k]="";
          $Emailarray[$k]="";
          $Usergroupsarray[$k]="";
        $Cus1array[$k]="";
        $Cus2array[$k]="";
        $Cus3array[$k]="";
        $Cus4array[$k]="";
        $Cus5array[$k]="";
        $Cus6array[$k]="";
        $Cus7array[$k]="";
        $Cus8array[$k]="";
        $Cus9array[$k]="";
        $Cus10array[$k]="";
        $Cus11array[$k]="";
        $Cus12array[$k]="";
        $Cus13array[$k]="";
        $Cus14array[$k]="";
        $Cus15array[$k]="";
        $Cus16array[$k]="";
        $Cus17array[$k]="";
        $Cus18array[$k]="";
        $Cus19array[$k]="";
        $Cus20array[$k]="";
        $Cus21array[$k]="";
        $Cus22array[$k]="";
        $Cus23array[$k]="";
        $Cus24array[$k]="";
        $Cus25array[$k]="";
        $Cus26array[$k]="";
        $Cus27array[$k]="";
        $Cus28array[$k]="";
        $Cus29array[$k]="";
        $Cus30array[$k]="";
        $Cus31array[$k]="";
        $Cus32array[$k]="";
        $Cus33array[$k]="";
        $Cus34array[$k]="";
        $Cus35array[$k]="";
        $Cus36array[$k]="";
        $Cus37array[$k]="";
        $Cus38array[$k]="";
        $Cus39array[$k]="";
        $Cus40array[$k]="";
        $Cus41array[$k]="";
        $Cus42array[$k]="";
        $Cus43array[$k]="";
        $Cus44array[$k]="";
        $Cus45array[$k]="";
        $Cus46array[$k]="";
        $Cus47array[$k]="";
        $Cus48array[$k]="";
        $Cus49array[$k]="";
        $Cus50array[$k]=""; 
        $Useridarray[$k]="";         
        }
      }
  }
  // See how many records are selected
  $selquery="SELECT count(*) FROM ".$DbTableName." WHERE ".$SelectedField."='Yes'";
  $mysql_result=mysqli_query($mysql_link,$selquery);
  $row = mysqli_fetch_row($mysql_result);
  if ($row!=false)
    $numselected = $row[0];
  else  
    $numselected = 0;
  // Store numselected in session so other pages have access
  $_SESSION['slnumselected']=$numselected;
  if ($queryerror)
  {
    $numrows=0;
    $Usernamearray=array();
    $jsonmessage=ADMINMSG_INVALIDQUERY;
  }
?>
{
  "draw": <?php echo $_POST['draw']; ?>,
  "recordsTotal": <?php echo $totalrows; ?>,
  "recordsFiltered": <?php echo $numrows; ?>,
  "slnumselected": <?php echo $numselected; ?>,
  "slquicksearch": "<?php echo str_replace('"','\"',$quicksearch); ?>",
  "slexpwithin": "<?php echo $expwithin; ?>",
  "slexpwithindays": "<?php echo $expwithindays; ?>",
  "slmemberof": "<?php echo $memberof; ?>",
  "slunexpmemberof": "<?php echo $unexpmemberof; ?>",
  "slexpmemberof": "<?php echo $expmemberof; ?>",
  "sljoinwithin": "<?php echo $joinwithin; ?>",
  "slonlyselected": "<?php echo $onlyselected; ?>",
  "slfilfield1": "<?php echo $filfield1; ?>",
  "slfilcond1": "<?php echo $filcond1; ?>",
  "slfildata1": "<?php echo $fildata1; ?>",
  "slfilbool1": "<?php echo $filbool1; ?>",
  "slfilfield2": "<?php echo $filfield2; ?>",
  "slfilcond2": "<?php echo $filcond2; ?>",
  "slfildata2": "<?php echo $fildata2; ?>",
  "slfilbool2": "<?php echo $filbool2; ?>",
  "slfilfield3": "<?php echo $filfield3; ?>",
  "slfilcond3": "<?php echo $filcond3; ?>",
  "slfildata3": "<?php echo $fildata3; ?>",
  "slfilbool3": "<?php echo $filbool3; ?>",
  "slfilfield4": "<?php echo $filfield4; ?>",
  "slfilcond4": "<?php echo $filcond4; ?>",
  "slfildata4": "<?php echo $fildata4; ?>",
  "slsqlinput": "<?php echo str_replace('"','\"',$sqlquery); ?>",  
  "slfiltertype": "<?php echo $_SESSION['filtertype']; ?>",
  "slmessage": "<?php echo $jsonmessage; ?>",

  "data": [
<?php
  $jsonusers=$ShowRows;
  if (count($Usernamearray)<$ShowRows)
    $jsonusers=count($Usernamearray);
  for ($k=0;$k<$jsonusers;$k++)
  {
?>
    [
<?php
  $columnordlen=strlen($ColumnOrder);   
  for ($col=0;$col<$columnordlen;$col=$col+2)
  {
    $coltag=substr($ColumnOrder,$col,2);
    switch ($coltag)
    {
      case "AC":
      echo "      \"".lengthlimit(htmlentities($Useridarray[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255).",".lengthlimit(htmlentities($Usernamearray[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "SL":
        echo "      \"".lengthlimit(htmlentities($Selectedarray[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255).",".lengthlimit(htmlentities($Useridarray[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "CR":
        if (($DateFormat=="DDMMYY") && (strlen($Createdarray[$k])==6))
          $cdate=substr($Createdarray[$k],4,2)."/".substr($Createdarray[$k],2,2)."/".substr($Createdarray[$k],0,2);
        else
          $cdate=substr($Createdarray[$k],2,2)."/".substr($Createdarray[$k],4,2)."/".substr($Createdarray[$k],0,2);  
        echo "      \"".lengthlimit(htmlentities($cdate,ENT_QUOTES,strtoupper($MetaCharSet)),255)."&nbsp;</nobr>\"";
        break;
      case "US";
        echo "      \"".lengthlimit(htmlentities($Usernamearray[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "PW";
        if (($slsubadmin) && ((trim(strtoupper(substr($Usergroupsarray[$k],0,5)=="ADMIN"))) || (strpos(trim(strtoupper($Usergroupsarray[$k])),"^ADMIN")!==false)))
          echo "      \""."*****"."\"";          
        else
          echo "      \"".lengthlimit(htmlentities($Passwordarray[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "EN":  
        echo "      \"".lengthlimit(htmlentities($Enabledarray[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "NM":  
        echo "      \"".lengthlimit(htmlentities($Namearray[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "EM":  
        echo "      \"".lengthlimit(htmlentities($Emailarray[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "UG":  
        echo "      \"".lengthlimit(htmlentities($Usergroupsarray[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "ID":  
        echo "      \"".lengthlimit(htmlentities($Useridarray[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "01":  
        echo "      \"".lengthlimit(htmlentities($Cus1array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "02":  
        echo "      \"".lengthlimit(htmlentities($Cus2array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "03":  
        echo "      \"".lengthlimit(htmlentities($Cus3array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "04":  
        echo "      \"".lengthlimit(htmlentities($Cus4array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "05":  
        echo "      \"".lengthlimit(htmlentities($Cus5array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "06":  
        echo "      \"".lengthlimit(htmlentities($Cus6array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "07":  
        echo "      \"".lengthlimit(htmlentities($Cus7array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "08":  
        echo "      \"".lengthlimit(htmlentities($Cus8array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "09":  
        echo "      \"".lengthlimit(htmlentities($Cus9array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "10":  
        echo "      \"".lengthlimit(htmlentities($Cus10array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "11":  
        echo "      \"".lengthlimit(htmlentities($Cus11array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "12":  
        echo "      \"".lengthlimit(htmlentities($Cus12array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "13":  
        echo "      \"".lengthlimit(htmlentities($Cus13array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "14":  
        echo "      \"".lengthlimit(htmlentities($Cus14array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "15":  
        echo "      \"".lengthlimit(htmlentities($Cus15array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "16":  
        echo "      \"".lengthlimit(htmlentities($Cus16array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "17":  
        echo "      \"".lengthlimit(htmlentities($Cus17array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "18":  
        echo "      \"".lengthlimit(htmlentities($Cus18array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "19":  
        echo "      \"".lengthlimit(htmlentities($Cus19array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "20":  
        echo "      \"".lengthlimit(htmlentities($Cus20array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "21":  
        echo "      \"".lengthlimit(htmlentities($Cus21array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "22":  
        echo "      \"".lengthlimit(htmlentities($Cus22array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "23":  
        echo "      \"".lengthlimit(htmlentities($Cus23array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "24":  
        echo "      \"".lengthlimit(htmlentities($Cus24array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "25":  
        echo "      \"".lengthlimit(htmlentities($Cus25array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "26":  
        echo "      \"".lengthlimit(htmlentities($Cus26array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "27":  
        echo "      \"".lengthlimit(htmlentities($Cus27array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "28":  
        echo "      \"".lengthlimit(htmlentities($Cus28array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "29":  
        echo "      \"".lengthlimit(htmlentities($Cus29array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "30":  
        echo "      \"".lengthlimit(htmlentities($Cus30array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "31":  
        echo "      \"".lengthlimit(htmlentities($Cus31array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "32":  
        echo "      \"".lengthlimit(htmlentities($Cus32array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "33":  
        echo "      \"".lengthlimit(htmlentities($Cus33array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "34":  
        echo "      \"".lengthlimit(htmlentities($Cus34array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "35":  
        echo "      \"".lengthlimit(htmlentities($Cus35array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "36":  
        echo "      \"".lengthlimit(htmlentities($Cus36array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "37":  
        echo "      \"".lengthlimit(htmlentities($Cus37array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "38":  
        echo "      \"".lengthlimit(htmlentities($Cus38array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "39":  
        echo "      \"".lengthlimit(htmlentities($Cus39array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "40":  
        echo "      \"".lengthlimit(htmlentities($Cus40array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "41":  
        echo "      \"".lengthlimit(htmlentities($Cus41array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "42":  
        echo "      \"".lengthlimit(htmlentities($Cus42array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "43":  
        echo "      \"".lengthlimit(htmlentities($Cus43array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "44":  
        echo "      \"".lengthlimit(htmlentities($Cus44array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "45":  
        echo "      \"".lengthlimit(htmlentities($Cus45array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "46":  
        echo "      \"".lengthlimit(htmlentities($Cus46array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "47":  
        echo "      \"".lengthlimit(htmlentities($Cus47array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "48":  
        echo "      \"".lengthlimit(htmlentities($Cus48array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "49":  
        echo "      \"".lengthlimit(htmlentities($Cus49array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
      case "50":  
        echo "      \"".lengthlimit(htmlentities($Cus50array[$k],ENT_QUOTES,strtoupper($MetaCharSet)),255)."\"";
        break;
    }
        if ($col!=($columnordlen-2))
          echo ",";
        echo "\n";
    }  
        echo "    ]";
         if ($k<($jsonusers-1)) echo ",";
         echo "\n";
    }
?>  ]
}
<?php
  function lengthlimit($s,$l)
  {
   $s=substr($s,0,$l);
   $s=preg_replace('/[\x00-\x1F\x7F]/', '', $s);
   return($s);
  }
  function clearFilters()
  {
    global $quicksearch,$expwithin,$expwithindays,$memberof,$unexpmemberof,$expmemberof,$joinwithin,$onlyselected,$sqlinput,$filteron;
    global $filfield1,$filcond1,$fildata1,$filbool1,$filfield2,$filcond2,$fildata2,$filbool2,$filfield3,$filcond3,$fildata3,$filbool3,$filfield4,$filcond4,$fildata4;
    $_SESSION['quicksearch']="";
    $_SESSION['expwithin']="";
    $_SESSION['expwithindays']="";
    $_SESSION['memberof']="";
    $_SESSION['memberof']="";
    $_SESSION['unexpmemberof']="";
    $_SESSION['expmemberof']="";
    $_SESSION['joinwithin']="";
    $_SESSION['onlyselected']="";
    $_SESSION['sqlinput']="";
    $_SESSION['filfield1']="";
    $_SESSION['filfield2']="";
    $_SESSION['filfield3']="";
    $_SESSION['filfield4']="";
    $_SESSION['filcond1']="";
    $_SESSION['filcond2']="";
    $_SESSION['filcond3']="";
    $_SESSION['filcond4']="";
    $_SESSION['fildata1']="";
    $_SESSION['fildata2']="";
    $_SESSION['fildata3']="";
    $_SESSION['fildata4']="";
    $_SESSION['filbool1']="";
    $_SESSION['filbool2']="";
    $_SESSION['filbool3']="";
    $_SESSION['filteron']=0;
    $quicksearch="";
    $expwithin="";
    $expwithindays="";
    $memberof="";
    $unexpmemberof="";
    $expmemberof="";
    $joinwithin="";
    $onlyselected="";
    $filfield1="";
    $filfield2="";
    $filfield3="";
    $filfield4="";
    $filcond1="";
    $filcond2="";
    $filcond3="";
    $filcond4="";
    $fildata1="";
    $fildata2="";
    $fildata3="";
    $fildata4="";
    $filbool1="";
    $filbool2="";
    $filbool3="";
    $sqlinput="";    
    $filteron=0;    
    $_SESSION['filtertype']="";
  }
?>
