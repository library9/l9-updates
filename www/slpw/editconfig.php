<?php
  $groupswithaccess="ADMIN";
  //$noaccesspage="";
  $dbupdate=true;
  require("sitelokpw.php");
  
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  if (!defined('ADMINCF_EMAILREPLYTO'))
    define("ADMINCF_EMAILREPLYTO","Reply to email override");  
  if (!defined('ADMINBUTTON_TESTEMAIL'))
    define("ADMINBUTTON_TESTEMAIL","Send test email");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  include("admincommonfunctions.php");
  // Get current values
  $newsitename=$SiteName;
  $newsiteemail=$SiteEmail;
  $newsiteemail2=$SiteEmail2;
  $newdateformat=$DateFormat;
  $newlogoutpage=$LogoutPage;
  $newsiteloklocation=$SitelokLocation;
  $newsiteloklocationurl=$SitelokLocationURL;
  $newemaillocation=$EmailLocation;
  $newemailurl=$EmailURL;
  $newbackuplocation=$BackupLocation;
  if ($DemoMode)
    $newbackuplocation="Hidden in demo mode";
  $newfilelocation=$FileLocation;
  // Convert ; character splitting S3 location to | used in Linklok and Sitelok  
  $newfilelocation=str_replace(";","|",$FileLocation);
  if ($DemoMode)
    $newfilelocation="Hidden in demo mode";
  for ($k=0;$k<count($FileLocations);$k++)
  {
    $elements=each($FileLocations);
    $var1="newfilelocationsname".$k;
    $var2="newfilelocations".$k;
    $$var1=$elements[0];
    $$var2=$elements[1];
    // Convert ; character splitting S3 location to | used in Linklok and Sitelok
    $$var2=str_replace(";","|",$$var2);
    if ($DemoMode)
      $$var2="Hidden in demo mode";      
  }
  $newsiteloklog=$SitelokLog;
  if (substr($LogDetails,0,1)=="Y")
    $newlogentry1="on";
  if (substr($LogDetails,1,1)=="Y")
    $newlogentry2="on";
  if (substr($LogDetails,2,1)=="Y")
    $newlogentry3="on";
  if (substr($LogDetails,3,1)=="Y")
    $newlogentry4="on";
  if (substr($LogDetails,4,1)=="Y")
    $newlogentry5="on";
  if (substr($LogDetails,5,1)=="Y")
    $newlogentry6="on";
  if (substr($LogDetails,6,1)=="Y")
    $newlogentry7="on";
  if (substr($LogDetails,7,1)=="Y")
    $newlogentry8="on";
  if (substr($LogDetails,8,1)=="Y")
    $newlogentry9="on";
  if (substr($LogDetails,9,1)=="Y")
    $newlogentry10="on";
  $newsitekey=$SiteKey;
  if ($DemoMode)
    $newsitekey="Hidden in demo mode";    
  $newlogintype=$LoginType;
  $newturinglogin=$TuringLogin;
  $newturingregister=$TuringRegister;
  $newmaxsessiontime=$MaxSessionTime;
  $newmaxinactivitytime=$MaxInactivityTime;
  $newcookielogin=$CookieLogin;
  $newlogintemplate=$LoginPage;
  $newnoaccesspage=$NoAccessPage;            
  $newexpiredpage=$ExpiredPage;
  $newwronggrouppage=$WrongGroupPage;
  $newmessagetemplate=$MessagePage;
  $newforgottenemail=$ForgottenEmail;
  if ($newforgottenemail=="")
    $forgotemailtype="default";
  else
    $forgotemailtype="template";
  $newshowrows=$ShowRows;
  // Split column order into variables
  for ($c=0;$c<strlen($ColumnOrder);$c++)
  {
    $coltag=substr($ColumnOrder,$c*2,2);
    if ($coltag=="AC") $newactioncolumn=$c+1;
    if ($coltag=="SL") $newselectedcolumn=$c+1;
    if ($coltag=="CR") $newcreatedcolumn=$c+1;
    if ($coltag=="US") $newusernamecolumn=$c+1;
    if ($coltag=="PW") $newpasswordcolumn=$c+1;
    if ($coltag=="EN") $newenabledcolumn=$c+1;
    if ($coltag=="NM") $newnamecolumn=$c+1;
    if ($coltag=="EM") $newemailcolumn=$c+1;
    if ($coltag=="UG") $newusergroupscolumn=$c+1;
    if ($coltag=="ID") $newuseridcolumn=$c+1;
    for($k=1;$k<=50;$k++)
    {
      if ($k<10) $val="0".$k; else $val=$k;
      $var="newcustom".$k."column";
      if ($coltag==$val) $$var=$c+1;
    }
  }
  $newrandompasswordmask=$RandomPasswordMask;
  if ($MD5passwords)
    $newmd5passwords="1";
  else
    $newmd5passwords="0";
  $newprofilepassrequired=$ProfilePassRequired; 
  $newemailconfirmrequired=$EmailConfirmRequired;
  $newemailconfirmtemplate=$EmailConfirmTemplate;
  $newemailunique=$EmailUnique;
  $newloginwithemail=$LoginWithEmail;
  if ($ConcurrentLogin)
    $newconcurrentlogin="1";
  else
    $newconcurrentlogin="0";  
  $newcustomtitle1=$CustomTitle1;  
  $newcustomtitle2=$CustomTitle2;  
  $newcustomtitle3=$CustomTitle3;  
  $newcustomtitle4=$CustomTitle4;  
  $newcustomtitle5=$CustomTitle5;  
  $newcustomtitle6=$CustomTitle6;  
  $newcustomtitle7=$CustomTitle7;  
  $newcustomtitle8=$CustomTitle8;  
  $newcustomtitle9=$CustomTitle9;  
  $newcustomtitle10=$CustomTitle10;  
  $newcustomtitle11=$CustomTitle11;  
  $newcustomtitle12=$CustomTitle12;  
  $newcustomtitle13=$CustomTitle13;  
  $newcustomtitle14=$CustomTitle14;  
  $newcustomtitle15=$CustomTitle15;  
  $newcustomtitle16=$CustomTitle16;  
  $newcustomtitle17=$CustomTitle17;  
  $newcustomtitle18=$CustomTitle18;  
  $newcustomtitle19=$CustomTitle19;  
  $newcustomtitle20=$CustomTitle20;  
  $newcustomtitle21=$CustomTitle21;  
  $newcustomtitle22=$CustomTitle22;  
  $newcustomtitle23=$CustomTitle23;  
  $newcustomtitle24=$CustomTitle24;  
  $newcustomtitle25=$CustomTitle25;  
  $newcustomtitle26=$CustomTitle26;  
  $newcustomtitle27=$CustomTitle27;  
  $newcustomtitle28=$CustomTitle28;  
  $newcustomtitle29=$CustomTitle29;  
  $newcustomtitle30=$CustomTitle30;  
  $newcustomtitle31=$CustomTitle31;  
  $newcustomtitle32=$CustomTitle32;  
  $newcustomtitle33=$CustomTitle33;  
  $newcustomtitle34=$CustomTitle34;  
  $newcustomtitle35=$CustomTitle35;  
  $newcustomtitle36=$CustomTitle36;  
  $newcustomtitle37=$CustomTitle37;  
  $newcustomtitle38=$CustomTitle38;  
  $newcustomtitle39=$CustomTitle39;  
  $newcustomtitle40=$CustomTitle40;  
  $newcustomtitle41=$CustomTitle41;  
  $newcustomtitle42=$CustomTitle42;  
  $newcustomtitle43=$CustomTitle43;  
  $newcustomtitle44=$CustomTitle44;  
  $newcustomtitle45=$CustomTitle45;  
  $newcustomtitle46=$CustomTitle46;  
  $newcustomtitle47=$CustomTitle47;  
  $newcustomtitle48=$CustomTitle48;  
  $newcustomtitle49=$CustomTitle49;  
  $newcustomtitle50=$CustomTitle50;             
  $newcustom1validate=$Custom1Validate;
  $newcustom2validate=$Custom2Validate;
  $newcustom3validate=$Custom3Validate;
  $newcustom4validate=$Custom4Validate;
  $newcustom5validate=$Custom5Validate;
  $newcustom6validate=$Custom6Validate;
  $newcustom7validate=$Custom7Validate;
  $newcustom8validate=$Custom8Validate;
  $newcustom9validate=$Custom9Validate;
  $newcustom10validate=$Custom10Validate;
  $newcustom11validate=$Custom11Validate;
  $newcustom12validate=$Custom12Validate;
  $newcustom13validate=$Custom13Validate;
  $newcustom14validate=$Custom14Validate;
  $newcustom15validate=$Custom15Validate;
  $newcustom16validate=$Custom16Validate;
  $newcustom17validate=$Custom17Validate;
  $newcustom18validate=$Custom18Validate;
  $newcustom19validate=$Custom19Validate;
  $newcustom20validate=$Custom20Validate;
  $newcustom21validate=$Custom21Validate;
  $newcustom22validate=$Custom22Validate;
  $newcustom23validate=$Custom23Validate;
  $newcustom24validate=$Custom24Validate;
  $newcustom25validate=$Custom25Validate;
  $newcustom26validate=$Custom26Validate;
  $newcustom27validate=$Custom27Validate;
  $newcustom28validate=$Custom28Validate;
  $newcustom29validate=$Custom29Validate;
  $newcustom30validate=$Custom30Validate;
  $newcustom31validate=$Custom31Validate;
  $newcustom32validate=$Custom32Validate;
  $newcustom33validate=$Custom33Validate;
  $newcustom34validate=$Custom34Validate;
  $newcustom35validate=$Custom35Validate;
  $newcustom36validate=$Custom36Validate;
  $newcustom37validate=$Custom37Validate;
  $newcustom38validate=$Custom38Validate;
  $newcustom39validate=$Custom39Validate;
  $newcustom40validate=$Custom40Validate;
  $newcustom41validate=$Custom41Validate;
  $newcustom42validate=$Custom42Validate;
  $newcustom43validate=$Custom43Validate;
  $newcustom44validate=$Custom44Validate;
  $newcustom45validate=$Custom45Validate;
  $newcustom46validate=$Custom46Validate;
  $newcustom47validate=$Custom47Validate;
  $newcustom48validate=$Custom48Validate;
  $newcustom49validate=$Custom49Validate;
  $newcustom50validate=$Custom50Validate;
  $newemailtype=$EmailType;
  $newemailreplyto=$EmailReplyTo;               
  $newemailusername=$EmailUsername;             
  $newemailpassword=$EmailPassword;             
  $newemailserver=$EmailServer; 
  $newemailport=$EmailPort; 
  $newemailauth=$EmailAuth;
  $newemailserversecurity=$EmailServerSecurity;
  $newemaildelay=$EmailDelay;
  $newdbupdate=$DBupdate;
  $newallowsearchengine=$AllowSearchEngine;
  $newsearchenginegroup=$SearchEngineGroup;
?>
<!DOCTYPE html>
<html>
<head>
<?php
$pagename="editconfig";
include("adminhead.php");
?>
<title><?php echo ADMINMENU_CONFIGURATION; ?></title>
<link rel="stylesheet" type="text/css" href="fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />
<link rel="stylesheet" href="editconfig.css"></head>
<?php include("adminthemeheader.php"); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-cogs"></i>&nbsp;<?php echo ADMINMENU_CONFIGURATION; ?> V<?php echo $SitelokVersion; ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><?php echo ADMINMENU_CONFIGURATION; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>

          <!-- Your Page Content Here -->
          <div class="row">
            <div class="col-xs-12">




              <div class="box">

                <div class="box-body">


            <div class="tabbable">
                <ul class="nav nav-pills nav-stacked col-md-3 col-lg-2">
                            <li class="active"><a href="#general" data-toggle="tab"><?php echo ADMINCF_GENSET; ?></a></li>
                            <li><a href="#passwords" data-toggle="tab"><?php echo ADMINCF_PASSWORDS; ?></a></li>
                            <li><a href="#pagestemplates" data-toggle="tab"><?php echo ADMINCF_PAGESET; ?></a></li>
                            <li><a href="#logsettings" data-toggle="tab"><?php echo ADMINCF_LOGSET; ?></a></li>
                            <li><a href="#controlpanel" data-toggle="tab"><?php echo ADMINCF_CPSET; ?></a></li>
                            <li><a href="#customfields" data-toggle="tab"><?php echo ADMINGENERAL_CUSTOMFIELDS; ?></a></li>
                            <li><a href="#download" data-toggle="tab"><?php echo ADMINCF_DWNPATHS; ?></a></li>
                            <li><a href="#email" data-toggle="tab"><?php echo ADMINCF_EMAILSET; ?></a></li>
                            <li><a href="#paths" data-toggle="tab"><?php echo ADMINCF_INSTPTHS; ?></a></li>
                        </ul>

            <div class="col-xs-12 hidden-md hidden-lg">
            <hr class="settings">
            </div>

            <div class="tab-content col-md-9 col-lg-10">

              <div class="tab-pane fade in active" id="general">
                <h4 class="settingstitle"><?php echo ADMINCF_GENSET; ?></h4>
                <form name="generalform" id="generalform" role="form" class="form-horizontal" action="" target="" method="POST">

                <div class="form-group">
                  <label class="col-xs-12" for="newsitename"><?php echo ADMINCF_SITENAME; ?></label>
                  <div class="col-xs-12 col-sm-7 col-md-5 col-lg-4" id="newsitenamediv">
                    <input type="text" class="form-control" name="newsitename" id="newsitename" maxlength="255" value="<?php echo $newsitename; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div>                

                <div class="form-group">
                  <label class="col-xs-12" for="newsiteemail"><?php echo ADMINCF_SITEEMAIL; ?></label>
                  <div class="col-xs-12 col-sm-7 col-md-5 col-lg-4" id="newsiteemaildiv">
                    <input type="text" class="form-control" name="newsiteemail" id="newsiteemail" maxlength="255" value="<?php echo $newsiteemail; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div>                

                <div class="form-group">
                  <label class="col-xs-12" for="newsiteemail2"><?php echo "Admin email (secondary)"; ?></label>
                  <div class="col-xs-12 col-sm-7 col-md-5 col-lg-4" id="newsiteemail2div">
                    <input type="text" class="form-control" name="newsiteemail2" id="newsiteemail2" maxlength="255" value="<?php echo $newsiteemail2; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <div class="col-xs-12 col-sm-5 col-md-7 col-lg-8">
                    <p class="helptext"><?php echo ADMINCF_OPTIONAL; ?></p>
                  </div>
                </div>  

                <div class="form-group">
                  <label class="col-xs-12" for="newdateformat"><?php echo ADMINCF_SITEDATEFMT; ?></label>
                   <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2" id="newdateformatdiv">
                      <select id="newdateformat" name="newdateformat" class="form-control selectpicker">
                      <option value="DDMMYY" <?php if ($DateFormat=="DDMMYY") print "selected=\"selected\"";?>>DDMMYY</option>
                      <option value="MMDDYY" <?php if ($DateFormat=="MMDDYY") print "selected=\"selected\"";?>>MMDDYY</option>
                      </select>  
                  </div>
                  <div class="col-xs-12 col-sm-9 col-md-9 col-lg-10">
                    <p class="helptext"><?php echo ADMINCF_DONTCHANGE; ?></p>
                  </div>
                </div> 

                <div class="form-group">
                  <label class="col-xs-12" for="newsitekey"><?php echo ADMINCF_SITEKEY; ?></label>
                  <div class="col-xs-12 col-sm-7 col-md-5 col-lg-4" id="newsitekeydiv">
                    <input type="text" class="form-control" name="newsitekey" id="newsitekey" maxlength="50" value="<?php echo $newsitekey; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <div class="col-xs-12 col-sm-5 col-md-7 col-lg-8">
                    <p class="helptext"><?php echo ADMINCF_DONTCHANGE; ?></p>
                  </div>
                </div>                

                <div class="form-group">
                  <label class="col-xs-12" for="newmaxsessiontime"><?php echo ADMINCF_MAXSESS; ?></label>
                  <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3" id="newmaxsessiontimediv">
                    <div class="input-group">
                      <input type="number" id="newmaxsessiontime" name="newmaxsessiontime" class="form-control" value="<?php echo $newmaxsessiontime; ?>" maxlength="7" min="0" max="9999999">
                      <span class="input-group-addon"><?php echo ADMINGENERAL_SECONDS; ?></span>
                    </div> 
                  </div>
                  <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                    <p class="helptext"><?php echo ADMINCF_ZEROMAX; ?></p>
                  </div>
                </div>  

                <div class="form-group">
                  <label class="col-xs-12" for="newmaxinactivitytime"><?php echo ADMINCF_MAXINACT; ?></label>
                  <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3" id="newmaxinactivitytimediv">
                    <div class="input-group">
                      <input type="number" id="newmaxinactivitytime" name="newmaxinactivitytime" class="form-control" value="<?php echo $newmaxinactivitytime; ?>" maxlength="7" min="0" max="9999999">
                      <span class="input-group-addon"><?php echo ADMINGENERAL_SECONDS; ?></span>
                    </div> 
                  </div>
                  <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                    <p class="helptext"><?php echo ADMINCF_ZEROMAX; ?></p>
                  </div>
                </div>  

                <div class="form-group">
                  <label class="col-xs-12" for="newturinglogin"><?php echo ADMINCF_LOGINCAP; ?></label>
                   <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2" id="newturinglogindiv">
                      <select id="newturinglogin" name="newturinglogin" class="form-control selectpicker">
                      <option value="0" <?php if ($newturinglogin=="0") print "selected=\"selected\""; ?>><?php echo ADMINGENERAL_DISABLED; ?></option>
                      <option value="1" <?php if ($newturinglogin=="1") print "selected=\"selected\""; ?>><?php echo ADMINGENERAL_ENABLED; ?></option>
                      </select>  
                  </div>
                </div> 

                <div class="form-group">
                  <label class="col-xs-12" for="newturingregister"><?php echo ADMINCF_REGCAP; ?></label>
                   <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2" id="newturingregisterdiv">
                      <select id="newturingregister" name="newturingregister" class="form-control selectpicker">
                      <option value="0" <?php if ($newturingregister=="0") print "selected=\"selected\""; ?>><?php echo ADMINGENERAL_DISABLED; ?></option>
                      <option value="1" <?php if ($newturingregister=="1") print "selected=\"selected\""; ?>><?php echo ADMINGENERAL_ENABLED; ?></option>
                      </select>  
                  </div>
                </div> 

                <div class="form-group">
                  <label class="col-xs-12" for="newcookielogin"><?php echo ADMINCF_REMME; ?></label>
                   <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="newcookielogindiv">
                      <select id="newcookielogin" name="newcookielogin" class="form-control selectpicker">
                      <option value="0" <?php if ($newcookielogin=="0") print "selected=\"selected\"";?>><?php echo ADMINGENERAL_DISABLED; ?></option>
                      <option value="1" <?php if ($newcookielogin=="1") print "selected=\"selected\"";?>><?php echo ADMINCF_REMME; ?></option>
                      <option value="2" <?php if ($newcookielogin=="2") print "selected=\"selected\"";?>><?php echo ADMINCF_AUTOLOGIN; ?></option>
                      </select>  
                  </div>
                </div> 

                <div class="form-group">
                  <label class="col-xs-12" for="newconcurrentlogin"><?php echo ADMINCF_CONCLOGIN; ?></label>
                   <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2" id="newconcurrentlogindiv">
                      <select id="newconcurrentlogin" name="newconcurrentlogin" class="form-control selectpicker">
                      <option value="0" <?php if ($newconcurrentlogin=="0") print "selected=\"selected\"";?>><?php echo ADMINGENERAL_NO; ?></option>
                      <option value="1" <?php if ($newconcurrentlogin=="1") print "selected=\"selected\"";?>><?php echo ADMINGENERAL_YES; ?></option>
                      </select>  
                  </div>
                </div> 

                <div class="form-group">
                  <label class="col-xs-12" for="newdbupdate"><?php echo ADMINCF_FORCEDB; ?></label>
                   <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2" id="newdbupdatediv">
                      <select id="newdbupdate" name="newdbupdate" class="form-control selectpicker">
                      <option value="0" <?php if ($newdbupdate=="0") print "selected=\"selected\"";?>><?php echo ADMINGENERAL_NO; ?></option>
                      <option value="1" <?php if ($newdbupdate=="1") print "selected=\"selected\"";?>><?php echo ADMINGENERAL_YES; ?></option>
                      </select>  
                  </div>
                  <div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
                    <p class="helptext"><?php echo ADMINCF_FORCEDBNT; ?></p>
                  </div>
                </div> 

                <div class="form-group">
                  <label class="col-xs-12" for="newallowsearchengine"><?php echo ADMINCF_ALLOWSRCH; ?></label>
                   <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2" id="newallowsearchenginediv">
                      <select id="newallowsearchengine" name="newallowsearchengine" class="form-control selectpicker">
                      <option value="0" <?php if ($newallowsearchengine=="0") print "selected=\"selected\"";?>><?php echo ADMINGENERAL_NO; ?></option>
                      <option value="1" <?php if ($newallowsearchengine=="1") print "selected=\"selected\"";?>><?php echo ADMINGENERAL_YES; ?></option>
                      </select>  
                  </div>
                  <div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
                    <p class="helptext"><?php echo ADMINCF_ALLOWSRCHNT; ?></p>
                  </div>
                </div>
                <div id="searchenginegroupdiv">
                  <div class="form-group">
                    <label class="col-xs-12" for="newsearchenginegroup"><?php echo ADMINCF_SRCHGRP; ?></label>
                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3" id="newsearchenginegroupdiv">
                      <select placeholder="" class="form-control usergrouplist" id="newsearchenginegroup" name="newsearchenginegroup">
                      <?php populateUsergroupSelect(); ?>
                      </select> 
                    </div>
                  <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                    <p class="helptext"><?php echo ADMINCF_SRCHGRPNT; ?></p>
                  </div>
                  </div>                        
                </div>

                <div class="form-group">
                  <label class="col-xs-12" for="newemailconfirmrequired"><?php echo ADMINCF_EMAILCHG; ?></label>
                   <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2" id="newemailconfirmrequireddiv">
                      <select id="newemailconfirmrequired" name="newemailconfirmrequired" class="form-control selectpicker">
                      <option value="0" <?php if ($newemailconfirmrequired=="0") print "selected";?>><?php echo ADMINGENERAL_NO; ?></option>
                      <option value="1" <?php if ($newemailconfirmrequired=="1") print "selected";?>><?php echo ADMINGENERAL_YES; ?></option>
                      </select>  
                  </div>
                  <div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
                    <p class="helptext"><?php echo ADMINCF_EMAILCHGNT; ?></p>
                  </div>
                </div> 
                <div id="newemailconfirmtemplatediv" class="templatename" onclick="openFileManager('newemailconfirmtemplate');">
                <input type="hidden" id="newemailconfirmtemplate" name="newemailconfirmtemplate" value="<?php echo $newemailconfirmtemplate; ?>">
                <span id="newemailconfirmtemplatetext" class="templatename"><?php if ($newemailconfirmtemplate!="") echo $newemailconfirmtemplate; else echo ADMINMSG_NOTEMPLATE; ?></span>&nbsp;&nbsp;<span><i class="glyphicon glyphicon-folder-open actionicon"></i></span>
                </div>

                <div class="form-group">
                  <label class="col-xs-12" for="newemailunique"><?php echo ADMINCF_EMAILUNIQUE; ?></label>
                   <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4" id="newemailuniquediv">
                      <select id="newemailunique" name="newemailunique" class="form-control selectpicker">
                      <option value="0" <?php if ($newemailunique=="0") print "selected";?>>Not required</option>
                      <option value="1" <?php if ($newemailunique=="1") print "selected";?>>Required only for user entry</option>
                      <option value="2" <?php if ($newemailunique=="2") print "selected";?>>Must always be unique</option>
                      </select>  
                  </div>
                </div> 

                <div class="form-group">
                  <label class="col-xs-12" for="newloginwithemail"><?php echo ADMINCF_LOGINEMAIL; ?></label>
                   <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4" id="newloginwithemaildiv">
                      <select id="newloginwithemail" name="newloginwithemail" class="form-control selectpicker">
                      <option value="0" <?php if ($newloginwithemail=="0") print "selected";?>><?php echo ADMINCF_LOGINEMAIL1; ?></option>
                      <option value="1" <?php if ($newloginwithemail=="1") print "selected";?>><?php echo ADMINCF_LOGINEMAIL2; ?></option>
                      </select>  
                  </div>
                </div> 

                <div class="form-group">
                  <div class="col-xs-12">
                    <div class="btn-toolbar">
                        <button type="submit" id="submitgeneral" class="btn btn-primary pull-left" ><?php echo ADMINBUTTON_UPDGENSET; ?></button>
                        <button type="button" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                    </div>   
                  </div>
                </div>
                <div id="resultgeneral"></div>
                </form>
              </div> <!--  general  -->

              <div class="tab-pane fade" id="passwords">
                <h4 class="settingstitle"><?php echo ADMINCF_PASSSET; ?></h4>
                <form name="passwordform" id="passwordform" role="form" class="form-horizontal" action="" target="" method="POST">

                <div class="form-group">
                  <label class="col-xs-12" for="forgotemailtype"><?php echo ADMINCF_FORGOTEM; ?></label>
                   <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="forgotemailtypediv">
                      <select id="forgotemailtype" name="forgotemailtype" class="form-control selectpicker">
                      <option value="default" <?php if ($forgotemailtype=="default") print "selected=\"selected\"";?>><?php echo ADMINCF_DEFAULTEM; ?></option>
                      <option value="template" <?php if ($forgotemailtype=="template") print "selected=\"selected\"";?>><?php echo ADMINBUTTON_SETTEMPLATE; ?></option>
                      </select>  
                  </div>
                </div> 
                <div id="forgotemailtemplatediv" class="templatename" onclick="openFileManager('newforgottenemail');">
                <input type="hidden" id="newforgottenemail" name="newforgottenemail" value="<?php echo $newforgottenemail; ?>">
                <span id="newforgottenemailtext" class="templatename"><?php if ($newforgottenemail!="") echo $newforgottenemail; else echo ADMINMSG_NOTEMPLATE; ?></span>&nbsp;&nbsp;<span><i class="glyphicon glyphicon-folder-open actionicon"></i></span>
                </div>

                <div class="form-group">
                  <label class="col-xs-12" for="newrandompasswordmask"><?php echo ADMINCF_RNDPSSMSK; ?></label>
                  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" id="newrandompasswordmaskdiv">
                    <input type="text" class="form-control" name="newrandompasswordmask" id="newrandompasswordmask" maxlength="50" value="<?php echo $newrandompasswordmask; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <div class="col-xs-12 col-sm-4 col-md-6 col-lg-9">
                    <p class="helptext"><?php echo ADMINCF_RNDPSSMSKEG; ?></p>
                  </div>
                </div> 

                <div class="form-group">
                  <label class="col-xs-12" for="newprofilepassrequired"><?php echo ADMINCF_PROFPASS; ?></label>
                   <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4" id="newprofilepassrequireddiv">
                      <select id="newprofilepassrequired" name="newprofilepassrequired" class="form-control selectpicker">
                      <option value="0" <?php if ($newprofilepassrequired=="0") print "selected";?>><?php echo ADMINCF_PROFPASS1; ?></option>
                      <option value="1" <?php if ($newprofilepassrequired=="1") print "selected";?>><?php echo ADMINCF_PROFPASS2; ?></option>
                      <option value="2" <?php if ($newprofilepassrequired=="2") print "selected";?>><?php echo ADMINCF_PROFPASS3; ?></option>
                      </select>  
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8">
                    <p class="helptext"><?php echo ADMINCF_PROFPASSNT; ?></p>
                  </div>
                </div> 
                  
                <div class="form-group">
                  <label class="col-xs-12" for="newmd5passwords"><?php echo ADMINCF_HASHPASS; ?></label>
                   <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2" id="newmd5passwordsdiv">
                      <select id="newmd5passwords" name="newmd5passwords" class="form-control selectpicker">
                      <option value="0" <?php if ($newmd5passwords=="0") print "selected";?>><?php echo ADMINGENERAL_NO; ?></option>
                      <option value="1" <?php if ($newmd5passwords=="1") print "selected";?>><?php echo ADMINGENERAL_YES; ?></option>
                      </select>  
                  </div>
                  <div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
                    <p class="helptext"><?php echo ADMINCF_HASHPASSNT; ?></p>
                  </div>
                </div> 

                <div class="form-group">
                  <div class="col-xs-12">
                    <div class="btn-toolbar">
                        <button type="submit" id="submitpassword" class="btn btn-primary pull-left" ><?php echo ADMINBUTTON_UPDPWDSET; ?></button>
                        <button type="button" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                    </div>   
                  </div>
                </div>
                <div id="resultpassword"></div>
                </form>
              </div> <!--  passwords  -->

              <div class="tab-pane fade" id="pagestemplates">
                <h4 class="settingstitle"><?php echo ADMINCF_PAGESET; ?></h4>
 
                <form name="pagestemplatesform" id="pagestemplatesform" role="form" class="form-horizontal" action="" target="" method="POST">

                <div class="form-group">
                  <label class="col-xs-12" for="newlogoutpage"><?php echo ADMINCF_LOGOUTURL; ?></label>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9" id="newlogoutpagediv">
                    <input type="text" class="form-control" name="newlogoutpage" id="newlogoutpage" maxlength="255" value="<?php echo $newlogoutpage; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div>                

                <div class="form-group">
                  <label class="col-xs-12" for="newlogintemplate"><?php echo ADMINCF_LGNTEMP; ?></label>
                  <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4" id="newlogintemplatediv">
                    <input type="text" class="form-control" name="newlogintemplate" id="newlogintemplate" maxlength="255" value="<?php echo $newlogintemplate; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                    <p class="helptext"><?php echo ADMINCF_TEMPLATENT; ?></p>
                  </div>                  
                </div> 
                
                <div class="form-group">
                  <label class="col-xs-12" for="newmessagetemplate"><?php echo ADMINCF_MSGTEMP; ?></label>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9" id="newmessagetemplatediv">
                    <input type="text" class="form-control" name="newmessagetemplate" id="newmessagetemplate" maxlength="255" value="<?php echo $newmessagetemplate; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div> 

                <div class="form-group">
                  <label class="col-xs-12" for="newexpiredpage"><?php echo ADMINCF_EXPIREDURL; ?></label>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9" id="newexpiredpagediv">
                    <input type="text" class="form-control" name="newexpiredpage" id="newexpiredpage" maxlength="255" value="<?php echo $newexpiredpage; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div>                

                <div class="form-group">
                  <label class="col-xs-12" for="newwronggrouppage"><?php echo ADMINCF_WRGGRPURL; ?></label>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9" id="newwronggrouppagediv">
                    <input type="text" class="form-control" name="newwronggrouppage" id="newwronggrouppage" maxlength="255" value="<?php echo $newwronggrouppage; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div>                

                <div class="form-group">
                  <label class="col-xs-12" for="newnoaccesspage"><?php echo ADMINCF_NOACCESSURL; ?></label>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9" id="newnoaccesspagediv">
                    <input type="text" class="form-control" name="newnoaccesspage" id="newnoaccesspage" maxlength="255" value="<?php echo $newnoaccesspage; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div>                

                <div class="form-group">
                  <div class="col-xs-12">
                    <div class="btn-toolbar">
                        <button type="submit" id="submitpagestemplates" class="btn btn-primary pull-left" ><?php echo ADMINBUTTON_UPDTEMPSET; ?></button>
                        <button type="button" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                    </div>   
                  </div>
                </div>
                <div id="resultpagestemplates"></div>
                </form>
              </div> <!--  pagestemplates  -->

              <div class="tab-pane fade" id="logsettings">
                <h4 class="settingstitle"><?php echo ADMINCF_LOGSET; ?></h4>
 
                <form name="logsettingsform" id="logsettingsform" role="form" class="form-horizontal" action="" target="" method="POST">

                <div class="form-group">
                  <div class="col-xs-12">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" class="form-control" name="selectall" id="selectall" value="1">&nbsp;&nbsp;<?php echo ADMINBUTTON_SELECTALL; ?>
                      </label>
                    </div>
                  </div>
                </div>    

                <div class="form-group">

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-control" name="newlogentry1" id="newlogentry1" value="on" <?php if ($newlogentry1=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP1; ?>
                        </label>
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-control" name="newlogentry2" id="newlogentry2" value="on" <?php if ($newlogentry2=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP2; ?>
                        </label>
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-control" name="newlogentry3" id="newlogentry3" value="on" <?php if ($newlogentry3=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP3; ?>
                        </label>
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-control" name="newlogentry4" id="newlogentry4" value="on" <?php if ($newlogentry4=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP4; ?>
                        </label>
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-control" name="newlogentry5" id="newlogentry5" value="on" <?php if ($newlogentry5=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP5; ?>
                        </label>
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-control" name="newlogentry6" id="newlogentry6" value="on" <?php if ($newlogentry6=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP6; ?>
                        </label>
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-control" name="newlogentry10" id="newlogentry10" value="on" <?php if ($newlogentry10=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP10; ?>
                        </label>
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-control" name="newlogentry7" id="newlogentry7" value="on" <?php if ($newlogentry7=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP7; ?>
                        </label>
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-control" name="newlogentry8" id="newlogentry8" value="on" <?php if ($newlogentry8=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP8; ?>
                        </label>
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-control" name="newlogentry9" id="newlogentry9" value="on" <?php if ($newlogentry9=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP9; ?>
                        </label>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                  <label class="col-xs-12" for="newsiteloklog"><?php echo ADMINCF_LOGTXT; ?></label>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9" id="newsiteloklogdiv">
                    <input type="text" class="form-control" name="newsiteloklog" id="newsiteloklog" maxlength="255" value="<?php echo $newsiteloklog; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <p class="helptext"><?php echo ADMINGENERAL_BLANK; ?></p>
                  </div>                  
                </div>                

                <div class="form-group">
                  <div class="col-xs-12">
                    <div class="btn-toolbar">
                        <button type="submit" id="submitlogsettings" class="btn btn-primary pull-left" ><?php echo ADMINBUTTON_UPDLOGSET; ?></button>
                        <button type="button" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                    </div>   
                  </div>
                </div>
                <div id="resultlogsettings"></div>
                </form>
              </div> <!--  logsettings  -->

              <div class="tab-pane fade" id="controlpanel">
                <h4 class="settingstitle"><?php echo ADMINCF_CPSET; ?></h4>
                <form name="controlpanelform" id="controlpanelform" role="form" class="form-horizontal" action="" target="" method="POST">

                <div class="form-group">
                  <label class="col-xs-12" for="newlogintemplate"><?php echo ADMINCF_TBLORDER; ?></label>
                </div>
                <div class="form-group">

                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                  <div style="width: 8em; margin-bottom: 10px;"">
                    <span class="light">
                    <?php echo ADMINFIELD_ACTIONS; ?>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <input type="checkbox" class="noicheck" name="newactioncolumncb" id="newactioncolumncb" value="on" disabled checked >
                      </span>
                      <input type="number" class="form-control orderaction" name="newactioncolumn" id="newactioncolumn" value="<?php echo $newactioncolumn; ?>" min="1" max="60" maxlength="2">
                    </div><!-- /input-group -->
                    </span>
                  </div>  
                </div>
                  
                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                  <div style="width: 8em; margin-bottom: 10px;"">
                    <span class="light">
                    <?php echo ADMINFIELD_SELECT; ?>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <input type="checkbox" class="noicheck" name="newselectedcolumncb" id="newselectedcolumncb" value="on" disabled checked >
                      </span>
                      <input type="number" class="form-control orderselect" name="newselectedcolumn" id="newselectedcolumn" value="<?php echo $newselectedcolumn; ?>" min="1" max="60" maxlength="2">
                    </div><!-- /input-group -->
                    </span>
                  </div>  
                </div>

                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                  <div style="width: 8em; margin-bottom: 10px;"">
                    <spam class="light">
                    <?php echo ADMINFIELD_CREATED; ?>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <input type="checkbox" class="noicheck cborder" name="newcreatedcolumncb" id="newcreatedcolumncb" value="on" <?php if ($newcreatedcolumn!="") echo "checked";?> >
                      </span>
                      <input type="number" class="form-control order" name="newcreatedcolumn" id="newcreatedcolumn" value="<?php echo $newcreatedcolumn; ?>" min="1" max="60" maxlength="2">
                    </div><!-- /input-group -->
                    </span>
                  </div>  
                </div>

                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                  <div style="width: 8em; margin-bottom: 10px;"">
                    <span class="light">
                    <?php echo ADMINFIELD_USERNAME; ?>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <input type="checkbox" class="noicheck cborder" name="newusernamecolumncb" id="newusernamecolumncb" value="on" <?php if ($newusernamecolumn!="") echo "checked";?> >
                      </span>
                      <input type="number" class="form-control order" name="newusernamecolumn" id="newusernamecolumn" value="<?php echo $newusernamecolumn; ?>" min="1" max="60" maxlength="2">
                    </div><!-- /input-group -->
                    </span>
                  </div>  
                </div>

                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                  <div style="width: 8em; margin-bottom: 10px;"">
                    <span class="light">
                    <?php echo ADMINFIELD_PASSWORD; ?>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <input type="checkbox" class="noicheck cborder" name="newpasswordcolumncb" id="newpasswordcolumncb" value="on" <?php if ($newpasswordcolumn!="") echo "checked";?> >
                      </span>
                      <input type="number" class="form-control order" name="newpasswordcolumn" id="newpasswordcolumn" value="<?php echo $newpasswordcolumn; ?>" min="1" max="60" maxlength="2">
                    </div><!-- /input-group -->
                    </span>
                  </div>  
                </div>

                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                  <div style="width: 8em; margin-bottom: 10px;"">
                    <span class="light">
                    <?php echo ADMINFIELD_ENABLED; ?>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <input type="checkbox" class="noicheck cborder" name="newenabledcolumncb" id="newenabledcolumncb" value="on" <?php if ($newenabledcolumn!="") echo "checked";?> >
                      </span>
                      <input type="number" class="form-control order" name="newenabledcolumn" id="newenabledcolumn" value="<?php echo $newenabledcolumn; ?>" min="1" max="60" maxlength="2">
                    </div><!-- /input-group -->
                    </span>
                  </div>  
                </div>

                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                  <div style="width: 8em; margin-bottom: 10px;"">
                    <span class="light">
                    <?php echo ADMINFIELD_NAME; ?>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <input type="checkbox" class="noicheck cborder" name="newnamecolumncb" id="newnamecolumncb" value="on" <?php if ($newnamecolumn!="") echo "checked";?> >
                      </span>
                      <input type="number" class="form-control order" name="newnamecolumn" id="newnamecolumn" value="<?php echo $newnamecolumn; ?>" min="1" max="60" maxlength="2">
                    </div><!-- /input-group -->
                    </span>
                  </div>  
                </div>

                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                  <div style="width: 8em; margin-bottom: 10px;"">
                    <span class="light">
                    <?php echo ADMINFIELD_EMAIL; ?>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <input type="checkbox" class="noicheck cborder" name="newemailcolumncb" id="newemailcolumncb" value="on" <?php if ($newemailcolumn!="") echo "checked";?> >
                      </span>
                      <input type="number" class="form-control order" name="newemailcolumn" id="newemailcolumn" value="<?php echo $newemailcolumn; ?>" min="1" max="60" maxlength="2">
                    </div><!-- /input-group -->
                    </span>
                  </div>  
                </div>

                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                  <div style="width: 8em; margin-bottom: 10px;"">
                    <span class="light">
                    <?php echo ADMINFIELD_USERGROUPS; ?>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <input type="checkbox" class="noicheck cborder" name="newusergroupscolumncb" id="newusergroupscolumncb" value="on" <?php if ($newusergroupscolumn!="") echo "checked";?> >
                      </span>
                      <input type="number" class="form-control order" name="newusergroupscolumn" id="newusergroupscolumn" value="<?php echo $newusergroupscolumn; ?>" min="1" max="60" maxlength="2">
                    </div><!-- /input-group -->
                    </span>
                  </div>  
                </div>

                <?php for($k=1;$k<=50;$k++)
                {
                  $varcb="newcustom".$k."columncb";
                  $var="newcustom".$k."column";
                ?>
                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                  <div style="width: 8em; margin-bottom: 10px;">
                    <span class="light">
                    <?php echo ADMINCF_CUSTOM." ".$k; ?>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <input type="checkbox" class="noicheck cborder" name="<?php echo $varcb; ?>" id="<?php echo $varcb; ?>" value="on" <?php if ($$var!="") echo "checked";?> >
                      </span>
                      <input type="number" class="form-control order" name="<?php echo $var; ?>" id="<?php echo $var; ?>" value="<?php echo $$var; ?>" min="1" max="60" maxlength="2">
                    </div><!-- /input-group -->
                    </span>
                  </div>  
                </div>

                <?php }?>

                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                  <div style="width: 8em; margin-bottom: 10px;"">
                    <span class="light">
                    <?php echo ADMINFIELD_USERID; ?>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <input type="checkbox" class="noicheck cborder" name="newuseridcolumncb" id="newuseridcolumncb" value="on" <?php if ($newuseridcolumn!="") echo "checked";?> >
                      </span>
                      <input type="number" class="form-control order" name="newuseridcolumn" id="newuseridcolumn" value="<?php echo $newuseridcolumn; ?>" min="1" max="60" maxlength="2">
                    </div><!-- /input-group -->
                    </span>
                  </div>  
                </div>

                </div> <!--  form-group  -->
                <div class="form-group">
                <span class="restorelink pull-left"><a href="#" onclick="event.preventDefault(); restoreDefaultOrder();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ADMINCF_RSTORDER; ?></a></span>
                </div>

                <div class="form-group">
                  <label class="col-xs-12" for="newshowrows"><?php echo ADMINCF_SHOWROWS; ?></label>
                   <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2" id="newshowrowsdiv">
                      <select id="newshowrows" name="newshowrows" class="form-control selectpicker">
                      <option value="1" <?php if ($newshowrows=="1") print "selected=\"selected\"";?>>1</option>
                      <option value="5" <?php if ($newshowrows=="5") print "selected=\"selected\"";?>>5</option>
                      <option value="10" <?php if ($newshowrows=="10") print "selected=\"selected\"";?>>10</option>
                      <option value="15" <?php if ($newshowrows=="15") print "selected=\"selected\"";?>>15</option>
                      <option value="20" <?php if ($newshowrows=="20") print "selected=\"selected\"";?>>20</option>
                      <option value="25" <?php if ($newshowrows=="25") print "selected=\"selected\"";?>>25</option>
                      <option value="30" <?php if ($newshowrows=="30") print "selected=\"selected\"";?>>30</option>
                      <option value="35" <?php if ($newshowrows=="35") print "selected=\"selected\"";?>>35</option>
                      <option value="40" <?php if ($newshowrows=="40") print "selected=\"selected\"";?>>40</option>
                      <option value="45" <?php if ($newshowrows=="45") print "selected=\"selected\"";?>>45</option>
                      <option value="50" <?php if ($newshowrows=="50") print "selected=\"selected\"";?>>50</option>
                      <option value="100" <?php if ($newshowrows=="100") print "selected=\"selected\"";?>>100</option>
                      <option value="200" <?php if ($newshowrows=="200") print "selected=\"selected\"";?>>200</option>
                      </select>  
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8">
                    <p class="helptext"><?php echo ADMINCF_SHOWROWSNT; ?></p>
                  </div>
                </div> 

                <div class="form-group">
                  <div class="col-xs-12">
                    <div class="btn-toolbar">
                        <button type="submit" id="submitcontrolpanel" class="btn btn-primary pull-left" ><?php echo ADMINBUTTON_UPDCPSET; ?></button>
                        <button type="button" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                    </div>   
                  </div>
                </div>
                <div id="resultcontrolpanel"></div>
                </form>
              </div> <!--  controlpanel  -->

              <div class="tab-pane fade" id="customfields">
                <h4 class="settingstitle"><?php echo ADMINCF_CUSFLDS; ?></h4>
                <form name="customfieldsform" id="customfieldsform" role="form" class="form-horizontal" action="" target="" method="POST">

                <?php for ($k=1; $k<=50; $k++)
                {
                  $var="newcustomtitle".$k;
                  $var2="newcustom".$k."validate";
                ?>  
                <div class="form-group">
                  <label class="col-xs-1 col-sm-1 col-md-2 col-lg-2 cusfield" for="newcustomtitle<?php echo $k; ?>"><nobr><span class="hidden-xs hidden-sm">Custom&nbsp;</span><?php echo $k; ?></nobr></label>
                  <div class="col-xs-5 col-sm-6 col-md-5 col-lg-6" id="newcustomtitle<?php echo $k; ?>div">
                    <input type="text" class="form-control" name="newcustomtitle<?php echo $k; ?>" id="newcustomtitle<?php echo $k; ?>" maxlength="255" value="<?php echo $$var; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>

                   <div class="col-xs-5 col-sm-5 col-md-5 col-lg-4" id="newcustom<?php echo $k; ?>validatediv">
                      <select id="newcustom<?php echo $k; ?>validate" name="newcustom<?php echo $k; ?>validate" class="form-control selectpicker">
                      <option value="0" <?php if ($$var2=="0") print "selected=\"selected\"";?>><?php echo ADMINCF_CUSVAL1; ?></option>
                      <option value="1" <?php if ($$var2=="1") print "selected=\"selected\"";?>><?php echo ADMINCF_CUSVAL2; ?></option>
                      <option value="2" <?php if ($$var2=="2") print "selected=\"selected\"";?>><?php echo ADMINCF_CUSVAL3; ?></option>
                      <option value="3" <?php if ($$var2=="3") print "selected=\"selected\"";?>><?php echo ADMINCF_CUSVAL4; ?></option>
                      </select>  
                  </div>

                </div> 
                <?php } ?>


                <div class="form-group">
                  <div class="col-xs-12">
                    <div class="btn-toolbar">
                        <button type="submit" id="submitcustomfields" class="btn btn-primary pull-left" ><?php echo ADMINBUTTON_UPDCUSSET; ?></button>
                        <button type="button" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                    </div>   
                  </div>
                </div>
                <div id="resultcustomfields"></div>
                </form>
              </div> <!--  customfields  -->

              <div class="tab-pane fade" id="download">
                <h4 class="settingstitle"><?php echo ADMINCF_DWNLOC; ?></h4>
                <form name="downloadform" id="downloadform" role="form" class="form-horizontal" action="" target="" method="POST">

                <div class="form-group">
                  <label class="col-xs-12" for="newfilelocation"><?php echo ADMINCF_DWNLOCDEF; ?></label>
                  <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9" id="newfilelocationdiv">
                    <input type="text" class="form-control" name="newfilelocation" id="newfilelocation" maxlength="255" value="<?php echo $newfilelocation; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div>                

                <div class="form-group">
                  <label class="col-xs-12" for="newfilelocation"><?php echo ADMINCF_DWNLOCS; ?></label>
                </div>                

                <?php
                for ($k=0;$k<(count($FileLocations));$k++)
                {
                  $var1="newfilelocationsname".$k;
                  $var2="newfilelocations".$k;      
                ?>
                <div id="locationblock<?php echo $k; ?>">
                <div class="form-group">
                  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" id="newfilelocationsname<?php echo $k; ?>div">
                    <input type="text" class="form-control" name="newfilelocationsname<?php echo $k; ?>" id="newfilelocationsname<?php echo $k; ?>" maxlength="255" value="<?php echo $$var1; ?>" placeholder="<?php echo ADMINCF_LOCNAME; ?>" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <div class="col-xs-6 col-sm-7 col-md-7 col-lg-8" id="newfilelocations<?php echo $k; ?>div">
                    <input type="text" class="form-control" name="newfilelocations<?php echo $k; ?>" id="newfilelocations<?php echo $k; ?>" maxlength="255" value="<?php echo $$var2; ?>" placeholder="<?php echo ADMINCF_LOCPATH; ?>" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                  <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" id="expiry<?php echo $k; ?>div">
                    <span class="glyphicon glyphicon-remove removegroup actionicon" title="<?php echo ADMINBUTTON_REMOVE; ?>" onclick="removeLocation(<?php echo $k; ?>);"></span>
                  </div>                  
                </div>
                </div>                
                <?php } ?>

                          <div id="extraLocationTemplate">
<!--                            <div id="locationblockXX">
                              <div class="form-group">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" id="newfilelocationsnameXXdiv">
                                   <input type="text" class="form-control" name="newfilelocationsnameXX" id="newfilelocationsnameXX" maxlength="255" value="" placeholder="<?php echo ADMINCF_LOCNAME; ?>" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                </div>
                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-8" id="newfilelocationsXXdiv">
                                  <input type="text" class="form-control" name="newfilelocationsXX" id="newfilelocationsXX" maxlength="255" value="" placeholder="<?php echo ADMINCF_LOCPATH; ?>" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" id="expiryXXdiv">
                                  <span class="glyphicon glyphicon-remove removegroup actionicon" title="<?php echo ADMINBUTTON_REMOVE; ?>" onclick="removeLocation(XX);"></span>
                                </div>
                            </div
                          </div>
 -->                     </div>

                <div id="extraLocationInsert"></div>

                <div class="form-group">
                  <div class="col-xs-12">
                    <button type="button" class="btn btn-xs btn-primary" onclick="addLocation();"><?php echo ADMINCF_ADDLOC; ?></button>
                  </div>
                </div>    

                <div class="form-group">
                  <div class="col-xs-12">
                    <div class="btn-toolbar">
                        <button type="submit" id="submitdownload" class="btn btn-primary pull-left" ><?php echo ADMINBUTTON_UPDDWNSET; ?></button>
                        <button type="button" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                    </div>   
                  </div>
                </div>
                <div id="resultdownload"></div>
                </form>
              </div> <!--  download  -->

              <div class="tab-pane fade" id="email">
                <h4 class="settingstitle"><?php echo ADMINCF_EMAILSET; ?></h4>
                <form name="emailform" id="emailform" role="form" class="form-horizontal" action="" target="" method="POST">

                <div class="form-group">
                  <label class="col-xs-12" for="newemailtype"><?php echo ADMINCF_EMAILTYPE; ?></label>
                   <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3" id="newemailtypediv">
                      <select id="newemailtype" name="newemailtype" class="form-control selectpicker">
                      <option value="0" <?php if ($newemailtype=="0") print "selected=\"selected\"";?>><?php echo ADMINCF_EMAILTYPE1; ?></option>
                      <option value="1" <?php if ($newemailtype=="1") print "selected=\"selected\"";?>><?php echo ADMINCF_EMAILTYPE2; ?></option>
                      </select>  
                  </div>
                </div> 

                <div class="form-group">
                  <label class="col-xs-12" for="newemailreplyto"><?php echo ADMINCF_EMAILREPLYTO; ?></label>
                  <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4" id="newemailreplytodiv">
                    <input type="text" class="form-control" name="newemailreplyto" id="newemailreplyto" maxlength="255" value="<?php echo $newemailreplyto; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div> 

                <div id="smtpsettings">

                  <div class="form-group">
                    <label class="col-xs-12" for="newemailusername"><?php echo ADMINCF_SMTPUSER; ?></label>
                    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4" id="newemailusernamediv">
                      <input type="text" class="form-control" name="newemailusername" id="newemailusername" maxlength="255" value="<?php echo $newemailusername; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                    </div>
                  </div> 

                  <div class="form-group">
                    <label class="col-xs-12" for="newemailpassword"><?php echo ADMINCF_SMTPPWD; ?></label>
                    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4" id="newemailpassworddiv">
                      <input type="text" class="form-control" name="newemailpassword" id="newemailpassword" maxlength="255" value="<?php echo $newemailpassword; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                    </div>
                  </div> 

                  <div class="form-group">
                    <label class="col-xs-12" for="newemailserver"><?php echo ADMINCF_SMTPSRV; ?></label>
                    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4" id="newemailserverdiv">
                      <input type="text" class="form-control" name="newemailserver" id="newemailserver" maxlength="255" value="<?php echo $newemailserver; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                    </div>
                  </div> 

                  <div class="form-group">
                    <label class="col-xs-12" for="newemailport"><?php echo ADMINCF_SMTPPORT; ?></label>
                    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4" id="newemailportdiv">
                      <input type="text" class="form-control" name="newemailport" id="newemailport" maxlength="255" value="<?php echo $newemailport; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                    </div>
                  </div> 

                  <div class="form-group">
                    <label class="col-xs-12" for="newemailauth"><?php echo ADMINCF_SMTPAUTH; ?></label>
                     <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3" id="newemailauthdiv">
                        <select id="newemailauth" name="newemailauth" class="form-control selectpicker">
                        <option value="0" <?php if ($newemailauth=="0") print "selected=\"selected\"";?>>Disabled</option>
                        <option value="1" <?php if ($newemailauth=="1") print "selected=\"selected\"";?>>Enabled</option>
                        </select>  
                    </div>
                  </div> 

                  <div class="form-group">
                    <label class="col-xs-12" for="newemailserversecurity"><?php echo ADMINCF_SMTPSEC; ?></label>
                     <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3" id="newemailserversecuritydiv">
                        <select id="newemailserversecurity" name="newemailserversecurity" class="form-control selectpicker">
                        <option value="" <?php if ($newemailserversecurity=="") print "selected=\"selected\"";?>><?php echo ADMINCF_SMTPSEC1; ?></option>
                        <option value="ssl" <?php if ($newemailserversecurity=="ssl") print "selected=\"selected\"";?>>SSL</option>
                        <option value="tls" <?php if ($newemailserversecurity=="tls") print "selected=\"selected\"";?>>TLS</option>
                        </select>  
                    </div>
                  </div> 

                </div>

                <div class="form-group">
                  <label class="col-xs-12" for="newemaildelay"><?php echo ADMINCF_EMAILDLY; ?></label>
                   <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3" id="newemaildelaydiv">
                      <select id="newemaildelay" name="newemaildelay" class="form-control selectpicker">
                      <option value="0" <?php if ($newemaildelay=="0") print "selected=\"selected\"";?>><?php echo ADMINCF_EMAILNODLY; ?></option>
                      <option value="100" <?php if ($newemaildelay=="100") print "selected=\"selected\"";?>>100<?php echo ADMINGENERAL_MSECONDS; ?></option>
                      <option value="250" <?php if ($newemaildelay=="250") print "selected=\"selected\"";?>>250<?php echo ADMINGENERAL_MSECONDS; ?></option>
                      <option value="500" <?php if ($newemaildelay=="500") print "selected=\"selected\"";?>>500<?php echo ADMINGENERAL_MSECONDS; ?></option>
                      <option value="1000" <?php if ($newemaildelay=="1000") print "selected=\"selected\"";?>>1 <?php echo ADMINGENERAL_SECONDS; ?></option>
                      <option value="2000" <?php if ($newemaildelay=="2000") print "selected=\"selected\"";?>>2 <?php echo ADMINGENERAL_SECONDS; ?></option>
                      <option value="3000" <?php if ($newemaildelay=="3000") print "selected=\"selected\"";?>>3 <?php echo ADMINGENERAL_SECONDS; ?></option>
                      <option value="5000" <?php if ($newemaildelay=="5000") print "selected=\"selected\"";?>>5 <?php echo ADMINGENERAL_SECONDS; ?></option>
                      <option value="8000" <?php if ($newemaildelay=="8000") print "selected=\"selected\"";?>>8 <?php echo ADMINGENERAL_SECONDS; ?></option>
                      <option value="10000" <?php if ($newemaildelay=="10000") print "selected=\"selected\"";?>>10 <?php echo ADMINGENERAL_SECONDS; ?></option>
                      </select>  
                  </div>
                </div> 

                <div class="form-group">
                  <div class="col-xs-12">
                    <button type="button" id="testemail" class="btn btn-xs btn-secondary" onclick="testEmail();"><?php echo ADMINBUTTON_TESTEMAIL; ?></button>
                  </div>
                </div>    


                <div class="form-group">
                  <div class="col-xs-12">
                    <div class="btn-toolbar">
                        <button type="submit" id="submitemail" class="btn btn-primary pull-left" ><?php echo ADMINBUTTON_UPDEMAILSET; ?></button>
                        <button type="button" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                    </div>   
                  </div>
                </div>
                <div id="resultemail"></div>
                </form>
              </div> <!--  email  -->

              <div class="tab-pane fade" id="paths">
                <h4 class="settingstitle"><?php echo ADMINCF_INSTPTHS; ?></h4>
                <form name="pathsform" id="pathsform" role="form" class="form-horizontal" action="" target="" method="POST">

                <div class="form-group">
                  <label class="col-xs-12" for="newsiteloklocation"><?php echo ADMINCF_INSTPTHS1; ?></label>
                  <div class="col-xs-12 col-sm-11 col-md-10 col-lg-9" id="newsiteloklocationdiv">
                    <input type="text" class="form-control" name="newsiteloklocation" id="newsiteloklocation" maxlength="255" value="<?php echo $newsiteloklocation; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div>                

                <div class="form-group">
                  <label class="col-xs-12" for="newsiteloklocationurl"><?php echo ADMINCF_INSTPTHS2; ?></label>
                  <div class="col-xs-12 col-sm-11 col-md-10 col-lg-9" id="newsiteloklocationurldiv">
                    <input type="text" class="form-control" name="newsiteloklocationurl" id="newsiteloklocationurl" maxlength="255" value="<?php echo $newsiteloklocationurl; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div>                

                <div class="form-group">
                  <label class="col-xs-12" for="newemaillocation"><?php echo ADMINCF_INSTPTHS3; ?></label>
                  <div class="col-xs-12 col-sm-11 col-md-10 col-lg-9" id="newemaillocationdiv">
                    <input type="text" class="form-control" name="newemaillocation" id="newemaillocation" maxlength="255" value="<?php echo $newemaillocation; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div>                

                <div class="form-group">
                  <label class="col-xs-12" for="newemailurl"><?php echo ADMINCF_INSTPTHS4; ?></label>
                  <div class="col-xs-12 col-sm-11 col-md-10 col-lg-9" id="newemailurldiv">
                    <input type="text" class="form-control" name="newemailurl" id="newemailurl" maxlength="255" value="<?php echo $newemailurl; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div>                

                <div class="form-group">
                  <label class="col-xs-12" for="newbackuplocation"><?php echo ADMINCF_INSTPTHS5; ?></label>
                  <div class="col-xs-12 col-sm-11 col-md-10 col-lg-9" id="newbackuplocationdiv">
                    <input type="text" class="form-control" name="newbackuplocation" id="newbackuplocation" maxlength="255" value="<?php echo $newbackuplocation; ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                  </div>
                </div>                


                <div class="form-group">
                  <div class="col-xs-12">
                    <div class="btn-toolbar">
                        <button type="submit" id="submitpaths" class="btn btn-primary pull-left" ><?php echo ADMINBUTTON_INSTALLSET; ?></button>
                        <button type="button" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                    </div>   
                  </div>
                </div>
                <div id="resultpaths"></div>
                </form>
              </div> <!--  paths  -->


              <div class="tab-pane fade" id="paths">Default 8</div>

                    </div> <!--  box-body  -->
                </div> <!--  box  -->

            </div><!-- /.col -->
          </div><!-- /.row -->

</div>
</div>

<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("adminthemefooter.php"); ?>

<script type="text/javascript" src="fancybox/source/jquery.fancybox.js?v=2.1.4"></script>
<script type="text/javascript" src="fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript">
  var locationcount=<?php echo (count($FileLocations)+1)."\n"; ?>
</script>
<script src="editconfig.js"></script>

  </body>
</html>
