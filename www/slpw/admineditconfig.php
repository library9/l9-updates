<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  require("admincommonfunctions.php");  

  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  $errors = array();    // array to hold validation errors
  $data = array();      // array to pass back data 
  // Check CSRF value
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError($data,$errors,ADMINMSG_NOTSAVED.". ".ADMINMSG_CSRFFAILED.".");
    exit;
  }  
  if ($DemoMode)
    returnSuccess(ADMINCF_SETTINGSOK); 
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError($data,$errors,ADMINMSG_NOTSAVED.". ".ADMINMSG_MYSQLERROR.".");
    exit;
  }
  if (get_magic_quotes_gpc())
  { 
    foreach ($_POST as $key => $value)
    {
      $_POST[$key]=stripslashes($value);
    }
  }  
  $settingtype=$_POST['settingtype'];

  if ($settingtype=="general")
  {
    // Get settings and validate where necessary
    // Site name
    $newsitename=$_POST['newsitename'];
    if (trim($newsitename)=="")
      $errors['newsitename']=ADMINCF_SITENAMEPR;
    // Admin email
    $newsiteemail=$_POST['newsiteemail'];
    if ($newsiteemail=="")
      $errors['newsiteemail']=ADMINCF_SITEEMAILPR;
    if (($newsiteemail!="") && (!sl_validate_email($newsiteemail)))
      $errors['newsiteemail']=ADMINMSG_EMAILINVALID;
    // Admin secondary email
    $newsiteemail2=$_POST['newsiteemail2'];
    if (($newsiteemail2!="") && (!sl_validate_email($newsiteemail2)))
      $errors['newsiteemail2']=ADMINMSG_EMAILINVALID;
    // Date format
    $newdateformat=$_POST['newdateformat'];
    // Site key
    $newsitekey=$_POST['newsitekey'];
    if (trim($newsitekey)=="")
      $errors['newsitekey']=ADMINCF_SITEKEYPR;
    // Max session time
    $newmaxsessiontime=$_POST['newmaxsessiontime'];
    if (!validInteger($newmaxsessiontime,0,9999999,true))
      $errors['newmaxsessiontime']=ADMINMSG_NOTNUMBER;
    else if (($newmaxsessiontime>0) && ($newmaxsessiontime<60))
      $errors['newmaxsessiontime']=ADMINCF_NOT60;
    // Max inactivity time
    $newmaxinactivitytime=$_POST['newmaxinactivitytime'];
    if (!validInteger($newmaxinactivitytime,0,9999999,true))
      $errors['newmaxinactivitytime']=ADMINMSG_NOTNUMBER;
    else if (($newmaxinactivitytime>0) && ($newmaxinactivitytime<60))
      $errors['newmaxinactivitytime']=ADMINCF_NOT60;
    // Login CAPTCHA
    $newturinglogin=$_POST['newturinglogin'];
    // Register CAPTCHA
    $newturingregister=$_POST['newturingregister'];
    // Remember me
    $newcookielogin=$_POST['newcookielogin'];
    // Concurrent login
    $newconcurrentlogin=$_POST['newconcurrentlogin'];
    // DB update
    $newdbupdate=$_POST['newdbupdate'];
    // Search engine usergroup
    $newallowsearchengine=$_POST['newallowsearchengine'];
    $newsearchenginegroup=$_POST['newsearchenginegroup'];
    if (($newallowsearchengine=="1") && (!validUsergroup($newsearchenginegroup,true)))
       $errors['newsearchenginegroup']=ADMINMSG_GROUPINVALID;
    // Email change verification
    $newemailconfirmrequired=$_POST['newemailconfirmrequired'];
    $newemailconfirmtemplate=$_POST['newemailconfirmtemplate'];
    if (($newemailconfirmrequired=="template") && ($newemailconfirmtemplate==""))
      $errors['newemailconfirmrequired']=ADMINMSG_NOTEMPLATE;
    if (($newemailconfirmrequired=='template') && ($newemailconfirmtemplate!=""))
    {  
      $ext = strtolower(pathinfo($newemailconfirmtemplate, PATHINFO_EXTENSION));
      if (($ext!="html") && ($ext!="htm") && ($ext!="txt"))
        $errors['newemailconfirmrequired']=ADMINMSG_TEMPLATETYPE;      
    }       
    // Unique emails
    $newemailunique=$_POST['newemailunique'];
    // Emails login
    $newloginwithemail=$_POST['newloginwithemail'];
    if (!empty($errors))
    {
      returnError($data,$errors,ADMINMSG_FORMERROR);
      exit;
    }
    // Save settings
    $query = "UPDATE " . $DbConfigTableName . " SET ";
    $query.="sitename=".sl_quote_smart($newsitename).",";
    $query.="siteemail=".sl_quote_smart($newsiteemail).",";
    $query.="siteemail2=".sl_quote_smart($newsiteemail2).",";
    $query.="dateformat=".sl_quote_smart($newdateformat).",";
    $query.="sitekey=".sl_quote_smart($newsitekey).",";
    $query.="maxsessiontime=".sl_quote_smart($newmaxsessiontime).",";
    $query.="maxinactivitytime=".sl_quote_smart($newmaxinactivitytime).",";
    $query.="turinglogin=".sl_quote_smart($newturinglogin).",";
    $query.="turingregister=".sl_quote_smart($newturingregister).",";
    $query.="cookielogin=".sl_quote_smart($newcookielogin).",";
    $query.="concurrentlogin=".sl_quote_smart($newconcurrentlogin).",";
    $query.="dbupdate=".sl_quote_smart($newdbupdate).",";
    $query.="allowsearchengine=".sl_quote_smart($newallowsearchengine).",";
    $query.="searchenginegroup=".sl_quote_smart($newsearchenginegroup).",";
    $query.="emailconfirmrequired=".sl_quote_smart($newemailconfirmrequired).",";
    $query.="emailconfirmtemplate=".sl_quote_smart($newemailconfirmtemplate).",";
    $query.="emailunique=".sl_quote_smart($newemailunique).",";
    $query.="loginwithemail=".sl_quote_smart($newloginwithemail);
    $query.=" WHERE confignum=1";
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_NOTSAVED);
      exit;
    }
    $_SESSION['ses_ConfigReload']="reload";
    returnSuccess(ADMINCF_SETTINGSOK);
    exit;
  }

  if ($settingtype=="password")
  {
     // Forgotten email template
    $forgotemailtype=$_POST['forgotemailtype'];
    $newforgottenemail=$_POST['newforgottenemail'];
    if (($forgotemailtype=="template") && ($newforgottenemail==""))
      $errors['forgotemailtype']=ADMINMSG_NOTEMPLATE;
    if (($forgotemailtype=='template') && ($newforgottenemail!=""))
    {  
      $ext = strtolower(pathinfo($newforgottenemail, PATHINFO_EXTENSION));
      if (($ext!="html") && ($ext!="htm") && ($ext!="txt"))
        $errors['forgotemailtype']=ADMINMSG_TEMPLATETYPE;      
    }
    if ($forgotemailtype=="default")
      $newforgottenemail="";
    // Random password mask
    $newrandompasswordmask=trim($_POST['newrandompasswordmask']);
    if (($newrandompasswordmask!="") && (strspn($newrandompasswordmask, "cCX#AU") != strlen($newrandompasswordmask)))
       $errors['newrandompasswordmask']=ADMINCF_RNDPSSMSKPR;
    // Profile update password required
    $newprofilepassrequired=$_POST['newprofilepassrequired'];
    // Hash passwords
    $newmd5passwords=$_POST['newmd5passwords'];
    if (!empty($errors))
    {
      returnError($data,$errors,ADMINMSG_FORMERROR);
      exit;
    }
    // Save settings
    $query = "UPDATE " . $DbConfigTableName . " SET ";
    $query.="forgottenemail=".sl_quote_smart($newforgottenemail).",";
    $query.="randompasswordmask=".sl_quote_smart($newrandompasswordmask).",";
    $query.="profilepassrequired=".sl_quote_smart($newprofilepassrequired).",";
    $query.="md5passwords=".$newmd5passwords;
    $query.=" WHERE confignum=1";
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_NOTSAVED);
      exit;
    }
    $_SESSION['ses_ConfigReload']="reload";
    returnSuccess(ADMINCF_SETTINGSOK);
    exit;    
  }

  if ($settingtype=="pagestemplates")
  {
    // Logout page URL
    $newlogoutpage=trim($_POST['newlogoutpage']);
    if ($newlogoutpage=="")
      $errors['newlogoutpage']=ADMINCF_LOGOUTURLPR;
    // Check newmessagetemplate exists if set to a filename (if not then we assume URL)
    $newmessagetemplate=trim($_POST['newmessagetemplate']);
    if ($newmessagetemplate!="")
    {
      if ((strtolower(substr($newmessagetemplate,0,1))!="/") && (strtolower(substr($newmessagetemplate,0,7))!="http://") && (strtolower(substr($newmessagetemplate,0,8))!="https://"))
      {
        $newmessagetemplate=str_replace("\\","/",$newmessagetemplate);
        if (!is_file($newmessagetemplate))
          $errors['newmessagetemplate']=ADMINCF_MSGTEMPPR;
      }
    }   
    // Check newlogintemplate exists if set
    $newlogintemplate=trim($_POST['newlogintemplate']);
    if ($newlogintemplate!="")
    { 
      $newlogintemplate=str_replace("\\","/",$newlogintemplate);
      if (!is_file($newlogintemplate))
        $errors['newlogintemplate']=ADMINCF_LGNTEMPPR;
    }   
    // Expired page
    $newexpiredpage=trim($_POST['newexpiredpage']);
    // Wrong group page
    $newwronggrouppage=trim($_POST['newwronggrouppage']);
    // No access page
    $newnoaccesspage=trim($_POST['newnoaccesspage']);
    if (!empty($errors))
    {
      returnError($data,$errors,ADMINMSG_FORMERROR);
      exit;
    }
    // Save settings
    $query = "UPDATE " . $DbConfigTableName . " SET ";
    $query.="logoutpage=".sl_quote_smart($newlogoutpage).",";
    $query.="messagetemplate=".sl_quote_smart($newmessagetemplate).",";
    $query.="logintemplate=".sl_quote_smart($newlogintemplate).",";
    $query.="expiredpage=".sl_quote_smart($newexpiredpage).",";
    $query.="wronggrouppage=".sl_quote_smart($newwronggrouppage).",";
    $query.="noaccesspage=".sl_quote_smart($newnoaccesspage);
    $query.=" WHERE confignum=1";
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_NOTSAVED);
      exit;
    }
    $_SESSION['ses_ConfigReload']="reload";
    returnSuccess(ADMINCF_SETTINGSOK);
    exit;    
  }

  if ($settingtype=="logsettings")
  {
    // Log entries
    $newlogentry1=$_POST['newlogentry1'];
    $newlogentry2=$_POST['newlogentry2'];
    $newlogentry3=$_POST['newlogentry3'];
    $newlogentry4=$_POST['newlogentry4'];
    $newlogentry5=$_POST['newlogentry5'];
    $newlogentry6=$_POST['newlogentry6'];
    $newlogentry7=$_POST['newlogentry7'];
    $newlogentry8=$_POST['newlogentry8'];
    $newlogentry9=$_POST['newlogentry9'];
    $newlogentry10=$_POST['newlogentry10'];
    // Check newsiteloklog exists if set
    $newsiteloklog=$_POST['newsiteloklog'];
    if ($newsiteloklog!="")
    { 
      $newsiteloklog=str_replace("\\","/",$newsiteloklog);
      if ((is_writeable($newsiteloklog) == false) || (is_readable($newsiteloklog) == false))
        $errors['newsiteloklog']=ADMINCF_LOGTXTPR;
    }   
    if (!empty($errors))
    {
      returnError($data,$errors,ADMINMSG_FORMERROR);
      exit;
    }
    // Save settings
    $query = "UPDATE " . $DbConfigTableName . " SET ";
    $query.="siteloklog=".sl_quote_smart($newsiteloklog).",";
    $newlogdetails="";
    if ($newlogentry1=="on")
      $newlogdetails.="Y";
    else
      $newlogdetails.="N";
    if ($newlogentry2=="on")
      $newlogdetails.="Y";
    else
      $newlogdetails.="N";
    if ($newlogentry3=="on")
      $newlogdetails.="Y";
    else
      $newlogdetails.="N";
    if ($newlogentry4=="on")
      $newlogdetails.="Y";
    else
      $newlogdetails.="N";
    if ($newlogentry5=="on")
      $newlogdetails.="Y";
    else
      $newlogdetails.="N";
    if ($newlogentry6=="on")
      $newlogdetails.="Y";
    else
      $newlogdetails.="N";
    if ($newlogentry7=="on")
      $newlogdetails.="Y";
    else
      $newlogdetails.="N";
    if ($newlogentry8=="on")
      $newlogdetails.="Y";
    else
      $newlogdetails.="N";
    if ($newlogentry9=="on")
      $newlogdetails.="Y";
    else
      $newlogdetails.="N";
    if ($newlogentry10=="on")
      $newlogdetails.="Y";
    else
      $newlogdetails.="N";
    $newlogdetails.="YYYYYY";          
    $query.="logdetails=".sl_quote_smart($newlogdetails);
    $query.=" WHERE confignum=1";
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_NOTSAVED);
      exit;
    }
    $_SESSION['ses_ConfigReload']="reload";
    returnSuccess(ADMINCF_SETTINGSOK);
    exit;    
  }

  if ($settingtype=="controlpanel")
  {
    // Get input
    $newactioncolumn  = trim($_POST["newactioncolumn"]);
    $newselectedcolumn = trim($_POST["newselectedcolumn"]);
    $newcreatedcolumn = ($_POST["newcreatedcolumncb"]=="on")?trim($_POST["newcreatedcolumn"]):"";
    $newusernamecolumn = ($_POST["newusernamecolumncb"]=="on")?trim($_POST["newusernamecolumn"]):"";
    $newpasswordcolumn = ($_POST["newpasswordcolumncb"]=="on")?trim($_POST["newpasswordcolumn"]):"";
    $newenabledcolumn = ($_POST["newenabledcolumncb"]=="on")?trim($_POST["newenabledcolumn"]):"";
    $newnamecolumn = ($_POST["newnamecolumncb"]=="on")?trim($_POST["newnamecolumn"]):"";
    $newemailcolumn = ($_POST["newemailcolumncb"]=="on")?trim($_POST["newemailcolumn"]):"";
    $newusergroupscolumn = ($_POST["newusergroupscolumncb"]=="on")?trim($_POST["newusergroupscolumn"]):"";
    $newuseridcolumn = ($_POST["newuseridcolumncb"]=="on")?trim($_POST["newuseridcolumn"]):"";
    for($k=1;$k<=50;$k++)
    {
      $var="newcustom".$k."column";
      $$var = ($_POST[$var."cb"]=="on")?trim($_POST[$var]):"";
    } 
    // Convert column order variables
    $newcolumnorder="";
    for ($c=1;$c<=60;$c++)
    {
      if ($newactioncolumn==$c) $newcolumnorder.="AC";
      if ($newselectedcolumn==$c) $newcolumnorder.="SL";
      if ($newcreatedcolumn==$c) $newcolumnorder.="CR";
      if ($newusernamecolumn==$c) $newcolumnorder.="US";
      if ($newpasswordcolumn==$c) $newcolumnorder.="PW";
      if ($newenabledcolumn==$c) $newcolumnorder.="EN";
      if ($newnamecolumn==$c) $newcolumnorder.="NM";
      if ($newemailcolumn==$c) $newcolumnorder.="EM";
      if ($newusergroupscolumn==$c) $newcolumnorder.="UG";
      if ($newuseridcolumn==$c) $newcolumnorder.="ID";
      for($k=1;$k<=50;$k++)
      {
        $var="newcustom".$k."column";
        if ($k<10) $val2="0".$k; else $val2=$k;
        if ($$var==$c) $newcolumnorder.=$val2;
      }
    } 
    $newshowrows=$_POST['newshowrows'];
    // Save settings
    $query = "UPDATE " . $DbConfigTableName . " SET ";
    $query.="columnorder=".sl_quote_smart($newcolumnorder).",";      
    $query.="showrows=".sl_quote_smart($newshowrows);
    $query.=" WHERE confignum=1";
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_NOTSAVED);
      exit;
    }
    $_SESSION['ses_ConfigReload']="reload";
    returnSuccess(ADMINCF_SETTINGSOK);
    exit;    
  }

  if ($settingtype=="customfields")
  {
    if (!empty($errors))
    {
      returnError($data,$errors,ADMINMSG_FORMERROR);
      exit;
    }
    // Save settings
    $query = "UPDATE " . $DbConfigTableName . " SET ";
    for ($k=1;$k<=50;$k++)
    {
      $query.="customtitle".$k."=".sl_quote_smart($_POST['newcustomtitle'.$k]).",";      
      $query.="custom".$k."validate=".sl_quote_smart($_POST['newcustom'.$k.'validate']);      
      if ($k<50)
        $query.=",";
    }
    $query.=" WHERE confignum=1";
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_NOTSAVED);
      exit;
    }
    $_SESSION['ses_ConfigReload']="reload";
    returnSuccess(ADMINCF_SETTINGSOK);
    exit;    
  }

  if ($settingtype=="download")
  {
    // Check newfilelocation is not blank and ends in /
    $newfilelocation=trim($_POST['newfilelocation']);
    $newfilelocation=str_replace("\\","/",$newfilelocation);
    if ($newfilelocation!="")
    {
      // If not S3 location then make sure last character is /
      if (strtolower(substr($newfilelocation,0,3))!="s3|")
      {
        if (substr($newfilelocation,strlen($newfilelocation)-1,1)!="/")
          $newfilelocation.="/";
      }  
    }
    if ($newfilelocation=="")
      $errors['newfilelocation']=ADMINCF_DWNLOCDEFPR1;
    else
    {
      // If not remote location or S3 then check folder exists
      if ((strtolower(substr($newfilelocation,0,3))!="s3|") && (strtolower(substr($newfilelocation,0,7))!="http://"))
      {      
        if (!is_dir($newfilelocation))
          $errors['newfilelocation']=ADMINCF_PATHNOTFND;
      }
    }
    // Check additional file locations
    $locationcount=$_POST['locationcount'];
    for($k=0;$k<=$locationcount;$k++)
    {
      $match=false;
      $missing=false;
      $notfound=false;
      $var1="newfilelocationsname".$k;
      $$var1=trim($_POST[$var1]);
      if ($$var1=="")
        continue;
      $var2="newfilelocations".$k;
      $$var2=trim($_POST[$var2]);
      $$var2=str_replace("\\","/",$$var2);
      if ($$var2!="")
      {
        // If not S3 location then make sure last character is /
        if (strtolower(substr($$var2,0,3))!="s3|")
        {     
          if (substr($$var2,strlen($$var2)-1,1)!="/")
            $$var2.="/";
        }  
      }           
      // Check if name is unique
      for ($j=0;$j<=$locationcount;$j++)
      {
        if ($j==$k)
          continue;
        $var3="newfilelocationsname".$j;
        if (($$var1=="") || ($$var3==""))
          continue;
        if ($$var1==$$var3)
          $match=true;  
      }
      // Check that each name has corresponding file location
      if (($$var1!="") && ($$var2==""))
        $missing=true;
      // Check that each file location has corresponding name
      if (($$var2!="") && ($$var1==""))
        $missing=true;
      // If not remote location or S3 then check folder exists
      if ((strtolower(substr($$var2,0,3))!="s3|") && (strtolower(substr($$var2,0,7))!="http://"))
      if ($$var2!="")
      {
        if (!is_dir($$var2))
        {
          $notfound=true;
        }
      }
      if ($notfound)
        $errors['newfilelocations'.$k]=ADMINCF_PATHNOTFND;
      if ($$var1=='default')
        $match=true;
      if ($match)
        $errors['newfilelocationsname'.$k]=ADMINCF_DWNLOCSPR2;
      if ($missing)
        $errors['newfilelocationsname'.$k]=ADMINCF_DWNLOCSPR3;
    }
    if (!empty($errors))
    {
      returnError($data,$errors,ADMINMSG_FORMERROR);
      exit;
    }
    // Save settings
    $newfilelocation=str_replace("|",";",$newfilelocation);
    $temp="default=".$newfilelocation;
    for($k=0;$k<=$locationcount;$k++)
    {
      $var1="newfilelocationsname".$k;
      $var2="newfilelocations".$k;
      // Convert | character splitting S3 location to ; as |is used internally already
      $$var2=str_replace("|",";",$$var2);
      if (($$var1!="") && ($$var2!=""))
      {
        $temp.="|".$$var1."=".$$var2;  
      }
    }
    $query = "UPDATE " . $DbConfigTableName . " SET ";
    $query.="filelocation=".sl_quote_smart($temp);
    $query.=" WHERE confignum=1";
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_NOTSAVED);
      exit;
    }
    $_SESSION['ses_ConfigReload']="reload";
    returnSuccess(ADMINCF_SETTINGSOK);
    exit;    
  }

  if ($settingtype=="email")
  {
    $newemailtype=$_POST['newemailtype'];
    $newemailreplyto=$_POST['newemailreplyto'];
    $newemailusername=trim($_POST['newemailusername']);
    $newemailpassword=trim($_POST['newemailpassword']);
    $newemailserver=trim($_POST['newemailserver']);
    $newemailport=trim($_POST['newemailport']);
    $newemailauth=trim($_POST['newemailauth']);
    $newemailserversecurity=trim($_POST['newemailserversecurity']);
    $newemaildelay=trim($_POST['newemaildelay']);
    // check SMTP settings entered for PHPmailer
    if ($newemailtype==1)
    {
//      if ($newemailusername=="")
//        $errors['newemailusername']=ADMINMSG_FIELDREQD;
//      if ($newemailpassword=="")
//        $errors['newemailpassword']=ADMINMSG_FIELDREQD;
      if ($newemailserver=="")
        $errors['newemailserver']=ADMINMSG_FIELDREQD;
      if ($newemailport=="")
        $errors['newemailport']=ADMINMSG_FIELDREQD;
    }
    if (!empty($errors))
    {
      returnError($data,$errors,ADMINMSG_FORMERROR);
      exit;
    }
    // Save settings
    $query = "UPDATE " . $DbConfigTableName . " SET ";
    $query.="emailtype=".$newemailtype.",";
    $query.="emailreplyto=".sl_quote_smart($newemailreplyto).",";
    $query.="emailusername=".sl_quote_smart($newemailusername).",";
    $query.="emailpassword=".sl_quote_smart($newemailpassword).",";
    $query.="emailserver=".sl_quote_smart($newemailserver).",";
    $query.="emailport=".sl_quote_smart($newemailport).",";
    $query.="emailauth=".$newemailauth.",";
    $query.="emailserversecurity=".sl_quote_smart($newemailserversecurity).",";
    $query.="emaildelay=".$newemaildelay;      
    $query.=" WHERE confignum=1";
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_NOTSAVED);
      exit;
    }
    $_SESSION['ses_ConfigReload']="reload";
    returnSuccess(ADMINCF_SETTINGSOK);
    exit;    
  }

  if ($settingtype=="paths")
  {
    $newsiteloklocation=trim($_POST['newsiteloklocation']);
    $newsiteloklocationurl=trim($_POST['newsiteloklocationurl']);
    $newemaillocation=trim($_POST['newemaillocation']);
    $newemailurl=trim($_POST['newemailurl']);
    $newbackuplocation=trim($_POST['newbackuplocation']);
    // Check newsiteloklocation is not blank and ends in /
    $newsiteloklocation=str_replace("\\","/",$newsiteloklocation);
    if ($newsiteloklocation!="")
    {
      if (substr($newsiteloklocation,strlen($newsiteloklocation)-1,1)!="/")
        $newsiteloklocation.="/";
    }        
    if ($newsiteloklocation=="")
      $errors['newsiteloklocation']=ADMINCF_INSTPTHS1PR1;
    else
    {
      if (!is_file($newsiteloklocation."sitelokpw.php"))
        $errors['newsiteloklocation']=ADMINCF_PATHNOTFND;
    }
    // Check newsiteloklocationurl is not blank, starts with http and ends in /
    $newsiteloklocationurl=str_replace("\\","/",$newsiteloklocationurl);
    if ($newsiteloklocationurl!="")
    {
      if (substr($newsiteloklocationurl,strlen($newsiteloklocationurl)-1,1)!="/")
        $newsiteloklocationurl.="/";
    }  
    if ((strlen($newsiteloklocationurl)<8) || (substr($newsiteloklocationurl,0,4)!="http"))
      $errors['newsiteloklocationurl']=ADMINCF_INSTPTHS2PR1;
    // Check newemaillocation is not blank and ends in /
    $newemaillocation=str_replace("\\","/",$newemaillocation);
    if ($newemaillocation!="")
    {
      if (substr($newemaillocation,strlen($newemaillocation)-1,1)!="/")
        $newemaillocation.="/";
    }    
    if ($newemaillocation=="")
      $errors['newemaillocation']=ADMINCF_INSTPTHS3PR1;
    else
    {
      if (!is_dir($newemaillocation))
        $errors['newemaillocation']=ADMINCF_PATHNOTFND;
    }    
    // Check newemailurl is not blank, starts with http and ends in /
    $newemailurl=str_replace("\\","/",$newemailurl);
    if ($newemailurl!="")
    {
      if (substr($newemailurl,strlen($newemailurl)-1,1)!="/")
        $newemailurl.="/";
    }    
    if ((strlen($newemailurl)<8) || (substr($newemailurl,0,4)!="http"))
      $errors['newemailurl']=ADMINCF_INSTPTHS4PR1;
    // Check newbackuplocation is not blank and ends in /
    $newbackuplocation=str_replace("\\","/",$newbackuplocation);
    if ($newbackuplocation!="")
    {
      if (substr($newbackuplocation,strlen($newbackuplocation)-1,1)!="/")
        $newbackuplocation.="/";
    }    
    if ($newbackuplocation=="")
      $errors['newbackuplocation']=ADMINCF_INSTPTHS5PR1;
    else
    {
      if (!is_dir($newbackuplocation))
        $errors['newbackuplocation']=ADMINCF_PATHNOTFND;
    }        
    if (!empty($errors))
    {
      returnError($data,$errors,ADMINMSG_FORMERROR);
      exit;
    }
    // Save settings
    $query = "UPDATE " . $DbConfigTableName . " SET ";
    $query.="siteloklocation=".sl_quote_smart($newsiteloklocation).",";
    $query.="siteloklocationurl=".sl_quote_smart($newsiteloklocationurl).",";
    $query.="emaillocation=".sl_quote_smart($newemaillocation).",";
    $query.="emailurl=".sl_quote_smart($newemailurl).",";
    $query.="backuplocation=".sl_quote_smart($newbackuplocation);      
    $query.=" WHERE confignum=1";
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_NOTSAVED);
      exit;
    }
    $_SESSION['ses_ConfigReload']="reload";
    returnSuccess(ADMINCF_SETTINGSOK);
    exit;    
  }



  function returnSuccess($msg)
  {
    $data['success'] = true;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

  function returnError($data,$errors,$msg)
  {
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

?>
