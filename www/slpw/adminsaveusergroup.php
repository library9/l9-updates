<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  require("admincommonfunctions.php");  
  $slsubadmin=false;
  $errors = array();    // array to hold validation errors
  $data = array();      // array to pass back data   
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_CSRFFAILED.".");
    exit;
  }   
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_MYSQLERROR.".");
    exit;
  }
  $act=$_POST['act'];
  $actid=$_POST['actid'];
  $oldgroupname=$_POST['oldgroupname'];
  $newgroupname=trim($_POST['newgroupname']);    
  $newdescription=$_POST['newdescription'];    
  $newloginaction=$_POST['newloginaction']; 
  $newloginvalue=trim($_POST['newloginvalue']);
  // Validate
  if ($newgroupname=="")
    $errors['newgroupname']=ADMINMSG_ENTGROUP;
  if (($newgroupname!="") && (!validUsergroup($newgroupname,true)))
    $errors['newgroupname']=ADMINMSG_GROUPINVALID;
  // Login action and value
  if (($newloginaction=="URL") && ($newloginvalue==""))
    $errors['newloginvalue']=ADMINMSG_ENTURL;
  if (!empty($errors))
  {
    returnError($data,$errors,ADMINMSG_FORMERROR);
    exit;
  }
  if (!$DemoMode)
  {
    if (($act=="addgroup") || ($act=="duplgroup"))
    {
      $mysql_result=mysqli_query($mysql_link,"INSERT INTO ".$DbGroupTableName." (name,description,loginaction,loginvalue) VALUES(".sl_quote_smart($newgroupname).",".sl_quote_smart($newdescription).",".sl_quote_smart($newloginaction).",".sl_quote_smart($newloginvalue).")");
      if (!$mysql_result)
      {
        $errors['newgroupname']="This usergroup already exists";
        returnError($data,$errors,ADMINMSG_FORMERROR);
        exit;
      }
        // Event point
        // Call event handler and plugins
        $newgroupid=mysqli_insert_id($mysql_link);
        $paramdata=array();
        $paramdata['oldname']="";
        $paramdata['name']=$newgroupname;
        $paramdata['groupid']=$newgroupid;
        $paramdata['description']=$newdescription;
        $paramdata['loginaction']=$newloginaction;
        $paramdata['loginvalue']=$newloginvalue;
        if (function_exists("sl_onAddGroup"))
          sl_onAddGroup($paramdata);
        for ($p=0;$p<$slnumplugins;$p++)
        {
          if (function_exists($slplugin_event_onAddGroup[$p]))
            call_user_func($slplugin_event_onAddGroup[$p],$slpluginid[$p],$paramdata);
        }    
    }
    else
    {
      $mysql_result=mysqli_query($mysql_link,"UPDATE ".$DbGroupTableName." SET name=".sl_quote_smart($newgroupname).", description=".sl_quote_smart($newdescription).", loginaction=".sl_quote_smart($newloginaction).", loginvalue=".sl_quote_smart($newloginvalue)." WHERE id=".sl_quote_smart($actid));
      if (!$mysql_result)
      {
        $errors['newgroupname']="This usergroup already exists";
        returnError($data,$errors,ADMINMSG_FORMERROR);
        exit;
      }
      // Event point
      // Call event handler and plugins
      $paramdata=array();
      $paramdata['oldname']=$oldgroupname;
      $paramdata['name']=$newgroupname;
      $paramdata['groupid']=$actid;
      $paramdata['description']=$newdescription;
      $paramdata['loginaction']=$newloginaction;
      $paramdata['loginvalue']=$newloginvalue;
      if (function_exists("sl_onModifyGroup"))
        sl_onModifyGroup($paramdata);
      for ($p=0;$p<$slnumplugins;$p++)
      {
        if (function_exists($slplugin_event_onModifyGroup[$p]))
          call_user_func($slplugin_event_onModifyGroup[$p],$slpluginid[$p],$paramdata);
      }    
    }
  }
  $_SESSION['ses_ConfigReload']="reload";
  if (($act=="addgroup") || ($act=="duplgroup"))
    returnSuccess($data,ADMINMU_GROUPADDED);
  else
    returnSuccess($data,ADMINMU_GROUPUPDATED);

  function returnSuccess($data,$msg)
  {
    $data['success'] = true;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

  function returnError($data,$errors,$msg)
  {
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

?>
