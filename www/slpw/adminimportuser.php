<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  if (!defined('ADMINIU_ISSUE15'))
    define("ADMINIU_ISSUE15","not added (user already exists)");
  if (!defined('ADMINIU_MINFIELDS2'))
    define("ADMINIU_MINFIELDS2","File must contain at least the email field and one other field");
  require("admincommonfunctions.php");  
  $slsubadmin=false;
  $errors = array();    // array to hold validation errors
  $data = array();      // array to pass back data   
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_CSRFFAILED.".".$_POST['slcsrf']." ");
    exit;
  }   
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_MYSQLERROR.".");
    exit;
  }
  // Get form data
  $firstrowheader=$_POST['firstrowheader'];
  $useemailasuser=$_POST['useemailasuser'];
  $addusers=$_POST['addusers'];
  $randpass=$_POST['randpass'];
  $groupcount=$_POST['groupcount']; // This isn't number of groups but maximum group number. e.g. 23 for group23.
  // Get usergroups. These can be group1 group3 etc with gaps.
  for ($k=1;$k<=$groupcount;$k++)
  {
    if ((isset($_POST['group'.$k])) || (isset($_POST['expiry'.$k])))
    {
      $var="group".$k;
      $$var=$_POST['group'.$k];
      $var="expiry".$k;
      $$var=$_POST['expiry'.$k];
    }
  }
  $selectadded=$_POST['selectadded'];
  $existingusers=$_POST['existingusers'];
  $blankdata=$_POST['blankdata'];
  $selectupdated=$_POST['selectupdated'];
  // Check file uploaded
  if ((!isset($_FILES['slimportedusers']['name'])) || ($_FILES['slimportedusers']['name']==""))
    $errors['slimportedusers']=ADMINIU_NOFILE;
  // Validate usergroups and expiries
  for ($k=1;$k<=$groupcount;$k++)
  {
    $gvar="group".$k;
    $evar="expiry".$k;
    if ((!isset($$gvar)) && (!isset($$evar)))
      continue;
    $grouperror="";
    $expiryerror="";
    if (($$gvar!="") && (!validUsergroup($$gvar,true)))
      $grouperror=ADMINMSG_GROUPINVALID;
    if (($grouperror=="") && ($slsubadmin) && (trim(strtoupper($$gvar))=="ADMIN"))
      $grouperror=ADMINMSG_ADMINNOTALLOWED;
    if (($$evar!="") && (strspn($$evar, "0123456789") != strlen($$evar)))
      $expiryerror=ADMINMSG_EXPIRYINVALID;
    if (strlen($$evar)==6)
    {
      if (!dateValid($$evar,$DateFormat))
        $expiryerror=ADMINMSG_EXPIRYDATEINVALID;
    }
    if (($$gvar=="") && ($$evar!=""))
      $grouperror=ADMINMSG_GROUPINVALID;      
    if ($grouperror!="")
      $errors[$gvar]=$grouperror;
    if ($expiryerror!="")
      $errors[$evar]=$expiryerror;
  }
  if (!empty($errors))
  {
    returnError($data,$errors,ADMINMSG_FORMERROR);
    exit;
  }
  // Save settings
  // First concatenate groups and expiry times in table format
  $ug="";
  for ($k=1;$k<=$groupcount;$k++)
  {
    $pvar1="group".$k;
    $pvar2="expiry".$k;
    if (!isset($$pvar1))
      continue;
    if ($$pvar1!="")
    {
      if ($ug!="")
        $ug.="^";
      $ug.=$$pvar1;  
      if ($$pvar2!="")
      {
        $expirystr=$$pvar2;
        $ug.=":";
        $ug.=$expirystr;
      }         
    }
  }
  $query="UPDATE sl_adminconfig SET ";
  if ($firstrowheader=="on")
    $query.="importheader='1', ";
  else
    $query.="importheader='0', ";
  $query.="importuseemailas=".sl_quote_smart($useemailasuser).", ";
  if ($addusers=="on")
    $query.="importaddusers='1', ";
  else
    $query.="importaddusers='0', ";
  $query.="importrandpass=".sl_quote_smart($randpass).", ";
  $query.="importusergroups=".sl_quote_smart($ug).", ";
  if ($selectadded=="on")
    $query.="importslctadded='1', ";
  else
    $query.="importslctadded='0', ";
  if ($existingusers=="on")
    $query.="importexistusers='1', ";
  else
    $query.="importexistusers='0', ";
  $query.="importblank=".sl_quote_smart($blankdata).", ";
  if ($selectupdated=="on")
    $query.="importslctexist='1'";
  else
    $query.="importslctexist='0'";
  $query.=" WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
  $_SESSION['ses_ConfigReload']="reload";     
  $log=array();
  @ini_set('auto_detect_line_endings', true);
  $usernotunique=false;
  if (isset($_FILES['slimportedusers']))
  {  
    $usersadded=0;
    $usersignored=0;
    $totalrecords=0;
    $usersupdated=0;
    $line=0;
    $fh=@fopen($_FILES['slimportedusers']['tmp_name'],"r");
    if (!($fh))
    {
      @unlink($_FILES['slimportedusers']['tmp_name']);
      mysqli_close($mysql_link);
      fclose($fh);
      returnError($data,$errors,ADMINIU_CANTOPEN);
      exit;
    }
    // Concatenate groups and expiry times in table format
    $defaultusergroups="";
    for ($k=1;$k<=5;$k++)
    {
      $pvar1="group".$k;
      $pvar2="expiry".$k;
      if ($$pvar1!="")
      {
        if ($defaultusergroups!="")
          $defaultusergroups.="^";
        $defaultusergroups.=$$pvar1;  
        if ($$pvar2!="")
        {
          // If expiry is number of days then convert to date
          if (strlen($$pvar2)<6)
          {
            if ($DateFormat=="DDMMYY")
              $expirystr=gmdate("dmy",time()+$$pvar2*86400);
            if ($DateFormat=="MMDDYY")
              $expirystr=gmdate("mdy",time()+$$pvar2*86400);            
          }
          else
            $expirystr=$$pvar2;
          $defaultusergroups.=":";
          $defaultusergroups.=$expirystr;
        }         
      }
    }
    $datetoday=gmdate("ymd");
    // If selecting users added or updated then clear already selected users
    if (($selectadded=="on") || ($selectupdated=="on"))
      $mysql_result=mysqli_query($mysql_link,"UPDATE ".$DbTableName." SET ".$SelectedField."='No' ");
    if (!isset($ImportSeparator))
      $ImportSeparator=",";
    // Reader first line of file to allocate fields or see if created field is used
    $fields = fgetcsv($fh, 15000, $ImportSeparator);
    if (($fields===null) || ($fields===false))
    {
      @unlink($_FILES['slimportedusers']['tmp_name']);
      mysqli_close($mysql_link);
      fclose($fh);
      returnError($data,$errors,ADMINIU_COLHEAD);
      exit;
    }
    for ($k=0;$k<count($fields);$k++)
      $fields[$k]=trim($fields[$k]);  
    // Determine columns
    $columns=array();  
    if ($firstrowheader=="on")
    {
      for ($k=0;$k<count($fields);$k++)
      {
        $fields[$k]=strtolower($fields[$k]);
        if ($fields[$k]=="created")
          $columns['created']=$k;
        if ($fields[$k]=="username")
          $columns['username']=$k;
        if ($fields[$k]=="password")
          $columns['password']=$k;
        if ($fields[$k]=="enabled")
          $columns['enabled']=$k;
        if ($fields[$k]=="name")
          $columns['name']=$k;
        if ($fields[$k]=="email")
          $columns['email']=$k;
        if ($fields[$k]=="usergroups")
          $columns['usergroups']=$k;
        if ($fields[$k]=="selected")
          $columns['selected']=$k;
        for($c=1;$c<=50;$c++)
        {
          if ($fields[$k]=="custom".$c)
            $columns['custom'.$c]=$k;
        }    
      }
    }
    else
    {
      $columnoffset=0;
      $crarray=explode("/",$fields[0]);
      if ((count($crarray)==3) && (is_numeric($crarray[0])) && (is_numeric($crarray[1]))  && (is_numeric($crarray[2])))
      {       
        $columns['created']=0;
        $columnoffset=1;
      }  
      $columns['username']=$columnoffset;
      $columns['password']=$columnoffset+1;
      $columns['enabled']=$columnoffset+2;
      $columns['name']=$columnoffset+3;
      $columns['email']=$columnoffset+4;
      $columns['usergroups']=$columnoffset+5;
      for($c=1;$c<=50;$c++)
      {
        $columns['custom'.$c]=$columnoffset+$c+5;;
      }
      // Close and reopen file so that we start with first row again
      fclose($fh);    
      $fh=@fopen($_FILES['slimportedusers']['tmp_name'],"r");
      if (!($fh))
      {
        @unlink($_FILES['slimportedusers']['tmp_name']);
        mysqli_close($mysql_link);
        fclose($fh);
        returnError($data,$errors,ADMINIU_CANTOPEN);
        exit;
      }        
    }  
    // Check we have at least username or email and one other field
    if ($useemailasuser=="emailnever")
    {
      if ((!isset($columns['username'])) || (count($columns)<2))
      {
        @unlink($_FILES['slimportedusers']['tmp_name']);
        mysqli_close($mysql_link);
        fclose($fh);
        returnError($data,$errors,ADMINIU_MINFIELDS);
        exit;
      }
    }
    else
    {
      if ((!isset($columns['username'])) && (!isset($columns['email'])) || (count($columns)<2))
      {
        @unlink($_FILES['slimportedusers']['tmp_name']);
        mysqli_close($mysql_link);
        fclose($fh);
        returnError($data,$errors,ADMINIU_MINFIELDS2);
        exit;
      }
    }     
    while (($fields = fgetcsv($fh, 15000, $ImportSeparator)) !== false)
    {
      $line++;
      // Get username for this row
      $user=trim($fields[$columns['username']]);
      // If no username and allowed or required use email address.
      if (isset($columns['email']))
      {
        if ((($user=="") && ($useemailasuser=="emailifnouser")) || ($useemailasuser=="emailalways"))
          $user=trim($fields[$columns['email']]);
      }
      if ($user=="")
      {
        $log[]=ADMINIU_LINE." ".$line." - ".ADMINIU_ISSUE1;
        $usersignored++;
        continue;
      }
      $totalrecords++;    
      // See if user exists already
      $thisuserexists=false;
      $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." WHERE ".$UsernameField." = ".sl_quote_smart($user));
      if(mysqli_num_rows($mysql_result))
        $thisuserexists=true;
      // Add user processing
      if (!$thisuserexists)
      {
        if ((!$thisuserexists) && ($addusers!="on"))
        {
          $usersignored++;
          continue;
        }
        $cr=$datetoday;
        $pass="";
        $en="Yes";
        $nm="";
        $em="";
        $ug=$defaultusergroups;
        $cu1="";
        $cu2="";
        $cu3="";
        $cu4="";
        $cu5="";
        $cu6="";
        $cu7="";
        $cu8="";
        $cu9="";
        $cu10="";
        $cu11="";
        $cu12="";
        $cu13="";
        $cu14="";
        $cu15="";
        $cu16="";
        $cu17="";
        $cu18="";
        $cu19="";
        $cu20="";
        $cu21="";
        $cu22="";
        $cu23="";
        $cu24="";
        $cu25="";
        $cu26="";
        $cu27="";
        $cu28="";
        $cu29="";
        $cu30="";
        $cu31="";
        $cu32="";
        $cu33="";
        $cu34="";
        $cu35="";
        $cu36="";
        $cu37="";
        $cu38="";
        $cu39="";
        $cu40="";
        $cu41="";
        $cu42="";
        $cu43="";
        $cu44="";
        $cu45="";
        $cu46="";
        $cu47="";
        $cu48="";
        $cu49="";
        $cu50="";      
        // Get row data and validate
        if ((isset($columns['created'])) && (isset($fields[$columns['created']])))
        {
          $cr=trim($fields[$columns['created']]);
          $crarray=explode("/",$cr);
          if ((count($crarray)!=3) || (!is_numeric($crarray[0])) || (!is_numeric($crarray[1])) || (!is_numeric($crarray[2])))
          {
            $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE2;
            $usersignored++;
            continue;
          }    
          if ($DateFormat=="DDMMYY")
          {
            if ($crarray[2]>=2000)
              $crarray[2]=$crarray[2]-2000;
            $cr=sprintf("%02d",$crarray[2]).sprintf("%02d",$crarray[1]).sprintf("%02d",$crarray[0]);
          }  
          else
          {
            if ($crarray[2]>=2000)
              $crarray[2]=$crarray[2]-2000;
            $cr=sprintf("%02d",$crarray[2]).sprintf("%02d",$crarray[0]).sprintf("%02d",$crarray[1]);
          }  
        }
        if ((isset($columns['password'])) && (isset($fields[$columns['password']])))
          $pass=trim($fields[$columns['password']]);
        if (($randpass=="randnew") || (($pass=="") && ($randpass=="randnewonly")))
          $pass=sl_CreatePassword($RandomPasswordMask);
        if ((isset($columns['enabled'])) && (isset($fields[$columns['enabled']])))
        {
          $en=trim($fields[$columns['enabled']]);
          $en=ucfirst($en);
          if ($en!="No")
            $en="Yes";
        }
        if ((isset($columns['name'])) && (isset($fields[$columns['name']])))
          $nm=trim($fields[$columns['name']]);
        if ((isset($columns['email'])) && (isset($fields[$columns['email']])))
        {
          $em=trim($fields[$columns['email']]);
          if (($em!="") && (!sl_validate_email($em)))
          {
            $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE3;
            $usersignored++;
            continue;
          }          
        }
        if ((isset($columns['usergroups'])) && (isset($fields[$columns['usergroups']])) && ($fields[$columns['usergroups']]!=""))
        {
          $ug=trim($fields[$columns['usergroups']]);
          // Check SUBADMIN not adding or modifying ADMIN user
          if (($slsubadmin) && (preg_match("/(^|\^)ADMIN(:|\^|$)/", $ug)))
          {
            $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE4;
            $usersignored++;
            continue;
          }
        }  
        for($c=1;$c<=50;$c++)
        {
          $var="cu".$c;
          if ((isset($columns['custom'.$c])) && (isset($fields[$columns['custom'.$c]])))
            $$var=trim($fields[$columns['custom'.$c]]);
        }
        // Check we have required data for adding a user
        if (($cr=="") || ($user=="") || ($pass=="") || ($nm=="") || ($em=="") || ($ug=="") || ($en==""))
        {
          $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE5;
          $usersignored++;
          continue;
        }
        // Check if email address already used (by a different user) if required                        
        if ($EmailUnique==2)
        {
          $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." WHERE ".$EmailField."=".sl_quote_smart($em)." LIMIT 1");
          if ($mysql_result!==false)
          {   
            $num = mysqli_num_rows($mysql_result);
            if ($num>0)
            {
              $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE6;
              $usersignored++;
              continue;              
            }
          }  
        } 
        $importmsg="";
        $paramdata['userid']="";  // Not yet known  
        $paramdata['oldusername']=$user;
        $paramdata['username']=$user;
        $paramdata['password']=$pass;
        $paramdata['enabled']=$en;
        $paramdata['name']=$nm;
        $paramdata['email']=$em;
        $paramdata['usergroups']=$ug;
        $paramdata['custom1']=$cu1;
        $paramdata['custom2']=$cu2;
        $paramdata['custom3']=$cu3;
        $paramdata['custom4']=$cu4;
        $paramdata['custom5']=$cu5;
        $paramdata['custom6']=$cu6;
        $paramdata['custom7']=$cu7;
        $paramdata['custom8']=$cu8;
        $paramdata['custom9']=$cu9;
        $paramdata['custom10']=$cu10;
        $paramdata['custom11']=$cu11;
        $paramdata['custom12']=$cu12;
        $paramdata['custom13']=$cu13;
        $paramdata['custom14']=$cu14;
        $paramdata['custom15']=$cu15;
        $paramdata['custom16']=$cu16;
        $paramdata['custom17']=$cu17;
        $paramdata['custom18']=$cu18;
        $paramdata['custom19']=$cu19;
        $paramdata['custom20']=$cu20;
        $paramdata['custom21']=$cu21;
        $paramdata['custom22']=$cu22;
        $paramdata['custom23']=$cu23;
        $paramdata['custom24']=$cu24;
        $paramdata['custom25']=$cu25;
        $paramdata['custom26']=$cu26;
        $paramdata['custom27']=$cu27;
        $paramdata['custom28']=$cu28;
        $paramdata['custom29']=$cu29;
        $paramdata['custom30']=$cu30;
        $paramdata['custom31']=$cu31;
        $paramdata['custom32']=$cu32;
        $paramdata['custom33']=$cu33;
        $paramdata['custom34']=$cu34;
        $paramdata['custom35']=$cu35;
        $paramdata['custom36']=$cu36;
        $paramdata['custom37']=$cu37;
        $paramdata['custom38']=$cu38;
        $paramdata['custom39']=$cu39;
        $paramdata['custom40']=$cu40;
        $paramdata['custom41']=$cu41;
        $paramdata['custom42']=$cu42;
        $paramdata['custom43']=$cu43;
        $paramdata['custom44']=$cu44;
        $paramdata['custom45']=$cu45;
        $paramdata['custom46']=$cu46;
        $paramdata['custom47']=$cu47;
        $paramdata['custom48']=$cu48;
        $paramdata['custom49']=$cu49;
        $paramdata['custom50']=$cu50;
        $paramdata['from']=1;                
        // Call plugin event
        for ($p=0;$p<$slnumplugins;$p++)
        {
          if ($importmsg=="")
          {
            if (function_exists($slplugin_event_onCheckAddUser[$p]))
            {
              $res=call_user_func($slplugin_event_onCheckAddUser[$p],$slpluginid[$p],$paramdata);
              if ($res['ok']==false)
              {
                $importmsg=$res['message'];
              } 
            } 
          }  
        }
        if ($importmsg=="")
        {
          // Call eventhandler
          if (function_exists("sl_onCheckAddUser"))
          {
            $res=sl_onCheckAddUser($paramdata);
            if ($res['ok']==false)
              $importmsg=$res['message'];
          }  
        }  
        if ($importmsg!="")
        {
          $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE7." (".$importmsg.")";
          $usersignored++;
          continue;
        }
        // Convert password to hash if necessary but only if not already a hash
        $passwordtostore=$pass;
        if (($MD5passwords) && (!preg_match('/^[a-f0-9]{32}$/i', $pass)))
          $passwordtostore=md5($pass.$SiteKey);
        // If selectadded checked then set sl=Yes
        if ($selectadded=="on")
          $sl="Yes";
        else  
          $sl="No";
        $query="INSERT INTO ".$DbTableName." (".$SelectedField.",".$CreatedField.",".$UsernameField.",".$PasswordField.",".$EnabledField.
        ",".$NameField.",".$EmailField.",".$UsergroupsField.",".
        $Custom1Field.",".$Custom2Field.",".$Custom3Field.",".$Custom4Field.",".$Custom5Field.",".$Custom6Field.",".$Custom7Field.",".$Custom8Field.",".$Custom9Field.",".$Custom10Field.",".
        $Custom11Field.",".$Custom12Field.",".$Custom13Field.",".$Custom14Field.",".$Custom15Field.",".$Custom16Field.",".$Custom17Field.",".$Custom18Field.",".$Custom19Field.",".$Custom20Field.",".
        $Custom21Field.",".$Custom22Field.",".$Custom23Field.",".$Custom24Field.",".$Custom25Field.",".$Custom26Field.",".$Custom27Field.",".$Custom28Field.",".$Custom29Field.",".$Custom30Field.",".
        $Custom31Field.",".$Custom32Field.",".$Custom33Field.",".$Custom34Field.",".$Custom35Field.",".$Custom36Field.",".$Custom37Field.",".$Custom38Field.",".$Custom39Field.",".$Custom40Field.",".
        $Custom41Field.",".$Custom42Field.",".$Custom43Field.",".$Custom44Field.",".$Custom45Field.",".$Custom46Field.",".$Custom47Field.",".$Custom48Field.",".$Custom49Field.",".$Custom50Field.          
        ") VALUES(".sl_quote_smart($sl).",".sl_quote_smart($cr).",".sl_quote_smart($user).",".sl_quote_smart($passwordtostore).",".sl_quote_smart($en).",".sl_quote_smart($nm).",".sl_quote_smart($em).",".sl_quote_smart($ug).",".sl_quote_smart($cu1).",".sl_quote_smart($cu2).",".sl_quote_smart($cu3).",".sl_quote_smart($cu4).",".sl_quote_smart($cu5).",".sl_quote_smart($cu6).",".sl_quote_smart($cu7).",".sl_quote_smart($cu8).",".sl_quote_smart($cu9).",".sl_quote_smart($cu10).",".
        sl_quote_smart($cu11).",".sl_quote_smart($cu12).",".sl_quote_smart($cu13).",".sl_quote_smart($cu14).",".sl_quote_smart($cu15).",".sl_quote_smart($cu16).",".sl_quote_smart($cu17).",".sl_quote_smart($cu18).",".sl_quote_smart($cu19).",".sl_quote_smart($cu20).",".
        sl_quote_smart($cu21).",".sl_quote_smart($cu22).",".sl_quote_smart($cu23).",".sl_quote_smart($cu24).",".sl_quote_smart($cu25).",".sl_quote_smart($cu26).",".sl_quote_smart($cu27).",".sl_quote_smart($cu28).",".sl_quote_smart($cu29).",".sl_quote_smart($cu30).",".
        sl_quote_smart($cu31).",".sl_quote_smart($cu32).",".sl_quote_smart($cu33).",".sl_quote_smart($cu34).",".sl_quote_smart($cu35).",".sl_quote_smart($cu36).",".sl_quote_smart($cu37).",".sl_quote_smart($cu38).",".sl_quote_smart($cu39).",".sl_quote_smart($cu40).",".
        sl_quote_smart($cu41).",".sl_quote_smart($cu42).",".sl_quote_smart($cu43).",".sl_quote_smart($cu44).",".sl_quote_smart($cu45).",".sl_quote_smart($cu46).",".sl_quote_smart($cu47).",".sl_quote_smart($cu48).",".sl_quote_smart($cu49).",".sl_quote_smart($cu50).")";          
        if (!$DemoMode)
        {
          $mysql_result=mysqli_query($mysql_link,$query);
          if ($mysql_result===false)
          {
            $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE8;
            $usersignored++;
            continue;
          }
          // User got added ok
          $usersadded++;
          // Event point
          $userid=mysqli_insert_id($mysql_link);
          $paramdata['userid']=$userid;
          $paramdata['oldusername']=$user;
          $paramdata['username']=$user;
          $paramdata['password']=$pass;
          $paramdata['enabled']=$en;
          $paramdata['name']=$nm;
          $paramdata['email']=$em;
          $paramdata['usergroups']=$ug;
          $paramdata['custom1']=$cu1;
          $paramdata['custom2']=$cu2;
          $paramdata['custom3']=$cu3;
          $paramdata['custom4']=$cu4;
          $paramdata['custom5']=$cu5;
          $paramdata['custom6']=$cu6;
          $paramdata['custom7']=$cu7;
          $paramdata['custom8']=$cu8;
          $paramdata['custom9']=$cu9;
          $paramdata['custom10']=$cu10;
          $paramdata['custom11']=$cu11;
          $paramdata['custom12']=$cu12;
          $paramdata['custom13']=$cu13;
          $paramdata['custom14']=$cu14;
          $paramdata['custom15']=$cu15;
          $paramdata['custom16']=$cu16;
          $paramdata['custom17']=$cu17;
          $paramdata['custom18']=$cu18;
          $paramdata['custom19']=$cu19;
          $paramdata['custom20']=$cu20;
          $paramdata['custom21']=$cu21;
          $paramdata['custom22']=$cu22;
          $paramdata['custom23']=$cu23;
          $paramdata['custom24']=$cu24;
          $paramdata['custom25']=$cu25;
          $paramdata['custom26']=$cu26;
          $paramdata['custom27']=$cu27;
          $paramdata['custom28']=$cu28;
          $paramdata['custom29']=$cu29;
          $paramdata['custom30']=$cu30;
          $paramdata['custom31']=$cu31;
          $paramdata['custom32']=$cu32;
          $paramdata['custom33']=$cu33;
          $paramdata['custom34']=$cu34;
          $paramdata['custom35']=$cu35;
          $paramdata['custom36']=$cu36;
          $paramdata['custom37']=$cu37;
          $paramdata['custom38']=$cu38;
          $paramdata['custom39']=$cu39;
          $paramdata['custom40']=$cu40;
          $paramdata['custom41']=$cu41;
          $paramdata['custom42']=$cu42;
          $paramdata['custom43']=$cu43;
          $paramdata['custom44']=$cu44;
          $paramdata['custom45']=$cu45;
          $paramdata['custom46']=$cu46;
          $paramdata['custom47']=$cu47;
          $paramdata['custom48']=$cu48;
          $paramdata['custom49']=$cu49;
          $paramdata['custom50']=$cu50;
          $paramdata['from']=1;
          // Call plugin event
          for ($p=0;$p<$slnumplugins;$p++)
          {
            if ($slplugin_event_onAddUser[$p]=="pwsec_onadduser")
                continue;            
            if (function_exists($slplugin_event_onAddUser[$p]))
              call_user_func($slplugin_event_onAddUser[$p],$slpluginid[$p],$paramdata);
          }         
          if (function_exists("sl_onAddUser"))
            sl_onAddUser($paramdata);
        }
        else
        {
          $usersadded++;
        }
      } // if (!$thisuserexists)
      else
      {
        if (($thisuserexists) && ($existingusers!="on"))
        {
          $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE15;
          $usersignored++;
          continue;
        }
        $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
        if ($row==false)
          continue;        
        $userid=$row[$IdField];
        $cr=$row[$CreatedField];
        $user=$row[$UsernameField];
        $pass=$row[$PasswordField];
        $en=$row[$EnabledField];
        $nm=$row[$NameField];
        $em=$row[$EmailField];
        $ug=$row[$UsergroupsField];
        $cu1=$row[$Custom1Field];
        $cu2=$row[$Custom2Field];
        $cu3=$row[$Custom3Field];
        $cu4=$row[$Custom4Field];
        $cu5=$row[$Custom5Field];
        $cu6=$row[$Custom6Field];
        $cu7=$row[$Custom7Field];
        $cu8=$row[$Custom8Field];
        $cu9=$row[$Custom9Field];
        $cu10=$row[$Custom10Field];
        $cu11=$row[$Custom11Field];
        $cu12=$row[$Custom12Field];
        $cu13=$row[$Custom13Field];
        $cu14=$row[$Custom14Field];
        $cu15=$row[$Custom15Field];
        $cu16=$row[$Custom16Field];
        $cu17=$row[$Custom17Field];
        $cu18=$row[$Custom18Field];
        $cu19=$row[$Custom19Field];
        $cu20=$row[$Custom20Field];
        $cu21=$row[$Custom21Field];
        $cu22=$row[$Custom22Field];
        $cu23=$row[$Custom23Field];
        $cu24=$row[$Custom24Field];
        $cu25=$row[$Custom25Field];
        $cu26=$row[$Custom26Field];
        $cu27=$row[$Custom27Field];
        $cu28=$row[$Custom28Field];
        $cu29=$row[$Custom29Field];
        $cu30=$row[$Custom30Field];
        $cu31=$row[$Custom31Field];
        $cu32=$row[$Custom32Field];
        $cu33=$row[$Custom33Field];
        $cu34=$row[$Custom34Field];
        $cu35=$row[$Custom35Field];
        $cu36=$row[$Custom36Field];
        $cu37=$row[$Custom37Field];
        $cu38=$row[$Custom38Field];
        $cu39=$row[$Custom39Field];
        $cu40=$row[$Custom40Field];
        $cu41=$row[$Custom41Field];
        $cu42=$row[$Custom42Field];
        $cu43=$row[$Custom43Field];
        $cu44=$row[$Custom44Field];
        $cu45=$row[$Custom45Field];
        $cu46=$row[$Custom46Field];
        $cu47=$row[$Custom47Field];
        $cu48=$row[$Custom48Field];
        $cu49=$row[$Custom49Field];
        $cu50=$row[$Custom50Field];
        $query="";
        // Get row data and validate
        if ((isset($columns['password'])) && (isset($fields[$columns['password']])) && (trim($fields[$columns['password']])!=""))
        {
          $pass=trim($fields[$columns['password']]);
          // Convert password to hash if necessary but only if not already a hash
          $passwordtostore=$pass;
          if (($MD5passwords) && (!preg_match('/^[a-f0-9]{32}$/i', $pass)))
            $passwordtostore=md5($pass.$SiteKey);
          if ($query!="")
            $query.=", ";
          $query.="$PasswordField=".sl_quote_smart($passwordtostore);
        }  
        if ((isset($columns['enabled'])) && (isset($fields[$columns['enabled']])) && (trim($fields[$columns['enabled']])!=""))
        {
          $en=trim($fields[$columns['enabled']]);
          $en=ucfirst($en);
          if ($en!="No")
            $en="Yes";
          if ($query!="")
            $query.=", ";
          $query.="$EnabledField=".sl_quote_smart($en);
        }
        if ((isset($columns['name'])) && (isset($fields[$columns['name']])) && (trim($fields[$columns['name']])!=""))
        {
          $nm=trim($fields[$columns['name']]);
          if ($query!="")
            $query.=", ";
          $query.="$NameField=".sl_quote_smart($nm);              
        }  
        if ((isset($columns['email'])) && (isset($fields[$columns['email']])) && (trim($fields[$columns['email']])!=""))
        {
          $em=trim($fields[$columns['email']]);
          if (!sl_validate_email($em))
          {
            $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE9;
            $usersignored++;
            continue;
          }
          // If required check email has not already been used
          if ($EmailUnique==2)
          {
            $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." WHERE ".$EmailField."=".sl_quote_smart($em)." AND ".$IdField."!=".sl_quote_smart($userid)." LIMIT 1");
            if ($mysql_result!==false)
            {   
              $num = mysqli_num_rows($mysql_result);
              if ($num>0)
              {
                $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE10;
                $usersignored++;
                continue;              
              }
            }  
          }
          if ($query!="")
            $query.=", ";
          $query.="$EmailField=".sl_quote_smart($em);              
        }
        if ((isset($columns['usergroups'])) && (isset($fields[$columns['usergroups']])) && (trim($fields[$columns['usergroups']])!=""))
        {
          $ug=trim($fields[$columns['usergroups']]);
          // Check SUBADMIN not adding or modifying ADMIN user
          if (($slsubadmin) && (preg_match("/(^|\^)ADMIN(:|\^|$)/", $ug)))
          {
            $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE11;
            $usersignored++;
            continue;
          }
          if ($query!="")
            $query.=", ";
          $query.="$UsergroupsField=".sl_quote_smart($ug);
        }  
        for($c=1;$c<=50;$c++)
        {
          if ((isset($columns['custom'.$c])) && (isset($fields[$columns['custom'.$c]])))
          {
            $var="cu".$c;
            $var2="Custom".$c."Field";
            $data=trim($fields[$columns['custom'.$c]]);
            if (($data!="") || ($blankdata=="allowblank"))
            {
              $$var=$data;
              if ($query!="")
                $query.=", ";
              $query.=$$var2."=".sl_quote_smart($$var);
            }
          }  
        }
        // Give last chance to plugins and event handler to block modification
        $paramdata['userid']=$userid; // This was blank prior to V4.5   
        $paramdata['oldusername']=$user;
        $paramdata['username']=$user;
        $paramdata['password']=$pass;
        $paramdata['enabled']=$en;
        $paramdata['name']=$nm;
        $paramdata['email']=$em;
        $paramdata['usergroups']=$ug;
        $paramdata['custom1']=$cu1;
        $paramdata['custom2']=$cu2;
        $paramdata['custom3']=$cu3;
        $paramdata['custom4']=$cu4;
        $paramdata['custom5']=$cu5;
        $paramdata['custom6']=$cu6;
        $paramdata['custom7']=$cu7;
        $paramdata['custom8']=$cu8;
        $paramdata['custom9']=$cu9;
        $paramdata['custom10']=$cu10;
        $paramdata['custom11']=$cu11;
        $paramdata['custom12']=$cu12;
        $paramdata['custom13']=$cu13;
        $paramdata['custom14']=$cu14;
        $paramdata['custom15']=$cu15;
        $paramdata['custom16']=$cu16;
        $paramdata['custom17']=$cu17;
        $paramdata['custom18']=$cu18;
        $paramdata['custom19']=$cu19;
        $paramdata['custom20']=$cu20;
        $paramdata['custom21']=$cu21;
        $paramdata['custom22']=$cu22;
        $paramdata['custom23']=$cu23;
        $paramdata['custom24']=$cu24;
        $paramdata['custom25']=$cu25;
        $paramdata['custom26']=$cu26;
        $paramdata['custom27']=$cu27;
        $paramdata['custom28']=$cu28;
        $paramdata['custom29']=$cu29;
        $paramdata['custom30']=$cu30;
        $paramdata['custom31']=$cu31;
        $paramdata['custom32']=$cu32;
        $paramdata['custom33']=$cu33;
        $paramdata['custom34']=$cu34;
        $paramdata['custom35']=$cu35;
        $paramdata['custom36']=$cu36;
        $paramdata['custom37']=$cu37;
        $paramdata['custom38']=$cu38;
        $paramdata['custom39']=$cu39;
        $paramdata['custom40']=$cu40;
        $paramdata['custom41']=$cu41;
        $paramdata['custom42']=$cu42;
        $paramdata['custom43']=$cu43;
        $paramdata['custom44']=$cu44;
        $paramdata['custom45']=$cu45;
        $paramdata['custom46']=$cu46;
        $paramdata['custom47']=$cu47;
        $paramdata['custom48']=$cu48;
        $paramdata['custom49']=$cu49;
        $paramdata['custom50']=$cu50;
        $paramdata['from']=1; 
        $importmsg="";                     
        // Call plugin event
        for ($p=0;$p<$slnumplugins;$p++)
        {
          if ($importmsg=="")
          {
            if ($slplugin_event_onCheckModifyUser[$p]=="pwsec_oncheckmodifyuser")
              continue;
            if (function_exists($slplugin_event_onCheckModifyUser[$p]))
            {
              $res=call_user_func($slplugin_event_onCheckModifyUser[$p],$slpluginid[$p],$paramdata);
              if ($res['ok']==false)
                $importmsg=$res['message'];
            } 
          }  
        }
        if ($importmsg=="")
        {
          // Call eventhandler
          if (function_exists("sl_onCheckModifyUser"))
          {
            $res=sl_onCheckModifyUser($paramdata);
            if ($res['ok']==false)
              $importmsg=$res['message'];
          }  
        }  
        if ($importmsg!="")
        {
          $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE12." (".$importmsg.")";
          $usersignored++;
          continue;
        }
        if ($query=="")
        {
          // Nothing to update
          $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE13;
          $usersignored++;
          continue;
        }
        if ($selectupdated=="on")
          $query.=",".$SelectedField."='Yes'";      
        $query="UPDATE ".$DbTableName." SET ".$query." WHERE ".$UsernameField."=".sl_quote_smart($user);
        $mysql_result=mysqli_query($mysql_link,$query);
        if ($mysql_result===false)   
        {
          $log[]=ADMINIU_LINE." ".$line." - ".$user." ".ADMINIU_ISSUE14;
          $usersignored++;
          continue;        
        }
        if (!$DemoMode)
        {
          // user updated
          $usersupdated++;
          // Event point
          $paramdata['userid']=$userid;  
          $paramdata['oldusername']=$user;
          $paramdata['username']=$user;
          $paramdata['password']=$pass;
          $paramdata['enabled']=$en;
          $paramdata['name']=$nm;
          $paramdata['email']=$em;
          $paramdata['usergroups']=$ug;
          $paramdata['custom1']=$cu1;
          $paramdata['custom2']=$cu2;
          $paramdata['custom3']=$cu3;
          $paramdata['custom4']=$cu4;
          $paramdata['custom5']=$cu5;
          $paramdata['custom6']=$cu6;
          $paramdata['custom7']=$cu7;
          $paramdata['custom8']=$cu8;
          $paramdata['custom9']=$cu9;
          $paramdata['custom10']=$cu10;
          $paramdata['custom11']=$cu11;
          $paramdata['custom12']=$cu12;
          $paramdata['custom13']=$cu13;
          $paramdata['custom14']=$cu14;
          $paramdata['custom15']=$cu15;
          $paramdata['custom16']=$cu16;
          $paramdata['custom17']=$cu17;
          $paramdata['custom18']=$cu18;
          $paramdata['custom19']=$cu19;
          $paramdata['custom20']=$cu20;
          $paramdata['custom21']=$cu21;
          $paramdata['custom22']=$cu22;
          $paramdata['custom23']=$cu23;
          $paramdata['custom24']=$cu24;
          $paramdata['custom25']=$cu25;
          $paramdata['custom26']=$cu26;
          $paramdata['custom27']=$cu27;
          $paramdata['custom28']=$cu28;
          $paramdata['custom29']=$cu29;
          $paramdata['custom30']=$cu30;
          $paramdata['custom31']=$cu31;
          $paramdata['custom32']=$cu32;
          $paramdata['custom33']=$cu33;
          $paramdata['custom34']=$cu34;
          $paramdata['custom35']=$cu35;
          $paramdata['custom36']=$cu36;
          $paramdata['custom37']=$cu37;
          $paramdata['custom38']=$cu38;
          $paramdata['custom39']=$cu39;
          $paramdata['custom40']=$cu40;
          $paramdata['custom41']=$cu41;
          $paramdata['custom42']=$cu42;
          $paramdata['custom43']=$cu43;
          $paramdata['custom44']=$cu44;
          $paramdata['custom45']=$cu45;
          $paramdata['custom46']=$cu46;
          $paramdata['custom47']=$cu47;
          $paramdata['custom48']=$cu48;
          $paramdata['custom49']=$cu49;
          $paramdata['custom50']=$cu50;
          $paramdata['from']=1;                        
          // Call plugin event
          for ($p=0;$p<$slnumplugins;$p++)
          {
            if ($slplugin_event_onModifyUser[$p]=="pwsec_onmodifyuser")
              continue;
            if (function_exists($slplugin_event_onModifyUser[$p]))
              call_user_func($slplugin_event_onModifyUser[$p],$slpluginid[$p],$paramdata);
          }
          // Call user event handler
          if (function_exists("sl_onModifyUser"))
            sl_onModifyUser($paramdata);
        }
      } //if (!$thisuserexists) else
    }
    fclose($fh);
    mysqli_close($mysql_link);
    @unlink($_FILES['slimportedusers']['tmp_name']);
    returnSuccess($log,$totalrecords,$usersadded,$usersignored,$usersupdated);
    exit;
  }

  function returnSuccess($log,$totalrecords,$usersadded,$usersignored,$usersupdated)
  {
    $data['success'] = true;
    $data['totalrecords'] = $totalrecords;
    $data['usersadded'] = $usersadded;
    $data['usersignored'] = $usersignored;
    $data['usersupdated'] = $usersupdated;
    $data['log'] = $log;
    echo json_encode($data);
    exit;
  }

  function returnError($data,$errors,$msg)
  {
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

?>
