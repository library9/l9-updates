<?php
// Next line can disable gzip if it causes issues
//header("Content-Encoding: none");
$backupnozip=false;
$groupswithaccess="ADMIN";
$noaccesspage="";
$adminajaxcall=true;
require("sitelokpw.php");
if (!isset($adminlanguage))
  $adminlanguage="en";
require("adminlanguage-".$adminlanguage.".php"); 
// Check CSRF value
if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
{
  ?>
  {
  "backupstatus": "error",
  "message": "<?php echo ADMINMSG_CSRFFAILED; ?>"
  }
  <?php
  exit;      
}
$cacheFile=$SitelokLocation."writefiles/backupprogress.json";
$commandFile=$SitelokLocation."writefiles/backupcontrol.json";
$tmpsession=array();
$backupact=$_POST['backupact'];
if ($backupact=="")
{
  $backupcompress=$_POST['backupcompress'];
  $_SESSION['ses_slbackupcompress']=$backupcompress;  
  $backupmigrate=$_POST['backupmigrate'];
  $_SESSION['ses_slbackupmigrate']=$backupmigrate;  
}
else
{
  $backupcompress=$_SESSION['ses_slbackupcompress'];
  $backupmigrate=$_SESSION['ses_slbackupmigrate'];
  $tmpsession['ses_slbackupcurrenttable']=$_SESSION['ses_slbackupcurrenttable'];
  $tmpsession['ses_slbackupcurrentrow']=$_SESSION['ses_slbackupcurrentrow'];
  $tmpsession['ses_slbackupfname']=$_SESSION['ses_slbackupfname'];
  $tmpsession['ses_slbackuptables']=$_SESSION['ses_slbackuptables'];
  $tmpsession['ses_slbackuptablerows']=$_SESSION['ses_slbackuptablerows'];
  $tmpsession['ses_slbackuptablefields']=$_SESSION['ses_slbackuptablefields'];
  $tmpsession['ses_slbackuptablefieldnames']=$_SESSION['ses_slbackuptablefieldnames'];
}
session_write_close();
// Check if gzip is supported
if (!function_exists("gzopen"))  
{
  ?>
  {
  "backupstatus": "error",
  "message": "<?php echo 'PHP on this server does not support the gzopen() function for compression'; ?>"
  }
  <?php
  exit;      
} 
$maxtime=ini_get('max_execution_time');
if ((!is_numeric($maxtime)) || ($maxtime<=0))
  $maxtime=30;
if ($maxtime>60)
  $maxtime=60;  
$maxtime=$maxtime-5;	 
$scriptstart=time();
$mysql_link=sl_DBconnect();
if ($mysql_link==false)
{
  ?>
  {
  "backupstatus": "error",
  "message": "<?php echo ADMINMSG_MYSQLERROR; ?>"
  }
  <?php
  exit;      
}
if ($backupact=="")
{
  // Create json file for cancel handling
  $json = <<<EOT
{
  "comact": ""
}
EOT;

  @file_put_contents($commandFile,$json);
  // We are starting the process
  $tmpsession['ses_slbackupcurrenttable']=0;
  $tmpsession['ses_slbackupcurrentrow']=-1;
  // Create file
  $fname=$BackupLocation."sitelok_".gmdate("YmdHis")."_".substr(md5($SiteKey.(string)time()),0,10).".tmp";
  $tmpsession['ses_slbackupfname']=$fname;
  // Make array of table names starting with the ones we know exist
  $tables=array();
  $tables[0]=$DbTableName;
  $tables[1]=$DbConfigTableName;
  $tables[2]=$DbLogTableName;
  $tables[3]=$DbGroupTableName;
  $tables[4]=$DbOrdersTableName;
  $tables[5]=$DbPluginsTableName;
  // Now add any others not already included that start with sl_
	$mysql_result = mysqli_query($mysql_link,'SHOW TABLES');
	$c=6;
	while($row = mysqli_fetch_row($mysql_result))
	{
	  if ((substr($row[0],0,3)=="sl_") && (false===array_search($row[0], $tables)))
	  {
		  $tables[$c] = $row[0];
		  $c++;
		}  
	}
  $tmpsession['ses_slbackuptables']=serialize($tables);
  // Get total fields and records in each table and field names
  for ($k=0;$k<count($tables);$k++)
  {
  	$mysql_result = mysqli_query($mysql_link,'SELECT COUNT(*) FROM '.$tables[$k]);
  	$row = mysqli_fetch_row($mysql_result);
  	$tablerows[$k]=$row[0];
  	$mysql_result = mysqli_query($mysql_link,'SHOW COLUMNS FROM '.$tables[$k]);
  	$tablefields[$k]=mysqli_num_rows($mysql_result);
    $field=0;
    while ($row = mysqli_fetch_assoc($mysql_result))
    {
      $tablefieldnames[$k][$field]=$row['Field'];
      $field++; 
    }
  }
  $tmpsession['ses_slbackuptablerows']=serialize($tablerows);
  $tmpsession['ses_slbackuptablefields']=serialize($tablefields);
  $tmpsession['ses_slbackuptablefields']=serialize($tablefields);
  $tmpsession['ses_slbackuptablefieldnames']=serialize($tablefieldnames);
}

if (($backupact=="") || ($backupact=="callback"))
{
  // Get variables from session so we can see where we got to
  $fname=$tmpsession['ses_slbackupfname'];
  $tables=unserialize($tmpsession['ses_slbackuptables']);
  $tablerows=unserialize($tmpsession['ses_slbackuptablerows']);
  // Get total of all table rows
  $allrows=array_sum($tablerows);
  $tablefields=unserialize($tmpsession['ses_slbackuptablefields']);
  $backupcurrenttable=$tmpsession['ses_slbackupcurrenttable'];
  $backupcurrentrow=$tmpsession['ses_slbackupcurrentrow'];
  $tablefieldnames=unserialize($tmpsession['ses_slbackuptablefieldnames']);
  $fh=@fopen($fname,"ab");
  if ($fh===false)
  {
    ?>
    {
    "backupstatus": "error",
    "message": "<?php echo 'Unable to open file for writing'; ?>"
    }
    <?php
    exit;
  }
  for ($t=$backupcurrenttable;$t<count($tables);$t++)
  {  
    // Get total of all rows so far
    $allrowssofar=0;
    for ($tot=0;$tot<$backupcurrenttable;$tot++)
      $allrowssofar+=$tablerows[$tot];
    if ($backupcurrentrow==-1)
    {
      $percent=round(($allrowssofar/$allrows)*100);
      if ($percent==100)
        $percent=99;
      updateprogress($percent);
      // We need to output table structure
		  if (($backupmigrate==0) || ($tables[$backupcurrenttable]!=$DbConfigTableName))
		  {
        fwrite($fh,"DROP TABLE IF EXISTS ".$tables[$backupcurrenttable].";\n");    
        $row=mysqli_fetch_row(mysqli_query($mysql_link,'SHOW CREATE TABLE '.$tables[$backupcurrenttable]));
        fwrite($fh,"\n\n".$row[1].";\n\n");
        fwrite($fh,"\n");
      }
      // Update where we are so far
      $backupcurrentrow=0;
    }
    // Check for timeout approaching
 	  if (($maxtime!=0) && ((time()-$scriptstart)>=($maxtime-1)))
 	  {
 	    callback($fh);
      exit;
 	  } 
    if ($backupcurrentrow>-1)
    {
      // We need to output rows
      for ($l=$backupcurrentrow;$l<$backupcurrentrow+$sl_dbblocksize;$l=$l+$sl_dbblocksize)
      {
        $percent=round((($allrowssofar+$backupcurrentrow)/$allrows)*100);
        if ($percent==100)
          $percent=99;
        updateprogress($percent);
        $limit=" LIMIT ".$l.",".$sl_dbblocksize;
        $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$tables[$backupcurrenttable].$limit);
  			while($row = mysqli_fetch_row($mysql_result))
  			{
          // See if handling migration in slconfig table 			
 				  if (($tables[$backupcurrenttable]==$DbConfigTableName) && ($backupmigrate==1))
 				  {
 				    $line="";
    				for($j=0; $j<$tablefields[$backupcurrenttable]; $j++) 
    				{
              if (($tablefieldnames[$backupcurrenttable][$j]=="siteloklocation") || ($tablefieldnames[$backupcurrenttable][$j]=="emaillocation") || ($tablefieldnames[$backupcurrenttable][$j]=="backuplocation") || ($tablefieldnames[$backupcurrenttable][$j]=="version"))
                continue;
    					if (isset($row[$j]))
    					{
      				  if ($line!="")
      				    $line.=", ";  
    					  $line.=$tablefieldnames[$backupcurrenttable][$j]."='".mysqli_real_escape_string($mysql_link,$row[$j])."'";
    					}
    				  else
    				  {
      				  if ($line!="")
      				    $line.=",";      					
    				    $line.='""';
    				  }
    				} 
    				$line.=" WHERE confignum=1;\n";
    				$line='UPDATE '.$tables[$backupcurrenttable].' SET '.$line;
    				fwrite($fh,$line);
 				  }
 				  else
 				  {
 				    $line="";
    				for($j=0; $j<$tablefields[$backupcurrenttable]; $j++) 
    				{
  //    					$row[$j] = addslashes($row[$j]);
  //    					$row[$j] = str_replace("\n","\\n",$row[$j]);
    					if (isset($row[$j]))
    					{
      				  if ($line!="")
      				    $line.=",";      					
  //    					  $line.='"'.$row[$j].'"';
    					  $line.="'".mysqli_real_escape_string($mysql_link,$row[$j])."'";
    					}
    				  else
    				  {
      				  if ($line!="")
      				    $line.=",";      					
    				    $line.='""';
    				  }
    				}  				
    				$line.=");\n";
    				$line='INSERT INTO '.$tables[$backupcurrenttable].' VALUES('.$line;
    				fwrite($fh,$line);
  				}
  				$backupcurrentrow++;
  


          // See if cancelled
          $str=@file_get_contents($commandFile);
          if ($str!==false)
          {
            $jsonstr=json_decode($str);
            if ($jsonstr->{'comact'}=="cancel")
            {
              @unlink($fname);
              unset($_SESSION['ses_slbackupcurrenttable']);
              unset($_SESSION['ses_slbackupcurrentrow']);
              unset($_SESSION['ses_slbackupfname']);
              unset($_SESSION['ses_slbackuptables']);
              unset($_SESSION['ses_slbackuptablerows']);
              unset($_SESSION['ses_slbackuptablefields']);
              unset($_SESSION['ses_slbackuptablefields']);
              unset($_SESSION['ses_slbackuptablefieldnames']);              
            ?>
{
  "backupstatus": "cancelled",
  "message": ""
}
            <?php
            exit;      
            }
          }




  				// Check for timeout approaching
       	  if (($maxtime!=0) && ((time()-$scriptstart)>=($maxtime-1)))
       	  {
       	    callback($fh);
    	      exit;
       	  } 	
  			}    		
      } 
   		fwrite($fh,"\n\n\n"); 
    }
    $backupcurrenttable++;
    $backupcurrentrow=-1;
  }
	// Check for timeout approaching
  if (($maxtime!=0) && ((time()-$scriptstart)>=($maxtime-1)))
  {
    callback($fh);
    exit;
  } 
  fwrite($fh,"# End of backup file\n");
  fclose($fh);
  rename($fname,str_replace(".tmp",".sql",$fname));
  if ($backupcompress==1)
  {
    if (function_exists("gzopen"))
    {
      updateprogress("Compressing file");
      gzipbackup(str_replace(".tmp",".sql",$fname));
      @unlink(str_replace(".tmp",".sql",$fname));
    }      
  }
  unset($_SESSION['ses_slbackupcurrenttable']);
  unset($_SESSION['ses_slbackupcurrentrow']);
  unset($_SESSION['ses_slbackupfname']);
  unset($_SESSION['ses_slbackuptables']);
  unset($_SESSION['ses_slbackuptablerows']);
  unset($_SESSION['ses_slbackuptablefields']);
  unset($_SESSION['ses_slbackuptablefields']);
  unset($_SESSION['ses_slbackuptablefieldnames']);
  ?>
  {
  "backupstatus": "finished",    
  "message": ""
  }
  <?php
  exit;
}

function callback($fh)
{
  global $fh,$backupcurrenttable,$backupcurrentrow;
  global $SessionName,$tmpsession;
  if ($SessionName!="")
    session_name($SessionName);
  session_start();
  $_SESSION['ses_slbackupcurrenttable']=$backupcurrenttable;
  $_SESSION['ses_slbackupcurrentrow']=$backupcurrentrow;
  $_SESSION['ses_slbackupfname']=$tmpsession['ses_slbackupfname'];
  $_SESSION['ses_slbackuptables']=$tmpsession['ses_slbackuptables'];
  $_SESSION['ses_slbackuptablerows']=$tmpsession['ses_slbackuptablerows'];
  $_SESSION['ses_slbackuptablefields']=$tmpsession['ses_slbackuptablefields'];
  $_SESSION['ses_slbackuptablefields']=$tmpsession['ses_slbackuptablefields'];
  $_SESSION['ses_slbackuptablefieldnames']=$tmpsession['ses_slbackuptablefieldnames'];  
  fclose($fh);
  ?>
  {
  "backupstatus": "more",
  "message": ""
  }
  <?php
  exit;      
}

function updateprogress($msg)
{
  global $cacheFile;
  // Output to json file for progress reporting
  $json = <<<EOT
{
  "message": "{$msg}"
}
EOT;
    file_put_contents($cacheFile,$json);
}

function gzipbackup($src, $level = 5, $dst = false){
    if($dst == false){
        $dst = $src.".gz";
    }
    if(file_exists($src)){
        $filesize = filesize($src);
        $src_handle = fopen($src, "r");
        if(!file_exists($dst)){
            $dst_handle = gzopen($dst, "w$level");
            while(!feof($src_handle)){
                $chunk = fread($src_handle, 2048);
                gzwrite($dst_handle, $chunk);
            }
            fclose($src_handle);
            gzclose($dst_handle);
            return true;
        } else {
            error_log("$dst already exists");
        }
    } else {
        error_log("$src doesn't exist");
    }
    return false;
}    
?>
