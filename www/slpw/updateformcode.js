$('#embed').on('ifChecked', function(event){
               updatepage();
});

$('#source').on('ifChecked', function(event){
               updatepage();
});

$('#style').on('ifChecked', function(event){
               updatepage();
});

$('#simple').on('ifChecked', function(event){
               updatepage();
});


updatepage();

function updatepage()
{
  if (($('#embed').iCheck('embed')[0].checked) && ($('#style').iCheck('style')[0].checked))
  {
     document.getElementById('sourcecodenormal').style['display']="none"; 
     document.getElementById('sourcecodesimple').style['display']="none"; 
     document.getElementById('linkednormal').style['display']="block"; 
     document.getElementById('linkedsimple').style['display']="none"; 
  }
  if (($('#embed').iCheck('embed')[0].checked) && ($('#simple').iCheck('simple')[0].checked))
  {
     document.getElementById('sourcecodenormal').style['display']="none"; 
     document.getElementById('sourcecodesimple').style['display']="none"; 
     document.getElementById('linkednormal').style['display']="none"; 
     document.getElementById('linkedsimple').style['display']="block"; 
  }
  if (($('#source').iCheck('source')[0].checked) && ($('#style').iCheck('style')[0].checked))
  {
     document.getElementById('sourcecodenormal').style['display']="block"; 
     document.getElementById('sourcecodesimple').style['display']="none"; 
     document.getElementById('linkednormal').style['display']="none"; 
     document.getElementById('linkedsimple').style['display']="none"; 
  }
  if (($('#source').iCheck('source')[0].checked) && ($('#simple').iCheck('simple')[0].checked))
  {
     document.getElementById('sourcecodenormal').style['display']="none"; 
     document.getElementById('sourcecodesimple').style['display']="block"; 
     document.getElementById('linkednormal').style['display']="none"; 
     document.getElementById('linkedsimple').style['display']="none"; 
  }
}