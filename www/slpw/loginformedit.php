<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  if (!defined('ADMINET_INPPADDING'))
    define("ADMINET_INPPADDING","Input Padding (Vert Horz)");
  if (!defined('ADMINET_BTNPADDING'))
    define("ADMINET_BTNPADDING","Button Padding (Vert Horz)");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Get current values
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    die(ADMINMSG_MYSQLERROR);
  }
  if (isset($_GET['actid']))
    $actid=$_GET['actid'];
  if (isset($_GET['act']))
    $act=$_GET['act'];



?>
<!DOCTYPE html>

<html>
<head>
    <?php
    $pagename="loginformedit";
    include("adminhead.php");
    ?>
    <title><?php echo ADMINMENU_LOGINFORMDESIGN; ?></title>
    <link rel="stylesheet" href="jquery-minicolors-master/jquery.minicolors.css" type="text/css">
    <link rel="stylesheet" href="loginformedit.css" type="text/css">


 <!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
    <!--[if lte IE 6]>
  <style type="text/css">
    div#loginbox {
    width: 420px;
    }
<![endif]-->

<script type="text/javascript">
function startvalues()
{
  <?php if ($act=="addform") { ?>  
  document.getElementById('newformname').value='Untitled';
  $('#newloginfont').selectpicker('val', 'Arial, Helvetica, sans-serif');  
  $('#newmessagecolor').minicolors('value', '#FF0000');  
  document.getElementById('newmessagesize').value="12";
  $('#newmessagestyle').selectpicker('val', 'normal normal bold');  
  document.getElementById('newusernamelabel').value="Username";
  document.getElementById('newusernameplaceholder').value="username";
  document.getElementById('newpasswordlabel').value="Password";
  document.getElementById('newpasswordplaceholder').value="password";
  <?php if ($TuringLogin==1) { ?>
    document.getElementById('includecaptcha').checked=true;
  <?php } else { ?>  
    document.getElementById('includecaptcha').checked=false;
  <?php } ?>  
  document.getElementById('newcaptchalabel').value="Captcha"
  document.getElementById('newcaptchaplaceholder').value="captcha"
  <?php if ($CookieLogin==1) { ?>
    includeremember=document.getElementById('includeremember').checked=true;
  <?php } else { ?>  
    includeremember=document.getElementById('includeremember').checked=false;
  <?php } ?>  
  rememberlabel=document.getElementById('newrememberlabel').value="Remember me"
  <?php if ($CookieLogin==2) { ?>
    includeautologin=document.getElementById('includeautologin').checked=true;
  <?php } else { ?>  
    includeautologin=document.getElementById('includeautologin').checked=false;
  <?php } ?>    
  document.getElementById('newautologinlabel').value="Auto Login";
  $('#newlabelcolor').minicolors('value', '#1A305E');  
  document.getElementById('newlabelsize').value="16";
  $('#newlabelstyle').selectpicker('val', 'normal normal bold');  
  $('#newinputtextcolor').minicolors('value', '#1A305E');  
  document.getElementById('newinputtextsize').value="16";
  $('#newinputtextstyle').selectpicker('val', 'normal normal normal');  
  $('#newinputbackcolor').minicolors('value', '#FFFFFF');  
  $('#newbordersize').selectpicker('val', '1');  
  $('#newbordercolor').minicolors('value', '#378EE5');  
  $('#newborderradius').selectpicker('val', '0');  
  $('#newinputpaddingv').selectpicker('val', '0.3em');    
  $('#newinputpaddingh').selectpicker('val', '0.3em');    
  document.getElementById('newlogintext').value="Login";
  $('#newlogintextfont').selectpicker('val', 'Arial, Helvetica, sans-serif');  
  $('#newlogintextstyle').selectpicker('val', 'normal normal bold');  
  $('#newlogintextcolor').minicolors('value', '#FFFFFF');  
  document.getElementById('newlogintextsize').value="14";
  $('#newloginbuttonfilltype').selectpicker('val', 'gradient');  
  $('#newloginbuttoncolorfrom').minicolors('value', '#1A305E');  
  $('#newloginbuttoncolorto').minicolors('value', '#378EE5');  
  $('#newbtnborderstyle').selectpicker('val', 'solid');  
  $('#newbtnbordercolor').minicolors('value', '#FFFFFF');  
  $('#newbtnpaddingv').selectpicker('val', '6');    
  $('#newbtnpaddingh').selectpicker('val', '24');        
  document.getElementById('newbtnbordersize').value="0";
  $('#newloginbuttonshape').selectpicker('val', '10');  
  document.getElementById('includeforgot').checked=true;
  document.getElementById('newforgottext').value="Forgotten?";
  $('#newforgotcolor').minicolors('value', '#1A305E');  
  document.getElementById('newforgotsize').value="12";
  $('#newforgotstyle').selectpicker('val', 'normal normal normal');  
  document.getElementById('includesignup').checked=false;
  document.getElementById('newsignuptext').value="Not signed up yet?";
  document.getElementById('newsignupurl').value="/register.php";
  $('#newsignupcolor').minicolors('value', '#1A305E');  
  document.getElementById('newsignupsize').value="14";
  $('#newsignupstyle').selectpicker('val', 'normal normal bold');  
  $('#newbottommargin').selectpicker('val', '15');  
  document.getElementById('newmaxformwidth').value="200";
  $('#newbackgroundcolor').minicolors('value', '#FFFFFF');  
  <?php } ?>

  <?php if (($act=="editform") || ($act=="duplform")){
  $mysql_result=mysqli_query($mysql_link,"SELECT name FROM sl_forms WHERE id=".sl_quote_smart($actid));
  if ($mysql_result===false)
  {
    print("Can't read form template");
    exit;      
  } 
  $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
  $formname=$row['name'];
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_loginforms WHERE id=".sl_quote_smart($actid));
  if ($mysql_result===false)
  {
    print("Can't read form template");
    exit;      
  }
  $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
  ?>
  // Set form style variables
  document.getElementById('newformname').value='<?php if ($act=="duplform") echo str_replace("'","\'",$formname)." copy"; else echo str_replace("'","\'",$formname); ?>';
  $('#newloginfont').selectpicker('val', '<?php echo $row['mainfont']; ?>');  
  $('#newmessagecolor').minicolors('value', '#<?php echo $row['messagecolor']; ?>');  
  document.getElementById('newmessagesize').value='<?php echo $row['messagesize']; ?>';
  $('#newmessagestyle').selectpicker('val', '<?php echo $row['messagestyle']; ?>');  
  document.getElementById('newusernamelabel').value='<?php echo str_replace("'", "\'",$row['usernamelabel']); ?>';
  document.getElementById('newusernameplaceholder').value='<?php echo str_replace("'", "\'",$row['usernameplaceholder']); ?>';
  document.getElementById('newpasswordlabel').value='<?php echo str_replace("'", "\'",$row['passwordlabel']); ?>';
  document.getElementById('newpasswordplaceholder').value='<?php echo str_replace("'", "\'",$row['passwordplaceholder']); ?>';
  <?php if ($row['includecaptcha']=="1") { ?>
  document.getElementById('includecaptcha').checked=true;
  <?php } else { ?>
  document.getElementById('includecaptcha').checked=false;    
  <?php } ?>
  document.getElementById('newcaptchalabel').value='<?php echo str_replace("'", "\'",$row['captchalabel']); ?>'
  document.getElementById('newcaptchaplaceholder').value='<?php echo str_replace("'", "\'",$row['captchaplaceholder']); ?>'
  <?php if ($row['includeremember']=="1") { ?>
  document.getElementById('includeremember').checked=true;
  <?php } else { ?>
  document.getElementById('includeremember').checked=false;    
  <?php } ?>
  document.getElementById('newrememberlabel').value='<?php echo str_replace("'", "\'",$row['rememberlabel']); ?>'
  <?php if ($row['includeautologin']=="1") { ?>
  document.getElementById('includeautologin').checked=true;
  <?php } else { ?>
  document.getElementById('includeautologin').checked=false;    
  <?php } ?>
  document.getElementById('newautologinlabel').value='<?php echo str_replace("'", "\'",$row['autologinlabel']); ?>';
  $('#newlabelcolor').minicolors('value', '#<?php echo $row['labelcolor']; ?>');  
  document.getElementById('newlabelsize').value='<?php echo $row['labelsize']; ?>';
  $('#newlabelstyle').selectpicker('val', '<?php echo $row['labelstyle']; ?>');  
  $('#newinputtextcolor').minicolors('value', '#<?php echo $row['inputtextcolor']; ?>');  
  document.getElementById('newinputtextsize').value='<?php echo $row['inputtextsize']; ?>';
  $('#newinputtextstyle').selectpicker('val', '<?php echo $row['inputtextstyle']; ?>');  
  $('#newinputbackcolor').minicolors('value', '#<?php echo $row['inputbackcolor']; ?>');  
  $('#newbordersize').selectpicker('val', '<?php echo $row['bordersize']; ?>');  
  $('#newbordercolor').minicolors('value', '#<?php echo $row['bordercolor']; ?>');    
  $('#newborderradius').selectpicker('val', '<?php echo $row['borderradius']; ?>');  
  $('#newinputpaddingv').selectpicker('val', '<?php echo $row['inputpaddingv']; ?>');  
  $('#newinputpaddingh').selectpicker('val', '<?php echo $row['inputpaddingh']; ?>');  
  document.getElementById('newlogintext').value='<?php echo $row['logintext']; ?>';
  $('#newlogintextfont').selectpicker('val', '<?php echo $row['logintextfont']; ?>');  
  $('#newlogintextstyle').selectpicker('val', '<?php echo $row['logintextstyle']; ?>');  
  $('#newlogintextcolor').minicolors('value', '#<?php echo $row['logintextcolor']; ?>');  
  document.getElementById('newlogintextsize').value='<?php echo $row['logintextsize']; ?>';
  $('#newloginbuttonfilltype').selectpicker('val', '<?php echo $row['loginfilltype']; ?>');  
  $('#newloginbuttoncolorfrom').minicolors('value', '#<?php echo $row['logincolorfrom']; ?>');  
  $('#newloginbuttoncolorto').minicolors('value', '#<?php echo $row['logincolorto']; ?>');  
  $('#newbtnborderstyle').selectpicker('val', '<?php echo $row['btnborderstyle']; ?>');  
  $('#newbtnbordercolor').minicolors('value', '#<?php echo $row['btnbordercolor']; ?>');  
  document.getElementById('newbtnbordersize').value='<?php echo $row['btnbordersize']; ?>';
  $('#newbtnpaddingv').selectpicker('val', '<?php echo $row['btnpaddingv']; ?>');  
  $('#newbtnpaddingh').selectpicker('val', '<?php echo $row['btnpaddingh']; ?>');  
  $('#newloginbuttonshape').selectpicker('val', '<?php echo $row['loginshape']; ?>');  
  <?php if ($row['includeforgot']=="1") { ?>
  document.getElementById('includeforgot').checked=true;
  <?php } else { ?>
  document.getElementById('includeforgot').checked=false;    
  <?php } ?>
  document.getElementById('newforgottext').value='<?php echo str_replace("'", "\'",$row['forgottext']); ?>';
  $('#newforgotcolor').minicolors('value', '#<?php echo $row['forgotcolor']; ?>');  
  document.getElementById('newforgotsize').value='<?php echo $row['forgotsize']; ?>';
  $('#newforgotstyle').selectpicker('val', '<?php echo $row['forgotstyle']; ?>');  
  <?php if ($row['includesignup']=="1") { ?>
  document.getElementById('includesignup').checked=true;
  <?php } else { ?>
  document.getElementById('includesignup').checked=false;    
  <?php } ?>
  document.getElementById('newsignuptext').value='<?php echo str_replace("'", "\'",$row['signuptext']); ?>';
  document.getElementById('newsignupurl').value='<?php echo $row['signupurl']; ?>';
  $('#newsignupcolor').minicolors('value', '#<?php echo $row['signupcolor']; ?>');  
  document.getElementById('newsignupsize').value='<?php echo $row['signupsize']; ?>';
  $('#newsignupstyle').selectpicker('val', '<?php echo $row['signupstyle']; ?>');  
  $('#newbottommargin').selectpicker('val', '<?php echo $row['bottommargin']; ?>');  
  document.getElementById('newmaxformwidth').value='<?php echo $row['maxformwidth']; ?>';
  $('#newbackgroundcolor').minicolors('value', '#<?php echo $row['backgroundcolor']; ?>');  
  <?php } ?>
  filltype('newloginbuttonfilltype','newloginbuttoncolortodiv');
  updateform();
  updatePreview();
}  
</script>


</head>
<?php include("adminthemeheader.php"); ?>
<!-- Content Wrapper. Contains page content -->

<body>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo ADMINMENU_LOGINFORMDESIGN; ?>
            <?php if ($act=="editform") { ?><small>form id: <?php echo $actid; ?></small><?php } ?>            
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><?php echo ADMINMENU_LOGINFORMDESIGN; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Your Page Content Here -->

          <div class="row">


         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
         <div class="box" id="previewbox">
         <div id="togglefixed" class="hidden-xs hidden-sm hidden-md"><span id="toggleicon" class="glyphicon glyphicon-pushpin" rel="tooltip" title="<?php echo ADMINET_TOGFIX; ?>" onclick="toggleFixed();"></span></div>
         <div class="box-body" id="formpreview">
            <!-- Simulate the login form in a div here -->
         </div><!-- /.box -->
         </div><!-- /.box-body -->
         </div><!-- /.col -->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

              <form id="loginform" role="form" class="form-horizontal" method="post">
              <input type="hidden" name="slcsrf" id="slcsrf" value="<?php echo $slcsrftoken; ?>">
              <input name="act" id="act" type="hidden" value="<?php echo $act; ?>">
              <input name="actid" id="actid" type="hidden" value="<?php echo $actid; ?>">
              <input name="formstylesfield" id="formstylesfield" type="hidden" value="">
              <div class="box">
                <div class="box-body">

                  <div class="form-group">
                      <label class="col-xs-12" for="newformname"><?php echo ADMINET_FRMNM; ?></label>
                      <div class="col-xs-12" id="newformnamediv">
                          <input type="text" id="newformname" name="newformname" class="form-control" maxlength="100" value="<?php echo $newformname; ?>">
                      </div>
                  </div>

                  <div class="panel-group" id="accordion" role="tablist">


                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse1">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse1" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_INPFLDS; ?></a>
                        </h4>
                      </div>
                      <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="form-group">
                              <label class="col-xs-12" for="newusernamelabel"><?php echo ADMINET_USERLBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newusernamelabeldiv">
                                  <input type="text" id="newusernamelabel" name="newusernamelabel" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newusernameplaceholder"><?php echo ADMINET_USERPLACE; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newusernameplaceholderdiv">
                                  <input type="text" id="newusernameplaceholder" name="newusernameplaceholder" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newpasswordlabel"><?php echo ADMINET_PASSLBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newpasswordlabeldiv">
                                  <input type="text" id="newpasswordlabel" name="newpasswordlabel" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newpasswordplaceholder"><?php echo ADMINET_PASSPLACE; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newpasswordplaceholderdiv">
                                  <input type="text" id="newpasswordplaceholder" name="newpasswordplaceholder" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                              <div class="col-sm-10">
                              <div class="checkbox">
                                <label>
                                <input type="checkbox" id="includecaptcha" name="includecaptcha" value="1">&nbsp;&nbsp;<?php echo ADMINET_INCLCAPTCHA; ?>
                                </label>
                              </div>
                              </div>
                          </div>
                          <div id="captchasection">
                          <div class="form-group">
                              <label class="col-xs-12" for="newcaptchalabel"><?php echo ADMINET_CAPTCHALBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newcaptchalabeldiv">
                                  <input type="text" id="newcaptchalabel" name="newcaptchalabel" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div> <!-- captchasection -->
                          <div class="form-group">
                              <label class="col-xs-12" for="newcaptchaplaceholder"><?php echo ADMINET_CAPTCHAPLACE; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newcaptchaplaceholderdiv">
                                  <input type="text" id="newcaptchaplaceholder" name="newcaptchaplaceholder" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          </div>
                          <div class="form-group">
                              <div class="col-sm-10">
                              <div class="checkbox">
                                <label>
                                <input type="checkbox" id="includeremember" name="includeremember" value="1" onchange="updateform()">&nbsp;&nbsp;<?php echo ADMINET_INCLREM; ?>
                                </label>
                              </div>
                              </div>
                          </div>
                          <div id="remembersection">
                          <div class="form-group">
                              <label class="col-xs-12" for="newrememberlabel"><?php echo ADMINET_REMLBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newrememberlabeldiv">
                                  <input type="text" id="newrememberlabel" name="newrememberlabel" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          </div> <!-- remembersection -->
                          <div class="form-group">
                              <div class="col-sm-10">
                              <div class="checkbox">
                                <label>
                                <input type="checkbox" id="includeautologin" name="includeautologin" value="1" onchange="updateform()">&nbsp;&nbsp;<?php echo ADMINET_INCLAUTO; ?>
                                </label>
                              </div>
                              </div>
                          </div>
                          <div id="autologinsection">
                          <div class="form-group">
                              <label class="col-xs-12" for="newautologinlabel"><?php echo ADMINET_AUTOLBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newautologinlabeldiv">
                                  <input type="text" id="newautologinlabel" name="newautologinlabel" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          </div> <!-- autologinsection -->
                        </div>
                      </div>
                    </div>  

                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse2">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse2" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_FORMSTYLE; ?></a>
                        </h4>
                      </div>
                      <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="form-group">
                            <label class="col-xs-12" for="newloginfont"><?php echo ADMINET_FONTTYP; ?></label>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newloginfontdiv">
                                <select id="newloginfont" name="newloginfont" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="Arial, Helvetica, sans-serif">Arial</option>
                                <option value="Times New Roman, Times, serif">Times New Roman</option>
                                <option value="Verdana, Geneva, sans-serif">Verdana</option>
                                <option value="Impact, Charcoal, sans-serif">Impact</option>
                                <option value="Palatino Linotype, Book Antiqua, Palatino, serif" >Palatino Linotype</option>
                                <option value="Tahoma, Geneva, sans-serif">Tahoma</option>
                                <option value="Century Gothic, sans-serif">Century Gothic</option>
                                <option value="Lucida Sans Unicode, Lucida Grande, sans-serif">Lucida Sans Unicode</option>
                                <option value="Arial Narrow, sans-serif">Arial Narrow</option>
                                <option value="Copperplate Gothic Light, sans-serif">Copperplate Gothic Light</option>
                                <option value="Lucida Console, Monaco, monospace">Lucida Console</option>
                                <option value="Gill Sans MT, sans-serif">Gill Sans MT</option>
                                <option value="Trebuchet MS, Helvetica, sans-serif">Trebuchet MS</option>
                                <option value="Courier New, Courier, monospace">Courier New</option>
                                <option value="Georgia, Serif">Georgia</option>
                                <option value="Garamond, Serif">Garamond</option>
                                </select>  
                            </div>    
                          </div>                      
                          <div class="form-group">
                            <label class="col-xs-12" for="newmessagestyle"><?php echo ADMINET_MSGSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newmessagestylediv" onchange="updatePreview()">
                                <select id="newmessagestyle" name="newmessagestyle" class="form-control selectpicker">
                                <option value="normal normal normal"><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold"><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal"><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold"><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newmessagecolordiv">
                                <input type="text" id="newmessagecolor" name="newmessagecolor" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newmessagesizediv">
                                <div class="input-group">
                                  <input type="number" id="newmessagesize" name="newmessagesize" class="form-control" maxlength="2" min="1" max="99" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newlabelstyle"><?php echo ADMINET_INPLBLSTY; ?></label>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newlabelstylediv" onchange="updatePreview()">
                              <select id="newlabelstyle" name="newlabelstyle" class="form-control selectpicker">
                              <option value="normal normal normal"><?php echo ADMINET_NORMAL; ?></option>
                              <option value="normal normal bold"><?php echo ADMINET_BOLD; ?></option>
                              <option value="italic normal normal"><?php echo ADMINET_ITALIC; ?></option>
                              <option value="italic normal bold"><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                              </select>  
                            </div>                             
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newlabelcolordiv">
                                <input type="text" id="newlabelcolor" name="newlabelcolor" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newlabelsizediv">
                                <div class="input-group">
                                  <input type="number" id="newlabelsize" name="newlabelsize" class="form-control" maxlength="2" min="1" max="99" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newinputtextstyle"><?php echo ADMINET_INPTXTSTY; ?></label>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newinputtextstylediv" onchange="updatePreview()">
                              <select id="newinputtextstyle" name="newinputtextstyle" class="form-control selectpicker">
                              <option value="normal normal normal"><?php echo ADMINET_NORMAL; ?></option>
                              <option value="normal normal bold"><?php echo ADMINET_BOLD; ?></option>
                              <option value="italic normal normal"><?php echo ADMINET_ITALIC; ?></option>
                              <option value="italic normal bold"><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                              </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newinputtextcolordiv">
                                <input type="text" id="newinputtextcolor" name="newinputtextcolor" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newinputtextsizediv">
                                <div class="input-group">
                                  <input type="number" id="newinputtextsize" name="newinputtextsize" class="form-control" maxlength="2" min="1" max="99" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div>
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbordersize"><?php echo ADMINET_INPTXTBRDSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbordersizediv" onchange="updatePreview()">
                                <select id="newbordersize" name="newbordersize" class="form-control selectpicker">
                                <option value="0"><?php echo ADMINET_NONE; ?></option>
                                <option value="1">1px</option>
                                <option value="2">2px</option>
                                <option value="3">3px</option>
                                <option value="4">4px</option>
                                <option value="5">5px</option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbordercolordiv">
                                <input type="text" id="newbordercolor" name="newbordercolor" class="form-control colorbox" maxlength="7" value="<?php echo $newbordercolor; ?>" onchange="updatePreview()">
                            </div> 
                             <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newborderradiusdiv" onchange="updatePreview()">
                                <select id="newborderradius" name="newborderradius" class="form-control selectpicker">
                                <option value="0"><?php echo ADMINET_SQUARE; ?></option>
                                <option value="5"><?php echo ADMINET_ROUNDED; ?> 5px</option>
                                <option value="10"><?php echo ADMINET_ROUNDED; ?> 10px</option>
                                <option value="15"><?php echo ADMINET_ROUNDED; ?> 15px</option>
                                <option value="20"><?php echo ADMINET_ROUNDED; ?> 20px</option>
                                </select>  
                            </div> 
                          </div>    
                          <div class="form-group">
                            <label class="col-xs-12" for="newinputpaddingv"><?php echo ADMINET_INPPADDING; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newinputpaddingvdiv" onchange="updatePreview()">
                                <select id="newinputpaddingv" name="newinputpaddingv" class="form-control selectpicker">
                                <option value="0.3em"><?php echo "default"; ?></option>
                                <option value="0px">0px</option>
                                <option value="5px">5px</option>
                                <option value="10px">10px</option>
                                <option value="15px">15px</option>
                                <option value="20px">20px</option>
                                <option value="25px">25px</option>
                                <option value="30px">30px</option>
                                </select>  
                            </div> 
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newinputpaddinghdiv" onchange="updatePreview()">
                                <select id="newinputpaddingh" name="newinputpaddingh" class="form-control selectpicker">
                                <option value="0.3em"><?php echo "default"; ?></option>
                                <option value="0px">0px</option>
                                <option value="5px">5px</option>
                                <option value="10px">10px</option>
                                <option value="15px">15px</option>
                                <option value="20px">20px</option>
                                <option value="25px">25px</option>
                                <option value="30px">30px</option>
                                </select>  
                            </div> 
                          </div>                                                
                          <div class="form-group">
                            <label class="col-xs-12" for="newinputbackcolor"><?php echo ADMINET_INPBCKCOL; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newinputbackcolordiv">
                              <input type="text" id="newinputbackcolor" name="newinputbackcolor" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                            </div> 
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbottommargin"><?php echo ADMINET_VERSPACE; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbottommargindiv">
                              <select id="newbottommargin" name="newbottommargin" class="form-control selectpicker" onchange="updatePreview()">
                              <option value="50">50px</option>
                              <option value="40">40px</option>
                              <option value="30">30px</option>
                              <option value="25">25px</option>
                              <option value="20">20px</option>
                              <option value="15">15px</option>
                              <option value="10">10px</option>
                              <option value="5">5px</option>
                              <option value="1">1px</option>
                              </select>  
                            </div>    
                          </div>                      
                          <div class="form-group">
                            <label class="col-xs-12" for="newmaxformwidth"><?php echo ADMINET_FRMMAXWID; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newmaxformwidthdiv">
                                <div class="input-group">
                                  <input type="number" id="newmaxformwidth" name="newmaxformwidth" class="form-control" maxlength="6" min="0" max="999999" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                            </div> 
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbackgroundcolor"><?php echo ADMINET_PREVCOL; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbackgroundcolordiv">
                              <input type="text" id="newbackgroundcolor" name="newbackgroundcolor" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                            </div> 
                          </div>                          
                        </div>
                      </div>
                    </div>  



                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse3">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse3" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_LOGINBTN; ?></a>
                        </h4>
                      </div>
                      <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="form-group">
                              <label class="col-xs-12" for="newlogintext"><?php echo ADMINET_BTNLBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newlogintextdiv">
                                  <input type="text" id="newlogintext" name="newlogintext" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newlogintextfont"><?php echo ADMINET_BTNFONT; ?></label>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newlogintextfontdiv">
                                <select id="newlogintextfont" name="newlogintextfont" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="Arial, Helvetica, sans-serif">Arial</option>
                                <option value="Times New Roman, Times, serif">Times New Roman</option>
                                <option value="Verdana, Geneva, sans-serif">Verdana</option>
                                <option value="Impact, Charcoal, sans-serif">Impact</option>
                                <option value="Palatino Linotype, Book Antiqua, Palatino, serif">Palatino Linotype</option>
                                <option value="Tahoma, Geneva, sans-serif">Tahoma</option>
                                <option value="Century Gothic, sans-serif">Century Gothic</option>
                                <option value="Lucida Sans Unicode, Lucida Grande, sans-serif">Lucida Sans Unicode</option>
                                <option value="Arial Narrow, sans-serif">Arial Narrow</option>
                                <option value="Copperplate Gothic Light, sans-serif">Copperplate Gothic Light</option>
                                <option value="Lucida Console, Monaco, monospace">Lucida Console</option>
                                <option value="Gill Sans MT, sans-serif">Gill Sans MT</option>
                                <option value="Trebuchet MS, Helvetica, sans-serif">Trebuchet MS</option>
                                <option value="Courier New, Courier, monospace">Courier New</option>
                                <option value="Georgia, Serif">Georgia</option>
                                <option value="Garamond, Serif">Garamond</option>
                                </select>  
                            </div>    
                          </div>                      
                          <div class="form-group">
                            <label class="col-xs-12" for="newlogintextstyle"><?php echo ADMINET_BTNLBLSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newlogintextstylediv">
                                <select id="newlogintextstyle" name="newlogintextstyle" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="normal normal normal"><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold"><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal"><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold"><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newlogintextcolordiv">
                                <input type="text" id="newlogintextcolor" name="newlogintextcolor" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newlogintextsizediv">
                                <div class="input-group">
                                  <input type="number" id="newlogintextsize" name="newlogintextsize" class="form-control" maxlength="2" min="1" max="99"" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newloginbuttonfilltype"><?php echo ADMINET_BTNCOL; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newloginbuttonfilltypediv">
                                <select id="newloginbuttonfilltype" name="newloginbuttonfilltype" class="form-control selectpicker" onchange="filltype('newloginbuttonfilltype','newloginbuttoncolortodiv'); updatePreview()">
                                <option value="solid"><?php echo ADMINET_SOLID; ?></option>
                                <option value="gradient"><?php echo ADMINET_GRADIENT; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newloginbuttoncolorfromdiv">
                                <input type="text" id="newloginbuttoncolorfrom" name="newloginbuttoncolorfrom" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                            </div> 
                            <div class="col-xs-6 col-sm-5 col-md-4 col-lg-5" id="newloginbuttoncolortodiv">
                              <div class="input-group">
                                <input type="text" id="newloginbuttoncolorto" name="newloginbuttoncolorto" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button" id="loginbuttoncolorreverse" onclick="gradientreverse('newloginbuttoncolorfrom','newloginbuttoncolorto')"><span class="glyphicon glyphicon-transfer actionicon" title="reverse"></span></button>
                                </span>
                              </div>                                                        
                            </div>                           
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtnborderstyle"><?php echo ADMINET_BTNBRDSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtnborderstylediv">
                                <select id="newbtnborderstyle" name="newbtnborderstyle" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="solid"><?php echo ADMINET_SOLID; ?></option>
                                <option value="dotted"><?php echo ADMINET_DOTTED; ?></option>
                                <option value="dashed"><?php echo ADMINET_DASHED; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbtnbordercolordiv">
                                <input type="text" id="newbtnbordercolor" name="newbtnbordercolor" class="form-control colorbox" maxlength="7" value="<?php echo $newbtnbordercolor; ?>" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newbtnbordersizediv">
                                <div class="input-group">
                                  <input type="number" id="newbtnbordersize" name="newbtnbordersize" class="form-control" maxlength="2" min="0" max="99" value="<?php echo $newbtnbordersize; ?>" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtnpaddingv"><?php echo ADMINET_BTNPADDING; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtnpaddingvdiv" onchange="updatePreview()">
                                <select id="newbtnpaddingv" name="newbtnpaddingv" class="form-control selectpicker">
                                <option value="6"><?php echo "default"; ?></option>
                                <option value="0">0px</option>
                                <option value="5">5px</option>
                                <option value="10">10px</option>
                                <option value="15">15px</option>
                                <option value="20">20px</option>
                                <option value="25">25px</option>
                                <option value="30">30px</option>
                                <option value="40">40px</option>
                                <option value="50">50px</option>
                                <option value="60">60px</option>
                                <option value="70">70px</option>
                                <option value="80">80px</option>
                                <option value="90">90px</option>
                                <option value="100">100px</option>
                                </select>  
                            </div> 
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtnpaddinghdiv" onchange="updatePreview()">
                                <select id="newbtnpaddingh" name="newbtnpaddingh" class="form-control selectpicker">
                                <option value="24"><?php echo "default"; ?></option>
                                <option value="0">0px</option>
                                <option value="5">5px</option>
                                <option value="10">10px</option>
                                <option value="15">15px</option>
                                <option value="20">20px</option>
                                <option value="25">25px</option>
                                <option value="30">30px</option>
                                <option value="40">40px</option>
                                <option value="50">50px</option>
                                <option value="60">60px</option>
                                <option value="70">70px</option>
                                <option value="80">80px</option>
                                <option value="90">90px</option>
                                <option value="100">100px</option>
                                </select>  
                            </div> 
                          </div>                                                    
                          <div class="form-group">
                            <label class="col-xs-12" for="newloginbuttonshape"><?php echo ADMINET_BTNSHAPE; ?></label>
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newloginbuttonshapediv">
                                <select id="newloginbuttonshape" name="newloginbuttonshape" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="0"><?php echo ADMINET_SQUARE; ?></option>
                                <option value="5"><?php echo ADMINET_ROUNDED; ?> 5px</option>
                                <option value="10"><?php echo ADMINET_ROUNDED; ?> 10px</option>
                                <option value="15"><?php echo ADMINET_ROUNDED; ?> 15px</option>
                                <option value="20"><?php echo ADMINET_ROUNDED; ?> 20px</option>
                                </select>  
                            </div>    
                          </div>                      
                        </div>
                      </div>
                    </div>  

                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse4">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse4" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_FRGLNK; ?></a>
                        </h4>
                      </div>
                      <div id="collapse4" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="form-group">
                              <div class="col-sm-10">
                              <div class="checkbox">
                                <label>
                                <input type="checkbox" id="includeforgot" name="includeforgot" value="1" onchange="updateform()">&nbsp;&nbsp;<?php echo ADMINET_INCLFORGOT; ?>
                                </label>
                              </div>
                              </div>
                          </div> 
                          <div id="forgotsection">                       
                          <div class="form-group">
                              <label class="col-xs-12" for="newforgottext"><?php echo ADMINET_FRGTXT; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newforgottextdiv">
                                  <input type="text" id="newforgottext" name="newforgottext" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newforgotstyle"><?php echo ADMINET_FRGSTY; ?></label>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newforgotstylediv">
                              <select id="newforgotstyle" name="newforgotstyle" class="form-control selectpicker" onchange="updatePreview()">
                              <option value="normal normal normal"><?php echo ADMINET_NORMAL; ?></option>
                              <option value="normal normal bold"><?php echo ADMINET_BOLD; ?></option>
                              <option value="italic normal normal"><?php echo ADMINET_ITALIC; ?></option>
                              <option value="italic normal bold"><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                              </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newforgotcolordiv">
                                <input type="text" id="newforgotcolor" name="newforgotcolor" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newforgotsizediv">
                                <div class="input-group">
                                  <input type="number" id="newforgotsize" name="newforgotsize" class="form-control" maxlength="2" min="1" max="99" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          </div> <!-- forgotsection -->
                        </div>
                      </div>
                    </div>  

                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse5">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse5" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_SIGNLNK; ?></a>
                        </h4>
                      </div>
                      <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="form-group">
                              <div class="col-sm-10">
                              <div class="checkbox">
                                <label>
                                <input type="checkbox" id="includesignup" name="includesignup" value="1" onchange="updateform()">&nbsp;&nbsp;<?php echo ADMINET_INCLSIGNUP; ?>
                                </label>
                              </div>
                              </div>
                          </div> 
                          <div id="signupsection">                                               
                          <div class="form-group">
                              <label class="col-xs-12" for="newforgottext"><?php echo ADMINET_SIGNTXT; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newsignuptextdiv">
                                  <input type="text" id="newsignuptext" name="newsignuptext" class="form-control" maxlength="100" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newforgottext"><?php echo ADMINET_SIGNURL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newsignupurldiv">
                                  <input type="text" id="newsignupurl" name="newsignupurl" class="form-control" maxlength="100" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newsignupstyle"><?php echo ADMINET_SIGNTXT; ?></label>
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-4" id="newsignupstylediv" onchange="updatePreview()">
                              <select id="newsignupstyle" name="newsignupstyle" class="form-control selectpicker">
                              <option value="normal normal normal"><?php echo ADMINET_NORMAL; ?></option>
                              <option value="normal normal bold"><?php echo ADMINET_BOLD; ?></option>
                              <option value="italic normal normal"><?php echo ADMINET_ITALIC; ?></option>
                              <option value="italic normal bold"><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                              </select>  
                            </div>                             
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newsignupcolordiv">
                                <input type="text" id="newsignupcolor" name="newsignupcolor" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newsignupsizediv">
                                <div class="input-group">
                                  <input type="number" id="newsignupsize" name="newsignupsize" class="form-control" maxlength="2" min="1" max="99" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          </div> <!-- signupsection -->
                        </div>
                      </div>
                    </div>




                  </div> <!-- panel-group -->

                </div> <!-- box body -->
              </div> <!-- box --> 

              <div class="alert alert-warning" role="alert" id="formissues"></div>

                    <div class="form-group">
                      <div class="col-xs-12">
                        <div class="btn-toolbar">
                            <button type="submit" id="submit" class="btn btn-primary pull-left"><?php echo ADMINBUTTON_SAVECHANGES; ?></button>
                            <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='loginforms.php';"><?php echo ADMINBUTTON_RETURNTOFORMS ?></button>
                            <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                        </div> 
                      </div>    
                    </div>

              <div id="resultloginform"></div>

              </form>




           </div> <!-- col -->


          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include("adminthemefooter.php"); ?>
    <script src="jquery-minicolors-master/jquery.minicolors.js" type="text/javascript"></script>
    <script src="loginformedit.js" type="text/javascript"></script>
    <script type="text/javascript">
      startvalues();
    </script>
</script>
</body>
</html>
