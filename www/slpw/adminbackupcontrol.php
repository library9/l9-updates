<?php
$groupswithaccess="ADMIN";
$noaccesspage="";
$adminajaxcall=true;
require("sitelokpw.php");
// Check CSRF value
if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
{
  ?>
  {
  "success": false,
  "message": "<?php echo ADMINMSG_CSRFFAILED; ?>"
  }
  <?php
  exit;      
}
$comact=$_POST['comact'];
$commandFile=$SitelokLocation."writefiles/backupcontrol.json";
// Create json file for pause handling
  $json = <<<EOT
{
  "comact": "{$comact}"
}
EOT;

@file_put_contents($commandFile,$json);
?>
{
"success": "true"
} 
<?php
exit;
?>