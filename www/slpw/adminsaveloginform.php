<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  require("admincommonfunctions.php");  

  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  $errors = array();    // array to hold validation errors
  $data = array();      // array to pass back data 
  $emailsenderror=false; // email send error is handled separately
  // Check CSRF value
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_CSRFFAILED.".");
    exit;
  }   
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_MYSQLERROR.".");
    exit;
  }
  $act=$_POST['act'];
  $actid=$_POST['actid'];
  $formstylesfield=$_POST['formstylesfield'];
  $formid=$actid;
  if (!$DemoMode)
  {
    $dataarray=json_decode($formstylesfield);
    // Create or update form in sl_forms table
    $query="sl_forms SET ";         
    $query.="name=".sl_quote_smart($dataarray[0]).",";
    $query.="type=".sl_quote_smart("login");
    if (($act=="addform") || ($act=="duplform"))
      $query = "INSERT INTO ".$query;
    else
      $query = "UPDATE ".$query." WHERE id=".sl_quote_smart($actid); 
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_MYSQLERROR);
      exit;
    }
    if (($act=="addform") || ($act=="duplform"))
      $formid=mysqli_insert_id($mysql_link);
    else
      $formid=$actid;
    $query ="sl_loginforms SET ";
    $query.="id=".sl_quote_smart($formid).",";
    $query.="mainfont=".sl_quote_smart($dataarray[1]).",";
    $query.="messagecolor=".sl_quote_smart(substr($dataarray[2],1)).",";
    $query.="messagesize=".sl_quote_smart($dataarray[3]).",";
    $query.="messagestyle=".sl_quote_smart($dataarray[4]).",";
    $query.="labelcolor=".sl_quote_smart(substr($dataarray[5],1)).",";
    $query.="labelsize=".sl_quote_smart($dataarray[6]).",";
    $query.="labelstyle=".sl_quote_smart($dataarray[7]).",";
    $query.="inputtextcolor=".sl_quote_smart(substr($dataarray[8],1)).",";
    $query.="inputtextsize=".sl_quote_smart($dataarray[9]).",";
    $query.="inputtextstyle=".sl_quote_smart($dataarray[10]).",";
    $query.="inputbackcolor=".sl_quote_smart(substr($dataarray[11],1)).",";
    $query.="bordersize=".sl_quote_smart($dataarray[12]).",";
    $query.="bordercolor=".sl_quote_smart(substr($dataarray[13],1)).",";
    $query.="borderradius=".sl_quote_smart($dataarray[14]).",";
    $query.="usernamelabel=".sl_quote_smart($dataarray[15]).",";
    $query.="usernameplaceholder=".sl_quote_smart($dataarray[16]).",";
    $query.="passwordlabel=".sl_quote_smart($dataarray[17]).",";
    $query.="passwordplaceholder=".sl_quote_smart($dataarray[18]).",";
    $query.="includecaptcha=".sl_quote_smart($dataarray[19]).",";
    $query.="captchalabel=".sl_quote_smart($dataarray[20]).",";
    $query.="captchaplaceholder=".sl_quote_smart($dataarray[21]).",";
    $query.="includeremember=".sl_quote_smart($dataarray[22]).",";
    $query.="rememberlabel=".sl_quote_smart($dataarray[23]).",";
    $query.="includeautologin=".sl_quote_smart($dataarray[24]).",";
    $query.="autologinlabel=".sl_quote_smart($dataarray[25]).",";
    $query.="logintext=".sl_quote_smart($dataarray[26]).",";
    $query.="logintextcolor=".sl_quote_smart(substr($dataarray[27],1)).",";
    $query.="logintextsize=".sl_quote_smart($dataarray[28]).",";
    $query.="loginfilltype=".sl_quote_smart($dataarray[29]).",";
    $query.="logincolorfrom=".sl_quote_smart(substr($dataarray[30],1)).",";
    $query.="logincolorto=".sl_quote_smart(substr($dataarray[31],1)).",";
    $query.="loginshape=".sl_quote_smart($dataarray[32]).",";
    $query.="includeforgot=".sl_quote_smart($dataarray[33]).",";
    $query.="forgottext=".sl_quote_smart($dataarray[34]).",";
    $query.="forgotcolor=".sl_quote_smart(substr($dataarray[35],1)).",";
    $query.="forgotsize=".sl_quote_smart($dataarray[36]).",";
    $query.="forgotstyle=".sl_quote_smart($dataarray[37]).",";
    $query.="includesignup=".sl_quote_smart($dataarray[38]).",";
    $query.="signuptext=".sl_quote_smart($dataarray[39]).",";
    $query.="signupurl=".sl_quote_smart($dataarray[40]).",";
    $query.="signupcolor=".sl_quote_smart(substr($dataarray[41],1)).",";
    $query.="signupsize=".sl_quote_smart($dataarray[42]).",";
    $query.="signupstyle=".sl_quote_smart($dataarray[43]).",";
    $query.="bottommargin=".sl_quote_smart($dataarray[44]).",";
    $query.="maxformwidth=".sl_quote_smart($dataarray[45]).",";
    $query.="backgroundcolor=".sl_quote_smart(substr($dataarray[46],1)).",";
    $query.="logintextfont=".sl_quote_smart($dataarray[47]).",";
    $query.="logintextstyle=".sl_quote_smart($dataarray[48]).",";
    $query.="btnbordercolor=".sl_quote_smart(substr($dataarray[49],1)).",";
    $query.="btnbordersize=".sl_quote_smart($dataarray[50]).",";
    $query.="btnborderstyle=".sl_quote_smart($dataarray[51]).",";
    $query.="inputpaddingv=".sl_quote_smart($dataarray[52]).",";
    $query.="inputpaddingh=".sl_quote_smart($dataarray[53]).",";
    $query.="btnpaddingv=".sl_quote_smart($dataarray[54]).",";
    $query.="btnpaddingh=".sl_quote_smart($dataarray[55]);

    if (($act=="addform") || ($act=="duplform"))
    {
      $query = "INSERT INTO ".$query;
      $formid=mysqli_insert_id($mysql_link);
    }  
    else
    {
      $query = "UPDATE ".$query." WHERE id=".sl_quote_smart($actid);;        
      $formid=$actid;
    } 
    $mysql_result = mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError($data,$errors,ADMINMSG_MYSQLERROR);
      exit;
    }
  }
  returnSuccess($data,ADMINET_FRMSAVED,$formid);
  exit;     

  function returnSuccess($data,$msg,$formid)
  {
    $data['success'] = true;
    $data['message'] = $msg;
    $data['formid'] = $formid;    
    echo json_encode($data);
    exit;
  }

  function returnError($data,$errors,$msg)
  {
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }


  ?>