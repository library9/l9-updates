// Enable select boxes
$('.selectpicker').selectpicker({
  style: 'btn-default',
  size: 'auto',
  dropupAuto: false
});

updateForm();

$("#newloginaction").change(function(){
  updateForm();
});

$('#newgroupname').focus();

// Validation and saving for add user form
$('#usergroupform').submit(function(event) {
        // process the form
        // remove the error classes
        $('#newgroupnamediv').removeClass('has-error');
        $('#newdescriptiondiv').removeClass('has-error');
        $('#newloginvaluediv').removeClass('has-error');
         // remove the error messages
        $('#newgroupnamehelp').remove();
        $('#newdescriptionhelp').remove();
        $('#newloginvaluehelp').remove();
        //Remove form message
        $('#resultusergroup').html('');

        var data={success:true,message:'',errors:{}};
        // Validate using JS first
        // Usergroup name
        var newgroupname=$( "#newgroupname" ).val().trim();
        if (newgroupname=="")
          data.errors.newgroupname=ADMINMSG_ENTGROUP;
        if ((newgroupname!="") && (!validChars("#{}()@.0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",newgroupname)))
          data.errors.user=ADMINMSG_GROUPINVALID;
        // Login action and value
        var newloginaction=$( "#newloginaction" ).val().trim();
        var newloginvalue=$( "#newloginvalue" ).val().trim();
        if ((newloginaction=="URL") && (newloginvalue==""))
          data.errors.newloginvalue=ADMINMSG_ENTURL;          
        if (Object.keys(data.errors).length>0)
        {
          data.success=false;
          data.message=ADMINMSG_FORMERROR;
          //data=JSON.stringify(data);
          // Show validation results from JS in form
          showValidation(data,'resultusergroup');
          // stop the form from submitting the normal way and refreshing the page
          event.preventDefault();
          return;
        }
       
        // Now call PHP for final validation and processing
        var label=showButtonBusy('submit');
        var formData = $('#usergroupform').serialize();
        //var formData = {
        //    'text1'              : $('#text1').val(),
        //    'hpos1'             : $('#hpos1').val(),
        //};

        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'adminsaveusergroup.php', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
            // using the done promise callback
            .done(function(data) {
              sessionExpiredReported(data,true);
              // Show validation results from PHP in form
              showValidation(data,'resultusergroup');
              hideButtonBusy('submit',label);
              if (data.success)
              {
                var act=$( "#act" ).val();
                if ((act=="addgroup") || (act=="duplgroup"))
                {
                  // If usergroup was added using addgroup or duplgroup clear fields ready for next entry
                  $( "#newgroupname" ).val("");
                  $( "#newdescription" ).val("");
                  $( "#newloginvalue" ).val("");
                  $( "#newgroupname" ).focus();
                }
                if (act=="editgroup")
                {
                  // If usergroup edited then update oldgroupname
                  $( "#oldgroupname" ).val($( "#newgroupname" ).val());                  
                }                
              }
            })

// using the fail promise callback
    .fail(function(data) {
    hideButtonBusy('submit',label);  
    $('#resultusergroup').html('<div id="resultusergroupmessage" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
//        console.log(data);
    });


        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });


function updateForm()
{
  var val=$('#newloginaction').val();
  if (val=="URL")
  {
    $('#newloginvaluegroup').show();
    $('#newloginvalue').focus();
  }
  else
    $('#newloginvaluegroup').hide();  
}