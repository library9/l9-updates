    <?php
    if (!isset($adminskin))
      $adminskin="blue";
    // See which page is being displayed
    if (!isset($pagename))
        $pagename="";  
    ?> 
    <meta charset="<?php echo $MetaCharSet; ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script type="text/javascript">
    var SitelokLocationURLPath='<?php echo $SitelokLocationURLPath; ?>';
    </script>
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo $SitelokLocationURLPath; ?>bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $SitelokLocationURLPath; ?>font-awesome/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo $SitelokLocationURLPath; ?>adminlte/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo $SitelokLocationURLPath; ?>adminlte/plugins/datatables/extensions/Scroller/css/scroller.dataTables.css">
    <link rel="stylesheet" href="<?php echo $SitelokLocationURLPath; ?>adminlte/plugins/datatables/extensions/Scroller/css/scroller.bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $SitelokLocationURLPath; ?>adminlte/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue however, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="<?php echo $SitelokLocationURLPath; ?>adminlte/css/skins/skin-<?php echo $adminskin; ?>.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->    
    <link href="<?php echo $SitelokLocationURLPath; ?>editable-select/jquery.editable-select.min.css" rel="stylesheet">
    <link href="<?php echo $SitelokLocationURLPath; ?>bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo $SitelokLocationURLPath; ?>adminlte/plugins/iCheck/square/blue.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $SitelokLocationURLPath; ?>bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="<?php echo $SitelokLocationURLPath; ?>admincommon.css" />
    <script type="text/javascript">
    var slcsrf='<?php echo $slcsrftoken; ?>';
    <?php
    // Get PHP defines for use by javascript too
    $constant=get_defined_constants(true);
    foreach ($constant['user'] as $key => $value)
    {
      $value=str_replace("'", "\'",$value);  
      echo "var $key='$value';\n";
    }
    ?>
    // Get local timezone offset for admin user
    var dtz = new Date()
    var adminLocalTzOffset=dtz.getTimezoneOffset();
    // Set required JS variables from PHP
    var UsernameField='<?php echo $UsernameField; ?>';
    var ValidUsernameChars="<?php echo $ValidUsernameChars; ?>";
    var ValidPasswordChars="<?php echo $ValidPasswordChars; ?>";
    var RandomPasswordMask="<?php echo $RandomPasswordMask; ?>";
    var PasswordsHashed=<?php if ($MD5passwords) echo 'true'; else echo 'false'; ?>;
    var DateFormat='<?php echo $DateFormat; ?>';
    var SitelokLocation='<?php echo $SitelokLocation; ?>';
    var SitelokLocationURL='<?php echo $SitelokLocationURL; ?>';
    var SitelokLocationURLPath='<?php echo $SitelokLocationURLPath; ?>';
    var EmailLocation='<?php echo $EmailLocation; ?>';    
    var EmailURL='<?php echo $EmailURL; ?>';  
    var TuringLogin='<?php echo $TuringLogin; ?>';  
    var TuringRegister='<?php echo $TuringRegister; ?>';  
    var CookieLogin='<?php echo $CookieLogin; ?>'; 
    var ProfilePassRequired='<?php echo $ProfilePassRequired; ?>';
    var SitelokVersion='<?php echo $SitelokVersion; ?>';
    </script>

<?php
if(isset($slplugin_adminhead))
{
  asort($slplugin_adminhead);
  foreach ($slplugin_adminhead as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadminhead.php");
  } 
}
?>
