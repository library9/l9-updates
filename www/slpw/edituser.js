// Enable select boxes
$('.selectpicker').selectpicker({
  style: 'btn-default',
  size: 4,
  dropupAuto: false
});

// Setup email template checkbox
$('#sendemail').on('ifChecked', function(event){
               $('#templatediv').show();
});
$('#sendemail').on('ifUnchecked', function(event){
               $('#templatediv').hide();
});

// Setup lightbox on view image icons
$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox({'loadingMessage': 'Loading...', 'always_show_close': false});
});

$("#canceledituser").click( function()
           {
             window.location.href='index.php';
           }
        );

// Validation and saving for edit user form
$('#edituserform').submit(function(event) {
        // process the form
        // remove the error classes
        $('#userdiv').removeClass('has-error');
        $('#pssdiv').removeClass('has-error');
        $('#nmdiv').removeClass('has-error');
        $('#emdiv').removeClass('has-error');
        for (k=1;k<groupcount;k++)
        {
          if ( !$( "#group"+k ).length )
            continue;
          $('#group'+k+'div').removeClass('has-error');
          $('#expiry'+k+'div').removeClass('has-error');
        }
        for (k=1;k<=50;k++)
          $('#cu'+k+'div').removeClass('has-error');
        $('#sendemaildiv').removeClass('has-error');
         // remove the error messages
        $('#userhelp').remove();
        $('#psshelp').remove();
        $('#nmhelp').remove();
        $('#emhelp').remove();
        for (k=1;k<groupcount;k++)
        {
          if ( !$( "#group"+k ).length )
            continue;
          $('#group'+k+'help').remove();
          $('#expiry'+k+'help').remove();
        }
        for (k=1;k<=50;k++)
          $('#cu'+k+'help').remove();        
        $('#sendemailhelp').remove();
        //Remove form message
        $('#resultedituser').html('');
        // Validate using JS first
        // Username
        var data={success:true,message:'',errors:{}};
        var user=$( "#user" ).val().trim();
        if (user=="")
          data.errors.user=ADMINMSG_ENTERUSER;
        if ((user!="") && (!validChars(ValidUsernameChars,user)))
          data.errors.user=ADMINMSG_USERINVALID;
        // Password
        var pss=$( "#pss" ).val().trim();
        if ((!PasswordsHashed) && (pss==""))
          data.errors.pss=ADMINMSG_ENTERPASS;
        if ((pss!="") && (pss.length<5))
          data.errors.pss=ADMINMSG_PASSSHORT;
        if ((pss!="") && (!validChars(ValidPasswordChars,pss)))
          data.errors.pss=ADMINMSG_PASSINVALID;
        // Name
        if ($( "#nm" ).val().trim()=="")
          data.errors.nm=ADMINMSG_ENTERNAME;
        // Email
        var em=$( "#em" ).val().trim();
        if (em=="")
          data.errors.em=ADMINMSG_ENTEREMAIL;
        if ((em!="") && (!validateEmail(em)))
          data.errors.em=ADMINMSG_EMAILINVALID;
        for (k=1;k<groupcount;k++)
        {
          // Ignore if field not present
          if ( !$( "#group"+k ).length )
            continue;
          var gstr=$( "#group"+k ).val();
          if (!validUsergroup(gstr,false))
            eval("data.errors.group"+k+"=ADMINMSG_GROUPINVALID");
          estr=$( "#expiry"+k ).val();
          if ((estr!="") && (!validChars("0123456789",estr)))
            eval("data.errors.expiry"+k+"=ADMINMSG_EXPIRYINVALID");
          if (estr.length==6)
          {
            if (!dateValid(estr,DateFormat))
              eval("data.errors.expiry"+k+"=ADMINMSG_EXPIRYDATEINVALID");
          }
          if ((gstr=="") && (estr!=""))
            eval("data.errors.group"+k+"=ADMINMSG_GROUPINVALID");
        }
        // If sending email check template is valid type
        var template=$( "#template" ).val().trim();
        if ($('#sendemail').iCheck('update')[0].checked)
        {  
          var fnext = template.substr((template.lastIndexOf('.') + 1));
          fnext=fnext.toLowerCase();
          if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
            data.errors.sendemail=ADMINMSG_TEMPLATETYPE;
        }               
        if (Object.keys(data.errors).length>0)
        {
          data.success=false;
          data.message=ADMINMSG_FORMERROR;
          //data=JSON.stringify(data);
          // Show validation results from JS in form
          showValidation(data,'resultedituser');
          // stop the form from submitting the normal way and refreshing the page
          event.preventDefault();
          return;
        }
       
        // Now call PHP for final validation and processing
        var label=showButtonBusy('submitedituser');
        var formData = $('#edituserform').serialize();
        formData=formData+"&groupcount="+(groupcount-1);
        //var formData = {
        //    'text1'              : $('#text1').val(),
        //    'hpos1'             : $('#hpos1').val(),
        //};

        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'adminedituser.php', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
            // using the done promise callback
            .done(function(data) {
              sessionExpiredReported(data,true);
              if (!data.success)
              {
                // Show validation results from PHP in form
                showValidation(data,'resultedituser');
                hideButtonBusy('submitedituser',label);
              }
              else
              {
                hideButtonBusy('submitedituser',label);
                window.location.href='index.php';               
              }
            })

// using the fail promise callback
    .fail(function(data) {
    hideButtonBusy('submitedituser',label);  
    $('#resultedituser').html('<div id="resulteditusermessage" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
//        console.log(data);
    });


        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

//$('#user').focus();

function openFileManager()
{
    $.fancybox({
        'type'  : 'iframe',
        'width' : '90%',
        'height' : '90%',
        'href' : 'filemanager/dialog.php?type=2&field_id=template&lang='+ADMINRFM_LANG+'&sort_by=name&descending=0'
      });
}

function responsive_filemanager_callback(field_id)
{
  if (-1!=EmailURL.indexOf('://www.'))
  {
    var emailurl1=EmailURL.replace('://www.','://');
    var emailurl2=EmailURL;
  }
  else
  {
    var emailurl1=EmailURL;
    var emailurl2=EmailURL.replace('://','://www.');
  }
  var emailurl3=EmailURL;
  emailurl3=EmailURL.replace('http://','https://');
  var emailurl4=EmailURL;
  emailurl4=EmailURL.replace('https://','http://');
  var url=$('#'+field_id).val();
  url=url.replace(emailurl1,'');
  url=url.replace(emailurl2,'');
  url=url.replace(emailurl3,'');
  url=url.replace(emailurl4,'');
  url=decodeURIComponent(url);
  $('#'+field_id).val(url);
  $('#templatename').text(url);
}

function addGroup()
{
  var code=$('#extraGroupTemplate').html();
  code=code.replace(/XX/g, groupcount);
  code=code.replace(/<!--/g, '');
  code=code.replace(/-->/g, '');
  $('#extraGroupInsert').before(code);
  $('#group'+groupcount).editableSelect();
  // Enable calendar
  $('#dateexpiry'+groupcount).datetimepicker({'keepInvalid': true, 'format':DateFormat,'defaultDate':'', 'useStrict': true});
  groupcount++;
}

function removeGroup(num)
{
  $('#groupblock'+num).remove();
}

function groupUp(num)
{
  if (num==1)
    return;
  var prev=0;
  for (var k=(num-1);k>=1;k--)
  {
    if ($("#groupblock"+k).length)
    {
      prev=k;
      break;
    }
  }
  if (prev==0)
    return;
  var tmp=$("#group"+prev).val()
  $("#group"+prev).val($("#group"+num).val())
  $("#group"+num).val(tmp)
  var tmp=$("#expiry"+prev).val()
  $("#expiry"+prev).val($("#expiry"+num).val())
  $("#expiry"+num).val(tmp)
}

function groupDown(num)
{
  if (num==(groupcount-1))
    return;
  var next=0;
  for (var k=(num+1);k<groupcount;k++)
  {
    if ($("#groupblock"+k).length)
    {
      next=k;
      break;
    }
  }
  if (next==0)
    return;
  var tmp=$("#group"+next).val()
  $("#group"+next).val($("#group"+num).val())
  $("#group"+num).val(tmp)
  var tmp=$("#expiry"+next).val()
  $("#expiry"+next).val($("#expiry"+num).val())
  $("#expiry"+num).val(tmp)
}

function randomPassword()
{
  var mask=RandomPasswordMask;
  if (mask=="")
    mask="cccc##"
  var password="";   
  for (k=0;k<mask.length;k++)  
  {
    if (mask.charAt(k)=="c")
      password=password+"abcdefghijklmnopqrstuvwxyz".charAt(Math.round(25*Math.random()));
    if (mask.charAt(k)=="C")
      password=password+"ABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(Math.round(25*Math.random()));
    if (mask.charAt(k)=="#")
      password=password+"0123456789".charAt(Math.round(9*Math.random()));
    if (mask.charAt(k)=="X")
      password=password+"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(Math.round(51*Math.random()));
    if (mask.charAt(k)=="A")
      password=password+"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".charAt(Math.round(61*Math.random()));
    if (mask.charAt(k)=="U")
      password=password+ValidPasswordChars.charAt(Math.round((ValidPasswordChars.length-1)*Math.random()));
  }
  return(password); 
}
