<?php
$groupswithaccess="ADMIN,SUBADMIN";
$noaccesspage="";
$adminajaxcall=true;
require("sitelokpw.php");
if (!isset($adminlanguage))
  $adminlanguage="en";
require("adminlanguage-".$adminlanguage.".php");  
$slsubadmin=false;
if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
  $slsubadmin=true;
// Check CSRF value
if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
{
  ?>
  {
  "success": false,
  "message": "<?php echo ADMINMSG_CSRFFAILED; ?>"
  }
  <?php
  exit;      
}
$mysql_link=sl_DBconnect();
if ($mysql_link==false)
{
  ?>
  {
  "success": false,
  "message": "<?php echo ADMINMSG_MYSQLERROR; ?>"
  }
  <?php
  exit;      
}
$userid=$_POST['userid'];
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." WHERE ".$IdField."=".sl_quote_smart($userid));
$emres=0;
if ($mysql_result!=false)
{
  $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
  $selected=$row[$SelectedField];
  $created=$row[$CreatedField];
  $user=$row[$UsernameField];
  $pass=$row[$PasswordField];
  $enabled=$row[$EnabledField];
  $name=$row[$NameField];
  $email=$row[$EmailField];
  $groups=$row[$UsergroupsField];
  $cus1=$row[$Custom1Field];
  $cus2=$row[$Custom2Field];
  $cus3=$row[$Custom3Field];
  $cus4=$row[$Custom4Field];
  $cus5=$row[$Custom5Field];
  $cus6=$row[$Custom6Field];
  $cus7=$row[$Custom7Field];
  $cus8=$row[$Custom8Field];
  $cus9=$row[$Custom9Field];
  $cus10=$row[$Custom10Field];
  $cus11=$row[$Custom11Field];
  $cus12=$row[$Custom12Field];
  $cus13=$row[$Custom13Field];
  $cus14=$row[$Custom14Field];
  $cus15=$row[$Custom15Field];
  $cus16=$row[$Custom16Field];
  $cus17=$row[$Custom17Field];
  $cus18=$row[$Custom18Field];
  $cus19=$row[$Custom19Field];
  $cus20=$row[$Custom20Field];
  $cus21=$row[$Custom21Field];
  $cus22=$row[$Custom22Field];
  $cus23=$row[$Custom23Field];
  $cus24=$row[$Custom24Field];
  $cus25=$row[$Custom25Field];
  $cus26=$row[$Custom26Field];
  $cus27=$row[$Custom27Field];
  $cus28=$row[$Custom28Field];
  $cus29=$row[$Custom29Field];
  $cus30=$row[$Custom30Field];
  $cus31=$row[$Custom31Field];
  $cus32=$row[$Custom32Field];
  $cus33=$row[$Custom33Field];
  $cus34=$row[$Custom34Field];
  $cus35=$row[$Custom35Field];
  $cus36=$row[$Custom36Field];
  $cus37=$row[$Custom37Field];
  $cus38=$row[$Custom38Field];
  $cus39=$row[$Custom39Field];
  $cus40=$row[$Custom40Field];
  $cus41=$row[$Custom41Field];
  $cus42=$row[$Custom42Field];
  $cus43=$row[$Custom43Field];
  $cus44=$row[$Custom44Field];
  $cus45=$row[$Custom45Field];
  $cus46=$row[$Custom46Field];
  $cus47=$row[$Custom47Field];
  $cus48=$row[$Custom48Field];
  $cus49=$row[$Custom49Field];
  $cus50=$row[$Custom50Field];
  if (!sl_ReadEmailTemplate($ForgottenEmail,$subject,$mailBody,$htmlformat))
  {
    $subject=$SiteName." login details";
    $htmlformat="Y";
    if ($MD5passwords!=true)
    {
      $mailBody="<html>
<head>
<title>Login details from !!!sitename!!!</title>
</head>
<body>Hi,<br>
<br>
Here are the login details that you requested.<br>
<br>
Username: !!!username!!!<br>
Password: !!!password!!!<br>
<br>
If you have any further questions or problems please email us at <a href='mailto:!!!siteemail!!!'>!!!siteemail!!!</a>.<br>
</body>
</html>";          
  }
  else
  {
    $mailBody="<html>
<head>
<title>!!!sitename!!! login details</title>
</head>
<body>Hi,<br>
<br>
Here are your login details for !!!sitename!!!. To activate your new password please click the link below.<br>
<br>
Username: !!!username!!!<br>
Passphrase: !!!newpassword!!!<br>
<br>
<a href='!!!activatepassword!!!'>Activate Now</a><br>
<br>
If you have any further questions or problems please email us at <a href='mailto:!!!siteemail!!!'>!!!siteemail!!!</a>.
</body>
</html>";
    }
  }
  $emres=sl_SendEmail($email,$mailBody,$subject,$htmlformat,$user,$pass,$name,$email,$groups,$cus1,$cus2,$cus3,$cus4,$cus5,$cus6,$cus7,$cus8,$cus9,$cus10,
  $cus11,$cus12,$cus13,$cus14,$cus15,$cus16,$cus17,$cus18,$cus19,$cus20,$cus21,$cus22,$cus23,$cus24,$cus25,$cus26,$cus27,$cus28,$cus29,$cus30,
  $cus31,$cus32,$cus33,$cus34,$cus35,$cus36,$cus37,$cus38,$cus39,$cus40,$cus41,$cus42,$cus43,$cus44,$cus45,$cus46,$cus47,$cus48,$cus49,$cus50);
}
if ($emres==0)
{
  ?>
  {
  "success": false,
  "message": "<?php echo ADMINMSG_SENDEMAILISSUE; ?>"
  }
  <?php
  exit;            
}
if ($emres==1)
{
  ?>
  {
  "success": true,
  "message": ""
  }
  <?php
  exit;            
}
if ($emres==2)
{
  ?>
  {
  "success": false,
  "message": "<?php echo ADMINMSG_EMAILBLOCKEDPLUG; ?>"
  }
  <?php
  exit;            
}  
