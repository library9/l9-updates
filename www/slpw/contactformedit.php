<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  if (!defined('ADMINET_INPPADDING'))
    define("ADMINET_INPPADDING","Input Padding (Vert Horz)");
  if (!defined('ADMINET_BTNPADDING'))
    define("ADMINET_BTNPADDING","Button Padding (Vert Horz)");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  include("admincommonfunctions.php");  
  // Get current values
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    die(ADMINMSG_MYSQLERROR);
  }
  if (isset($_GET['actid']))
    $actid=$_GET['actid'];
  if (isset($_GET['act']))
    $act=$_GET['act'];  
?>
<!DOCTYPE html>

<html>
<head>
    <?php
    $pagename="contactformedit";
    include("adminhead.php");
    ?>
    <title><?php echo ADMINMENU_CONTACTFORMDESIGN; ?></title>
    <link rel="stylesheet" href="jquery-minicolors-master/jquery.minicolors.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />
    <link rel="stylesheet" href="contactformedit.css" type="text/css">


 <!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
    <!--[if lte IE 6]>
  <style type="text/css">
    div#loginbox {
    width: 420px;
    }
<![endif]-->
<script type="text/javascript">
// Run this when the page first loads
var gradientPrefix = getCssValuePrefix('background','linear-gradient(to bottom, #79bcff 5%, #378ee5 100%)');
var radiusPrefix = getCssValuePrefix('border-radius','10px');
currenteditfield=-1;
// Setup global form field arrays
var sitelokfield_array = new Array();
var inputtype_array = new Array();
var labeltext_array = new Array();
var placetext_array = new Array();
var value_array = new Array();
var checked_array = new Array();
var validation_array = new Array();
var showrequired_array = new Array();
var errormsg_array = new Array();
var useas_array = new Array();
var showfieldfor_array = new Array();
var fieldwidth_array = new Array();
var bottommargin_array = new Array();

var showformerrormessage=false;
var numberoffields;

// Detect which browser prefix to use for the specified CSS value
// (e.g., background-image: -moz-linear-gradient(...);
//        background-image:   -o-linear-gradient(...); etc).
//
// http://stackoverflow.com/questions/15071062/using-javascript-to-edit-css-gradient
function getCssValuePrefix(name, value) {
    var prefixes = ['', '-o-', '-ms-', '-moz-', '-webkit-'];

    // Create a temporary DOM object for testing
    var dom = document.createElement('div');

    for (var i = 0; i < prefixes.length; i++) {
        // Attempt to set the style
        dom.style[name] = prefixes[i] + value;

        // Detect if the style was successfully set
        if (dom.style[name]) {
            return prefixes[i];
        }
        dom.style[name] = '';   // Reset the style
    }
}

function startvalues()
{
  <?php if ($act=="addform") { ?>
  // Set form style variables
  document.getElementById('newformname').value='Untitled'
  document.getElementById('newsendemail').value='<?php echo $SiteEmail; ?>'
  document.getElementById('newredirect').value='contactthanks.php'

  $('#newuseremailvisitor').val('');
  $('#senduseremailvisitordiv').hide();
  $('#newuseremailvisitorname').text(ADMINMSG_NOTEMPLATE);
  $('#senduseremailvisitor').iCheck('uncheck');

  $('#newuseremailmember').val('');
  $('#senduseremailmemberdiv').hide();
  $('#newuseremailmembername').text(ADMINMSG_NOTEMPLATE);
  $('#senduseremailmember').iCheck('uncheck');

  $('#newadminemailvisitor').val('');
  $('#sendadminemailvisitordiv').hide();
  $('#newadminemailvisitorname').text(ADMINMSG_NOTEMPLATE);
  $('#sendadminemailvisitor').iCheck('uncheck');

  $('#newadminemailmember').val('');
  $('#sendadminemailmemberdiv').hide();
  $('#newadminemailmembername').text(ADMINMSG_NOTEMPLATE);
  $('#sendadminemailmember').iCheck('uncheck');

  document.getElementById('newattachmenttypes').value='.jpg .gif .png .zip .txt'
  document.getElementById('newattachmentsize').value='2000000'
  document.getElementById('newfromnameuser').value='<?php echo $SiteName; ?>'
  document.getElementById('newreplytouser').value='<?php echo $SiteEmail; ?>'
  document.getElementById('newsendasuser').value='0'

  $('#newfonttype').selectpicker('val', 'Arial, Helvetica, sans-serif');    
  $('#newlabelcolor').minicolors('value', '#1A305E');
  document.getElementById('newlabelsize').value='18';
  $('#newlabelstyle').selectpicker('val', 'normal normal bold');    
  $('#newinputtextcolor').minicolors('value', '#1A305E');
  document.getElementById('newinputtextsize').value='16';
  $('#newinputtextstyle').selectpicker('val', 'normal normal normal');    
  $('#newinputbackcolor').minicolors('value', '#FFFFFF');  
  document.getElementById('newbordersize').value='1';
  $('#newbordersize').selectpicker('val', '1');    
  $('#newbordercolor').minicolors('value', '#378EE5');  
  $('#newborderradius').selectpicker('val', '0');    
  $('#newinputpaddingv').selectpicker('val', '0.3em');    
  $('#newinputpaddingh').selectpicker('val', '0.3em');    
  document.getElementById('newrqdfieldlabel').value='*';
  $('#newrqdfieldcolor').minicolors('value', '#FF0000');  
  document.getElementById('newrqdfieldsize').value='12';
  $('#newrqdfieldstyle').selectpicker('val', 'normal normal normal');      
  $('#newmessagecolor').minicolors('value', '#FF0000');  
  document.getElementById('newmessagesize').value='12';
  $('#newmessagestyle').selectpicker('val', 'normal normal normal');    
  document.getElementById('newbtnlabel').value='SEND';
  $('#newbtnlabelfont').selectpicker('val', 'Arial, Helvetica, sans-serif');    
  $('#newbtnlabelstyle').selectpicker('val', 'normal normal bold');    
  $('#newbtnlabelcolor').minicolors('value', '#FFFFFF');  
  document.getElementById('newbtnlabelsize').value='14';
  $('#newbtncolortype').selectpicker('val', 'gradient');    
  $('#newbtncolorfrom').minicolors('value', '#1A305E');    
  $('#newbtncolorto').minicolors('value', '#378EE5');    
  $('#newbtnborderstyle').selectpicker('val', 'solid');    
  $('#newbtnbordercolor').minicolors('value', '#FFFFFF');    
  $('#newbtnpaddingv').selectpicker('val', '6');    
  $('#newbtnpaddingh').selectpicker('val', '24');        
  document.getElementById('newbtnbordersize').value='0';
  $('#newbtnradius').selectpicker('val', '10');    
  document.getElementById('newformerrormsg').value='Please correct the errors below';
  $('#newformerrormsgcolor').minicolors('value', '#FF0000');    
  document.getElementById('newformerrormsgsize').value='14';
  $('#newformerrormsgstyle').selectpicker('val', 'normal normal bold');    
  $('#newpreviewbackcolor').minicolors('value', '#FFFFFF');    
  document.getElementById('newmaxformwidth').value='300';

  // Set field properties
  numberoffields=4
  
  sitelokfield_array[0]='fullname'
  inputtype_array[0]='text'
  labeltext_array[0]='Full name'
  placetext_array[0]='full name'
  value_array[0]=''
  checked_array[0]='0'
  validation_array[0]='required'
  showrequired_array[0]='1'
  errormsg_array[0]='Please enter your full name'
  useas_array[0]='2'
  showfieldfor_array[0]='0'
  fieldwidth_array[0]='100'
  bottommargin_array[0]='20'
  
  inputtype_array[1]='text'
  sitelokfield_array[1]='email'
  labeltext_array[1]='Email'
  placetext_array[1]='email'
  value_array[1]=''
  checked_array[1]='0'
  validation_array[1]='requiredemail'
  showrequired_array[1]='1'
  errormsg_array[1]='Please enter your email'
  useas_array[1]='1'
  showfieldfor_array[1]='0'
  fieldwidth_array[1]='100'
  bottommargin_array[1]='20'

  inputtype_array[2]='textarea'
  sitelokfield_array[2]=''
  labeltext_array[2]='Message'
  placetext_array[2]='message'
  value_array[2]=''
  checked_array[2]='0'
  validation_array[2]='required'
  showrequired_array[2]='1'
  errormsg_array[2]='Please enter your message'
  useas_array[2]='0'
  showfieldfor_array[2]='0'
  fieldwidth_array[2]='100'
  bottommargin_array[2]='20'
  
  inputtype_array[3]='captcha'
  sitelokfield_array[3]=''
  labeltext_array[3]='Captcha'
  placetext_array[3]='captcha'
  value_array[3]=''
  checked_array[3]='0'
  validation_array[3]='required'
  showrequired_array[3]='1'
  errormsg_array[3]='Please enter the captcha code'
  useas_array[3]='0'
  showfieldfor_array[3]='1'
  fieldwidth_array[3]='100'
  bottommargin_array[3]='20'
  <?php } ?>
  
  <?php if (($act=="editform") || ($act=="duplform")){
  $mysql_result=mysqli_query($mysql_link,"SELECT name FROM sl_forms WHERE id=".sl_quote_smart($actid));
  if ($mysql_result===false)
  {
    print("Can't read form template");
    exit;      
  } 
  $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
  $formname=$row['name'];
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_contactforms WHERE id=".sl_quote_smart($actid)." AND position=0");
  if ($mysql_result===false)
  {
    print("Can't read form template");
    exit;      
  }
  $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
  ?>
  // Set form style variables
  document.getElementById('newformname').value='<?php if ($act=="duplform") echo $formname." copy"; else echo $formname; ?>'
  document.getElementById('newsendemail').value='<?php echo str_replace("\n","\\n",$row['sendemail']); ?>'
  document.getElementById('newredirect').value='<?php echo $row['redirect']; ?>'
  document.getElementById('newuseremailvisitor').value='<?php echo $row['useremailvisitor']; ?>'
  document.getElementById('newadminemailvisitor').value='<?php echo $row['adminemailvisitor']; ?>'
  document.getElementById('newuseremailmember').value='<?php echo $row['useremailmember']; ?>'
  document.getElementById('newadminemailmember').value='<?php echo $row['adminemailmember']; ?>'
  document.getElementById('newattachmenttypes').value='<?php echo $row['attachmenttypes']; ?>'
  document.getElementById('newattachmentsize').value='<?php echo $row['attachmentsize']; ?>'
  document.getElementById('newfromnameuser').value='<?php echo $row['fromnameuser']; ?>'
  document.getElementById('newreplytouser').value='<?php echo $row['replytouser']; ?>'
  document.getElementById('newsendasuser').value='<?php echo $row['sendasuser']; ?>'
  <?php if ($row['useremailvisitor']=="") { ?>
  $('#newuseremailvisitor').val('');
  $('#senduseremailvisitordiv').hide();
  $('#newuseremailvisitorname').text(ADMINMSG_NOTEMPLATE);
  $('#senduseremailvisitor').iCheck('uncheck');
  <?php } else { ?>
  $('#newuseremailvisitor').val('<?php echo $row['useremailvisitor']; ?>');
  $('#senduseremailvisitordiv').show();
  $('#newuseremailvisitorname').text('<?php echo $row['useremailvisitor']; ?>');
  $('#senduseremailvisitor').iCheck('check');
  <?php } ?>
  <?php if ($row['adminemailvisitor']=="") { ?>
  $('#newadminemailvisitor').val('');
  $('#sendadminemailvisitordiv').hide();
  $('#newadminemailvisitorname').text(ADMINMSG_NOTEMPLATE);
  $('#sendadminemailvisitor').iCheck('uncheck');
  <?php } else { ?>
  $('#newadminemailvisitor').val('<?php echo $row['adminemailvisitor']; ?>');
  $('#sendadminemailvisitordiv').show();
  $('#newadminemailvisitorname').text('<?php echo $row['adminemailvisitor']; ?>');
  $('#sendadminemailvisitor').iCheck('check');
  <?php } ?>
  <?php if ($row['useremailmember']=="") { ?>
  $('#newuseremailmember').val('');
  $('#senduseremailmemberdiv').hide();
  $('#newuseremailmembername').text(ADMINMSG_NOTEMPLATE);
  $('#senduseremailmember').iCheck('uncheck');
  <?php } else { ?>
  $('#newuseremailmember').val('<?php echo $row['useremailmember']; ?>');
  $('#senduseremailmemberdiv').show();
  $('#newuseremailmembername').text('<?php echo $row['useremailmember']; ?>');
  $('#senduseremailmember').iCheck('check');
  <?php } ?>
  <?php if ($row['adminemailmember']=="") { ?>
  $('#newadminemailmember').val('');
  $('#sendadminemailmemberdiv').hide();
  $('#newadminemailmembername').text(ADMINMSG_NOTEMPLATE);
  $('#sendadminemailmember').iCheck('uncheck');
  <?php } else { ?>
  $('#newadminemailmember').val('<?php echo $row['adminemailmember']; ?>');
  $('#sendadminemailmemberdiv').show();
  $('#newadminemailmembername').text('<?php echo $row['adminemailmember']; ?>');
  $('#sendadminemailmember').iCheck('check');
  <?php } ?>
  $('#newfonttype').selectpicker('val', '<?php echo $row['fonttype']; ?>');  
  $('#newlabelcolor').minicolors('value', '#<?php echo $row['labelcolor']; ?>');    
  document.getElementById('newlabelsize').value='<?php echo $row['labelsize']; ?>';
  $('#newlabelstyle').selectpicker('val', '<?php echo $row['labelstyle']; ?>');  
  $('#newinputtextcolor').minicolors('value', '#<?php echo $row['inputtextcolor']; ?>');      
  document.getElementById('newinputtextsize').value='<?php echo $row['inputtextsize']; ?>';
  $('#newinputtextstyle').selectpicker('val', '<?php echo $row['inputtextstyle']; ?>');  
  $('#newinputbackcolor').minicolors('value', '#<?php echo $row['inputbackcolor']; ?>');      
  $('#newbordersize').selectpicker('val', '<?php echo $row['bordersize']; ?>');  
  $('#newbordercolor').minicolors('value', '#<?php echo $row['bordercolor']; ?>');      
  $('#newborderradius').selectpicker('val', '<?php echo $row['borderradius']; ?>');  
  $('#newinputpaddingv').selectpicker('val', '<?php echo $row['inputpaddingv']; ?>');  
  $('#newinputpaddingh').selectpicker('val', '<?php echo $row['inputpaddingh']; ?>');  
  document.getElementById('newrqdfieldlabel').value='<?php echo $row['rqdfieldlabel']; ?>';
  $('#newrqdfieldcolor').minicolors('value', '#<?php echo $row['rqdfieldcolor']; ?>');      
  document.getElementById('newrqdfieldsize').value='<?php echo $row['rqdfieldsize']; ?>';
  $('#newrqdfieldstyle').selectpicker('val', '<?php echo $row['rqdfieldstyle']; ?>');    
  $('#newmessagecolor').minicolors('value', '#<?php echo $row['messagecolor']; ?>');      
  document.getElementById('newmessagesize').value='<?php echo $row['messagesize']; ?>';
  $('#newmessagestyle').selectpicker('val', '<?php echo $row['messagestyle']; ?>');    
  document.getElementById('newbtnlabel').value='<?php echo $row['btnlabel']; ?>';
  $('#newbtnlabelfont').selectpicker('val', '<?php echo $row['btnlabelfont']; ?>');    
  $('#newbtnlabelstyle').selectpicker('val', '<?php echo $row['btnlabelstyle']; ?>');    
  $('#newbtnlabelcolor').minicolors('value', '#<?php echo $row['btnlabelcolor']; ?>');      
  document.getElementById('newbtnlabelsize').value='<?php echo $row['btnlabelsize']; ?>';
  $('#newbtncolortype').selectpicker('val', '<?php echo $row['btncolortype']; ?>');    
  $('#newbtncolorfrom').minicolors('value', '#<?php echo $row['btncolorfrom']; ?>');      
  $('#newbtncolorto').minicolors('value', '#<?php echo $row['btncolorto']; ?>');      
  $('#newbtnborderstyle').selectpicker('val', '<?php echo $row['btnborderstyle']; ?>');    
  $('#newbtnbordercolor').minicolors('value', '#<?php echo $row['btnbordercolor']; ?>');      
  document.getElementById('newbtnbordersize').value='<?php echo $row['btnbordersize']; ?>';
  $('#newbtnpaddingv').selectpicker('val', '<?php echo $row['btnpaddingv']; ?>');  
  $('#newbtnpaddingh').selectpicker('val', '<?php echo $row['btnpaddingh']; ?>');  
  $('#newbtnradius').selectpicker('val', '<?php echo $row['btnradius']; ?>');    
  document.getElementById('newformerrormsg').value='<?php echo $row['formerrormsg']; ?>';
  $('#newformerrormsgcolor').minicolors('value', '#<?php echo $row['formerrormsgcolor']; ?>');      
  document.getElementById('newformerrormsgsize').value='<?php echo $row['formerrormsgsize']; ?>';
  $('#newformerrormsgstyle').selectpicker('val', '<?php echo $row['formerrormsgstyle']; ?>');    
  $('#newpreviewbackcolor').minicolors('value', '#<?php echo $row['backcolor']; ?>');      
  document.getElementById('newmaxformwidth').value='<?php echo $row['maxformwidth']; ?>';
  <?php
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_contactforms WHERE id=".sl_quote_smart($actid)." AND position>0 ORDER BY position ASC");
  if ($mysql_result===false)
  {
    print("Can't read form template");
    exit;      
  }
  $numfields=0;
  while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
  {
  $val=str_replace("\n","\\n",$row['value']);
  $val=str_replace("'","\'",$val);
  ?>  
  sitelokfield_array[<?php echo $numfields; ?>]='<?php echo $row['sitelokfield']; ?>'
  inputtype_array[<?php echo $numfields; ?>]='<?php echo $row['inputtype']; ?>'
  labeltext_array[<?php echo $numfields; ?>]='<?php echo str_replace("'","\'",$row['labeltext']); ?>'
  placetext_array[<?php echo $numfields; ?>]='<?php echo str_replace("'","\'",$row['placetext']); ?>'
  value_array[<?php echo $numfields; ?>]='<?php echo $val; ?>'
  checked_array[<?php echo $numfields; ?>]='<?php echo $row['checked']; ?>'
  validation_array[<?php echo $numfields; ?>]='<?php echo $row['validation']; ?>'
  showrequired_array[<?php echo $numfields; ?>]='<?php echo $row['showrequired']; ?>'
  errormsg_array[<?php echo $numfields; ?>]='<?php echo str_replace("'","\'",$row['errormsg']); ?>'
  useas_array[<?php echo $numfields; ?>]='<?php echo $row['useas']; ?>'
  showfieldfor_array[<?php echo $numfields; ?>]='<?php echo $row['showfieldfor']; ?>'
  fieldwidth_array[<?php echo $numfields; ?>]='<?php echo $row['fieldwidth']; ?>'
  bottommargin_array[<?php echo $numfields; ?>]='<?php echo $row['bottommargin']; ?>'
  <?php
  $numfields++;
  } ?>
  numberoffields=<?php echo $numfields; ?>  
<?php } ?>
  filltype('newbtncolortype','newbtncolortodiv')
  updatePreview()
}

function addfield()
{
  customtitles = new Array();
  // Get custom field titles from PHP
  <?php for ($k=1;$k<=50;$k++)
  {
    $var="CustomTitle".$k;
    if ($$var!="")
      echo "  customtitles[$k]='".str_replace("'","\s",$$var)."'\n";
    else
      echo "  customtitles[$k]='Custom $k'\n";      
  }
  ?>
  numberoffields++;
  currenteditfield=numberoffields-1;

  sitelokfield_array[currenteditfield]='';  
  inputtype_array[currenteditfield]='text'; 
  labeltext_array[currenteditfield]='Label text';
  placetext_array[currenteditfield]='placeholder';
  value_array[currenteditfield]='';
  checked_array[currenteditfield]='0';
  validation_array[currenteditfield]='notrequired';
  showrequired_array[currenteditfield]='0';
  errormsg_array[currenteditfield]='This field is required';
  useas_array[currenteditfield]='0';
  showfieldfor_array[currenteditfield]='0';
  fieldwidth_array[currenteditfield]='100';
  bottommargin_array[currenteditfield]=20;
  editfield(currenteditfield)
  updatePreview()   
}

function editfield(fieldno)
{
  currenteditfield=fieldno
  if (currenteditfield==-1)
  {
    document.getElementById('nopropertiesdiv').style['display']="block"; 
    document.getElementById('propertiesdiv').style['display']="none"; 
  }
  else
  {
    document.getElementById('nopropertiesdiv').style['display']="none"; 
    document.getElementById('propertiesdiv').style['display']="block"; 
    // Get current field details and enter in form
    setfieldproperties(fieldno);
    fieldpropertiesform(inputtype_array[fieldno])
    // Remove options from position dropdown
    var selectobj = document.getElementById('newposition');
    var k;
    for(k=selectobj.options.length-1;k>=0;k--)
      selectobj.remove(k);
    // Add new options to position drop down
    var newpos=0; 
    for (k=0;k<=numberoffields;k++)
    {
      if (k==(currenteditfield+1))
        continue
      opt = document.createElement('option');
      opt.value = newpos;
      newpos++
      if (k==0)
        opt.innerHTML = 'top'; 
      else
      {  
        opt.innerHTML = 'after '+labeltext_array[k-1];
      }
      selectobj.appendChild(opt);  
    }
    selectobj.value=fieldno
    $('#newposition').selectpicker('refresh');

    // document.getElementById('newsitelokfield').focus()
//    setTimeout(function() { document.getElementById('newsitelokfield').focus();$("#newsitelokfield").get(0).scrollIntoView(); }, 300);  // Allow panel to open before focus
  }
  $('#collapse1').collapse('hide');
  $('#collapse2').collapse('hide');
  $('#collapse3').collapse('hide');
  $('#collapse4').collapse('show');
  setTimeout(function() { document.getElementById('newinputtype').focus();$("#collapse3").get(0).scrollIntoView(); }, 300);  // Allow panel to open before focus
  updatePreview()
}

function setfieldproperties(fieldno)
{
  $('#newsitelokfield').selectpicker('val', sitelokfield_array[fieldno]);   
  $('#newinputtype').selectpicker('val', inputtype_array[fieldno]);   
  document.getElementById('newlabeltext').value=labeltext_array[fieldno]; 
  document.getElementById('newplacetext').value=placetext_array[fieldno];
  document.getElementById('newvalue').value=value_array[fieldno]; 
  document.getElementById('newvaluedropdown').value=value_array[fieldno]; 
  $('#newchecked').selectpicker('val', checked_array[fieldno]);   
  $('#newvalidation').selectpicker('val', validation_array[fieldno]);   
  $('#newvalidationdropdown').selectpicker('val', validation_array[fieldno]);   
  $('#newshowrequired').selectpicker('val', showrequired_array[fieldno]); 
  document.getElementById('newerrormsg').value=errormsg_array[fieldno]; 
  $('#newuseas').selectpicker('val', useas_array[fieldno]);   
  $('#newshowfieldfor').selectpicker('val', showfieldfor_array[fieldno]);   
  $('#newfieldwidth').selectpicker('val', fieldwidth_array[fieldno]);   
  $('#newbottommargin').selectpicker('val', bottommargin_array[fieldno]);   
}

function fieldpropertiesform()
{
  var fieldtype=inputtype_array[currenteditfield]
  if ((fieldtype=='text') || (fieldtype=="password"))
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="none"; 
    document.getElementById('valuedropdowndiv').style['display']="none";
    document.getElementById('placeholderdiv').style['display']="block";
    document.getElementById('errormsgdiv').style['display']="block";     
    document.getElementById('fieldwidthdiv').style['display']="block";     
    document.getElementById('bottommargindiv').style['display']="block";     
    document.getElementById('validationdiv').style['display']="block";     
    document.getElementById('validationdropdowndiv').style['display']="none";
    document.getElementById('checkeddiv').style['display']="none"; 
    document.getElementById('useasdiv').style['display']="block";
    document.getElementById('includefordiv').style['display']="block";
     
    if (document.getElementById('newvalidation').value=='notrequired')
    {
      document.getElementById('errormsgdiv').style['display']='none'
      document.getElementById('showrequireddiv').style['display']='none'
    }  
    else
    {
      document.getElementById('errormsgdiv').style['display']='block'
      document.getElementById('showrequireddiv').style['display']='block'
    }      
  }
  if ((fieldtype=='checkbox') || (fieldtype=="radio"))
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="block"; 
    document.getElementById('valuedropdowndiv').style['display']="none"; 
    document.getElementById('checkeddiv').style['display']="block"; 
    document.getElementById('placeholderdiv').style['display']="none";
    document.getElementById('errormsgdiv').style['display']="block";     
    document.getElementById('fieldwidthdiv').style['display']="none";     
    document.getElementById('bottommargindiv').style['display']="block";     
    document.getElementById('validationdiv').style['display']="block";     
    document.getElementById('validationdropdowndiv').style['display']="none";     
    document.getElementById('useasdiv').style['display']="none";
    document.getElementById('includefordiv').style['display']="block";
//    if (document.getElementById('newvalidation').value=='notrequired')
    if (validation_array[currenteditfield]=='notrequired')
    {
      document.getElementById('errormsgdiv').style['display']='none'
      document.getElementById('showrequireddiv').style['display']='none'
    }  
    else
    {
      document.getElementById('errormsgdiv').style['display']='block'
      document.getElementById('showrequireddiv').style['display']='block'
    }  
  }
  if (fieldtype=='dropdown')
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="none"; 
    document.getElementById('valuedropdowndiv').style['display']="block"; 
    document.getElementById('checkeddiv').style['display']="none"; 
    document.getElementById('placeholderdiv').style['display']="none";
    document.getElementById('errormsgdiv').style['display']="block";     
    document.getElementById('fieldwidthdiv').style['display']="block";     
    document.getElementById('bottommargindiv').style['display']="block";     
    document.getElementById('validationdiv').style['display']="none";     
    document.getElementById('validationdropdowndiv').style['display']="block";     
    document.getElementById('useasdiv').style['display']="none";
    document.getElementById('includefordiv').style['display']="block";
//    if (document.getElementById('newvalidationdropdown').value=='notrequired')
    if (validation_array[currenteditfield]=='notrequired')
    {
      document.getElementById('errormsgdiv').style['display']='none'
      document.getElementById('showrequireddiv').style['display']='none'
    }  
    else
    {
      document.getElementById('errormsgdiv').style['display']='block'
      document.getElementById('showrequireddiv').style['display']='block'
    }  
  }
  if (fieldtype=='label')
  {
    document.getElementById('sitelokfielddiv').style['display']="none"; 
    document.getElementById('valuediv').style['display']="none"; 
    document.getElementById('valuedropdowndiv').style['display']="none"; 
    document.getElementById('checkeddiv').style['display']="none"; 
    document.getElementById('placeholderdiv').style['display']="none";
    document.getElementById('errormsgdiv').style['display']="none";     
    document.getElementById('fieldwidthdiv').style['display']="none";     
    document.getElementById('bottommargindiv').style['display']="block";     
    document.getElementById('validationdiv').style['display']="none";     
    document.getElementById('validationdropdowndiv').style['display']="none";     
    document.getElementById('showrequireddiv').style['display']='block'
    document.getElementById('useasdiv').style['display']="none";
    document.getElementById('includefordiv').style['display']="block";
  }
  if (fieldtype=='captcha')
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="none"; 
    document.getElementById('valuedropdowndiv').style['display']="none"; 
    document.getElementById('checkeddiv').style['display']="none"; 
    document.getElementById('placeholderdiv').style['display']="block";
    document.getElementById('errormsgdiv').style['display']="block";     
    document.getElementById('fieldwidthdiv').style['display']="none";     
    document.getElementById('bottommargindiv').style['display']="block";     
    document.getElementById('validationdiv').style['display']="block";     
    document.getElementById('validationdropdowndiv').style['display']="none";     
    document.getElementById('useasdiv').style['display']="none";
    document.getElementById('includefordiv').style['display']="block";
//    if (document.getElementById('newvalidation').value=='notrequired')
    if (validation_array[currenteditfield]=='notrequired')
    {
      document.getElementById('errormsgdiv').style['display']='none'
      document.getElementById('showrequireddiv').style['display']='none'
    }  
    else
    {
      document.getElementById('errormsgdiv').style['display']='block'
      document.getElementById('showrequireddiv').style['display']='block'
    }  
  }
  if (fieldtype=='file')
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="none"; 
    document.getElementById('valuedropdowndiv').style['display']="none";
    document.getElementById('checkeddiv').style['display']="none"; 
    document.getElementById('placeholderdiv').style['display']="none";
    document.getElementById('errormsgdiv').style['display']="block";     
    document.getElementById('fieldwidthdiv').style['display']="none";     
    document.getElementById('bottommargindiv').style['display']="block";     
    document.getElementById('validationdiv').style['display']="block";     
    document.getElementById('validationdropdowndiv').style['display']="none";
    document.getElementById('useasdiv').style['display']="none";
    document.getElementById('includefordiv').style['display']="block";
//    if (document.getElementById('newvalidation').value=='notrequired')
    if (validation_array[currenteditfield]=='notrequired')
    {
      document.getElementById('errormsgdiv').style['display']='none'
      document.getElementById('showrequireddiv').style['display']='none'
    }  
    else
    {
      document.getElementById('errormsgdiv').style['display']='block'
      document.getElementById('showrequireddiv').style['display']='block'
    }      
  }
  if (fieldtype=='textarea')
  {
    document.getElementById('sitelokfielddiv').style['display']="block"; 
    document.getElementById('valuediv').style['display']="none"; 
    document.getElementById('valuedropdowndiv').style['display']="none";
    document.getElementById('placeholderdiv').style['display']="block";
    document.getElementById('errormsgdiv').style['display']="block";     
    document.getElementById('fieldwidthdiv').style['display']="block";     
    document.getElementById('bottommargindiv').style['display']="block";     
    document.getElementById('validationdiv').style['display']="block";     
    document.getElementById('validationdropdowndiv').style['display']="none";
    document.getElementById('checkeddiv').style['display']="none"; 
    document.getElementById('useasdiv').style['display']="none";
    document.getElementById('includefordiv').style['display']="block";
    if (document.getElementById('newvalidation').value=='notrequired')
    {
      document.getElementById('errormsgdiv').style['display']='none'
      document.getElementById('showrequireddiv').style['display']='none'
    }  
    else
    {
      document.getElementById('errormsgdiv').style['display']='block'
      document.getElementById('showrequireddiv').style['display']='block'
    }      
  }
  $('#newinputtype').selectpicker('refresh');
  $('#useas').selectpicker('refresh');
  $('#includefor').selectpicker('refresh');
}

</script>
</head>
<?php include("adminthemeheader.php"); ?>
<!-- Content Wrapper. Contains page content -->
<body>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-list-alt"></i>&nbsp;<?php echo ADMINMENU_CONTACTFORMDESIGN; ?>
            <?php if ($act=="editform") { ?><small>form id: <?php echo $actid; ?></small><?php } ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><a href="contactforms.php"><?php echo ADMINMENU_CONTACTFORMS; ?></a></li>
            <li class="active"><?php echo ADMINMENU_CONTACTFORMDESIGN; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Your Page Content Here -->
          <div class="row">


         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
         <div class="box" id="previewbox">
         <div id="togglefixed" class="hidden-xs hidden-sm hidden-md"><span id="toggleicon" class="glyphicon glyphicon-pushpin" rel="tooltip" title="<?php echo ADMINET_TOGFIX; ?>" onclick="toggleFixed();"></span></div>
         <div class="box-body" id="formpreview">
            <!-- Simulate the login form in a div here -->
         </div><!-- /.box -->
         </div><!-- /.box-body -->
         </div><!-- /.col -->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

              <form id="contactform" role="form" class="form-horizontal" method="post">
              <input type="hidden" name="slcsrf" id="slcsrf" value="<?php echo $slcsrftoken; ?>">
              <input name="act" id="act" type="hidden" value="<?php echo $act; ?>">
              <input name="actid" id="actid" type="hidden" value="<?php echo $actid; ?>">
              <input name="formpropertiesfield" id="formpropertiesfield" type="hidden" value="">
              <input name="formstylesfield" id="formstylesfield" type="hidden" value="">
              <input name="numfields" id="numfields" type="hidden" value="">
              <div class="box">
                <div class="box-body">



                  <div class="form-group">
                      <label class="col-xs-12" for="newformname"><?php echo ADMINET_FRMNM; ?></label>
                      <div class="col-xs-12" id="newformnamediv">
                          <input type="text" id="newformname" name="newformname" class="form-control" maxlength="100" value="<?php echo $newformname; ?>">
                      </div>
                  </div>

                  <div class="panel-group" id="accordion" role="tablist">

                
                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse1">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse1" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_FORMPROP; ?></a>
                        </h4>
                      </div>
                      <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-body">

                           <div class="form-group">
                              <label class="col-xs-12" for="newsendemail"><?php echo ADMINET_SENDTO; ?></label>
                              <div class="col-xs-12 col-sm-8 col-md-6 col-lg-8" id="newsendemaildiv">
                                 <textarea class="form-control" rows="4" maxlength="255" id="newsendemail" name="newsendemail"></textarea>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newredirect"><?php echo ADMINET_THANKPAGE; ?></label>
                              <div class="col-xs-12 col-sm-8 col-md-6 col-lg-8" id="newredirectdiv">
                                  <input type="text" id="newredirect" name="newredirect" class="form-control" maxlength="255">
                              </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-10">
                              <div class="checkbox">
                                <label>
                                <input type="checkbox" id="senduseremailvisitor" name="senduseremailvisitor" value="1">&nbsp;&nbsp;<?php echo ADMINET_USEREMAIL." ".ADMINET_VISITOR; ?>
                                </label>
                              </div>
                              <div class="selectemailtemplate" id="senduseremailvisitordiv" onclick="openFileManager('newuseremailvisitor');">
                              <input type="hidden" id="newuseremailvisitor" name="newuseremailvisitor" value="<?php echo $newuseremail; ?>">
                              <span id="newuseremailvisitorname"><?php if ($newuseremailvisitor!="") echo $newuseremailvisitor; else echo ADMINMSG_NOTEMPLATE; ?></span>&nbsp;&nbsp;<span><i class="glyphicon glyphicon-folder-open actionicon"></i></span>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-10">
                              <div class="checkbox">
                                <label>
                                <input type="checkbox" id="sendadminemailvisitor" name="sendadminemailvisitor" value="1">&nbsp;&nbsp;<?php echo ADMINET_ADMINEMAIL." ".ADMINET_VISITOR; ?>
                                </label>
                              </div>
                              <div class="selectemailtemplate" id="sendadminemailvisitordiv" onclick="openFileManager('newadminemailvisitor');">
                              <input type="hidden" id="newadminemailvisitor" name="newadminemailvisitor" value="<?php echo $newadminemail; ?>">
                              <span id="newadminemailvisitorname"><?php if ($newadminemailvisitor!="") echo $newadminemailvisitor; else echo ADMINMSG_NOTEMPLATE; ?></span>&nbsp;&nbsp;<span><i class="glyphicon glyphicon-folder-open actionicon"></i></span>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-10">
                              <div class="checkbox">
                                <label>
                                <input type="checkbox" id="senduseremailmember" name="senduseremailmember" value="1">&nbsp;&nbsp;<?php echo ADMINET_USEREMAIL." ".ADMINET_MEMBER; ?>
                                </label>
                              </div>
                              <div class="selectemailtemplate" id="senduseremailmemberdiv" onclick="openFileManager('newuseremailmember');">
                              <input type="hidden" id="newuseremailmember" name="newuseremailmember" value="<?php echo $newuseremail; ?>">
                              <span id="newuseremailmembername"><?php if ($newuseremailmember!="") echo $newuseremailmember; else echo ADMINMSG_NOTEMPLATE; ?></span>&nbsp;&nbsp;<span><i class="glyphicon glyphicon-folder-open actionicon"></i></span>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-10">
                              <div class="checkbox">
                                <label>
                                <input type="checkbox" id="sendadminemailmember" name="sendadminemailmember" value="1">&nbsp;&nbsp;<?php echo ADMINET_ADMINEMAIL." ".ADMINET_MEMBER; ?>
                                </label>
                              </div>
                              <div class="selectemailtemplate" id="sendadminemailmemberdiv" onclick="openFileManager('newadminemailmember');">
                              <input type="hidden" id="newadminemailmember" name="newadminemailmember" value="<?php echo $newadminemail; ?>">
                              <span id="newadminemailmembername"><?php if ($newadminemailmember!="") echo $newadminemailmember; else echo ADMINMSG_NOTEMPLATE; ?></span>&nbsp;&nbsp;<span><i class="glyphicon glyphicon-folder-open actionicon"></i></span>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newfromnameuser"><?php echo ADMINET_FROMNAME; ?></label>
                              <div class="col-xs-12 col-sm-8 col-md-6 col-lg-8" id="newfromnameuserdiv">
                                  <input type="text" id="newfromnameuser" name="newfromnameuser" class="form-control" maxlength="255">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newreplytouser"><?php echo ADMINET_REPLYTO; ?></label>
                              <div class="col-xs-12 col-sm-8 col-md-6 col-lg-8" id="newreplytouserdiv">
                                  <input type="text" id="newreplytouser" name="newreplytouser" class="form-control" maxlength="255">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newsendasuser"><?php echo ADMINET_SENDAS; ?></label>
                             <div class="col-xs-12 col-sm-8 col-md-6 col-lg-8" id="newsendasuserdiv">
                                <select id="newsendasuser" name="newsendasuser" class="form-control selectpicker">
                                <option value="0">No - Only reply-to and from name are set as user</option>
                                <option value="1">Yes - not supported by all servers or Amazon SES</option>
                                </select>  
                            </div>
                           </div> 
                          <div class="form-group">
                              <label class="col-xs-12" for="newattachmentsize"><?php echo ADMINET_MAXSIZE; ?></label>
                              <div class="col-xs-12 col-sm-4 col-md-3 col-lg-4" id="newattachmentsizediv">
                                  <input type="text" id="newattachmentsize" name="newattachmentsize" class="form-control" maxlength="9">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newattachmenttypes"><?php echo ADMINET_ATTACHTYP; ?></label>
                              <div class="col-xs-12 col-sm-8 col-md-6 col-lg-8" id="newattachmenttypesdiv">
                                  <input type="text" id="newattachmenttypes" name="newattachmenttypes" class="form-control" maxlength="255">
                              <div class="help-block"><?php echo ADMINET_ATTACHTYPNT; ?></div>
                              </div>
                          </div>

                          </div>
                        <div>    
                      </div>
                    </div>  
                    </div>

                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse2">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse2" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_FORMSTYLE; ?></a>
                        </h4>
                      </div>
                      <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">

                          <div class="form-group">
                            <label class="col-xs-12" for="newfonttype"><?php echo ADMINET_FONTTYP; ?></label>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newfonttypediv">
                                <select id="newfonttype" name="newfonttype" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="Arial, Helvetica, sans-serif" <?php if ($newfonttype=="Arial, Helvetica, sans-serif") print "selected=\"selected\"";?>>Arial</option>
                                <option value="Arial Narrow, sans-serif" <?php if ($newfonttype=="Arial Narrow, sans-serif") print "selected=\"selected\"";?>>Arial Narrow</option>
                                <option value="Century Gothic, sans-serif" <?php if ($newfonttype=="Century Gothic, sans-serif") print "selected=\"selected\"";?>>Century Gothic</option>
                                <option value="Copperplate Gothic Light, sans-serif" <?php if ($newfonttype=="Copperplate Gothic Light, sans-serif") print "selected=\"selected\"";?>>Copperplate Gothic Light</option>
                                <option value="Courier New, Courier, monospace" <?php if ($newfonttype=="Courier New, Courier, monospace") print "selected=\"selected\"";?>>Courier New</option>
                                <option value="Garamond, Serif" <?php if ($newfonttype=="Garamond, Serif") print "selected=\"selected\"";?>>Garamond</option>
                                <option value="Georgia, Serif" <?php if ($newfonttype=="Georgia, Serif") print "selected=\"selected\"";?>>Georgia</option>
                                <option value="Gill Sans MT, sans-serif" <?php if ($newfonttype=="Gill Sans MT, sans-serif") print "selected=\"selected\"";?>>Gill Sans MT</option>
                                <option value="Impact, Charcoal, sans-serif" <?php if ($newfonttype=="Impact, Charcoal, sans-serif") print "selected=\"selected\"";?>>Impact</option>
                                <option value="Lucida Console, Monaco, monospace" <?php if ($newfonttype=="Lucida Console, Monaco, monospace") print "selected=\"selected\"";?>>Lucida Console</option>
                                <option value="Lucida Sans Unicode, Lucida Grande, sans-serif" <?php if ($newfonttype=="Lucida Sans Unicode, Lucida Grande, sans-serif") print "selected=\"selected\"";?>>Lucida Sans Unicode</option>
                                <option value="Palatino Linotype, Book Antiqua, Palatino, serif" <?php if ($newfonttype=="Palatino Linotype, Book Antiqua, Palatino, serif") print "selected=\"selected\"";?>>Palatino Linotype</option>
                                <option value="Tahoma, Geneva, sans-serif" <?php if ($newfonttype=="Tahoma, Geneva, sans-serif") print "selected=\"selected\"";?>>Tahoma</option>
                                <option value="Times New Roman, Times, serif" <?php if ($newfonttype=="Times New Roman, Times, serif") print "selected=\"selected\"";?>>Times New Roman</option>
                                <option value="Verdana, Geneva, sans-serif" <?php if ($newfonttype=="Verdana, Geneva, sans-serif") print "selected=\"selected\"";?>>Verdana</option>
                                <option value="Trebuchet MS, Helvetica, sans-serif" <?php if ($newfonttype=="Trebuchet MS, Helvetica, sans-serif") print "selected=\"selected\"";?>>Trebuchet MS</option>
                                </select>  
                            </div>    
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newlabelstyle"><?php echo ADMINET_INPLBLSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newlabelstylediv" onchange="updatePreview()">
                                <select id="newlabelstyle" name="newlabelstyle" class="form-control selectpicker">
                                <option value="normal normal normal" <?php if ($newlabelstyle=="normal normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold" <?php if ($newlabelstyle=="normal normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal" <?php if ($newlabelstyle=="italic normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold" <?php if ($newlabelstyle=="italic normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newlabelcolordiv">
                                <input type="text" id="newlabelcolor" name="newlabelcolor" class="form-control colorbox" maxlength="7" value="<?php echo $newlabelcolor; ?>" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newlabelsizediv">
                                <div class="input-group">
                                  <input type="number" id="newlabelsize" name="newlabelsize" class="form-control" maxlength="2" min="1" max="99" value="<?php echo $newlabelsize; ?>" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newinputtextstyle"><?php echo ADMINET_INPTXTSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newinputtextstylediv" onchange="updatePreview()">
                                <select id="newinputtextstyle" name="newinputtextstyle" class="form-control selectpicker">
                                <option value="normal normal normal" <?php if ($newinputtextstyle=="normal normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold" <?php if ($newinputtextstyle=="normal normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal" <?php if ($newinputtextstyle=="italic normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold" <?php if ($newinputtextstyle=="italic normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newinputtextcolordiv">
                                <input type="text" id="newinputtextcolor" name="newinputtextcolor" class="form-control colorbox" maxlength="7" value="<?php echo $newinputtextcolor; ?>" onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newinputtextsizediv">
                                <div class="input-group">
                                  <input type="number" id="newinputtextsize" name="newinputtextsize" class="form-control" maxlength="2" min="1" max="99" value="<?php echo $newinputtextsize; ?>" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newinputbackcolor"><?php echo ADMINET_INPBCKCOL; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newinputbackcolordiv">
                              <input type="text" id="newinputbackcolor" name="newinputbackcolor" class="form-control colorbox" maxlength="7" value="<?php echo $newinputbackcolor; ?>" onchange="updatePreview()">
                            </div> 
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbordersize"><?php echo ADMINET_INPTXTBRDSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbordersizediv" onchange="updatePreview()">
                                <select id="newbordersize" name="newbordersize" class="form-control selectpicker">
                                <option value="0"><?php echo ADMINET_NONE; ?></option>
                                <option value="1">1px</option>
                                <option value="2">2px</option>
                                <option value="3">3px</option>
                                <option value="4">4px</option>
                                <option value="5">5px</option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbordercolordiv">
                                <input type="text" id="newbordercolor" name="newbordercolor" class="form-control colorbox" maxlength="7" value="<?php echo $newbordercolor; ?>" onchange="updatePreview()"">
                            </div> 
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newborderradiusdiv" onchange="updatePreview()">
                                <select id="newborderradius" name="newborderradius" class="form-control selectpicker">
                                <option value="0"><?php echo ADMINET_SQUARE; ?></option>
                                <option value="5"><?php echo ADMINET_ROUNDED; ?> 5px</option>
                                <option value="10"><?php echo ADMINET_ROUNDED; ?> 10px</option>
                                <option value="15"><?php echo ADMINET_ROUNDED; ?> 15px</option>
                                <option value="20"><?php echo ADMINET_ROUNDED; ?> 20px</option>
                                </select>  
                            </div> 
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newinputpaddingv"><?php echo ADMINET_INPPADDING; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newinputpaddingvdiv" onchange="updatePreview()">
                                <select id="newinputpaddingv" name="newinputpaddingv" class="form-control selectpicker">
                                <option value="0.3em"><?php echo "default"; ?></option>
                                <option value="0px">0px</option>
                                <option value="5px">5px</option>
                                <option value="10px">10px</option>
                                <option value="15px">15px</option>
                                <option value="20px">20px</option>
                                <option value="25px">25px</option>
                                <option value="30px">30px</option>
                                </select>  
                            </div> 
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newinputpaddinghdiv" onchange="updatePreview()">
                                <select id="newinputpaddingh" name="newinputpaddingh" class="form-control selectpicker">
                                <option value="0.3em"><?php echo "default"; ?></option>
                                <option value="0px">0px</option>
                                <option value="5px">5px</option>
                                <option value="10px">10px</option>
                                <option value="15px">15px</option>
                                <option value="20px">20px</option>
                                <option value="25px">25px</option>
                                <option value="30px">30px</option>
                                </select>  
                            </div> 
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newrqdfieldlabel"><?php echo ADMINET_RQDFLDLAB; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-5" id="newrqdfieldlabeldiv">
                                  <input type="text" id="newrqdfieldlabel" name="newrqdfieldlabel" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newrqdfieldstyle"><?php echo ADMINET_RQDFLDSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newrqdfieldstylediv" onchange="updatePreview()">
                                <select id="newrqdfieldstyle" name="newrqdfieldstyle" class="form-control selectpicker">
                                <option value="normal normal normal" <?php if ($newrqdfieldstyle=="normal normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold" <?php if ($newrqdfieldstyle=="normal normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal" <?php if ($newrqdfieldstyle=="italic normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold" <?php if ($newrqdfieldstyle=="italic normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newrqdfieldcolordiv">
                                <input type="text" id="newrqdfieldcolor" name="newrqdfieldcolor" class="form-control colorbox" maxlength="7"  onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newrqdfieldsizediv">
                                <div class="input-group">
                                  <input type="number" id="newrqdfieldsize" name="newrqdfieldsize" class="form-control" maxlength="2" min="1" max="99"  onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newmessagestyle"><?php echo ADMINET_MSGSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newmessagestylediv">
                                <select id="newmessagestyle" name="newmessagestyle" class="form-control selectpicker" onchange="updatePreview();">
                                <option value="normal normal normal" <?php if ($newmessagestyle=="normal normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold" <?php if ($newmessagestyle=="normal normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal" <?php if ($newmessagestyle=="italic normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold" <?php if ($newmessagestyle=="italic normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newmessagecolordiv">
                                <input type="text" id="newmessagecolor" name="newmessagecolor" class="form-control colorbox" maxlength="7"  onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newmessagesizediv">
                                <div class="input-group">
                                  <input type="number" id="newmessagesize" name="newmessagesize" class="form-control" maxlength="2" min="1" max="99" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-xs-12" for="newformerrormsg"><?php echo ADMINET_FRMERRMSG; ?></label>
                              <div class="col-xs-12" id="newformerrormsgdiv">
                                  <input type="text" id="newformerrormsg" name="newformerrormsg" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newformerrormsgstyle"><?php echo ADMINET_FRMERRMSGSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newformerrormsgstylediv">
                                <select id="newformerrormsgstyle" name="newformerrormsgstyle" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="normal normal normal" <?php if ($newformerrormsgstyle=="normal normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold" <?php if ($newformerrormsgstyle=="normal normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal" <?php if ($newformerrormsgstyle=="italic normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold" <?php if ($newformerrormsgstyle=="italic normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newformerrormsgcolordiv">
                                <input type="text" id="newformerrormsgcolor" name="newformerrormsgcolor" class="form-control colorbox" maxlength="7"  onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newformerrormsgsizediv">
                                <div class="input-group">
                                  <input type="number" id="newformerrormsgsize" name="newformerrormsgsize" class="form-control" maxlength="2" min="1" max="99"  onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newmaxformwidth"><?php echo ADMINET_FRMMAXWID; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newmaxformwidthdiv">
                                <div class="input-group">
                                  <input type="number" id="newmaxformwidth" name="newmaxformwidth" class="form-control" maxlength="6" min="0" max="999999" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                            </div> 
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newpreviewbackcolor"><?php echo ADMINET_PREVCOL; ?></label>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newpreviewbackcolordiv">
                              <input type="text" id="newpreviewbackcolor" name="newpreviewbackcolor" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                            </div> 
                          </div>

                         </div>
                      </div>
                    </div>  


                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse3">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse3" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_BTNSTYLE; ?></a>
                        </h4>
                      </div>
                      <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">

                            <div class="form-group">
                              <label class="col-xs-12" for="newbtnlabel"><?php echo ADMINET_BTNLBL; ?></label>
                              <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newbtnlabeldiv">
                                  <input type="text" id="newbtnlabel" name="newbtnlabel" class="form-control" maxlength="50" onchange="updatePreview()">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtnlabelfont"><?php echo ADMINET_BTNFONT; ?></label>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newbtnlabelfontdiv">
                                <select id="newbtnlabelfont" name="newbtnlabelfont" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="Arial, Helvetica, sans-serif" <?php if ($newbtnlabelfont=="Arial, Helvetica, sans-serif") print "selected=\"selected\"";?>>Arial</option>
                                <option value="Times New Roman, Times, serif" <?php if ($newbtnlabelfont=="Times New Roman, Times, serif") print "selected=\"selected\"";?>>Times New Roman</option>
                                <option value="Verdana, Geneva, sans-serif" <?php if ($newbtnlabelfont=="Verdana, Geneva, sans-serif") print "selected=\"selected\"";?>>Verdana</option>
                                <option value="Impact, Charcoal, sans-serif" <?php if ($newbtnlabelfont=="Impact, Charcoal, sans-serif") print "selected=\"selected\"";?>>Impact</option>
                                <option value="Palatino Linotype, Book Antiqua, Palatino, serif" <?php if ($newbtnlabelfont=="Palatino Linotype, Book Antiqua, Palatino, serif") print "selected=\"selected\"";?>>Palatino Linotype</option>
                                <option value="Tahoma, Geneva, sans-serif" <?php if ($newbtnlabelfont=="Tahoma, Geneva, sans-serif") print "selected=\"selected\"";?>>Tahoma</option>
                                <option value="Century Gothic, sans-serif" <?php if ($newbtnlabelfont=="Century Gothic, sans-serif") print "selected=\"selected\"";?>>Century Gothic</option>
                                <option value="Lucida Sans Unicode, Lucida Grande, sans-serif" <?php if ($newbtnlabelfont=="Lucida Sans Unicode, Lucida Grande, sans-serif") print "selected=\"selected\"";?>>Lucida Sans Unicode</option>
                                <option value="Arial Narrow, sans-serif" <?php if ($newbtnlabelfont=="Arial Narrow, sans-serif") print "selected=\"selected\"";?>>Arial Narrow</option>
                                <option value="Copperplate Gothic Light, sans-serif" <?php if ($newbtnlabelfont=="Copperplate Gothic Light, sans-serif") print "selected=\"selected\"";?>>Copperplate Gothic Light</option>
                                <option value="Lucida Console, Monaco, monospace" <?php if ($newbtnlabelfont=="Lucida Console, Monaco, monospace") print "selected=\"selected\"";?>>Lucida Console</option>
                                <option value="Gill Sans MT, sans-serif" <?php if ($newbtnlabelfont=="Gill Sans MT, sans-serif") print "selected=\"selected\"";?>>Gill Sans MT</option>
                                <option value="Trebuchet MS, Helvetica, sans-serif" <?php if ($newbtnlabelfont=="Trebuchet MS, Helvetica, sans-serif") print "selected=\"selected\"";?>>Trebuchet MS</option>
                                <option value="Courier New, Courier, monospace" <?php if ($newbtnlabelfont=="Courier New, Courier, monospace") print "selected=\"selected\"";?>>Courier New</option>
                                <option value="Georgia, Serif" <?php if ($newbtnlabelfont=="Georgia, Serif") print "selected=\"selected\"";?>>Georgia</option>
                                <option value="Garamond, Serif" <?php if ($newbtnlabelfont=="Garamond, Serif") print "selected=\"selected\"";?>>Garamond</option>
                                </select>  
                            </div>    
                          </div>                      
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtnlabelstyle"><?php echo ADMINET_BTNLBLSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtnlabelstylediv">
                                <select id="newbtnlabelstyle" name="newbtnlabelstyle" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="normal normal normal" <?php if ($newbtnlabelstyle=="normal normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_NORMAL; ?></option>
                                <option value="normal normal bold" <?php if ($newbtnlabelstyle=="normal normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?></option>
                                <option value="italic normal normal" <?php if ($newbtnlabelstyle=="italic normal normal") print "selected=\"selected\"";?>><?php echo ADMINET_ITALIC; ?></option>
                                <option value="italic normal bold" <?php if ($newbtnlabelstyle=="italic normal bold") print "selected=\"selected\"";?>><?php echo ADMINET_BOLD; ?> <?php echo ADMINET_ITALIC; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbtnlabelcolordiv">
                                <input type="text" id="newbtnlabelcolor" name="newbtnlabelcolor" class="form-control colorbox" maxlength="7"  onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newbtnlabelsizediv">
                                <div class="input-group">
                                  <input type="number" id="newbtnlabelsize" name="newbtnlabelsize" class="form-control" maxlength="2" min="1" max="99" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtncolortype"><?php echo ADMINET_BTNCOL; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtncolortypediv">
                                <select id="newbtncolortype" name="newbtncolortype" class="form-control selectpicker" onchange="filltype('newbtncolortype','newbtncolortodiv'); updatePreview()">
                                <option value="solid" <?php if ($newbtncolortype=="solid") print "selected=\"selected\"";?>><?php echo ADMINET_SOLID; ?></option>
                                <option value="gradient" <?php if ($newbtncolortype=="gradient") print "selected=\"selected\"";?>><?php echo ADMINET_GRADIENT; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbtncolorfromdiv">
                                <input type="text" id="newbtncolorfrom" name="newbtncolorfrom" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                            </div> 
                            <div class="col-xs-6 col-sm-5 col-md-4 col-lg-5" id="newbtncolortodiv">
                              <div class="input-group">
                                <input type="text" id="newbtncolorto" name="newbtncolorto" class="form-control colorbox" maxlength="7" onchange="updatePreview()">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button" id="loginbuttoncolorreverse" onclick="gradientreverse('newbtncolorfrom','newbtncolorto')"><span class="glyphicon glyphicon-transfer actionicon" title="reverse"></span></button>
                                </span>
                              </div>                                                        
                            </div>                           
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtnborderstyle"><?php echo ADMINET_BTNBRDSTY; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtnborderstylediv">
                                <select id="newbtnborderstyle" name="newbtnborderstyle" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="solid" <?php if ($newbtnborderstyle=="solid") print "selected=\"selected\"";?>><?php echo ADMINET_SOLID; ?></option>
                                <option value="dotted" <?php if ($newbtnborderstyle=="dotted") print "selected=\"selected\"";?>><?php echo ADMINET_DOTTED; ?></option>
                                <option value="dashed" <?php if ($newbtnborderstyle=="dashed") print "selected=\"selected\"";?>><?php echo ADMINET_DASHED; ?></option>
                                </select>  
                            </div> 
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-4" id="newbtnbordercolordiv">
                                <input type="text" id="newbtnbordercolor" name="newbtnbordercolor" class="form-control colorbox" maxlength="7"  onchange="updatePreview()">
                            </div> 
                              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-4" id="newbtnbordersizediv">
                                <div class="input-group">
                                  <input type="number" id="newbtnbordersize" name="newbtnbordersize" class="form-control" maxlength="2" min="0" max="99" onchange="updatePreview()">
                                  <span class="input-group-addon" id="basic-addon1">px</span>
                                </div> 
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtnpaddingv"><?php echo ADMINET_BTNPADDING; ?></label>
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtnpaddingvdiv" onchange="updatePreview()">
                                <select id="newbtnpaddingv" name="newbtnpaddingv" class="form-control selectpicker">
                                <option value="6"><?php echo "default"; ?></option>
                                <option value="0">0px</option>
                                <option value="5">5px</option>
                                <option value="10">10px</option>
                                <option value="15">15px</option>
                                <option value="20">20px</option>
                                <option value="25">25px</option>
                                <option value="30">30px</option>
                                <option value="40">40px</option>
                                <option value="50">50px</option>
                                <option value="60">60px</option>
                                <option value="70">70px</option>
                                <option value="80">80px</option>
                                <option value="90">90px</option>
                                <option value="100">100px</option>
                                </select>  
                            </div> 
                             <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="newbtnpaddinghdiv" onchange="updatePreview()">
                                <select id="newbtnpaddingh" name="newbtnpaddingh" class="form-control selectpicker">
                                <option value="24"><?php echo "default"; ?></option>
                                <option value="0">0px</option>
                                <option value="5">5px</option>
                                <option value="10">10px</option>
                                <option value="15">15px</option>
                                <option value="20">20px</option>
                                <option value="25">25px</option>
                                <option value="30">30px</option>
                                <option value="40">40px</option>
                                <option value="50">50px</option>
                                <option value="60">60px</option>
                                <option value="70">70px</option>
                                <option value="80">80px</option>
                                <option value="90">90px</option>
                                <option value="100">100px</option>
                                </select>  
                            </div> 
                          </div>                          
                          <div class="form-group">
                            <label class="col-xs-12" for="newbtnradius"><?php echo ADMINET_BTNSHAPE; ?></label>
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newbtnradiusdiv">
                                <select id="newbtnradius" name="newbtnradius" class="form-control selectpicker" onchange="updatePreview()">
                                <option value="0" <?php if ($newbtnradius=="0") print "selected=\"selected\"";?>><?php echo ADMINET_SQUARE; ?></option>
                                <option value="5" <?php if ($newbtnradius=="5") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 5px</option>
                                <option value="10" <?php if ($newbtnradius=="10") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 10px</option>
                                <option value="15" <?php if ($newbtnradius=="15") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 15px</option>
                                <option value="20" <?php if ($newbtnradius=="20") print "selected=\"selected\"";?>><?php echo ADMINET_ROUNDED; ?> 20px</option>
                                </select>  
                            </div>    
                          </div>                      

                         </div>
                      </div>
                    </div>  



                    <div class="panel panel-default">
                      <div class="panel-heading" data-toggle="collapse" href="#collapse4">
                        <h4 class="panel-title">
                          <a class="" data-toggle="collapse" href="#collapse4" data-parent="#accordion"><span class="glyphicon glyphicon-plus"></span>&nbsp;<?php echo ADMINET_FLDPROP; ?></a>
                        </h4>
                      </div>
                      <div id="collapse4" class="panel-collapse collapse">
                        <div class="panel-body">

<div id="nopropertiesdiv">
<p id="nopropertiesp">
<?php
$str=ADMINET_FLDNOTE;
$str=str_replace("***1***", "<span class=\"glyphicon glyphicon-edit actionicon\"></span>", $str);
$str=str_replace("***2***", "<i class=\"fa fa-plus actionicon\" rel=\"tooltip\" title=\"".ADMINET_ADDFLD."\" onclick=\"addfield();\"></i>", $str);
echo $str;
?>
</p>
</div>

<div id="propertiesdiv">

                        <div class="form-group">
                          <label class="col-xs-12" for="newinputtype"><?php echo ADMINET_INPTYP; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newinputtypediv">
                              <select id="newinputtype" name="newinputtype" class="form-control selectpicker" onchange="inputtype_array[currenteditfield]=this.value;fieldpropertiesform(this.value); updatePreview()">
                              <option value="text"><?php echo ADMINET_TEXT; ?></option>
                              <option value="password"><?php echo ADMINET_PASS; ?></option>
                              <option value="dropdown"><?php echo ADMINET_DRPDWN; ?></option>
                              <option value="checkbox"><?php echo ADMINET_CHECKBOX; ?></option>
                              <option value="radio"><?php echo ADMINET_RADIO; ?></option>
                              <option value="label"><?php echo ADMINET_LABEL; ?></option>
                              <option value="captcha"><?php echo ADMINET_CAPTCHA; ?></option>
                              <option value="file"><?php echo ADMINET_FILEUPL; ?></option>
                              <option value="textarea"><?php echo ADMINET_TXTAREA; ?></option>
                              </select>  
                          </div>    
                        </div>                      

                        <div class="form-group">
                          <label class="col-xs-12" for="newlabeltext"><?php echo ADMINET_LBLTXT; ?></label>
                          <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newlabeltextdiv">
                              <input type="text" id="newlabeltext" name="newlabeltext" class="form-control" maxlength="255" onchange="labeltext_array[currenteditfield]=this.value; updatePreview()">
                          </div>
                        </div>

                        <div id="valuediv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newvalue"><?php echo ADMINET_VALUE; ?></label>
                          <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newvaluediv">
                              <input type="text" id="newvalue" name="newvalue" class="form-control" maxlength="255" onchange="value_array[currenteditfield]=this.value; updatePreview()">
                          </div>
                        </div>
                        </div> <!-- valuediv -->                        

                        <div id="valuedropdowndiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newvaluedropdown"><?php echo ADMINET_OPTIONS; ?></label>
                          <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newvaluedropdowndiv">
                              <textarea id="newvaluedropdown" name="newvaluedropdown" wrap="off" class="form-control" maxlength="255" onchange="value_array[currenteditfield]=this.value; updatePreview()"></textarea>
                          </div>
                        </div>
                        </div> <!-- valuedropdowndiv -->

                        <div id="checkeddiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newchecked"><?php echo ADMINET_INICHKD; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newcheckeddiv">
                              <select id="newchecked" name="newchecked" class="form-control selectpicker" onchange="checked_array[currenteditfield]=this.value; updatePreview()">
                              <option value="0"><?php echo ADMINET_NOTCHKD; ?></option>
                              <option value="1"><?php echo ADMINET_CHKD; ?></option>
                              </select>  
                          </div>    
                        </div>                      
                        </div> <!-- checkeddiv -->

                        <div id="placeholderdiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newplacetext"><?php echo ADMINET_PLACETXT; ?></label>
                          <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newplacetextdiv">
                              <input type="text" id="newplacetext" name="newplacetext" class="form-control" maxlength="255" onchange="placetext_array[currenteditfield]=this.value; updatePreview();">
                          </div>
                        </div>
                        </div> <!-- placeholderdiv -->

                        <div id="validationdiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newvalidation"><?php echo ADMINET_VALIDATION; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newvalidationdiv">
                              <select id="newvalidation" name="newvalidation" class="form-control selectpicker" onchange="validation_array[currenteditfield]=this.value; if (this.value!='notrequired') showrequired_array[currenteditfield]='1'; else showrequired_array[currenteditfield]='0'; document.getElementById('newshowrequired').value=showrequired_array[currenteditfield]; fieldpropertiesform();  updatePreview()">
                              <option value="notrequired">not required</option>
                              <option value="required">required</option>
                              <option value="requiredemail">email address required</option>
                              </select>  
                          </div>    
                        </div>                      
                        </div> <!-- validationdiv -->

                        <div id="validationdropdowndiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newvalidationdropdown"><?php echo ADMINET_REQD; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newvalidationdropdowndiv">
                              <select id="newvalidationdropdown" name="newvalidationdropdown" class="form-control selectpicker" onchange="validation_array[currenteditfield]=this.value;  if (this.value!='notrequired') showrequired_array[currenteditfield]='1'; else showrequired_array[currenteditfield]='0'; document.getElementById('newshowrequired').value=showrequired_array[currenteditfield]; fieldpropertiesform(); updatePreview()">
                              <option value="notrequired"><?php echo ADMINET_NOTREQD; ?></option>
                              <option value="required"><?php echo ADMINET_REQD." ".ADMINET_NOTFIRST; ?></option>
                              </select>  
                          </div>    
                        </div>                      
                        </div> <!-- validationdropdowndiv -->

                        <div id="showrequireddiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newshowrequired"><?php echo ADMINET_SHWREQ; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newshowrequireddiv">
                              <select id="newshowrequired" name="newshowrequired" class="form-control selectpicker" onchange="showrequired_array[currenteditfield]=this.value; updatePreview()">
                              <option value="1"><?php echo ADMINET_SHWREQLAB; ?></option>
                              <option value="0"><?php echo ADMINET_NOREQLAB; ?></option>
                              </select>  
                          </div>    
                        </div>                      
                        </div> <!-- showrequireddiv -->

                        <div id="errormsgdiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newerrormsg"><?php echo ADMINET_ERRMSG; ?></label>
                          <div class="col-xs-12 col-sm-6 col-md-5 col-lg-7" id="newerrormsgdiv">
                              <input type="text" id="newerrormsg" name="newerrormsg" class="form-control" maxlength="255" onchange="errormsg_array[currenteditfield]=this.value; updatePreview();">
                          </div>
                        </div>
                        </div> <!-- errormsgdiv -->

                        <div id="sitelokfielddiv">                        
                          <div class="form-group">
                            <label class="col-xs-12" for="newsitelokfield"><?php echo ADMINET_PREFILLOPT; ?></label>
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newsitelokfielddiv">
                                <select id="newsitelokfield" name="newsitelokfield" class="form-control selectpicker" onchange="sitelokfield_array[currenteditfield]=this.value;">
                                  <option value=""><?php echo ADMINET_NOPREFILL; ?></option>
                                  <option value="username"><?php echo ADMINFIELD_USERNAME; ?></option>
                                  <option value="fullname"><?php echo ADMINEMAILUSER_FULLNAME; ?></option>
                                  <option value="firstname"><?php echo ADMINEMAILUSER_FIRSTNAME; ?></option>
                                  <option value="lastname"><?php echo ADMINEMAILUSER_LASTNAME; ?></option>
                                  <option value="email"><?php echo ADMINFIELD_EMAIL; ?></option>
                                  <option value="usergroup"><?php echo ADMINFIELD_USERGROUP; ?></option>
                                <?php for ($k=1;$k<51;$k++)
                                {
                                  $custitle="CustomTitle".$k;  
                                ?>
                                <option value="custom<?php echo $k; ?>"><?php if ($$custitle!="") echo $$custitle." (custom ".$k.")"; else echo "custom ".$k; ?></option>
                                <?php } ?>
                                </select>  
                            </div>    
                          </div>                      
                        </div> <!-- sitelokfielddiv -->

                        <div id="useasdiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newuseas"><?php echo ADMINET_USEAS; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newuseasdiv">
                              <select id="newuseas" name="newuseas" class="form-control selectpicker" onchange="useas_array[currenteditfield]=this.value; updatePreview()">
                              <option value="0">don't use</option>
                              <option value="1">reply-to email</option>
                              <option value="2">from name</option>
                              </select>  
                          </div>    
                        </div>                      
                        </div> <!-- useasdiv -->

                        <div id="includefordiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newshowfieldfor"><?php echo ADMINET_INCLFOR; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newshowfieldfordiv">
                              <select id="newshowfieldfor" name="newshowfieldfor" class="form-control selectpicker" onchange="showfieldfor_array[currenteditfield]=this.value; updatePreview()">
                              <option value="0">all users</option>
                              <option value="1">visitors only</option>
                              <option value="2">logged in users only</option>
                              </select>  
                          </div>    
                        </div>                      
                        </div> <!-- includefordiv -->

                        <div id="fieldwidthdiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newfieldwidth"><?php echo ADMINET_FLDWID; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newfieldwidthdiv">
                              <select id="newfieldwidth" name="newfieldwidth" class="form-control selectpicker" onchange="fieldwidth_array[currenteditfield]=this.value; updatePreview()">
                              <option value="100">100%</option>
                              <option value="75">75%</option>
                              <option value="66">66%</option>
                              <option value="50">50%</option>
                              <option value="33">33%</option>
                              <option value="25">25%</option>
                              <option value="20">20%</option>
                              <option value="10">10%</option>
                              </select>  
                          </div>    
                        </div>                      
                        </div> <!-- fieldwidthdiv -->

                        <div id="bottommargindiv">
                        <div class="form-group">
                          <label class="col-xs-12" for="newbottommargin"><?php echo ADMINET_BOTMGN; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newfieldwidthdiv">
                              <select id="newbottommargin" name="newbottommargin" class="form-control selectpicker" onchange="bottommargin_array[currenteditfield]=this.value; updatePreview()">
                              <option value="50">50px</option>
                              <option value="40">40px</option>
                              <option value="30">30px</option>
                              <option value="25">25px</option>
                              <option value="20">20px</option>
                              <option value="15">15px</option>
                              <option value="10">10px</option>
                              <option value="5">5px</option>
                              <option value="1">1px</option>
                              </select>  
                          </div>    
                        </div>
                        </div> <!-- bottommargindiv -->                     

                        <div class="form-group">
                          <label class="col-xs-12" for="newposition"><?php echo ADMINET_FRMPOS; ?></label>
                          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-7" id="newpositiondiv">
                              <select id="newposition" name="newposition" class="form-control selectpicker" onchange="updateposition(this.value); updatePreview()">
                              </select>  
                          </div>    
                        </div>                      

</div> <!-- propertiesdiv -->



                        </div>
                      </div>
                    </div>  



                  </div> <!-- panel-group -->

                </div> <!-- box body -->
              </div> <!-- box --> 

              <div class="alert alert-warning" role="alert" id="formissues"></div>


                    <div class="form-group">
                      <div class="col-xs-12">
                        <div class="btn-toolbar">
                            <button type="submit" id="submit" class="btn btn-primary pull-left"><?php echo ADMINBUTTON_SAVECHANGES; ?></button>
                            <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='contactforms.php';"><?php echo ADMINBUTTON_RETURNTOFORMS ?></button>
                            <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                        </div> 
                      </div>    
                    </div>

              <div id="resultcontactform"></div>

              </form>




           </div> <!-- col -->


          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include("adminthemefooter.php"); ?>
    <script src="jquery-minicolors-master/jquery.minicolors.js" type="text/javascript"></script>
    <script type="text/javascript" src="fancybox/source/jquery.fancybox.js?v=2.1.4"></script>
    <script src="contactformedit.js" type="text/javascript"></script>
    <script type="text/javascript">
      startvalues();
    </script>
</body>
</html>
