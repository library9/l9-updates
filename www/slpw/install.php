<?php
@error_reporting (E_ERROR);

require("slconfig.php");
$installversion="5.1";

// Get full server path to slpw folder
$slpwpath=str_replace(basename(__FILE__), '', realpath(__FILE__));
$slpwpath=str_replace("\\","/",$slpwpath);

// Get URL to slpw folder
if ($_SERVER["HTTPS"]=="on")
{
  $slpwurl="https://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
  $slpwurl=str_replace("install.php","",$slpwurl);
}
else
{
  $slpwurl="http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
  $slpwurl=str_replace("install.php","",$slpwurl);
}

print "<html>\n";
print "<head>\n";
print "<title>Sitelok V".$installversion." installation</title>\n";
print "<head>\n";
print "<body>\n";

// First check that PHP version is 5.3.7 or above and mysqli library is available
if (version_compare(phpversion(), '5.3.7', '<'))
{
  print "This version of Sitelok requires PHP 5.3.7 or greater<br>\n";
  print "</body>\n";
  print "</html>\n";
  exit;
}
if (!function_exists('mysqli_query'))
{
  print "This version of Sitelok requires the mysqli library which should be included by default. Check with your hosting company.<br>\n";
  print "</body>\n";
  print "</html>\n";
  exit;
}

// Connect to database
$pos=strpos($DbHost,":");
if ($pos!==false)
  $mysql_link=mysqli_connect(substr($DbHost,0,$pos),$DbUser,$DbPassword,$DbName,substr($DbHost,$pos+1));
else
  $mysql_link=mysqli_connect($DbHost,$DbUser,$DbPassword,$DbName);    
if ($mysql_link===false)
{
  print("Can't connect to MySQL server. Please check the settings in slconfig.php.");
  print "</body>\n";
  print "</html>\n";
  exit;
}
/*
$db=mysqli_select_db($mysql_link,$DbName);
if ($db===false)
{
  print("Can't open database. Please check the settings in slconfig.php.");
  mysqli_close($mysql_link);
  print "</body>\n";
  print "</html>\n";
  exit;
}
*/
// Create main sitelok (users) table
if (!tableexists($mysql_link, $DbTableName, "Selected"))
{
  $query="CREATE TABLE `$DbTableName` (
  `Selected` varchar(3) NOT NULL NOT NULL default '',
  `Created` varchar(6) NOT NULL default '',
  `Username` varchar(100) NOT NULL,
  `Passphrase` varchar(255) NOT NULL default '',
  `Enabled` varchar(3) NOT NULL default '',
  `Name` varchar(100) NOT NULL default '',
  `Email` varchar(100) NOT NULL default '',
  `Usergroups` varchar(255) NOT NULL default '',
  `Custom1` varchar(255) NOT NULL default '',
  `Custom2` varchar(255) NOT NULL default '',
  `Custom3` varchar(255) NOT NULL default '',
  `Custom4` varchar(255) NOT NULL default '',
  `Custom5` varchar(255) NOT NULL default '',
  `Custom6` varchar(255) NOT NULL default '',
  `Custom7` varchar(255) NOT NULL default '',
  `Custom8` varchar(255) NOT NULL default '',
  `Custom9` varchar(255) NOT NULL default '',
  `Custom10` varchar(255) NOT NULL default '',
  `Custom11` varchar(255) NOT NULL default '',
  `Custom12` varchar(255) NOT NULL default '',
  `Custom13` varchar(255) NOT NULL default '',
  `Custom14` varchar(255) NOT NULL default '',
  `Custom15` varchar(255) NOT NULL default '',
  `Custom16` varchar(255) NOT NULL default '',
  `Custom17` varchar(255) NOT NULL default '',
  `Custom18` varchar(255) NOT NULL default '',
  `Custom19` varchar(255) NOT NULL default '',
  `Custom20` varchar(255) NOT NULL default '',
  `Custom21` varchar(255) NOT NULL default '',
  `Custom22` varchar(255) NOT NULL default '',
  `Custom23` varchar(255) NOT NULL default '',
  `Custom24` varchar(255) NOT NULL default '',
  `Custom25` varchar(255) NOT NULL default '',
  `Custom26` varchar(255) NOT NULL default '',
  `Custom27` varchar(255) NOT NULL default '',
  `Custom28` varchar(255) NOT NULL default '',
  `Custom29` varchar(255) NOT NULL default '',
  `Custom30` varchar(255) NOT NULL default '',
  `Custom31` varchar(255) NOT NULL default '',
  `Custom32` varchar(255) NOT NULL default '',
  `Custom33` varchar(255) NOT NULL default '',
  `Custom34` varchar(255) NOT NULL default '',
  `Custom35` varchar(255) NOT NULL default '',
  `Custom36` varchar(255) NOT NULL default '',
  `Custom37` varchar(255) NOT NULL default '',
  `Custom38` varchar(255) NOT NULL default '',
  `Custom39` varchar(255) NOT NULL default '',
  `Custom40` varchar(255) NOT NULL default '',
  `Custom41` varchar(255) NOT NULL default '',
  `Custom42` varchar(255) NOT NULL default '',
  `Custom43` varchar(255) NOT NULL default '',
  `Custom44` varchar(255) NOT NULL default '',
  `Custom45` varchar(255) NOT NULL default '',
  `Custom46` varchar(255) NOT NULL default '',
  `Custom47` varchar(255) NOT NULL default '',
  `Custom48` varchar(255) NOT NULL default '',
  `Custom49` varchar(255) NOT NULL default '',
  `Custom50` varchar(255) NOT NULL default '',
  `Session` varchar(255) NOT NULL default '',
  `id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`id`),  
  UNIQUE KEY `Username` (`Username`)) CHARACTER SET utf8 COLLATE utf8_general_ci;";    
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create the table $DbTableName. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}
// Create log table
if (!tableexists($mysql_link, $DbLogTableName, "id"))
{
  $query="CREATE TABLE `$DbLogTableName` (
  `id` int(11) NOT NULL auto_increment,
  `time` datetime NOT NULL,
  `username` varchar(100) NOT NULL default '',
  `type` varchar(30) NOT NULL default '',
  `details` varchar(255) NOT NULL default '',
  `ip` varchar(46) NOT NULL default '',
  `session` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `username` (`username`),
  KEY `type` (`type`)) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create the table $DbLogTableName. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}

// create configuration table
if (!tableexists($mysql_link, $DbConfigTableName, "confignum"))
{
  $query="CREATE TABLE `$DbConfigTableName` (
  `confignum` int(11) NOT NULL default '1',
  `version` varchar(5) NOT NULL default '',
  `sitename` varchar(255) NOT NULL default 'Members Area',
  `siteemail` varchar(255) NOT NULL default 'you@yoursite.com',
  `dateformat` varchar(6) NOT NULL default 'DDMMYY',
  `logoutpage` varchar(255) NOT NULL default 'http://www.yoursite.com/index.html',
  `siteloklocation` varchar(255) NOT NULL default '',
  `siteloklocationurl` varchar(255) NOT NULL default '',
  `emaillocation` varchar(255) NOT NULL default '',
  `emailurl` varchar(255) NOT NULL default '',
  `filelocation` mediumtext,
  `siteloklog` varchar(255) NOT NULL default '',
  `logdetails` varchar(50) NOT NULL default 'YYYYYYYYYYYYYYYY',
  `sitekey` varchar(50) NOT NULL default '',
  `logintype` varchar(10) NOT NULL default 'NORMAL',
  `turinglogin` int(11) NOT NULL default '0',
  `turingregister` int(11) NOT NULL default '0',
  `maxsessiontime` int(11) NOT NULL default '0',
  `maxinactivitytime` int(11) NOT NULL default '0',
  `cookielogin` int(11) NOT NULL default '0',
  `logintemplate` varchar(255) NOT NULL default '',
  `expiredpage` varchar(255) NOT NULL default '',
  `wronggrouppage` varchar(255) NOT NULL default '',
  `messagetemplate` varchar(255) NOT NULL default '',
  `forgottenemail` varchar(255) NOT NULL default '',
  `newuseremail` varchar(255) NOT NULL default 'newuser.htm',
  `showrows` int(11) NOT NULL default '10',
  `randompasswordmask` varchar(50) NOT NULL default 'cccc##',
  `md5passwords` int(11) NOT NULL default '0',
  `concurrentlogin` int(11) NOT NULL default '1',
  `logviewoffset` int(11) NOT NULL default '0',
  `logvieworder` varchar(4) NOT NULL default 'ASC',
  `logviewdetails` varchar(50) NOT NULL default 'YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY',
  `sortfield` varchar(100) NOT NULL default '',
  `sortdirection` varchar(4) NOT NULL default '',
  `customtitle1` varchar(255) NOT NULL default 'Custom1',
  `customtitle2` varchar(255) NOT NULL default 'Custom2',
  `customtitle3` varchar(255) NOT NULL default 'Custom3',
  `customtitle4` varchar(255) NOT NULL default 'Custom4',
  `customtitle5` varchar(255) NOT NULL default 'Custom5',
  `customtitle6` varchar(255) NOT NULL default 'Custom6',
  `customtitle7` varchar(255) NOT NULL default 'Custom7',
  `customtitle8` varchar(255) NOT NULL default 'Custom8',
  `customtitle9` varchar(255) NOT NULL default 'Custom9',
  `customtitle10` varchar(255) NOT NULL default 'Custom10',
  `customtitle11` varchar(255) NOT NULL default '',
  `customtitle12` varchar(255) NOT NULL default '',
  `customtitle13` varchar(255) NOT NULL default '',
  `customtitle14` varchar(255) NOT NULL default '',
  `customtitle15` varchar(255) NOT NULL default '',
  `customtitle16` varchar(255) NOT NULL default '',
  `customtitle17` varchar(255) NOT NULL default '',
  `customtitle18` varchar(255) NOT NULL default '',
  `customtitle19` varchar(255) NOT NULL default '',
  `customtitle20` varchar(255) NOT NULL default '',
  `customtitle21` varchar(255) NOT NULL default '',
  `customtitle22` varchar(255) NOT NULL default '',
  `customtitle23` varchar(255) NOT NULL default '',
  `customtitle24` varchar(255) NOT NULL default '',
  `customtitle25` varchar(255) NOT NULL default '',
  `customtitle26` varchar(255) NOT NULL default '',
  `customtitle27` varchar(255) NOT NULL default '',
  `customtitle28` varchar(255) NOT NULL default '',
  `customtitle29` varchar(255) NOT NULL default '',
  `customtitle30` varchar(255) NOT NULL default '',
  `customtitle31` varchar(255) NOT NULL default '',
  `customtitle32` varchar(255) NOT NULL default '',
  `customtitle33` varchar(255) NOT NULL default '',
  `customtitle34` varchar(255) NOT NULL default '',
  `customtitle35` varchar(255) NOT NULL default '',
  `customtitle36` varchar(255) NOT NULL default '',
  `customtitle37` varchar(255) NOT NULL default '',
  `customtitle38` varchar(255) NOT NULL default '',
  `customtitle39` varchar(255) NOT NULL default '',
  `customtitle40` varchar(255) NOT NULL default '',
  `customtitle41` varchar(255) NOT NULL default '',
  `customtitle42` varchar(255) NOT NULL default '',
  `customtitle43` varchar(255) NOT NULL default '',
  `customtitle44` varchar(255) NOT NULL default '',
  `customtitle45` varchar(255) NOT NULL default '',
  `customtitle46` varchar(255) NOT NULL default '',
  `customtitle47` varchar(255) NOT NULL default '',
  `customtitle48` varchar(255) NOT NULL default '',
  `customtitle49` varchar(255) NOT NULL default '',
  `customtitle50` varchar(255) NOT NULL default '',
  `custom1validate` int(11) NOT NULL default '0',
  `custom2validate` int(11) NOT NULL default '0',
  `custom3validate` int(11) NOT NULL default '0',
  `custom4validate` int(11) NOT NULL default '0',
  `custom5validate` int(11) NOT NULL default '0',
  `custom6validate` int(11) NOT NULL default '0',
  `custom7validate` int(11) NOT NULL default '0',
  `custom8validate` int(11) NOT NULL default '0',
  `custom9validate` int(11) NOT NULL default '0',
  `custom10validate` int(11) NOT NULL default '0',
  `custom11validate` int(11) NOT NULL default '0',
  `custom12validate` int(11) NOT NULL default '0',
  `custom13validate` int(11) NOT NULL default '0',
  `custom14validate` int(11) NOT NULL default '0',
  `custom15validate` int(11) NOT NULL default '0',
  `custom16validate` int(11) NOT NULL default '0',
  `custom17validate` int(11) NOT NULL default '0',
  `custom18validate` int(11) NOT NULL default '0',
  `custom19validate` int(11) NOT NULL default '0',
  `custom20validate` int(11) NOT NULL default '0',
  `custom21validate` int(11) NOT NULL default '0',
  `custom22validate` int(11) NOT NULL default '0',
  `custom23validate` int(11) NOT NULL default '0',
  `custom24validate` int(11) NOT NULL default '0',
  `custom25validate` int(11) NOT NULL default '0',
  `custom26validate` int(11) NOT NULL default '0',
  `custom27validate` int(11) NOT NULL default '0',
  `custom28validate` int(11) NOT NULL default '0',
  `custom29validate` int(11) NOT NULL default '0',
  `custom30validate` int(11) NOT NULL default '0',
  `custom31validate` int(11) NOT NULL default '0',
  `custom32validate` int(11) NOT NULL default '0',
  `custom33validate` int(11) NOT NULL default '0',
  `custom34validate` int(11) NOT NULL default '0',
  `custom35validate` int(11) NOT NULL default '0',
  `custom36validate` int(11) NOT NULL default '0',
  `custom37validate` int(11) NOT NULL default '0',
  `custom38validate` int(11) NOT NULL default '0',
  `custom39validate` int(11) NOT NULL default '0',
  `custom40validate` int(11) NOT NULL default '0',
  `custom41validate` int(11) NOT NULL default '0',
  `custom42validate` int(11) NOT NULL default '0',
  `custom43validate` int(11) NOT NULL default '0',
  `custom44validate` int(11) NOT NULL default '0',
  `custom45validate` int(11) NOT NULL default '0',
  `custom46validate` int(11) NOT NULL default '0',
  `custom47validate` int(11) NOT NULL default '0',
  `custom48validate` int(11) NOT NULL default '0',
  `custom49validate` int(11) NOT NULL default '0',
  `custom50validate` int(11) NOT NULL default '0',
  `emailtype` int(11) NOT NULL default '0',
  `emailreplyto` varchar(255) NOT NULL default '',
  `emailusername` varchar(255) NOT NULL default '',
  `emailpassword` varchar(255) NOT NULL default '',
  `emailserver` varchar(255) NOT NULL default '',
  `emailport` varchar(6) NOT NULL default '25',  
  `emailauth` int(11) NOT NULL default '1',
  `emailserversecurity` varchar(6) NOT NULL default '',  
  `emaildelay` int(11) NOT NULL default '0',
  `modifyuseremail` varchar(255) NOT NULL default 'updateuser.htm', 
  `noaccesspage` varchar(255) NOT NULL default '', 
  `dbupdate` int(11) NOT NULL default '0',
  `siteemail2` varchar(255) NOT NULL default '', 
  `allowsearchengine` int(11) NOT NULL default '0',
  `searchenginegroup` varchar(255) NOT NULL default 'ALL', 
  `profilepassrequired` int(11) NOT NULL default '0',   
  `emailconfirmrequired` int(11) NOT NULL default '0',
  `emailconfirmtemplate` varchar(255) NOT NULL default '', 
  `emailunique` int(11) NOT NULL default '0',
  `loginwithemail` int(11) NOT NULL default '0',
  `columnorder` varchar(120) NOT NULL default '',
  `backuplocation` varchar(255) NOT NULL default '',
  `octoapikey` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`confignum`)) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create the table $DbConfigTableName. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}

// Create sl_adminconfig table
if (!tableexists($mysql_link, "sl_adminconfig", "confignum"))
{
  $query="CREATE TABLE `sl_adminconfig` (
  `confignum` int(11) NOT NULL DEFAULT '1',
  `sendnewuseremail` tinyint(4) NOT NULL DEFAULT '0',
  `sendedituseremail` tinyint(4) NOT NULL DEFAULT '0',
  `emaildedupe` tinyint(4) NOT NULL DEFAULT '1',
  `emaildeselect` tinyint(4) NOT NULL DEFAULT '1',
  `exportfilename` varchar(100) NOT NULL DEFAULT 'sitelokusers.csv',
  `exportuseheader` tinyint(4) NOT NULL DEFAULT '0',
  `exporttype` tinyint(4) NOT NULL DEFAULT '0',
  `exportfields` varchar(120) NOT NULL DEFAULT 'CRUSPWENNMEMUG0102030405060708091011121314151617181920212223242526272829303132333435363738394041424344454647484950',
  `importheader` tinyint(4) NOT NULL DEFAULT '0',
  `importuseemailas` varchar(15) NOT NULL DEFAULT 'emailnever',
  `importaddusers` tinyint(4) NOT NULL DEFAULT '1',
  `importrandpass` varchar(15) NOT NULL DEFAULT 'randnewonly',
  `importusergroups` text NOT NULL,
  `importslctadded` tinyint(4) NOT NULL DEFAULT '0',
  `importexistusers` tinyint(4) NOT NULL DEFAULT '1',
  `importblank` varchar(15) NOT NULL DEFAULT 'allowblank',
  `importslctexist` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY  (`confignum`)) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create the table sl_adminconfig. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}

// Create usergroups table
if (!tableexists($mysql_link, $DbGroupTableName, "name"))
{
  $query="CREATE TABLE `$DbGroupTableName` (
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL default '',
  `loginaction` varchar(255) NOT NULL default '',
  `loginvalue` varchar(255) NOT NULL default '',
  `id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create the table $DbGroupTableName. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}

// Create sl_ordercontrol table
if (!tableexists($mysql_link, "sl_ordercontrol", "orderno"))
{
  $query="CREATE TABLE `sl_ordercontrol` (
  `orderno` varchar(255) NOT NULL,
  `timest` int(11) NOT NULL default '0',
  `status` varchar(255) NOT NULL default '',
  `username` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`orderno`)) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create the table sl_ordercontrol. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}

// Create sl_logintemplate table
if (!tableexists($mysql_link, "sl_logintemplate", "id"))
{
  $query="CREATE TABLE `sl_logintemplate` (
  `id` int(11) NOT NULL  default 0,
  `backcolor` varchar(6) NOT NULL default '',
  `backimage` varchar(100) NOT NULL default '',
  `backimagerp` varchar(9) NOT NULL default '',
  `mainfont` varchar(100) NOT NULL default '',
  `boxcolortype` varchar(8) NOT NULL default '',
  `boxcolorfrom` varchar(6) NOT NULL default '',
  `boxcolorto` varchar(6) NOT NULL default '',
  `boxradius` tinyint(4) NOT NULL default 0,
  `boxshadow` tinyint(4) NOT NULL default 0,
  `title` varchar(100) NOT NULL default '',
  `titlecolor` varchar(6) NOT NULL default '',
  `titlesize` tinyint(4) NOT NULL default 0,
  `titlealign` varchar(6) NOT NULL default '',
  `titlefont` varchar(100) NOT NULL default '',
  `msgcolor` varchar(6) NOT NULL default '',
  `msgsize` tinyint(4) NOT NULL default 0,
  `msgalign` varchar(6) NOT NULL default '',
  `username` varchar(50) NOT NULL default '',
  `password` varchar(50) NOT NULL default '',
  `captcha` varchar(50) NOT NULL default '',
  `remember` varchar(50) NOT NULL default '',
  `autologin` varchar(50) NOT NULL default '',
  `labelcolor` varchar(6) NOT NULL default '',
  `labelsize` tinyint(4) NOT NULL default 0,
  `inputcolor` varchar(6) NOT NULL default '',
  `inputsize` tinyint(4) NOT NULL default 0,
  `inputbackcolor` varchar(6) NOT NULL default '',
  `showicons` tinyint(4) NOT NULL default 0,
  `btnlbltext` varchar(50) NOT NULL default '',
  `btnlblfont` varchar(255) NOT NULL DEFAULT 'Arial, Helvetica, sans-serif',
  `btnlblcolor` varchar(6) NOT NULL default '',
  `btnlblsize` tinyint(4) NOT NULL default 0,
  `btnlblstyle` varchar(25) NOT NULL DEFAULT 'normal normal bold',
  `btncolortype` varchar(8) NOT NULL default '',
  `btncolorfrom` varchar(6) NOT NULL default '',
  `btncolorto` varchar(6) NOT NULL default '',
  `btnradius` tinyint(4) NOT NULL default 0,
  `btnbordercolor` varchar(6) NOT NULL DEFAULT 'FFFFFF',
  `btnbordersize` tinyint(4) NOT NULL DEFAULT '0',
  `btnborderstyle` varchar(8) NOT NULL DEFAULT 'solid',
  `forgottxt` varchar(50) NOT NULL default '',
  `forgotcolor` varchar(6) NOT NULL default '',
  `forgotsize` tinyint(4) NOT NULL default 0,
  `signuptext` varchar(100) NOT NULL default '',
  `signupurl` varchar(100) NOT NULL default '',
  `signupcolor` varchar(6) NOT NULL default '',
  `signupsize` tinyint(4) NOT NULL default 0,
  `signupalign` varchar(6) NOT NULL default '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create the table sl_logintemplate. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}

// Create sl_forms table
if (!tableexists($mysql_link, "sl_forms", "id"))
{
  $query="CREATE TABLE `sl_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL default '',
  `type` varchar(20) CHARACTER SET utf8 NOT NULL default '',
  PRIMARY KEY (`id`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create the table sl_forms. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
  // Add example tables (only do this if we create the table to not overwrite customies forms
  $query ="INSERT INTO `sl_forms` VALUES(1, 'Example for register.php', 'register'),";
  $query.="(2, 'Example for registerapprove.php', 'register'),";
  $query.="(3, 'Example for registerturing.php', 'register'),";
  $query.="(4, 'Example for update.php', 'update'),";
  $query.="(5, 'Example for pagewithlogin.php', 'login'),";
  $query.="(6, 'Example for contactpage.php', 'contact');";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create example tables in sl_forms. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
  $query="ALTER TABLE `sl_forms` AUTO_INCREMENT=10;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not set auto increment in the table 'sl_forms'. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }    
}

// Create sl_registerforms table
if (!tableexists($mysql_link, "sl_registerforms", "id"))
{
  $query="CREATE TABLE `sl_registerforms` (
  `id` int(11) NOT NULL default 0,
  `position` tinyint(4) NOT NULL default 0,
  `fonttype` varchar(255) NOT NULL default '',
  `labelcolor` varchar(6) NOT NULL default '',
  `labelsize` tinyint(4) NOT NULL default 0,
  `labelstyle` varchar(25) NOT NULL default '',
  `inputtextcolor` varchar(6) NOT NULL default '',
  `inputtextsize` tinyint(4) NOT NULL  default 0,
  `inputtextstyle` varchar(25) NOT NULL default '',
  `inputbackcolor` varchar(6) NOT NULL default '',
  `bordersize` tinyint(4) NOT NULL default 0,
  `bordercolor` varchar(6) NOT NULL default '',
  `borderradius` tinyint(4) NOT NULL default 0,
  `inputpaddingv` varchar(5) NOT NULL default '',
  `inputpaddingh` varchar(5) NOT NULL default '',
  `rqdfieldlabel` varchar(255) NOT NULL default '',
  `rqdfieldcolor` varchar(6) NOT NULL default '',
  `rqdfieldsize` tinyint(4) NOT NULL default 0,
  `rqdfieldstyle` varchar(25) NOT NULL default '',
  `messagecolor` varchar(6) NOT NULL default '',
  `messagesize` tinyint(4) NOT NULL default 0,
  `messagestyle` varchar(25) NOT NULL default '',
  `usergroup` varchar(255) NOT NULL default '',
  `expiry` varchar(255) NOT NULL default '',
  `redirect` varchar(255) NOT NULL default '',
  `useremail` varchar(255) NOT NULL default '',
  `adminemail` varchar(255) NOT NULL default '',
  `enabled` varchar(3) NOT NULL default '',
  `sitelokfield` varchar(10) NOT NULL default '',
  `inputtype` varchar(10) NOT NULL default '',
  `labeltext` varchar(255) NOT NULL default '',
  `placetext` varchar(255) NOT NULL default '',
  `validation` varchar(20) NOT NULL default '',
  `errormsg` varchar(255) NOT NULL default '',
  `fieldwidth` tinyint(4) NOT NULL default 0,
  `value` text NOT NULL,
  `checked` tinyint(4) NOT NULL default 0,
  `bottommargin` tinyint(4) NOT NULL default 0,
  `showrequired` varchar(1) NOT NULL default '',
  `btncolortype` varchar(8) NOT NULL default '',
  `btncolorfrom` varchar(6) NOT NULL default '',
  `btncolorto` varchar(6) NOT NULL default '',
  `btnradius` tinyint(4) NOT NULL default 0,
  `btnlabel` varchar(100) NOT NULL default '',
  `btnlabelcolor` varchar(6) NOT NULL default '',
  `btnlabelsize` tinyint(4) NOT NULL default 0,
  `btnlabelfont` varchar(255) NOT NULL default '',
  `btnlabelstyle` varchar(25) NOT NULL default '',
  `btnbordercolor` varchar(6) NOT NULL DEFAULT '',
  `btnbordersize` tinyint(4) NOT NULL DEFAULT '0',
  `btnborderstyle` varchar(8) NOT NULL DEFAULT '',
  `btnpaddingv` tinyint(4) NOT NULL default 0,
  `btnpaddingh` tinyint(4) NOT NULL default 0,
  `formerrormsg` varchar(255) NOT NULL default '',
  `formerrormsgcolor` varchar(6) NOT NULL default '',
  `formerrormsgsize` tinyint(4) NOT NULL default 0,
  `formerrormsgstyle` varchar(25) NOT NULL default '',
  `maxformwidth` int(11) NOT NULL default 0,
  `backcolor` varchar(6) NOT NULL default '',
  UNIQUE KEY `id` (`id`,`position`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create the table sl_registerforms. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
  // Add example tables (only do this if we create the table to not overwrite customies forms
$query= <<<EOT

INSERT INTO `sl_registerforms` (`id`, `position`, `fonttype`, `labelcolor`, `labelsize`, `labelstyle`, `inputtextcolor`, `inputtextsize`, `inputtextstyle`, `inputbackcolor`, `bordersize`, `bordercolor`, `borderradius`, `inputpaddingv`, `inputpaddingh`, `rqdfieldlabel`, `rqdfieldcolor`, `rqdfieldsize`, `rqdfieldstyle`, `messagecolor`, `messagesize`, `messagestyle`, `usergroup`, `expiry`, `redirect`, `useremail`, `adminemail`, `enabled`, `sitelokfield`, `inputtype`, `labeltext`, `placetext`, `validation`, `errormsg`, `fieldwidth`, `value`, `checked`, `bottommargin`, `showrequired`, `btncolortype`, `btncolorfrom`, `btncolorto`, `btnradius`, `btnlabel`, `btnlabelcolor`, `btnlabelsize`, `btnlabelfont`, `btnlabelstyle`, `btnbordercolor`, `btnbordersize`, `btnborderstyle`, `btnpaddingv`, `btnpaddingh`, `formerrormsg`, `formerrormsgcolor`, `formerrormsgsize`, `formerrormsgstyle`, `maxformwidth`, `backcolor`) VALUES
(1, 0, 'Arial, Helvetica, sans-serif', '1A305E', 16, 'normal normal bold', '1A305E', 14, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '0.3em', '0.3em', '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', 'CLIENT', '0', '/members/registerthanks.php', 'newuser.htm', 'newuseradmin.htm', 'Yes', '', '', '', '', '', '', 0, '', 0, 0, '', 'gradient', '1A305E', '378EE5', 10, 'Register', 'FFFFFF', 14, 'Arial, Helvetica, sans-serif', 'normal normal bold', 'FFFFFF', 0, 'solid', 6, 24, 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 300, 'FFFFFF'),
(1, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'name', 'text', 'Full name', '', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(1, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'email', 'text', 'Email', '', 'required', 'Please enter your email', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(1, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom1', 'text', 'What type of camera do you use', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(1, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom2', 'text', 'Where did you hear about us', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(2, 0, 'Arial, Helvetica, sans-serif', '1A305E', 16, 'normal normal bold', '1A305E', 14, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '0.3em', '0.3em', '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', 'CLIENT', '0', '/members/registerthanks.php', 'pending.htm', 'pendingadmin.htm', 'No', '', '', '', '', '', '', 0, '', 0, 0, '', 'gradient', '1A305E', '378EE5', 10, 'Register', 'FFFFFF', 14, 'Arial, Helvetica, sans-serif', 'normal normal bold', 'FFFFFF', 0, 'solid', 6, 24, 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 300, 'FFFFFF'),
(2, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'name', 'text', 'Full name', '', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(2, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'email', 'text', 'Email', '', 'required', 'Please enter your email', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(2, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom1', 'text', 'What type of camera do you use', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(2, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom2', 'text', 'Where did you hear about us', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(3, 0, 'Arial, Helvetica, sans-serif', '1A305E', 16, 'normal normal bold', '1A305E', 14, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '0.3em', '0.3em', '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', 'CLIENT', '0', '/members/registerthanks.php', 'newuser.htm', 'newuseradmin.htm', 'Yes', '', '', '', '', '', '', 0, '', 0, 0, '', 'gradient', '1A305E', '378EE5', 10, 'Register', 'FFFFFF', 14, 'Arial, Helvetica, sans-serif', 'normal normal bold', 'FFFFFF', 0, 'solid', 6, 24, 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 300, 'FFFFFF'),
(3, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'name', 'text', 'Full name', '', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(3, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'email', 'text', 'Email', '', 'required', 'Please enter your email', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(3, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom1', 'text', 'What type of camera do you use', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(3, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom2', 'text', 'Where did you hear about us', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(3, 5, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'captcha', 'captcha', 'Captcha', '', 'required', 'Please enter the letters shown', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, '');

EOT;

  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create example tables in sl_registerforms. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }  
}

// Create sl_updateforms table
if (!tableexists($mysql_link, "sl_updateforms", "id"))
{
  $query="CREATE TABLE `sl_updateforms` (
  `id` int(11) NOT NULL,
  `position` tinyint(4) NOT NULL default 0,
  `fonttype` varchar(255) NOT NULL default '',
  `labelcolor` varchar(6) NOT NULL default '',
  `labelsize` tinyint(4) NOT NULL default 0,
  `labelstyle` varchar(25) NOT NULL default '',
  `inputtextcolor` varchar(6) NOT NULL default '',
  `inputtextsize` tinyint(4) NOT NULL default 0,
  `inputtextstyle` varchar(25) NOT NULL default '',
  `inputbackcolor` varchar(6) NOT NULL default '',
  `bordersize` tinyint(4) NOT NULL default 0,
  `bordercolor` varchar(6) NOT NULL default '',
  `borderradius` tinyint(4) NOT NULL default 0,
  `inputpaddingv` varchar(5) NOT NULL default '',
  `inputpaddingh` varchar(5) NOT NULL default '',
  `rqdfieldlabel` varchar(255) NOT NULL default '',
  `rqdfieldcolor` varchar(6) NOT NULL default '',
  `rqdfieldsize` tinyint(4) NOT NULL default 0,
  `rqdfieldstyle` varchar(25) NOT NULL default '',
  `messagecolor` varchar(6) NOT NULL default '',
  `messagesize` tinyint(4) NOT NULL default 0,
  `messagestyle` varchar(25) NOT NULL default '',
  `redirect` varchar(255) NOT NULL default '',
  `useremail` varchar(255) NOT NULL default '',
  `adminemail` varchar(255) NOT NULL default '',
  `sitelokfield` varchar(10) NOT NULL default '',
  `inputtype` varchar(10) NOT NULL default '',
  `labeltext` varchar(255) NOT NULL default '',
  `placetext` varchar(255) NOT NULL default '',
  `validation` varchar(20) NOT NULL default '',
  `errormsg` varchar(255) NOT NULL default '',
  `fieldwidth` tinyint(4) NOT NULL default 0,
  `value` text NOT NULL,
  `emailaction` tinyint(4) NOT NULL default 0,
  `bottommargin` tinyint(4) NOT NULL default 0,
  `showrequired` varchar(1) NOT NULL default '',
  `btncolortype` varchar(8) NOT NULL default '',
  `btncolorfrom` varchar(6) NOT NULL default '',
  `btncolorto` varchar(6) NOT NULL default '',
  `btnradius` tinyint(4) NOT NULL default 0,
  `btnlabel` varchar(100) NOT NULL default '',
  `btnlabelcolor` varchar(6) NOT NULL default '',
  `btnlabelsize` tinyint(4) NOT NULL default 0,
  `btnlabelfont` varchar(255) NOT NULL default '',
  `btnlabelstyle` varchar(25) NOT NULL default '',
  `btnbordercolor` varchar(6) NOT NULL DEFAULT '',
  `btnbordersize` tinyint(4) NOT NULL DEFAULT '0',
  `btnborderstyle` varchar(8) NOT NULL DEFAULT '',
  `btnpaddingv` tinyint(4) NOT NULL default 0,
  `btnpaddingh` tinyint(4) NOT NULL default 0,
  `formerrormsg` varchar(255) NOT NULL default '',
  `formerrormsgcolor` varchar(6) NOT NULL default '',
  `formerrormsgsize` tinyint(4) NOT NULL default 0,
  `formerrormsgstyle` varchar(25) NOT NULL default '',
  `maxformwidth` int(11) NOT NULL default 0,
  `backcolor` varchar(6) NOT NULL default '',
  UNIQUE KEY `id` (`id`,`position`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create the table sl_updateforms. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
  // Add example tables (only do this if we create the table to not overwrite customised forms
$query= <<<EOT
  
INSERT INTO `sl_updateforms` (`id`, `position`, `fonttype`, `labelcolor`, `labelsize`, `labelstyle`, `inputtextcolor`, `inputtextsize`, `inputtextstyle`, `inputbackcolor`, `bordersize`, `bordercolor`, `borderradius`, `inputpaddingv`, `inputpaddingh`, `rqdfieldlabel`, `rqdfieldcolor`, `rqdfieldsize`, `rqdfieldstyle`, `messagecolor`, `messagesize`, `messagestyle`, `redirect`, `useremail`, `adminemail`, `sitelokfield`, `inputtype`, `labeltext`, `placetext`, `validation`, `errormsg`, `fieldwidth`, `value`, `emailaction`, `bottommargin`, `showrequired`, `btncolortype`, `btncolorfrom`, `btncolorto`, `btnradius`, `btnlabel`, `btnlabelcolor`, `btnlabelsize`, `btnlabelfont`, `btnlabelstyle`, `btnbordercolor`, `btnbordersize`, `btnborderstyle`, `btnpaddingv`, `btnpaddingh`, `formerrormsg`, `formerrormsgcolor`, `formerrormsgsize`, `formerrormsgstyle`, `maxformwidth`, `backcolor`) VALUES
(4, 0, 'Arial, Helvetica, sans-serif', '1A305E', 14, 'normal normal bold', '1A305E', 16, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '0.3em', '0.3em', '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', '', 'updateuser.htm', 'updateuseradmin.htm', '', '', '', '', '', '', 0, '', 0, 0, '', 'gradient', '1A305E', '378EE5', 10, 'Update', 'FFFFFF', 14, 'Arial, Helvetica, sans-serif', 'normal normal bold', 'FFFFFF', 0, 'solid', 6, 24, 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 400, 'FFFFFF'),
(4, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', 'password', 'password', 'New password (leave blank to keep the same)', '', 'notrequired', '', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(4, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', 'vpassword', 'password', 'Type password again', '', 'required', 'The passwords don\'t match', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(4, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', 'name', 'text', 'Name', '', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(4, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', 'email', 'text', 'Email', '', 'required', 'Please enter your email', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(4, 5, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', 'custom1', 'text', 'What type of camera do you use', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(4, 6, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', 'custom2', 'text', 'Where did you hear about us', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, '');

EOT;

  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create example tables in sl_updateforms. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }    
}

// Create sl_loginforms table
if (!tableexists($mysql_link, "sl_loginforms", "id"))
{
  $query="CREATE TABLE `sl_loginforms` (
  `id` int(11) NOT NULL,
  `mainfont` varchar(255) NOT NULL default '',
  `messagecolor` varchar(6) NOT NULL default '',
  `messagesize` tinyint(4) NOT NULL default 0,
  `messagestyle` varchar(25) NOT NULL default '',
  `usernamelabel` varchar(255) NOT NULL default '',
  `usernameplaceholder` varchar(255) NOT NULL,
  `passwordlabel` varchar(255) NOT NULL default '',
  `passwordplaceholder` varchar(255) NOT NULL,
  `includecaptcha` varchar(1) NOT NULL default '',
  `captchalabel` varchar(255) NOT NULL default '',
  `captchaplaceholder` varchar(255) NOT NULL,
  `includeremember` varchar(1) NOT NULL default '',
  `rememberlabel` varchar(255) NOT NULL default '',
  `includeautologin` varchar(1) NOT NULL default '',
  `autologinlabel` varchar(255) NOT NULL default '',
  `labelcolor` varchar(6) NOT NULL default '',
  `labelsize` tinyint(4) NOT NULL default 0,
  `labelstyle` varchar(25) NOT NULL default '',
  `inputtextcolor` varchar(6) NOT NULL default '',
  `inputtextsize` tinyint(4) NOT NULL default 0,
  `inputtextstyle` varchar(25) NOT NULL default '',
  `inputbackcolor` varchar(6) NOT NULL default '',
  `bordersize` tinyint(4) NOT NULL default 0,
  `bordercolor` varchar(6) NOT NULL default '',
  `borderradius` tinyint(4) NOT NULL default 0,
  `inputpaddingv` varchar(5) NOT NULL default '',
  `inputpaddingh` varchar(5) NOT NULL default '',
  `logintext` varchar(100) NOT NULL default '',
  `logintextfont` varchar(255) NOT NULL DEFAULT 'Arial, Helvetica, sans-serif',
  `logintextstyle` varchar(25) NOT NULL DEFAULT 'normal normal bold',
  `logintextcolor` varchar(6) NOT NULL default '',
  `logintextsize` tinyint(4) NOT NULL default 0,
  `loginfilltype` varchar(8) NOT NULL default '',
  `logincolorfrom` varchar(6) NOT NULL default '',
  `logincolorto` varchar(6) NOT NULL default '',
  `loginshape` tinyint(4) NOT NULL default 0,
  `btnbordercolor` varchar(6) NOT NULL DEFAULT 'FFFFFF',
  `btnbordersize` tinyint(4) NOT NULL DEFAULT '0',
  `btnborderstyle` varchar(8) NOT NULL DEFAULT 'solid',
  `btnpaddingv` tinyint(4) NOT NULL default 0,
  `btnpaddingh` tinyint(4) NOT NULL default 0,
  `includeforgot` varchar(1) NOT NULL default '',
  `forgottext` varchar(255) NOT NULL default '',
  `forgotcolor` varchar(6) NOT NULL default '',
  `forgotsize` tinyint(4) NOT NULL default 0,
  `forgotstyle` varchar(25) NOT NULL default '',
  `includesignup` varchar(1) NOT NULL default '',
  `signuptext` varchar(255) NOT NULL default '',
  `signupurl` varchar(255) NOT NULL default '',
  `signupcolor` varchar(6) NOT NULL default '',
  `signupsize` tinyint(4) NOT NULL default 0,
  `signupstyle` varchar(25) NOT NULL default '',
  `bottommargin` tinyint(4) NOT NULL default 0,
  `maxformwidth` int(11) NOT NULL default 0,
  `backgroundcolor` varchar(6) NOT NULL default '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)  
) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create the table sl_loginforms. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
  // Add example tables (only do this if we create the table to not overwrite customies forms
$query= <<<EOT
  
INSERT INTO `sl_loginforms` (`id`, `mainfont`, `messagecolor`, `messagesize`, `messagestyle`, `usernamelabel`, `usernameplaceholder`, `passwordlabel`, `passwordplaceholder`, `includecaptcha`, `captchalabel`, `captchaplaceholder`, `includeremember`, `rememberlabel`, `includeautologin`, `autologinlabel`, `labelcolor`, `labelsize`, `labelstyle`, `inputtextcolor`, `inputtextsize`, `inputtextstyle`, `inputbackcolor`, `bordersize`, `bordercolor`, `borderradius`, `inputpaddingv`, `inputpaddingh`, `logintext`, `logintextfont`, `logintextstyle`, `logintextcolor`, `logintextsize`, `loginfilltype`, `logincolorfrom`, `logincolorto`, `loginshape`, `btnbordercolor`, `btnbordersize`, `btnborderstyle`, `btnpaddingv`, `btnpaddingh`, `includeforgot`, `forgottext`, `forgotcolor`, `forgotsize`, `forgotstyle`, `includesignup`, `signuptext`, `signupurl`, `signupcolor`, `signupsize`, `signupstyle`, `bottommargin`, `maxformwidth`, `backgroundcolor`) VALUES
(5, 'Arial, Helvetica, sans-serif', 'FF0000', 12, 'normal normal bold', 'Username', '', 'Password', '', '0', 'Captcha', 'captcha', '0', 'Remember me', '0', 'Auto Login', '1A305E', 16, 'normal normal normal', '1A305E', 14, 'normal normal normal', 'FFFFFF', 1, '378EE5', 5, '0.3em', '0.3em', 'Login', 'Arial, Helvetica, sans-serif', 'normal normal bold', 'FFFFFF', 14, 'gradient', '1A305E', '378EE5', 10, 'FFFFFF', 0, 'solid', 6, 24, '1', 'Forgotten?', '1A305E', 14, 'normal normal normal', '0', 'Not signed up yet?', '/register.php', '000000', 12, 'normal normal normal', 15, 200, 'FFFFFF');

EOT;

  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create example tables in sl_loginforms. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }    
}

// Create sl_contactforms table
if (!tableexists($mysql_link, "sl_contactforms", "id"))
{
  $query="CREATE TABLE `sl_contactforms` (
  `id` int(11) NOT NULL DEFAULT '0',
  `position` tinyint(4) NOT NULL DEFAULT '0',
  `fonttype` varchar(255) NOT NULL DEFAULT '',
  `labelcolor` varchar(6) NOT NULL DEFAULT '',
  `labelsize` tinyint(4) NOT NULL DEFAULT '0',
  `labelstyle` varchar(25) NOT NULL DEFAULT '',
  `inputtextcolor` varchar(6) NOT NULL DEFAULT '',
  `inputtextsize` tinyint(4) NOT NULL DEFAULT '0',
  `inputtextstyle` varchar(25) NOT NULL DEFAULT '',
  `inputbackcolor` varchar(6) NOT NULL DEFAULT '',
  `bordersize` tinyint(4) NOT NULL DEFAULT '0',
  `bordercolor` varchar(6) NOT NULL DEFAULT '',
  `borderradius` tinyint(4) NOT NULL DEFAULT '0',
  `inputpaddingv` varchar(5) NOT NULL default '',
  `inputpaddingh` varchar(5) NOT NULL default '',
  `rqdfieldlabel` varchar(255) NOT NULL DEFAULT '',
  `rqdfieldcolor` varchar(6) NOT NULL DEFAULT '',
  `rqdfieldsize` tinyint(4) NOT NULL DEFAULT '0',
  `rqdfieldstyle` varchar(25) NOT NULL DEFAULT '',
  `messagecolor` varchar(6) NOT NULL DEFAULT '',
  `messagesize` tinyint(4) NOT NULL DEFAULT '0',
  `messagestyle` varchar(25) NOT NULL DEFAULT '',
  `sendemail` varchar(255) NOT NULL DEFAULT '',
  `redirect` varchar(255) NOT NULL DEFAULT '',
  `useremailvisitor` varchar(255) NOT NULL DEFAULT '',
  `adminemailvisitor` varchar(255) NOT NULL DEFAULT '',
  `useremailmember` varchar(255) NOT NULL DEFAULT '',
  `adminemailmember` varchar(255) NOT NULL DEFAULT '',
  `fromnameuser` varchar(255) NOT NULL DEFAULT '',
  `replytouser` varchar(255) NOT NULL DEFAULT '',
  `attachmenttypes` varchar(255) NOT NULL DEFAULT '',
  `sendasuser` varchar(1) NOT NULL DEFAULT '',
  `attachmentsize` int(11) NOT NULL,
  `sitelokfield` varchar(10) NOT NULL DEFAULT '',
  `inputtype` varchar(10) NOT NULL DEFAULT '',
  `labeltext` varchar(255) NOT NULL DEFAULT '',
  `placetext` varchar(255) NOT NULL DEFAULT '',
  `validation` varchar(20) NOT NULL DEFAULT '',
  `errormsg` varchar(255) NOT NULL DEFAULT '',
  `fieldwidth` tinyint(4) NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  `checked` tinyint(4) NOT NULL DEFAULT '0',
  `bottommargin` tinyint(4) NOT NULL DEFAULT '0',
  `showrequired` varchar(1) NOT NULL DEFAULT '',
  `useas` varchar(1) NOT NULL DEFAULT '',
  `showfieldfor` varchar(1) NOT NULL DEFAULT '',
  `btncolortype` varchar(8) NOT NULL DEFAULT '',
  `btncolorfrom` varchar(6) NOT NULL DEFAULT '',
  `btncolorto` varchar(6) NOT NULL DEFAULT '',
  `btnradius` tinyint(4) NOT NULL DEFAULT '0',
  `btnlabel` varchar(100) NOT NULL DEFAULT '',
  `btnlabelcolor` varchar(6) NOT NULL DEFAULT '',
  `btnlabelsize` tinyint(4) NOT NULL DEFAULT '0',
  `btnlabelfont` varchar(255) NOT NULL DEFAULT '',
  `btnlabelstyle` varchar(25) NOT NULL DEFAULT '',
  `btnbordercolor` varchar(6) NOT NULL DEFAULT '',
  `btnbordersize` tinyint(4) NOT NULL DEFAULT '0',
  `btnborderstyle` varchar(8) NOT NULL DEFAULT '',
  `btnpaddingv` tinyint(4) NOT NULL default 0,
  `btnpaddingh` tinyint(4) NOT NULL default 0,
  `formerrormsg` varchar(255) NOT NULL DEFAULT '',
  `formerrormsgcolor` varchar(6) NOT NULL DEFAULT '',
  `formerrormsgsize` tinyint(4) NOT NULL DEFAULT '0',
  `formerrormsgstyle` varchar(25) NOT NULL DEFAULT '',
  `maxformwidth` int(11) NOT NULL DEFAULT '0',
  `backcolor` varchar(6) NOT NULL DEFAULT '',
  UNIQUE KEY `id` (`id`,`position`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create the table sl_contactforms. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
  // Add example tables (only do this if we create the table to not overwrite customies forms
$query= <<<EOT
  
INSERT INTO `sl_contactforms` (`id`, `position`, `fonttype`, `labelcolor`, `labelsize`, `labelstyle`, `inputtextcolor`, `inputtextsize`, `inputtextstyle`, `inputbackcolor`, `bordersize`, `bordercolor`, `borderradius`, `inputpaddingv`, `inputpaddingh`, `rqdfieldlabel`, `rqdfieldcolor`, `rqdfieldsize`, `rqdfieldstyle`, `messagecolor`, `messagesize`, `messagestyle`, `sendemail`, `redirect`, `useremailvisitor`, `adminemailvisitor`, `useremailmember`, `adminemailmember`, `fromnameuser`, `replytouser`, `attachmenttypes`, `sendasuser`, `attachmentsize`, `sitelokfield`, `inputtype`, `labeltext`, `placetext`, `validation`, `errormsg`, `fieldwidth`, `value`, `checked`, `bottommargin`, `showrequired`, `useas`, `showfieldfor`, `btncolortype`, `btncolorfrom`, `btncolorto`, `btnradius`, `btnlabel`, `btnlabelcolor`, `btnlabelsize`, `btnlabelfont`, `btnlabelstyle`, `btnbordercolor`, `btnbordersize`, `btnborderstyle`, `btnpaddingv`, `btnpaddingh`, `formerrormsg`, `formerrormsgcolor`, `formerrormsgsize`, `formerrormsgstyle`, `maxformwidth`, `backcolor`) VALUES
(6, 0, 'Arial, Helvetica, sans-serif', '1A305E', 18, 'normal normal bold', '1A305E', 16, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '0.3em', '0.3em', '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', '', 'contactthanks.php', 'contactemailuser.htm', 'contactemailadmin.htm', 'contactemailuser.htm', 'contactemailadmin.htm', '', '', '.jpg .gif .png .zip .txt .pdf', '0', 2000000, '', '', '', '', '', '', 0, '', 0, 0, '', '', '', 'gradient', '1A305E', '378EE5', 10, 'Send', 'FFFFFF', 14, 'Arial, Helvetica, sans-serif', 'normal normal bold', 'FFFFFF', 0, 'solid', 6, 24, 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 300, 'FFFFFF'),
(6, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, 'fullname', 'text', 'Full name', 'full name', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '2', '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(6, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, 'email', 'text', 'Email', 'email', 'requiredemail', 'Please enter your email', 100, '', 0, 20, '1', '1', '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(6, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, '', 'textarea', 'Message', 'message', 'required', 'Please enter your message', 100, '', 0, 20, '1', '', '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, ''),
(6, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', '', '', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, '', 'captcha', 'Captcha', 'captcha', 'required', 'Please enter the captcha code', 100, '', 0, 20, '1', '', '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, 0, '', '', 0, '', 0, '');

EOT;

  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not create example tables in sl_contactforms. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }  
}


// Insert default default admin user into table if entry doesn't already exist
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." WHERE username='admin'");
if ($mysql_result==false)
{
  $err=mysqli_error($mysql_link);
  mysqli_close($mysql_link);
  print "Could not query the table $DbTableName. The following error was reported by MySql<br><br>\n";
  print("$err");
  print "</body>\n";
  print "</html>\n";
  exit;
}
if (mysqli_num_rows($mysql_result)==0)
{
  $query="INSERT INTO `$DbTableName` (
`Selected` ,
`Created` ,
`Username` ,
`Passphrase` ,
`Enabled` ,
`Name` ,
`Email` ,
`Usergroups` ,
`Custom1` ,
`Custom2` ,
`Custom3` ,
`Custom4` ,
`Custom5` ,
`Custom6` ,
`Custom7` ,
`Custom8` ,
`Custom9` ,
`Custom10` ,
`Custom11` ,
`Custom12` ,
`Custom13` ,
`Custom14` ,
`Custom15` ,
`Custom16` ,
`Custom17` ,
`Custom18` ,
`Custom19` ,
`Custom20` ,
`Custom21` ,
`Custom22` ,
`Custom23` ,
`Custom24` ,
`Custom25` ,
`Custom26` ,
`Custom27` ,
`Custom28` ,
`Custom29` ,
`Custom30` ,
`Custom31` ,
`Custom32` ,
`Custom33` ,
`Custom34` ,
`Custom35` ,
`Custom36` ,
`Custom37` ,
`Custom38` ,
`Custom39` ,
`Custom40` ,
`Custom41` ,
`Custom42` ,
`Custom43` ,
`Custom44` ,
`Custom45` ,
`Custom46` ,
`Custom47` ,
`Custom48` ,
`Custom49` ,
`Custom50` ,
`Session`
)
VALUES (
'No',  '010101',  'admin',  'letmein',  'Yes',  'Administrator',  'you@yoursite.com',  'ADMIN', '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , ''
)";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not insert data in table $DbTableName. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}

// Insert default groups in to table
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbGroupTableName." WHERE name='ADMIN'");
if ($mysql_result==false)
{
  $err=mysqli_error($mysql_link);
  mysqli_close($mysql_link);
  print "Could not query the table $DbGroupTableName. The following error was reported by MySql<br><br>\n";
  print("$err");
  print "</body>\n";
  print "</html>\n";
  exit;
}
if (mysqli_num_rows($mysql_result)==0)
{
  $tmppurl=parse_url($slpwurl);
  $query="INSERT INTO `$DbGroupTableName` (
`name` ,
`description` ,
`loginaction` ,
`loginvalue`
)
VALUES (
'ADMIN',  'Administrator', 'URL' , '".$tmppurl[path]."index.php'
)";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not insert data in table $DbGroupTableName. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}

$mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbGroupTableName." WHERE name='ALL'");
if ($mysql_result==false)
{
  $err=mysqli_error($mysql_link);
  mysqli_close($mysql_link);
  print "Could not query the table $DbGroupTableName. The following error was reported by MySql<br><br>\n";
  print("$err");
  print "</body>\n";
  print "</html>\n";
  exit;
}
if (mysqli_num_rows($mysql_result)==0)
{
  $query="INSERT INTO `$DbGroupTableName` (
`name` ,
`description` ,
`loginaction` ,
`loginvalue`
)
VALUES (
'ALL',  'All Areas', '' , ''
)";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not insert data in table $DbGroupTableName. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}

// CLIENT group is setup for the example members pages
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbGroupTableName." WHERE name='CLIENT'");
if ($mysql_result==false)
{
  $err=mysqli_error($mysql_link);
  mysqli_close($mysql_link);
  print "Could not query the table $DbGroupTableName. The following error was reported by MySql<br><br>\n";
  print("$err");
  print "</body>\n";
  print "</html>\n";
  exit;
}
if (mysqli_num_rows($mysql_result)==0)
{
  $tmppurl=parse_url($slpwurl);
  $query="INSERT INTO `$DbGroupTableName` (
`name` ,
`description` ,
`loginaction` ,
`loginvalue`
)
VALUES (
'CLIENT',  'Client', 'URL' , '/members/members.php'
)";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not insert data in table $DbGroupTableName. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}


// Determine settings for configuration table
$install_version=$installversion;

$install_sitename="Members Area";

$install_siteemail="you@yoursite.com";

$install_siteemail2="";

$install_dateformat="DDMMYY";

$pos=strpos($slpwurl,"/",8);
if (is_integer($pos)) 
  $install_logoutpage=substr($slpwurl,0,$pos);

$install_siteloklocation=$slpwpath;

$install_siteloklocationurl=$slpwurl;

$install_emaillocation=$slpwpath."email/";

$install_emailurl=$slpwurl."email/";

$install_emailthumbslocation=$slpwpath."emailthumbs/";

$install_writefileslocation=$slpwpath."writefiles/";

$validchars  = "abcdefghijklmnopqrstuvwxyz0123456789";
$randomstr="slfiles_";
for ($k=1;$k<mt_rand(10,16);$k++)
  $randomstr.=substr($validchars,mt_rand(0,35),1);
$pos=strrpos(substr($slpwpath,0,strlen($slpwpath)-1),"/");
if (is_integer($pos))
  $install_filelocation=substr($slpwpath,0,$pos);
$install_filelocation="default=".$install_filelocation."/".$randomstr."/";  

$randomstr="slbackups_";
for ($k=1;$k<mt_rand(10,16);$k++)
  $randomstr.=substr($validchars,mt_rand(0,35),1);
$pos=strrpos(substr($slpwpath,0,strlen($slpwpath)-1),"/");
if (is_integer($pos))
  $install_backuplocation=substr($slpwpath,0,$pos);
$install_backuplocation=$install_backuplocation."/".$randomstr."/";  
  
$install_siteloklog="";

$install_logdetails="YYYYYYYYYYYYYYYY";

$validchars  = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
$install_sitekey="";
for ($k=1;$k<mt_rand(30,50);$k++)
  $install_sitekey.=substr($validchars,mt_rand(0,61),1);

$install_logintype="NORMAL";

$install_turinglogin=0;

$install_turingregister=0;

$install_maxsessiontime=0;

$install_maxinactivitytime=0;

$install_cookielogin=0;

$install_logintemplate="";

$install_expiredpage="";

$install_wronggrouppage="";

$install_messagetemplate="";

$install_forgottenemail="forgotpass.htm";

$install_newuseremail="newuser.htm";

$install_showrows=10;

$install_customintable=1;

$install_randompasswordmask="cccc##";

$install_md5passwords=0;

$install_concurrentlogin=1;

$install_logviewoffset=0;

$install_logvieworder="ASC";

$install_logviewdetails="YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY";

$install_sortfield="";

$install_sortdirection="";

$install_customtitle1="Custom 1";
$install_customtitle2="Custom 2";
$install_customtitle3="Custom 3";
$install_customtitle4="";
$install_customtitle5="";
$install_customtitle6="";
$install_customtitle7="";
$install_customtitle8="";
$install_customtitle9="";
$install_customtitle10="";
$install_customtitle11="";
$install_customtitle12="";
$install_customtitle13="";
$install_customtitle14="";
$install_customtitle15="";
$install_customtitle16="";
$install_customtitle17="";
$install_customtitle18="";
$install_customtitle19="";
$install_customtitle20="";
$install_customtitle21="";
$install_customtitle22="";
$install_customtitle23="";
$install_customtitle24="";
$install_customtitle25="";
$install_customtitle26="";
$install_customtitle27="";
$install_customtitle28="";
$install_customtitle29="";
$install_customtitle30="";
$install_customtitle31="";
$install_customtitle32="";
$install_customtitle33="";
$install_customtitle34="";
$install_customtitle35="";
$install_customtitle36="";
$install_customtitle37="";
$install_customtitle38="";
$install_customtitle39="";
$install_customtitle40="";
$install_customtitle41="";
$install_customtitle42="";
$install_customtitle43="";
$install_customtitle44="";
$install_customtitle45="";
$install_customtitle46="";
$install_customtitle47="";
$install_customtitle48="";
$install_customtitle49="";
$install_customtitle50="";

$install_custom1validate=0;
$install_custom2validate=0;
$install_custom3validate=0;
$install_custom4validate=0;
$install_custom5validate=0;
$install_custom6validate=0;
$install_custom7validate=0;
$install_custom8validate=0;
$install_custom9validate=0;
$install_custom10validate=0;
$install_custom11validate=0;
$install_custom12validate=0;
$install_custom13validate=0;
$install_custom14validate=0;
$install_custom15validate=0;
$install_custom16validate=0;
$install_custom17validate=0;
$install_custom18validate=0;
$install_custom19validate=0;
$install_custom20validate=0;
$install_custom21validate=0;
$install_custom22validate=0;
$install_custom23validate=0;
$install_custom24validate=0;
$install_custom25validate=0;
$install_custom26validate=0;
$install_custom27validate=0;
$install_custom28validate=0;
$install_custom29validate=0;
$install_custom30validate=0;
$install_custom31validate=0;
$install_custom32validate=0;
$install_custom33validate=0;
$install_custom34validate=0;
$install_custom35validate=0;
$install_custom36validate=0;
$install_custom37validate=0;
$install_custom38validate=0;
$install_custom39validate=0;
$install_custom40validate=0;
$install_custom41validate=0;
$install_custom42validate=0;
$install_custom43validate=0;
$install_custom44validate=0;
$install_custom45validate=0;
$install_custom46validate=0;
$install_custom47validate=0;
$install_custom48validate=0;
$install_custom49validate=0;
$install_custom50validate=0;

$install_emailtype=0;
$install_emailreplyto="";
$install_emailusername="";
$install_emailpassword="";
$install_emailserver="";
$install_emailport="25";
$install_emailauth=1;
$install_emailserversecurity="";
$install_emaildelay=0;
$install_modifyuseremail="updateuser.htm";
$install_noaccesspage="";
$install_dbupdate=0;
$install_allowsearchengine=0;
$install_searchenginegroup="ALL";
$install_profilepassrequired=0;
$install_emailconfirmrequired=0;
$install_emailconfirmtemplate="verifyemail.htm";
$install_emailunique=0;
$install_loginwithemail=0;
$install_columnorder="ACSLCRUSPWENNMEMUG01020304050607080910";
$install_actionitems=0;

// Create settings in table
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbConfigTableName." WHERE confignum='1'");
if ($mysql_result==false)
{
  $err=mysqli_error($mysql_link);
  mysqli_close($mysql_link);
  print "Could not query the table $DbConfigTableName. The following error was reported by MySql<br><br>\n";
  print("$err");
  print "</body>\n";
  print "</html>\n";
  exit;
}
if (mysqli_num_rows($mysql_result)==0)
{
  $query="INSERT INTO `$DbConfigTableName` (
`confignum` ,
`version` ,
`sitename` ,
`siteemail` ,
`dateformat` ,
`logoutpage` ,
`siteloklocation` ,
`siteloklocationurl` ,
`emaillocation` ,
`emailurl` ,
`filelocation` ,
`siteloklog` ,
`logdetails` ,
`sitekey` ,
`logintype` ,
`turinglogin` ,
`turingregister` ,
`maxsessiontime` ,
`maxinactivitytime` ,
`cookielogin` ,
`logintemplate` ,
`expiredpage` ,
`wronggrouppage` ,
`messagetemplate` ,
`forgottenemail` ,
`newuseremail` ,
`showrows` ,
`randompasswordmask` ,
`md5passwords` ,
`concurrentlogin` ,
`logviewoffset` ,
`logvieworder` ,
`logviewdetails` ,
`sortfield` ,
`sortdirection` ,
`customtitle1` ,
`customtitle2` ,
`customtitle3` ,
`customtitle4` ,
`customtitle5` ,
`customtitle6` ,
`customtitle7` ,
`customtitle8` ,
`customtitle9` ,
`customtitle10` ,
`customtitle11` ,
`customtitle12` ,
`customtitle13` ,
`customtitle14` ,
`customtitle15` ,
`customtitle16` ,
`customtitle17` ,
`customtitle18` ,
`customtitle19` ,
`customtitle20` ,
`customtitle21` ,
`customtitle22` ,
`customtitle23` ,
`customtitle24` ,
`customtitle25` ,
`customtitle26` ,
`customtitle27` ,
`customtitle28` ,
`customtitle29` ,
`customtitle30` ,
`customtitle31` ,
`customtitle32` ,
`customtitle33` ,
`customtitle34` ,
`customtitle35` ,
`customtitle36` ,
`customtitle37` ,
`customtitle38` ,
`customtitle39` ,
`customtitle40` ,
`customtitle41` ,
`customtitle42` ,
`customtitle43` ,
`customtitle44` ,
`customtitle45` ,
`customtitle46` ,
`customtitle47` ,
`customtitle48` ,
`customtitle49` ,
`customtitle50` ,
`custom1validate` ,
`custom2validate` ,
`custom3validate` ,
`custom4validate` ,
`custom5validate` ,
`custom6validate` ,
`custom7validate` ,
`custom8validate` ,
`custom9validate` ,
`custom10validate` ,
`custom11validate` ,
`custom12validate` ,
`custom13validate` ,
`custom14validate` ,
`custom15validate` ,
`custom16validate` ,
`custom17validate` ,
`custom18validate` ,
`custom19validate` ,
`custom20validate` ,
`custom21validate` ,
`custom22validate` ,
`custom23validate` ,
`custom24validate` ,
`custom25validate` ,
`custom26validate` ,
`custom27validate` ,
`custom28validate` ,
`custom29validate` ,
`custom30validate` ,
`custom31validate` ,
`custom32validate` ,
`custom33validate` ,
`custom34validate` ,
`custom35validate` ,
`custom36validate` ,
`custom37validate` ,
`custom38validate` ,
`custom39validate` ,
`custom40validate` ,
`custom41validate` ,
`custom42validate` ,
`custom43validate` ,
`custom44validate` ,
`custom45validate` ,
`custom46validate` ,
`custom47validate` ,
`custom48validate` ,
`custom49validate` ,
`custom50validate` ,
`emailtype` ,
`emailreplyto` ,
`emailusername` ,
`emailpassword` ,
`emailserver`,
`emailport`,
`emailauth`,
`emailserversecurity`,
`emaildelay`,
`modifyuseremail`,
`noaccesspage`,
`dbupdate`,
`siteemail2`,
`allowsearchengine`,
`searchenginegroup`,
`profilepassrequired`,
`emailconfirmrequired`,
`emailconfirmtemplate`,
`emailunique`,
`loginwithemail`,
`columnorder`,
`backuplocation`
)
VALUES (
'1',
'$install_version',
'$install_sitename',
'$install_siteemail',
'$install_dateformat',
'$install_logoutpage',
'$install_siteloklocation',
'$install_siteloklocationurl',
'$install_emaillocation',
'$install_emailurl',
'$install_filelocation',
'$install_siteloklog',
'$install_logdetails',
'$install_sitekey',
'$install_logintype',
'$install_turinglogin',
'$install_turingregister',
'$install_maxsessiontime',
'$install_maxinactivitytime',
'$install_cookielogin',
'$install_logintemplate',
'$install_expiredpage',
'$install_wronggrouppage',
'$install_messagetemplate',
'$install_forgottenemail',
'$install_newuseremail',
'$install_showrows',
'$install_randompasswordmask',
'$install_md5passwords',
'$install_concurrentlogin',
'$install_logviewoffset',
'$install_logvieworder',
'$install_logviewdetails',
'$install_sortfield',
'$install_sortdirection',
'$install_customtitle1',
'$install_customtitle2',
'$install_customtitle3',
'$install_customtitle4',
'$install_customtitle5',
'$install_customtitle6',
'$install_customtitle7',
'$install_customtitle8',
'$install_customtitle9',
'$install_customtitle10',
'$install_customtitle11',
'$install_customtitle12',
'$install_customtitle13',
'$install_customtitle14',
'$install_customtitle15',
'$install_customtitle16',
'$install_customtitle17',
'$install_customtitle18',
'$install_customtitle19',
'$install_customtitle20',
'$install_customtitle21',
'$install_customtitle22',
'$install_customtitle23',
'$install_customtitle24',
'$install_customtitle25',
'$install_customtitle26',
'$install_customtitle27',
'$install_customtitle28',
'$install_customtitle29',
'$install_customtitle30',
'$install_customtitle31',
'$install_customtitle32',
'$install_customtitle33',
'$install_customtitle34',
'$install_customtitle35',
'$install_customtitle36',
'$install_customtitle37',
'$install_customtitle38',
'$install_customtitle39',
'$install_customtitle40',
'$install_customtitle41',
'$install_customtitle42',
'$install_customtitle43',
'$install_customtitle44',
'$install_customtitle45',
'$install_customtitle46',
'$install_customtitle47',
'$install_customtitle48',
'$install_customtitle49',
'$install_customtitle50',
'$install_custom1validate',
'$install_custom2validate',
'$install_custom3validate',
'$install_custom4validate',
'$install_custom5validate',
'$install_custom6validate',
'$install_custom7validate',
'$install_custom8validate',
'$install_custom9validate',
'$install_custom10validate',
'$install_custom11validate',
'$install_custom12validate',
'$install_custom13validate',
'$install_custom14validate',
'$install_custom15validate',
'$install_custom16validate',
'$install_custom17validate',
'$install_custom18validate',
'$install_custom19validate',
'$install_custom20validate',
'$install_custom21validate',
'$install_custom22validate',
'$install_custom23validate',
'$install_custom24validate',
'$install_custom25validate',
'$install_custom26validate',
'$install_custom27validate',
'$install_custom28validate',
'$install_custom29validate',
'$install_custom30validate',
'$install_custom31validate',
'$install_custom32validate',
'$install_custom33validate',
'$install_custom34validate',
'$install_custom35validate',
'$install_custom36validate',
'$install_custom37validate',
'$install_custom38validate',
'$install_custom39validate',
'$install_custom40validate',
'$install_custom41validate',
'$install_custom42validate',
'$install_custom43validate',
'$install_custom44validate',
'$install_custom45validate',
'$install_custom46validate',
'$install_custom47validate',
'$install_custom48validate',
'$install_custom49validate',
'$install_custom50validate',
'$install_emailtype',
'$install_emailreplyto',
'$install_emailusername',
'$install_emailpassword',
'$install_emailserver',
'$install_emailport',
'$install_emailauth',
'$install_emailserversecurity',
'$install_emaildelay',
'$install_modifyuseremail',
'$install_noaccesspage',
'$install_dbupdate',
'$install_siteemail2',
'$install_allowsearchengine',
'$install_searchenginegroup',
'$install_profilepassrequired',
'$install_emailconfirmrequired',
'$install_emailconfirmtemplate',
'$install_emailunique',
'$install_loginwithemail',
'$install_columnorder',
'$install_backuplocation')";


  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not insert data in table $DbConfigTableName. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}

// Create settings in sl_adminconfig table
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_adminconfig WHERE confignum='1'");
if ($mysql_result==false)
{
  $err=mysqli_error($mysql_link);
  mysqli_close($mysql_link);
  print "Could not query the table sl_adminconfig. The following error was reported by MySql<br><br>\n";
  print("$err");
  print "</body>\n";
  print "</html>\n";
  exit;
}
if (mysqli_num_rows($mysql_result)==0)
{
  $query="INSERT INTO `sl_adminconfig` (
`confignum` ,
`sendnewuseremail` ,
`sendedituseremail` ,
`emaildedupe` ,
`emaildeselect` ,
`exportfilename` ,
`exportuseheader` ,
`exporttype` ,
`exportfields` ,
`importheader` ,
`importuseemailas` ,
`importaddusers` ,
`importrandpass` ,
`importusergroups` ,
`importslctadded` ,
`importexistusers` ,
`importblank` ,
`importslctexist`
)
VALUES (
'1',
'0',
'0',
'0',
'1',
'sitelokusers.csv',
'0',
'0',
'CRUSPWENNMEMUG0102030405060708091011121314151617181920212223242526272829303132333435363738394041424344454647484950',
'0',
'emailnever',
'1',
'randnewonly',
'',
'0',
'1',
'allowblank',
'0')";

  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not insert data in table sl_adminconfig. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}

// Add default login template if not already set
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_logintemplate WHERE id='1'");
if ($mysql_result==false)
{
  $err=mysqli_error($mysql_link);
  mysqli_close($mysql_link);
  print "Could not query the table sl_logintemplate. The following error was reported by MySql<br><br>\n";
  print("$err");
  print "</body>\n";
  print "</html>\n";
  exit;
}
if (mysqli_num_rows($mysql_result)==0)
{
  $query="INSERT INTO `sl_logintemplate` (`id`, `backcolor`, `backimage`, `backimagerp`, `mainfont`, `boxcolortype`, `boxcolorfrom`, `boxcolorto`, `boxradius`, `boxshadow`, `title`, `titlecolor`, `titlesize`, `titlealign`, `titlefont`, `msgcolor`, `msgsize`, `msgalign`, `username`, `password`, `captcha`, `remember`, `autologin`, `labelcolor`, `labelsize`, `inputcolor`, `inputsize`, `inputbackcolor`, `showicons`, `btnlbltext`, `btnlblfont`, `btnlblcolor`, `btnlblsize`, `btnlblstyle`, `btncolortype`, `btncolorfrom`, `btncolorto`, `btnradius`, `btnbordercolor`, `btnbordersize`, `btnborderstyle`, `forgottxt`, `forgotcolor`, `forgotsize`, `signuptext`, `signupurl`, `signupcolor`, `signupsize`, `signupalign`) VALUES(1, 'FFFFFF', '', 'no-repeat', 'Arial, Helvetica, sans-serif', 'gradient', '1A305E', '2F5DAA', 10, 6, 'Login', 'FFFFFF', 44, 'center', 'Arial, Helvetica, sans-serif', 'FF0000', 16, 'left', 'Username', 'Password', 'Captcha', 'Remember Me', 'Auto Login', 'FFFFFF', 18, '1A305E', 16, 'FFFFFF', 1, 'Login', 'Arial, Helvetica, sans-serif', 'FFFFFF', 17, 'normal normal bold', 'gradient', '79BCFF', '378EE5', 10, 'FFFFFF', 0, 'solid', 'Forgot Password', 'FFFFFF', 14, '', '', 'FFA500', 16, 'center');";
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    $err=mysqli_error($mysql_link);
    mysqli_close($mysql_link);
    print "Could not insert data in table sl_logintemplate. The following error was reported by MySql<br><br>\n";
    print("$err");
    print "</body>\n";
    print "</html>\n";
    exit;
  }
}


// As the filelocation and backuplocation may have been created in a previous run of install.php we should get it from the table
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbConfigTableName." WHERE confignum='1'");
if ($mysql_result==false)
{
  $err=mysqli_error($mysql_link);
  mysqli_close($mysql_link);
  print "Could not read data from table $DbConfigTableName. The following error was reported by MySql<br><br>\n";
  print("$err");
  print "</body>\n";
  print "</html>\n";
  exit;
}
else
{
  if ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
  {
    $temp=explode("|",$row['filelocation']);
    for ($k=0;$k<count($temp);$k++)
    {
      $label=strtok($temp[$k],"=");
      $value=strtok("=");
      if ($label=="default")
        $install_filelocation=$value;
    }
    $install_backuplocation=$row['backuplocation'];
  }
}
$problem=false;
$problemmsg="";
// Create FileLocation folder
if (!file_exists($install_filelocation))
{
  if (!@mkdir($install_filelocation, 0777))
  {
    $problem=true;
    $problemmsg.="* Create a folder called <b>".basename($install_filelocation)."</b> in the root of your site using your filemanager or FTP client.<br><br>\n";
    $problemmsg.="and set the permissions to allow write (usually 666 or 777).<br><br>\n";
  }  
}

// Create BackupLocation folder
if (!file_exists($install_backuplocation))
{
  if (!@mkdir($install_backuplocation, 0777))
  {
    $problem=true;
    $problemmsg.="* Create a folder called <b>".basename($install_backuplocation)."</b> in the root of your site using your filemanager or FTP client<br>\n";
    $problemmsg.="and set the permissions to allow write (usually 666 or 777).<br><br>\n";
  }
}

// Set permission on email folder
if (!is_writeable($install_emaillocation))
{
  @chmod($install_emaillocation,0777);
  if (!is_writeable($install_emaillocation))
  {
    $problemmsg.= "* Set write permission (usually 666 or 777) on the <b>email</b> folder (in slpw) using your filemanager or FTP client.<br><br>\n";         
    $problem=true;  
  }
}

// Set permission on writefiles folder
if (!is_writeable($install_writefileslocation))
{
  @chmod($install_writefileslocation,0777);
  if (!is_writeable($install_writefileslocation))
  {
    $problemmsg.= "* Set write permission (usually 666 or 777) on the <b>writefiles</b> folder (in slpw) using your filemanager or FTP client.<br><br>\n";         
    $problem=true;  
  }
}

// Set permission on emailthumbs folder
if (!is_writeable($install_emailthumbslocation))
{
  @chmod($install_emailthumbslocation,0777);
  if (!is_writeable($install_emailthumbslocation))
  {
    $problemmsg.= "* Set write permission (usually 666 or 777) on the <b>emailthumbs</b> folder (in slpw) using your filemanager or FTP client.<br><br>\n";         
    $problem=true;  
  }
}

// Try to create .htaccess file in filelocation and backuplocation
$fcontents="order allow,deny\ndeny from *\n";
if (!file_exists($install_filelocation.".htaccess"))
{
  if (is_writeable($install_filelocation))
  {
    $fh=@fopen($install_filelocation.".htaccess","w");
    fwrite($fh,$fcontents);
    fclose($fh);
  }
}  

if (!file_exists($install_backuplocation.".htaccess"))
{
  if (is_writeable($install_backuplocation))
  {
    $fh=@fopen($install_backuplocation.".htaccess","w");
    fwrite($fh,$fcontents);
    fclose($fh);
  }
}  


if (!$problem)
{
  print "Installation completed sucessfully.<br>\n";
  print "<br>\n";
  print "You can now login to the Sitelok control panel at <a href=\"".$install_siteloklocationurl."index.php\">".$install_siteloklocationurl."</a> (admin / letmein).<br>\n";  
}
else
{
  print "Installation completed sucessfully however there are some task(s) that install could not perform for you.<br>\n";
  print "Please complete the following step(s) to complete the installation. You do <b>not</b> need to run install again.<br>\n"; 
  print "<br>\n";
  print $problemmsg;
  print "Once you have completed the above step(s) you can then login to the Sitelok control panel at <a href=\"".$install_siteloklocationurl."index.php\">".$install_siteloklocationurl."</a> (admin / letmein).<br>\n";  
  print "If you need help with installation please <a href=\"http://www.vibralogix.com/company/contact.php\">contact us</a>.";
}

print "</body>\n";
print "</html>\n";

function tableexists($mysql_link, $tablename, $firstfield)
{
  // See if table exists
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$tablename);
  if ($mysql_result==false)
    return(false);
  // If it does check firstname field matches
  $finfo = mysqli_fetch_field($mysql_result);
  $fieldname=$finfo->name;
  if (strtolower($fieldname)==strtolower($firstfield))
    return(true);
  return(false);  
}
?>