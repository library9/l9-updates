<?php
$groupswithaccess="ADMIN,SUBADMIN";
$noaccesspage="";
$adminajaxcall=true;
require("sitelokpw.php");
if (!isset($adminlanguage))
  $adminlanguage="en";
require("adminlanguage-".$adminlanguage.".php");  
$slsubadmin=false;
if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
  $slsubadmin=true;
// Check CSRF value
if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
{
  ?>
  {
  "esstatus": "error",
  "message": "<?php echo ADMINMSG_CSRFFAILED; ?>"
  }
  <?php
  exit;      
}
$cacheFile=$SitelokLocation."writefiles/emailselectedprogress.json";
$commandFile=$SitelokLocation."writefiles/emailselectedcontrol.json";
$esact=$_POST['esact'];
if ($esact=="init")
{
  // Create json file for pause and cancel handling
  $json = <<<EOT
{
  "comact": ""
}
EOT;

  @file_put_contents($commandFile,$json);
  // Get email detals and store for later calls
  $body=$_POST['content'];
  $_SESSION['ses_slemailbody']=$body;
  $subject=$_POST['subject'];
  $_SESSION['ses_slemailsubject']=$subject;
  $htmlformat=$_POST['htmlformat'];
  if ($htmlformat=="html")
    $htmlformat="Y";
  else
    $htmlformat="N";
  $_SESSION['ses_slemailhtmlformat']=$htmlformat;
  $dedupe=$_POST['dedupe'];
  $_SESSION['ses_slemaildedupe']=$dedupe;
  $deselect=$_POST['deselect'];
  $_SESSION['ses_slemaildeselect']=$deselect;
  // Initialise counters etc
  $emailcount=0;
  $_SESSION['ses_slemailcount']=$emailcount;
  $emailsent=0;
  $_SESSION['ses_slemailsent']=$emailsent;
  $emailfail=0;
  $_SESSION['ses_slemailfail']=$emailfail;
  $emailblocked=0;
  $_SESSION['ses_slemailblocked']=$emailblocked;
  $lastemailuserid=0;
  $_SESSION['ses_sllastemailuserid']=$lastemailuserid;
  // Clear number of slected users if deslect checked
  if ($deselect=="1")
    $_SESSION['slnumselected']=""; 
}
else
{
  // Get email details from session  
  $body=$_SESSION['ses_slemailbody'];
  $subject=$_SESSION['ses_slemailsubject'];
  $htmlformat=$_SESSION['ses_slemailhtmlformat'];
  $dedupe=$_SESSION['ses_slemaildedupe'];
  $deselect=$_SESSION['ses_slemaildeselect'];
  // Get counters etc from session
  $emailcount=$_SESSION['ses_slemailcount'];
  $emailsent=$_SESSION['ses_slemailsent'];
  $emailfail=$_SESSION['ses_slemailfail'];
  $emailblocked=$_SESSION['ses_slemailblocked'];
  $lastemailuserid=$_SESSION['ses_sllastemailuserid'];
  // see if session ended for some reason
  if ($subject=="")
  {
  ?>
  {
  "esstatus": "error",
  "message": "Session ended"
  }
  <?php
  exit;      
  }
}
$maxtime=ini_get('max_execution_time');
if ((!is_numeric($maxtime)) || ($maxtime<=0))
  $maxtime=30;
if ($maxtime>60)
  $maxtime=60; // (to be safe as some servers have 50000) 
$maxtime=$maxtime-5;
$scriptstart=time();
$mysql_link=sl_DBconnect();
if ($mysql_link==false)
{
  ?>
  {
  "esstatus": "error",
  "message": "<?php echo ADMINMSG_MYSQLERROR; ?>"
  }
  <?php
  exit;      
}
if ($esact=="init")
{
  $query="UPDATE sl_adminconfig SET emaildedupe=".sl_quote_smart($dedupe).", emaildeselect=".sl_quote_smart($deselect)." WHERE confignum=1";
  $mysql_result=mysqli_query($mysql_link,$query);
}

$sl_dbblocksize=1000;
$finished=false;  
do
{
  // See how many selected records if this is first call
  if ($emailcount==0)
  {            
    if ($dedupe=="1") 
      $mysql_result=mysqli_query($mysql_link,"SELECT SQL_CALC_FOUND_ROWS * FROM ".$DbTableName." WHERE ".$SelectedField."='Yes' AND ".$IdField.">".$lastemailuserid." GROUP BY(".$EmailField.") ORDER BY ".$IdField." ASC LIMIT ".$sl_dbblocksize);  
    else
      $mysql_result=mysqli_query($mysql_link,"SELECT SQL_CALC_FOUND_ROWS * FROM ".$DbTableName." WHERE ".$SelectedField."='Yes' AND ".$IdField.">".$lastemailuserid." ORDER BY ".$IdField." ASC LIMIT ".$sl_dbblocksize);  
    $sqlnum=mysqli_query($mysql_link,"SELECT FOUND_ROWS() AS `found_rows`;");
    $rows = mysqli_fetch_array($sqlnum,MYSQLI_ASSOC);
    $numtoemail = $rows['found_rows'];
    // Output to json file for progress reporting
    $json = <<<EOT
{
  "emailcount": "0",
  "emailsent": "0",
  "emailblocked": "0",
  "emailfail": "0",
  "numtoemail": "{$numtoemail}"
}
EOT;
    file_put_contents($cacheFile,$json);
  }
  else
  {
    if ($dedupe=="1") 
      $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." WHERE ".$SelectedField."='Yes' AND ".$IdField.">".$lastemailuserid." GROUP BY(".$EmailField.") ORDER BY ".$IdField." ASC LIMIT ".$sl_dbblocksize);  
    else
      $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." WHERE ".$SelectedField."='Yes' AND ".$IdField.">".$lastemailuserid." ORDER BY ".$IdField." ASC LIMIT ".$sl_dbblocksize);          
    $numtoemail=$_SESSION['ses_slnumtoemail'];  
  }
  if ($mysql_result!=false)
  {
    while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
    {
      // See if paused or cancelled
      $str=@file_get_contents($commandFile);
      if ($str!==false)
      {
        $jsonstr=json_decode($str);
        if ($jsonstr->{'comact'}=="pause")
        {
        ?>
{
  "esstatus": "paused",
  "message": "",
  "emailcount": "<?php echo $emailcount; ?>",
  "emailsent": "<?php echo $emailsent; ?>",
  "emailfail": "<?php echo $emailfail; ?>",
  "emailblocked": "<?php echo $emailblocked; ?>",
  "numtoemail": "<?php echo $numtoemail; ?>"
}
        <?php
        exit;      
        }

        if ($jsonstr->{'comact'}=="cancel")
        {
        ?>
{
  "esstatus": "cancelled",
  "message": "",
  "emailcount": "<?php echo $emailcount; ?>",
  "emailsent": "<?php echo $emailsent; ?>",
  "emailfail": "<?php echo $emailfail; ?>",
  "emailblocked": "<?php echo $emailblocked; ?>",
  "numtoemail": "<?php echo $numtoemail; ?>"
}
        <?php
        exit;      
        }

      }
      if ($EmailDelay!=0)
      {
        if ($EmailDelay>=1000)
          sleep(intval($EmailDelay/1000));
        else
          usleep($EmailDelay*1000);  
      }
      else
        usleep(50000); // This was needed on 1&1 for time() to update!
        
      // See if near max script time
      if (($maxtime!=0) && ((time()-$scriptstart)>=($maxtime-1-(intval($EmailDelay/1000)))))
      {
        ?>
        {
        "esstatus": "more",
        "message": ""
        }
        <?php
        exit;      
      }
      $Selected=$row[$SelectedField];
      $Created=$row[$CreatedField];
      $Username=$row[$UsernameField];
      $Password=$row[$PasswordField];
      $Enabled=$row[$EnabledField];
      $Name=$row[$NameField];
      $Email=$row[$EmailField];
      $Usergroups=$row[$UsergroupsField];
      $UserId=$row[$IdField];
      $Cus1=$row[$Custom1Field];
      $Cus2=$row[$Custom2Field];
      $Cus3=$row[$Custom3Field];
      $Cus4=$row[$Custom4Field];
      $Cus5=$row[$Custom5Field];
      $Cus6=$row[$Custom6Field];
      $Cus7=$row[$Custom7Field];
      $Cus8=$row[$Custom8Field];
      $Cus9=$row[$Custom9Field];
      $Cus10=$row[$Custom10Field];
      $Cus11=$row[$Custom11Field];
      $Cus12=$row[$Custom12Field];
      $Cus13=$row[$Custom13Field];
      $Cus14=$row[$Custom14Field];
      $Cus15=$row[$Custom15Field];
      $Cus16=$row[$Custom16Field];
      $Cus17=$row[$Custom17Field];
      $Cus18=$row[$Custom18Field];
      $Cus19=$row[$Custom19Field];
      $Cus20=$row[$Custom20Field];
      $Cus21=$row[$Custom21Field];
      $Cus22=$row[$Custom22Field];
      $Cus23=$row[$Custom23Field];
      $Cus24=$row[$Custom24Field];
      $Cus25=$row[$Custom25Field];
      $Cus26=$row[$Custom26Field];
      $Cus27=$row[$Custom27Field];
      $Cus28=$row[$Custom28Field];
      $Cus29=$row[$Custom29Field];
      $Cus30=$row[$Custom30Field];
      $Cus31=$row[$Custom31Field];
      $Cus32=$row[$Custom32Field];
      $Cus33=$row[$Custom33Field];
      $Cus34=$row[$Custom34Field];
      $Cus35=$row[$Custom35Field];
      $Cus36=$row[$Custom36Field];
      $Cus37=$row[$Custom37Field];
      $Cus38=$row[$Custom38Field];
      $Cus39=$row[$Custom39Field];
      $Cus40=$row[$Custom40Field];
      $Cus41=$row[$Custom41Field];
      $Cus42=$row[$Custom42Field];
      $Cus43=$row[$Custom43Field];
      $Cus44=$row[$Custom44Field];
      $Cus45=$row[$Custom45Field];
      $Cus46=$row[$Custom46Field];
      $Cus47=$row[$Custom47Field];
      $Cus48=$row[$Custom48Field];
      $Cus49=$row[$Custom49Field];
      $Cus50=$row[$Custom50Field];
      $emres=sl_SendEmail($Email,$body,$subject,$htmlformat,$Username,$Password,$Name,$Email,$Usergroups,$Cus1,$Cus2,$Cus3,$Cus4,$Cus5,$Cus6,$Cus7,$Cus8,$Cus9,$Cus10,
      $Cus11,$Cus12,$Cus13,$Cus14,$Cus15,$Cus16,$Cus17,$Cus18,$Cus19,$Cus20,$Cus21,$Cus22,$Cus23,$Cus24,$Cus25,$Cus26,$Cus27,$Cus28,$Cus29,$Cus30,
      $Cus31,$Cus32,$Cus33,$Cus34,$Cus35,$Cus36,$Cus37,$Cus38,$Cus39,$Cus40,$Cus41,$Cus42,$Cus43,$Cus44,$Cus45,$Cus46,$Cus47,$Cus48,$Cus49,$Cus50);
      $num=$emailcount+1;
      if ($emres==1)
      {
        $emailsent++;
        if ($deselect=="1")
        {
          // Deselect user
          if ($dedupe=="1")
            mysqli_query($mysql_link,"UPDATE ".$DbTableName." SET ".$SelectedField."='No' WHERE ".$EmailField."='".$Email."'");
          else
            mysqli_query($mysql_link,"UPDATE ".$DbTableName." SET ".$SelectedField."='No' WHERE ".$IdField."=".$UserId);
        }
      } 
      if ($emres==2)
        $emailblocked++;
      if ($emres==0)
        $emailfail++;
      $emailcount++;
      $lastemailuserid=$UserId;
      if ($SessionName!="")
        session_name($SessionName);
      session_start();
      $_SESSION['ses_slnumtoemail']=$numtoemail;
      $_SESSION['ses_slemailcount']=$emailcount;
      $_SESSION['ses_slemailsent']=$emailsent;
      $_SESSION['ses_slemailfail']=$emailfail;
      $_SESSION['ses_slemailblocked']=$emailblocked;
      $_SESSION['ses_sllastemailuserid']=$lastemailuserid;
      // If init we need to force config update in case dedupe or deselect settings have changed
      if ($esact=="init")
        $_SESSION['ses_ConfigReload']="reload";     
      session_write_close();
      // Output to json file for progress reporting
      $json = <<<EOT
{
     "emailcount": "{$emailcount}",
     "emailsent": "{$emailsent}",
     "emailblocked": "{$emailblocked}",
     "emailfail": "{$emailfail}",
     "numtoemail": "{$numtoemail}"
}
EOT;

file_put_contents($cacheFile,$json);

    }      
  }
  else
  {
    $finished=true;
  }
}
while((!$finished) && ($emailcount<$numtoemail));
@unlink($cacheFile);        
@unlink($commandFile);        
?>
{
"esstatus": "finished",
"message": "",
"emailcount": "<?php echo $emailcount; ?>",
"emailsent": "<?php echo $emailsent; ?>",
"emailfail": "<?php echo $emailfail; ?>",
"emailblocked": "<?php echo $emailblocked; ?>",
"numtoemail": "<?php echo $numtoemail; ?>"
} 
<?php
exit;
?>