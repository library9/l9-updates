var cbselect=Array();
var cbdeselect=Array();
var cbupdateprocessing=false; // handle checkbox clicked in fast succession
var cbupdatewait=false; // Stop actions on selected being used while updating checkboxes
var advancedfilters=Array();
var moreoptionsuserid=0; // Stores user clicked for more options
var moreoptionsusername="";
advancedfilters[0]=true;
advancedfilters[1]=false;
advancedfilters[2]=false;
advancedfilters[3]=false;

updatenumselected();
updatenumfiltered();                

$.fn.dataTable.ext.errMode = 'none';
// Handle table error event
$('#siteloktable').on('error.dt', function ( e, settings, techNote, message ) {
  var msgid=message.substr(-3,3);
  switch (msgid)
  {
    case "001":
      location.reload(true);
      break;
    case "002":
      bootbox.alert("Could not connect to Mysql");
      break;
    default:
      bootbox.alert(message);
  }
});




// Populate usergroup lists (done via PHP now)
//$('.usergrouplist').append('<option value="BRONZE">BRONZE</option>');
//$('.usergrouplist').append('<option value="SILVER">SILVER</option>');
//$('.usergrouplist').append('<option value="GOLD">GOLD</option>');

// Enable combo boxes
$('#memberof').editableSelect();
$('#unexpmemberof').editableSelect();
$('#expmemberof').editableSelect();
$('#expwithin').editableSelect();

$('.selectpicker').selectpicker({
  style: 'btn-default',
  size: 4,
  dropupAuto: false  
});


showFilter('quicksearch');
function showFilter(filter,title)
{
  $('#quicksearchblock').hide();
  $('#memberofblock').hide();
  $('#unexpmemberofblock').hide();
  $('#expmemberofblock').hide();
  $('#expwithinblock').hide();
  $('#joinwithinblock').hide();
  $('#onlyselectedblock').hide();
  $('#filterblock').hide();
  $('#queryblock').hide();

  if (filter!="")
  {
    $('#'+filter+'block').show();
    // If advanced filter then see if conditions need to be displayed
    if (filter=='filter')
    {
      if (fildata2!='')
      {
        $('#'+filter+'2block').show();
        if (fildata3!='')
        {
          $('#'+filter+'3block').show();
          if (fildata4!='')
          {
            $('#'+filter+'4block').show();
          }
        }
      }

    }
    switch (filter)
    {
      case 'quicksearch':
        var title=ADMINFILTER_QUICKSEARCH;
        break;
      case 'memberof':
        var title=ADMINFILTER_MEMBEROF;
        break;
      case 'unexpmemberof':
        var title=ADMINFILTER_UNEXPMEMBEROF;
        break;
      case 'expmemberof':
        var title=ADMINFILTER_EXPMEMBEROF;
        break;
      case 'expwithin':
        var title=ADMINFILTER_EXPWITHIN;
        break;
      case 'joinwithin':
        var title=ADMINFILTER_NEWMEMBERS;
        break;
      case 'onlyselected':
        var title=ADMINFILTER_ONLYSELECTED;
        break;
      case 'filter':
        var title=ADMINFILTER_ADVANCEDFILTER;
        break;
      case 'query':
        var title=ADMINFILTER_ENTERSQLQUERY;
        break;
    }
    $('#filtertitle').text(title);
  }
}

$('#quicksearchform').submit(function()
{
  if ($('#quicksearch').val()=="")
    return(false);
  filteract="quicksearch";
  filteron=0;
  quicksearch=$('#quicksearch').val();
  sitelokdatatable.page(0);
  sitelokdatatable.ajax.reload( null, false );
  return(false);
});

$('#memberofform').submit(function()
{
  var formid='memberofform';
  removeFormErrors(formid);
  var errormsg="";
  var obj=$('#memberof');
  var val=$(obj).val();
  if (!validUsergroup(val,true))
  {
    showFieldError(obj);
    errormsg=ADMINMSG_INVALIDGROUP;
  }
  if (errormsg!="")
  {
    showFormError(formid,errormsg);
    return(false);
  }
  filteract="memberof";
  filteron=0;
  memberof=$('#memberof').val();
  sitelokdatatable.page(0);
  sitelokdatatable.ajax.reload( null, false );
  return(false);
});

$('#unexpmemberofform').submit(function()
{
  var formid='unexpmemberofform';
  removeFormErrors(formid);
  var errormsg="";
  var obj=$('#unexpmemberof');
  var val=$(obj).val();
  if (!validUsergroup(val,true))
  {
    showFieldError(obj);
    errormsg=ADMINMSG_INVALIDGROUP;
  }
  if (errormsg!="")
  {
    showFormError(formid,errormsg);
    return(false);
  }
  filteract="unexpmemberof";
  filteron=0;
  unexpmemberof=$('#unexpmemberof').val();
  sitelokdatatable.page(0);  
  sitelokdatatable.ajax.reload( null, false );
  return(false);
});

$('#expmemberofform').submit(function()
{
  var formid='expmemberofform';
  removeFormErrors(formid);
  var errormsg="";
  var obj=$('#expmemberof');
  var val=$(obj).val();
  if (!validUsergroup(val,true))
  {
    showFieldError(obj);
    errormsg=ADMINMSG_INVALIDGROUP;
  }
  if (errormsg!="")
  {
    showFormError(formid,errormsg);
    return(false);
  }
  filteract="expmemberof";
  filteron=0;
  expmemberof=$('#expmemberof').val();
  sitelokdatatable.page(0);
  sitelokdatatable.ajax.reload( null, false );
  return(false);
});

$('#expwithinform').submit(function()
{
  var formid='expwithinform';
  removeFormErrors(formid);
  var errormsg="";
  var obj=$('#expwithin');
  var val=$(obj).val();
  if (!validUsergroup(val,true))
  {
    showFieldError(obj);
    errormsg=ADMINMSG_INVALIDGROUP;
  }
  obj=$('#expwithindays');
  val=$(obj).val(); 
  if (!validInteger(val,1,999,true))
  {
    showFieldError(obj);
    errormsg+=ADMINMSG_INVALIDDAYS;
  }
  if (errormsg!="")
  {
    showFormError(formid,errormsg);
    return(false);
  }
  filteract="expwithin";
  filteron=0;
  expwithin=$('#expwithin').val();
  expwithindays=$('#expwithindays').val();
  sitelokdatatable.page(0);
  sitelokdatatable.ajax.reload( null, false );
  return(false);
});

$('#joinwithinform').submit(function()
{
  var formid='joinwithinform';
  removeFormErrors(formid);
  var errormsg="";
  var obj=$('#joinwithin');
  var val=$(obj).val();
  if (!validInteger(val,1,999,true))
  {
    showFieldError(obj);
    errormsg+=ADMINMSG_INVALIDDAYS;
  }
  if (errormsg!="")
  {
    showFormError(formid,errormsg);
    return(false);
  }
  filteract="joinwithin";
  filteron=0;
  joinwithin=$('#joinwithin').val();
  sitelokdatatable.page(0);
  sitelokdatatable.ajax.reload( null, false );
  return(false);
});

$('#onlyselectedform').submit(function()
{
  filteract="onlyselected";
  filteron=0;
  onlyselected=$('#onlyselected').val();
  sitelokdatatable.page(0);
  sitelokdatatable.ajax.reload( null, false );
  return(false);
});

$('#filterform').submit(function()
{
  filteract="filter";
  filteron=1;
  filfield1=$('#filfield1').val();
  filcond1=$('#filcond1').val();
  fildata1=$('#fildata1').val();
  filbool1=$('#filbool1').val();
  filfield2=$('#filfield2').val();
  filcond2=$('#filcond2').val();
  fildata2=$('#fildata2').val();
  filbool2=$('#filbool2').val();
  filfield3=$('#filfield3').val();
  filcond3=$('#filcond3').val();
  fildata3=$('#fildata3').val();
  filbool3=$('#filbool3').val();
  filfield4=$('#filfield4').val();
  filcond4=$('#filcond4').val();
  fildata4=$('#fildata4').val();
  sitelokdatatable.page(0);
  sitelokdatatable.ajax.reload( null, false );
  return(false);
});

$('#sqlinputform').submit(function()
{
  if ($('#sqlinput').val()=="")
    return(false);
  filteract="query";
  filteron=0;
  sqlinput=$('#sqlinput').val();
  sitelokdatatable.page(0);
  sitelokdatatable.ajax.reload( null, false );
  return(false);
});

function datatablesReady()
{

}


function onPageShow()
{
  sitelokdatatable.ajax.reload( null, false );  
}

function selectAll()
{
  filteract="selectall";
  sitelokdatatable.ajax.reload( null, false );
  return(false);
}

function deselectAll()
{
  filteract="deselectall";
  sitelokdatatable.ajax.reload( null, false );
  return(false);
}

function showAll()
{
  filteract="showall";
  filteron=0;
  quicksearch="";
  memberof="";
  unexpmemberof="";
  expmemberof="";
  expwithin="";
  expwithindays="";
  joinwithin="";
  onlyselected="Yes";
  filfield1=UsernameField;
  filcond1="contains";
  fildata1="";
  filbool1="AND";
  filfield2=UsernameField;
  filcond2="contains";
  fildata2="";
  filbool2="AND";
  filfield3=UsernameField;
  filcond3="contains";
  fildata3="";
  filbool3="AND";
  filfield4=UsernameField;
  filcond4="contains";
  fildata4="";
  sqlinput="";  
  $('#quicksearch').val(quicksearch);
  $('#memberof').val(memberof);
  $('#unexpmemberof').val(unexpmemberof);
  $('#expmemberof').val(expmemberof);
  $('#expwithin').val(expwithin);
  $('#expwithindays').val(expwithindays);
  $('#joinwithin').val(joinwithin);
  $('#onlyselected').selectpicker('val', onlyselected);  
  $('#filfield1').selectpicker('val', filfield1);  
  $('#filcond1').selectpicker('val', filcond1);  
  $('#fildata1').val(fildata1);
  $('#filbool1').selectpicker('val', filbool1);  
  $('#filfield2').selectpicker('val', filfield2);  
  $('#filcond2').selectpicker('val', filcond2);  
  $('#fildata2').val(fildata2);
  $('#filbool2').selectpicker('val', filbool2);  
  $('#filfield3').selectpicker('val', filfield3);  
  $('#filcond3').selectpicker('val', filcond3);  
  $('#fildata3').val(fildata3);
  $('#filbool3').selectpicker('val', filbool3);  
  $('#filfield4').selectpicker('val', filfield4);  
  $('#filcond4').selectpicker('val', filcond4);  
  $('#fildata4').val(fildata4);
  $('#sqlinput').val(sqlinput);
  updateFilterFields();
  sitelokdatatable.page(0);
  sitelokdatatable.ajax.reload( null, false );
}

function clearAll()
{
  filteract="clearall";
  filteron=0;
  quicksearch="";
  memberof="";
  unexpmemberof="";
  expmemberof="";
  expwithin="";
  expwithindays="";
  joinwithin="";
  onlyselected="Yes";
  filfield1=UsernameField;
  filcond1="contains";
  fildata1="";
  filbool1="AND";
  filfield2=UsernameField;
  filcond2="contains";
  fildata2="";
  filbool2="AND";
  filfield3=UsernameField;
  filcond3="contains";
  fildata3="";
  filbool3="AND";
  filfield4=UsernameField;
  filcond4="contains";
  fildata4="";
  sqlinput="";  
  $('#quicksearch').val(quicksearch);
  $('#memberof').val(memberof);
  $('#unexpmemberof').val(unexpmemberof);
  $('#expmemberof').val(expmemberof);
  $('#expwithin').val(expwithin);
  $('#expwithindays').val(expwithindays);
  $('#joinwithin').val(joinwithin);
  $('#onlyselected').val(onlyselected);
  $('#filfield1').selectpicker('val', filfield1);  
  $('#filcond1').selectpicker('val', filcond1);  
  $('#fildata1').val(fildata1);
  $('#filbool1').selectpicker('val', filbool1);  
  $('#filfield2').selectpicker('val', filfield2);  
  $('#filcond2').selectpicker('val', filcond2);  
  $('#fildata2').val(fildata2);
  $('#filbool2').selectpicker('val', filbool2);  
  $('#filfield3').selectpicker('val', filfield3);  
  $('#filcond3').selectpicker('val', filcond3);  
  $('#fildata3').val(fildata3);
  $('#filbool3').selectpicker('val', filbool3);  
  $('#filfield4').selectpicker('val', filfield4);  
  $('#filcond4').selectpicker('val', filcond4);  
  $('#fildata4').val(fildata4);
  $('#sqlinput').val(sqlinput);  
  updateFilterFields();
  sitelokdatatable.page(0);
  sitelokdatatable.ajax.reload( null, false );
}

function updateFilterFields()
{
  $('#quicksearch').val(quicksearch);
  $('#memberof').val(memberof);
  $('#unexpmemberof').val(unexpmemberof);
  $('#expmemberof').val(expmemberof);
  $('#expwithin').val(expwithin);
  $('#expwithindays').val(expwithindays);
  $('#joinwithin').val(joinwithin);
  $('#onlyselected').selectpicker('val', onlyselected);
  $('#filfield1').selectpicker('val', filfield1);  
  $('#filcond1').selectpicker('val', filcond1);  
  $('#fildata1').val(fildata1);
  $('#filbool1').selectpicker('val', filbool1);  
  $('#filfield2').selectpicker('val', filfield2);  
  $('#filcond2').selectpicker('val', filcond2);  
  $('#fildata2').val(fildata2);
  $('#filbool2').selectpicker('val', filbool2);  
  $('#filfield3').selectpicker('val', filfield3);  
  $('#filcond3').selectpicker('val', filcond3);  
  $('#fildata3').val(fildata3);
  $('#filbool3').selectpicker('val', filbool3);  
  $('#filfield4').selectpicker('val', filfield4);  
  $('#filcond4').selectpicker('val', filcond4);  
  $('#fildata4').val(fildata4);
  $('#sqlinput').val(sqlinput); 
//  alert(filtertype); 
  switch(filtertype)
  {
    case 'quicksearch':
      showFilter('quicksearch');
      break;
    case 'memberof':
      showFilter('memberof');
      break;
    case 'unexpmemberof':
      showFilter('unexpmemberof');
      break;
    case 'expmemberof':
      showFilter('expmemberof');
      break;
    case 'expwithin':
      showFilter('expwithin');
      break;
    case 'joinwithin':
      showFilter('joinwithin');
      break;
    case 'onlyselected':
      showFilter('onlyselected');
      break;
    case 'filter':
      showFilter('filter');
      break;
    case 'query':
      showFilter('query');
      break;
  } 
}

function slrowsel(cb,id)
{
  if (cb.checked)
  {  
    slnumselected++;
    // if already listed to deselect then remove
    var pos=cbdeselect.indexOf(id);
    if (pos>-1)
      cbdeselect.splice(pos,1);
    else
      cbselect.push(id);
  }  
  else
  {
    slnumselected--;
    // if already listed to select then remove
    var pos=cbselect.indexOf(id);
    if (pos>-1)
      cbselect.splice(pos,1);
    else
      cbdeselect.push(id);
  }
  updatenumselected();
  // Set a timer to call ajax function if not processing already.
  // This way we don't need to call for every checkbox clicked in fast succession
  if ((cbupdateprocessing) || ((cbselect.length==0) && (cbdeselect.length==0)))
    return;
  cbupdateprocessing=true;
  window.setTimeout(updateselected, 1000);
}

function updateselected()
{
  cbupdateprocessing=false;
  cbupdatewait=true;
  var datatosend = { cbselect : cbselect, cbdeselect : cbdeselect };
  cbselect=[];
  cbdeselect=[];
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : 'adminadjustselected.php', // the url where we want to POST
        data        : datatosend,
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
    .done(function(data) {
      sessionExpiredReported(data,true);
      if (!data.success)
        bootbox.alert(data.message);
    })
    .fail(function() {
      bootbox.alert("Connection failed");
    })
    .always(function() {
      cbupdatewait=false;
  }); 
}

function updatenumselected()
{
    if (slnumselected==1)
      $('#shownumselected').text(slnumselected+' '+ADMINDATATABLE_USERSELECTED);
    else
      $('#shownumselected').text(slnumselected+' '+ADMINDATATABLE_USERSSELECTED);      
    if (slnumselected<1)
    {
      $('#selectedactions').hide();
      $('.selectactionmenu').hide();

    }
    else
    {
      $('#selectedactions').show();
      $('.selectactionmenu').show();
    }
}

function updatenumfiltered()
{
    if (slnumfiltered<1)
    {
      $('#usersfiltered').hide();
    }
    else
    {
      $('#usersfiltered').html('<a href="#" onclick="event.preventDefault(); showAll();">clear filter</a>');
      $('#usersfiltered').show();
    }
}

function addUser(id)
{
  window.location.href = "adduser.php";
}

function editUser(id)
{
  window.location.href = "edituser.php?&userid="+id;
}

function emailUser(id,username)
{
  window.location.href = "emailuser.php?act=emailuser&id="+id+"&un="+encodeURIComponent(username);
}


function deleteUser(userid,username)
{
  bootbox.confirm(ADMINMSG_DELETEUSER+" "+username+"?", function(result) {
    if (result)
    {
      $('body').addClass('wait');
      var datatosend = { userid : userid, slcsrf : slcsrf };
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'admindeleteuser.php', // the url where we want to POST
            data        : datatosend,
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        .done(function(data) {
          sessionExpiredReported(data,true);
          if (!data.success)
            bootbox.alert(data.message);
        })
        .fail(function() {
          bootbox.alert(ADMINMSG_NOTDELUSER);
        })
        .always(function() {
          $('body').removeClass('wait');          
          sitelokdatatable.ajax.reload( null, false );
      });
    }  

  });
}

function passwordReminder(userid,username)
{
  bootbox.confirm(ADMINBUTTON_PASSREMIND+" - "+username+"?", function(result) {
    if (result)
    {
      $('body').addClass('wait');
      var datatosend = { userid : userid, slcsrf : slcsrf };
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'adminpasswordreminder.php', // the url where we want to POST
            data        : datatosend,
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        .done(function(data) {
          sessionExpiredReported(data,true);
          if (!data.success)
            bootbox.alert(data.message);
        })
        .fail(function() {
          bootbox.alert(ADMINMSG_NOTDELUSER);
        })
        .always(function() {
        $('body').removeClass('wait');  
      });
    }  

  });
}

function pluginUser(pluginid,pluginindex,plugin,user,userid,v5)
{
  if (v5)
    window.location.href = plugin+"?act=pluginuser&user="+urlencode(user)+"&pluginid="+pluginid+"&pluginindex="+pluginindex+"&userid="+userid;
  else
    window.location.href = plugin+"?slcsrf="+slcsrf+"&act=pluginuser&user="+urlencode(user)+"&pluginid="+pluginid+"&pluginindex="+pluginindex;
}

function urlencode(str)
{
    str = (str+'').toString();
    // Tilde should be allowed unescaped in future versions of PHP (as reflected below), but if you want to reflect current
    // PHP behavior, you would need to add ".replace(/~/g, '%7E');" to the following.
    return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}

function emailSelected()
{
  if (cbupdatewait)
    return;
  window.location.href = "emailuser.php?act=emailselected";
}

function deleteSelected()
{
  if (cbupdatewait)
    return;
  bootbox.confirm(ADMINMSG_DELETESELECTED+" ("+slnumselected+")?", function(result) {
      if (result)
      {
      $('body').addClass('wait');
      var datatosend = { slcsrf : slcsrf };
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'admindeleteselected.php', // the url where we want to POST
            data        : datatosend,
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        .done(function(data) {
          sessionExpiredReported(data,true);
          if (!data.success)
            bootbox.alert(data.message);
        })
        .fail(function() {
          bootbox.alert(ADMINMSG_NOTDELUSERS);
        })
        .always(function() {
          $('body').removeClass('wait');          
          sitelokdatatable.ajax.reload( null, false );
      });
    } 

});
}

function exportSelected()
{
  if (cbupdatewait)
    return;
  window.location.href = "exportusers.php";
}

function showFieldError(obj,msg)
{
  $(obj).parent().parent().addClass('has-error');
}

function showFormError(formid,msg)
{
  $('#'+formid).next().text(msg);
}

function removeFormErrors(formid)
{
  $('#'+formid+' .form-group').removeClass('has-error');
  $('#'+formid).next().text('');
}

function addFilterCondition()
{
  if (!advancedfilters[1])
  {
    advancedfilters[1]=true;
    updateAdvancedFilterFields();
    return;
  }  
  if (!advancedfilters[2])
  {
    advancedfilters[2]=true;
    updateAdvancedFilterFields();
    return;
  }
  if (!advancedfilters[3])
  {
    advancedfilters[3]=true;
    updateAdvancedFilterFields();
    return;
  }
}

function removeFilterCondition(num)
{
  advancedfilters[num]=false;
  updateAdvancedFilterFields();
}

function updateAdvancedFilterFields()
{
  if (!advancedfilters[0])
  {
    if (advancedfilters[1])
    {
      $('#filfield1').val($('#filfield2').val());
      $('#filcond1').val($('#filcond2').val());
      $('#fildata1').val($('#fildata2').val());
      advancedfilters[0]=true;  
      advancedfilters[1]=false;  
    }
    else if (advancedfilters[2])
    {
      $('#filfield1').val($('#filfield3').val());
      $('#filcond1').val($('#filcond3').val());
      $('#fildata1').val($('#fildata3').val());
      advancedfilters[0]=true;  
      advancedfilters[1]=false;  
    }
    else if (advancedfilters[3])
    {
      $('#filfield1').val($('#filfield4').val());
      $('#filcond1').val($('#filcond4').val());
      $('#fildata1').val($('#fildata4').val());
      advancedfilters[0]=true;  
      advancedfilters[1]=false;  
    }
  }  
  if (!advancedfilters[1])
  {
    if (advancedfilters[2])
    {
      $('#filfield2').val($('#filfield3').val());
      $('#filcond2').val($('#filcond3').val());
      $('#fildata2').val($('#fildata3').val());
      advancedfilters[1]=true;  
      advancedfilters[2]=false;  
    }
    else if (advancedfilters[3])
    {
      $('#filfield2').val($('#filfield4').val());
      $('#filcond2').val($('#filcond4').val());
      $('#fildata2').val($('#fildata4').val());
      advancedfilters[1]=true;  
      advancedfilters[2]=false;  
    }
  }  
  if (!advancedfilters[2])
  {
    if (advancedfilters[3])
    {
      $('#filfield3').val($('#filfield4').val());
      $('#filcond3').val($('#filcond4').val());
      $('#fildata3').val($('#fildata4').val());
      advancedfilters[2]=true;  
      advancedfilters[3]=false;  
    }
  }
  if (!advancedfilters[1])
  {
    $('#filfield2').val('');
    $('#filcond2').val('');
    $('#fildata2').val('');
    $('#filter2block').hide();            
  }
  else
    $('#filter2block').show();            
  if (!advancedfilters[2])
  {
    $('#filfield3').val('');
    $('#filcond3').val('');
    $('#fildata3').val('');        
    $('#filter3block').hide();            
  }
  else
    $('#filter3block').show();            
  if (!advancedfilters[3])
  {
    $('#filfield4').val('');
    $('#filcond4').val('');
    $('#fildata4').val('');
    $('#filter4block').hide();            
    $('#addfilterblock').show();            
  }
  else
  {
    $('#filter4block').show();
    $('#addfilterblock').hide();            
  }
}

function showActivity(userid,username)
{
  var datatosend = { slcsrf : slcsrf, logmanageact: "recentactivity", usernm: username, browsertimeoffset: adminLocalTzOffset };
  $('body').addClass('wait');
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : 'adminuseractivity.php', // the url where we want to POST
        data        : datatosend,
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
    .done(function(data) {
        sessionExpiredReported(data,true);
        if (data.success)
        {
            $('#activitycontent').html(data.activity);
            var title=
            $('#activitymodaltitle').text(username);
            $('#activitymodal').modal('show');
        }                  
        else
        {
          bootbox.alert(data.message);
        }
    })
    .fail(function() {
      bootbox.alert(ADMINMSG_NOLOG);
    })
    .always(function() {
      $('body').removeClass('wait');
      sitelokdatatable.ajax.reload( null, false );
  });
}

