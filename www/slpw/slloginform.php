<?php
@error_reporting (E_ERROR);
$slpagename=$_SERVER['PHP_SELF'];
if ($slpagename=="")
  $slpagename=$_SERVER['REQUEST_URI'];
if ($slpagename=="")
  $slpagename=$_SERVER['SCRIPT_NAME'];
$slpagename=basename($slpagename);

function sl_loginformhead($id,$simplestyle)
{
  $res=sl_get_login_formcode(false,$id,false,$simplestyle,$css,$js,$html);
  if ($res)
    echo $css."\n".$js;  
}
function sl_loginformbody($id,$simplestyle,$msg)
{
  $res=sl_get_login_formcode(false,$id,false,$simplestyle,$css,$js,$html,$msg);
  if ($res)
    echo $html;
  else
  {
    echo "<strong>Login form not found</strong>";
  }    
}
function sl_get_login_formcode($mysql_link,$id,$fullsource,$simplestyle,&$css,&$js,&$html,$msg="")
{
global $DbTableName;
global $SitelokLocationURL,$SitelokLocationURLPath,$slcookieusername,$slcookiepassword,$slcookielogin;
global $sl_ajaxforms;
global $startpage;
if ($mysql_link===false)
{
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
    return(false);
}
// If form is not register type then return
$mysql_result=mysqli_query($mysql_link,"SELECT type FROM sl_forms WHERE id=".sl_quote_smart($id));
if ($mysql_result===false)
  return(false);
$row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
if ($row===false)
  return(false);
if ($row['type']!="login")
  return(false);
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_loginforms WHERE id=".sl_quote_smart($id));
if ($mysql_result===false)
  return(false);
$row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
$fonttype=$row['mainfont'];
$formerrormsgcolor=$row['messagecolor'];
$formerrormsgsize=$row['messagesize'];
$formerrormsgstyle=$row['messagestyle'];
$usernamelabel=$row['usernamelabel'];
$usernameplaceholder=$row['usernameplaceholder'];
$passwordlabel=$row['passwordlabel'];
$passwordplaceholder=$row['passwordplaceholder'];
$includecaptcha=$row['includecaptcha'];
$captchalabel=$row['captchalabel'];
$captchaplaceholder=$row['captchaplaceholder'];
$includeremember=$row['includeremember'];
$rememberlabel=$row['rememberlabel'];
$includeautologin=$row['includeautologin'];
$autologinlabel=$row['autologinlabel'];
$labelcolor=$row['labelcolor'];
$labelsize=$row['labelsize'];
$labelstyle=$row['labelstyle'];
$inputtextcolor=$row['inputtextcolor'];
$inputtextsize=$row['inputtextsize'];
$inputtextstyle=$row['inputtextstyle'];
$inputbackcolor=$row['inputbackcolor'];
$bordersize=$row['bordersize'];
$bordercolor=$row['bordercolor'];
$borderradius=$row['borderradius'];
$inputpaddingv=$row['inputpaddingv'];
$inputpaddingh=$row['inputpaddingh'];
$btnlabel=$row['logintext'];
$btnlabelstyle=$row['logintextstyle'];
$btnlabelfont=$row['logintextfont'];
$btnlabelcolor=$row['logintextcolor'];
$btnlabelsize=$row['logintextsize'];
$btncolortype=$row['loginfilltype'];
$btncolorfrom=$row['logincolorfrom'];
$btncolorto=$row['logincolorto'];
$btnborderstyle=$row['btnborderstyle'];
$btnbordercolor=$row['btnbordercolor'];
$btnbordersize=$row['btnbordersize'];
$btnradius=$row['loginshape'];
$btnpaddingv=$row['btnpaddingv'];
$btnpaddingh=$row['btnpaddingh'];
$includeforgot=$row['includeforgot'];
$forgottext=$row['forgottext'];
$forgotcolor=$row['forgotcolor'];
$forgotsize=$row['forgotsize'];
$forgotstyle=$row['forgotstyle'];
$includesignup=$row['includesignup'];
$signuptext=$row['signuptext'];
$signupurl=$row['signupurl'];
$signupcolor=$row['signupcolor'];
$signupsize=$row['signupsize'];
$signupstyle=$row['signupstyle'];
$bottommargin=$row['bottommargin'];
$backcolor=$row['backgroundcolor'];
$maxformwidth=$row['maxformwidth'];

$border='solid';
if ($bordersize==0)
  $border='none';
$border.=' #'.$bordercolor.' '.$bordersize.'px'; 
if ($btncolortype=="solid")
  $btncolorto=$btncolorfrom;
if ($simplestyle)
  $captchaheight=30;
else
{
  if ($inputpaddingv=="0.3em")
    $captchaheight=($inputtextsize*1.75)+($bordersize*2);
  else
  {
    $inputpaddingvnum=str_replace('px','',$inputpaddingv);
    $captchaheight=($inputtextsize*1.2)+($inputpaddingvnum*2)+($bordersize*2);        
  }
  $captchaheight=round($captchaheight,2);
  if ($captchaheight<30) 
    $captchaheight=30;
}
$captchawidth=4;
if ($inputpaddingh!="0.3em")
{  
  $inputpaddinghnum=str_replace('px','',$inputpaddingh);
  $captchawidth=3+((2*$inputpaddinghnum)/$inputtextsize);
  $captchawidth=round($captchawidth,2);
  if ($captchawidth<4) 
    $captchawidth=4;
}
// Adjust settings for spinner
if ($simplestyle)
  $spinnerposition="form";
else
  $spinnerposition="button";

if ($spinnerposition=="button")
{
  if ($btnlabelsize>=14)
  {
    $spinnermargintop=intval((($btnlabelsize*1.16)-16)/2);
    $spinnersize=12;
  }
  else
  {
    $spinnermargintop=0;
    $spinnersize=intval($btnlabelsize/1.4);
  }
  $spinnermarginright=-19;
  $spinnercolorR=hexdec(substr($btnlabelcolor,0,2));
  $spinnercolorG=hexdec(substr($btnlabelcolor,2,2));
  $spinnercolorB=hexdec(substr($btnlabelcolor,4,2));
}
if ($spinnerposition=="form")
{
  $spinnersize=$btnlabelsize;
  $spinnercolorR=0;
  $spinnercolorG=179;
  $spinnercolorB=234;
  $spinnermargintop=0;
  $spinnermarginright=0;
}

if (!$simplestyle)
{
$css = <<<EOT
<style type="text/css">

div#slform_{$id}{
  border: none;
  padding: 0;
  margin: 0;
  width: 100%;
  max-width: {$maxformwidth}px;
}

/* Text fields */
div.sltextfield_{$id} {
  margin-bottom: {$bottommargin}px;
}

div#slform_{$id} div.sltextfield_{$id} label {
  display: block;
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding:0;
  text-transform: none;
}

div#slform_{$id} div.sltextfield_{$id} input[type="text"], input[type="email"], input[type="password"] {
  border: {$border};
  background-image: none;
  background-repeat: no-repeat;
  background-color:#{$inputbackcolor};
  font: {$inputtextstyle} {$inputtextsize}px {$fonttype};
  color: #{$inputtextcolor};
  margin: 0;
  width:100%;
  -moz-border-radius: {$borderradius}px;
  -khtml-border-radius: {$borderradius}px;
  border-radius: {$borderradius}px;	
  padding: {$inputpaddingv} {$inputpaddingh};
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

/* Checkbox fields */
div.slcbfield_{$id} {
  margin-bottom: {$bottommargin}px;
}

div#slform_{$id} div.slcbfield_{$id} label {  
  display: inline;
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  vertical-align: middle;
  margin: 0;
  padding:0;
  text-transform: none;
}

div#slform_{$id} div.slcbfield_{$id} input[type="checkbox"] {
  margin-left: 0;
  background-image: none;
  background-repeat: no-repeat;
  color: #{$inputtextcolor};
  font: {$labelstyle} {$labelsize}px {$fonttype};
  vertical-align: middle;
  margin: 0;
  padding: 0;
}

/* Captcha fields */
div.slcaptchafield_{$id} {
  margin-bottom: {$bottommargin}px;
}

div#slform_{$id} div.slcaptchafield_{$id} input[type="text"]{
  border: {$border};
  background-image: none;
  background-repeat: no-repeat;
  background-color:#{$inputbackcolor};
  font: {$inputtextstyle} {$inputtextsize}px {$fonttype};
  color: #{$inputtextcolor};
  width: {$captchawidth}em;
  margin: 0;
  padding: {$inputpaddingv} {$inputpaddingh};
  -moz-border-radius: {$borderradius}px;
  -khtml-border-radius: {$borderradius}px;
  border-radius: {$borderradius}px;
  max-width: {$maxformwidth}px;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;  
}

div#slform_{$id} div.slcaptchafield_{$id} label {
  display: block;  
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding:0;
  text-transform: none;
}

div#slform_{$id} div.slcaptchafield_{$id} img{
  vertical-align: top;
  height: {$captchaheight}px;
}

div#slform_{$id} div.slformmsg_{$id} {
  padding: 0;
  margin: 0 0 10px 0;
  text-align: left;
  color: #{$formerrormsgcolor};
  font: {$formerrormsgstyle} {$formerrormsgsize}px {$fonttype};  
}

a#slforgot_{$id}{
  color: #{$forgotcolor};
  font: {$forgotstyle} {$forgotsize}px {$fonttype};
  text-align: left;
  vertical-align: middle;
  padding: 0;
  margin: 0;
}

p#slsignup_{$id}{
  text-align: left;
  padding: 0;
  margin: {$bottommargin}px 0 0 0;
}

p#slsignup_{$id} a{
  color: #{$signupcolor};
  font: {$signupstyle} {$signupsize}px {$fonttype};
  text-align: left;
}

/* Button from http://www.bestcssbuttongenerator.com */
div#slform_{$id} #myButton_{$id} {
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #{$btncolorfrom}), color-stop(1, #{$btncolorto}));
  background:-moz-linear-gradient(top, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
  background:-webkit-linear-gradient(top, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
  background:-o-linear-gradient(top, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
  background:-ms-linear-gradient(top, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
  background:linear-gradient(to bottom, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#{$btncolorfrom}', endColorstr='#{$btncolorto}',GradientType=0);
  background-color:#{$btncolorfrom};
  -moz-border-radius: {$btnradius}px;
  -webkit-border-radius: {$btnradius}px;
  border-radius: {$btnradius}px;
  display:inline-block;
  cursor:pointer;
  color: #{$btnlabelcolor};
  font: $btnlabelstyle {$btnlabelsize}px {$btnlabelfont};
  padding: {$btnpaddingv}px {$btnpaddingh}px;
  text-decoration:none;
  border: $btnborderstyle #{$btnbordercolor} {$btnbordersize}px;
  margin: 0 5px 5px 0;
}
div#slform_{$id} #myButton_{$id}:hover {
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #{$btncolorto}), color-stop(1, #{$btncolorfrom}));
  background:-moz-linear-gradient(top, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
  background:-webkit-linear-gradient(top, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
  background:-o-linear-gradient(top, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
  background:-ms-linear-gradient(top, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
  background:linear-gradient(to bottom, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#{$btncolorto}', endColorstr='#{$btncolorfrom}',GradientType=0);
  background-color:#{$btncolorto};
}
div#slform_{$id} #myButton_{$id}:active {
  position:relative;
  top:1px;
}
/* End of button */
EOT;

if ($sl_ajaxforms)
{
$css .= <<<EOT
/* busy spinner */
div#slspinner_{$id} {
  display: none;
  box-sizing: content-box;
  float: right;
  height: {$spinnersize}px;
  width: {$spinnersize}px;
  margin-left: 0px;
  margin-right: {$spinnermarginright}px;
  margin-top: {$spinnermargintop}px;
  position: relative;
  -webkit-animation: rotation .6s infinite linear;
  -moz-animation: rotation .6s infinite linear;
  -o-animation: rotation .6s infinite linear;
  animation: rotation .6s infinite linear;
  border-left: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-right: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-bottom: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-top: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, 1);
  border-radius: 100%;
}

@-webkit-keyframes rotation {
  from {
    -webkit-transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(359deg);
  }
}

@-moz-keyframes rotation {
  from {
    -moz-transform: rotate(0deg);
  }
  to {
    -moz-transform: rotate(359deg);
  }
}

@-o-keyframes rotation {
  from {
    -o-transform: rotate(0deg);
  }
  to {
    -o-transform: rotate(359deg);
  }
}

@keyframes rotation {
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(359deg);
  }
}

EOT;
}

$css .= <<<EOT

</style>
<!--[if lt IE 9]>
<style type="text/css">
div#slform_{$id} {
  width: {$maxformwidth}px;
}
</style>
<![endif]-->

<!--[if lte IE 7]>
<style type="text/css">
div#slform_{$id} div.sltextfield_{$id} input[type="text"], input[type="email"], input[type="password"] {
  width: 95%;
}

div#slform_{$id} #myButton_{$id} {
  padding: 0px 0px;
}
</style>
<![endif]-->


EOT;
}
if ($simplestyle)
{
$css = <<<EOT
<style type="text/css">

div#slform_{$id}{
  border: none;
  padding: 0;
  margin: 0;
  width: 100%;
  max-width: {$maxformwidth}px;
}

/* Text fields */
div.sltextfield_{$id} {
  margin-bottom: {$bottommargin}px;
}

div#slform_{$id} div.sltextfield_{$id} label {
  display: block;
  margin: 0 0 2px 0;
  padding:0;
}

div#slform_{$id} div.sltextfield_{$id} input[type="text"], input[type="email"], input[type="password"] {
  margin: 0 0 0 0px;
	width:100%;
  /* box-sizing: border-box; */
}

/* Checkbox fields */
div.slcbfield_{$id} {
  margin-bottom: {$bottommargin}px;
}

div#slform_{$id} div.slcbfield_{$id} label {  
  display: inline;
	vertical-align: middle;
	margin: 0;
	padding:0;
}

div#slform_{$id} div.slcbfield_{$id} input[type="checkbox"] {
	margin-left: 0;
	vertical-align: middle;
	margin: 0;
	padding: 0;
}

/* Captcha fields */
div.slcaptchafield_{$id} {
  margin-bottom: {$bottommargin}px;
}

div#slform_{$id} div.slcaptchafield_{$id} input[type="text"]{
	margin: 0;
  max-width: {$maxformwidth}px;
}

div#slform_{$id} div.slcaptchafield_{$id} label {
  display: block;  
  margin: 0 0 2px 0;
  padding:0;
}

div#slform_{$id} div.slcaptchafield_{$id} img{
  vertical-align: top;
  height: {$captchaheight}px;
}

a#slforgot_{$id}{
  color: #{$forgotcolor};
	font: {$forgotstyle} {$forgotsize}px {$fonttype};
	text-align: left;
	vertical-align: middle;
	padding: 0;
	margin: 0;
}

p#slsignup_{$id}{
	text-align: left;
  padding: 0;
  margin: {$bottommargin}px 0 0 0;
}

p#slsignup_{$id} a{
  color: #{$signupcolor};
	font: {$signupstyle} {$signupsize}px {$fonttype};
	text-align: left;
}


div#slform_{$id} div.slformmsg_{$id} {
  padding: 0;
  margin: 0 0 10px 0;
  text-align: left;
  color: #{$formerrormsgcolor};
	font: {$formerrormsgstyle} {$formerrormsgsize}px {$fonttype};  
}

div#slform_{$id} #myButton_{$id} {
}
div#slform_{$id} #myButton_{$id}:hover {
}
div#slform_{$id} #myButton_{$id}:active {
}

EOT;

if ($sl_ajaxforms)
{
$css .= <<<EOT
/* busy spinner */
div#slspinner_{$id} {
  display: none;
  box-sizing: content-box;
  float: right;
  height: {$spinnersize}px;
  width: {$spinnersize}px;
  margin-left: 0px;
  margin-right: {$spinnermarginright}px;
  margin-top: {$spinnermargintop}px;
  position: relative;
  -webkit-animation: rotation .6s infinite linear;
  -moz-animation: rotation .6s infinite linear;
  -o-animation: rotation .6s infinite linear;
  animation: rotation .6s infinite linear;
  border-left: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-right: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-bottom: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-top: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, 1);
  border-radius: 100%;
}

@-webkit-keyframes rotation {
  from {
    -webkit-transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(359deg);
  }
}

@-moz-keyframes rotation {
  from {
    -moz-transform: rotate(0deg);
  }
  to {
    -moz-transform: rotate(359deg);
  }
}

@-o-keyframes rotation {
  from {
    -o-transform: rotate(0deg);
  }
  to {
    -o-transform: rotate(359deg);
  }
}

@keyframes rotation {
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(359deg);
  }
}

EOT;
}

$css .= <<<EOT
</style>
<!--[if lt IE 9]>
<style type="text/css">
div#slform_{$id} {
  width: {$maxformwidth}px;
}
</style>

<![endif]-->

<!--[if lte IE 7]>
<style type="text/css">
div#slform_{$id} div.sltextfield_{$id} input[type="text"], input[type="email"], input[type="password"] {
  width: 95%;
}
div#slform_{$id} div.sltextareafield_{$id} textarea{
  width: 95%;
}

div#slform_{$id} #myButton_{$id} {
	padding: 0px 0px;
}
</style>
<![endif]-->


EOT;
}
$html="<div id=\"slform_{$id}\">\n";
if ($fullsource)
  $html.="<form action=\"<?php echo \$startpage; ?>\" method=\"post\" id=\"siteloklogin_{$id}\" onSubmit=\"return slvalidateform_{$id}(this)\">\n";
else
  $html.="<form action=\"{$startpage}\" method=\"post\" id=\"siteloklogin_{$id}\" onSubmit=\"return slvalidateform_{$id}(this)\">\n";  
$html .= <<<EOT
<input type="hidden" id="loginformused_{$id}" name="loginformused" value="1">
<input type="hidden" id="forgotpassword_{$id}" name="forgotpassword" value="">

EOT;
$html.="<div id=\"slformmsg_{$id}\" class=\"slformmsg_{$id}\">";
if ($fullsource)
  $html.="<?php if (\$msg!=\"\") echo \$msg; ?>";
else
{
  if ($msg!="")
    $html.=$msg;
}  
$html.="</div>\n";

// Username field
$html .= <<<EOT

<div class="sltextfield_{$id}">
<label for="slfieldinput_{$id}_username">{$usernamelabel}</label>
<input type="text" name="username" id="slfieldinput_{$id}_username"
EOT;
if ($usernameplaceholder!='')
  $html.=" placeholder=\"{$usernameplaceholder}\"";
$html.=" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"off\"";
$html.=" maxlength=\"50\"";
if ($fullsource)
  $html.=" value=\"<?php print \$slcookieusername; ?>\"";  
else
  $html.=" value=\"".$slcookieusername."\"";      
$html.= ">\n";
$html .= "</div>\n";

// Password field
$html .= <<<EOT

<div class="sltextfield_{$id}">
<label for="slfieldinput_{$id}_password">{$passwordlabel}</label>
<input type="password" name="password" id="slfieldinput_{$id}_password"
EOT;
if ($passwordplaceholder!='')
  $html.=" placeholder=\"{$passwordplaceholder}\""; 
$html.=" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"off\" maxlength=\"50\"";         
if ($fullsource)
  $html.=" value=\"<?php print \$slcookiepassword; ?>\"";  
else
  $html.=" value=\"".$slcookiepassword."\"";      
$html .= ">\n";
$html .= "</div>\n";

// Remember me field
if ($includeremember=="1")
{
if ($fullsource)
{
$html .= <<<EOT

<div class="slcbfield_{$id}">
<input type="checkbox" name="remember" id="slfieldinput_{$id}_remember" value="1" <?php if (\$slcookielogin=="1") echo "checked"; ?> >
<label for="slfieldinput_{$id}_remember">{$rememberlabel}</label>
</div>

EOT;
}
else
{
$html .= <<<EOT

<div class="slcbfield_{$id}">
<input type="checkbox" name="remember" id="slfieldinput_{$id}_remember" value="1"

EOT;
if ($slcookielogin=="1")
  $html.=" checked ";
$html .= <<<EOT

>
<label for="slfieldinput_{$id}_remember">{$rememberlabel}</label>
</div>

EOT;
}
}

// Remember me field
if ($includeautologin=="1")
{
if ($fullsource)
{
$html .= <<<EOT

<div class="slcbfield_{$id}">
<input type="checkbox" name="remember" id="slfieldinput_{$id}_remember" value="2" <?php if (\$slcookielogin=="2") echo "checked"; ?>>
<label for="slfieldinput_{$id}_remember">{$autologinlabel}</label>
</div>

EOT;
}
else
{
$html .= <<<EOT

<div class="slcbfield_{$id}">
<input type="checkbox" name="remember" id="slfieldinput_{$id}_remember" value="2"

EOT;
if ($slcookielogin=="2")
  $html.=" checked ";
$html .= <<<EOT

>
<label for="slfieldinput_{$id}_remember">{$autologinlabel}</label>
</div>

EOT;
}
}
// Captcha field
if ($includecaptcha=="1")
{
$html .= <<<EOT

<div class="slcaptchafield_{$id}">
<label for="slfieldinput_{$id}_captcha">{$captchalabel}
</label>
EOT;

if ($fullsource)
{
  $html .="<?php\n";
  $html .="if (function_exists('slshowcaptcha'))\n";
  $html .="{\n";
  $html.="echo slshowcaptcha();\n";
  $html .="}\n";
  $html .="else\n";
  $html .="{\n";
  $html .="?>\n";  
  $html .= <<<EOT

<input type="text" name="turing" id="slfieldinput_{$id}_captcha" placeholder="$captchaplaceholder" maxlength="5" size="6" autocorrect="off" autocapitalize="off" spellcheck="off" autocomplete="off">
&nbsp;
EOT;
  $html.="<img src=\"<?php echo \$SitelokLocationURLPath; ?>turingimage.php\" height=\"{$captchaheight}\" title=\"{$captchalabel}\" alt=\"{$captchalabel}\">\n";
  $html .="<?php\n";  
  $html .="}\n";
  $html .="?>\n";
}
else
{
  if (function_exists('slshowcaptcha'))
  {
    $html.=slshowcaptcha();
  }  
  else
  {    
$html .= <<<EOT

<input type="text" name="turing" id="slfieldinput_{$id}_captcha" placeholder="$captchaplaceholder" maxlength="5" size="6" autocorrect="off" autocapitalize="off" spellcheck="off" autocomplete="off">
&nbsp;

EOT;
  $html.="<img src=\"{$SitelokLocationURLPath}turingimage.php\" height=\"{$captchaheight}\" title=\"{$captchalabel}\" alt=\"{$captchalabel}\">\n";
  }
} 
  $html .= "</div>\n";
}

// Button
if ($sl_ajaxforms)
{
  if ($spinnerposition=="button")
    $html.="<button id=\"myButton_".$id."\" type=\"submit\">".$btnlabel."<div id=\"slspinner_".$id."\"></div></button>\n";
  if ($spinnerposition=="form")
    $html.="<button id=\"myButton_{$id}\" type=\"submit\">".$btnlabel."</button><div id=\"slspinner_".$id."\"></div>\n";
}
else
  $html.="<button id=\"myButton_{$id}\" type=\"submit\">".$btnlabel."</button>\n";

if ($includeforgot)
  $html .= "<a id=\"slforgot_{$id}\" href=\"javascript: void forgotpw_{$id}()\" title=\"Forgot your password? Enter username or email &amp; click link\">{$forgottext}</a>\n";
if ($includesignup)
  $html .= "<p id=\"slsignup_{$id}\"><a id=\"signuplink\" href=\"{$signupurl}\">{$signuptext}</a></p>\n";

$html .= <<<EOT
</form>
</div>

EOT;

$MSG_ENTERUSER=MSG_ENTERUSER;
$MSG_ENTERPASS=MSG_ENTERPASS;
$MSG_ENTERTURING=MSG_ENTERTURING;
$MSG_FORGOT1=MSG_FORGOT1;
$MSG_FORGOT2=MSG_FORGOT2;

$js="";
$js .= <<<EOT

<script type="text/javascript">
function slvalidateform_{$id}(form)
{
  var loginmessage=document.getElementById('slformmsg_{$id}')
  loginmessage.innerHTML=''
  var username=document.getElementById('slfieldinput_{$id}_username')	
  if (sltrim_{$id}(username.value)=="")
  {
    loginmessage.innerHTML='{$MSG_ENTERUSER}'	
    username.focus()
    return(false)
  }
  var password=document.getElementById('slfieldinput_{$id}_password')	
  if (sltrim_{$id}(password.value)=="")
  {
    loginmessage.innerHTML='{$MSG_ENTERPASS}'	
    password.focus()
    return(false)
  }
EOT;
if ($includecaptcha=="1")
{
  if (!function_exists('slshowcaptcha'))
  {
$js .= <<<EOT

  var captcha=document.getElementById('slfieldinput_{$id}_captcha')	
  if (sltrim_{$id}(captcha.value)=="")
  {
    loginmessage.innerHTML='{$MSG_ENTERTURING}'	
    captcha.focus()
    return(false)
  }

EOT;
}
}
if ($sl_ajaxforms)
{
$js .= <<<EOT
  loginmessage.innerHTML='';
  var slajaxavailable=false;
  if ((window.XMLHttpRequest) && (typeof JSON === "object"))
    slajaxavailable=true;
  if (slajaxavailable)
  {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {
      if (xhttp.readyState == 4 && xhttp.status == 200)
      {
        // Handle callback
        document.getElementById('myButton_{$id}').disabled=false
        document.getElementById('slspinner_{$id}').style['display']="none"
        var data = JSON.parse(xhttp.responseText);
        if (data.success)
        {
          window.location=data.redirect;
          return(false);
        }
        else  
        {  
          loginmessage.innerHTML = data.message;
          return(false);
        }
      }
    };
    // Serialize form
    var formData=sl_serialize(form);
    formData+="&slajaxform=1";
    var slfrmact=form.action;
    if (slfrmact=="")
      slfrmact=window.location.href;
    document.getElementById('myButton_{$id}').disabled=true
    document.getElementById('slspinner_{$id}').style['display']="block"    
    xhttp.open("POST", slfrmact, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(formData);
    return(false);
  }

EOT;
}
$js .= <<<EOT
  return(true);
}
function forgotpw_{$id}()
{
  var loginmessage=document.getElementById('slformmsg_{$id}')
  loginmessage.innerHTML=''
  var username=document.getElementById('slfieldinput_{$id}_username')	
  if (sltrim_{$id}(username.value)=="")
  {
  
EOT;
if ($includecaptcha=="1")
  $js .= "   loginmessage.innerHTML='{$MSG_FORGOT1}'\n";	  	
else
  $js .= "   loginmessage.innerHTML='{$MSG_FORGOT2}'\n";
$js .= <<<EOT
    username.focus()
    return(false)
  }
EOT;
if ($includecaptcha=="1")
{
  if (!function_exists('slshowcaptcha'))
  {
$js .= <<<EOT

  var captcha=document.getElementById('slfieldinput_{$id}_captcha')	
  if (sltrim_{$id}(captcha.value)=="")
  {
    loginmessage.innerHTML='{$MSG_ENTERTURING}'	
    captcha.focus()
    return(false)
  }
EOT;
}
}
$js .= <<<EOT

  var forgotpassword=document.getElementById('forgotpassword_{$id}')	
  forgotpassword.value="forgotten-it"
  var form=document.getElementById('siteloklogin_{$id}')

EOT;
if ($sl_ajaxforms)
{
$js .= <<<EOT
  loginmessage.innerHTML='';
  var slajaxavailable=false;
  if ((window.XMLHttpRequest) && (typeof JSON === "object"))
    slajaxavailable=true;
  if (slajaxavailable)
  {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {
      if (xhttp.readyState == 4 && xhttp.status == 200)
      {
        // Handle callback
        document.getElementById('myButton_{$id}').disabled=false
        document.getElementById('slspinner_{$id}').style['display']="none"
        var data = JSON.parse(xhttp.responseText);
        if (data.success)
        {
          forgotpassword.value="";          
          window.location=data.redirect;
          return(false);
        }
        else  
        {  
          forgotpassword.value="";
          loginmessage.innerHTML = data.message;
          return(false);
        }
      }
    };
    // Serialize form
    var formData=sl_serialize(form);
    formData+="&slajaxform=1";
    var slfrmact=form.action;
    if (slfrmact=="")
      slfrmact=window.location.href;
    document.getElementById('myButton_{$id}').disabled=true
    document.getElementById('slspinner_{$id}').style['display']="block"    
    xhttp.open("POST", slfrmact, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(formData);
    return(false);
  }

EOT;
}
$js .= <<<EOT
  form.submit() 
  return(true)
}

function sltrim_{$id}(x)
{
    return x.replace(/^\s+|\s+$/gm,'');
}

EOT;
if ($sl_ajaxforms)
{
$js .= <<<EOT
function sl_serialize(form)
{
 if (!form || form.nodeName !== "FORM") {
   return;
 }
 var i, j, q = [];
 for (i = form.elements.length - 1; i >= 0; i = i - 1) {
   if (form.elements[i].name === "") {
     continue;
   }
   switch (form.elements[i].nodeName) {
   case 'INPUT':
     switch (form.elements[i].type) {
     case 'text':
     case 'email':
     case 'number':
     case 'hidden':
     case 'password':
     case 'button':
     case 'reset':
     case 'submit':
       q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
       break;
     case 'checkbox':
     case 'radio':
       if (form.elements[i].checked) {
         q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
       }           
       break;
     case 'file':
       break;
     }
     break;       
   case 'TEXTAREA':
     q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
     break;
   case 'SELECT':
     switch (form.elements[i].type) {
     case 'select-one':
       q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
       break;
     case 'select-multiple':
       for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
         if (form.elements[i].options[j].selected) {
           q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].options[j].value));
         }
       }
       break;
     }
     break;
   case 'BUTTON':
     switch (form.elements[i].type) {
     case 'reset':
     case 'submit':
     case 'button':
       q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
       break;
     }
     break;
   }
 }
 return q.join("&");
}

EOT;
}
$js .= <<<EOT
</script>

EOT;
if ($fullsource)
{
  $js .="<?php\n";
  $js .="if (function_exists('slcaptchahead'))\n";
  $js .="  echo slcaptchahead();\n";
  $js .="?>\n";    
}
else
{
  if (function_exists('slcaptchahead'))
    $js.=slcaptchahead();  
}
// Adjust for RW compatibilty
$html=sl_rwadjust($html);
return(true);
}
?>