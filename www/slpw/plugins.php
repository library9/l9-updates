<?php
  $groupswithaccess="ADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
?>
<!DOCTYPE html>
<html>
<head>
<?php
$pagename="plugins";
include("adminhead.php");
?>
<title><?php echo ADMINMENU_MANAGEPLUGINS; ?></title>
<link rel="stylesheet" href="plugins.css"></head>
<?php include("adminthemeheader.php"); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-puzzle-piece"></i>&nbsp;<?php echo ADMINMENU_MANAGEPLUGINS; ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><?php echo ADMINMENU_MANAGEPLUGINS; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>
          <!-- Your Page Content Here -->
          <form name="form" role="form" class="form-horizontal">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">

                <div class="box-body">

                  <div id="pluginlist">
                  </div>


                </div><!-- /.box-body -->
              </div><!-- /.box -->

              <div class="form-group">
                <div class="col-xs-12">
                  <div class="btn-toolbar">
                      <button type="button" id="cancel" class="btn btn-primary pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                      <button type="button" id="cancel" class="btn btn-default pull-left" onclick="checkForPluginUpdates();"><?php echo ADMINUPDATE_CHKUPDATES ?></button>
                  </div> 
                </div>    
              </div>

          <div id="resultplugin"></div>


            </div><!-- /.col -->


          </div><!-- /.row -->
          </form>  

<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>
   
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("adminthemefooter.php"); ?>
    <script src="plugins.js"></script>

  </body>
</html>
