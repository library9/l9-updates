<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    ?>
    {
    "success": false,
    "message": "<?php echo ADMINMSG_CSRFFAILED; ?>"
    }
    <?php
    exit;      
  }   
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    ?>
    {
    "success": false,
    "message": "<?php echo ADMINMSG_NOTDELUSER; ?>. <?php echo ADMINMSG_MYSQLERROR; ?>."
    }
    <?php
    exit;  
  }
  $userid=$_POST['userid'];
  if (is_numeric($userid))
  {

    if (!$DemoMode)
    {
      if ($slsubadmin)
         $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." WHERE ".$IdField."=".sl_quote_smart($userid)." AND ".$UsergroupsField." NOT REGEXP '(^|\\\^)ADMIN(:|\\\^|$)'");
      else
         $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." WHERE ".$IdField."=".sl_quote_smart($userid));                
      if ($mysql_result!=false)
      {
        $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
        if ($row!==null)
        {
          $paramdata['oldusername']=$row[$UsernameField];
          $paramdata['username']=$row[$UsernameField];
          $paramdata['userid']=$row[$IdField];        
          $paramdata['created']=$row[$CreatedField];        
          $paramdata['password']=$row[$PasswordField];
          $paramdata['enabled']=$row[$EnabledField];
          $paramdata['name']=$row[$NameField];
          $paramdata['email']=$row[$EmailField];
          $paramdata['usergroups']=$row[$UsergroupsField];
          $paramdata['custom1']=$row[$Custom1Field];
          $paramdata['custom2']=$row[$Custom2Field];
          $paramdata['custom3']=$row[$Custom3Field];
          $paramdata['custom4']=$row[$Custom4Field];
          $paramdata['custom5']=$row[$Custom5Field];
          $paramdata['custom6']=$row[$Custom6Field];
          $paramdata['custom7']=$row[$Custom7Field];
          $paramdata['custom8']=$row[$Custom8Field];
          $paramdata['custom9']=$row[$Custom9Field];
          $paramdata['custom10']=$row[$Custom10Field];
          $paramdata['custom11']=$row[$Custom11Field];
          $paramdata['custom12']=$row[$Custom12Field];
          $paramdata['custom13']=$row[$Custom13Field];
          $paramdata['custom14']=$row[$Custom14Field];
          $paramdata['custom15']=$row[$Custom15Field];
          $paramdata['custom16']=$row[$Custom16Field];
          $paramdata['custom17']=$row[$Custom17Field];
          $paramdata['custom18']=$row[$Custom18Field];
          $paramdata['custom19']=$row[$Custom19Field];
          $paramdata['custom20']=$row[$Custom20Field];
          $paramdata['custom21']=$row[$Custom21Field];
          $paramdata['custom22']=$row[$Custom22Field];
          $paramdata['custom23']=$row[$Custom23Field];
          $paramdata['custom24']=$row[$Custom24Field];
          $paramdata['custom25']=$row[$Custom25Field];
          $paramdata['custom26']=$row[$Custom26Field];
          $paramdata['custom27']=$row[$Custom27Field];
          $paramdata['custom28']=$row[$Custom28Field];
          $paramdata['custom29']=$row[$Custom29Field];
          $paramdata['custom30']=$row[$Custom30Field];
          $paramdata['custom31']=$row[$Custom31Field];
          $paramdata['custom32']=$row[$Custom32Field];
          $paramdata['custom33']=$row[$Custom33Field];
          $paramdata['custom34']=$row[$Custom34Field];
          $paramdata['custom35']=$row[$Custom35Field];
          $paramdata['custom36']=$row[$Custom36Field];
          $paramdata['custom37']=$row[$Custom37Field];
          $paramdata['custom38']=$row[$Custom38Field];
          $paramdata['custom39']=$row[$Custom39Field];
          $paramdata['custom40']=$row[$Custom40Field];
          $paramdata['custom41']=$row[$Custom41Field];
          $paramdata['custom42']=$row[$Custom42Field];
          $paramdata['custom43']=$row[$Custom43Field];
          $paramdata['custom44']=$row[$Custom44Field];
          $paramdata['custom45']=$row[$Custom45Field];
          $paramdata['custom46']=$row[$Custom46Field];
          $paramdata['custom47']=$row[$Custom47Field];
          $paramdata['custom48']=$row[$Custom48Field];
          $paramdata['custom49']=$row[$Custom49Field];
          $paramdata['custom50']=$row[$Custom50Field];
          // Call plugin event
          for ($p=0;$p<$slnumplugins;$p++)
          {
            if (function_exists($slplugin_event_onDeleteUser[$p]))
              call_user_func($slplugin_event_onDeleteUser[$p],$slpluginid[$p],$paramdata);
          }
          // Call user event handler       
          if (function_exists("sl_onDeleteUser"))
            sl_onDeleteUser($paramdata);  
                // Delete the session data associated with this user if known.
          if ($row[$SessionField]!="")
          {
            $ThisSession=session_id();
            session_id($row[$SessionField]);
            @session_destroy();
            if ($SessionName!="")
              session_name($SessionName);
            session_id($ThisSession);
            session_start();     
          }
          $mysql_result=mysqli_query($mysql_link,"DELETE FROM ".$DbTableName." WHERE ".$UsernameField."=".sl_quote_smart($row[$UsernameField]));
          // Process after deleting user
          $res=sl_userdeleted($row[$UsernameField],$row[$IdField]);
          if (!res)
          {
            ?>
            {
            "success": false,
            "message": "<?php ADMINMSG_NOTDELUSER; ?>"
            }
            <?php
            exit;  
          }
        }  
      }
    }

  }

?>
{
  "success": true
}

