<?php
$groupswithaccess="ADMIN,SUBADMIN";
$noaccesspage="";
$adminajaxcall=true;
require("sitelokpw.php");
if (!isset($adminlanguage))
  $adminlanguage="en";
require("adminlanguage-".$adminlanguage.".php");  
$slsubadmin=false;
if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
  $slsubadmin=true;
// Check CSRF value
if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
{
  ?>
  {
  "success": false,
  "message": "<?php echo ADMINMSG_CSRFFAILED; ?>"
  }
  <?php
  exit;      
}   
$filename=$_POST['filename'];
$subject=$_POST['subject'];
$content=$_POST['content'];
$htmlformat=$_POST['htmlformat'];
if ($htmlformat=="html")
  $htmlformat="Y";
else
  $htmlformat="N";
// Double check path doesn't start with / or \ or contain .. or :
if ((substr($filename,0,1)=="/") || (substr($filename,0,1)=="\\") || (false!=(strpos($filename, ".."))) || (false!=(strpos($filename, ":"))))
{
?>
{
  "success": false,
  "message": "<?php echo ADMINMSG_TEMPLATENOLOAD; ?>"
}
<?php
exit;
}
// Check valid file extension
$ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
if (($ext!="html") && ($ext!="htm") && ($ext!="txt"))
{
?>
{
  "success": false,
  "message": "<?php echo ADMINMSG_TEMPLATETYPE; ?>"
}
<?php
exit;
}

if (!is_writable($EmailLocation.$filename))
  @chmod($EmailLocation.$filename,0777);
$myfile=@fopen($EmailLocation.$filename,"w");
if ($myfile===false)
{
  ?>
  {
  "success": false,
  "message": "<?php echo "Can't save template"; ?>"
  }
  <?php
  exit;      
}  
else
{ 
  if ($htmlformat!="Y")
    fwrite($myfile,$subject."\n".$content);
  else
  {
    //See if <title> </title> tags exist
    $content=preg_replace("/<title>.*<\/title>/i", "<title>".$subject."</title>",$content,-1,$count);
    if ($count<1)
      $content=preg_replace("/<head>/i","<head><title>".$subject."</title>", $content);           
    // Standardise the eachgroup comments
    $content=preg_replace("/<!--.{0,4}eachgroupstart.{0,4}-->/i","<!--eachgroupstart-->",$content);
    $content=preg_replace("/<!--.{0,4}eachgroupend.{0,4}-->/i","<!--eachgroupend-->",$content);
    // Firefox converts ! to %21, ( to %28, ) to %29, & to &amp; so convert these back.
    $content=str_replace("%21","!",$content);
    $content=str_replace("%28","(",$content);
    $content=str_replace("%29",")",$content);        
    $content=str_replace("&amp;","&",$content);
    $content=str_replace(chr(0xC2).chr(0xA0)," ",$content); 
    // Remove any contentEditiable="true" as this causes issues with IE9 on some systems
    $content=str_replace("contentEditable=\"true\"","",$content);
    fwrite($myfile,$content);
  }  
  fclose($myfile);
}

?>
{
  "success": true,
  "message": ""
}