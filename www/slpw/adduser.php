<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $startpage="index.php";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  include("admincommonfunctions.php");
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    die(ADMINMSG_MYSQLERROR);
    exit;
  }
  // Get configuration data
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_adminconfig LIMIT 1");
  if ($mysql_result!=false)
  {
    $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
    if ($row!=false)
    {
      $sendnewuseremail=$row["sendnewuseremail"];
    }
  }  
  // get field lengths and other info for custom fields
  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." LIMIT 1");
  $fcharset=mysqli_get_charset($mysql_link);
  $finfo=mysqli_fetch_fields($mysql_result);
  $fieldlengths=array();
  $fieldtitle=array();
  for ($k=1;$k<=50;$k++)
  {
    $fieldlengths[$k] = ($finfo[$k+7]->length)/$fcharset->max_length;
    $titlevar="CustomTitle".$k;
    $fieldtitle[$k]=$$titlevar;
  }
  $groupcount=1;
?>
<!DOCTYPE html>
<html>
<head>
<?php
$pagename="adduser";
include("adminhead.php");
?>
<title><?php echo ADMINBUTTON_ADDUSER; ?></title>
<link rel="stylesheet" type="text/css" href="maxlength/jquery.maxlength.css">
<link rel="stylesheet" type="text/css" href="fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />
<link rel="stylesheet" href="adduser.css">
</head>
<?php
include("adminthemeheader.php");
?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1><i class="fa fa-user-plus"></i>&nbsp;<?php echo ADMINBUTTON_ADDUSER; ?></h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><?php echo ADMINBUTTON_ADDUSER; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>


          <div class="row">


                    <form id="adduserform" role="form" class="form-horizontal" method="post">
                      <input type="hidden" name="slcsrf" id="slcsrf" value="<?php echo $slcsrftoken; ?>">
                      <input type="hidden" name="existingsendemail" value="<?php echo $sendnewuseremail; ?>">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">


                      <div class="box box-default">
                        <div class="box-header">
                          <h3 class="box-title"><?php echo ADMINGENERAL_USERDETAILS; ?></h3>
                        </div>
                        <div class="box-body">


                          <div class="form-group" id="usergroup">
                              <label class="col-xs-12" for="user" id="labeluser"><?php echo ADMINFIELD_USERNAME; ?></label>
                              <div class="col-xs-12" id="userdiv">
                                  <input type="text" class="form-control" name="user" id="user" maxlength="100" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                              </div>
                          </div>

                          <div class="form-group" id="pssgroup">
                              <label class="col-xs-12" for="pss" id="labelpss"><?php echo ADMINFIELD_PASSWORD; ?></label>
                              <div class="col-xs-12" id="pssdiv">
                                  <div class="input-group">
                                    <input type="text" class="form-control" name="pss" id="pss" maxlength="255" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                    <span class="input-group-btn">
                                      <button class="btn btn-default" type="button" onclick="$('#pss').val(randomPassword());"><?php echo ADMINBUTTON_RANDOM; ?></button>
                                    </span>
                                  </div>
                              </div>
                          </div>

                          <div class="form-group" id="nmgroup">
                              <label class="col-xs-12" for="nm" id="labelnm"><?php echo ADMINFIELD_NAME; ?></label>
                              <div class="col-xs-12" id="nmdiv">
                                  <input type="text" class="form-control" name="nm" id="nm" maxlength="100" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                              </div>
                          </div>

                          <div class="form-group" id="emgroup">
                              <label class="col-xs-12" for="em" id="labelem"><?php echo ADMINFIELD_EMAIL; ?></label>
                              <div class="col-xs-12" id="emdiv">
                                  <input type="text" class="form-control" name="em" id="em" maxlength="100" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                              </div>
                          </div>

                          <div class="form-group" id="engroup">
                              <div class="col-sm-10" id="endiv">
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" name="en" id="en" value="Yes" checked >&nbsp;&nbsp;<?php echo ADMINFIELD_ENABLED; ?>
                                  </label>
                                </div>
                              </div>
                          </div>

                        </div>
                      </div>  


                      <div class="box box-default">
                        <div class="box-header">
                          <h3 class="box-title"><?php echo ADMINFIELD_USERGROUPS; ?></h3>
                        </div>
                        <div class="box-body">



                           <div id="groupblock1"> 
                             <div class="form-group">
                                <div class="col-xs-6 col-sm-5 col-md-5 col-lg-6" id="group1div">
                                   <select placeholder="<?php echo strtolower(ADMINFILTER_USERGROUP); ?>" class="form-control usergrouplist" id="group1" name="group1">
                                   <?php populateUsergroupSelect(); ?>
                                   </select> 
                                </div>

                                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4" id="expiry1div">
                                  <div class="input-group datepicker" id="dateexpiry1">
                                     <input type="text" placeholder="<?php echo ADMINADDUSER_EXPIREPLACE.$DateFormat; ?>" class="form-control" id="expiry1" name="expiry1" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                     <div class="input-group-addon"><span class="glyphicon glyphicon-calendar actionicon"></span></div>
                                  </div>   
                                </div>

                                <div class="col-sm-3 col-md-3 col-lg-2 hidden-xs" id="expiry1div">
                                  <span class="glyphicon glyphicon-triangle-top actionicon" title="<?php echo ADMINBUTTON_MOVEUP; ?>" onclick="groupUp(1);"></span>
                                  <span class="glyphicon glyphicon-triangle-bottom actionicon" title="<?php echo ADMINBUTTON_MOVEDOWN; ?>" onclick="groupDown(1);"></span>
                                  <span class="glyphicon glyphicon-remove removegroup actionicon" title="<?php echo ADMINBUTTON_REMOVE; ?>" onclick="removeGroup(1);"></span>
                                </div>
                            </div>
                          </div>



                          <div id="extraGroupTemplate">
<!--                            <div id="groupblockXX">
                              <div class="form-group">
                                <div class="col-xs-6 col-sm-5 col-md-5 col-lg-6" id="groupXXdiv">
                                   <select placeholder="<?php echo strtolower(ADMINFILTER_USERGROUP); ?>" class="form-control usergrouplist" id="groupXX" name="groupXX">
                                   <?php populateUsergroupSelect(); ?>                 
                                   </select> 
                                </div>

                                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4" id="expiryXXdiv">
                                  <div class="input-group datepicker" id="dateexpiryXX">
                                     <input type="text" placeholder="<?php echo ADMINADDUSER_EXPIREPLACE.$DateFormat; ?>" class="form-control" id="expiryXX"  name="expiryXX" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                     <div class="input-group-addon"><span class="glyphicon glyphicon-calendar actionicon"></span></div>
                                  </div>   
                                </div>

                                <div class="col-sm-3 col-md-3 col-lg-2 hidden-xs" id="expiryXXdiv">
                                  <span class="glyphicon glyphicon-triangle-top actionicon" title="<?php echo ADMINBUTTON_MOVEUP; ?>" onclick="groupUp(XX);"></span>
                                  <span class="glyphicon glyphicon-triangle-bottom actionicon" title="<?php echo ADMINBUTTON_MOVEDOWN; ?>" onclick="groupDown(XX);"></span>
                                  <span class="glyphicon glyphicon-remove removegroup actionicon" title="<?php echo ADMINBUTTON_REMOVE; ?>" onclick="removeGroup(XX);"></span>
                                </div>
                            </div
                          </div>
 -->                     </div>


                        <div id="extraGroupInsert"></div>



                          <div class="form-group">
                              <div class="col-xs-12">
                                <button type="button" class="btn btn-xs btn-default" onclick="addGroup();">
                                  <span class="glyphicon glyphicon-plus actionicon"></span>
                                </button>
                              </div>
                          </div>


                        </div>
                    </div>



                      <div class="box box-default">
                        <div class="box-header">
                          <h3 class="box-title"><?php echo ADMINGENERAL_OTHEROPTIONS; ?></h3>
                        </div>                      
                        <div class="box-body">


                          <div class="form-group" id="sendemailgroup">
                              <div class="col-sm-10">
                                <div class="checkbox" id="sendemaildiv">
                                  <label>
                                    <input type="checkbox" id="sendemail" name="sendemail" value="1">&nbsp;&nbsp;<?php echo ADMINADDUSER_EMAILUSER; ?>
                                  </label>
                                </div>
                              </div>
                          </div>

                          <div id="templatediv" onclick="openFileManager();">
                          <input type="hidden" id="template" name="template" value="<?php echo $NewUserEmail; ?>">
                          <span id="templatename"><?php if ($NewUserEmail!="") echo $NewUserEmail; else echo ADMINMSG_NOTEMPLATE; ?></span>&nbsp;&nbsp;<span><i class="glyphicon glyphicon-folder-open actionicon"></i></span>
                          </div>

                        </div>
                      </div>  



                    <div class="form-group">
                    <div class="col-sm-11">
                        <div class="btn-toolbar">
                            <button type="submit" id="submitadduser" class="btn btn-primary pull-left"><?php echo ADMINBUTTON_ADDUSER; ?></button>
                            <button type="button" id="canceladduser" class="btn btn-default pull-left"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                        </div>   
                    </div>
                    </div>

                      <div id="resultadduser"></div>




</div><!-- /.col -->
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">



                      <div class="box box-default">
                        <div class="box-header">
                          <h3 class="box-title"><?php echo ADMINGENERAL_CUSTOMFIELDS; ?></h3>
                        </div>
                        <div class="box-body">

<?php
$customcols="12";
if ($downloadicon)
  $customcols="10";


for ($k=1;$k<=50;$k++)
{
if ($fieldtitle[$k]!="")
{
  if ($fieldlengths[$k]>255)
  {
?>

                          <div class="form-group" id="<?php echo $fieldvalue[$k]; ?>group">
                              <label class="col-xs-12" for="cu<?php echo $k; ?>" id="labelcu<?php echo $k; ?>"><?php echo $fieldtitle[$k]; ?></label>
                              <div class="col-xs-<?php echo $customcols; ?>" id="cu<?php echo $k; ?>div">
                                  <textarea class="form-control overlaidLength" name="cu<?php echo $k; ?>" id="cu<?php echo $k; ?>" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"><?php echo htmlentities($fieldvalue[$k],ENT_QUOTES,strtoupper($MetaCharSet)); ?></textarea>
                              </div>
                              <div class="col-sm-2 col-md-2 col-lg-2">
                                <?php if ($download[$k]!="") { ?><a href="<?php echo $download[$k]; ?>"><span class="glyphicon glyphicon-download-alt actionicon" title="<?php echo ADMINBUTTON_DOWNLOAD; ?>"></span></a><?php } ?>
                                <?php if ($inline[$k]!="") { ?><a href="#" data-featherlight="<?php echo $inline[$k]; ?>"><span class="glyphicon glyphicon-eye-open actionicon" title="<?php echo ADMINBUTTON_VIEW; ?>"></span></a><?php } ?>
                              </div>
                          </div>


<?php } else { ?>
                          <div class="form-group" id="text1group">
                              <label class="col-xs-12" for="cu<?php echo $k; ?>" id="labelcu<?php echo $k; ?>"><?php echo $fieldtitle[$k]; ?></label>
                              <div class="col-xs-<?php echo $customcols; ?>" id="cu<?php echo $k; ?>div">
                                  <input type="text" class="form-control" name="cu<?php echo $k; ?>" id="cu<?php echo $k; ?>" maxlength="<?php echo $fieldlengths[$k]; ?>" value="<?php echo htmlentities($fieldvalue[$k],ENT_QUOTES,strtoupper($MetaCharSet)); ?>" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                              </div>
                              <div class="col-sm-2 col-md-2 col-lg-2">
                                <?php if ($download[$k]!="") { ?><a href="<?php echo $download[$k]; ?>"><span class="glyphicon glyphicon-download-alt actionicon" title="<?php echo ADMINBUTTON_DOWNLOAD; ?>"></span></a><?php } ?>
                                <?php if ($inline[$k]!="") { ?><a href="#" data-featherlight="<?php echo $inline[$k]; ?>"><span class="glyphicon glyphicon-eye-open actionicon" title="<?php echo ADMINBUTTON_VIEW; ?>"></span></a><?php } ?>
                              </div>

                          </div>

<?php } ?>
<?php } ?>
<?php } ?>

                        </div>
                      </div> 
</div><!-- /.col -->                       




</form>





          </div><!-- /.row -->


<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->



<?php include("adminthemefooter.php"); ?>
<script type="text/javascript" src="maxlength/jquery.plugin.min.js"></script> 
<script type="text/javascript" src="maxlength/jquery.maxlength.min.js"></script>
<script type="text/javascript" src="fancybox/source/jquery.fancybox.js?v=2.1.4"></script>
<script type="text/javascript" src="fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript">
  var groupcount=2;
  var NewUserEmail='<?php echo $NewUserEmail; ?>';
  var sendnewuseremail='<?php echo $sendnewuseremail; ?>';
  fieldlengths=new Array();
  <?php for ($k=1;$k<=50;$k++)
{
?>
  fieldlengths[<?php echo $k; ?>]=<?php echo $fieldlengths[$k]; ?>;
<?php } ?> 
</script>
<script src="adduser.js"></script>
<script type="text/javascript">
// Javascript here needs PHP so is not in .js file
<?php for ($k=1;$k<=$groupcount;$k++)
{
  $gvar="group".$k;
  $xvar="expirydate".$k;
?>
  // Make select into combo
  $('#group<?php echo $k; ?>').editableSelect();
  $('#group<?php echo $k; ?>').val('<?php echo $$gvar; ?>');
  // Enable calendar
  $('#dateexpiry<?php echo $k; ?>').datetimepicker({'keepInvalid': true, 'format':DateFormat,'defaultDate':'', 'useStrict': true});
  $('#expiry<?php echo $k; ?>').val('<?php echo $$xvar; ?>');
<?php } ?> 

</script>
  </body>
</html>
