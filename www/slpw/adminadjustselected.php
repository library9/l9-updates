<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    ?>
    {
    "success": false,
    "message": "Could not connect to Mysql"
    }
    <?php
    exit;  
  }
  $cbselect=$_POST['cbselect'];
  $cbdeselect=$_POST['cbdeselect'];
  if (count($cbselect)>0)
  {
    $query="";
    for ($k=0;$k<count($cbselect);$k++)
    {
      if ($query!="")
        $query.=" OR ";
      $query.=$IdField."=".sl_quote_smart($cbselect[$k]);
    }
    $query="UPDATE ".$DbTableName." SET ".$SelectedField."='Yes' WHERE ".$query;
    $mysql_result=mysqli_query($mysql_link,$query);
  }
  if (count($cbdeselect)>0)
  {
    $query="";
    for ($k=0;$k<count($cbdeselect);$k++)
    {
      if ($query!="")
        $query.=" OR ";
      $query.=$IdField."=".sl_quote_smart($cbdeselect[$k]);
    }
    $query="UPDATE ".$DbTableName." SET ".$SelectedField."='No' WHERE ".$query;
    $mysql_result=mysqli_query($mysql_link,$query);
  }
  $numselected=$_SESSION['slnumselected'];
  if (!is_numeric($numselected))
    $numselected=0;
  $numselected=$numselected+count($cbselect)-count($cbdeselect);
  if ($numselected<0)
    $numselected=0;
  $_SESSION['slnumselected']=$numselected;
?>
{
  "success": true
}

