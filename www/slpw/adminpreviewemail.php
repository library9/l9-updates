<?php
$groupswithaccess="ADMIN,SUBADMIN";
$noaccesspage="";
$adminajaxcall=true;
require("sitelokpw.php");
if (!isset($adminlanguage))
  $adminlanguage="en";
require("adminlanguage-".$adminlanguage.".php");  
$slsubadmin=false;
if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
  $slsubadmin=true;
// Check CSRF value
if ($_GET['slcsrf']!=$_SESSION['ses_slcsrf'])
{
  echo ADMINMSG_CSRFFAILED;
  exit;
}
$body=$_SESSION['ses_slemailbody'];
print $body;
