<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value  
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    ?>
    {
    "success": false,
    "message": "<?php echo ADMINMSG_CSRFFAILED; ?>"
    }
    <?php
    exit;      
  }
  $hDirectory=@opendir($BackupLocation);
  $fnames=array();
  $fsizes=array();
  $fmtimes=array();
  $flinks=array();
  if ($hDirectory!=false)
  {
    while($entryname=readdir($hDirectory))
    {
      if (($entryname!=".") && ($entryname!="..") && ($entryname!=""))
      {
        // Only get details about .gz .sql or .tmp files
        $ext=strtolower(".".pathinfo($entryname, PATHINFO_EXTENSION));
        if (($ext!=".gz") && ($ext!=".sql") && ($ext!=".tmp"))
          continue;
        if ($DemoMode)
        {
           @unlink($BackupLocation.$entryname);  
           continue;
        }   
        if ($ext==".tmp")
        {
          // See if file is .tmp and older than 1 day (3 minutes for demo mode)
          if (time()-filemtime($BackupLocation.$entryname)>86400)
          {
            @unlink($BackupLocation.$entryname); 
            continue;           
          }
          continue;
        }      
        $fnames[]=$entryname;
        $filestat=stat($BackupLocation.$entryname);
        $fsizes[]=$filestat['size'];
        $fmtimes[]=$filestat['mtime'];
      }
    }
    closedir($hDirectory);
  }
  // Get date format for list
  if ($DateFormat=="DDMMYY")
    $dformat="d/m/Y";
  if ($DateFormat=="MMDDYY")
    $dformat="m/d/Y";
  // Sort file list as required
  array_multisort($fmtimes, SORT_DESC, SORT_NUMERIC, $fnames, $fsizes, $fmtimes);
  for ($k=0;$k<count($fnames);$k++)
  {
    $fmtimes[$k]=gmdate($dformat." H:i",$fmtimes[$k]);
    $flinks[$k]=sl_siteloklink($fnames[$k].":slbackups",1);
    $fsizes[$k]=backupFriendlyFileSize($fsizes[$k]);
  }
  $filecount=count($fnames);

  function backupFriendlyFileSize($sz)
  {
    if ($sz==0)
      return("0");  
    if ($sz < 1000)
      return($sz . " Bytes");
    if (($sz >= 1000) && ($sz <= 999999))
    {
      $sz = intval($sz / 1000);
      return($sz . "KB");
    }
    if (($sz >= 1000000) && ($sz <= 999999999))
    {
      $sz = $sz / 1000000;
      $sz = intval($sz * 100) / 100;
      return($sz . "MB");
    }
    if ($sz >= 1000000000)
    {
      $sz = $sz / 1000000000;
      $sz = intval($sz * 100) / 100;
      return($sz . "GB");
    }
  } 



  ?>
{
  "success": true,
  "message": "",
  "filecount": <?php echo $filecount; ?>,
  "files": [
<?php
  for ($k=0;$k<$filecount;$k++)
  {
?>
      <?php echo "\"".$fnames[$k]."\""; if ($k!=($filecount-1)) echo ",";?>

<?php    
  }
?>
  ],
  "sizes": [
<?php
  for ($k=0;$k<$filecount;$k++)
  {
?>
      <?php echo "\"".$fsizes[$k]."\""; if ($k!=($filecount-1)) echo ",";?>

<?php    
  }
?>
  ],
  "times": [
<?php
  for ($k=0;$k<$filecount;$k++)
  {
?>
      <?php echo "\"".$fmtimes[$k]."\""; if ($k!=($filecount-1)) echo ",";?>

<?php    
  }
?>
  ],
  "links": [
<?php
  for ($k=0;$k<$filecount;$k++)
  {
?>
      <?php echo "\"".$flinks[$k]."\""; if ($k!=($filecount-1)) echo ",";?>

<?php    
  }
?>
  ]




}

