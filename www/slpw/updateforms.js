updateFormList();

function updateFormList()
{
  $('#formlist').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
  var formData = {
      'slcsrf'    : slcsrf,
      'formtype'  : 'update'
  };

  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : 'admingetforms.php', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
  })
      .done(function(data) {
        sessionExpiredReported(data,true);
        if (data.success)
        {
          if (data.formcount>0)
          {
            var htmlstr="";
            htmlstr+='<table>';
            for (k=0;k<data.formcount;k++)
            {
              htmlstr+='<tr>';
              htmlstr+='<td class="actions">';
              htmlstr+='<input class="delcb" type="checkbox" name="sel[]" value="'+data.formids[k]+'">';
              htmlstr+='&nbsp;&nbsp;<a href="updateformedit.php?act=editform&actid='+data.formids[k]+'"><span class="glyphicon glyphicon-edit actionicon" rel="tooltip" title="'+ADMINBUTTON_EDIT+'"></span></a>';
              htmlstr+='&nbsp;&nbsp;<a href="updateformedit.php?act=duplform&actid='+data.formids[k]+'"><span class="glyphicon glyphicon-duplicate actionicon" rel="tooltip" title="'+ADMINBUTTON_DUPLICATE+'"></span></a>';
              htmlstr+='&nbsp;&nbsp;<a href="updateformcode.php?actid='+data.formids[k]+'"><i class="fa fa-file-code-o actionicon" rel="tooltip" title="'+ADMINET_GENCODE+'"></i></a>';
              htmlstr+='&nbsp;&nbsp;<span class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINBUTTON_DELETE+'" onclick="deleteForm(\''+data.formids[k]+'\');"></span>';
              htmlstr+='</td>\n';
              htmlstr+='<td class="formname">'+data.formnames[k]+'</td>';
              htmlstr+='</tr>';
            }
            htmlstr+='<tr><td colspan="2"><p>&nbsp;</p></td></tr>';
            htmlstr+='<tr>';
            htmlstr+='<td class="actions" colspan="2">';
            htmlstr+='<input class="delcb" type="checkbox" id="selectall" value="on" onclick="checkAll();">';
            htmlstr+='&nbsp;&nbsp;<span class="glyphicon glyphicon-remove actionicon" rel="tooltip" title="'+ADMINMENU_DELETESELECTED+'" onclick="deleteSelectedForms();"></span>';
            htmlstr+='</td>\n';              
            htmlstr+='</tr>';              
            htmlstr+='</table>';            
          }
          else
          {
            htmlstr='<p class="formmessage">'+ADMINET_NOFORMS+'</p>';
          }
          $('#formlist').html(htmlstr);
        }
        else
        {
          $('#formlist').html('<p class="formerror">'+data.message+'</p>');
        }
      })

    .fail(function(data) {
      $('#formlist').html('<p class="formerror">'+ADMINET_FORMERROR+'</p>');
     });
}

function deleteForm(id)
{
  bootbox.confirm(ADMINET_DELETEFORM, function(result) {
    if (result)
    {
      var datatosend = { formid : id, slcsrf : slcsrf, formtype : 'update'};
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'admindeleteform.php', // the url where we want to POST
            data        : datatosend,
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        .done(function(data) {
          sessionExpiredReported(data,true);
          if (data.success)
            updateFormList();
          else
          {
            bootbox.alert(data.message);
            updateFormList();
          }
        })
        .fail(function() {
          bootbox.alert(ADMINET_NODELETE);
          updateFormList();
        })
    }  
  });
}

function deleteSelectedForms()
{
  // Get id's of selected forms
  formname="form";
  var checkboxes = new Array();
  var formids = new Array();
  checkboxes = document[formname].getElementsByTagName('input');
  var ct=0;
  for (var i=0; i<checkboxes.length; i++)  {
    if (checkboxes[i].type == 'checkbox')   {
      if (checkboxes[i].checked)
      {
        formids[ct]=checkboxes[i].value;
        ct++;
      }
    }
  }
  if (ct==0)
    return;  
  bootbox.confirm(ADMINET_DELETESELFRM, function(result) {
    if (result)
    {
      var datatosend = { slcsrf : slcsrf, formtype : 'update', formids : formids.join()};
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'admindeleteselectedforms.php', // the url where we want to POST
            data        : datatosend,
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        .done(function(data) {
          sessionExpiredReported(data,true);
          if (data.success)
            updateFormList();
          else
          {
            bootbox.alert(data.message);
            updateFormList();
          }
        })
        .fail(function() {
          bootbox.alert(ADMINET_NODELETE);
          updateFormList();
        })
    }  
  });
}

function restoreExamples()
{
  bootbox.confirm(ADMINET_EXAMPLES, function(result) {
    if (result)
    {
      var datatosend = { slcsrf : slcsrf, formtype : 'update'};
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'adminrestoreexampleforms.php', // the url where we want to POST
            data        : datatosend,
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        .done(function(data) {
          sessionExpiredReported(data,true);
          if (data.success)
            updateFormList();
          else
          {
            bootbox.alert(data.message);
            updateFormList();
          }
        })
        .fail(function() {
          bootbox.alert(ADMINET_NOEXAMPLES);
          updateFormList();
        })
    }  
  });
}

function checkAll()
{
  formname="form";
  checktoggle=document.getElementById('selectall').checked;
  var checkboxes = new Array(); 
  checkboxes = document[formname].getElementsByTagName('input');
  for (var i=0; i<checkboxes.length; i++)  {
    if (checkboxes[i].type == 'checkbox')   {
      checkboxes[i].checked = checktoggle;
    }
  }
}


