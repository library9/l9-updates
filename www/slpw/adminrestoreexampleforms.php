<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value  
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError(ADMINMSG_CSRFFAILED);
    exit;      
  }
  $formtype="register";
  if ($_POST['formtype']=="register")
    $formtype="register";
  if ($_POST['formtype']=="update")
    $formtype="update";
  if ($_POST['formtype']=="login")
    $formtype="login";
  if ($_POST['formtype']=="contact")
    $formtype="contact";
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError(ADMINMSG_MYSQLERROR);
    exit;
  }
  if (($formtype=="register") && (!$DemoMode))
  {
    $query ="DELETE FROM `sl_forms` WHERE id<10 AND type='register';";
    $mysql_result=mysqli_query($mysql_link,$query);    
    $query ="DELETE FROM `sl_registerforms` WHERE id<10;";
    $mysql_result=mysqli_query($mysql_link,$query);    
    $query ="INSERT INTO `sl_forms` VALUES(1, 'Example for register.php', 'register'),";
    $query.="(2, 'Example for registerapprove.php', 'register'),";
    $query.="(3, 'Example for registerturing.php', 'register');";
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_link==false)
    {
      returnError(ADMINET_NOEXAMPLES);
      exit;
    }
    $query ="INSERT INTO `sl_registerforms` (`id`, `position`, `fonttype`, `labelcolor`, `labelsize`, `labelstyle`, `inputtextcolor`, `inputtextsize`, `inputtextstyle`, `inputbackcolor`, `bordersize`, `bordercolor`, `borderradius`, `rqdfieldlabel`, `rqdfieldcolor`, `rqdfieldsize`, `rqdfieldstyle`, `messagecolor`, `messagesize`, `messagestyle`, `usergroup`, `expiry`, `redirect`, `useremail`, `adminemail`, `enabled`, `sitelokfield`, `inputtype`, `labeltext`, `placetext`, `validation`, `errormsg`, `fieldwidth`, `value`, `checked`, `bottommargin`, `showrequired`, `btncolortype`, `btncolorfrom`, `btncolorto`, `btnradius`, `btnlabel`, `btnlabelcolor`, `btnlabelsize`, `btnlabelfont`, `btnlabelstyle`, `btnbordercolor`, `btnbordersize`, `btnborderstyle`, `formerrormsg`, `formerrormsgcolor`, `formerrormsgsize`, `formerrormsgstyle`, `maxformwidth`, `backcolor`) VALUES";
    $query.="(1, 0, 'Arial, Helvetica, sans-serif', '1A305E', 16, 'normal normal bold', '1A305E', 14, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', 'CLIENT', '0', '/members/registerthanks.php', 'newuser.htm', 'newuseradmin.htm', 'Yes', '', '', '', '', '', '', 0, '', 0, 0, '', 'gradient', '1A305E', '378EE5', 10, 'Register', 'FFFFFF', 14, 'Arial, Helvetica, sans-serif', 'normal normal bold', 'FFFFFF', 0, 'solid', 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 300, 'FFFFFF'),";
    $query.="(1, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'name', 'text', 'Full name', '', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(1, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'email', 'text', 'Email', '', 'required', 'Please enter your email', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(1, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom1', 'text', 'What type of camera do you use', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(1, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom2', 'text', 'Where did you hear about us', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(2, 0, 'Arial, Helvetica, sans-serif', '1A305E', 16, 'normal normal bold', '1A305E', 14, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', 'CLIENT', '0', '/members/registerthanks.php', 'pending.htm', 'pendingadmin.htm', 'No', '', '', '', '', '', '', 0, '', 0, 0, '', 'gradient', '1A305E', '378EE5', 10, 'Register', 'FFFFFF', 14, 'Arial, Helvetica, sans-serif', 'normal normal bold', 'FFFFFF', 0, 'solid', 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 300, 'FFFFFF'),";
    $query.="(2, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'name', 'text', 'Full name', '', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(2, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'email', 'text', 'Email', '', 'required', 'Please enter your email', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(2, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom1', 'text', 'What type of camera do you use', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(2, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom2', 'text', 'Where did you hear about us', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(3, 0, 'Arial, Helvetica, sans-serif', '1A305E', 16, 'normal normal bold', '1A305E', 14, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', 'CLIENT', '0', '/members/registerthanks.php', 'newuser.htm', 'newuseradmin.htm', 'Yes', '', '', '', '', '', '', 0, '', 0, 0, '', 'gradient', '1A305E', '378EE5', 10, 'Register', 'FFFFFF', 14, 'Arial, Helvetica, sans-serif', 'normal normal bold', 'FFFFFF', 0, 'solid', 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 300, 'FFFFFF'),";
    $query.="(3, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'name', 'text', 'Full name', '', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(3, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'email', 'text', 'Email', '', 'required', 'Please enter your email', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(3, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom1', 'text', 'What type of camera do you use', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(3, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'custom2', 'text', 'Where did you hear about us', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(3, 5, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', 'captcha', 'captcha', 'Captcha', '', 'required', 'Please enter the letters shown', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, '');";
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_link==false)
    {
      returnError(ADMINET_NOEXAMPLES);
      exit;
    }
  }

  if (($formtype=="update") && (!$DemoMode))
  {
    $query ="DELETE FROM `sl_forms` WHERE id<10 AND type='update';";
    $mysql_result=mysqli_query($mysql_link,$query);    
    $query ="DELETE FROM `sl_updateforms` WHERE id<10;";
    $mysql_result=mysqli_query($mysql_link,$query);        
    $query ="INSERT INTO `sl_forms` VALUES(4, 'Example for update.php', 'update');";
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_link==false)
    {
      returnError(ADMINET_NOEXAMPLES);
      exit;
    }
    $query ="INSERT INTO `sl_updateforms` (`id`, `position`, `fonttype`, `labelcolor`, `labelsize`, `labelstyle`, `inputtextcolor`, `inputtextsize`, `inputtextstyle`, `inputbackcolor`, `bordersize`, `bordercolor`, `borderradius`, `rqdfieldlabel`, `rqdfieldcolor`, `rqdfieldsize`, `rqdfieldstyle`, `messagecolor`, `messagesize`, `messagestyle`, `redirect`, `useremail`, `adminemail`, `sitelokfield`, `inputtype`, `labeltext`, `placetext`, `validation`, `errormsg`, `fieldwidth`, `value`, `emailaction`, `bottommargin`, `showrequired`, `btncolortype`, `btncolorfrom`, `btncolorto`, `btnradius`, `btnlabel`, `btnlabelcolor`, `btnlabelsize`, `btnlabelfont`, `btnlabelstyle`, `btnbordercolor`, `btnbordersize`, `btnborderstyle`, `formerrormsg`, `formerrormsgcolor`, `formerrormsgsize`, `formerrormsgstyle`, `maxformwidth`, `backcolor`) VALUES";
    $query.="(4, 0, 'Arial, Helvetica, sans-serif', '1A305E', 14, 'normal normal bold', '1A305E', 16, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', '', 'updateuser.htm', 'updateuseradmin.htm', '', '', '', '', '', '', 0, '', 0, 0, '', 'gradient', '1A305E', '378EE5', 10, 'Update', 'FFFFFF', 14, 'Arial, Helvetica, sans-serif', 'normal normal bold', 'FFFFFF', 0, 'solid', 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 400, 'FFFFFF'),";
    $query.="(4, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', 'password', 'password', 'New password (leave blank to keep the same)', '', 'notrequired', '', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(4, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', 'vpassword', 'password', 'Type password again', '', 'required', 'The passwords don''t match', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(4, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', 'name', 'text', 'Name', '', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(4, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', 'email', 'text', 'Email', '', 'required', 'Please enter your email', 100, '', 0, 20, '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(4, 5, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', 'custom1', 'text', 'What type of camera do you use', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(4, 6, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', 'custom2', 'text', 'Where did you hear about us', '', 'notrequired', 'This field is required', 100, '', 0, 20, '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, '');";
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_link==false)
    {
      returnError(ADMINET_NOEXAMPLES);
      exit;
    }
  }

  if (($formtype=="login") && (!$DemoMode))
  {
    $query ="DELETE FROM `sl_forms` WHERE id<10 AND type='login';";
    $mysql_result=mysqli_query($mysql_link,$query);    
    $query ="DELETE FROM `sl_loginforms` WHERE id<10;";
    $mysql_result=mysqli_query($mysql_link,$query);    
    $query ="INSERT INTO `sl_forms` VALUES(5, 'Example for pagewithlogin.php', 'login');";
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_link==false)
    {
      returnError(ADMINET_NOEXAMPLES);
      exit;
    }
    $query ="INSERT INTO `sl_loginforms` VALUES(5, 'Arial, Helvetica, sans-serif', 'FF0000', 12, 'normal normal bold', 'Username', '', 'Password', '', '0', 'Captcha', 'captcha', '0', 'Remember me', '0', 'Auto Login', '1A305E', 16, 'normal normal normal', '1A305E', 14, 'normal normal normal', 'FFFFFF', 1, '378EE5', 5, 'Login','Arial, Helvetica, sans-serif','normal normal bold', 'FFFFFF', 14, 'gradient', '1A305E', '378EE5', 10,'FFFFFF',0,'solid', '1', 'Forgotten?', '1A305E', 14, 'normal normal normal', '0', 'Not signed up yet?', '/register.php', '000000', 12, 'normal normal normal', 15, 200, 'FFFFFF');";
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_link==false)
    {
      returnError(ADMINET_NOEXAMPLES);
      exit;
    }
  }

  if (($formtype=="contact") && (!$DemoMode))
  {
    $query ="DELETE FROM `sl_forms` WHERE id<10 AND type='contact';";
    $mysql_result=mysqli_query($mysql_link,$query);    
    $query ="DELETE FROM `sl_contactforms` WHERE id<10;";
    $mysql_result=mysqli_query($mysql_link,$query);    
    $query ="INSERT INTO `sl_forms` VALUES(6, 'Example for contactpage.php', 'contact');";
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_link==false)
    {
      returnError(ADMINET_NOEXAMPLES);
      exit;
    }
    $query ="INSERT INTO `sl_contactforms` (`id`, `position`, `fonttype`, `labelcolor`, `labelsize`, `labelstyle`, `inputtextcolor`, `inputtextsize`, `inputtextstyle`, `inputbackcolor`, `bordersize`, `bordercolor`, `borderradius`, `rqdfieldlabel`, `rqdfieldcolor`, `rqdfieldsize`, `rqdfieldstyle`, `messagecolor`, `messagesize`, `messagestyle`, `sendemail`, `redirect`, `useremailvisitor`, `adminemailvisitor`, `useremailmember`, `adminemailmember`, `fromnameuser`, `replytouser`, `attachmenttypes`, `sendasuser`, `attachmentsize`, `sitelokfield`, `inputtype`, `labeltext`, `placetext`, `validation`, `errormsg`, `fieldwidth`, `value`, `checked`, `bottommargin`, `showrequired`, `useas`, `showfieldfor`, `btncolortype`, `btncolorfrom`, `btncolorto`, `btnradius`, `btnlabel`, `btnlabelcolor`, `btnlabelsize`, `btnlabelfont`, `btnlabelstyle`, `btnbordercolor`, `btnbordersize`, `btnborderstyle`, `formerrormsg`, `formerrormsgcolor`, `formerrormsgsize`, `formerrormsgstyle`, `maxformwidth`, `backcolor`) VALUES";
    $query.="(6, 0, 'Arial, Helvetica, sans-serif', '1A305E', 18, 'normal normal bold', '1A305E', 16, 'normal normal normal', 'FFFFFF', 1, '378EE5', 0, '*', 'FF0000', 12, 'normal normal normal', 'FF0000', 12, 'normal normal normal', '', 'contactthanks.php', 'contactemailuser.htm', 'contactemailadmin.htm', 'contactemailuser.htm', 'contactemailadmin.htm', '', '', '.jpg .gif .png .zip .txt .pdf', '0', 2000000, '', '', '', '', '', '', 0, '', 0, 0, '', '', '', 'gradient', '1A305E', '378EE5', 10, 'Send', 'FFFFFF', 14, 'Arial, Helvetica, sans-serif', 'normal normal bold', 'FFFFFF', 0, 'solid', 'Please correct the errors below', 'FF0000', 14, 'normal normal bold', 300, 'FFFFFF'),";
    $query.="(6, 1, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, 'fullname', 'text', 'Full name', 'full name', 'required', 'Please enter your full name', 100, '', 0, 20, '1', '2', '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(6, 2, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, 'email', 'text', 'Email', 'email', 'requiredemail', 'Please enter your email', 100, '', 0, 20, '1', '1', '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(6, 3, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, '', 'textarea', 'Message', 'message', 'required', 'Please enter your message', 100, '', 0, 20, '1', '', '0', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, ''),";
    $query.="(6, 4, '', '', 0, '', '', 0, '', '', 0, '', 0, '', '', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, '', 'captcha', 'Captcha', 'captcha', 'required', 'Please enter the captcha code', 100, '', 0, 20, '1', '', '1', '', '', '', 0, '', '', 0, '', '', '', 0, '', '', '', 0, '', 0, '');";
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_link==false)
    {
      returnError(ADMINET_NOEXAMPLES);
      exit;
    }
  }

  if ($mysql_result==false)
  {
    returnError(ADMINET_NODELETE);
    exit;
  }

  function returnError($msg)
  {
    $data['success'] = false;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }


?>  
{
  "success": true,
  "message": ""
}


