// Fix bug where bootstrap doesn't close panels properly after call to collapse
$('#accordion').on('show.bs.collapse', function () {
    $('#accordion .in').collapse('hide');
});

// Switch + and - icons on accordion panels
$('.collapse').on('shown.bs.collapse', function(){
$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
}).on('hidden.bs.collapse', function(){
$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
});




// Remove iCheck from the checkbox on the login form preview
$('#remember').iCheck('destroy');

<!-- Setup minicolor control -->
  var colpick = $('.colorbox').each( function() {
    $(this).minicolors({
      control: $(this).attr('data-control') || 'hue',
      inline: $(this).attr('data-inline') === 'true',
      letterCase: 'uppercase',
      opacity: false,
      changeDelay: 250,
      theme: 'bootstrap'
    });
  });

$('.selectpicker').selectpicker({
  style: 'btn-default',
  size: 'auto',
  dropupAuto: false
});

// Run this when the page first loads
var gradientPrefix = getCssValuePrefix('background','linear-gradient(to bottom, #79bcff 5%, #378ee5 100%)');
var radiusPrefix = getCssValuePrefix('border-radius','10px');

startValues();

// Validate fields on blur
$("#newbackgroundcolor").focusout(function(){
  validateColorBlur('#newbackgroundcolor');
});
 
$("#newloginboxcolorfrom").focusout(function(){
  validateColorBlur('#newloginboxcolorfrom');
});

$("#newloginboxcolorto").focusout(function(){
  validateColorBlur('#newloginboxcolorto');
});

$("#newtitlecolor").focusout(function(){
  validateColorBlur('#newtitlecolor');
});

$("#newlabelcolor").focusout(function(){
  validateColorBlur('#newlabelcolor');
});

$("#newinputtextcolor").focusout(function(){
  validateColorBlur('#newinputtextcolor');
});

$("#newinputbackcolor").focusout(function(){
  validateColorBlur('#newinputbackcolor');
});

$("#newmessagecolor").focusout(function(){
  validateColorBlur('#newmessagecolor');
});

$("#newlogintextcolor").focusout(function(){
  validateColorBlur('#newlogintextcolor');
});

$("#newloginbuttoncolorfrom").focusout(function(){
  validateColorBlur('#newloginbuttoncolorfrom');
});

$("#newloginbuttoncolorto").focusout(function(){
  validateColorBlur('#newloginbuttoncolorto');
});

$("#newbtnbordercolor").focusout(function(){
  validateColorBlur('#newbtnbordercolor');
});

$("#newforgotcolor").focusout(function(){
  validateColorBlur('#newforgotcolor');
});

$("#newsignupcolor").focusout(function(){
  validateColorBlur('#newsignupcolor');
});

$("#newtitlesize").focusout(function(){
  validateSizeBlur('#newtitlesize');
});

$("#newlabelsize").focusout(function(){
  validateSizeBlur('#newlabelsize');
});

$("#newinputtextsize").focusout(function(){
  validateSizeBlur('#newinputtextsize');
});

$("#newmessagesize").focusout(function(){
  validateSizeBlur('#newmessagesize');
});

$("#newlogintextsize").focusout(function(){
  validateSizeBlur('#newlogintextsize');
});

$("#newbtnbordersize").focusout(function(){
  validateSizeBlur('#newbtnbordersize');
});

$("#newforgotsize").focusout(function(){
  validateSizeBlur('#newforgotsize');
});

$("#newsignupsize").focusout(function(){
  validateSizeBlur('#newsignupsize');
});

// Save changes
$('#editloginform').submit(function(event) {
        //Remove form message
        $('#resulteditlogin').html('');
        var data={success:true,message:'',errors:{}};
        var label=showButtonBusy('submit');
        var formData = $('#editloginform').serialize();
        //var formData = {
        //    'text1'              : $('#text1').val(),
        //    'hpos1'             : $('#hpos1').val(),
        //};

        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'adminsavedefaultlogin.php', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
            // using the done promise callback
            .done(function(data) {
              sessionExpiredReported(data,true);
              hideButtonBusy('submit',label);
              showValidation(data,'resulteditlogin');
            })

// using the fail promise callback
    .fail(function(data) {
    hideButtonBusy('submit',label);  
    $('#resulteditlogin').html('<div id="resulteditloginmessage" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
//        console.log(data);
    });


        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });







// Detect which browser prefix to use for the specified CSS value
// (e.g., background-image: -moz-linear-gradient(...);
//        background-image:   -o-linear-gradient(...); etc).
//
// http://stackoverflow.com/questions/15071062/using-javascript-to-edit-css-gradient
function getCssValuePrefix(name, value) {
    var prefixes = ['', '-o-', '-ms-', '-moz-', '-webkit-'];

    // Create a temporary DOM object for testing
    var dom = document.createElement('div');

    for (var i = 0; i < prefixes.length; i++) {
        // Attempt to set the style
        dom.style[name] = prefixes[i] + value;

        // Detect if the style was successfully set
        if (dom.style[name]) {
            return prefixes[i];
        }
        dom.style[name] = '';   // Reset the style
    }
}

function startValues()
{
  filltype('newloginboxfilltype','newloginboxcolortodiv')
  filltype('newloginbuttonfilltype','newloginbuttoncolortodiv')
  updatePreview()
}

function filltype(filltype,togradient)
{
  var filltype=$('#'+filltype).val();
  if (filltype=="solid")
    $('#'+togradient).hide();
  else
    $('#'+togradient).show();
}

function gradientreverse(fromgradient,togradient)
{
  var from=$('#'+fromgradient).val();
  var to=$('#'+togradient).val();
  $('#'+fromgradient).minicolors('value', to);
  $('#'+togradient).minicolors('value', from);
  updatePreview()
}

function updatePreview()
{
  // Background color
  var loginpreviewobj=document.getElementById('loginpreview')
  var newbackgroundcolorobj=document.getElementById('newbackgroundcolor')
  loginpreviewobj.style['backgroundColor']=newbackgroundcolorobj.value

  // Background image
  var newbackgroundimageobj=document.getElementById('newbackgroundimage')
  if (newbackgroundimageobj.value=="")
    loginpreviewobj.style['backgroundImage']="none"
  else
    loginpreviewobj.style['backgroundImage']="url("+newbackgroundimageobj.value+")"

  // Background image repeat
  var newbackgroundimagerpobj=document.getElementById('newbackgroundimagerp')
  if (newbackgroundimagerpobj.value.indexOf('repeat')==-1)
  {
    loginpreviewobj.style['backgroundRepeat']='no-repeat'
    loginpreviewobj.style['backgroundSize']=newbackgroundimagerpobj.value    
  }
  else
    loginpreviewobj.style['backgroundRepeat']=newbackgroundimagerpobj.value

  // Login box color
  var newloginboxcolorfromobj=document.getElementById('newloginboxcolorfrom')
  var newloginboxcolorfrom=newloginboxcolorfromobj.value
  var newloginboxcolortoobj=document.getElementById('newloginboxcolorto')
  var newloginboxcolorto=newloginboxcolortoobj.value
  var loginboxobj=document.getElementById('loginbox')
  if (document.getElementById('newloginboxfilltype').value=="solid")
    newloginboxcolorto=newloginboxcolorfrom
  if (gradientPrefix=="")
    loginboxobj.style['background'] = 'linear-gradient(to bottom, '+newloginboxcolorfrom+' 5%, '+newloginboxcolorto+' 100%)'
  else
    loginboxobj.style['background'] = gradientPrefix+'linear-gradient(to bottom, '+newloginboxcolorfrom+' 5%, '+newloginboxcolorto+' 100%)' 

  // Login box corners
  var newloginboxshapeobj=document.getElementById('newloginboxshape')
  loginboxobj.style['borderRadius'] = newloginboxshapeobj.value+"px"

  // Login box shadow
  var newloginboxshadowobj=document.getElementById('newloginboxshadow')
  loginboxobj.style['boxShadow'] = newloginboxshadowobj.value+"px "+newloginboxshadowobj.value+"px "+newloginboxshadowobj.value+"px 0px rgba(50, 50, 50, 0.75)"
    
  // Title color
  var titleobj=document.getElementById('title')
  var newtitlecolorobj=document.getElementById('newtitlecolor')
  titleobj.style['color']=newtitlecolorobj.value

  // Title size
  var newtitlesizeobj=document.getElementById('newtitlesize')
  titleobj.style['fontSize']=newtitlesizeobj.value+"px"  

  // Title font
  var newtitlefontobj=document.getElementById('newtitlefont')
  titleobj.style['fontFamily']=newtitlefontobj.value  

  // Title align
  var newtitlealignobj=document.getElementById('newtitlealign')
  titleobj.style['textAlign']=newtitlealignobj.value
  
  // Title text
  var newtitletextobj=document.getElementById('newtitletext')
  titleobj.innerHTML=newtitletextobj.value
  
  if (newtitletextobj.value=="")
    titleobj.style['display']="none";
  else  
    titleobj.style['display']="block";

  // Main font type
  var newloginfontobj=document.getElementById('newloginfont') 

  // Message color
  var messageobj=document.getElementById('message')
  var newmessagecolorobj=document.getElementById('newmessagecolor')
  messageobj.style['color']=newmessagecolorobj.value

  // Message size
  var newmessagesizeobj=document.getElementById('newmessagesize')
  messageobj.style['fontSize']=newmessagesizeobj.value+"px"  

  // Message align
  var newmessagealignobj=document.getElementById('newmessagealign')
  messageobj.style['textAlign']=newmessagealignobj.value

  // Label color
  var newlabelcolorobj=document.getElementById('newlabelcolor')
  var usernamelabelobj=document.getElementById('usernamelabel')
  var passwordlabelobj=document.getElementById('passwordlabel')
  var captchalabelobj=document.getElementById('captchalabel')
  var rememberlabelobj=document.getElementById('rememberlabel') 
  usernamelabelobj.style['color']=newlabelcolorobj.value
  passwordlabelobj.style['color']=newlabelcolorobj.value
  if(captchalabelobj)
    captchalabelobj.style['color']=newlabelcolorobj.value
  if(rememberlabelobj)
    rememberlabelobj.style['color']=newlabelcolorobj.value

 // Label size
  var newlabelsizeobj=document.getElementById('newlabelsize')
  usernamelabelobj.style['fontSize']=newlabelsizeobj.value+"px"
  passwordlabelobj.style['fontSize']=newlabelsizeobj.value+"px"
  if(captchalabelobj)
    captchalabelobj.style['fontSize']=newlabelsizeobj.value+"px"
  if(rememberlabelobj)
    rememberlabelobj.style['fontSize']=newlabelsizeobj.value+"px"

  // Username label
  var newusernamelabelobj=document.getElementById('newusernamelabel')
  usernamelabelobj.innerHTML=newusernamelabelobj.value
  var usernameobj=document.getElementById('username')
  usernameobj.placeholder=newusernamelabelobj.value.toLowerCase()

  // Password label
  var newpasswordlabelobj=document.getElementById('newpasswordlabel')
  passwordlabelobj.innerHTML=newpasswordlabelobj.value
  var passwordobj=document.getElementById('password')
  passwordobj.placeholder=newpasswordlabelobj.value.toLowerCase()

  // Captcha label
  if (captchalabelobj)
  {
    var newcaptchalabelobj=document.getElementById('newcaptchalabel')
    captchalabelobj.innerHTML=newcaptchalabelobj.value
    var turingobj=document.getElementById('turing')
    turingobj.placeholder=newcaptchalabelobj.value.toLowerCase()
  }
  // Remember label
  if (rememberlabelobj)
  {
    var newrememberlabelobj=document.getElementById('newrememberlabel')
    if (newrememberlabelobj)
      document.getElementById('rememberlabel').innerHTML=newrememberlabelobj.value
    var newautologinlabelobj=document.getElementById('newautologinlabel')
    if (newautologinlabelobj)
      document.getElementById('rememberlabel').innerHTML=newautologinlabelobj.value
  }  

  // Input text color
  var newinputtextcolorobj=document.getElementById('newinputtextcolor')
  usernameobj.style['color']=newinputtextcolorobj.value
  passwordobj.style['color']=newinputtextcolorobj.value
  if (turingobj)
    turingobj.style['color']=newinputtextcolorobj.value
  if (rememberlabelobj)
  {
    var rememberobj=document.getElementById('remember')  
    rememberobj.style['color']=newinputtextcolorobj.value
  }

  // Input text size
  var newinputtextsizeobj=document.getElementById('newinputtextsize')
  usernameobj.style['fontSize']=newinputtextsizeobj.value+"px"
  passwordobj.style['fontSize']=newinputtextsizeobj.value+"px"
  if (turingobj)
    turingobj.style['fontSize']=newinputtextsizeobj.value+"px"

  // Input background color
  var newinputbackcolorobj=document.getElementById('newinputbackcolor')
  usernameobj.style['backgroundColor']=newinputbackcolorobj.value
  passwordobj.style['backgroundColor']=newinputbackcolorobj.value
  if (turingobj)
    turingobj.style['backgroundColor']=newinputbackcolorobj.value
  if (rememberlabelobj)
  {
    var rememberobj=document.getElementById('remember')  
    rememberobj.style['backgroundColor']=newinputbackcolorobj.value
  }
 
  // Show icons
  var newinputshowiconsobj=document.getElementById('newinputshowicons')
  if (newinputshowiconsobj.value==0)
  {
    usernameobj.style['backgroundImage']="none"
    passwordobj.style['backgroundImage']="none"   
    usernameobj.style['paddingLeft']="10px"
    passwordobj.style['paddingLeft']="10px"   
    usernameobj.style['width']="100%" // This is normally 96% but needs to be 100% here
    passwordobj.style['width']="100%" // This is normally 96% but needs to be 100% here  
  }
  else
  {
    usernameobj.style['backgroundImage']="url(username.png)"
    passwordobj.style['backgroundImage']="url(password.png)"     
    usernameobj.style['paddingLeft']="45px"
    passwordobj.style['paddingLeft']="45px"   
    usernameobj.style['width']="100%"; // This is normally 85% but needs to be 100% here
    passwordobj.style['width']="100%"; // This is normally 85% but needs to be 100% here  
  }

  // Login button color     
  var newloginbuttoncolorfromobj=document.getElementById('newloginbuttoncolorfrom')
  var newloginbuttoncolorfrom=newloginbuttoncolorfromobj.value
  var newloginbuttoncolortoobj=document.getElementById('newloginbuttoncolorto')
  var newloginbuttoncolorto=newloginbuttoncolortoobj.value
  var loginbuttonobj=document.getElementById('myButton')
  if (document.getElementById('newloginbuttonfilltype').value=="solid")
    newloginbuttoncolorto=newloginbuttoncolorfrom
  if (gradientPrefix=="")
    loginbuttonobj.style['background'] = 'linear-gradient(to bottom, '+newloginbuttoncolorfrom+' 5%, '+newloginbuttoncolorto+' 100%)'
  else
    loginbuttonobj.style['background'] = gradientPrefix+'linear-gradient(to bottom, '+newloginbuttoncolorfrom+' 5%, '+newloginbuttoncolorto+' 100%)' 

  // Login button corners
  var newloginbuttonshapeobj=document.getElementById('newloginbuttonshape')
  loginbuttonobj.style['borderRadius'] = newloginbuttonshapeobj.value+"px"
    
  // Login button text
  var newlogintextobj=document.getElementById('newlogintext')
  loginbuttonobj.value=newlogintextobj.value  
 
  // Login button font
  var newlogintextfontobj=document.getElementById('newlogintextfont')

  // Login button textcolor
  var newlogintextcolorobj=document.getElementById('newlogintextcolor')
  loginbuttonobj.style['color']=newlogintextcolorobj.value

  // Login button text size
  var newlogintextsizeobj=document.getElementById('newlogintextsize')

  // Login button text style
  var newlogintextstyleobj=document.getElementById('newlogintextstyle')
  loginbuttonobj.style['font']=newlogintextstyleobj.value+" "+newlogintextsizeobj.value+"px"+" "+newlogintextfontobj.value

  // btn border  
  var newbtnborderstyleobj=document.getElementById('newbtnborderstyle')  
  var newbtnbordercolorobj=document.getElementById('newbtnbordercolor')  
  var newbtnbordersizeobj=document.getElementById('newbtnbordersize')  
  loginbuttonobj.style['border']=newbtnborderstyleobj.value+" "+newbtnbordercolorobj.value+" "+newbtnbordersizeobj.value+"px"

  // Forgot Password text
  var newforgottextobj=document.getElementById('newforgottext')
  var forgotobj=document.getElementById('forgot')
  forgotobj.innerHTML=newforgottextobj.value

  // Forgot Password color
  var newforgotcolorobj=document.getElementById('newforgotcolor')
  forgotobj.style['color']=newforgotcolorobj.value

  // Forgot Password size
  var newforgotsizeobj=document.getElementById('newforgotsize')
  forgotobj.style['fontSize']=newforgotsizeobj.value+"px"  

  // Background color
  var loginpreviewobj=document.getElementById('loginpreview')
  var newbackgroundcolorobj=document.getElementById('newbackgroundcolor')
  loginpreviewobj.style['backgroundColor']=newbackgroundcolorobj.value

  // Signup color
  var signupobj=document.getElementById('signup')
  var signuplinkobj=document.getElementById('signuplink')
  var newsignupcolorobj=document.getElementById('newsignupcolor')
  signuplinkobj.style['color']=newsignupcolorobj.value

  // Signup size
  var newsignupsizeobj=document.getElementById('newsignupsize')
  signuplinkobj.style['fontSize']=newsignupsizeobj.value+"px"  

  // Signup align
  var newsignupalignobj=document.getElementById('newsignupalign')
  signupobj.style['textAlign']=newsignupalignobj.value
  
  // Signup text
  var newsignuptextobj=document.getElementById('newsignuptext')
  signuplinkobj.innerHTML=newsignuptextobj.value
  
  if (newsignuptextobj.value=="")
    signupobj.style['display']="none";
  else  
    signupobj.style['display']="block";

  // Main font
  usernamelabelobj.style['fontFamily']=newloginfontobj.value  
  usernameobj.style['fontFamily']=newloginfontobj.value  
  passwordlabelobj.style['fontFamily']=newloginfontobj.value  
  passwordobj.style['fontFamily']=newloginfontobj.value  
  if (captchalabelobj)
  {
    captchalabelobj.style['fontFamily']=newloginfontobj.value  
    turingobj.style['fontFamily']=newloginfontobj.value  
  }
  if (rememberlabelobj)
  {
    rememberlabelobj.style['fontFamily']=newloginfontobj.value  
    rememberobj.style['fontFamily']=newloginfontobj.value  
  }
  messageobj.style['fontFamily']=newloginfontobj.value  
  forgotobj.style['fontFamily']=newloginfontobj.value  
  signuplinkobj.style['fontFamily']=newloginfontobj.value  
  
  // No reason to set signup link here
}

function previewclick(id,panel)
{
  if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();
  $('#'+panel).collapse('show');
  if (panel!='collapse1')
    $('#collapse1').collapse('hide');
  if (panel!='collapse2')
    $('#collapse2').collapse('hide');
  if (panel!='collapse3')
    $('#collapse3').collapse('hide');
  if (panel!='collapse4')
    $('#collapse4').collapse('hide');
  if (panel!='collapse5')
    $('#collapse5').collapse('hide');
  if (panel!='collapse6')
    $('#collapse6').collapse('hide');
  setTimeout(function() { document.getElementById(id).focus();$("#"+id).get(0).scrollIntoView(); }, 300);  // Allow panel to open before focus
}

function revertToDefaults()
{
//  document.getElementById('newbackgroundcolor').color.fromString('FFFFFF')
  $('#newbackgroundcolor').minicolors('value','#FFFFFF');
  document.getElementById('newbackgroundimage').value='';
  $('#newbackgroundimagerp').selectpicker('val', 'no-repeat');
  $('#newloginfont').selectpicker('val', 'Arial, Helvetica, sans-serif');
  $('#newloginboxfilltype').selectpicker('val', 'gradient');
  $('#newloginboxcolorfrom').minicolors('value','#1A305E');
  $('#newloginboxcolorto').minicolors('value','#2F5DAA');
  $('#newloginboxshape').selectpicker('val', '10');
  $('#newloginboxshadow').selectpicker('val', '6');
  document.getElementById('newtitletext').value='Login';
  $('#newtitlecolor').minicolors('value','#FFFFFF');
  document.getElementById('newtitlesize').value='44';
  $('#newtitlealign').selectpicker('val', 'center');
  $('#newtitlefont').selectpicker('val', 'Arial, Helvetica, sans-serif');
  $('#newmessagecolor').minicolors('value','#FF0000');
  document.getElementById('newmessagesize').value='16';
  $('#newmessagealign').selectpicker('val', 'left');
  document.getElementById('newusernamelabel').value='Username';
  document.getElementById('newpasswordlabel').value='Password';
  if (TuringLogin==1)
    document.getElementById('newcaptchalabel').value='Captcha';
  if (CookieLogin==1)
    document.getElementById('newrememberlabel').value='Remember Me';
  if (CookieLogin==2)
    document.getElementById('newautologinlabel').value='Auto Login';
  $('#newlabelcolor').minicolors('value','#FFFFFF');
  document.getElementById('newlabelsize').value='18';
  $('#newinputtextcolor').minicolors('value','#1A305E');
  document.getElementById('newinputtextsize').value='16';
  $('#newinputbackcolor').minicolors('value','#FFFFFF');
  $('#newinputshowicons').selectpicker('val', '1');
  document.getElementById('newlogintext').value='Login';
  $('#newlogintextfont').selectpicker('val', 'Arial, Helvetica, sans-serif');
  $('#newlogintextcolor').minicolors('value','#FFFFFF');
  document.getElementById('newlogintextsize').value='17';
  $('#newlogintextstyle').selectpicker('val', 'normal normal bold');
  $('#newloginbuttonfilltype').selectpicker('val', 'gradient');
  $('#newloginbuttoncolorfrom').minicolors('value','#79BCFF');
  $('#newloginbuttoncolorto').minicolors('value','#378EE5');
  $('#newloginbuttonshape').selectpicker('val', '10');
  $('#newbtnborderstyle').selectpicker('val', 'solid');
  document.getElementById('newbtnbordercolor').value='FFFFFF';
  document.getElementById('newbtnbordersize').value='0';
  $('#newforgotcolor').minicolors('value','#FFFFFF');
  document.getElementById('newforgotsize').value='14';
  document.getElementById('newsignuptext').value='';
  document.getElementById('newsignupurl').value='';
  $('#newsignupcolor').minicolors('value','#FFA500');
  document.getElementById('newsignupsize').value='16';
  $('#newsignupalign').selectpicker('val', 'center');
  startValues();
  updatePreview();
}

var formFixed=true;

function toggleFixed()
{
  if ($('#previewbox').css('position')=='fixed')
  {
    $('#previewbox').css('position','static');
    $('#previewbox').css('width','100%');
    $('#toggleicon').css('opacity',"0.25");
    formFixed=false;
  }
  else
  {
    $('#previewbox').css('position','fixed');
    $('#previewbox').css('width','40%');
    $('#toggleicon').css('opacity',"0.6");
    formFixed=true;
  } 
}

$(window).on('resize', function(){
  var width=document.documentElement.clientWidth || window.innerWidth || document.getElementsByTagName('body')[0].clientWidth;
  if (width>=1200)
  {
    if (formFixed)
    {
      $('#previewbox').css('position','fixed');
      $('#previewbox').css('width','40%');
    }
    else
    {
      $('#previewbox').css('position','static');
      $('#previewbox').css('width','100%');
    }     
  }
  else
  {
    $('#previewbox').css('position','static');
    $('#previewbox').css('width','100%');    
  }
});



