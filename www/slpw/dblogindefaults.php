<?php
$mysql_link=sl_DBconnect();
if ($mysql_link==false)
{
  print("Can't connect to MySQL server");
  exit;
}
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_logintemplate WHERE id=1");
if ($mysql_result===false)
{
  print("Can't read template table");
  exit;      
}
$row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
if ($row===false)
{
  print("Template data row not found");
  exit;      
}
// Adjust settings for spinner
  if ($row['btnlblsize']>=14)
  {
    $spinnermargintop=intval((($row['btnlblsize']*1.16)-14)/2);
    $spinnersize=12;
  }
  else
  {
    $spinnermargintop=0;
    $spinnersize=intval($row['btnlblsize']/1.4);
  }
  $spinnermarginright=-22;
  $spinnercolorR=hexdec(substr($row['btnlblcolor'],0,2));
  $spinnercolorG=hexdec(substr($row['btnlblcolor'],2,2));
  $spinnercolorB=hexdec(substr($row['btnlblcolor'],4,2));
?>
<style type="text/css">

html, body { margin: 0; padding: 0; }

body{
  background-color: #<?php echo $row['backcolor']; ?>;
<?php if (trim($row['backimage'])!="") { ?>  
  background-image: url(<?php echo $row['backimage']; ?>);
  <?php if (false===strpos($row['backimagerp'],"repeat")) { ?>
  background-repeat: no-repeat;
  -webkit-background-size: <?php echo $row['backimagerp']; ?>;
  -moz-background-size: <?php echo $row['backimagerp']; ?>;
  -o-background-size: <?php echo $row['backimagerp']; ?>;
  background-size: <?php echo $row['backimagerp']; ?>;
  <?php } else { ?>
  background-repeat: <?php echo $row['backimagerp']; ?>;
  <?php } ?>  
<?php } ?>
}
div#topspace {
  width: 100%;
  height: 0px;
}

div#loginbox {
  width: 80%;
  max-width: 420px;
  min-width: 320px;
  margin-left: auto;
  margin-right: auto;
  padding-top: 10px;
  padding-bottom: 20px;
  padding-left: 0;
  padding-right: 0;
  
  /* Background gradient from http://www.colorzilla.com/gradient-editor/ */
  background: #<?php echo $row['boxcolorfrom']; ?>; /* Old browsers */
<?php if ($row['boxcolortype']=="gradient") { ?>
  /* IE9 SVG, needs conditional override of 'filter' to 'none' */
  background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzFhMzA1ZSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMyZjVkYWEiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
  background: -moz-linear-gradient(top,  #<?php echo $row['boxcolorfrom']; ?> 0%, #<?php echo $row['boxcolorto']; ?> 100%); /* FF3.6+ */
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#<?php echo $row['boxcolorfrom']; ?>), color-stop(100%,#<?php echo $row['boxcolorto']; ?>)); /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top,  #<?php echo $row['boxcolorfrom']; ?> 0%,#<?php echo $row['boxcolorto']; ?> 100%); /* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(top,  #<?php echo $row['boxcolorfrom']; ?> 0%,#<?php echo $row['boxcolorto']; ?> 100%); /* Opera 11.10+ */
  background: -ms-linear-gradient(top,  #<?php echo $row['boxcolorfrom']; ?> 0%,#<?php echo $row['boxcolorto']; ?> 100%); /* IE10+ */
  background: linear-gradient(to bottom,  #<?php echo $row['boxcolorfrom']; ?> 0%,#<?php echo $row['boxcolorto']; ?> 100%); /* W3C */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#<?php echo $row['boxcolorfrom']; ?>', endColorstr='#<?php echo $row['boxcolorto']; ?>',GradientType=0 ); /* IE6-8 */
  /* End of gradient */
<?php } ?>
  -webkit-box-shadow: <?php echo $row['boxshadow']; ?>px <?php echo $row['boxshadow']; ?>px <?php echo $row['boxshadow']; ?>px 0px rgba(50, 50, 50, 0.75);
  -moz-box-shadow:    <?php echo $row['boxshadow']; ?>px <?php echo $row['boxshadow']; ?>px <?php echo $row['boxshadow']; ?>px 0px rgba(50, 50, 50, 0.75);
  box-shadow:         <?php echo $row['boxshadow']; ?>px <?php echo $row['boxshadow']; ?>px <?php echo $row['boxshadow']; ?>px 0px rgba(50, 50, 50, 0.75);

  -webkit-border-radius: <?php echo $row['boxradius']; ?>px;
  -moz-border-radius: <?php echo $row['boxradius']; ?>px;
  border-radius: <?php echo $row['boxradius']; ?>px;
}

div#innerbox {
  width: 80%;
  margin-left: auto;
  margin-right: auto;
  padding: 0;
}

div#loginbox h1{
  color: #<?php echo $row['titlecolor']; ?>;
	font: bold <?php echo $row['titlesize']; ?>px <?php echo $row['titlefont']; ?>;
	text-align: <?php echo $row['titlealign']; ?>;
	padding: 0;
	margin: 10px 0 0 0;
}

input#username{
<?php if ($row['showicons']!="0") { ?>
  background: url('<?php echo $SitelokLocationURLPath; ?>username.png') 5px 3px no-repeat;
  width: 85%;
  padding-left: 45px;
<?php } else { ?>
  width: 96%;
  padding-left: 10px;
<?php } ?>  
  background-color: #<?php echo $row['inputbackcolor']; ?>;
  padding-top: 10px;
  padding-bottom: 10px;  
  color: #<?php echo $row['inputcolor']; ?>;
	font: bold <?php echo $row['inputsize']; ?>px <?php echo $row['mainfont']; ?>;
	border: none;
  }

input#password{
<?php if ($row['showicons']!="0") { ?>
  background: url('<?php echo $SitelokLocationURLPath; ?>password.png') 5px 3px no-repeat;
  width: 85%;
  padding-left: 45px;
<?php } else { ?>
  width: 96%;
  padding-left: 10px;
<?php } ?>  
  background-color: #<?php echo $row['inputbackcolor']; ?>;
  padding-top: 10px;
  padding-bottom: 10px;
  color: #<?php echo $row['inputcolor']; ?>;
	font: bold <?php echo $row['inputsize']; ?>px <?php echo $row['mainfont']; ?>;
	border: none;
}

input#turing{
  background: url('<?php echo $SitelokLocationURLPath; ?>turingimage.php?w=80&h=40') 0px 0px no-repeat;
  background-size: 80px 100%;
  background-color: #<?php echo $row['inputbackcolor']; ?>;
  width: 72%;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 90px;
  color: #<?php echo $row['inputcolor']; ?>;
	font: bold <?php echo $row['inputsize']; ?>px <?php echo $row['mainfont']; ?>;
	border: none;
}

input#remember{
  width: 20px;
  height : 20px;
  border: none;
  margin: 17px 0 0 10px;
}

div#loginbox label{
  display: block;
  clear: both;
  color: #<?php echo $row['labelcolor']; ?>;
	font: bold <?php echo $row['labelsize']; ?>px <?php echo $row['mainfont']; ?>;
  margin: 15px 0 2px 0 ;
  padding: 0;
  float: left;
}

div#loginbox label.labelcb{
  display: inline;
  clear: none;
}

a#forgot{
  color: #<?php echo $row['forgotcolor']; ?>;
	font: bold <?php echo $row['forgotsize']; ?>px <?php echo $row['mainfont']; ?>;
	text-align: left;
	vertical-align: middle;
	padding: 0;
	margin-left: 20px;
  margin-top: -20px;
}

p#loginmessage{
  color: #<?php echo $row['msgcolor']; ?>;
	font: bold <?php echo $row['msgsize']; ?>px <?php echo $row['mainfont']; ?>;
	text-align: <?php echo $row['msgalign']; ?>;
	padding: 0;
	margin: 15px 0 0 0;
}

p#signup{
	text-align: <?php echo $row['signupalign']; ?>;
  padding: 0;
	margin: 15px 0 0 0;
}

p#signup a{
  color: #<?php echo $row['signupcolor']; ?>;
	font: bold <?php echo $row['signupsize']; ?>px <?php echo $row['mainfont']; ?>;
	text-align: <?php echo $row['signupalign']; ?>;
	vertical-align: middle;
}


/* Button from http://www.bestcssbuttongenerator.com */
.myButton {
<?php if ($row['btncolortype']=="gradient") { ?>
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #<?php echo $row['btncolorfrom']; ?>), color-stop(1, #<?php echo $row['btncolorto']; ?>));
	background:-moz-linear-gradient(top, #<?php echo $row['btncolorfrom']; ?> 5%, #<?php echo $row['btncolorto']; ?> 100%);
	background:-webkit-linear-gradient(top, #<?php echo $row['btncolorfrom']; ?> 5%, #<?php echo $row['btncolorto']; ?> 100%);
	background:-o-linear-gradient(top, #<?php echo $row['btncolorfrom']; ?> 5%, #<?php echo $row['btncolorto']; ?> 100%);
	background:-ms-linear-gradient(top, #<?php echo $row['btncolorfrom']; ?> 5%, #<?php echo $row['btncolorto']; ?> 100%);
	background:linear-gradient(to bottom, #<?php echo $row['btncolorfrom']; ?> 5%, #<?php echo $row['btncolorto']; ?> 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#<?php echo $row['btncolorfrom']; ?>', endColorstr='#<?php echo $row['btncolorto']; ?>',GradientType=0);
<?php } ?>
	background-color:#<?php echo $row['btncolorfrom']; ?>;
	-moz-border-radius: <?php echo $row['btnradius']; ?>px;
	-webkit-border-radius:  <?php echo $row['btnradius']; ?>px;
	border-radius: <?php echo $row['btnradius']; ?>px;
	display:inline-block;
	cursor:pointer;
	color:#<?php echo $row['btnlblcolor']; ?>;
  font: <?php echo $row['btnlblstyle']; ?> <?php echo $row['btnlblsize']; ?>px <?php echo $row['btnlblfont']; ?>;
	padding: 8px 31px;
	text-decoration: none;
	border: <?php echo $row['btnborderstyle']; ?> #<?php echo $row['btnbordercolor']; ?> <?php echo $row['btnbordersize']; ?>px;
/*
	text-shadow:0px 1px 0px #ffffff;
*/
}
.myButton:hover {
<?php if ($row['btncolortype']=="gradient") { ?>
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #<?php echo $row['btncolorto']; ?>), color-stop(1, #<?php echo $row['btncolorfrom']; ?>));
	background:-moz-linear-gradient(top, #<?php echo $row['btncolorto']; ?> 5%, #<?php echo $row['btncolorfrom']; ?> 100%);
	background:-webkit-linear-gradient(top, #<?php echo $row['btncolorto']; ?> 5%, #<?php echo $row['btncolorfrom']; ?> 100%);
	background:-o-linear-gradient(top, #<?php echo $row['btncolorto']; ?> 5%, #<?php echo $row['btncolorfrom']; ?> 100%);
	background:-ms-linear-gradient(top, #<?php echo $row['btncolorto']; ?> 5%, #<?php echo $row['btncolorfrom']; ?> 100%);
	background:linear-gradient(to bottom, #<?php echo $row['btncolorto']; ?> 5%, #<?php echo $row['btncolorfrom']; ?> 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#<?php echo $row['btncolorto']; ?>', endColorstr='#<?php echo $row['btncolorfrom']; ?>',GradientType=0);
<?php } ?>
	background-color:#<?php echo $row['btncolorto']; ?>;
}
.myButton:active {
	position:relative;
	top:1px;
}
/* End of button */

<?php if ($sl_ajaxforms) { ?>

/* busy spinner */
div#slspinner {
  display: none;
  box-sizing: content-box;  
  float: right;
  height: <?php echo $spinnersize; ?>px;
  width: <?php echo $spinnersize; ?>px;
  margin-left: 0px;
  margin-right: <?php echo $spinnermarginright; ?>px;
  margin-top: <?php echo $spinnermargintop; ?>px;
  position: relative;
  -webkit-animation: rotation .6s infinite linear;
  -moz-animation: rotation .6s infinite linear;
  -o-animation: rotation .6s infinite linear;
  animation: rotation .6s infinite linear;
  border-left: 2px solid rgba(<?php echo $spinnercolorR; ?>,<?php echo $spinnercolorG; ?>,<?php echo $spinnercolorB; ?>, .3);
  border-right: 2px solid rgba(<?php echo $spinnercolorR; ?>,<?php echo $spinnercolorG; ?>,<?php echo $spinnercolorB; ?>, .3);
  border-bottom: 2px solid rgba(<?php echo $spinnercolorR; ?>,<?php echo $spinnercolorG; ?>,<?php echo $spinnercolorB; ?>, .3);
  border-top: 2px solid rgba(<?php echo $spinnercolorR; ?>,<?php echo $spinnercolorG; ?>,<?php echo $spinnercolorB; ?>, 1);
  border-radius: 100%;
}

@-webkit-keyframes rotation {
  from {
    -webkit-transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(359deg);
  }
}

@-moz-keyframes rotation {
  from {
    -moz-transform: rotate(0deg);
  }
  to {
    -moz-transform: rotate(359deg);
  }
}

@-o-keyframes rotation {
  from {
    -o-transform: rotate(0deg);
  }
  to {
    -o-transform: rotate(359deg);
  }
}

@keyframes rotation {
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(359deg);
  }
}

<?php } ?>

</style>  

<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->

<!--[if lte IE 6]>
  <style type="text/css">
    div#loginbox {
    width: 420px;
    }
  </style>
<![endif]-->
<?php
if (($TuringLogin==1) && (function_exists('slcaptchahead')))
  echo slcaptchahead();
?>
</head>
<body>
<div id="topspace"></div>
<div id="loginbox" class="gradient">
<form name="siteloklogin" action="<?php echo $startpage; ?>" method="post" <?php if ($LoginType=="SECURE") echo " autocomplete=\"off\""; ?>  onSubmit="return validatelogin()" >
<?php siteloklogin(); ?>
<div id="innerbox">
<h1><?php echo $row['title']; ?></h1>
<p id="loginmessage"><?php if ($msg!="") echo $msg; ?></p>

<label for="username"><?php echo $row['username']; ?></label>
<input type="text" name="username" id="username" value="<?php echo $slcookieusername; ?>" placeholder="<?php echo strtolower($row['username']); ?>" maxlength="50" tabindex="1" autocorrect="off" autocapitalize="off"  spellcheck="off" autofocus="autofocus">

<label for="password"><?php echo $row['password']; ?></label>
<input type="password" name="password" id="password" value="<?php echo $slcookiepassword; ?>" placeholder="<?php echo strtolower($row['password']); ?>" maxlength="50" tabindex="2" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="off">

<?php if ($TuringLogin==1) { ?>
<label for="turing"><?php echo $row['captcha']; ?></label>
<?php
if (function_exists('slshowcaptcha'))
{
  print "<div style=\"display: block;clear: both;\">\n";
  echo slshowcaptcha();
  print "</div>\n";
}  
else
{
?>
<input type="text" name="turing" id="turing" value="" placeholder="<?php echo strtolower($row['captcha']); ?>" maxlength="5" tabindex="3" autocorrect="off" autocapitalize="off" spellcheck="off">
<?php
}
}
?>

<?php if ($CookieLogin==1) { ?>
<label class="labelcb" for="remember"><?php echo $row['remember']; ?></label>
<input type="checkbox" name="remember" id="remember"  value="1" <?php if ($slcookielogin=="1") echo "checked"; ?> tabindex="4">
<?php } ?>

<?php if ($CookieLogin==2) { ?>
<label class="labelcb" for="remember"><?php echo $row['autologin']; ?></label>
<input type="checkbox" name="remember" id="remember"  value="2" tabindex="4">
<?php } ?>

<div style="display: block;clear: both;margin-top: 15px; line-height:10px;padding:0;margin:0;"><br></div>
<?php if ($sl_ajaxforms) { ?>
<button type="submit" class="myButton" id="myButton" tabindex="5" ><?php echo $row['btnlbltext']; ?><div id="slspinner"></div></button>
<?php } else { ?>
<button type="submit" class="myButton" id="myButton" tabindex="5" ><?php echo $row['btnlbltext']; ?></button>
<?php } ?>
<?php if (trim($row['forgottxt'])!="") { ?>
<a id="forgot" href="javascript: void forgotpw()" title="Forgot your password? Enter username or email &amp; click link"><?php echo $row['forgottxt']; ?></a>
<?php } ?>

<?php
// Call $slplugin_addtodefaultlogin for each
for ($p=0;$p<$slnumplugins;$p++)
{
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if (function_exists($slplugin_addtodefaultlogin[$p]))
    {
      $content=call_user_func($slplugin_addtodefaultlogin[$p],$slpluginid[$p]);
      echo $content;
    }
  }
}
?>

<?php if (($row['signupurl']!="") && ($row['signuptext']!="")) { ?>
<p id="signup"><a href="<?php echo $row['signupurl']; ?>"><?php echo $row['signuptext']; ?></a></p>
<?php } ?>
</div>
</form>
</div> 
<script type="text/javascript">
centerLogin();
var obj=document.getElementById("username")
obj.focus()
window.onresize=centerLogin;
function centerLogin()
{
  var dsocheight = document.documentElement.clientHeight || window.innerHeight || document.getElementsByTagName('body')[0].clientHeight;
  var loginboxheight = document.getElementById('loginbox').clientHeight;
  var newtop=0;
  newtop=Math.floor((dsocheight-loginboxheight)/2);
  document.getElementById('topspace').style.height=newtop+'px';
}  
</script>
</body>
</html>
 