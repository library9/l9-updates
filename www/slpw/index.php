<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  if (!isset($sl_noqueryoption))
    $sl_noqueryoptio=false;
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
  {
    $slsubadmin=true;
    $sl_noqueryoption=true;
  }
  if ($DemoMode)
    $sl_noqueryoption=true;  
  include("admincommonfunctions.php");  
  // Determine sort column number in table
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
    die("Can't connect to mysql");


  $mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbConfigTableName." LIMIT 1");
  if ($mysql_result!=false)
  {
    $row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
    if ($row!=false)
    {
      $SortField=$row["sortfield"];
      $SortDirection=$row["sortdirection"];
    }
  }  
  $sortcolumn=false;
  $sortcolumndirection="asc";
  switch ($SortField)
  {
    case "":
      $sortcolumn=strpos($ColumnOrder,"AC");    
      break;
    case $SelectedField:
      $sortcolumn=strpos($ColumnOrder,"SL");
      break;  
    case $CreatedField:
      $sortcolumn=strpos($ColumnOrder,"CR");
      break;
    case $UsernameField:
      $sortcolumn=strpos($ColumnOrder,"US");
      break;
    case $PasswordField:
      $sortcolumn=strpos($ColumnOrder,"PW");
      break;
    case $EnabledField:
      $sortcolumn=strpos($ColumnOrder,"EN");
      break;
    case $NameField:
      $sortcolumn=strpos($ColumnOrder,"NM");
      break;
    case $EmailField:
      $sortcolumn=strpos($ColumnOrder,"EM");
      break;
    case $UsergroupsField:
      $sortcolumn=strpos($ColumnOrder,"UG");
      break;
    case $IdField:
      $sortcolumn=strpos($ColumnOrder,"ID");
      break;  
    default:
      for ($k=1;$k<=50;$k++)
      {
        $var="Custom".$k."Field";
        if ($SortField==$$var)
        {
          $sortcolumn=getCustomSortColumn($ColumnOrder,sprintf('%02d', $k));
          break;
        }
      }
  }
  if (is_numeric($sortcolumn))
  {
    $sortcolumn=$sortcolumn/2;
    $sortcolumndirection=strtolower($SortDirection);
    if ($sortcolumndirection=="")
      $sortcolumndirection="asc";
  }
  // In case sortcolumn is no longer visible in table
  if ($sortcolumn=="")
    $sortcolumn=0;

  function getCustomSortColumn($ColumnOrder,$num)
  {
    for ($k=0;$k<strlen($ColumnOrder);$k=$k+2)
    {
      if (substr($ColumnOrder,$k,2)==$num)
        return($k);
    }
    return("");  
  }  
?>
<!DOCTYPE html>
<html>
<head>
<?php
$pagename="userlist";
include("adminhead.php");
?>
<title>Sitelok Control Panel</title>
<link type="text/css" rel="stylesheet" href="jquery-dropdown/jquery.dropdown.css" />
<link rel="stylesheet" href="indexsl.css">
</head>
<?php
include("adminthemeheader.php");
?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Your Page Content Here -->

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>

          <div class="row">
            <div class="col-xs-12">

              <div class="box">

                <div class="box-header">
                <div class="box-body">

<div class="row">

  <div class="col-xs-12">

    <div class="panel panel-default">

      <div class="panel-heading">


          <span id="filtertitle" class="panel-title" style="display:inline-block; vertical-align:middle;font-size: 12pt;position:relative; top:-2px;"><?php echo ADMINFILTER_QUICKSEARCH; ?></span>&nbsp;&nbsp;
          <div class="btn-group">
            <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php echo ADMINFILTER_FILTERTYPE; ?> <span class="caret"></span>
            </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li><a href="#" onclick="event.preventDefault();showFilter('quicksearch');"><?php echo ADMINFILTER_QUICKSEARCH; ?></a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#" onclick="event.preventDefault();showFilter('memberof');"><?php echo ADMINFILTER_MEMBEROF; ?></a></li>
              <li><a href="#" onclick="event.preventDefault();showFilter('unexpmemberof');"><?php echo ADMINFILTER_UNEXPMEMBEROF; ?></a></li>
              <li><a href="#" onclick="event.preventDefault();showFilter('expmemberof');"><?php echo ADMINFILTER_EXPMEMBEROF; ?></a></li>
              <li><a href="#" onclick="event.preventDefault();showFilter('expwithin');"><?php echo ADMINFILTER_EXPWITHIN; ?></a></li>
              <li><a href="#" onclick="event.preventDefault();showFilter('joinwithin');"><?php echo ADMINFILTER_NEWMEMBERS; ?></a></li>
              <li><a href="#" onclick="event.preventDefault();showFilter('onlyselected');"><?php echo ADMINFILTER_ONLYSELECTED; ?></a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#" onclick="event.preventDefault();showFilter('filter');"><?php echo ADMINFILTER_ADVANCEDFILTER; ?></a></li>
              <?php if (!$sl_noqueryoption) { ?>
              <li role="separator" class="divider"></li>
              <li><a href="#" onclick="event.preventDefault();showFilter('query');"><?php echo ADMINFILTER_ENTERSQLQUERY; ?></a></li>
              <?php } ?>
            </ul>
          </div>

      </div>

      <div class="panel-body">

        <div id="quicksearchblock">
          <form action="#" method="get" id="quicksearchform" class="form-inline">
          <div class="form-group">
            <div class="input-group">
              <input type="text" id="quicksearch" class="form-control" placeholder="<?php echo ADMINFILTER_SEARCH; ?>">
              <span class="input-group-btn">
                 <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </div>
          </form>
        </div>

        <div id="memberofblock">
          <form action="" class="form-inline" id="memberofform">
            <div class="form-group">
              <label class="control-label lighter" for="memberof"><?php echo ADMINFILTER_USERGROUP; ?></label>
              <div class="input-group" style="width: 140px;">
                <select type="text" placeholder="<?php echo strtolower(ADMINFILTER_USERGROUP); ?>" class="form-control usergrouplist" id="memberof">
                <?php populateUsergroupSelect(); ?>
                </select> 
              </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default">
              <span class="glyphicon glyphicon-filter" aria-hidden="true"></span> <?php echo ADMINFILTER_FILTER; ?>     
              </button>
            </div>
          </form>
          <span class="formerrormsg"></span>
        </div>

        <div id="unexpmemberofblock">
          <form action="" class="form-inline" id="unexpmemberofform">
            <div class="form-group">
              <label class="control-label lighter" for="unexpmemberof"><?php echo ADMINFILTER_USERGROUP; ?></label>
              <div class="input-group" style="width: 140px;">
                <select type="text" placeholder="<?php echo strtolower(ADMINFILTER_USERGROUP); ?>" class="form-control usergrouplist" id="unexpmemberof">
                <?php populateUsergroupSelect(); ?>
                </select> 
              </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default">
              <span class="glyphicon glyphicon-filter" aria-hidden="true"></span> <?php echo ADMINFILTER_FILTER; ?>     
              </button>
            </div>
          </form>
          <span class="formerrormsg"></span>
        </div>

        <div id="expmemberofblock">
          <form action="" class="form-inline" id="expmemberofform">
            <div class="form-group">
              <label class="control-label lighter" for="expmemberof"><?php echo ADMINFILTER_USERGROUP; ?></label>
              <div class="input-group" style="width: 140px;">
                <select type="text" placeholder="<?php echo strtolower(ADMINFILTER_USERGROUP); ?>" class="form-control usergrouplist" id="expmemberof">
                <?php populateUsergroupSelect(); ?>
                </select> 
              </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default">
              <span class="glyphicon glyphicon-filter" aria-hidden="true"></span> <?php echo ADMINFILTER_FILTER; ?>     
              </button>
            </div>
          </form>
          <span class="formerrormsg"></span>
        </div>

        <div id="expwithinblock">
          <form action="" class="form-inline" id="expwithinform">
            <div class="form-group">
              <label class="control-label lighter" for="expwithin"><?php echo ADMINFILTER_USERGROUP; ?></label>
              <div class="input-group" style="width: 140px;">
                <select type="text" placeholder="<?php echo strtolower(ADMINFILTER_USERGROUP); ?>" class="form-control usergrouplist" id="expwithin">
                <?php populateUsergroupSelect(); ?>
                </select> 
              </div>
            </div>
            <div class="form-group">
              <label for="expwithindays" class="control-label lighter"><?php echo ADMINFILTER_WITHIN; ?></label>
                <div class="input-group" style="width: 130px;">
                  <input type="number" id="expwithindays" min="1" max="999" class="form-control" placeholder="days" maxlen="3"/>
                  <span class="input-group-addon"><?php echo ADMINFILTER_DAYS; ?></span>
                </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default">
              <span class="glyphicon glyphicon-filter" aria-hidden="true"></span> <?php echo ADMINFILTER_FILTER; ?>     
              </button>
            </div>
          </form>
          <span class="formerrormsg"></span>
        </div>

        <div id="joinwithinblock">
          <form action="" class="form-inline" id="joinwithinform">
            <div class="form-group">
              <label for="joinwithin" class="control-label lighter"><?php echo ADMINFILTER_NEWWITHIN; ?></label>
                <div class="input-group" style="width: 130px;">
                  <input type="number" id="joinwithin" min="1" max="999" class="form-control" placeholder="days" maxlen="3"/>
                  <span class="input-group-addon"><?php echo ADMINFILTER_DAYS; ?></span>
                </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default">
              <span class="glyphicon glyphicon-filter" aria-hidden="true"></span> <?php echo ADMINFILTER_FILTER; ?>     
              </button>
            </div>
          </form>
          <span class="formerrormsg"></span>
        </div>

        <div id="onlyselectedblock">
          <form action="" class="form-inline" id="onlyselectedform">
            <div class="form-group">
              <label for="onlyselected" class="control-label lighter"><?php echo ADMINFILTER_USERSTHATARE; ?></label>
                <div class="input-group" style="width: 130px;">
                  <select id="onlyselected" class="form-control selectpicker">
                    <option value="Yes"><?php echo ADMINFILTER_SELECTED; ?></option>
                    <option value="No"><?php echo ADMINFILTER_NOTSELECTED; ?></option>
                  </select>  
                </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default">
              <span class="glyphicon glyphicon-filter" aria-hidden="true"></span> <?php echo ADMINFILTER_FILTER; ?>     
              </button>
            </div>
          </form>
          <span class="formerrormsg"></span>
        </div>

        <div id="filterblock">
          <form action="" class="form-horizontal" id="filterform">

            <div class="form-group" style="margin-bottom: 0px;">
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2" style="margin-bottom:15px;">
                <select class="form-control selectpicker" id="filfield1">
                <option value="<?php echo $UsernameField; ?>"><?php echo ADMINFIELD_USERNAME; ?></option>
                <option value="<?php echo $PasswordField; ?>"><?php echo ADMINFIELD_PASSWORD; ?></option>
                <option value="<?php echo $EnabledField; ?>"><?php echo ADMINFIELD_ENABLED; ?></option>
                <option value="<?php echo $NameField; ?>"><?php echo ADMINFIELD_NAME; ?></option>
                <option value="<?php echo $EmailField; ?>"><?php echo ADMINFIELD_EMAIL; ?></option>
                <option value="<?php echo $UsergroupsField; ?>"><?php echo ADMINFIELD_USERGROUPS; ?></option>
                <option value="groupexpiry"><?php echo ADMINFILTER_USERGROUPSEXPIRY; ?></option>
                <option value="<?php echo $CreatedField; ?>"><?php echo ADMINFIELD_CREATED; ?></option>
                <?php
                for ($k=1;$k<=50;$k++)
                {
                  $var1="CustomTitle".$k;
                  $var2="Custom".$k."Field";
                  if ($$var1=="")
                    continue;
                  echo "<option value=\"".$$var2."\">".$$var1."</option>\n";
                }
                ?>
                </select> 
              </div>
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="margin-bottom:15px;">
                <select class="form-control selectpicker" id="filcond1">
                  <option value="equals"><?php echo ADMINFILTERCOMP_EQUALS; ?></option>
                  <option value="notequal"><?php echo ADMINFILTERCOMP_NOTEQUALS; ?></option>
                  <option value="contains"><?php echo ADMINFILTERCOMP_CONTAINS; ?></option>
                  <option value="notcontain"><?php echo ADMINFILTERCOMP_NOTCONTAIN; ?></option>
                  <option value="less"><?php echo ADMINFILTERCOMP_LESS; ?></option>
                  <option value="greater"><?php echo ADMINFILTERCOMP_GREATER; ?></option>
                  <option value="starts"><?php echo ADMINFILTERCOMP_STARTS; ?></option>
                  <option value="ends"><?php echo ADMINFILTERCOMP_ENDS; ?></option>
                  <option value="lessnum"><?php echo ADMINFILTERCOMP_LESSNUM; ?></option>
                  <option value="greaternum"><?php echo ADMINFILTERCOMP_GREATERNUM; ?></option>
                </select> 
              </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="margin-bottom: 15px">
                  <input type="text" id="fildata1" class="form-control" maxlen="255"/>
                </div>
            </div>

            <div id="filter2block">
            <div class="form-group">
              <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2">
                <select class="form-control selectpicker" id="filbool1" style="width: 70px;">
                  <option value="AND">AND</option>
                  <option value="OR">OR</option>
                </select> 
              </div>
            </div>
            <div class="form-group" style="margin-bottom: 0px;">
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2" style="margin-bottom:15px;">
                <select class="form-control selectpicker" id="filfield2">
                <option value="<?php echo $UsernameField; ?>"><?php echo ADMINFIELD_USERNAME; ?></option>
                <option value="<?php echo $PasswordField; ?>"><?php echo ADMINFIELD_PASSWORD; ?></option>
                <option value="<?php echo $EnabledField; ?>"><?php echo ADMINFIELD_ENABLED; ?></option>
                <option value="<?php echo $NameField; ?>"><?php echo ADMINFIELD_NAME; ?></option>
                <option value="<?php echo $EmailField; ?>"><?php echo ADMINFIELD_EMAIL; ?></option>
                <option value="<?php echo $UsergroupsField; ?>"><?php echo ADMINFIELD_USERGROUPS; ?></option>
                <option value="groupexpiry"><?php echo ADMINFILTER_USERGROUPSEXPIRY; ?></option>
                <option value="<?php echo $CreatedField; ?>"><?php echo ADMINFIELD_CREATED; ?></option>
                <?php
                for ($k=1;$k<=50;$k++)
                {
                  $var1="CustomTitle".$k;
                  $var2="Custom".$k."Field";
                  if ($$var1=="")
                    continue;
                  echo "<option value=\"".$$var2."\">".$$var1."</option>\n";
                }
                ?>
                </select> 
              </div>
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="margin-bottom:15px;">
                <select class="form-control selectpicker" id="filcond2">
                  <option value="equals"><?php echo ADMINFILTERCOMP_EQUALS; ?></option>
                  <option value="notequal"><?php echo ADMINFILTERCOMP_NOTEQUALS; ?></option>
                  <option value="contains"><?php echo ADMINFILTERCOMP_CONTAINS; ?></option>
                  <option value="notcontain"><?php echo ADMINFILTERCOMP_NOTCONTAIN; ?></option>
                  <option value="less"><?php echo ADMINFILTERCOMP_LESS; ?></option>
                  <option value="greater"><?php echo ADMINFILTERCOMP_GREATER; ?></option>
                  <option value="starts"><?php echo ADMINFILTERCOMP_STARTS; ?></option>
                  <option value="ends"><?php echo ADMINFILTERCOMP_ENDS; ?></option>
                  <option value="lessnum"><?php echo ADMINFILTERCOMP_LESSNUM; ?></option>
                  <option value="greaternum"><?php echo ADMINFILTERCOMP_GREATERNUM; ?></option>
                </select> 
              </div>
                <div class="col-xs-11 col-sm-11 col-md-4 col-lg-4" style="margin-bottom: 15px">
                  <input type="text" id="fildata2" class="form-control" maxlen="255"/>
                </div>
                &nbsp;<span class="glyphicon glyphicon-remove removefilter actionicon" title="<?php echo ADMINFILTER_REMOVECONDITION; ?>" onclick="removeFilterCondition(1);"></span>                
            </div>
            </div>

            <div id="filter3block">
            <div class="form-group">
              <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2">
                <select class="form-control selectpicker" id="filbool2" style="width: 70px;">
                  <option value="AND">AND</option>
                  <option value="OR">OR</option>
                </select> 
              </div>
            </div>
            <div class="form-group" style="margin-bottom: 0px;">
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2" style="margin-bottom:15px;">
                <select class="form-control selectpicker" id="filfield3">
                <option value="<?php echo $UsernameField; ?>"><?php echo ADMINFIELD_USERNAME; ?></option>
                <option value="<?php echo $PasswordField; ?>"><?php echo ADMINFIELD_PASSWORD; ?></option>
                <option value="<?php echo $EnabledField; ?>"><?php echo ADMINFIELD_ENABLED; ?></option>
                <option value="<?php echo $NameField; ?>"><?php echo ADMINFIELD_NAME; ?></option>
                <option value="<?php echo $EmailField; ?>"><?php echo ADMINFIELD_EMAIL; ?></option>
                <option value="<?php echo $UsergroupsField; ?>"><?php echo ADMINFIELD_USERGROUPS; ?></option>
                <option value="groupexpiry"><?php echo ADMINFILTER_USERGROUPSEXPIRY; ?></option>
                <option value="<?php echo $CreatedField; ?>"><?php echo ADMINFIELD_CREATED; ?></option>
                <?php
                for ($k=1;$k<=50;$k++)
                {
                  $var1="CustomTitle".$k;
                  $var2="Custom".$k."Field";
                  if ($$var1=="")
                    continue;
                  echo "<option value=\"".$$var2."\">".$$var1."</option>\n";
                }
                ?>
                </select> 
              </div>
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="margin-bottom:15px;">
                <select class="form-control selectpicker" id="filcond3">
                  <option value="equals"><?php echo ADMINFILTERCOMP_EQUALS; ?></option>
                  <option value="notequal"><?php echo ADMINFILTERCOMP_NOTEQUALS; ?></option>
                  <option value="contains"><?php echo ADMINFILTERCOMP_CONTAINS; ?></option>
                  <option value="notcontain"><?php echo ADMINFILTERCOMP_NOTCONTAIN; ?></option>
                  <option value="less"><?php echo ADMINFILTERCOMP_LESS; ?></option>
                  <option value="greater"><?php echo ADMINFILTERCOMP_GREATER; ?></option>
                  <option value="starts"><?php echo ADMINFILTERCOMP_STARTS; ?></option>
                  <option value="ends"><?php echo ADMINFILTERCOMP_ENDS; ?></option>
                  <option value="lessnum"><?php echo ADMINFILTERCOMP_LESSNUM; ?></option>
                  <option value="greaternum"><?php echo ADMINFILTERCOMP_GREATERNUM; ?></option>
                </select> 
              </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="margin-bottom: 15px">
                  <input type="text" id="fildata3" class="form-control" maxlen="255"/>
                </div>
                &nbsp;<span class="glyphicon glyphicon-remove removefilter actionicon" title="<?php echo ADMINFILTER_REMOVECONDITION; ?>" onclick="removeFilterCondition(2);"></span>                
            </div>
            </div>

            <div id="filter4block">
            <div class="form-group">
              <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2">
                <select class="form-control selectpicker" id="filbool3" style="width: 70px;">
                  <option value="AND">AND</option>
                  <option value="OR">OR</option>
                </select> 
              </div>
            </div>
            <div class="form-group" style="margin-bottom: 0px;">
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2" style="margin-bottom:15px;">
                <select class="form-control selectpicker" id="filfield4">
                <option value="<?php echo $UsernameField; ?>"><?php echo ADMINFIELD_USERNAME; ?></option>
                <option value="<?php echo $PasswordField; ?>"><?php echo ADMINFIELD_PASSWORD; ?></option>
                <option value="<?php echo $EnabledField; ?>"><?php echo ADMINFIELD_ENABLED; ?></option>
                <option value="<?php echo $NameField; ?>"><?php echo ADMINFIELD_NAME; ?></option>
                <option value="<?php echo $EmailField; ?>"><?php echo ADMINFIELD_EMAIL; ?></option>
                <option value="<?php echo $UsergroupsField; ?>"><?php echo ADMINFIELD_USERGROUPS; ?></option>
                <option value="groupexpiry"><?php echo ADMINFILTER_USERGROUPSEXPIRY; ?></option>
                <option value="<?php echo $CreatedField; ?>"><?php echo ADMINFIELD_CREATED; ?></option>
                <?php
                for ($k=1;$k<=50;$k++)
                {
                  $var1="CustomTitle".$k;
                  $var2="Custom".$k."Field";
                  if ($$var1=="")
                    continue;
                  echo "<option value=\"".$$var2."\">".$$var1."</option>\n";
                }
                ?>
                </select> 
              </div>
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="margin-bottom:15px;">
                <select class="form-control selectpicker" id="filcond4">
                  <option value="equals"><?php echo ADMINFILTERCOMP_EQUALS; ?></option>
                  <option value="notequal"><?php echo ADMINFILTERCOMP_NOTEQUALS; ?></option>
                  <option value="contains"><?php echo ADMINFILTERCOMP_CONTAINS; ?></option>
                  <option value="notcontain"><?php echo ADMINFILTERCOMP_NOTCONTAIN; ?></option>
                  <option value="less"><?php echo ADMINFILTERCOMP_LESS; ?></option>
                  <option value="greater"><?php echo ADMINFILTERCOMP_GREATER; ?></option>
                  <option value="starts"><?php echo ADMINFILTERCOMP_STARTS; ?></option>
                  <option value="ends"><?php echo ADMINFILTERCOMP_ENDS; ?></option>
                  <option value="lessnum"><?php echo ADMINFILTERCOMP_LESSNUM; ?></option>
                  <option value="greaternum"><?php echo ADMINFILTERCOMP_GREATERNUM; ?></option>
                </select> 
              </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="margin-bottom: 15px">
                  <input type="text" id="fildata4" class="form-control" maxlen="255"/>
                </div>
                &nbsp;<span class="glyphicon glyphicon-remove removefilter actionicon" title="<?php echo ADMINFILTER_REMOVECONDITION; ?>" onclick="removeFilterCondition(3);"></span>                
            </div>
            </div>

            <div id="addfilterblock" class="form-group" style="margin-bottom: 15px;">
              <div class="col-xs-12">
                <button type="button" class="btn btn-xs btn-default" onclick="addFilterCondition();">
                  <?php echo ADMINFILTER_ADDCONDITION; ?>&nbsp;<span class="glyphicon glyphicon-plus actionicon"></span>
                </button>
              </div>
            </div>


            <div class="form-group" style="margin-bottom: 0px;">
              <div  class="col-xs-12">
                <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon-filter" aria-hidden="true"></span> <?php echo ADMINFILTER_FILTER; ?>     
                </button>
              </div>
            </div>

          </form>
          <span class="formerrormsg"></span>
        </div>





        <?php if (!$sl_noqueryoption) { ?>
        <div id="queryblock">
          <form action="" class="form-inline" id="sqlinputform">
            <div class="form-group">
              <label for="sqlinput" class="control-label lighter sr-only"><?php echo ADMINFILTER_SQLQUERY; ?></label>
                <div class="input-group" style="width: 300px;">
                  <input type="text" id="sqlinput" class="form-control" placeholder="<?php echo ADMINFILTER_SQLQUERY; ?>" maxlen="1000"/>
                </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default">
              <span class="fa fa-database" aria-hidden="true"></span> <?php echo ADMINFILTER_QUERY; ?>     
              </button>
            </div>
          </form>
          <span class="formerrormsg"></span>
        </div>
        <?php } ?>


      </div>
    </div>

  </div>
</div>



                <div class="row">
                  <div class="col-xs-12">
                  <div class="btn-toolbar">
                    <span class="btn-group" role="group">
                      <button type="button" class="btn btn-default" onclick="addUser();" rel="tooltip" title="<?php echo ADMINMENU_ADDUSER; ?>"><span class="fa fa-user-plus actionicon"></span></button>
                      <button type="button" class="btn btn-default" onclick="selectAll();" rel="tooltip" title="<?php echo ADMINMENU_SELECTALL; ?>"><span class="fa fa-check-square-o actionicon"></span></button>
                      <button type="button" class="btn btn-default" onclick="clearAll();" rel="tooltip" title="<?php echo ADMINBUTTON_CLEARFILTER; ?>"><span class="glyphicon glyphicon-refresh actionicon"></span></button>

                    <span id="selectedactions">
                      <button type="button" class="btn btn-default" onclick="deselectAll();" rel="tooltip" title="<?php echo ADMINMENU_DESELECTALL; ?>"><span class="fa fa-square-o actionicon"></span></button>
                      <button type="button" class="btn btn-default" onclick="emailSelected();" rel="tooltip" title="<?php echo ADMINMENU_EMAILSELECTED; ?>"><span class="glyphicon glyphicon-envelope actionicon"></span></button>
                      <?php if (!$slsubadmin) { ?><button type="button" class="btn btn-default" onclick="deleteSelected();" rel="tooltip" title="<?php echo ADMINMENU_DELETESELECTED; ?>"><span class="glyphicon glyphicon-remove actionicon"></span></button><?php } ?>
                      <?php if (!$slsubadmin) { ?><button type="button" class="btn btn-default" onclick="exportSelected();" rel="tooltip" title="<?php echo ADMINMENU_EXPORTSELECTED;?>"><span class="glyphicon glyphicon-export actionicon"></span></button><?php } ?>


<?php
  $pluginselectedactions=false;
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if ((($slplugin_adminselectedicon[$p]!="") && ($slplugin_adminselectedpage[$p]!="")) || (($slplugin_adminselectediconv5[$p]!="") && ($slplugin_adminselectedpagev5[$p]!="")))
      $pluginselectedactions=true;
  }
  if (($pluginselectedactions) && (!$slsubadmin))
  {
  ?>
                      <span class="btn-group" role="group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" rel="tooltip" title="<?php echo ADMINBUTTON_MOREOPTIONS; ?>"><?php echo ADMINBUTTON_MORE; ?> <span class="caret"></span></button>
                      <ul class="dropdown-menu">
<?php
  for ($p=0;$p<$slnumplugins;$p++)
  {
    if (($slplugin_adminselectedicon[$p]!="") && ($slplugin_adminselectedpage[$p]!=""))
    {
      ?>
                        <li><a href="#" onclick="event.preventDefault();pluginSelected(<?php echo $slpluginid[$p]; ?>,<?php echo $p; ?>,'<?php echo $slpluginfolder[$p]."/".$slplugin_adminselectedpage[$p]; ?>',<?php echo $numsel; ?>,<?php echo $slplugin_adminv5[$p]?'true':'false'; ?>);"><span class="pluginiconspace"><img class="pluginicon" src="<?php echo $slpluginfolder[$p]."/".$slplugin_adminselectedicon[$p]; ?>" border="0"></span><?php echo $slplugin_adminselectedtooltip[$p]; ?></a></li>             
      <?php
    }
  }
?>
                      </ul>
                      </span>
<?php } ?>



                      </span>
                    </span>
                  </div>
                </div>
              </div>
              


                  <table id="siteloktable" class="table table-striped table-bordered">
                    <thead class="thead-default">
                      <tr>
                        <?php
                        $columnordlen=strlen($ColumnOrder);
                        for ($col=0;$col<$columnordlen;$col=$col+2)
                        {
                          $coltag=substr($ColumnOrder,$col,2);
                          //echo "<th><nobr>";
                          switch ($coltag)
                          {
                            case "AC":
                              echo "<th><nobr>&nbsp</nobr></th>\n";
                              break;
                            case "SL":
                              echo "<th><nobr>&nbsp</nobr></th>\n";
                              break;
                            case "CR":
                              echo "<th><nobr>".ADMINFIELD_CREATED."</nobr></th>\n";
                              break;
                            case "US":
                              echo "<th><nobr>".ADMINFIELD_USERNAME."</nobr></th>\n";
                              break;
                            case "PW":
                              echo "<th><nobr>".ADMINFIELD_PASSWORD."</nobr></th>\n";
                              break;
                            case "EN":
                              echo "<th><nobr>".ADMINFIELD_ENABLED."</nobr></th>\n";
                              break;
                            case "NM":
                              echo "<th><nobr>".ADMINFIELD_NAME."</nobr></th>\n";
                              break;
                            case "EM":
                              echo "<th><nobr>".ADMINFIELD_EMAIL."</nobr></th>\n";
                              break;
                            case "UG":
                              echo "<th><nobr>".ADMINFIELD_USERGROUPS."</nobr></th>\n";
                              break;
                            case "ID":
                              echo "<th><nobr>".ADMINFIELD_USERID."</nobr></th>\n";
                              break;  
                            case "01":
                              echo "<th><nobr>".$CustomTitle1."</nobr></th>\n";
                              break;
                            case "02":
                              echo "<th><nobr>".$CustomTitle2."</nobr></th>\n";
                              break;
                            case "03":
                              echo "<th><nobr>".$CustomTitle3."</nobr></th>\n";
                              break;
                            case "04":
                              echo "<th><nobr>".$CustomTitle4."</nobr></th>\n";
                              break;
                            case "05":
                              echo "<th><nobr>".$CustomTitle5."</nobr></th>\n";
                              break;
                            case "06":
                              echo "<th><nobr>".$CustomTitle6."</nobr></th>\n";
                              break;
                            case "07":
                              echo "<th><nobr>".$CustomTitle7."</nobr></th>\n";
                              break;
                            case "08":
                              echo "<th><nobr>".$CustomTitle8."</nobr></th>\n";
                              break;
                            case "09":
                              echo "<th><nobr>".$CustomTitle9."</nobr></th>\n";
                              break;
                            case "10":
                              echo "<th><nobr>".$CustomTitle10."</nobr></th>\n";
                              break;
                            case "11":
                              echo "<th><nobr>".$CustomTitle11."</nobr></th>\n";
                              break;
                            case "12":
                              echo "<th><nobr>".$CustomTitle12."</nobr></th>\n";
                              break;
                            case "13":
                              echo "<th><nobr>".$CustomTitle13."</nobr></th>\n";
                              break;
                            case "14":
                              echo "<th><nobr>".$CustomTitle14."</nobr></th>\n";
                              break;
                            case "15":
                              echo "<th><nobr>".$CustomTitle15."</nobr></th>\n";
                              break;
                            case "16":
                              echo "<th><nobr>".$CustomTitle16."</nobr></th>\n";
                              break;
                            case "17":
                              echo "<th><nobr>".$CustomTitle17."</nobr></th>\n";
                              break;
                            case "18":
                              echo "<th><nobr>".$CustomTitle18."</nobr></th>\n";
                              break;
                            case "19":
                              echo "<th><nobr>".$CustomTitle19."</nobr></th>\n";
                              break;
                            case "20":
                              echo "<th><nobr>".$CustomTitle20."</nobr></th>\n";
                              break;
                            case "21":
                              echo "<th><nobr>".$CustomTitle21."</nobr></th>\n";
                              break;
                            case "22":
                              echo "<th><nobr>".$CustomTitle22."</nobr></th>\n";
                              break;
                            case "23":
                              echo "<th><nobr>".$CustomTitle23."</nobr></th>\n";
                              break;
                            case "24":
                              echo "<th><nobr>".$CustomTitle24."</nobr></th>\n";
                              break;
                            case "25":
                              echo "<th><nobr>".$CustomTitle25."</nobr></th>\n";
                              break;
                            case "26":
                              echo "<th><nobr>".$CustomTitle26."</nobr></th>\n";
                              break;
                            case "27":
                              echo "<th><nobr>".$CustomTitle27."</nobr></th>\n";
                              break;
                            case "28":
                              echo "<th><nobr>".$CustomTitle28."</nobr></th>\n";
                              break;
                            case "29":
                              echo "<th><nobr>".$CustomTitle29."</nobr></th>\n";
                              break;
                            case "30":
                              echo "<th><nobr>".$CustomTitle30."</nobr></th>\n";
                              break;
                            case "31":
                              echo "<th><nobr>".$CustomTitle31."</nobr></th>\n";
                              break;
                            case "32":
                              echo "<th><nobr>".$CustomTitle32."</nobr></th>\n";
                              break;
                            case "33":
                              echo "<th><nobr>".$CustomTitle33."</nobr></th>\n";
                              break;
                            case "34":
                              echo "<th><nobr>".$CustomTitle34."</nobr></th>\n";
                              break;
                            case "35":
                              echo "<th><nobr>".$CustomTitle35."</nobr></th>\n";
                              break;
                            case "36":
                              echo "<th><nobr>".$CustomTitle36."</nobr></th>\n";
                              break;
                            case "37":
                              echo "<th><nobr>".$CustomTitle37."</nobr></th>\n";
                              break;
                            case "38":
                              echo "<th><nobr>".$CustomTitle38."</nobr></th>\n";
                              break;
                            case "39":
                              echo "<th><nobr>".$CustomTitle39."</nobr></th>\n";
                              break;
                            case "40":
                              echo "<th><nobr>".$CustomTitle40."</nobr></th>\n";
                              break;
                            case "41":
                              echo "<th><nobr>".$CustomTitle41."</nobr></th>\n";
                              break;
                            case "42":
                              echo "<th><nobr>".$CustomTitle42."</nobr></th>\n";
                              break;
                            case "43":
                              echo "<th><nobr>".$CustomTitle43."</nobr></th>\n";
                              break;
                            case "44":
                              echo "<th><nobr>".$CustomTitle44."</nobr></th>\n";
                              break;
                            case "45":
                              echo "<th><nobr>".$CustomTitle45."</nobr></th>\n";
                              break;
                            case "46":
                              echo "<th><nobr>".$CustomTitle46."</nobr></th>\n";
                              break;
                            case "47":
                              echo "<th><nobr>".$CustomTitle47."</nobr></th>\n";
                              break;
                            case "48":
                              echo "<th><nobr>".$CustomTitle48."</nobr></th>\n";
                              break;
                            case "49":
                              echo "<th><nobr>".$CustomTitle49."</nobr></th>\n";
                              break;
                            case "50":
                              echo "<th><nobr>".$CustomTitle50."</nobr></th>\n";
                              break;

                          }
                          //echo "</nobr></th>\n";
                        } 
                        ?>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>




                </div><!-- /.box-body -->



              </div><!-- /.box -->

<!-- activity modal -->
<div class="modal fade bannerformmodal" tabindex="-1" role="dialog" aria-labelledby="activitymodal" aria-hidden="true" id="activitymodal">
<div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="activitymodaltitle"></h4>
                </div>
                <div class="modal-body" id="activitycontent">
                </div>
        </div>
        </div>
      </div>
    </div>



            </div><!-- /.col -->
          </div><!-- /.row -->
</div>

<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->



 

<!-- More options dropdown menu -->
<div id="jq-dropdown-1" class="jq-dropdown">
    <ul class="jq-dropdown-menu">
        <li><a href="#" onclick="event.preventDefault();showActivity(moreoptionsuserid,moreoptionsusername);"><span class="pluginiconspace"><span class="glyphicon glyphicon-eye-open"></span></span><?php echo ADMINBUTTON_USERACTIVITY; ?></a></li>
        <li><a href="#" onclick="event.preventDefault();passwordReminder(moreoptionsuserid,moreoptionsusername);"><span class="pluginiconspace"><span class="fa fa-user-secret"></span></span><?php echo ADMINBUTTON_PASSREMIND; ?></a></li>
<?php
  if (!$slsubadmin)
  {
    for ($p=0;$p<$slnumplugins;$p++)
    {
      if (($slplugin_adminusericon[$p]!="") && ($slplugin_adminuserpage[$p]!=""))
      {
        ?>
            <li><a href="#" onclick="event.preventDefault();pluginUser(<?php echo $slpluginid[$p]; ?>,<?php echo $p; ?>,'<?php echo $slpluginfolder[$p]."/".$slplugin_adminuserpage[$p]; ?>',moreoptionsusername,moreoptionsuserid,<?php echo $slplugin_adminv5[$p]?'true':'false'; ?>); return false;"><span class="pluginiconspace"><img class="pluginicon" src="<?php echo $slpluginfolder[$p]."/".$slplugin_adminusericon[$p]; ?>" border="0"></span><?php echo $slplugin_adminusertooltip[$p]; ?></a></li>             
        <?php
      }
    }
  }
?>

    </ul>
</div>


<?php include("adminthemefooter.php"); ?>
    <!-- DataTables -->
    <script src="adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="adminlte/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.2/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript" src="jquery-dropdown/jquery.dropdown.js"></script>


    <!-- Datatables setup -->
    <script>
      var sitelokdatatable=null;
      var slnumselected=0;
      var slnumfiltered=0;
      var filteron=0;
      var filtertype="";
      var filteract="";
      var quicksearch="";
      var expwithin="";
      var expwithindays="";
      var memberof="";
      var unexpmemberof="";
      var expmemberof="";
      var joinwithin="";
      var onlyselected="";

      var filfield1="<?php echo $UsernameField; ?>";
      var filcond1="contains";
      var fildata1="";
      var filbool1="AND";
      var filfield2="<?php echo $UsernameField; ?>";
      var filcond2="contains";
      var fildata2="";
      var filbool2="AND";
      var filfield3="<?php echo $UsernameField; ?>";
      var filcond3="contains";
      var fildata3="";
      var filbool3="AND";
      var filfield4="<?php echo $UsernameField; ?>";
      var filcond4="contains";
      var fildata4="";

      var filfield1="";
      var filcond1="";
      var fildata1="";
      var filbool1="";
      var filfield2="";
      var filcond2="";
      var fildata2="";
      var filbool2="";
      var filfield3="";
      var filcond3="";
      var fildata3="";
      var filbool3="";
      var filfield4="";
      var filcond4="";
      var fildata4="";



      var sqlinput="";

      $(function () {
        sitelokdatatable=$('#siteloktable').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": false,
          "ordering": true,
          "orderMulti": false,
          "info": true,
          "scrollX": true,
          "autoWidth": true,
          "serverSide": true,
          "ajax": {
              "type": "POST",
              "url": "admingetrows.php",
              "data": function ( d ) {
                    d.act=filteract;
                    d.filteron=filteron;
                    d.quicksearch=quicksearch;
                    d.memberof=memberof;
                    d.unexpmemberof=unexpmemberof;
                    d.expmemberof=expmemberof;
                    d.expwithin=expwithin;
                    d.expwithindays=expwithindays;
                    d.joinwithin=joinwithin;
                    d.onlyselected=onlyselected;
                    d.filfield1='';
                    d.filcond1='';
                    d.fildata1='';
                    d.filbool1='';
                    d.filfield2='';
                    d.filcond2='';
                    d.fildata2='';
                    d.filbool2='';
                    d.filfield3='';
                    d.filcond3='';
                    d.fildata3='';
                    d.filbool3='';
                    d.filfield4='';
                    d.filcond4='';
                    d.fildata4='';
                    if (advancedfilters[0])
                    {
                      d.filfield1=filfield1;
                      d.filcond1=filcond1;
                      d.fildata1=fildata1;
                      d.filbool1=filbool1;
                    }
                    if (advancedfilters[1])
                    {
                      d.filfield2=filfield2;
                      d.filcond2=filcond2;
                      d.fildata2=fildata2;
                      d.filbool2=filbool2;
                    }
                    if (advancedfilters[2])
                    {
                      d.filfield3=filfield3;
                      d.filcond3=filcond3;
                      d.fildata3=fildata3;
                      d.filbool3=filbool3;
                    }
                    if (advancedfilters[3])
                    {                    
                      d.filfield4=filfield4;
                      d.filcond4=filcond4;
                      d.fildata4=fildata4;
                    }
                    d.sqlinput=sqlinput;
                  },
              "dataSrc": function ( json ) {
                    filteract="";
                    sltotalusers=json.recordsTotal;
                    slnumselected=json.slnumselected;
                    slnumfiltered=json.recordsFiltered;
                    // Filtered users is sltotalusers-slnumfiltered
                    slnumfiltered=sltotalusers-slnumfiltered;
                    quicksearch=json.slquicksearch;
                    memberof=json.slmemberof;
                    unexpmemberof=json.slunexpmemberof;
                    expmemberof=json.slexpmemberof;
                    expwithin=json.slexpwithin;
                    expwithindays=json.slexpwithindays;
                    joinwithin=json.sljoinwithin;
                    onlyselected=json.slonlyselected;
                    if (onlyselected=='')
                      onlyselected='Yes';
                    filfield1=json.slfilfield1;
                    if (filfield1=='')
                      filfield1=UsernameField;
                    filcond1=json.slfilcond1;
                    if (filcond1=='')
                      filcond1='contains';
                    fildata1=json.slfildata1;
                    filbool1=json.slfilbool1;
                    if (filbool1=='')
                      filbool1='AND';
                    filfield2=json.slfilfield2;
                    if (filfield2=='')
                      filfield2=UsernameField;
                    filcond2=json.slfilcond2;
                    if (filcond2=='')
                      filcond2='contains';
                    fildata2=json.slfildata2;
                    filbool2=json.slfilbool2;
                    if (filbool2=='')
                      filbool2='AND';
                    filfield3=json.slfilfield3;
                    if (filfield3=='')
                      filfield3=UsernameField;
                    filcond3=json.slfilcond3;
                    if (filcond3=='')
                      filcond3='contains';
                    fildata3=json.slfildata3;
                    filbool3=json.slfilbool3;
                    if (filbool3=='')
                      filbool3='AND';
                    filfield4=json.slfilfield4;
                    if (filfield4=='')
                      filfield4=UsernameField;
                    filcond4=json.slfilcond4;
                    if (filcond4=='')
                      filcond4='contains';
                    fildata4=json.slfildata4;
                    sqlinput=json.slsqlinput;
                    filtertype=json.slfiltertype;
                    jsonmessage=json.slmessage;
                    updatenumselected();
                    updatenumfiltered();
                    updateFilterFields();
                    if (jsonmessage!="")
                      bootbox.alert(jsonmessage);                
                    return json.data;
                  }
              

            },
          "processing": true,
          "columnDefs": [
          <?php
          $columnordlen=strlen($ColumnOrder);   
          for ($col=0;$col<$columnordlen;$col=$col+2)
          {
            $coltag=substr($ColumnOrder,$col,2);
            switch ($coltag)
            {
              case "SL":
              echo "{\n";
              echo "\"targets\": ".(int)($col/2).",\n";
              echo "\"orderable\": false,\n";
              echo "\"className\": 'text-nowrap text-center',\n";
              echo "render: function(data, type, full, meta) {\n";
              echo "var sldata=data.split(\",\")\n";
              echo "if (sldata[0]==\"Yes\")\n";
              echo "{\n";
              echo "  return '<input type=\"checkbox\" value=\"Yes\" onclick=\"slrowsel(this,\''+sldata[1]+'\');\" checked >';\n";
              echo "}\n";
              echo "else\n";
              echo "{\n";
              echo "  return '<input type=\"checkbox\" value=\"Yes\" onclick=\"slrowsel(this,\''+sldata[1]+'\');\" >';\n";
              echo "}\n";
              echo "}";
              echo "}";
              if ($col!=($columnordlen-2)) echo ",";
              echo "\n";
              break;

              case "AC":
              echo "{\n";
              echo "\"targets\": ".(int)($col/2).",\n";
              echo "\"orderable\": true,\n";
              echo "\"className\": 'text-nowrap text-left',\n";
              echo "render: function(data, type, full, meta) {\n";
              echo "var sldata=data.split(\",\")\n";

              echo 'var columndata="<div class=\"dropdown\">"'.";\n";

              echo 'columndata+="<span class=\"glyphicon glyphicon-edit actionicon\" rel=\"tooltip\" title=\"'.ADMINBUTTON_EDITUSER.'\" onclick=\"editUser("+sldata[0]+");\"></span>"'.";\n";
              echo 'columndata+="&nbsp;&nbsp;<span class=\"glyphicon glyphicon-envelope actionicon\" rel=\"tooltip\" title=\"'.ADMINBUTTON_EMAILUSER.'\" onclick=\"emailUser("+sldata[0]+",\'"+sldata[1]+"\');\"></span>"'.";\n";
              echo 'columndata+="&nbsp;&nbsp;<span class=\"glyphicon glyphicon-remove actionicon\" rel=\"tooltip\" title=\"'.ADMINBUTTON_DELETEUSER.'\" onclick=\"deleteUser("+sldata[0]+",\'"+sldata[1]+"\');\"></span>"'.";\n";

              echo 'columndata+="&nbsp;&nbsp;<span data-jq-dropdown=\"#jq-dropdown-1\" onclick=\"moreoptionsuserid="+sldata[0]+";moreoptionsusername=\'"+sldata[1]+"\';\" class=\"glyphicon glyphicon-chevron-down actionicon\" rel=\"tooltip\" title=\"'.ADMINBUTTON_MOREOPTIONS.'\"></span>"'.";\n";

// echo 'columndata+="<div id=\"jq-dropdown-"+sldata[0]+"\" class=\"jq-dropdown jq-dropdown-tip\">"'."\n";
// echo 'columndata+="    <ul class=\"jq-dropdown-menu\">"'."\n";
// echo 'columndata+="        <li><a href=\"#1\">Item 1</a></li>"'."\n";
// echo 'columndata+="        <li><a href=\"#2\">Item 2</a></li>"'."\n";
// echo 'columndata+="        <li><a href=\"#3\">Item 3</a></li>"'."\n";
// echo 'columndata+="        <li class=\"jq-dropdown-divider\"></li>"'."\n";
// echo 'columndata+="        <li><a href=\"#4\">Item 4</a></li>"'."\n";
// echo 'columndata+="        <li><a href=\"#5\">Item 5</a></li>"'."\n";
// echo 'columndata+="        <li><a href=\"#5\">Item 6</a></li>"'."\n";
// echo 'columndata+="    </ul>"'."\n";
// echo 'columndata+="</div>"'."\n";


//              echo 'columndata+="&nbsp;&nbsp;<span data-toggle=\"dropdown\" id=\"moreoptions"+sldata[0]+"\" onclick=\"showMoreOptions("+sldata[0]+")\" class=\"dropdown-toggle glyphicon glyphicon-chevron-down actionicon\" rel=\"tooltip\" title=\"'.ADMINBUTTON_MOREOPTIONS.'\"></span>"'.";\n";
//              echo 'columndata+="<ul class=\"dropdown-menu\">"'.";\n";
//              echo 'columndata+="    <li><a href=\"#\" onclick=\"editUser("+sldata[0]+");\"><span class=\"glyphicon glyphicon-eye-open\"></span>Recent activity</a></li>"'.";\n";
//              echo 'columndata+="    <li><a href=\"#\" onclick=\"passwordReminder("+sldata[0]+",\'"+sldata[1]+"\');\"><span class=\"fa fa-user-secret\"></span>'.ADMINBUTTON_PASSREMIND.'</a></li>"'.";\n";
//              echo 'columndata+="</ul>"'.";\n";

              echo 'columndata+="</div>"'.";\n";

              echo "return columndata;\n";

              echo "if (sldata[0]==\"Yes\")\n";
              echo "{\n";
              echo "  return '<input type=\"checkbox\" value=\"Yes\" onclick=\"slrowsel(this,\''+sldata[1]+'\');\" checked >';\n";
              echo "}\n";
              echo "else\n";
              echo "{\n";
              echo "  return '<input type=\"checkbox\" value=\"Yes\" onclick=\"slrowsel(this,\''+sldata[1]+'\');\" >';\n";
              echo "}\n";
              echo "}";
              echo "}";
              if ($col!=($columnordlen-2)) echo ",";
              echo "\n";
              break;

              default:
              echo "{\n";
              echo "\"targets\": ".(int)($col/2).",\n";
              echo "\"className\": 'text-nowrap text-left',\n";
              echo "}";
              if ($col!=($columnordlen-2)) echo ",";
              echo "\n";
              break;

            }
          }  
          ?>
          ],


        select: {
           style: 'multi',
           selector: 'td:first-child',
        },
        "dom": 'i<"#shownumselected"><"#usersfiltered">frtpl',
        order: [[<?php echo $sortcolumn; ?>, '<?php echo $sortcolumndirection; ?>']],
        "stateSave": false,
        "stateDuration": -1,

        "pageLength": <?php echo $ShowRows; ?>,
        "displayStart": <?php echo ($_SESSION['ses_admin_tablestart']=="")? 0 :$_SESSION['ses_admin_tablestart']; ?>,
        "lengthMenu": [ 1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 200],
        language: {
          "emptyTable":     "<?php echo ADMINDATATABLE_NOUSERS; ?>",
          "zeroRecords":    "<?php echo ADMINDATATABLE_NOMATCH; ?>",
          "processing":     "<?php echo ADMINDATATABLE_PROCESSING; ?>",
          "lengthMenu":     "<?php echo ADMINDATATABLE_LENGTHMENU; ?>",
          "thousands":      "",
          "info":           "<?php echo ADMINDATATABLE_INFO; ?>",
          "infoFiltered":   "<?php echo ADMINDATATABLE_INFOFILTERED; ?>",
            "paginate": {
              "previous": "<?php echo ADMINDATATABLE_PREVIOUS; ?>",
              "next": "<?php echo ADMINDATATABLE_NEXT; ?>",
              "first": "<?php echo ADMINDATATABLE_FIRST; ?>",
              "last": "<?php echo ADMINDATATABLE_LAST; ?>"
              }
          },


"initComplete": function(settings, json) {
    datatablesReady();
  }


        });
      });


    </script>

    <script src="indexsl.js"></script>

  </body>
</html>
