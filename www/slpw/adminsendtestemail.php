<?php
  $groupswithaccess="ADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  // Check CSRF value
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError($data,$errors,ADMINMSG_NOTADDUSER.". ".ADMINMSG_CSRFFAILED.".");
    exit;
  } 
  $errors = array();    // array to hold validation errors
  $data = array();      // array to pass back data 
  $newemailtype=$_POST['newemailtype'];
  $newemailreplyto=$_POST['newemailreplyto'];
  $newemailusername=trim($_POST['newemailusername']);
  $newemailpassword=trim($_POST['newemailpassword']);
  $newemailserver=trim($_POST['newemailserver']);
  $newemailport=trim($_POST['newemailport']);
  $newemailauth=trim($_POST['newemailauth']);
  $newemailserversecurity=trim($_POST['newemailserversecurity']);
  $newemaildelay=trim($_POST['newemaildelay']);
  // If demo mode then don't send email
  if ($DemoMode)
  {
    returnError($data,$errors,"Emails not sent in demo mode");
    exit;
  }
  // check SMTP settings entered for PHPmailer
  if ($newemailtype==1)
  {
//      if ($newemailusername=="")
//        $errors['newemailusername']=ADMINMSG_FIELDREQD;
//      if ($newemailpassword=="")
//        $errors['newemailpassword']=ADMINMSG_FIELDREQD;
    if ($newemailserver=="")
      $errors['newemailserver']=ADMINMSG_FIELDREQD;
    if ($newemailport=="")
      $errors['newemailport']=ADMINMSG_FIELDREQD;
  }
  if (!empty($errors))
  {
    returnError($data,$errors,ADMINMSG_FORMERROR);
    exit;
  }
  $fromname=$SiteName;
  $fromemail=$SiteEmail;
  $toemail=$SiteEmail;
  $htmlformat="Y";
  $EmailReplyTo=$newemailreplyto;
  $UsePHPmailer=0;
  if ($newemailtype==1)
    $UsePHPmailer=1;
  $EmailUsername=$newemailusername;
  $EmailPassword=$newemailpassword;
  $EmailServer=$newemailserver;
  $EmailPort=$newemailport;
  $EmailAuth=$newemailauth;
  $EmailServerSecurity=$newemailserversecurity;
  $subject="Test email";
  if ($UsePHPmailer==1)
    $mailBody="Test email sent using PHPmailer";
  else
    $mailBody="Test email sent using mail()";    
  if ($UsePHPmailer==1)
  { 
    if ($EmailPort=="")
      $EmailPort=25;
    require_once("class.phpmailer.php");
    require_once("class.smtp.php");
    $mail = new PHPMailer(true);
    try
    {
    if (($EmailUsername!="") && ($EmailPassword!=""))
      $mail->IsSMTP();
    $mail->SMTPDebug = false;
    $mail->do_debug = 0;
    $mail->CharSet=$MetaCharSet;
    $mail->Host     = $EmailServer;
    $mail->Port = $EmailPort;
    if ($EmailAuth=="0")        
      $mail->SMTPAuth = false;
    else
      $mail->SMTPAuth = true;
    if ($EmailServerSecurity!="")     
      $mail->SMTPSecure = $EmailServerSecurity;
    $mail->Username = $EmailUsername;
    $mail->Password = $EmailPassword;
    $mail->From     = $fromemail;
    $mail->FromName = $fromname;
    if ($EmailReplyTo!="")
      $mail->AddReplyTo($EmailReplyTo,$fromname);      
    $mail->AddAddress($toemail);
    // If admin email then send also to $SiteEmail2 if required
    if ($htmlformat=="Y")
      $mail->IsHTML(true);
    else
      $mail->IsHTML(false);
    $mail->Subject  =  $subject;
    $mail->Body     =  $mailBody;
    $mail->Send();
    returnSuccess("Test email sent to ".$SiteEmail);
    exit;
    } 
    catch (phpmailerException $e)
    {      
      $result=false;      
    }
    catch (Exception $e)
    {
      $result=false;            
    }
    if ($mail->isError())
    {
      returnError($data,$errors,ADMINMSG_SENDEMAILISSUE.". ".$mail->ErrorInfo);
      exit;
    }
  }
  else
  {
    $headers = "From: " . $fromname . " <" . $fromemail . ">\r\n";
    if ($EmailReplyTo!="")
      $headers.= "Reply-To: " . $fromname . " <" . $EmailReplyTo . ">\r\n";        
    else
      $headers.= "Reply-To: " . $fromname . " <" . $fromemail . ">\r\n";
    $headers.= "MIME-Version: 1.0\r\n";
    if ($htmlformat=="Y")
    {
      $headers .= "Content-type: text/html; charset=$MetaCharSet\r\n";
      $headers .= "Content-Transfer-Encoding: base64\r\n";      
      $mailBody=chunk_split(base64_encode($mailBody));
    }
    else
      $headers .= "Content-type: text/plain\r\n";
    if ($EmailHeaderNoSlashR == 1)
      $headers = str_replace("\r", "", $headers);
    if ($ExtraMailParam!="")
      $result=mail($toemail, $subject, $mailBody, $headers, $ExtraMailParam);
    else
      $result=mail($toemail, $subject, $mailBody, $headers);
    if ($result)
    {
      returnSuccess(ADMINMSG_EMAILSENTTO.$SiteEmail);
      exit;
    }
    else
    {
      returnError($data,$errors,ADMINMSG_SENDEMAILISSUE);
      exit;      
    }
  }

  function returnSuccess($msg)
  {
    $data['success'] = true;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

  function returnError($data,$errors,$msg)
  {
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }


  ?>