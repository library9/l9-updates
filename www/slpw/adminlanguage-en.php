<?php
// " characters in text or html should be entered as \"

// General text
define("ADMINGENERAL_FOOTERLEFT","<strong>Copyright &copy; <a href=\"http://www.vibralogix.com/sitelokpw\">Vibralogix Ltd</a>.</strong> All rights reserved.");
define("ADMINGENERAL_FOOTERRIGHT","Smart scripts for smart sites");
define("ADMINGENERAL_MAINNAV","MAIN NAVIGATION");
define("ADMINGENERAL_USERDETAILS","User Details");
define("ADMINGENERAL_CUSTOMFIELDS","Custom Fields");
define("ADMINGENERAL_OTHEROPTIONS","Other Options");
define("ADMINGENERAL_CONTACTFORM","Contact form");
define("ADMINGENERAL_OTHER","Other");
define("ADMINGENERAL_SITENAME","Site name");
define("ADMINGENERAL_SITEEMAIL","Admin email");
define("ADMINGENERAL_SITEEMAIL2","Admin email 2");
define("ADMINGENERAL_DATE","Date");
define("ADMINGENERAL_TIME","Time");
define("ADMINGENERAL_USERS","Users");
define("ADMINGENERAL_UTCGMT","UTC/GMT");
define("ADMINGENERAL_HOURS","hours");
define("ADMINGENERAL_SECONDS","seconds");
define("ADMINGENERAL_MSECONDS","ms");
define("ADMINGENERAL_TYPE","Type");
define("ADMINGENERAL_DETAILS","Details");
define("ADMINGENERAL_IPADR","IP Address");
define("ADMINGENERAL_SESSION","Session");
define("ADMINGENERAL_ENABLED","Enabled");
define("ADMINGENERAL_DISABLED","Disabled");
define("ADMINGENERAL_YES","Yes");
define("ADMINGENERAL_NO","No");
define("ADMINGENERAL_BLANK","Leave blank if not required");

// Main menu labels (used on action buttons too)
define("ADMINMENU_HOME","Home");
define("ADMINMENU_DASHBOARD","Dashboard");
define("ADMINMENU_USERACTIONS","User actions");
define("ADMINMENU_ADDUSER","Add user");
define("ADMINMENU_SELECTALL","Select all users");
define("ADMINMENU_DESELECTALL","Deselect all users");
define("ADMINMENU_EMAILSELECTED","Email selected users");
define("ADMINMENU_EXPORTSELECTED","Export selected users");
define("ADMINMENU_IMPORTUSERS","Import users");
define("ADMINMENU_DELETESELECTED","Delete selected users");

define("ADMINMENU_USERGROUPS","Usergroups");
define("ADMINMENU_ADDUSERGROUP","Add usergroup");
define("ADMINMENU_EDITUSERGROUP","Edit usergroup");

define("ADMINMENU_LOG","Log");

define("ADMINMENU_EMAILTEMPLATES","Email templates");

define("ADMINMENU_PLUGINS","Plugins");
define("ADMINMENU_MANAGEPLUGINS","Manage plugins");
define("ADMINMENU_GETPLUGINS","Get plugins");

define("ADMINMENU_FORMS","Forms");
define("ADMINMENU_DEFAULTLOGINSTYLE","Default login form style");
define("ADMINMENU_REGISTERFORMS","Registration forms");
define("ADMINMENU_REGISTERFORMDESIGN","Registration form designer");
define("ADMINMENU_REGISTERFORMCODE","Registration form code");
define("ADMINMENU_PROFILEFORMS","Update profile forms");
define("ADMINMENU_PROFILEFORMDESIGN","Update profile form designer");
define("ADMINMENU_PROFILEFORMCODE","Update profile form code");
define("ADMINMENU_LOGINFORMS","Login forms");
define("ADMINMENU_LOGINFORMDESIGN","Login form designer");
define("ADMINMENU_LOGINFORMCODE","Login form code");
define("ADMINMENU_CONTACTFORMS","Contact forms");
define("ADMINMENU_CONTACTFORMDESIGN","Contact form designer");
define("ADMINMENU_CONTACTFORMCODE","Contact form code");

define("ADMINMENU_TOOLS","Tools");
define("ADMINMENU_CONFIGURATION","Configuration");
define("ADMINMENU_BACKUP","Backup");

define("ADMINMENU_LOGOUT","Logout");

// Field names
// Custom fields are titles in configuration page
define("ADMINFIELD_ACTIONS","Actions");
define("ADMINFIELD_SELECT","Selected");
define("ADMINFIELD_CREATED","Created");
define("ADMINFIELD_USERNAME","Username");
define("ADMINFIELD_PASSWORD","Password");
define("ADMINFIELD_VERPASS","Verify password");
define("ADMINFIELD_ENABLED","Enabled");
define("ADMINFIELD_NAME","Name");
define("ADMINFIELD_EMAIL","Email");
define("ADMINFIELD_VEREMAIL","Verify email");
define("ADMINFIELD_EXISTPASS","Existing password");
define("ADMINFIELD_USERGROUPS","Usergroups");
define("ADMINFIELD_USERID","User id");
define("ADMINFIELD_USERGROUP","Usergroup");
define("ADMINFIELD_CAPTCHA","Captcha");


// Filter titles and fields
define("ADMINFILTER_FILTERTYPE","Filter Type");
define("ADMINFILTER_QUICKSEARCH","Quick search");
define("ADMINFILTER_MEMBEROF","All members of usergroup");
define("ADMINFILTER_UNEXPMEMBEROF","Unexpired members of usergroup");
define("ADMINFILTER_EXPMEMBEROF","Expired members of usergroup");
define("ADMINFILTER_EXPWITHIN","Expiring soon");
define("ADMINFILTER_NEWMEMBERS","New members");
define("ADMINFILTER_ONLYSELECTED","Only selected members");
define("ADMINFILTER_ADVANCEDFILTER","Advanced filter");
define("ADMINFILTER_ENTERSQLQUERY","Enter SQL Query");
define("ADMINFILTER_USERGROUP","Usergroup");
define("ADMINFILTER_SEARCH","search&hellip;");
define("ADMINFILTER_NEWWITHIN","New members joined within");
define("ADMINFILTER_USERSTHATARE","Only users that are");
define("ADMINFILTER_FILTER","Filter");
define("ADMINFILTER_SQLQUERY","SQL query");
define("ADMINFILTER_WITHIN","within");
define("ADMINFILTER_DAYS","days");
define("ADMINFILTER_SELECTED","selected");
define("ADMINFILTER_NOTSELECTED","not selected");
define("ADMINFILTER_QUERY","Query");
define("ADMINFILTER_USERGROUPSEXPIRY","Usergroups Expiry");
define("ADMINFILTER_ADDCONDITION","add condition");
define("ADMINFILTER_REMOVECONDITION","remove condition");

// Filter comparisons
define("ADMINFILTERCOMP_EQUALS","equals");
define("ADMINFILTERCOMP_NOTEQUALS","not equal to");
define("ADMINFILTERCOMP_CONTAINS","contains");
define("ADMINFILTERCOMP_NOTCONTAIN","does not contain");
define("ADMINFILTERCOMP_LESS","less than");
define("ADMINFILTERCOMP_GREATER","greater than");
define("ADMINFILTERCOMP_STARTS","starts with");
define("ADMINFILTERCOMP_ENDS","ends with");
define("ADMINFILTERCOMP_LESSNUM","less than (numeric)");
define("ADMINFILTERCOMP_GREATERNUM","greater than (numeric)");

// Messages
define("ADMINMSG_FORMERROR","Please correct the form errors. ");
define("ADMINMSG_INVALIDGROUP","Invalid usergroup. ");
define("ADMINMSG_INVALIDDAYS","Days should be between 1 and 999. ");
define("ADMINMSG_NOTDELUSER","Could not delete user");
define("ADMINMSG_NOTDELUSERS","Could not delete users");
define("ADMINMSG_CSRFFAILED","CSRF check failed. Please refresh the page");
define("ADMINMSG_MYSQLERROR","Could not connect to mysql");
define("ADMINMSG_INVALIDQUERY","Invalid query");
define("ADMINMSG_DELETEUSER","Permanently delete user");
define("ADMINMSG_DELETESELECTED","Permanently delete selected users");
define("ADMINMSG_NOTEMPLATE","No template selected");
define("ADMINMSG_TEMPLATENOLOAD","Could not load template");
define("ADMINMSG_TEMPLATETYPE","Template must be .html .htm or .txt");
define("ADMINMSG_SAVING","saving&hellip;");
define("ADMINMSG_LOADING","loading&hellip;");
define("ADMINMSG_SENDING","sending&hellip;");
define("ADMINMSG_NOTSAVED","Unable to save data");
define("ADMINMSG_ENTERUSER","Enter the username");
define("ADMINMSG_USERINVALID","Username contains invalid characters");
define("ADMINMSG_ENTERPASS","Enter the password");
define("ADMINMSG_PASSINVALID","Password contains invalid characters");
define("ADMINMSG_PASSSHORT","Password should contain at least 5 characters");
define("ADMINMSG_ENTERNAME","Enter the users name");
define("ADMINMSG_ENTEREMAIL","Enter the users email address");
define("ADMINMSG_EMAILINVALID","Email address is not valid");
define("ADMINMSG_USERNAMEINUSE","This username is already in use");
define("ADMINMSG_EMAILINUSE","This email address is already in use");
define("ADMINMSG_GROUPINVALID","Usergroup contains invalid characters");
define("ADMINMSG_EXPIRYINVALID","Expiry days contains invalid characters");
define("ADMINMSG_EXPIRYDATEINVALID","Expiry date is not valid. Use ".$DateFormat." format");
define("ADMINMSG_ADMINNOTALLOWED","You are not allowed to create an ADMIN user");
define("ADMINMSG_ADDEDUSER","User added");
define("ADMINMSG_UPDATEDUSER","User updated");
define("ADMINMSG_NOTADDUSER","Could not add user");
define("ADMINMSG_NOTUPDATEUSER","Could not update user");
define("ADMINMSG_EMAILNOTSENT","Email not sent so please check email settings");
define("ADMINMSG_LINKEXPIRYINVALID","Must be a number");
define("ADMINMSG_URLINVALID","URL is not valid");
define("ADMINMSG_ENTERFILE","Enter the filename");
define("ADMINMSG_SAVETEMPLATEERROR","Could not save template");
define("ADMINMSG_ENTERSUBJECT","Enter the email subject");
define("ADMINMSG_EMAILSENTTO","Email sent to ");
define("ADMINMSG_SENDEMAILISSUE","Could not send email");
define("ADMINMSG_EMAILBLOCKEDPLUG","Plugin blocked email being sent");
define("ADMINMSG_USEROREMAILNOTVALID","Not a valid username or email address");
define("ADMINMSG_PAUSING","Pausing&hellip;");
define("ADMINMSG_PAUSED","Paused");
define("ADMINMSG_SENDINGEMAIL","Sending email&hellip;");
define("ADMINMSG_CANCELING","Canceling&hellip;");
define("ADMINMSG_CANCELED","Canceled");
define("ADMINMSG_FINISHED","Finished");
define("ADMINMSG_NOTDELFILE","Could not delete file");
define("ADMINMSG_RUSURE","Are you sure?");
define("ADMINMSG_FIELDREQD","This field is required");
define("ADMINMSG_ENTGROUP","Enter the usergroup");
define("ADMINMSG_ENTURL","Enter the URL");
define("ADMINMSG_FILENMIVD","Filename is not valid");
define("ADMINMSG_NOLOG","Log could not be accessed");
define("ADMINMSG_NOTNUMBER","Must be a number");

// Action buttons. Some use menu settings.
define("ADMINBUTTON_ADDUSER","Add User");
define("ADMINBUTTON_EDITUSER","Edit user");
define("ADMINBUTTON_EMAILUSER","Email user");
define("ADMINBUTTON_DELETEUSER","Delete user");
define("ADMINBUTTON_USERACTIVITY","Recent activity");
define("ADMINBUTTON_PASSREMIND","Send password reminder");
define("ADMINBUTTON_CLEARFILTER","Clear filter &amp; deselect all users");
define("ADMINBUTTON_MORE","More");
define("ADMINBUTTON_MOREOPTIONS","More options");
define("ADMINBUTTON_MOVEUP","move up");
define("ADMINBUTTON_MOVEDOWN","move down");
define("ADMINBUTTON_REMOVE","remove");
define("ADMINBUTTON_DOWNLOAD","download");
define("ADMINBUTTON_VIEW","view");
define("ADMINBUTTON_SAVECHANGES","Save Changes");
define("ADMINBUTTON_RETURNDASHBOARD","Return to dashboard");
define("ADMINBUTTON_RETURNPLUGINS","Return to plugins");
define("ADMINBUTTON_CANCEL","Cancel");
define("ADMINBUTTON_SETTEMPLATE","Use Template");
define("ADMINBUTTON_RANDOM","Random");
define("ADMINBUTTON_LOAD","Load");
define("ADMINBUTTON_SAVE","Save");
define("ADMINBUTTON_NEW","New");
define("ADMINBUTTON_MNG","Manage");
define("ADMINBUTTON_INSERT","Insert");
define("ADMINBUTTON_PREVIEW","Preview");
define("ADMINBUTTON_SENDEMAIL","Send Email");
define("ADMINBUTTON_SENDEMAILS","Send Emails");
define("ADMINBUTTON_PAUSE","Pause");
define("ADMINBUTTON_CONTINUE","Continue");
define("ADMINBUTTON_CLOSE","Close");
define("ADMINBUTTON_OK","OK");
define("ADMINBUTTON_CONFIRM","Confirm");
define("ADMINBUTTON_BACKUP","Create Backup");
define("ADMINBUTTON_DELETE","delete");
define("ADMINBUTTON_RESETDEFAULTS","Reset to default values");
define("ADMINBUTTON_RETURNTOFORMS","Return to form list");
define("ADMINBUTTON_RETURNTOUGLIST","Return to usergroup list");
define("ADMINBUTTON_DUPLICATE","Duplicate");
define("ADMINBUTTON_EDIT","Edit");
define("ADMINBUTTON_BROWSE","Browse&hellip;");
define("ADMINBUTTON_SELECTALL","Select/deselect all");
define("ADMINBUTTON_UPDGENSET","Update general settings");
define("ADMINBUTTON_UPDPWDSET","Update password settings");
define("ADMINBUTTON_UPDTEMPSET","Update template settings");
define("ADMINBUTTON_UPDLOGSET","Update log settings");
define("ADMINBUTTON_UPDCPSET","Update CP settings");
define("ADMINBUTTON_UPDCUSSET","Update field settings");
define("ADMINBUTTON_UPDDWNSET","Update download settings");
define("ADMINBUTTON_UPDEMAILSET","Update email settings");
define("ADMINBUTTON_INSTALLSET","Update install paths");
define("ADMINBUTTON_TESTEMAIL","Send test email");

// Datatable text
define("ADMINDATATABLE_INFO","Showing _START_ to _END_ of _TOTAL_ users");
define("ADMINDATATABLE_INFOFILTERED","(filtered from _MAX_)");
define("ADMINDATATABLE_USERSELECTED","user selected");
define("ADMINDATATABLE_USERSSELECTED","users selected");
define("ADMINDATATABLE_PREVIOUS","Previous");
define("ADMINDATATABLE_NEXT","Next");
define("ADMINDATATABLE_FIRST","First");
define("ADMINDATATABLE_LAST","Last");
define("ADMINDATATABLE_NOUSERS","No users to display");
define("ADMINDATATABLE_NOMATCH","No matching users");
define("ADMINDATATABLE_PROCESSING","Processing&hellip;");
define("ADMINDATATABLE_LENGTHMENU","Show _MENU_ users");

// General form text
define("ADMINGENERAL_USEREMAIL","User email template (blank for none)");
define("ADMINGENERAL_ADMINEMAIL","Admin email template (blank for none)");
define("ADMINGENERAL_REDIRECTPAGE","Redirect page URL");

// adduser.php
define("ADMINADDUSER_EXPIREPLACE","days or "); // DDMMYY or MMDDYY added
define("ADMINADDUSER_EMAILUSER","Email user on save");

// edituser.php
define("ADMINEDITUSER_PASSBLANK","Leave blank for existing password (hashed)");
define("ADMINEDITUSER_EXPIREPLACE","days or "); // DDMMYY or MMDDYY added
define("ADMINEDITUSER_EMAILUSER","Email user on save");
define("ADMINEDITUSER_FORCELOGOUT","Force logout on save");

// emailuser.php
define("ADMINEMAILUSER_HTMLFORMAT","HTML Format");
define("ADMINEMAILUSER_TEXTFORMAT","Text Format");
define("ADMINEMAILUSER_SUBJECT","Subject");
define("ADMINEMAILUSER_VISUALEDITOR","Visual Editor");
define("ADMINEMAILUSER_SOURCEEDITOR","HTML Source code");
define("ADMINEMAILUSER_INSVARS","Insert Variables");
define("ADMINEMAILUSER_LINKEXPIRY","Link valid for (minutes)");
define("ADMINEMAILUSER_NOEXPIRY","Set to 0 for no expiry");
define("ADMINEMAILUSER_EXPIRY12","Set to 0 for no expiry or YYYYMMDDYYhhmm");
define("ADMINEMAILUSER_GROUPEXPIRY","Expiry days or "); // DDMMYY or MMDDYY added
define("ADMINEMAILUSER_EXPIRYTYPE","Expiry type");
define("ADMINEMAILUSER_NEWUSERGROUP","New usergroup");
define("ADMINEMAILUSER_NEWGROUPEXPIRY","New expiry days or "); // DDMMYY or MMDDYY added
define("ADMINEMAILUSER_LEAVEEXPIRYBLANK","Leave blank to keep the same");
define("ADMINEMAILUSER_LOGINURL","Login page URL");
define("ADMINEMAILUSER_FILENAME","Filename");
define("ADMINEMAILUSER_FILELOCATION","File location");
define("ADMINEMAILUSER_EMAILTEMPLATE","Template filename");
define("ADMINEMAILUSER_FIELDNUM","Field number");
define("ADMINEMAILUSER_FIELDNUMHELP","First field is 0, second is 1 etc");
define("ADMINEMAILUSER_PREVIEWISSUE","Could not display preview");
define("ADMINEMAILUSER_PREVIEWUSER","Username or email to use for preview");
define("ADMINEMAILUSER_SENDUSER","Username or email to send to");
define("ADMINEMAILUSER_SAVEEMAIL","Save email template");
define("ADMINEMAILUSER_EMAILSELECTED","Send email to selected users");
define("ADMINEMAILUSER_SENDEMAILTO","Send email to ");
define("ADMINEMAILUSER_DEDUPE","Send only once to each email");
define("ADMINEMAILUSER_DESELECT","Deselect users when email sent");
define("ADMINEMAILUSER_SENT","Sent");
define("ADMINEMAILUSER_FAILED","Failed");
define("ADMINEMAILUSER_BLOCKED","Blocked");


// Email Variables
define("ADMINEMAILUSER_USERDATA","User data");
define("ADMINEMAILUSER_PASSIFKNOWN","Password if known");
define("ADMINEMAILUSER_PASSCLUE","Password clue");
define("ADMINEMAILUSER_PASSHASH","Password hash");
define("ADMINEMAILUSER_FULLNAME","Full name");
define("ADMINEMAILUSER_FIRSTNAME","First name");
define("ADMINEMAILUSER_LASTNAME","Last name");
define("ADMINEMAILUSER_IP","IP address");
define("ADMINEMAILUSER_STARTGROUPLOOP","Start group loop");
define("ADMINEMAILUSER_GROUPNAME","Group name");
define("ADMINEMAILUSER_GROUPDESC","Group description");
define("ADMINEMAILUSER_GROUPEXPIRY","Group expiry");
define("ADMINEMAILUSER_ENDGROUPLOOP","End group loop");
define("ADMINEMAILUSER_ADDGROUP","Add usergroup link");
define("ADMINEMAILUSER_REMGROUP","Remove usergroup link");
define("ADMINEMAILUSER_REPGROUP","Replace group link");
define("ADMINEMAILUSER_EXTGROUP","Extend usergroup link");
define("ADMINEMAILUSER_RESETPASS","Reset password");
define("ADMINEMAILUSER_NEWPASS","New password");
define("ADMINEMAILUSER_ACTPASS","Activate password link");
define("ADMINEMAILUSER_CHGEMAIL","Change email");
define("ADMINEMAILUSER_REQEMAIL","Requested email");
define("ADMINEMAILUSER_VEREMAIL","Verify email link");
define("ADMINEMAILUSER_LOGINLINK","Auto Login link");
define("ADMINEMAILUSER_DOWNLINK","Download link");
define("ADMINEMAILUSER_FILESIZE","File size");
define("ADMINEMAILUSER_ACTPROC","Account processing");
define("ADMINEMAILUSER_CONACT","Approve/confirm account link");
define("ADMINEMAILUSER_DISACT","Disable account link");
define("ADMINEMAILUSER_DELACT","Delete account link");
define("ADMINEMAILUSER_CONDATA","Contact form data");
define("ADMINEMAILUSER_CONLAB","Contact form label");
define("ADMINEMAILUSER_CONVAL","Contact form value");
define("ADMINEMAILUSER_DATEDD","Date DD/MM/YY");
define("ADMINEMAILUSER_DATEMM","Date MM/DD/YY");
define("ADMINEMAILUSER_VIEWEMAIL","View email in browser");
define("ADMINEMAILUSER_PLUGIN","Plugin variables");

// File Manager
define("ADMINRFM_LANG","en_EN");  // See /slpw/filemanager/lang

// TinyMCE
define("ADMINTINYMCE_LANG","");  // See /slpw/tinymce/langs. For US English set to ""

// Calendar
define("ADMINDTTMPICK_LANG","");  // See /slpw/bootstrap-datetimepicker/locale/. For US English set to ""

// Backup
define("ADMINBACKUP_COMPRESS","Compress file in .gz format");
define("ADMINBACKUP_MIGRATE","Generate data for migration only");
define("ADMINBACKUP_FILES","Backup files");
define("ADMINBACKUP_ABOUT","About backup files");
define("ADMINBACKUP_NOFILES","There are no stored backup files");
define("ADMINBACKUP_FILEERROR","Could not access backup files");
define("ADMINBACKUP_DELETEFILE","Are you sure you want to delete this file?");
define("ADMINBACKUP_NODELETE","Could not delete file");
define("ADMINBACKUP_CREATING","Creating backup file");
define("ADMINBACKUP_WARNING","Warning. Closing this window will cancel the backup.");


// Edit forms and login template
define("ADMINET_FRMSAVED","Form saved");
define("ADMINET_PGBACK","Page background");
define("ADMINET_PGBACKCOL","Page background color");
define("ADMINET_PGBACKURL","Page background image URL");
define("ADMINET_PGBACKIMG","Page background image");
define("ADMINET_LGBOXSTY","Login box style");
define("ADMINET_LGBOXCOL","Login box color");
define("ADMINET_LGBOXCOR","Login box shape");
define("ADMINET_SOLID","solid");
define("ADMINET_GRADIENT","gradient");
define("ADMINET_DOTTED","dotted");
define("ADMINET_DASHED","dashed");
define("ADMINET_SQUARE","square");
define("ADMINET_ROUNDED","rounded");
define("ADMINET_TITLE","Title");
define("ADMINET_TITLESTY","Title style");
define("ADMINET_TITLEFONT","Title font type");
define("ADMINET_SHADOW","shadow");
define("ADMINET_NOSHADOW","no shadow");
define("ADMINET_LEFT","align left");
define("ADMINET_RIGHT","align right");
define("ADMINET_CENTER","align center");
define("ADMINET_INPFLDS","Input fields");
define("ADMINET_USERLBL","Username label");
define("ADMINET_PASSLBL","Password label");
define("ADMINET_CAPTCHALBL","Captcha label");
define("ADMINET_AUTOLBL","Auto login label");
define("ADMINET_REMLBL","Remember me label");
define("ADMINET_FONTTYP","Font type");
define("ADMINET_INPLBLSTY","Input label style");
define("ADMINET_INPTXTSTY","Input text style");
define("ADMINET_INPPADDING","Input Padding (Vert Horz)");
define("ADMINET_INPBCKCOL","Input background color");
define("ADMINET_MSGSTY","Message style");
define("ADMINET_LOGINBTN","Login button");
define("ADMINET_BTNFONT","Button font type");
define("ADMINET_BTNLBL","Button label");
define("ADMINET_BTNLBLSTY","Button label style");
define("ADMINET_BTNCOL","Button color");
define("ADMINET_NORMAL","normal");
define("ADMINET_BOLD","bold");
define("ADMINET_ITALIC","italic");
define("ADMINET_BTNSHAPE","Button shape");
define("ADMINET_BTNBRDSTY","Button border style");
define("ADMINET_BTNPADDING","Button Padding (Vert Horz)");
define("ADMINET_FRGLNK","Forgot password link");
define("ADMINET_FRGTXT","Forgot password text");
define("ADMINET_FRGSTY","Forgot password style");
define("ADMINET_SIGNLNK","Signup link");
define("ADMINET_SIGNTXT","Signup text");
define("ADMINET_SIGNURL","Signup URL");
define("ADMINET_SIGNSTY","signup style");
define("ADMINET_INPTXTBRDSTY","Input border style");
define("ADMINET_FORMPROP","Form properties");
define("ADMINET_FORMSTYLE","Form style");
define("ADMINET_BTNSTYLE","Button style");
define("ADMINET_FLDPROP","Selected field properties");
define("ADMINET_ADDFLD","add field");
define("ADMINET_EDITFLD","edit field");
define("ADMINET_DELFLD","delete field");
define("ADMINET_FRMNM","Form name");
define("ADMINET_REGTYPE","Registration type");
define("ADMINET_REGTYPEFREE","Register for free access");
define("ADMINET_REGTYPEPPL","Register and pay via Paypal");
define("ADMINET_REGTYPESTRP","Pay via Stripe and register");
define("ADMINET_GRPADD","Group to add user to");
define("ADMINET_EXPDAYS","Expiry (days)");
define("ADMINET_THANKPAGE","Thankyou page");
define("ADMINET_INITSTAT","Initial account status");
define("ADMINET_USEREMAIL","Send email to user");
define("ADMINET_ADMINEMAIL","Send email to admin");
define("ADMINET_ENABLED","Enabled");
define("ADMINET_DISABLED","Disabled");
define("ADMINET_RQDFLDLAB","Required field label");
define("ADMINET_RQDFLDSTY","Required field style");
define("ADMINET_FRMERRMSG","Form error message");
define("ADMINET_FRMERRMSGSTY","Form error message style");
define("ADMINET_FRMMAXWID","Form max width");
define("ADMINET_PREVCOL","Preview background color");
define("ADMINET_SLFIELD","Sitelok field");
define("ADMINET_FRMLAB","Form label");
define("ADMINET_INPTYP","Input type");
define("ADMINET_EMFLACT","Email field action");
define("ADMINET_TEXT","Text");
define("ADMINET_PASS","Password");
define("ADMINET_DRPDWN","Dropdown menu");
define("ADMINET_CHECKBOX","Checkbox");
define("ADMINET_RADIO","Radio");
define("ADMINET_LABEL","Label");
define("ADMINET_CAPTCHA","Captcha");
define("ADMINET_FILEUPL","File upload");
define("ADMINET_TXTAREA","Text area");
define("ADMINET_LBLTXT","Label text");
define("ADMINET_VALUE","Value");
define("ADMINET_OPTIONS","Options");
define("ADMINET_INICHKD","Initially checked");
define("ADMINET_NOTCHKD","Not checked");
define("ADMINET_CHKD","Checked");
define("ADMINET_PLACETXT","Placeholder text");
define("ADMINET_REQD","Required");
define("ADMINET_VALIDATION","Validation");
define("ADMINET_NOTREQD","Not required");
define("ADMINET_NOTFIRST","(first option not allowed)");
define("ADMINET_SHWREQ","Show as required");
define("ADMINET_SHWREQLAB","Show required label");
define("ADMINET_NOREQLAB","No required label");
define("ADMINET_ERRMSG","Error message");
define("ADMINET_FLDWID","Field width");
define("ADMINET_BOTMGN","Bottom margin space");
define("ADMINET_FRMPOS","Position in form");
define("ADMINET_NONE","None");
define("ADMINET_FLDNOTE","Please click ***1*** next to a form field to edit it or click ***2*** to add a new field.");
define("ADMINET_NOFORMS","There are no forms setup. Click New Form to create one.");
define("ADMINET_NEWFORMS","New form");
define("ADMINET_RESTOREX","Restore examples");
define("ADMINET_FORMERROR","Could not access forms");
define("ADMINET_DELETEFORM","Are you sure you want to delete this form?");
define("ADMINET_DELETESELFRM","Are you sure you want to delete the selected forms?");
define("ADMINET_NODELETE","Could not delete form");
define("ADMINET_GENCODE","Generate code");
define("ADMINET_EXAMPLES","Restore examples?");
define("ADMINET_NOEXAMPLES","Could not restore examples");
define("ADMINET_VISITOR","(visitor)");
define("ADMINET_MEMBER","(member)");
define("ADMINET_PREFILLOPT","Prefill field using (for members)");
define("ADMINET_NOPREFILL","Don't prefill");
define("ADMINET_INVFILEEXT","Invalid file extension (example .zip)");
define("ADMINET_INVSIZE","Please enter the max size in bytes");
define("ADMINET_USEAS","In admin email header use as");
define("ADMINET_INCLFOR","Include field for");
define("ADMINET_SENDTO","Send to email (one per line)");
define("ADMINET_FROMNAME","From name for user email");
define("ADMINET_REPLYTO","Reply to address for user email");
define("ADMINET_SENDAS","Send email as user");
define("ADMINET_MAXSIZE","Max Attachment size (bytes)");
define("ADMINET_ATTACHTYP","Attachment types allowed");
define("ADMINET_ATTACHTYPNT","separate with spaces. e.g. .zip .jpg .png");
define("ADMINET_USERPLACE","Username placeholder");
define("ADMINET_PASSPLACE","Password placeholder");
define("ADMINET_INCLCAPTCHA","Include Captcha");
define("ADMINET_CAPTCHAPLACE","Captcha placeholder");
define("ADMINET_INCLREM","Include remember me?");
define("ADMINET_INCLAUTO","Include auto login?");
define("ADMINET_VERSPACE","Vertical spacing");
define("ADMINET_INCLFORGOT","Include forgot password link?");
define("ADMINET_INCLSIGNUP","Include signup link?");
define("ADMINET_REGPROB1","Registration forms must have a name field.");
define("ADMINET_REGPROB2","Registration forms must have an email field.");
define("ADMINET_UPDPROB1","Existing password requirement is enabled so you should add an existing password field to the form.");
define("ADMINET_UPDPROB2","You have an existing password field in the form but this feature is not enabled in the configuration.");
define("ADMINET_CAPTCHAPROB1","CAPTCHA is enabled so you should include a CAPTCHA field in the form.");
define("ADMINET_CAPTCHAPROB2","You have CAPTCHA in the form but this feature is disabled in the configuration.");
define("ADMINET_REMPROB1","You have a remember me field but this feature is disabled in the configuration.");
define("ADMINET_REMPROB2","You have a remember me field but auto login is enabled instead in the configuration.");
define("ADMINET_AUTOPROB1","You have an auto login field but this feature is disabled in the configuration.");
define("ADMINET_AUTOPROB2","You have an auto login field but remember me is enabled instead in the configuration.");
define("ADMINET_NOPAYPAL","No Paypal products created");
define("ADMINET_TOGFIX","Toggle fixed or scrolling");




// Code generator
define("ADMINCG_EMBEDTYP","Choose the form embedding method");
define("ADMINCG_EMBED","Embed the form directly in my page. Changes to your form will appear automatically in the page.");
define("ADMINCG_SOURCE","Generate full source code that can edited further before pasting in my page.");
define("ADMINCG_STYLETYP","Choose the form style");
define("ADMINCG_FORMSTYLE","Use the styles from the form editor.");
define("ADMINCG_SIMPLESTYLE","Simplify the form style so that my existing page CSS can be used.");
define("ADMINCG_STEP1","Step1");
define("ADMINCG_STEP2","Step2");
define("ADMINCG_STEP3","Step3");
define("ADMINCG_SELECT","Select code");
define("ADMINCG_PRECODE","Copy and paste the code below and add to the very top of your page (before any DOCTYPE or &lt;HTML&gt;)");
define("ADMINCG_HEADCODE","Copy and paste the code below to the &lt;HEAD&gt; section of your page");
define("ADMINCG_BODYCODE","Copy and paste the code below to the &lt;BODY&gt; section of your page where you want the form to appear");
define("ADMINCG_PAGETEMP","Are you making a login page or login template?");
define("ADMINCG_PAGE","I am adding the login form to a page on my site.");
define("ADMINCG_TEMP","I am making a login template to replace the default login template.");
define("ADMINCG_REDSTAY","After login should the user redirect or stay on the page?");
define("ADMINCG_RED","Redirect the user to the usergroup start page.");
define("ADMINCG_STAY","Keep the user on the same page but logged in.");
define("ADMINCG_SHOWHIDE","Do you want to hide the login form if a user is already logged in?");
define("ADMINCG_SHOW","Keep the login form visible.");
define("ADMINCG_HIDE","Hide the login form if a user is logged in.");

// Manage usergroups
define("ADMINMU_GROUPERROR","Could not access usergroups");
define("ADMINMU_ADD","Add usergroup");
define("ADMINMU_NOGROUPS","There are no usergroups setup.<br>Click New Usergroup to create one.");
define("ADMINMU_DELETEGROUP","Are you sure you want to delete this usergroup?");
define("ADMINMU_DELETESELGRP","Are you sure you want to delete the selected usergroups?");
define("ADMINMU_NODELETE","Could not delete usergroup");
define("ADMINMU_CANTDELETE","You can't delete this usergroup");
define("ADMINMU_CANTDELETE","You can't delete this usergroup");
define("ADMINMU_GROUPADDED","Usergroup added");
define("ADMINMU_GROUPUPDATED","Usergroup updated");
define("ADMINMU_NONE","None");
define("ADMINMU_URL","Redirect to URL below");
define("ADMINMU_CUSTOM","Redirect to URL in - ");

// Import users
define("ADMINIU_IMPORTFILE","Import file");
define("ADMINIU_HEADER","First row is column header");
define("ADMINIU_NEWUSERS","New users");
define("ADMINIU_EMAILCOL","Use email column as username");
define("ADMINIU_EMAILCOL1","Never");
define("ADMINIU_EMAILCOL2","Only if no username provided");
define("ADMINIU_EMAILCOL3","Always - ignore any provided username");
define("ADMINIU_ADD","Add new users");
define("ADMINIU_RND","Random passwords");
define("ADMINIU_RND1","Only if no password provided");
define("ADMINIU_RND2","Yes - ignore any provided password");
define("ADMINIU_USERGROUPS","Usergroups if not set");
define("ADMINIU_SELADD","Select users added");
define("ADMINIU_UPDUSERS","Existing users");
define("ADMINIU_UPDATE","Update existing users");
define("ADMINIU_BLANK","Blank data in columns");
define("ADMINIU_BLANKUPD","Update - set field blank");
define("ADMINIU_BLANKIGN","Ignore - don't update");
define("ADMINIU_SELUPD","Select users updated");
define("ADMINIU_NOFILE","Select the file to import");
define("ADMINIU_CANTOPEN","Cant open imported file");
define("ADMINIU_COLHEAD","Cant read column headers");
define("ADMINIU_MINFIELDS","File must contain at least the username field and one other field");
define("ADMINIU_MINFIELDS2","File must contain at least the username or email field and one other field");
define("ADMINIU_USERSREAD","users read from file");
define("ADMINIU_USERSADD","users added");
define("ADMINIU_USERSIGN","users not added or updated");
define("ADMINIU_VIEW","view details");
define("ADMINIU_USERUPD","users updated");
define("ADMINIU_LINE","Line");
define("ADMINIU_ISSUE1","User not processed (missing username)");
define("ADMINIU_ISSUE2","not added (invalid created date)");
define("ADMINIU_ISSUE3","not added (invalid email)");
define("ADMINIU_ISSUE4","not added (SUBADMIN can't add ADMIN user)");
define("ADMINIU_ISSUE5","not added (required data missing)");
define("ADMINIU_ISSUE6","not added (Email address already used)");
define("ADMINIU_ISSUE7","not added");
define("ADMINIU_ISSUE8","not added (mysql error reported)");
define("ADMINIU_ISSUE9","not updated (invalid email)");
define("ADMINIU_ISSUE10","not updated (Email address already used)");
define("ADMINIU_ISSUE11","not updated (SUBADMIN can't update ADMIN user)");
define("ADMINIU_ISSUE12","not updated");
define("ADMINIU_ISSUE13","not updated (nothing to update)");
define("ADMINIU_ISSUE14","not updated (mysql error reported)");
define("ADMINIU_ISSUE15","not added (user already exists)");

// Export users
define("ADMINEU_EXPHEAD","Include header row");
define("ADMINEU_EXPTYPE","Data to export");
define("ADMINEU_EXPTYPE1","Default");
define("ADMINEU_EXPTYPE2","User defined");
define("ADMINEU_FILE","Filename");

// Log
define("ADMINLG_DTRANGE","Date range");
define("ADMINLG_DATENV","Date not valid");
define("ADMINLG_ONEENTRY","You must select at least one entry type");
define("ADMINLG_LVBLANK"," or leave either blank"); // DDMMYY or MMDDYY added to start
define("ADMINLG_ALLUSERS","Leave blank for all users");
define("ADMINLG_SELUSERS","Selected users only");
define("ADMINLG_LOGENT","Entries to include");
define("ADMINLG_LOGTYP1","Login / logout");
define("ADMINLG_LOGTYP2","Login problems");
define("ADMINLG_LOGTYP3","Password requested");
define("ADMINLG_LOGTYP4","Download");
define("ADMINLG_LOGTYP5","Download problems");
define("ADMINLG_LOGTYP6","Email sent");
define("ADMINLG_LOGTYP7","Expired access attempt");
define("ADMINLG_LOGTYP8","User modified details");
define("ADMINLG_LOGTYP9","API function call");
define("ADMINLG_LOGTYP10","User registered");
define("ADMINLG_LOGOUTPUT","Log output");
define("ADMINLG_SORTBY","Sort by");
define("ADMINLG_LIMIT","Limit output");
define("ADMINLG_ALLENTRIES","All entries");
define("ADMINLG_ENTRIES","entries");
define("ADMINLG_TZ","Timezone");
define("ADMINLG_LOCALTZ","Your local time");
define("ADMINLG_NEWEST","Newest first");
define("ADMINLG_OLDEST","Oldest first");
define("ADMINLG_VIEW","View entries");
define("ADMINLG_EXPORT","Export entries");
define("ADMINLG_DELETE","Delete entries");
define("ADMINLG_VERDEL","Delete matching log entries?");
define("ADMINLG_NODELETE","Could not delete log entries");
define("ADMINLG_DELETED"," log entries deleted");

// Configuration
define("ADMINCF_SETTINGSOK","Settings updated");
define("ADMINCF_GENSET","General settings");
define("ADMINCF_SITENAME","Site name");
define("ADMINCF_SITENAMEPR","Enter a site name (e.g. My Members Area)");
define("ADMINCF_SITEEMAIL","Admin email");
define("ADMINCF_SITEEMAILPR","Enter the  email address");
define("ADMINCF_SITEEMAIL","Admin email (secondary)");
define("ADMINCF_OPTIONAL","optional");
define("ADMINCF_SITEDATEFMT","Date format");
define("ADMINCF_DONTCHANGE","don't change after setup");
define("ADMINCF_SITEKEY","Site Key");
define("ADMINCF_SITEKEYPR","Enter a random site key (e.g. fgf73bc6w)");
define("ADMINCF_MAXSESS","Maximum session time");
define("ADMINCF_ZEROMAX","Enter 0 for maximum supported by server");
define("ADMINCF_NOT60","Should be at least 60 seconds");
define("ADMINCF_MAXINACT","Maximum inactivity time");
define("ADMINCF_LOGINCAP","Login CAPTCHA");
define("ADMINCF_REGCAP","Register CAPTCHA");
define("ADMINCF_REMME","Remember me");
define("ADMINCF_AUTOLOGIN","Auto Login");
define("ADMINCF_DEFAULTEM","Default built in");
define("ADMINCF_RNDPSSMSKEG","e.g. cccc##");
define("ADMINCF_RNDPSSMSKPR","Enter a valid random password mask (leave blank for default)");
define("ADMINCF_CONCLOGIN","Allow concurrent logins");
define("ADMINCF_FORCEDB","Force database read");
define("ADMINCF_FORCEDBNT","forces DB update on each page access");
define("ADMINCF_ALLOWSRCH","Allow search engine access");
define("ADMINCF_ALLOWSRCHNT","allows some search engine to access protected pages");
define("ADMINCF_SRCHGRP","Search engine usergroup");
define("ADMINCF_SRCHGRPNT","usergroup the search engine will access as");
define("ADMINCF_EMAILCHG","Email change verification");
define("ADMINCF_EMAILCHGNT","setting yes requires users to confirm email address via emailed link");
define("ADMINCF_EMAILUNIQUE","Email field must be unique");
define("ADMINCF_EMAILUNIQUE1","Not required");
define("ADMINCF_EMAILUNIQUE2","Required only for user entry");
define("ADMINCF_EMAILUNIQUE3","Must always be unique");
define("ADMINCF_LOGINEMAIL","Login with username or email");
define("ADMINCF_LOGINEMAIL1","Username only");
define("ADMINCF_LOGINEMAIL2","Username or email");
define("ADMINCF_PASSWORDS","Passwords");
define("ADMINCF_PASSSET","Password settings");
define("ADMINCF_FORGOTEM","Forgotten password email");
define("ADMINCF_RNDPSSMSK","Random password mask");
define("ADMINCF_PROFPASS","Profile update password required");
define("ADMINCF_PROFPASS1","Not required");
define("ADMINCF_PROFPASS2","Profile update password required");
define("ADMINCF_PROFPASS3","Required to change password");
define("ADMINCF_PROFPASSNT","password required to update profile");
define("ADMINCF_HASHPASS","Store passwords as hashes");
define("ADMINCF_HASHPASSNT","<span style=\"color: red;\">Important</span>&nbsp&nbsp;See the manual for more details");
define("ADMINCF_PAGESET","Pages &amp; templates");
define("ADMINCF_LOGOUTURL","Logout page URL");
define("ADMINCF_LOGOUTURLPR","Enter a logout page URL (e.g. http://www.yoursite.com/logout.html)");
define("ADMINCF_MSGTEMP","Message &amp; error page URL");
define("ADMINCF_TEMPLATENT","enter template filename (stored in /slpw) or leave blank for default");
define("ADMINCF_MSGTEMPPR","Check that the message template file exists (leave blank for default)");
define("ADMINCF_LGNTEMP","Login template");
define("ADMINCF_LGNTEMPPR","Check that the login template file exists (leave blank for default)");
define("ADMINCF_EXPIREDURL","Expired membership page URL");
define("ADMINCF_WRGGRPURL","Wrong group page URL");
define("ADMINCF_NOACCESSURL","No Access Page URL");
define("ADMINCF_LOGSET","Log settings");
define("ADMINCF_LOGTXT","Server path to text format log file");
define("ADMINCF_LOGTXTPR","Check that the logfile exists and has read and write permission");
define("ADMINCF_CPSET","Control panel");
define("ADMINCF_CUSTOM","Custom");
define("ADMINCF_TBLORDER","Main user table column order");
define("ADMINCF_RSTORDER","Restore default order");
define("ADMINCF_RSTORDERVER","Restore the default order?");
define("ADMINCF_SHOWROWS","Default rows to display");
define("ADMINCF_SHOWROWSNT","this is the default number of rows on the main admin page");
define("ADMINCF_CUSFLDS","Custom field titles &amp; validation");
define("ADMINCF_CUSVAL1","No validation");
define("ADMINCF_CUSVAL2","User validation only");
define("ADMINCF_CUSVAL3","Admin validation only");
define("ADMINCF_CUSVAL4","User &amp; admin  validation");
define("ADMINCF_DWNLOC","Download Locations");
define("ADMINCF_DWNPATHS","Download paths");
define("ADMINCF_DWNLOCDEF","Full file path to download folder");
define("ADMINCF_DWNLOCDEFPR1","Enter the full file path to the download file folder (e.g. /home/public_html/rt7f67dfg/");
define("ADMINCF_DWNLOCS","Additional download locations");
define("ADMINCF_PATHNOTFND","Check that path entered exists");
define("ADMINCF_DWNLOCSPR2","The name assigned to each file location must be unique");
define("ADMINCF_DWNLOCSPR3","Each specified location must have a name and file path");
define("ADMINCF_ADDLOC","Add location");
define("ADMINCF_DELLOC","Remove file location?");
define("ADMINCF_LOCNAME","name");
define("ADMINCF_LOCPATH","path");
define("ADMINCF_EMAILSET","Email settings");
define("ADMINCF_EMAILTYPE","Send email using");
define("ADMINCF_EMAILTYPE1","PHP Mail() function");
define("ADMINCF_EMAILTYPE2","PHPmailer");
define("ADMINCF_EMAILREPLYTO","Reply to email override");
define("ADMINCF_EMAILDLY","Delay between bulk emails");
define("ADMINCF_EMAILNODLY","No delay");
define("ADMINCF_SMTPUSER","Email account username");
define("ADMINCF_SMTPPWD","Email account password");
define("ADMINCF_SMTPSRV","Email account smtp server");
define("ADMINCF_SMTPPORT","Email account smtp port");
define("ADMINCF_SMTPAUTH","Email authentication");
define("ADMINCF_SMTPSEC","Email security");
define("ADMINCF_SMTPSEC1","No SSL");
define("ADMINCF_INSTPTHS","Install paths");
define("ADMINCF_INSTPTHS1","Full file path to Sitelok folder");
define("ADMINCF_INSTPTHS1PR1","Enter the full file path to the slpw folder (e.g. /home/public_html/slpw/)");
define("ADMINCF_INSTPTHS2","URL to Sitelok folder");
define("ADMINCF_INSTPTHS2PR1","Enter the full url to the slpw folder (e.g. http://www.yoursite.com/slpw/)");
define("ADMINCF_INSTPTHS3","Full file path to email folder");
define("ADMINCF_INSTPTHS3PR1","Enter the full file path to the email folder (e.g. /home/public_html/slpw/email/)");
define("ADMINCF_INSTPTHS4","URL to email template folder");
define("ADMINCF_INSTPTHS4PR1","Enter the full url to the email folder (e.g. http://www.yoursite.com/slpw/email/)");
define("ADMINCF_INSTPTHS5","Full file path to backup folder");
define("ADMINCF_INSTPTHS5PR1","Enter the full file path to the backup folder (e.g. /home/public_html/slbackups_4567ewge37534ty23/)");

// Plugins
// Plugin text is defined by individual plugins
define("ADMINPL_NOPLUGINS","There are no plugins installed.<br><a href=\"http://www.vibralogix.com/sitelokpw/plugins.php\" target=\"_blank\">See available plugins</a>.");
define("ADMINPL_CLKEN","Click to enable plugin");
define("ADMINPL_CLKDIS","Click to disable plugin");
define("ADMINPL_CONFIG","Configure plugin");
define("ADMINPL_CLKUNSTL","Click to uninstall plugin");
define("ADMINPL_DISTOUNSTL","Disable plugin before you uninstall");
define("ADMINPL_ENVER","Enable ***1*** plugin?"); // ***1*** will insert plugin name
define("ADMINPL_NOTEN","Unable to enable plugin");
define("ADMINPL_DISVER","Disable ***1*** plugin?"); // ***1*** will insert plugin name
define("ADMINPL_NOTDIS","Could not disable plugin");
define("ADMINPL_UNSTLVER","Uninstall ***1*** plugin?"); // ***1*** will insert plugin name
define("ADMINPL_NOTUNSTL","Could not uninstall plugin?");
define("ADMINPL_NOACCESS","Could not access plugins");

// Update checking
define("ADMINUPDATE_CHKUPDATES","Check for updates");
define("ADMINUPDATE_CANTUPD","Can't check for updates. Please try later.");
define("ADMINUPDATE_LATEST","You have the latest version V***1***"); // ***1*** will insert current version
// In the next define ***1*** inserts new version, ***2*** inserts current version and ***3*** inserts web site link
define("ADMINUPDATE_UPDATE","Sitelok V***1*** has been released.<br>You are currently using V***2***<br><br><a href=\"***3***\" target=\"_blank\">More information</a>");
define("ADMINUPDATE_UPDAVAIL","V***1*** available"); // ***1*** will insert new version (plugin)
define("ADMINUPDATE_UPDSAVAIL","Some plugins have updates available (shown above)");
define("ADMINUPDATE_PLGNSOK","All plugins are the latest versions");
define("ADMINUPDATE_PLGREQ","(requires Sitelok V***1***)"); // ***1*** will insert required sitelok version for plugin







