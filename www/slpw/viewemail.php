<?php
require_once("sitelokapi.php");
// Don't change message text here. Change the settings in slconfig.php if necessary
if (!defined('MSG_ACCDEN'))
  define("MSG_ACCDEN","Access Denied");
if (!defined('MSG_LINKAUTH'))
  define("MSG_LINKAUTH","Sorry this link failed. Authentication failed");
if (!defined('MSG_LINKEXP'))
  define("MSG_LINKEXP","Sorry but this link has expired");
   
if (isset($_GET['auth']))
{
  $auth=$_GET['auth'];
  $auth = rawurldecode($auth);
  $auth=base64_decode($auth);
  $linkvars=explode(",",$auth);
  $userid = trim($linkvars[0]);
  $expiry = trim($linkvars[1]);
  $template = trim($linkvars[2]);
  $hash = md5($SiteKey . $userid . $expiry . $template);
  $verifyhash = trim($linkvars[3]);
  // Get username from user id
  // Connect to mysql
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    print "Can't connect to Database";
    exit;
  }
  $query="SELECT * FROM ".$DbTableName." WHERE ".$IdField."=".sl_quote_smart($userid);
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_query===false)
  {
    print "Can't connect to Database";
    exit;
  }
  if (!$row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
  {
    print MSG_ACCDEN;
    exit;    
  }  
  $created=$row[$CreatedField];
  $username=$row[$UsernameField];
  $password=$row[$PasswordField];
  $name=$row[$NameField];
  $enabled=$row[$EnabledField];
  $email=$row[$EmailField];
  $usergroups=$row[$UsergroupsField];
  $custom1=$row[$Custom1Field];
  $custom2=$row[$Custom2Field];
  $custom3=$row[$Custom3Field];
  $custom4=$row[$Custom4Field];
  $custom5=$row[$Custom5Field];
  $custom6=$row[$Custom6Field];
  $custom7=$row[$Custom7Field];
  $custom8=$row[$Custom8Field];
  $custom9=$row[$Custom9Field];
  $custom10=$row[$Custom10Field];
  $custom11=$row[$Custom11Field];
  $custom12=$row[$Custom12Field];
  $custom13=$row[$Custom13Field];
  $custom14=$row[$Custom14Field];
  $custom15=$row[$Custom15Field];
  $custom16=$row[$Custom16Field];
  $custom17=$row[$Custom17Field];
  $custom18=$row[$Custom18Field];
  $custom19=$row[$Custom19Field];
  $custom20=$row[$Custom20Field];
  $custom21=$row[$Custom21Field];
  $custom22=$row[$Custom22Field];
  $custom23=$row[$Custom23Field];
  $custom24=$row[$Custom24Field];
  $custom25=$row[$Custom25Field];
  $custom26=$row[$Custom26Field];
  $custom27=$row[$Custom27Field];
  $custom28=$row[$Custom28Field];
  $custom29=$row[$Custom29Field];
  $custom30=$row[$Custom30Field];
  $custom31=$row[$Custom31Field];
  $custom32=$row[$Custom32Field];
  $custom33=$row[$Custom33Field];
  $custom34=$row[$Custom34Field];
  $custom35=$row[$Custom35Field];
  $custom36=$row[$Custom36Field];
  $custom37=$row[$Custom37Field];
  $custom38=$row[$Custom38Field];
  $custom39=$row[$Custom39Field];
  $custom40=$row[$Custom40Field];
  $custom41=$row[$Custom41Field];
  $custom42=$row[$Custom42Field];
  $custom43=$row[$Custom43Field];
  $custom44=$row[$Custom44Field];
  $custom45=$row[$Custom45Field];
  $custom46=$row[$Custom46Field];
  $custom47=$row[$Custom47Field];
  $custom48=$row[$Custom48Field];
  $custom49=$row[$Custom49Field];
  $custom50=$row[$Custom50Field];
  if ($verifyhash != $hash)
  {
    if (substr($LogDetails,1,1)=="Y")
	    sl_AddToLog("Login Problem",$username,"User cannot view email - authentication");
    sl_ShowMessage($MessagePage, MSG_LINKAUTH);
	  exit;
  }
  // auth is OK but we should now check if link expired
  if ($expiry != 0)
  {
    $curtime = time();
    if ($curtime > $expiry)
    {
      if (substr($LogDetails,1,1)=="Y")
  		  sl_AddToLog("Login Problem",$username,"User cannot view email - link expired");
      sl_ShowMessage($MessagePage, MSG_LINKEXP);
      exit;
    }
  }
  if (!sl_ReadEmailTemplate($template,$subject,$mailBody,$htmlformat))
  {
    if (substr($LogDetails,1,1)=="Y")
		  sl_AddToLog("Login Problem",$username,"User cannot view email - template not found");
    sl_ShowMessage($MessagePage, MSG_LINKEXP);
    exit;  
  }
  sl_PrepareEmail($mailBody,$subject,$htmlformat,$userid,$username,$password,$name,$email,$usergroups,$custom1,$custom2,$custom3,$custom4,$custom5,$custom6,$custom7,$custom8,$custom9,$custom10,
        $custom11,$custom12,$custom13,$custom14,$custom15,$custom16,$custom17,$custom18,$custom19,$custom20,$custom21,$custom22,$custom23,$custom24,$custom25,$custom26,$custom27,$custom28,$custom29,$custom30,
        $custom31,$custom32,$custom33,$custom34,$custom35,$custom36,$custom37,$custom38,$custom39,$custom40,$custom41,$custom42,$custom43,$custom44,$custom45,$custom46,$custom47,$custom48,$custom49,$custom50);
  print $mailBody;
  exit;                 
}
else
{
  if (defined('MSG_ACCDEN'))
    sl_ShowMessage($MessagePage,MSG_ACCDEN);
  else
    sl_ShowMessage($MessagePage, "Access Denied");
}
?>