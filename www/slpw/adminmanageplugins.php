<?php
  $groupswithaccess="ADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value  
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError(ADMINMSG_CSRFFAILED);
    exit;      
  }
  $act=$_POST['act'];
  $pluginid=$_POST['pluginid'];
  
  if ($act=="disable")
  {
    $mysql_link=sl_DBconnect();
    if ($mysql_link==false)
    {
      returnError(ADMINMSG_MYSQLERROR);
      exit;
    }
    $query="UPDATE ".$DbPluginsTableName." SET enabled='No' WHERE id=".sl_quote_smart($pluginid);
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError(ADMINPL_NOTDIS);
      exit;
    }
    $_SESSION['ses_ConfigReload']="reload";
  }  

  if ($act=="enable")
  {
    $mysql_link=sl_DBconnect();
    if ($mysql_link==false)
    {
      returnError(ADMINMSG_MYSQLERROR);
      exit;
    }
    $query="UPDATE ".$DbPluginsTableName." SET enabled='Yes' WHERE id=".sl_quote_smart($pluginid);
    $mysql_result=mysqli_query($mysql_link,$query);
    if ($mysql_result==false)
    {
      returnError(ADMINPL_NOTEN);
      exit;
    }
    $_SESSION['ses_ConfigReload']="reload";
  }  

  function returnError($msg)
  {
    $data['success'] = false;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

?>  
{
  "success": true,
  "message": ""
}


