<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value  
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError(ADMINMSG_CSRFFAILED);
    exit;      
  }
  $formids=$_POST['formids'];
  $formidsarray=explode(",",$formids);
  $formtype="register";
  if ($_POST['formtype']=="register")
    $formtype="register";
  if ($_POST['formtype']=="update")
    $formtype="update";
  if ($_POST['formtype']=="login")
    $formtype="login";
  if ($_POST['formtype']=="contact")
    $formtype="contact";
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError(ADMINMSG_MYSQLERROR);
    exit;
  }
  if (count($formidsarray)==0)  
  {
    returnError(ADMINET_NODELETE);
    exit;
  }
  if (count($formidsarray)==1)
    $wherequery="id=".sl_quote_smart($formids);
  if (count($formidsarray)>1)
  {
    $wherequery=str_replace(",","' OR id='",$formids);
    $wherequery="id='".$wherequery."'";
  }
  if (!$DemoMode)
    $mysql_result=mysqli_query($mysql_link,"DELETE FROM sl_forms WHERE ".$wherequery);
  else
    $mysql_result=true;
  if ($mysql_result==false)
  {
    returnError(ADMINET_NODELETE);
    exit;
  }
  if ($formtype=="register")
    $query="DELETE FROM sl_registerforms WHERE ".$wherequery;
  if ($formtype=="update")
    $query="DELETE FROM sl_updateforms WHERE ".$wherequery;
  if ($formtype=="login")
    $query="DELETE FROM sl_loginforms WHERE ".$wherequery;
  if ($formtype=="contact")
    $query="DELETE FROM sl_contactforms WHERE ".$wherequery;
  if (!$DemoMode)
    $mysql_result=mysqli_query($mysql_link,$query);
  else
    $mysql_result=true;
  if ($mysql_result==false)
  {
    returnError(ADMINET_NODELETE);
    exit;
  }

  function returnError($msg)
  {
    $data['success'] = false;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

?>  
{
  "success": true,
  "message": ""
}


