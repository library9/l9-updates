<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  require_once("slloginform.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  include("admincommonfunctions.php");
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    die(ADMINMSG_MYSQLERROR);
    exit;
  }  
  $id=$_GET['actid'];
  // If $thispage is set ok we canuse it to set variables on user pages
  $usethispage=false;
  $slpagename=$_SERVER['PHP_SELF'];
  if ($slpagename=="")
    $slpagename=$_SERVER['REQUEST_URI'];
  if ($slpagename=="")
    $slpagename=$_SERVER['SCRIPT_NAME'];
  $slpagename=basename($slpagename);
  if ($slpagename=="loginformcode.php")
    $usethispage=true;

  // Get prefix code required for different settings
  $prefix ="<?php\n";
  $prefix.="require_once(\"".$SitelokLocation."slloginform.php\");\n";
  $prefix.="\$groupswithaccess=\"PUBLIC\";\n";
  if ($usethispage)
  {
    $prefix.="\$loginpage=\$slpagename;\n";
    $prefix.="\$logoutpage=\$slpagename;\n";
  }
  else
  {
    $prefix.="\$loginpage=\"yourpage.php\";\n";
    $prefix.="\$logoutpage=\"yourpage.php\";\n";
  }  
  $prefix.="\$loginredirect=2;\n";
  $prefix.="require_once(\"".$SitelokLocation."sitelokpw.php\");\n";
  $prefix.="?>";
  $prefix = str_replace("&", "&amp;",$prefix);
  $prefix = str_replace("<", "&lt;",$prefix);
  $prefixpage=$prefix;

  $prefix="<?php require_once(\"".$SitelokLocation."slloginform.php\"); ?>";
  $prefix = str_replace("&", "&amp;",$prefix);
  $prefix = str_replace("<", "&lt;",$prefix);
  $prefixtemplate=$prefix;
     
  sl_get_login_formcode($mysql_link,$id,true,false,$css,$js,$html);
  $css = str_replace("&", "&amp;",$css);
  $css = str_replace("<", "&lt;",$css);
  $js = str_replace("&", "&amp;",$js);
  $js = str_replace("<", "&lt;",$js);
  $html = str_replace("&", "&amp;",$html);
  $html = str_replace("<", "&lt;",$html);
  $headsourcenormal=$css."\n\n".$js;
  $bodysourcenormal=$html;  

  sl_get_login_formcode($mysql_link,$id,true,true,$css,$js,$html);
  $css = str_replace("&", "&amp;",$css);
  $css = str_replace("<", "&lt;",$css);
  $js = str_replace("&", "&amp;",$js);
  $js = str_replace("<", "&lt;",$js);
  $html = str_replace("&", "&amp;",$html);
  $html = str_replace("<", "&lt;",$html);
  $headsourcesimple=$css."\n\n".$js;
  $bodysourcesimple=$html;    

  $head = "<?php sl_loginformhead({$id},false); ?>";
  $head = sl_rwadjust($head);
  $head = str_replace("&", "&amp;",$head);
  $head = str_replace("<", "&lt;",$head);
  $headembednormal=$head;
  $body = "<?php sl_loginformbody({$id},false,\$msg); ?>";
  $body = sl_rwadjust($body);
  $body = str_replace("&", "&amp;",$body);
  $body = str_replace("<", "&lt;",$body);
  $bodyembednormal=$body;

  $head = "<?php sl_loginformhead({$id},true); ?>";
  $head = sl_rwadjust($head);
  $head = str_replace("&", "&amp;",$head);
  $head = str_replace("<", "&lt;",$head);
  $headembedsimple=$head;
  $body = "<?php sl_loginformbody({$id},true,\$msg); ?>";
  $body = sl_rwadjust($body);
  $body = str_replace("&", "&amp;",$body);
  $body = str_replace("<", "&lt;",$body);
  $bodyembedsimple=$body;

?>
<!DOCTYPE html>
<html>
<head>
<?php
$pagename="loginformcode";
include("adminhead.php");
?>
<title><?php echo ADMINMENU_LOGINFORMCODE; ?></title>
<link rel="stylesheet" href="formcode.css"></head>
<?php include("adminthemeheader.php"); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-list-alt"></i>&nbsp;<?php echo ADMINMENU_LOGINFORMCODE; ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><a href="loginforms.php"><?php echo ADMINMENU_LOGINFORMS; ?></a></li>
            <li class="active"><?php echo ADMINMENU_LOGINFORMCODE; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>
          <!-- Your Page Content Here -->
          <div class="row">

            <div class="col-xs-12">
<form class="form-horizontal">          
              <div class="box">

                <div class="box-body">

<h3><?php echo ADMINCG_PAGETEMP; ?></h3>
<div class="form-group">
    <div class="col-xs-12">
      <div class="radio">
        <label>
          <p class="hanging-indent"><input type="radio" name="pagetype" id="page" value="page" checked="checked">&nbsp;&nbsp;<?php echo ADMINCG_PAGE; ?></p>
        </label>
      </div>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12">
      <div class="radio">
        <label>
          <p class="hanging-indent"><input type="radio" name="pagetype" id="template" value="template">&nbsp;&nbsp;<?php echo ADMINCG_TEMP; ?></p>
        </label>
      </div>
    </div>
</div>

<h3><?php echo ADMINCG_EMBEDTYP; ?></h3>
<div class="form-group">
    <div class="col-xs-12">
      <div class="radio">
        <label>
          <p class="hanging-indent"><input type="radio" name="embedmethod" id="embed" value="embed" checked="checked">&nbsp;&nbsp;<?php echo ADMINCG_EMBED; ?></p>
        </label>
      </div>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12">
      <div class="radio">
        <label>
          <p class="hanging-indent"><input type="radio" name="embedmethod" id="source" value="source">&nbsp;&nbsp;<?php echo ADMINCG_SOURCE; ?></p>
        </label>
      </div>
    </div>
</div>

<h3><?php echo ADMINCG_STYLETYP; ?></h3>
<div class="form-group">
    <div class="col-xs-12">
      <div class="radio">
        <label>
          <p class="hanging-indent"><input type="radio" name="styletype" id="style" value="style" checked="checked">&nbsp;&nbsp;<?php echo ADMINCG_FORMSTYLE; ?></p>
        </label>
      </div>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12">
      <div class="radio">
        <label>
          <p class="hanging-indent"><input type="radio" name="styletype" id="simple" value="simple">&nbsp;&nbsp;<?php echo ADMINCG_SIMPLESTYLE; ?></p>
        </label>
      </div>
    </div>
</div>

<div id="templatesettings">
  <h3><?php echo ADMINCG_REDSTAY; ?></h3>
  <div class="form-group">
      <div class="col-xs-12">
        <div class="radio">
          <label>
            <p class="hanging-indent"><input type="radio" name="redirecttype" id="redirect" value="redirect" checked="checked">&nbsp;&nbsp;<?php echo ADMINCG_RED; ?></p>
          </label>
        </div>
      </div>
  </div>
  <div class="form-group">
      <div class="col-xs-12">
        <div class="radio">
          <label>
            <p class="hanging-indent"><input type="radio" name="redirecttype" id="stay" value="stay">&nbsp;&nbsp;<?php echo ADMINCG_STAY; ?></p>
          </label>
        </div>
      </div>
  </div>

  <h3><?php echo ADMINCG_SHOWHIDE; ?></h3>
  <div class="form-group">
      <div class="col-xs-12">
        <div class="radio">
          <label>
            <p class="hanging-indent"><input type="radio" name="hidetype" id="show" value="show" checked="checked">&nbsp;&nbsp;<?php echo ADMINCG_SHOW; ?></p>
          </label>
        </div>
      </div>
  </div>
  <div class="form-group">
      <div class="col-xs-12">
        <div class="radio">
          <label>
            <p class="hanging-indent"><input type="radio" name="hidetype" id="hide" value="hide">&nbsp;&nbsp;<?php echo ADMINCG_HIDE; ?></p>
          </label>
        </div>
      </div>
  </div>
</div>  

<div id="prefixblock">
  <h3><?php echo ADMINCG_STEP1; ?></h3>
  <p><?php echo ADMINCG_PRECODE; ?></p>
</div>
<div id="prefixpage">
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="8" class="form-control" readonly name="prefixcodepage" id="prefixcodepage"><?php echo $prefixpage; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('prefixcodepage').focus(); document.getElementById('prefixcodepage').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>

<div id="prefixtemplate">
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="4" class="form-control" readonly name="prefixcodetemplatet" id="prefixcodetemplate"><?php echo $prefixtemplate; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('prefixcodetemplate').focus(); document.getElementById('prefixcodetemplate').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>

<h3 id="headstep"><?php echo ADMINCG_STEP2; ?></h3>
<p><?php echo ADMINCG_HEADCODE; ?></p>
<div id="headsourcenormal">
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="16" class="form-control" readonly name="headcodesourcenormal" id="headcodesourcenormal"><?php echo $headsourcenormal; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('headcodesourcenormal').focus(); document.getElementById('headcodesourcenormal').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>

<div id="headsourcesimple">
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="16" class="form-control" readonly name="headcodesourcesimple" id="headcodesourcesimple"><?php echo $headsourcesimple; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('headcodesourcesimple').focus(); document.getElementById('headcodesourcesimple').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>

<div id="headembednormal">
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="4" class="form-control" readonly name="headcodeembednormal" id="headcodeembednormal"><?php echo $headembednormal; ?></textarea>
    </div>   
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('headcodeembednormal').focus(); document.getElementById('headcodeembednormal').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>

<div id="headembedsimple">
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="4" class="form-control" readonly name="headcodeembedsimple" id="headcodeembedsimple"><?php echo $headembedsimple; ?></textarea>
    </div>   
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('headcodeembedsimple').focus(); document.getElementById('headcodeembedsimple').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>

<h3 id="bodystep"><?php echo ADMINCG_STEP2; ?></h3>
<p><?php echo ADMINCG_BODYCODE; ?></p>
<div id="bodysourcenormal">
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="16" class="form-control" readonly name="bodycodesourcenormal" id="bodycodesourcenormal"><?php echo $bodysourcenormal; ?></textarea>
    </div>   
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('bodycodesourcenormal').focus(); document.getElementById('bodycodesourcenormal').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>

<div id="bodysourcesimple">
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="16" class="form-control" readonly name="bodycodesourcesimple" id="bodycodesourcesimple"><?php echo $bodysourcesimple; ?></textarea>
    </div>   
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('bodycodesourcesimple').focus(); document.getElementById('bodycodesourcesimple').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>

<div id="bodyembednormal">
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="4" class="form-control" readonly name="bodycodeembednormal" id="bodycodeembednormal"><?php echo $bodyembednormal; ?></textarea>
    </div>   
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('bodycodeembednormal').focus(); document.getElementById('bodycodeembednormal').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>

<div id="bodyembedsimple">
  <div class="form-group">    
    <div class="col-xs-12">
       <textarea rows="4" class="form-control" readonly name="bodycodeembedsimple" id="bodycodeembedsimple"><?php echo $bodyembedsimple; ?></textarea>
    </div>   
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="button" id="selectcodebutton-go" name="selectbutton" class="btn btn-primary btn-xs pull-left" value="<?php echo ADMINCG_SELECT; ?>" onclick="document.getElementById('bodycodeembedsimple').focus(); document.getElementById('bodycodeembedsimple').select();"><?php echo ADMINCG_SELECT; ?></button>
    </div>   
  </div>
</div>



                </div><!-- /.box-body -->
              </div><!-- /.box -->

<div class="form-group">
  <div class="col-xs-12">
    <div class="btn-toolbar">
        <button type="button" id="cancel" class="btn btn-primary pull-left" onclick="window.location.href='loginforms.php';"><?php echo ADMINBUTTON_RETURNTOFORMS ?></button>
        <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
    </div> 
  </div>    
</div>

</form>
            </div><!-- /.col -->





          </div><!-- /.row -->

<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>


        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("adminthemefooter.php"); ?>
    <script src="loginformcode.js"></script>

  </body>
</html>
