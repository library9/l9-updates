<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  $adminajaxcall=true;
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");  
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  // Check CSRF value  
  if ($_POST['slcsrf']!=$_SESSION['ses_slcsrf'])
  {
    returnError(ADMINMSG_CSRFFAILED);
    exit;
  }
  $formtype="all";
  if ($_POST['formtype']=="register")
    $formtype="register";
  if ($_POST['formtype']=="update")
    $formtype="update";
  if ($_POST['formtype']=="login")
    $formtype="login";
  if ($_POST['formtype']=="contact")
    $formtype="contact";
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
  {
    returnError(ADMINMSG_MYSQLERROR);
    exit;
  }
  if ($formtype=="all")
    $query="SELECT * FROM sl_forms ORDER BY name ASC";
  else
    $query="SELECT * FROM sl_forms WHERE type='".$formtype."' ORDER BY name ASC"; 
  $mysql_result=mysqli_query($mysql_link,$query);
  if ($mysql_result==false)
  {
    returnError(ADMINMSG_MYSQLERROR);
    exit;
  }
  $formnames=array();
  $formids=array();
  $numforms=mysqli_num_rows($mysql_result);
  while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
  {
    $formids[]=$row['id'];
    $formnames[]=$row['name'];
    $formtypes[]=$row['type'];
  }




  function returnError($msg)
  {
    $data['success'] = false;
    $data['message'] = $msg;
    echo json_encode($data);
    exit;
  }

  ?>
{
  "success": true,
  "message": "",
  "formcount": <?php echo $numforms; ?>,
  "formtypes": [
<?php
  for ($k=0;$k<$numforms;$k++)
  {
?>
      <?php echo json_encode($formtypes[$k]); if ($k!=($numforms-1)) echo ",";?>

<?php    
  }
?>
  ],  "formnames": [
<?php
  for ($k=0;$k<$numforms;$k++)
  {
?>
      <?php echo json_encode($formnames[$k]); if ($k!=($numforms-1)) echo ",";?>

<?php    
  }
?>
  ],
  "formids": [
<?php
  for ($k=0;$k<$numforms;$k++)
  {
?>
      <?php echo json_encode($formids[$k]); if ($k!=($numforms-1)) echo ",";?>

<?php    
  }
?>
  ]
}


