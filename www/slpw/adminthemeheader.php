<?php
// See which page is being displayed
if (!isset($pagename))
  $pagename="";
// Get number of selected users if not already stored
$numsel=$_SESSION['slnumselected'];
if (!is_numeric($numsel))
{
  $mysql_link=sl_DBconnect();
  if ($mysql_link!=false)
  {
  $selquery="SELECT count(*) FROM ".$DbTableName." WHERE ".$SelectedField."='Yes'";
  $mysql_result=mysqli_query($mysql_link,$selquery);
  $row = mysqli_fetch_row($mysql_result);
  if ($row!=false)
    $numsel = $row[0];
  else  
    $numsel = 0;
  // Store numselected in session so other pages have access
  $_SESSION['slnumselected']=$numsel;
  } 
  else
    $numsel=0;
}
// Update page names and icon image names if plugin supports V5 directly
for ($p=0;$p<$slnumplugins;$p++)
{
  if ((isset($slplugin_adminv5[$p])) && ($slplugin_adminv5[$p]))
  {
    $slplugin_adminmenupage[$p]=str_replace(".php", "v5.php", $slplugin_adminmenupage[$p]);
    $slplugin_adminpluginmenupage[$p]=str_replace(".php", "v5.php", $slplugin_adminpluginmenupage[$p]);
    $slplugin_adminuserpage[$p]=str_replace(".php", "v5.php", $slplugin_adminuserpage[$p]);
    $slplugin_adminselectedpage[$p]=str_replace(".php", "v5.php", $slplugin_adminselectedpage[$p]);
    $slplugin_icon[$p]=str_replace(".png", "v5.png", $slplugin_icon[$p]);
    $slplugin_adminusericon[$p]=str_replace(".png", "v5.png", $slplugin_adminusericon[$p]);
    $slplugin_adminselectedicon[$p]=str_replace(".png", "v5.png", $slplugin_adminselectedicon[$p]);
  }
  else
    $slplugin_adminv5[$p]=false; 
}
?>
<?php
           
 $actual_link = 'http://'.$_SERVER['HTTP_HOST'];
$host='localhost';
$username='root';
$password='usbw';
$database="zadmin_l9";
$con=mysql_connect($host, $username, $password)or die("cannot connect");
@mysql_select_db($database) or die( "Unable to select database");
$res = mysql_query("update slconfig set logoutpage='".$actual_link."' where confignum='1'")  or die(mysql_error()."update failed");
		?>
  <body class="hold-transition skin-<?php echo $adminskin; ?> sidebar-mini fixed" onpageshow="if (event.persisted) onPageShow();">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo $SitelokLocationURLPath; ?>index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="<?php echo $SitelokLocationURLPath; ?>adminlte/img/adminlogomini.png"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="<?php echo $SitelokLocationURLPath; ?>adminlte/img/adminlogo.png"></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>      
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

<?php
if(isset($slplugin_admintopnav))
{
  asort($slplugin_admintopnav);
  foreach ($slplugin_admintopnav as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmintopnav.php");
  } 
}
?>

              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="<?php if ($slprofileimage=='') echo $SitelokLocationURLPath.'adminlte/img/noprofileimage.png'; else echo $slprofileimage; ?>" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?php echo $slusername; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="<?php if ($slprofileimage=='') echo $SitelokLocationURLPath.'adminlte/img/noprofileimage.png'; else echo $slprofileimage; ?>" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $slname ?>
                      <small><?php echo $slemail;?></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
<!--                   <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">link1</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">link2</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">link3</a>
                    </div>
                  </li>
 -->                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo $SitelokLocationURLPath; ?>edituser.php?userid=<?php echo $sluserid; ?>" class="btn btn-default btn-flat"><?php echo ADMINBUTTON_EDITUSER; ?></a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php siteloklogout(); ?>" class="btn btn-default btn-flat"><?php echo ADMINMENU_LOGOUT; ?></a>
                    </div>
                  </li>
                </ul>
              </li>


            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

 
          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header"><?php echo ADMINGENERAL_MAINNAV; ?></li>

            <li><a href="<?php echo $SitelokLocationURLPath; ?>index.php"><i class="glyphicon glyphicon-home"></i> <span><?php echo ADMINMENU_HOME; ?></span></a></li>


<?php if (!$slsubadmin) { ?>
            <li><a href="<?php echo $SitelokLocationURLPath; ?>groupmanage.php"><i class="fa fa-group"></i> <span><?php echo ADMINMENU_USERGROUPS; ?></span></a></li>
<?php } ?>
            <li><a href="<?php echo $SitelokLocationURLPath; ?>logmanage.php"><i class="fa fa-align-justify"></i> <span><?php echo ADMINMENU_LOG; ?></span></a></li>

            <li><a href="<?php echo $SitelokLocationURLPath; ?>emailuser.php?act=managetemplates"><i class="fa fa-envelope-o"></i> <span><?php echo ADMINMENU_EMAILTEMPLATES; ?></span></a></li>


        


            <li><a href="<?php siteloklogout(); ?>"><i class="glyphicon glyphicon-log-out"></i> <span> <?php echo ADMINMENU_LOGOUT; ?></span></a></li>

<?php
// Custom menu entries
if (file_exists($SitelokLocation."slcustommenu.php"))
  include($SitelokLocation."slcustommenu.php"); 
?>

          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
