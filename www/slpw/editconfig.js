$('.selectpicker').selectpicker({
  style: 'btn-default',
  size: 'auto',
  dropupAuto: false
});

$('#newsearchenginegroup').editableSelect();

//$( ".form-control" ).addClass( "input-sm" );
//$( ".btn" ).addClass( "btn-sm" );


updateForm();

$("#forgotemailtype").change(function(){
    updateForm();
});

$("#newallowsearchengine").change(function(){
    updateForm();
});

$("#newemailconfirmrequired").change(function(){
    updateForm();
});

$("#newemailtype").change(function(){
    updateForm();
});

// Setup selectall checkbox
$('#selectall').on('ifChecked', function(event){
               selectAll();
});
$('#selectall').on('ifUnchecked', function(event){
               selectAll();
});

// Remove iCheck from the checkbox on the login form preview
$('.noicheck').iCheck('destroy');

// Clear order number if check =box unchecked
$('.cborder').change(function() {
  if (!$(this).prop("checked"))
    $(this).parent().parent().find('input').val('');
}); 

// Check for valid value
$('.order').focusout(function() {
  var ord=$(this).val();
  if (validInteger(ord,1,60,true))
    $(this).parent().parent().find('input').prop( "checked", true );
  else
  {
    $(this).val('');
    $(this).parent().parent().find('input').prop( "checked", false );
  }
}); 

// Check for valid value for actionitems which is required
$('.orderaction').focusout(function() {
  var ord=$(this).val();
  if (!validInteger(ord,1,60,true))
    $(this).val('1');
}); 

// Check for valid value for selected which is required
$('.orderselect').focusout(function() {
  var ord=$(this).val();
  if (!validInteger(ord,1,60,true))
    $(this).val('2');
}); 

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  if (($(window).width() < 992) && ($(window).height() < 600))
  {
    var target = $(e.target).attr("href") // activated tab
    if (target!="#general")
      setTimeout(function() { $(target).get(0).scrollIntoView(); }, 300);  // Allow panel to open before focus
  }
});

// General settings form handler
$('#generalform').submit(function(event) {
    var formtype="general";
    // remove the error classes
    $('#newsitenamediv').removeClass('has-error');
    $('#newsiteemaildiv').removeClass('has-error');
    $('#newsiteemail2div').removeClass('has-error');
    $('#newsitekeydiv').removeClass('has-error');
    $('#newmaxsessiontimediv').removeClass('has-error');
    $('#newmaxinactivitytimediv').removeClass('has-error');
    $('#forgotemailtypediv').removeClass('has-error');
    $('#newrandompasswordmaskdiv').removeClass('has-error');
    $('#newsearchenginegroupdiv').removeClass('has-error');
    $('#newemailconfirmrequireddiv').removeClass('has-error');
     // remove the error messages
    $('#newsitenamehelp').remove();
    $('#newsiteemailhelp').remove();
    $('#newsiteemail2help').remove();
    $('#newsitekeyhelp').remove();
    $('#newmaxsessiontimehelp').remove();
    $('#newmaxinactivitytimehelp').remove();
    $('#forgotemailtypehelp').remove();
    $('#newrandompasswordmaskhelp').remove();
    $('#newsearchenginegrouphelp').remove();
    $('#newemailconfirmrequiredhelp').remove();
    //Remove form message
    $('#result'+formtype).html('');
    // Validate using JS first
    var data={success:true,message:'',errors:{}};
    // Sitename
    var newsitename=$( "#newsitename" ).val().trim();
    if (newsitename=="")
      data.errors.newsitename=ADMINCF_SITENAMEPR;
    // Admin email
    var newsiteemail=$( "#newsiteemail" ).val().trim();
    if (newsiteemail=="")
      data.errors.newsiteemail=ADMINCF_SITEEMAILPR;
    if ((newsiteemail!="") && (!validateEmail(newsiteemail)))
      data.errors.newsiteemail=ADMINMSG_EMAILINVALID;
    // Admin secondary email
    var newsiteemail2=$( "#newsiteemail2" ).val().trim();
    if ((newsiteemail2!="") && (!validateEmail(newsiteemail2)))
      data.errors.newsiteemail2=ADMINMSG_EMAILINVALID;
    // Site key
    var newsitekey=$( "#newsitekey" ).val().trim();
    if (newsitekey=="")
      data.errors.newsitekey=ADMINCF_SITEKEYPR;
    // Max session
    var newmaxsessiontime=$( "#newmaxsessiontime" ).val();
    if (!validInteger(newmaxsessiontime,0,9999999,true))
      data.errors.newmaxsessiontime=ADMINMSG_NOTNUMBER;
    else if ((newmaxsessiontime>0) && (newmaxsessiontime<60))
      data.errors.newmaxsessiontime=ADMINCF_NOT60;
    // Max inactivity
    var newmaxinactivitytime=$( "#newmaxinactivitytime" ).val();
    if (!validInteger(newmaxinactivitytime,0,9999999,true))
      data.errors.newmaxinactivitytime=ADMINMSG_NOTNUMBER;
    else if ((newmaxinactivitytime>0) && (newmaxinactivitytime<60))
      data.errors.newmaxinactivitytime=ADMINCF_NOT60;
    // Forgotten email template
    var forgotemailtype=$( "#forgotemailtype" ).val();
    var newforgottenemail=$( "#newforgottenemail" ).val().trim();
    if ((forgotemailtype=="template") && (newforgottenemail==""))
      data.errors.forgotemailtype=ADMINMSG_NOTEMPLATE;
    if ((forgotemailtype=='template') && (newforgottenemail!=""))
    {  
      var fnext = newforgottenemail.substr((newforgottenemail.lastIndexOf('.') + 1));
      fnext=fnext.toLowerCase();
      if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
        data.errors.forgotemailtype=ADMINMSG_TEMPLATETYPE;
    }
    // Random password mask
    var newrandompasswordmask=$( "#newrandompasswordmask" ).val().trim();
    if ((newrandompasswordmask!="") && (!validChars("cCX#AU",newrandompasswordmask)))
       data.errors.newrandompasswordmask=ADMINCF_RNDPSSMSKPR;
    // Search engine usergroup
    var newallowsearchengine=$( "#newallowsearchengine" ).val();
    var newsearchenginegroup=$( "#newsearchenginegroup" ).val();
    if ((newallowsearchengine=="1") && (!validUsergroup(newsearchenginegroup,true)))
       data.errors.newsearchenginegroup=ADMINMSG_GROUPINVALID;
    // Email change verification
    var newemailconfirmrequired=$( "#newemailconfirmrequired" ).val();
    var newemailconfirmtemplate=$( "#newemailconfirmtemplate" ).val().trim();
    if ((newemailconfirmrequired=="1") && (newemailconfirmtemplate==""))
      data.errors.newemailconfirmrequired=ADMINMSG_NOTEMPLATE;
    if ((newemailconfirmrequired=='1') && (newemailconfirmtemplate!=""))
    {  
      var fnext = newemailconfirmtemplate.substr((newemailconfirmtemplate.lastIndexOf('.') + 1));
      fnext=fnext.toLowerCase();
      if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
        data.errors.newemailconfirmrequired=ADMINMSG_TEMPLATETYPE;
    }
    if (Object.keys(data.errors).length>0)
    {
      data.success=false;
      data.message=ADMINMSG_FORMERROR;
      //data=JSON.stringify(data);
      // Show validation results from JS in form
      showValidation(data,'result'+formtype);
      // stop the form from submitting the normal way and refreshing the page
      event.preventDefault();
      return;
    }  
        
    // Now call PHP for final validation and processing
    var label=showButtonBusy('submit'+formtype);
    var formData = $('#'+formtype+'form').serialize();
    formData=formData+"&slcsrf="+slcsrf+"&settingtype="+formtype;
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : 'admineditconfig.php', // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
    .done(function(data) {
      sessionExpiredReported(data,true);
      // Show validation results from PHP in form
      showValidation(data,'result'+formtype);
      hideButtonBusy('submit'+formtype,label);
    })
    .fail(function(data) {
    hideButtonBusy('submit'+formtype,label);  
    $('#result'+formtype).html('<div id="result'+formtype+'message" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
    });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});

// Password settings form handler
$('#passwordform').submit(function(event) {
    var formtype="password";
    // remove the error classes
    $('#forgotemailtypediv').removeClass('has-error');
    $('#newrandompasswordmaskdiv').removeClass('has-error');
     // remove the error messages
    $('#forgotemailtypehelp').remove();
    $('#newrandompasswordmaskhelp').remove();
    //Remove form message
    $('#result'+formtype).html('');
    // Validate using JS first
    var data={success:true,message:'',errors:{}};
    // Forgotten email template
    var forgotemailtype=$( "#forgotemailtype" ).val();
    var newforgottenemail=$( "#newforgottenemail" ).val().trim();
    if ((forgotemailtype=="template") && (newforgottenemail==""))
      data.errors.forgotemailtype=ADMINMSG_NOTEMPLATE;
    if ((forgotemailtype=='template') && (newforgottenemail!=""))
    {  
      var fnext = newforgottenemail.substr((newforgottenemail.lastIndexOf('.') + 1));
      fnext=fnext.toLowerCase();
      if ((fnext!="html") && (fnext!="htm") && (fnext!="txt"))
        data.errors.forgotemailtype=ADMINMSG_TEMPLATETYPE;
    }
    // Random password mask
    var newrandompasswordmask=$( "#newrandompasswordmask" ).val().trim();
    if ((newrandompasswordmask!="") && (!validChars("cCX#AU",newrandompasswordmask)))
       data.errors.newrandompasswordmask=ADMINCF_RNDPSSMSKPR;
    if (Object.keys(data.errors).length>0)
    {
      data.success=false;
      data.message=ADMINMSG_FORMERROR;
      //data=JSON.stringify(data);
      // Show validation results from JS in form
      showValidation(data,'result'+formtype);
      // stop the form from submitting the normal way and refreshing the page
      event.preventDefault();
      return;
    }  
        
    // Now call PHP for final validation and processing
    var label=showButtonBusy('submit'+formtype);
    var formData = $('#'+formtype+'form').serialize();
    formData=formData+"&slcsrf="+slcsrf+"&settingtype="+formtype;
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : 'admineditconfig.php', // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
    .done(function(data) {
      sessionExpiredReported(data,true);
      // Show validation results from PHP in form
      showValidation(data,'result'+formtype);
      hideButtonBusy('submit'+formtype,label);
    })
    .fail(function(data) {
    hideButtonBusy('submit'+formtype,label);  
    $('#result'+formtype).html('<div id="result'+formtype+'message" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
    });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});

// Page and template settings form handler
$('#pagestemplatesform').submit(function(event) {
    var formtype="pagestemplates";
    // remove the error classes
    $('#newlogoutpagediv').removeClass('has-error');
    $('#newmessagetemplatediv').removeClass('has-error');
    $('#newlogintemplatediv').removeClass('has-error');
     // remove the error messages
    $('#newlogoutpagehelp').remove();
    $('#newmessagetemplatehelp').remove();
    $('#newlogintemplatehelp').remove();
    //Remove form message
    $('#result'+formtype).html('');
    // Validate using JS first
    var data={success:true,message:'',errors:{}};
    // Logout page URL
    var newlogoutpage=$( "#newlogoutpage" ).val().trim();
    if (newlogoutpage=="")
      data.errors.newlogoutpage=ADMINCF_LOGOUTURLPR;
    if (Object.keys(data.errors).length>0)
    {
      data.success=false;
      data.message=ADMINMSG_FORMERROR;
      //data=JSON.stringify(data);
      // Show validation results from JS in form
      showValidation(data,'result'+formtype);
      // stop the form from submitting the normal way and refreshing the page
      event.preventDefault();
      return;
    }  
        
    // Now call PHP for final validation and processing
    var label=showButtonBusy('submit'+formtype);
    var formData = $('#'+formtype+'form').serialize();
    formData=formData+"&slcsrf="+slcsrf+"&settingtype="+formtype;
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : 'admineditconfig.php', // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
    .done(function(data) {
      sessionExpiredReported(data,true);
      // Show validation results from PHP in form
      showValidation(data,'result'+formtype);
      hideButtonBusy('submit'+formtype,label);
    })
    .fail(function(data) {
    hideButtonBusy('submit'+formtype,label);  
    $('#result'+formtype).html('<div id="result'+formtype+'message" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
    });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});

// Log settings form handler
$('#logsettingsform').submit(function(event) {
    var formtype="logsettings";
    // remove the error classes
    $('#newsiteloklogdiv').removeClass('has-error');
     // remove the error messages
    $('#newsitelokloghelp').remove();
    //Remove form message
    $('#result'+formtype).html('');
    // Validate using JS first
    var data={success:true,message:'',errors:{}};
    if (Object.keys(data.errors).length>0)
    {
      data.success=false;
      data.message=ADMINMSG_FORMERROR;
      //data=JSON.stringify(data);
      // Show validation results from JS in form
      showValidation(data,'result'+formtype);
      // stop the form from submitting the normal way and refreshing the page
      event.preventDefault();
      return;
    }  
        
    // Now call PHP for final validation and processing
    var label=showButtonBusy('submit'+formtype);
    var formData = $('#'+formtype+'form').serialize();
    formData=formData+"&slcsrf="+slcsrf+"&settingtype="+formtype;
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : 'admineditconfig.php', // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
    .done(function(data) {
      sessionExpiredReported(data,true);
      // Show validation results from PHP in form
      showValidation(data,'result'+formtype);
      hideButtonBusy('submit'+formtype,label);
    })
    .fail(function(data) {
    hideButtonBusy('submit'+formtype,label);  
    $('#result'+formtype).html('<div id="result'+formtype+'message" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
    });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});

// Control panel settings form handler
$('#controlpanelform').submit(function(event) {
    var formtype="controlpanel";
    //Remove form message
    $('#result'+formtype).html('');
    // Validate using JS first
    var data={success:true,message:'',errors:{}};
    if (Object.keys(data.errors).length>0)
    {
      data.success=false;
      data.message=ADMINMSG_FORMERROR;
      //data=JSON.stringify(data);
      // Show validation results from JS in form
      showValidation(data,'result'+formtype);
      // stop the form from submitting the normal way and refreshing the page
      event.preventDefault();
      return;
    }  
    // Now call PHP for final validation and processing
    var label=showButtonBusy('submit'+formtype);
    var formData = $('#'+formtype+'form').serialize();
    formData=formData+"&slcsrf="+slcsrf+"&settingtype="+formtype;
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : 'admineditconfig.php', // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
    .done(function(data) {
      sessionExpiredReported(data,true);
      // Show validation results from PHP in form
      showValidation(data,'result'+formtype);
      hideButtonBusy('submit'+formtype,label);
    })
    .fail(function(data) {
    hideButtonBusy('submit'+formtype,label);  
    $('#result'+formtype).html('<div id="result'+formtype+'message" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
    });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});

// Custom field settings form handler
$('#customfieldsform').submit(function(event) {
    var formtype="customfields";
    //Remove form message
    $('#result'+formtype).html('');
    // Validate using JS first
    var data={success:true,message:'',errors:{}};
    if (Object.keys(data.errors).length>0)
    {
      data.success=false;
      data.message=ADMINMSG_FORMERROR;
      //data=JSON.stringify(data);
      // Show validation results from JS in form
      showValidation(data,'result'+formtype);
      // stop the form from submitting the normal way and refreshing the page
      event.preventDefault();
      return;
    }  
        
    // Now call PHP for final validation and processing
    var label=showButtonBusy('submit'+formtype);
    var formData = $('#'+formtype+'form').serialize();
    formData=formData+"&slcsrf="+slcsrf+"&settingtype="+formtype;
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : 'admineditconfig.php', // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
    .done(function(data) {
      sessionExpiredReported(data,true);
      // Show validation results from PHP in form
      showValidation(data,'result'+formtype);
      hideButtonBusy('submit'+formtype,label);
    })
    .fail(function(data) {
    hideButtonBusy('submit'+formtype,label);  
    $('#result'+formtype).html('<div id="result'+formtype+'message" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
    });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});

// Download location settings form handler
$('#downloadform').submit(function(event) {
    var formtype="download";
    // remove the error classes
    $('#newfilelocationdiv').removeClass('has-error');
    for (k=1;k<locationcount;k++)
    {
      if ( !$( '#newfilelocationsname'+k ).length )
        continue;
      $('#newfilelocationsname'+k+'div').removeClass('has-error');
      $('#newfilelocations'+k+'div').removeClass('has-error');
    }    
     // remove the error messages
    $('#newfilelocationhelp').remove();
    for (k=1;k<locationcount;k++)
    {
      if ( !$( '#newfilelocationsname'+k ).length )
        continue;
      $('#newfilelocationsname'+k+'help').remove();
      $('#newfilelocations'+k+'help').remove();
    }    
    //Remove form message
    $('#result'+formtype).html('');
    // Validate using JS first
    var data={success:true,message:'',errors:{}};
    if (Object.keys(data.errors).length>0)
    {
      data.success=false;
      data.message=ADMINMSG_FORMERROR;
      //data=JSON.stringify(data);
      // Show validation results from JS in form
      showValidation(data,'result'+formtype);
      // stop the form from submitting the normal way and refreshing the page
      event.preventDefault();
      return;
    }  
        
    // Now call PHP for final validation and processing
    var label=showButtonBusy('submit'+formtype);
    var formData = $('#'+formtype+'form').serialize();
    formData=formData+"&slcsrf="+slcsrf+"&settingtype="+formtype+"&locationcount="+(locationcount-1);
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : 'admineditconfig.php', // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
    .done(function(data) {
      sessionExpiredReported(data,true);
      // Show validation results from PHP in form
      showValidation(data,'result'+formtype);
      hideButtonBusy('submit'+formtype,label);
    })
    .fail(function(data) {
    hideButtonBusy('submit'+formtype,label);  
    $('#result'+formtype).html('<div id="result'+formtype+'message" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
    });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});

// Email settings form handler
$('#emailform').submit(function(event) {
    var formtype="email";
    // remove the error classes
    $('#newemailreplytodiv').removeClass('has-error');
    $('#newemailusernamediv').removeClass('has-error');
    $('#newemailpassworddiv').removeClass('has-error');
    $('#newemailserverdiv').removeClass('has-error');
    $('#newemailportdiv').removeClass('has-error');
     // remove the error messages
    $('#newemailreplytohelp').remove();
    $('#newemailusernamehelp').remove();
    $('#newemailpasswordhelp').remove();
    $('#newemailserverhelp').remove();
    $('#newemailporthelp').remove();
    //Remove form message
    $('#result'+formtype).html('');
    // Validate using JS first
    var data={success:true,message:'',errors:{}};
    var newemailtype=$('#newemailtype').val();
    var newemailreplyto=$( "#newemailreplyto" ).val().trim();
    if ((newemailreplyto!="") && (!validateEmail(newemailreplyto)))    
      data.errors.newemailreplyto=ADMINMSG_EMAILINVALID;
    // check SMTP settings entered for PHPmailer
    if (newemailtype==1)
    {
      var newemailusername=$('#newemailusername').val().trim();
//      if (newemailusername=="")
//        data.errors.newemailusername=ADMINMSG_FIELDREQD;
      var newemailpassword=$('#newemailpassword').val().trim();
//      if (newemailpassword=="")
//        data.errors.newemailpassword=ADMINMSG_FIELDREQD;
      var newemailserver=$('#newemailserver').val().trim();
      if (newemailserver=="")
        data.errors.newemailserver=ADMINMSG_FIELDREQD;
      var newemailport=$('#newemailport').val().trim();
      if (newemailport=="")
        data.errors.newemailport=ADMINMSG_FIELDREQD;
    }
    if (Object.keys(data.errors).length>0)
    {
      data.success=false;
      data.message=ADMINMSG_FORMERROR;
      //data=JSON.stringify(data);
      // Show validation results from JS in form
      showValidation(data,'result'+formtype);
      // stop the form from submitting the normal way and refreshing the page
      event.preventDefault();
      return;
    }          
    // Now call PHP for final validation and processing
    var label=showButtonBusy('submit'+formtype);
    var formData = $('#'+formtype+'form').serialize();
    formData=formData+"&slcsrf="+slcsrf+"&settingtype="+formtype;
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : 'admineditconfig.php', // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
    .done(function(data) {
      sessionExpiredReported(data,true);
      // Show validation results from PHP in form
      showValidation(data,'result'+formtype);
      hideButtonBusy('submit'+formtype,label);
    })
    .fail(function(data) {
    hideButtonBusy('submit'+formtype,label);  
    $('#result'+formtype).html('<div id="result'+formtype+'message" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
    });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});

// Install path settings form handler
$('#pathsform').submit(function(event) {
    var formtype="paths";
    // remove the error classes
    $('#newsiteloklocationdiv').removeClass('has-error');
    $('#newsiteloklocationurldiv').removeClass('has-error');
    $('#newemaillocationdiv').removeClass('has-error');
    $('#newemailurldiv').removeClass('has-error');
    $('#newbackuplocationdiv').removeClass('has-error');
     // remove the error messages
    $('#newsiteloklocationhelp').remove();
    $('#newsiteloklocationurlhelp').remove();
    $('#newemaillocationhelp').remove();
    $('#newemailurlhelp').remove();
    $('#newbackuplocationhelp').remove();
    //Remove form message
    $('#result'+formtype).html('');
    // Validate using JS first
    var data={success:true,message:'',errors:{}};
    var newsiteloklocation=$('#newsiteloklocation').val().trim();
    if (newsiteloklocation=="")
      data.errors.newsiteloklocation=ADMINCF_INSTPTHS1PR1;
    var newsiteloklocationurl=$('#newsiteloklocationurl').val().trim();
    if (newsiteloklocationurl=="")
      data.errors.newsiteloklocationurl=ADMINCF_INSTPTHS2PR1;
    var newemaillocation=$('#newemaillocation').val().trim();
    if (newemaillocation=="")
      data.errors.newemaillocation=ADMINCF_INSTPTHS3PR1;
    var newemailurl=$('#newemailurl').val().trim();
    if (newemailurl=="")
      data.errors.newemailurl=ADMINCF_INSTPTHS4PR1;
    var newbackuplocation=$('#newbackuplocation').val().trim();
    if (newbackuplocation=="")
      data.errors.newbackuplocation=ADMINCF_INSTPTHS5PR1;
    if (Object.keys(data.errors).length>0)
    {
      data.success=false;
      data.message=ADMINMSG_FORMERROR;
      //data=JSON.stringify(data);
      // Show validation results from JS in form
      showValidation(data,'result'+formtype);
      // stop the form from submitting the normal way and refreshing the page
      event.preventDefault();
      return;
    }
    // Now call PHP for final validation and processing
    var label=showButtonBusy('submit'+formtype);
    var formData = $('#'+formtype+'form').serialize();
    formData=formData+"&slcsrf="+slcsrf+"&settingtype="+formtype;
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : 'admineditconfig.php', // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
    .done(function(data) {
      sessionExpiredReported(data,true);
      // Show validation results from PHP in form
      showValidation(data,'result'+formtype);
      hideButtonBusy('submit'+formtype,label);
    })
    .fail(function(data) {
    hideButtonBusy('submit'+formtype,label);  
    $('#result'+formtype).html('<div id="result'+formtype+'message" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
    });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});


function updateForm()
{
  if ($('#forgotemailtype').val()=="default")
    $('#forgotemailtemplatediv').hide();
  else   
    $('#forgotemailtemplatediv').show();

  if ($('#newallowsearchengine').val()=="0")
    $('#searchenginegroupdiv').hide();
  else   
    $('#searchenginegroupdiv').show();

  if ($('#newemailconfirmrequired').val()=="0")
    $('#newemailconfirmtemplatediv').hide();
  else   
    $('#newemailconfirmtemplatediv').show();

  if ($('#newemailtype').val()=="0")
    $('#smtpsettings').hide();
  else   
    $('#smtpsettings').show();

}

function selectAll()
{
  var formname='logsettingsform';
  if ($('#selectall').iCheck('update')[0].checked)
    var toggle="check";
  else  
    var toggle="uncheck";
  var checkboxes = new Array(); 
  checkboxes = document[formname].getElementsByTagName('input');
  for (var i=0; i<checkboxes.length; i++) 
  {
    if ((checkboxes[i].type == 'checkbox') && (!checkboxes[i].disabled))
    {
      if (checkboxes[i].id.substr(0,11)=='newlogentry')
      {
        $('#'+checkboxes[i].id).iCheck(toggle);
      }
    }
  }
}

function restoreDefaultOrder()
{
  bootbox.confirm(ADMINCF_RSTORDERVER, function(result) {
    if (result)
    {
      $('#newactioncolumncb').prop( "checked", true );
      $('#newactioncolumn').val('1');
      $('#newselectedcolumncb').prop( "checked", true );
      $('#newselectedcolumn').val('2');
      $('#newcreatedcolumncb').prop( "checked", true );
      $('#newcreatedcolumn').val('3');
      $('#newusernamecolumncb').prop( "checked", true );
      $('#newusernamecolumn').val('4');
      $('#newpasswordcolumncb').prop( "checked", false );
      $('#newpasswordcolumn').val('');
      $('#newenabledcolumncb').prop( "checked", true );
      $('#newenabledcolumn').val('5');
      $('#newnamecolumncb').prop( "checked", true );
      $('#newnamecolumn').val('6');
      $('#newemailcolumncb').prop( "checked", true );
      $('#newemailcolumn').val('7');
      $('#newusergroupscolumncb').prop( "checked", true );
      $('#newusergroupscolumn').val('8');
      for (k=1;k<=10;k++)
      {
        $('#newcustom'+k+'columncb').prop( "checked", true );
        $('#newcustom'+k+'column').val(k+8);    
      }
      for (k=11;k<=560;k++)
      {
        $('#newcustom'+k+'columncb').prop( "checked", false );
        $('#newcustom'+k+'column').val('');    
      }
      $('#newuseridcolumncb').prop( "checked", false );
      $('#newuseridcolumn').val('');
    }
  });  
}

function testEmail()
{
    var formtype="email";
    // remove the error classes
    $('#newemailreplytodiv').removeClass('has-error');
    $('#newemailusernamediv').removeClass('has-error');
    $('#newemailpassworddiv').removeClass('has-error');
    $('#newemailserverdiv').removeClass('has-error');
    $('#newemailportdiv').removeClass('has-error');
     // remove the error messages
    $('#newemailreplytohelp').remove();
    $('#newemailusernamehelp').remove();
    $('#newemailpasswordhelp').remove();
    $('#newemailserverhelp').remove();
    $('#newemailporthelp').remove();
    //Remove form message
    $('#result'+formtype).html('');
    // Validate using JS first
    var data={success:true,message:'',errors:{}};
    var newemailtype=$('#newemailtype').val();
    var newemailreplyto=$( "#newemailreplyto" ).val().trim();
    if ((newemailreplyto!="") && (!validateEmail(newemailreplyto)))    
      data.errors.newemailreplyto=ADMINMSG_EMAILINVALID;
    // check SMTP settings entered for PHPmailer
    if (newemailtype==1)
    {
      var newemailusername=$('#newemailusername').val().trim();
//      if (newemailusername=="")
//        data.errors.newemailusername=ADMINMSG_FIELDREQD;
      var newemailpassword=$('#newemailpassword').val().trim();
//      if (newemailpassword=="")
//        data.errors.newemailpassword=ADMINMSG_FIELDREQD;
      var newemailserver=$('#newemailserver').val().trim();
      if (newemailserver=="")
        data.errors.newemailserver=ADMINMSG_FIELDREQD;
      var newemailport=$('#newemailport').val().trim();
      if (newemailport=="")
        data.errors.newemailport=ADMINMSG_FIELDREQD;
    }
    if (Object.keys(data.errors).length>0)
    {
      data.success=false;
      data.message=ADMINMSG_FORMERROR;
      //data=JSON.stringify(data);
      // Show validation results from JS in form
      showValidation(data,'result'+formtype);
      return;
    }          
    // Now call PHP for final validation and processing
    var label=showButtonBusy('testemail');
    var formData = $('#'+formtype+'form').serialize();
    formData=formData+"&slcsrf="+slcsrf;
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : 'adminsendtestemail.php', // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
    .done(function(data) {
      sessionExpiredReported(data,true);
      // Show validation results from PHP in form
      showValidation(data,'result'+formtype);
      hideButtonBusy('testemail',label);
    })
    .fail(function(data) {
    hideButtonBusy('testemail',label);  
    $('#result'+formtype).html('<div id="result'+formtype+'message" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
    });
}

function addLocation()
{
  var code=$('#extraLocationTemplate').html();
  code=code.replace(/XX/g, locationcount);
  code=code.replace(/<!--/g, '');
  code=code.replace(/-->/g, '');
  $('#extraLocationInsert').before(code);
  locationcount++;
}

function removeLocation(num)
{
  bootbox.confirm(ADMINCF_DELLOC, function(result) {
    if (result)
    {
      $('#locationblock'+num).remove();
    }
  });      
}

function openFileManager(field)
{
    $.fancybox({
        'type'  : 'iframe',
        'width' : '90%',
        'height' : '90%',
        'href' : 'filemanager/dialog.php?type=2&field_id='+field+'&lang='+ADMINRFM_LANG+'&sort_by=name&descending=0'
      });
}

function responsive_filemanager_callback(field_id)
{
  if (-1!=EmailURL.indexOf('://www.'))
  {
    var emailurl1=EmailURL.replace('://www.','://');
    var emailurl2=EmailURL;
  }
  else
  {
    var emailurl1=EmailURL;
    var emailurl2=EmailURL.replace('://','://www.');
  }
  var emailurl3=EmailURL;
  emailurl3=EmailURL.replace('http://','https://');
  var emailurl4=EmailURL;
  emailurl4=EmailURL.replace('https://','http://');
  var url=$('#'+field_id).val();
  url=url.replace(emailurl1,'');
  url=url.replace(emailurl2,'');
  url=url.replace(emailurl3,'');
  url=url.replace(emailurl4,'');
  url=decodeURIComponent(url);
  $('#'+field_id).val(url);
  $('#'+field_id+'text').text(url);
}
