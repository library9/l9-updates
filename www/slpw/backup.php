<?php
  $groupswithaccess="ADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
?>
<!DOCTYPE html>
<html>
<head>
<?php
$pagename="backup";
include("adminhead.php");
?>
<title><?php echo ADMINMENU_BACKUP; ?></title>
<link rel="stylesheet" href="backup.css"></head>
<?php include("adminthemeheader.php"); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header">
          <h1>
            <i class="fa fa-life-bouy"></i>&nbsp;<?php echo ADMINMENU_BACKUP; ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><?php echo ADMINMENU_BACKUP; ?></li>
          </ol>
        </section>



        <!-- Main content -->
        <section class="content">

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>

          <!-- Your Page Content Here -->





<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo ADMINBACKUP_FILES; ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form id="backupform" role="form" class="form-horizontal" method="post">

          <div class="form-group">
            <div class="col-sm-10" id="backupcompressdiv">
              <div class="checkbox">
                <label>
                <?php if (function_exists("gzopen")) { ?>
                  <input type="checkbox" id="backupcompress" value="1" checked>&nbsp;&nbsp;<?php echo ADMINBACKUP_COMPRESS; ?>
                <?php } else { ?>
                  <input type="checkbox" id="backupcompress" value="1" disabled ><span style="color: #C0C0C0;">&nbsp;&nbsp;<?php echo ADMINBACKUP_COMPRESS; ?></span>                
                <?php } ?>  
                </label>
              </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-10" id="backupmigratediv">
              <div class="checkbox">
                <label>
                  <input type="checkbox" id="backupmigrate" value="1" >&nbsp;&nbsp;<?php echo ADMINBACKUP_MIGRATE; ?>
                </label>
              </div>
            </div>
        </div>

        <p>&nbsp;</p>

        <div class="form-group">
        <div class="col-sm-11">
            <div class="btn-toolbar">
                <button type="submit" id="backup" class="btn btn-primary pull-left"><?php echo ADMINBUTTON_BACKUP; ?></button>
                <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
            </div>   
        </div>
        </div>
        <p>&nbsp;</p>

        <div id="filelist" class="box box-default">
        </div>
        </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo ADMINBACKUP_ABOUT; ?></h3>
          <span class="label label-primary pull-right"><i class="fa fa-info"></i></span>
        </div><!-- /.box-header -->
        <div class="box-body">

          <p>When you click the Create Backup button Sitelok will generate a backup of all Sitelok data in a standard .sql file and compress it in .gz format if required.<p>
          <p>The backup file can be used by most Mysql administrator control panels (like phpmyadmin) to fully restore the Sitelok database. If you wish use the backup to migrate Sitelok to a new server then check the 'Generate data for migration only' checkbox. This will still backup all user data but will not restore server specific paths etc on the new server.</p>


          <p>As backup files can become very large we strongly recommend that after download you verify that it completed successfully. To do this open the file in a text editor (you can rename it with .txt) and check that the last line contains</p>
          <p><code># End of backup file</code></p>
          <p>For large files we recommend downloading using an FTP client.</p>  
          <p>Backup files are stored in the following folder which can be changed in the configuration page.</p>
          <p><code><?php if ($DemoMode) echo "Hidden in demo mode"; else echo $BackupLocation; ?></code></p>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->

 
</div><!-- /.row -->

<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<!-- Backp process modal-->
  <div class="modal fade" id="backupModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"> 
              <h4 class="modal-title"><?php echo ADMINMENU_BACKUP; ?></h4>
            </div>
            <div class="modal-body">
              <div id="backupwarning"></div>
              <div class="progress" id="backupprogressdiv">
                <div id="backupprogress" class="progress-bar progress-bar-striped" role="progressbar">
                  0%
                </div>
              </div>
              <div id="statustext"><?php echo ADMINBACKUP_CREATING; ?></div>

            </div>  
            <div class="modal-footer">
              <button type="button" class="btn btn-default" id="backup_cancel" onclick="cancelClicked();"><?php echo ADMINBUTTON_CANCEL; ?></button>
              <button type="button" class="btn btn-primary" id="backup_close" onclick="closeClicked();"><?php echo ADMINBUTTON_CLOSE; ?></button>
            </div>
        </div>
      </div>
  </div>



<?php include("adminthemefooter.php"); ?>
    <script src="backup.js"></script>

  </body>
</html>
