<?php
  $groupswithaccess="ADMIN,SUBADMIN";
  $noaccesspage="";
  require("sitelokpw.php");
  if (!isset($adminlanguage))
    $adminlanguage="en";
  require("adminlanguage-".$adminlanguage.".php");
  $slsubadmin=false;
  if ((!sl_isactivememberof("ADMIN")) && (!sl_isactivememberof("DEMOADMIN")))
    $slsubadmin=true;
  require_once("adminprocesslog.php");
  $logmanageact=$_POST['logmanageact'];
  if ($logmanageact=="")
  {
    $timeoffset=$LogViewOffset;
    $order=$LogViewOrder;
    // Get log entry settings
    for ($k=1;$k<=50;$k++)
    {
      $var="logentry".$k;
      if (substr($LogViewDetails,$k-1,1)!="N")
        $$var="on";
    }
    // Set initial start date to 7 days ago
    if ($DateFormat=="DDMMYY")
      $fromdate=date('dmy', strtotime('-7 days'));
    else
      $fromdate=date('mdy', strtotime('-7 days'));
  }

  if (($logmanageact=="export") && ($_POST['slcsrf']==$_SESSION['ses_slcsrf']))
  {
    $mysql_link=sl_DBconnect();
    if ($mysql_link==false)
    {
      die(ADMINMSG_MYSQLERROR);
      exit;
    }
    slProcessLog($logmanageact,$mysql_link);
    exit;
  }
  if (($logmanageact=="view") && ($_POST['slcsrf']==$_SESSION['ses_slcsrf']))
  {
    $mysql_link=sl_DBconnect();
    if ($mysql_link==false)
    {
      die(ADMINMSG_MYSQLERROR);
      exit;
    }
    slProcessLog($logmanageact,$mysql_link);
    exit;
  }  
?>
<!DOCTYPE html>
<html>
<head>
<?php
$pagename="logmanage";
include("adminhead.php");
?>
<title><?php echo ADMINMENU_LOG; ?></title>
<link rel="stylesheet" href="logmanage.css"></head>
<?php include("adminthemeheader.php"); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-align-justify"></i> <?php echo ADMINMENU_LOG; ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo ADMINMENU_DASHBOARD; ?></a></li>
            <li class="active"><?php echo ADMINMENU_LOG; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

<?php
// Load pluginadmincontenttop.php for plugins if required
if(isset($slplugin_admincontenttop))
{
  asort($slplugin_admincontenttop);
  foreach ($slplugin_admincontenttop as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontenttop.php");
  } 
}
?>
          <div class="row">

          <form name="logform" id="logform" role="form" class="form-horizontal" action="logmanage.php" target="" method="POST">
          <input name="logmanageact" id="logmanageact" type="hidden" value="">
          <input name="browsertimeoffset" id="browsertimeoffset" type="hidden" value="">
          <input name="slcsrf" id="slcsrf" type="hidden" value="<?php echo $slcsrftoken; ?>">

            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo ADMINLG_DTRANGE; ?></h3>
                </div>
                <div class="box-body">

                  <div class="form-group">
                    <label class="col-xs-12" for="em"><?php echo $DateFormat.ADMINLG_LVBLANK; ?></label>
                    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2" id="fromdatediv">
                      <div class="input-group datepicker" id="fromdatecal">
                         <input type="text" placeholder="<?php echo "from"; ?>" class="form-control" id="fromdate" name="fromdate" value="<?php echo $fromdate; ?>" maxlength="6" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                         <div class="input-group-addon"><span class="glyphicon glyphicon-calendar actionicon"></span></div>
                      </div>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2" id="todatediv">
                      <div class="input-group datepicker" id="todatecal">
                         <input type="text" placeholder="<?php echo "to"; ?>" class="form-control" id="todate" name="todate" value="<?php echo $todate; ?>" maxlength="6" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                         <div class="input-group-addon"><span class="glyphicon glyphicon-calendar actionicon"></span></div>
                      </div>   
                    </div>

                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo ADMINGENERAL_USERS; ?></h3>
                </div>
                <div class="box-body">

                  <div class="form-group">
                      <label class="col-xs-12" for="usernm"><?php echo ADMINFIELD_USERNAME; ?></label>
                      <div class="col-xs-12 col-sm-7 col-md-5 col-lg-4" id="usernmdiv">
                          <input type="text" class="form-control" name="usernm" id="usernm" maxlength="100" value="" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                          <p class="help-block"><?php echo ADMINLG_ALLUSERS; ?></p>
                      </div>
                  </div>                

                  <div class="form-group">
                      <div class="col-sm-10" id="selectedonly">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="selectedonly" id="selectedonly" value="on">&nbsp;&nbsp;<?php echo ADMINLG_SELUSERS; ?>
                          </label>
                        </div>
                      </div>
                  </div>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo ADMINLG_LOGENT; ?></h3>
                </div>
                <div class="box-body">

                <div id="logentrydiv">

                  <div class="form-group">
                    <div class="col-xs-12">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-control" name="selectall" id="selectall" value="1">&nbsp;&nbsp;<?php echo ADMINBUTTON_SELECTALL; ?>
                        </label>
                      </div>
                    </div>
                  </div>    

                  <div class="form-group">

                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="logentry1" id="logentry1" value="on" <?php if ($logentry1=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP1; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="logentry2" id="logentry2" value="on" <?php if ($logentry2=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP2; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="logentry3" id="logentry3" value="on" <?php if ($logentry3=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP3; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="logentry4" id="logentry4" value="on" <?php if ($logentry4=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP4; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="logentry5" id="logentry5" value="on" <?php if ($logentry5=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP5; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="logentry6" id="logentry6" value="on" <?php if ($logentry6=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP6; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="logentry10" id="logentry10" value="on" <?php if ($logentry10=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP10; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="logentry7" id="logentry7" value="on" <?php if ($logentry7=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP7; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="logentry8" id="logentry8" value="on" <?php if ($logentry8=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP8; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="logentry9" id="logentry9" value="on" <?php if ($logentry9=="on") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_LOGTYP9; ?>
                          </label>
                        </div>
                      </div>

<?php
for ($k=11;$k<=50;$k++)
{
  $var="logentry".$k;
  if ($slplugin_logentryname[$k]!="")
  {
?>

                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="form-control" name="<?php echo $var; ?>" id="<?php echo $var; ?>" value="on" <?php if ($$var=="on") print "checked=\"checked \""; ?>>&nbsp;&nbsp;<?php echo $slplugin_logentryname[$k]; ?>
                          </label>
                        </div>
                      </div>

<?php
  }
  else
  {
?>
<input type="hidden" name="<?php echo $var; ?>" id="<?php echo $var; ?>" value="on">
<?php
  }  
}
?>

                  </div>

                  </div> <!-- logentrydiv -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo ADMINLG_LOGOUTPUT; ?></h3>
                </div>
                <div class="box-body">

                  <div class="form-group">
                    <label class="col-xs-12" for="logsortfield"><?php echo ADMINLG_SORTBY; ?></label>
                     <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3" id="logsortfielddiv">
                        <select id="logsortfield" name="logsortfield" class="form-control selectpicker">
                        <option value="id" ><?php echo ADMINGENERAL_DATE; ?></option>
                        <option value="username"><?php echo ADMINFIELD_USERNAME; ?></option>
                        </select>  
                    </div>
                  </div> 

                  <div class="form-group">
                    <label class="col-xs-12" for="maxentries"><?php echo ADMINLG_LIMIT; ?></label>
                     <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3" id="maxentriesdiv">
                        <select id="maxentries" name="maxentries" class="form-control selectpicker">
                        <option value="0"><?php echo ADMINLG_ALLENTRIES; ?></option>
                        <option value="1">1 <?php echo ADMINLG_ENTRIES;?></option>
                        <option value="3">3 <?php echo ADMINLG_ENTRIES;?></option>
                        <option value="10">10 <?php echo ADMINLG_ENTRIES;?></option>
                        <option value="50">50 <?php echo ADMINLG_ENTRIES;?></option>
                        <option value="100">100 <?php echo ADMINLG_ENTRIES;?></option>
                        <option value="250">250 <?php echo ADMINLG_ENTRIES;?></option>
                        <option value="500">500 <?php echo ADMINLG_ENTRIES;?></option>
                        <option value="1000">1000 <?php echo ADMINLG_ENTRIES;?></option>
                        <option value="3000">3000 <?php echo ADMINLG_ENTRIES;?></option>
                        <option value="10000">10000 <?php echo ADMINLG_ENTRIES;?></option>
                        </select>  
                    </div>
                  </div> 

                  <div class="form-group">
                    <label class="col-xs-12" for="timeoffset"><?php echo ADMINLG_TZ; ?></label>
                     <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3" id="timeoffsetdiv">
                        <select id="timeoffset" name="timeoffset" class="form-control selectpicker">
                        <option value="0" <?php if ($timeoffset==0) print "selected=\"selected\""; ?>><?php echo ADMINGENERAL_UTCGMT; ?></option>
                        <option value="9999" <?php if ($timeoffset==9999) print "selected=\"selected\""; ?>><?php echo ADMINLG_LOCALTZ; ?></option>
                        <option value="-720" <?php if ($timeoffset==-720) print "selected=\"selected\""; ?>>-12 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-660" <?php if ($timeoffset==-660) print "selected=\"selected\""; ?>>-11 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-600" <?php if ($timeoffset==-600) print "selected=\"selected\""; ?>>-10 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-570" <?php if ($timeoffset==-570) print "selected=\"selected\""; ?>>-9.5 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-540" <?php if ($timeoffset==-540) print "selected=\"selected\""; ?>>-9 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-510" <?php if ($timeoffset==-510) print "selected=\"selected\""; ?>>-8.5 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-480" <?php if ($timeoffset==-480) print "selected=\"selected\""; ?>>-8 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-420" <?php if ($timeoffset==-420) print "selected=\"selected\""; ?>>-7 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-360" <?php if ($timeoffset==360)  print "selected=\"selected\""; ?>>-6 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-300" <?php if ($timeoffset==-300) print "selected=\"selected\""; ?>>-5 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-240" <?php if ($timeoffset==-240) print "selected=\"selected\""; ?>>-4 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-210" <?php if ($timeoffset==-210) print "selected=\"selected\""; ?>>-3.5 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-180" <?php if ($timeoffset==-180) print "selected=\"selected\""; ?>>-3 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-120" <?php if ($timeoffset==-120) print "selected=\"selected\""; ?>>-2 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="-60" <?php if ($timeoffset==-60) print "selected=\"selected\""; ?>>-1 hour</option>
                        <option value="60" <?php if ($timeoffset==60) print "selected=\"selected\""; ?>>+1 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="120" <?php if ($timeoffset==120) print "selected=\"selected\""; ?>>+2 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="180" <?php if ($timeoffset==180) print "selected=\"selected\""; ?>>+3 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="210" <?php if ($timeoffset==210) print "selected=\"selected\""; ?>>+3.5 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="240" <?php if ($timeoffset==240) print "selected=\"selected\""; ?>>+4 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="270" <?php if ($timeoffset==270) print "selected=\"selected\""; ?>>+4.5 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="300" <?php if ($timeoffset==300) print "selected=\"selected\""; ?>>5 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="330" <?php if ($timeoffset==330) print "selected=\"selected\""; ?>>+5.5 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="360" <?php if ($timeoffset==360) print "selected=\"selected\""; ?>>+6 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="390" <?php if ($timeoffset==390) print "selected=\"selected\""; ?>>+6.5 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="420" <?php if ($timeoffset==420) print "selected=\"selected\""; ?>>+7 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="480" <?php if ($timeoffset==480) print "selected=\"selected\""; ?>>+8 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="540" <?php if ($timeoffset==540) print "selected=\"selected\""; ?>>+9 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="570" <?php if ($timeoffset==570) print "selected=\"selected\""; ?>>+9.5 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="600" <?php if ($timeoffset==600) print "selected=\"selected\""; ?>>+10 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="630" <?php if ($timeoffset==630) print "selected=\"selected\""; ?>>+10.5 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="660" <?php if ($timeoffset==660) print "selected=\"selected\""; ?>>+11 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="690" <?php if ($timeoffset==690) print "selected=\"selected\""; ?>>+11.5 <?php echo ADMINGENERAL_HOURS; ?></option>
                        <option value="720" <?php if ($timeoffset==720) print "selected=\"selected\""; ?>>+12 <?php echo ADMINGENERAL_HOURS; ?></option>
                        </select>  
                    </div>
                  </div> 

                  <div class="form-group">
                      <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <div class="checkbox">
                          <label>
                            <input type="radio" class="form-control" name="order" id="orderdesc" value="DESC" <?php if ($order=="DESC") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_NEWEST; ?>
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <div class="checkbox">
                          <label>
                            <input type="radio" class="form-control" name="order" id="orderasc" value="ASC" <?php if ($order=="ASC") echo "checked "; ?>>&nbsp;&nbsp;<?php echo ADMINLG_OLDEST; ?>
                          </label>
                        </div>
                      </div>
                  </div>  


                </div><!-- /.box-body -->
              </div><!-- /.box -->


               <div class="form-group">
                <div class="col-xs-12">
                    <div class="btn-toolbar">
                        <button type="button" id="view" class="btn btn-primary pull-left" onclick="viewLog();"><?php echo ADMINLG_VIEW; ?></button>
                        <button type="button" id="export" class="btn btn-primary pull-left" onclick="exportLog();"><?php echo ADMINLG_EXPORT; ?></button>
                        <button type="button" id="delete" class="btn btn-primary pull-left" onclick="clearLog();"><?php echo ADMINLG_DELETE; ?></button>
                        <button type="button" id="cancel" class="btn btn-default pull-left" onclick="window.location.href='index.php';"><?php echo ADMINBUTTON_RETURNDASHBOARD ?></button>
                    </div>   
                </div>
              </div>

            <div id="resultlog"></div>



            </div><!-- /.col -->




            </form>

          </div><!-- /.row -->

<?php
// Load pluginadmincontentbottom.php for plugins if required
if(isset($slplugin_admincontentbottom))
{
  asort($slplugin_admincontentbottom);
  foreach ($slplugin_admincontentbottom as $key => $value)
  {
    if ($value>0)
      include($SitelokLocation.$slpluginfolder[$key]."/pluginadmincontentbottom.php");
  } 
}
?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("adminthemefooter.php"); ?>
    <script src="logmanage.js"></script>

  </body>
</html>
