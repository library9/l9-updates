<?php
function sl_registerformhead($id,$simplestyle)
{
  $res=sl_get_register_formcode(false,$id,false,$simplestyle,$css,$js,$html);
  if ($res)
    echo $css."\n".$js;  
}
function sl_registerformbody($id,$simplestyle)
{
  $res=sl_get_register_formcode(false,$id,false,$simplestyle,$css,$js,$html);
  if ($res)
  {
    echo $html;
  }  
  else
  {
    echo "<strong>Registration form not found</strong>";
  }    
}
function sl_get_register_formcode($mysql_link,$id,$fullsource,$simplestyle,&$css,&$js,&$html)
{
global $DbTableName;
global $registermsg,$SitelokLocationURL,$SitelokLocationURLPath,$username,$password,$verifypassword,$name,$email,$verifyemail,$usergroup;
global $custom1,$custom2,$custom3,$custom4,$custom5,$custom6,$custom7,$custom8,$custom9,$custom10;
global $custom11,$custom12,$custom13,$custom14,$custom15,$custom16,$custom17,$custom18,$custom19,$custom20;
global $custom21,$custom22,$custom23,$custom24,$custom25,$custom26,$custom27,$custom28,$custom29,$custom30;
global $custom31,$custom32,$custom33,$custom34,$custom35,$custom36,$custom37,$custom38,$custom39,$custom40;
global $custom41,$custom42,$custom43,$custom44,$custom45,$custom46,$custom47,$custom48,$custom49,$custom50;
global $regname,$regemail,$regusername,$regpassword,$sl_ajaxforms;
if ($mysql_link===false)
{
  $mysql_link=sl_DBconnect();
  if ($mysql_link==false)
    return(false);
}
// If form is not register type then return
$mysql_result=mysqli_query($mysql_link,"SELECT type FROM sl_forms WHERE id=".sl_quote_smart($id));
if ($mysql_result===false)
  return(false);
$row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
if ($row===false)
  return(false);
if ($row['type']!="register")
  return(false);
// Get custom field lengths to see if text area fields can be used
$customfieldlengths=array();
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM ".$DbTableName." LIMIT 1");
$fcharset=mysqli_get_charset($mysql_link);
$finfo=mysqli_fetch_fields($mysql_result);
for($k=1;$k<=50;$k++)
  $customfieldlengths[$k]=($finfo[$k+7]->length)/$fcharset->max_length;
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_registerforms WHERE id=".sl_quote_smart($id)." AND position=0");
if ($mysql_result===false)
  return(false);
$row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC);
$usergroup=$row['usergroup'];
$expiry=$row['expiry'];
$redirect=$row['redirect'];
$useremail=$row['useremail'];
$adminemail=$row['adminemail'];
$enabled=$row['enabled'];
$fonttype=$row['fonttype'];
$labelcolor=$row['labelcolor'];
$labelsize=$row['labelsize'];
$labelstyle=$row['labelstyle'];
$inputtextcolor=$row['inputtextcolor'];
$inputtextsize=$row['inputtextsize'];
$inputtextstyle=$row['inputtextstyle'];
$inputbackcolor=$row['inputbackcolor'];
$bordersize=$row['bordersize'];
$bordercolor=$row['bordercolor'];
$borderradius=$row['borderradius'];
$inputpaddingv=$row['inputpaddingv'];
$inputpaddingh=$row['inputpaddingh'];
$rqdfieldlabel=$row['rqdfieldlabel'];
$rqdfieldcolor=$row['rqdfieldcolor'];
$rqdfieldsize=$row['rqdfieldsize'];
$rqdfieldstyle=$row['rqdfieldstyle'];
$messagecolor=$row['messagecolor'];
$messagesize=$row['messagesize'];
$messagestyle=$row['messagestyle'];
$btnlabel=$row['btnlabel'];
$btnlabelfont=$row['btnlabelfont'];
$btnlabelstyle=$row['btnlabelstyle'];
$btnlabelcolor=$row['btnlabelcolor'];
$btnlabelsize=$row['btnlabelsize'];
$btncolortype=$row['btncolortype'];
$btncolorfrom=$row['btncolorfrom'];
$btncolorto=$row['btncolorto'];
$btnborderstyle=$row['btnborderstyle'];
$btnbordercolor=$row['btnbordercolor'];
$btnbordersize=$row['btnbordersize'];
$btnradius=$row['btnradius'];
$btnpaddingv=$row['btnpaddingv'];
$btnpaddingh=$row['btnpaddingh'];
$formerrormsg=$row['formerrormsg'];
$formerrormsgcolor=$row['formerrormsgcolor'];
$formerrormsgsize=$row['formerrormsgsize'];
$formerrormsgstyle=$row['formerrormsgstyle'];
$backcolor=$row['backcolor'];
$maxformwidth=$row['maxformwidth'];

$border='solid';
if ($bordersize==0)
  $border='none';
$border.=' #'.$bordercolor.' '.$bordersize.'px';
if ($btncolortype=="solid")
  $btncolorto=$btncolorfrom;
if ($simplestyle)
  $captchaheight=30;
else
{
  if ($inputpaddingv=="0.3em")
    $captchaheight=($inputtextsize*1.75)+($bordersize*2);
  else
  {
    $inputpaddingvnum=str_replace('px','',$inputpaddingv);
    $captchaheight=($inputtextsize*1.2)+($inputpaddingvnum*2)+($bordersize*2);        
  }
  $captchaheight=round($captchaheight,2);
  if ($captchaheight<30) 
    $captchaheight=30;
}
$captchawidth=4;
if ($inputpaddingh!="0.3em")
{  
  $inputpaddinghnum=str_replace('px','',$inputpaddingh);
  $captchawidth=3+((2*$inputpaddinghnum)/$inputtextsize);
  $captchawidth=round($captchawidth,2);
  if ($captchawidth<4) 
    $captchawidth=4;
}
$textareaheight=5;
if ($inputpaddingv!="0.3em")
{  
  $inputpaddingvnum=str_replace('px','',$inputpaddingv);
  $textareaheight=5+((2*$inputpaddingvnum)/$inputtextsize);
  $textareaheight=round($textareaheight,2);
} 
$fileinputsize=$inputtextsize;
if ($inputtextsize>17)
  $fileinputsize=17;
// Get field details
$mysql_result=mysqli_query($mysql_link,"SELECT * FROM sl_registerforms WHERE id=".sl_quote_smart($id)." AND position>0 ORDER BY position ASC");
if ($mysql_result===false)
  return(false);
$numfields=0;
while ($row=mysqli_fetch_array($mysql_result,MYSQLI_ASSOC))
{ 
  $sitelokfield_array[$numfields]=$row['sitelokfield'];
  $inputtype_array[$numfields]=$row['inputtype'];
  $labeltext_array[$numfields]=$row['labeltext'];
  $placetext_array[$numfields]=$row['placetext'];
  $value_array[$numfields]=$row['value'];
  $checked_array[$numfields]=$row['checked'];
  $validation_array[$numfields]=$row['validation'];
  $showrequired_array[$numfields]=$row['showrequired'];
  $errormsg_array[$numfields]=$row['errormsg'];
  $fieldwidth_array[$numfields]=$row['fieldwidth'];
  $bottommargin_array[$numfields]=$row['bottommargin'];
  $numfields++;
}

// Adjust settings for spinner
if ($simplestyle)
  $spinnerposition="form";
else
  $spinnerposition="button";

if ($spinnerposition=="button")
{
  if ($btnlabelsize>=14)
  {
    $spinnermargintop=intval((($btnlabelsize*1.16)-16)/2);
    $spinnersize=12;
  }
  else
  {
    $spinnermargintop=0;
    $spinnersize=intval($btnlabelsize/1.4);
  }
  $spinnermarginright=-19;
  $spinnercolorR=hexdec(substr($btnlabelcolor,0,2));
  $spinnercolorG=hexdec(substr($btnlabelcolor,2,2));
  $spinnercolorB=hexdec(substr($btnlabelcolor,4,2));
}
if ($spinnerposition=="form")
{
  $spinnersize=$btnlabelsize;
  $spinnercolorR=0;
  $spinnercolorG=179;
  $spinnercolorB=234;
  $spinnermargintop=0;
  $spinnermarginright=0;
}

if (!$simplestyle)
{
$css = <<<EOT
<style type="text/css">

div#slform_{$id}{
  border: none;
  padding: 0;
  margin: 0;
  width: 100%;
  max-width: {$maxformwidth}px;
}

div#slform_{$id} label em{
  color: #{$rqdfieldcolor};
  font: {$rqdfieldstyle} {$rqdfieldsize}px {$fonttype};
  margin: 0 0 0 2px;
  padding:0;
  vertical-align: top;
  text-transform: none;
}

/* Text fields */
div.sltextfield_{$id} {
}

div#slform_{$id} div.sltextfield_{$id} label {
  display: block;
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding:0;
  text-transform: none;
}

div#slform_{$id} div.sltextfield_{$id} input[type="text"], input[type="email"], input[type="password"] {
	border: {$border};
	background-image: none;
	background-repeat: no-repeat;
	background-color:#{$inputbackcolor};
	font: {$inputtextstyle} {$inputtextsize}px {$fonttype};
	color: #{$inputtextcolor};
  margin: 0 0 0 0px;
	width:100%;
	-moz-border-radius: {$borderradius}px;
	-khtml-border-radius: {$borderradius}px;
	border-radius: {$borderradius}px;	
  padding: {$inputpaddingv} {$inputpaddingh};
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

/* Radio fields */
div.slradiofield_{$id} {
}

div#slform_{$id} div.slradiofield_{$id} label {  
  display: inline;
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
	vertical-align: middle;
	margin: 0;
	padding: 0;
  text-transform: none;	
}

div#slform_{$id} div.slradiofield_{$id} input[type="radio"] {
	margin: 0;
	padding: 0;
	background-image: none;
	background-repeat: no-repeat;
	color: #{$inputtextcolor};
  font: {$labelstyle} {$labelsize}px {$fonttype};
	vertical-align: middle;
}

/* Checkbox fields */
div.slcbfield_{$id} {
}

div#slform_{$id} div.slcbfield_{$id} label {  
  display: inline;
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
	vertical-align: middle;
	margin: 0;
	padding:0;
  text-transform: none;  
}

div#slform_{$id} div.slcbfield_{$id} input[type="checkbox"] {
	margin-left: 0;
	background-image: none;
	background-repeat: no-repeat;
	color: #{$inputtextcolor};
  font: {$labelstyle} {$labelsize}px {$fonttype};
	vertical-align: middle;
	margin: 0;
	padding: 0;
}

/* Select fields */
div.slselectfield_{$id} {
}

div#slform_{$id} div.slselectfield_{$id} label {
  display: block;  
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding: 0;
  text-transform: none;  
}

div#slform_{$id} div.slselectfield_{$id} select {
	border: {$border};
	background-image: none;
	background-repeat: no-repeat;
	background-color: #{$inputbackcolor};
	font: {$inputtextstyle} {$inputtextsize}px {$fonttype};
	color: #{$inputtextcolor};
	width: 100%;
  -moz-border-radius: {$borderradius}px;
	-khtml-border-radius:{$borderradius}px;
  border-radius: {$borderradius}px;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  height: 1.8em;
}

/* Simple label_{$id} fields */
div.sllabelfield {
}

div#slform_{$id} div.sllabelfield_{$id} label {
  display: block;
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding: 0;
  text-transform: none;  	
}

/* Captcha fields */
div.slcaptchafield_{$id} {
}

div#slform_{$id} div.slcaptchafield_{$id} input[type="text"]{
	border: {$border};
	background-image: none;
	background-repeat: no-repeat;
	background-color:#{$inputbackcolor};
	font: {$inputtextstyle} {$inputtextsize}px {$fonttype};
	color: #{$inputtextcolor};
	width: {$captchawidth}em;
	margin: 0;
	padding: {$inputpaddingv} {$inputpaddingh};
	-moz-border-radius: {$borderradius}px;
	-khtml-border-radius: {$borderradius}px;
	border-radius: {$borderradius}px;
  max-width: {$maxformwidth}px;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

div#slform_{$id} div.slcaptchafield_{$id} label {
  display: block;  
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding:0;
  text-transform: none;
}

div#slform_{$id} div.slcaptchafield_{$id} img{
  vertical-align: top;
  height: {$captchaheight}px;
}

/* File fields */
div.slfilefield_{$id} {
  margin: 0;
  padding:0;
}

div#slform_{$id} div.slfilefield_{$id} label {
  display: block;  
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding: 0;	
  text-transform: none;
}

div#slform_{$id} div.slfilefield_{$id} input[type="file"] {
	border: none;
	background-image: none;
	background-repeat: no-repeat;
	background-color:#{$inputbackcolor};
	font: {$inputtextstyle} {$fileinputsize}px {$fonttype};
	color: #{$inputtextcolor};
	margin: 0;
	padding: 2px;
	-moz-border-radius: {$borderradius}px;
	-khtml-border-radius: {$borderradius}px;
	border-radius: {$borderradius}px;
	width: 100%;
  padding: 0.3em;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

/* Textarea fields */
div.sltextareafield_{$id} {
}

div#slform_{$id} div.sltextareafield_{$id} label {
  display: block;
  color: #{$labelcolor}; 
  font: {$labelstyle} {$labelsize}px {$fonttype};
  margin: 0 0 2px 0;
  padding:0;
  text-transform: none;
}

div#slform_{$id} div.sltextareafield_{$id} textarea{
	border: {$border};
	background-image: none;
	background-repeat: no-repeat;
	background-color:#{$inputbackcolor};
	font: {$inputtextstyle} {$inputtextsize}px {$fonttype};
	color: #{$inputtextcolor};
  margin: 0 0 0 0px;
	width:100%;
	height: {$textareaheight}em;
	resize: none;
	-moz-border-radius: {$borderradius}px;
	-khtml-border-radius: {$borderradius}px;
	border-radius: {$borderradius}px;	
  padding: {$inputpaddingv} {$inputpaddingh};
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

div#slform_{$id} div.slmsg_{$id} {  
  padding: 0;
  margin: 1px 0 0 0;
  text-align: left;
  color: #{$messagecolor};
	font: {$messagestyle} {$messagesize}px {$fonttype};
/*
	border: solid 1px white;
*/
}

div#slform_{$id} div.slformmsg_{$id} {
  padding: 0;
  margin: 0 0 10px 0;
  text-align: left;
  color: #{$formerrormsgcolor};
	font: {$formerrormsgstyle} {$formerrormsgsize}px {$fonttype};  
}

/* Button from http://www.bestcssbuttongenerator.com */
div#slform_{$id} #myButton_{$id} {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #{$btncolorfrom}), color-stop(1, #{$btncolorto}));
	background:-moz-linear-gradient(top, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
	background:-webkit-linear-gradient(top, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
	background:-o-linear-gradient(top, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
	background:-ms-linear-gradient(top, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
	background:linear-gradient(to bottom, #{$btncolorfrom} 5%, #{$btncolorto} 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#{$btncolorfrom}', endColorstr='#{$btncolorto}',GradientType=0);
	background-color:#{$btncolorfrom};
	-moz-border-radius: {$btnradius}px;
	-webkit-border-radius: {$btnradius}px;
	border-radius: {$btnradius}px;
	display:inline-block;
	cursor:pointer;
	color: #{$btnlabelcolor};
  font: $btnlabelstyle {$btnlabelsize}px {$btnlabelfont};
	padding: {$btnpaddingv}px {$btnpaddingh}px;
	margin: 0;
	text-decoration:none;
  border: $btnborderstyle #{$btnbordercolor} {$btnbordersize}px;
/*
	text-shadow:0px 1px 0px #ffffff;
*/

}
div#slform_{$id} #myButton_{$id}:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #{$btncolorto}), color-stop(1, #{$btncolorfrom}));
	background:-moz-linear-gradient(top, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
	background:-webkit-linear-gradient(top, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
	background:-o-linear-gradient(top, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
	background:-ms-linear-gradient(top, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
	background:linear-gradient(to bottom, #{$btncolorto} 5%, #{$btncolorfrom} 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#{$btncolorto}', endColorstr='#{$btncolorfrom}',GradientType=0);
	background-color:#{$btncolorto};
}
div#slform_{$id} #myButton_{$id}:active {
	position:relative;
	top:1px;
}
/* End of button */

EOT;

if ($sl_ajaxforms)
{
$css .= <<<EOT
/* busy spinner */
div#slspinner_{$id} {
  display: none;
  box-sizing: content-box;
  float: right;
  height: {$spinnersize}px;
  width: {$spinnersize}px;
  margin-left: 0px;
  margin-right: {$spinnermarginright}px;
  margin-top: {$spinnermargintop}px;
  position: relative;
  border: solid white 1px;
  -webkit-animation: rotation .6s infinite linear;
  -moz-animation: rotation .6s infinite linear;
  -o-animation: rotation .6s infinite linear;
  animation: rotation .6s infinite linear;
  border-left: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-right: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-bottom: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-top: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, 1);
  border-radius: 100%;  
}

@-webkit-keyframes rotation {
  from {
    -webkit-transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(359deg);
  }
}

@-moz-keyframes rotation {
  from {
    -moz-transform: rotate(0deg);
  }
  to {
    -moz-transform: rotate(359deg);
  }
}

@-o-keyframes rotation {
  from {
    -o-transform: rotate(0deg);
  }
  to {
    -o-transform: rotate(359deg);
  }
}

@keyframes rotation {
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(359deg);
  }
}

EOT;
}

$css .="\n/* Field specific styles */\n";
// Get count array for $sitelokfield_array so we can see if field used more than once
$countfield_array=array_count_values($sitelokfield_array);
for ($k=0;$k<$numfields;$k++)
{  
$css .= <<<EOT

#slfielddiv_{$id}_{$k}{
  width: {$fieldwidth_array[$k]}%;
  margin-bottom: {$bottommargin_array[$k]}px;
  padding:0;
  max-width: {$maxformwidth}px;
}

EOT;

}

$css .= <<<EOT
</style>
<!--[if lt IE 9]>
<style type="text/css">
div#slform_{$id} {
  width: {$maxformwidth}px;
}
</style>

<![endif]-->

<!--[if lte IE 7]>
<style type="text/css">
div#slform_{$id} div.sltextfield_{$id} input[type="text"], input[type="email"], input[type="password"] {
  width: 95%;
}
div#slform_{$id} div.sltextareafield_{$id} textarea{
  width: 95%;
}

div#slform_{$id} #myButton_{$id} {
	padding: 0px 0px;
}
</style>
<![endif]-->


EOT;
}
if ($simplestyle)
{
$css = <<<EOT
<style type="text/css">

div#slform_{$id}{
  border: none;
  padding: 0;
  margin: 0;
  width: 100%;
  max-width: {$maxformwidth}px;
}

div#slform_{$id} label em{
  color: #{$rqdfieldcolor};
  margin: 0 0 0 2px;
  padding:0;
  vertical-align: top;
}

/* Text fields */
div.sltextfield_{$id} {
}

div#slform_{$id} div.sltextfield_{$id} label {
  display: block;
  margin: 0 0 2px 0;
  padding:0;
}

div#slform_{$id} div.sltextfield_{$id} input[type="text"], input[type="email"], input[type="password"] {
  margin: 0 0 0 0px;
	width:100%;
  /* box-sizing: border-box; */
}

/* Radio fields */
div.slradiofield_{$id} {
}

div#slform_{$id} div.slradiofield_{$id} label {  
  display: inline;
	vertical-align: middle;
	margin: 0;
	padding: 0;
}

div#slform_{$id} div.slradiofield_{$id} input[type="radio"] {
	margin: 0;
	padding: 0;
	vertical-align: middle;
}

/* Checkbox fields */
div.slcbfield_{$id} {
}

div#slform_{$id} div.slcbfield_{$id} label {  
  display: inline;
	vertical-align: middle;
	margin: 0;
	padding:0;
}

div#slform_{$id} div.slcbfield_{$id} input[type="checkbox"] {
	margin-left: 0;
	vertical-align: middle;
	margin: 0;
	padding: 0;
}

/* Select fields */
div.slselectfield_{$id} {
}

div#slform_{$id} div.slselectfield_{$id} label {
  display: block;  
  margin: 0 0 2px 0;
  padding: 0;	
}

div#slform_{$id} div.slselectfield_{$id} select {
	margin: 0;
  /* box-sizing: border-box; */
}

/* Simple label_{$id} fields */
div.sllabelfield {
}

div#slform_{$id} div.sllabelfield_{$id} label {
  display: block;
  margin: 0 0 2px 0;
  padding: 0;	
}

/* Captcha fields */
div.slcaptchafield_{$id} {
}

div#slform_{$id} div.slcaptchafield_{$id} input[type="text"]{
	margin: 0;
  max-width: {$maxformwidth}px;
}

div#slform_{$id} div.slcaptchafield_{$id} label {
  display: block;  
  margin: 0 0 2px 0;
  padding:0;
}

div#slform_{$id} div.slcaptchafield_{$id} img{
  vertical-align: top;
  height: {$captchaheight}px;
}

/* File fields */
div.slfilefield_{$id} {
  margin: 0;
  padding:0;
}

div#slform_{$id} div.slfilefield_{$id} label {
  display: block;  
  margin: 0 0 2px 0;
  padding: 0;	
}

div#slform_{$id} div.slfilefield_{$id} input[type="file"] {
	margin: 0;
	padding: 2px;
	width: 100%;
  /* box-sizing: border-box; */
}

/* Textarea fields */
div.sltextareafield_{$id} {
}

div#slform_{$id} div.sltextareafield_{$id} label {
  display: block;
  margin: 0 0 2px 0;
  padding:0;
}

div#slform_{$id} div.sltextareafield_{$id} textarea{
  margin: 0 0 0 0px;
	width:100%;
	height: 5em;
	resize: none;
  /* box-sizing: border-box; */
}

div#slform_{$id} div.slmsg_{$id} {  
  padding: 0;
  margin: 1px 0 0 0;
  text-align: left;
  color: #{$messagecolor};
	font: {$messagestyle} {$messagesize}px {$fonttype};
}

div#slform_{$id} div.slformmsg_{$id} {
  padding: 0;
  margin: 0 0 10px 0;
  text-align: left;
  color: #{$formerrormsgcolor};
	font: {$formerrormsgstyle} {$formerrormsgsize}px {$fonttype};  
}

div#slform_{$id} #myButton_{$id} {
}
div#slform_{$id} #myButton_{$id}:hover {
}
div#slform_{$id} #myButton_{$id}:active {
}
EOT;

if ($sl_ajaxforms)
{
$css .= <<<EOT
/* busy spinner */
div#slspinner_{$id} {
  display: none;
  box-sizing: content-box;
  float: right;
  height: {$spinnersize}px;
  width: {$spinnersize}px;
  margin-left: 0px;
  margin-right: {$spinnermarginright}px;
  margin-top: {$spinnermargintop}px;
  position: relative;
  -webkit-animation: rotation .6s infinite linear;
  -moz-animation: rotation .6s infinite linear;
  -o-animation: rotation .6s infinite linear;
  animation: rotation .6s infinite linear;
  border-left: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-right: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-bottom: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, .3);
  border-top: 2px solid rgba({$spinnercolorR},{$spinnercolorG},{$spinnercolorB}, 1);
  border-radius: 100%;
}

@-webkit-keyframes rotation {
  from {
    -webkit-transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(359deg);
  }
}

@-moz-keyframes rotation {
  from {
    -moz-transform: rotate(0deg);
  }
  to {
    -moz-transform: rotate(359deg);
  }
}

@-o-keyframes rotation {
  from {
    -o-transform: rotate(0deg);
  }
  to {
    -o-transform: rotate(359deg);
  }
}

@keyframes rotation {
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(359deg);
  }
}

EOT;
}

$css .="\n/* Field specific styles */\n";
// Get count array for $sitelokfield_array so we can see if field used more than once
$countfield_array=array_count_values($sitelokfield_array);
for ($k=0;$k<$numfields;$k++)
{  
$css .= <<<EOT

#slfielddiv_{$id}_{$k}{
  width: {$fieldwidth_array[$k]}%;
  margin-bottom: {$bottommargin_array[$k]}px;
  padding:0;
  max-width: {$maxformwidth}px;
}

EOT;

}

$css .= <<<EOT
</style>
<!--[if lt IE 9]>
<style type="text/css">
div#slform_{$id} {
  width: {$maxformwidth}px;
}
</style>

<![endif]-->

<!--[if lte IE 7]>
<style type="text/css">
div#slform_{$id} div.sltextfield_{$id} input[type="text"], input[type="email"], input[type="password"] {
  width: 95%;
}
div#slform_{$id} div.sltextareafield_{$id} textarea{
  width: 95%;
}

div#slform_{$id} #myButton_{$id} {
	padding: 0px 0px;
}
</style>
<![endif]-->


EOT;
}





// See which fields are used in the form for 'allowed' parameter
$allowed="";
if (false!==array_search("username", $sitelokfield_array))
  $allowed.="Y";
else  
  $allowed.="N";
if (false!==array_search("password", $sitelokfield_array))
  $allowed.="Y";
else  
  $allowed.="N";
if (false!==array_search("name", $sitelokfield_array))
  $allowed.="Y";
else  
  $allowed.="N";
if (false!==array_search("email", $sitelokfield_array))
  $allowed.="Y";
else  
  $allowed.="N";
for ($k=1;$k<=50;$k++)
{
if (false!==array_search("custom".$k, $sitelokfield_array))
  $allowed.="Y";
else  
  $allowed.="N";
} 
// If file type in form then add enctype="multipart/form-data" for form
$enctype="";
if (false!==array_search("file", $inputtype_array))
  $enctype="enctype=\"multipart/form-data\" ";
$html = <<<EOT
<div id="slform_{$id}">
<form action="" method="post" {$enctype}onSubmit="return slvalidateform_{$id}(this)">

EOT;
if ($sl_ajaxforms)
{
$html.= <<<EOT
    <input type="hidden" name="slajaxform" value="0">

EOT;
}
if ($fullsource)
  $html.="<?php registeruser(\"{$usergroup}\",\"{$expiry}\",\"{$redirect}\",\"{$useremail}\",\"{$adminemail}\",\"{$enabled}\",\"{$allowed}\"); ?>\n";
else
{
ob_start();
registeruser($usergroup,$expiry,$redirect,$useremail,$adminemail,$enabled,$allowed);
$html.= ob_get_contents(); 
ob_end_clean();    
$html.="\n";
}
$html.="<div id=\"slformmsg_{$id}\" class=\"slformmsg_{$id}\">";
if ($fullsource)
  $html.="<?php if (\$registermsg!=\"\") echo \$registermsg; ?>";
else
{
  if ($registermsg!="")
    $html.="$registermsg";
}  
$html.="</div>\n";
// setup array to flag error field use
$msgused=array();
$msgused['username']=false;
$msgused['password']=false;
$msgused['vpassword']=false;
$msgused['name']=false;
$msgused['email']=false;
$msgused['vemail']=false;
for ($j=1;$j<=50;$j++)
  $msgused['custom'.$j]=false;
$msgused['captcha']=false;
// See which field number should display message for that field type
$msgfield=array();
for ($k=0;$k<$numfields;$k++)
{
  $msgfield[$k]=false;
  // If field type not used before then flag to show message
  if (!$msgused[$sitelokfield_array[$k]])
  {
    $msgfield[$k]=true;
    $msgused[$sitelokfield_array[$k]]=true;
  }
}
for ($k=0;$k<$numfields;$k++)
{
// Set field name to use
$fieldname=$sitelokfield_array[$k];
if ($fieldname=="captcha")
  $fieldname="turing";
if ($fieldname=="vpassword")
  $fieldname="verifypassword";
if ($fieldname=="vemail")
  $fieldname="verifyemail";
  
// Convert some field names to be compatible with Wordpress
if ($fieldname=="name")
  $fieldname="regname";
if ($fieldname=="email")
  $fieldname="regemail";
if ($fieldname=="username")
  $fieldname="regusername";
if ($fieldname=="password")
  $fieldname="regpassword";
// If custom field is a checkbox and used more than once then use array
if ((substr($fieldname,0,6)=="custom") && ($inputtype_array[$k]=="checkbox"))
{
  if ($countfield_array[$fieldname]>1)
    $fieldname.="[]";
}

if ($inputtype_array[$k]=="text")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="sltextfield_{$id}">
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$type="text";
if (($fieldname=="regemail") || ($fieldname=="verifyemail"))
  $type="email";
$maxlength="255";
if (($fieldname=="regemail") || ($fieldname=="verifyemail") || ($fieldname=="regusername") || ($fieldname=="regname"))
  $maxlength="100";
if (($fieldname=="password") || ($fieldname=="verifypassword"))
  $maxlength="50";

$html .= <<<EOT
</label>
<input type="{$type}" name="{$fieldname}" id="slfieldinput_{$id}_{$k}"
EOT;
if ($placetext_array[$k]!='')
  $html.=" placeholder=\"{$placetext_array[$k]}\"";
if (($fieldname=="regusername") || ($fieldname=="regpassword") || ($fieldname=="verifypassword"))
  $html.=" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"off\" autocomplete=\"off\"";
$html.=" maxlength=\"".$maxlength."\"";
if ($fullsource)
  $html.=" value=\"<?php echo \${$fieldname}; ?>\"";  
else
  $html.=" value=\"".$$fieldname."\"";  
    
$html.= ">\n";
if ($msgfield[$k])
    $html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


if ($inputtype_array[$k]=="password")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="sltextfield_{$id}">
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .= <<<EOT
</label>
<input type="password" name="{$fieldname}" id="slfieldinput_{$id}_{$k}"
EOT;
if ($placetext_array[$k]!='')
  $html.=" placeholder=\"{$placetext_array[$k]}\"";
$html.=" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"off\" autocomplete=\"off\"";          
$html .= ">\n";
if ($msgfield[$k])
  $html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


if (($inputtype_array[$k]=="dropdown") || ($inputtype_array[$k]=="usergroup"))
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="slselectfield_{$id}">
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .= <<<EOT
</label>
<select name="{$fieldname}" id="slfieldinput_{$id}_{$k}">
EOT;
$options=explode("\n",$value_array[$k]);
for ($j=0;$j<count($options);$j++)
{
  if($options[$j]!="")
  {
    $optionname=strtok($options[$j],",");
    $optionvalue=trim(strtok(","));
    // See if separate value
    if (($j==0) && ($validation_array[$k]!='notrequired'))
    {
      $html.="<option value=\"\">".$optionname."</option>\n";
      continue;
    }
    if ($optionvalue!="")
    {
      if ($fullsource)
        $html.="<option value=\"".$optionvalue."\" <?php if (\${$fieldname}==\"{$optionvalue}\") echo \"selected=selected\"; ?>>".$optionname."</option>\n";
      else
      {
        $html.="<option value=\"".$optionvalue."\" ";
        if ($$fieldname==$optionvalue) $html.= "selected=selected";
        $html.=">".$optionname."</option>\n";        
      }  
      continue;   
    } 
    if ($fullsource) 
      $html.="<option value=\"".$optionname."\" <?php if (\${$fieldname}==\"{$optionname}\") echo \"selected=selected\"; ?>>".$optionname."</option>\n";            
    else
    {
      $html.="<option value=\"".$optionname."\" ";
      if ($$fieldname==$optionname) $html.="selected=selected";
      $html.=">".$optionname."</option>\n";            
    }   
  }  
}
$html .= <<<EOT
</select>

EOT;
if ($msgfield[$k])
  $html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


if ($inputtype_array[$k]=="checkbox")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="slcbfield_{$id}">
<input type="checkbox" name="{$fieldname}" id="slfieldinput_{$id}_{$k}" value="{$value_array[$k]}"
EOT;
$pos=strpos($fieldname,"[]");
if (false!==$pos)
{
  $fieldnameadjusted=substr($fieldname,0,$pos);
  if ($checked_array[$k]=="1")
  {
    if ($fullsource)
      $html.=" <?php if ((preg_match(\"/(^|,){$value_array[$k]}(,|$)/\", \${$fieldnameadjusted})) || (\${$fieldnameadjusted}==\"\")) echo \" checked=checked\"; ?>";    
    else
      if ((preg_match("/(^|,){$value_array[$k]}(,|$)/",$$fieldnameadjusted)) || ($$fieldnameadjusted=="")) $html.= " checked=checked";    
  }  
  else
  {
    if ($fullsource)
      $html.=" <?php if (preg_match(\"/(^|,){$value_array[$k]}(,|$)/\", \${$fieldnameadjusted})) echo \" checked=checked\"; ?>";
    else
      if (preg_match("/(^|,){$value_array[$k]}(,|$)/",$$fieldnameadjusted)) $html.= " checked=checked";          
  }  
}
else
{
  if ($checked_array[$k]=="1")
  {
    if ($fullsource)
      $html.=" <?php if ((\${$fieldname}==\"{$value_array[$k]}\") || (\${$fieldname}==\"\")) echo \" checked=checked\"; ?>";    
    else
      if (($$fieldname==$value_array[$k]) || ($$fieldname=="")) $html.= " checked=checked";
  }
  else
  {
    if ($fullsource)
      $html.=" <?php if (\${$fieldname}==\"{$value_array[$k]}\") echo \" checked=checked\"; ?>";    
    else
      if ($$fieldname==$value_array[$k]) $html.= " checked=checked";
  }  
}  
$html .= <<<EOT
>
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .= <<<EOT
</label>

EOT;
if ($msgfield[$k])
  $html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


if ($inputtype_array[$k]=="radio")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="slradiofield_{$id}">
<input type="radio" name="{$fieldname}" id="slfieldinput_{$id}_{$k}" value="{$value_array[$k]}"
EOT;
if ($checked_array[$k]=="1")
{
  if ($fullsource)
    $html.=" <?php if ((\${$fieldname}==\"{$value_array[$k]}\") || (\${$fieldname}==\"\")) echo \" checked=checked\"; ?>";    
  else
    if (($$fieldname==$value_array[$k]) || ($$fieldname=="")) $html.=" checked=checked";
}  
else
{
  if ($fullsource)
    $html.=" <?php if (\${$fieldname}==\"{$value_array[$k]}\") echo \" checked=checked\"; ?>";    
  else
    if ($$fieldname==$value_array[$k]) $html.=" checked=checked";  
}  
$html .= <<<EOT
>
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .= <<<EOT
</label>

EOT;
if ($msgfield[$k])
  $html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


if ($inputtype_array[$k]=="captcha")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="slcaptchafield_{$id}">
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .="</label>\n";


if ($fullsource)
{
  $html .="<?php\n";
  $html .="if (function_exists('slshowcaptcha'))\n";
  $html .="{\n";
  $html.="echo slshowcaptcha();\n";
  $html .="}\n";
  $html .="else\n";
  $html .="{\n";
  $html .="?>\n";  
  $html .= <<<EOT
<input type="text" name="{$fieldname}" id="slfieldinput_{$id}_{$k}" placeholder="{$placetext_array[$k]}" maxlength="5" autocorrect="off" autocapitalize="off" spellcheck="off" autocomplete="off">
&nbsp;
EOT;
  $html.="<img src=\"<?php echo \$SitelokLocationURLPath; ?>turingimage.php\" height=\"{$captchaheight}\" title=\"{$placetext_array[$k]}\" alt=\"{$placetext_array[$k]}\">\n";
  $html .="<?php\n";  
  $html .="}\n";
  $html .="?>\n";    
}
else
{
  if (function_exists('slshowcaptcha'))
    $html.=slshowcaptcha();
  else
  {    
$html .= <<<EOT
<input type="text" name="{$fieldname}" id="slfieldinput_{$id}_{$k}" placeholder="{$placetext_array[$k]}" maxlength="5" autocorrect="off" autocapitalize="off" spellcheck="off" autocomplete="off">
&nbsp;
EOT;
$html.="<img src=\"{$SitelokLocationURLPath}turingimage.php\" height=\"{$captchaheight}\" title=\"{$placetext_array[$k]}\" alt=\"{$placetext_array[$k]}\">\n";
  }
} 
  
if ($msgfield[$k])
  $html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


if ($inputtype_array[$k]=="label")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="sllabelfield_{$id}">
<label>{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .= <<<EOT
</label>
<div id="slmsg_{$id}_{$k}" class ="slmsg_{$id}"></div>
</div>

EOT;
}


if ($inputtype_array[$k]=="file")
{
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="slfilefield_{$id}">
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .= <<<EOT
</label>
<input type="file" name="{$fieldname}" id="slfieldinput_{$id}_{$k}">

EOT;
if ($msgfield[$k])
  $html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


if ($inputtype_array[$k]=="textarea")
{
$cusnum=substr($fieldname,6);
$html .= <<<EOT

<div id="slfielddiv_{$id}_{$k}" class="sltextareafield_{$id}">
<label for="slfieldinput_{$id}_{$k}">{$labeltext_array[$k]}
EOT;
if (($rqdfieldlabel!='') && ($validation_array[$k]!='notrequired') && ($showrequired_array[$k]!=0))
{
  $html .= "<em>{$rqdfieldlabel}</em>\n";
}
$html .= <<<EOT
</label>
<textarea name="{$fieldname}" id="slfieldinput_{$id}_{$k}"
EOT;
if ($placetext_array[$k]!='')
  $html.=" placeholder=\"{$placetext_array[$k]}\"";
$html.=" maxlength=\"{$customfieldlengths[$cusnum]}\">";
if ($fullsource)          
  $html .= "<?php echo \${$fieldname}; ?></textarea>\n";
else
  $html .= "{$$fieldname}</textarea>\n";
  
if ($msgfield[$k])
  $html .="<div id=\"slmsg_{$id}_{$k}\" class =\"slmsg_{$id}\"></div>\n";
$html .= "</div>\n";
}


}

// Button
if ($sl_ajaxforms)
{  
  if ($spinnerposition=="button")
    $html.="<button id=\"myButton_".$id."\" type=\"submit\">".$btnlabel."<div id=\"slspinner_".$id."\"></div></button>\n";
  if ($spinnerposition=="form")
    $html.="<button id=\"myButton_{$id}\" type=\"submit\">".$btnlabel."</button><div id=\"slspinner_".$id."\"></div>\n";
}
else
{  
  $html.="<button id=\"myButton_{$id}\" type=\"submit\">".$btnlabel."</button>\n";
}

$html .= <<<EOT

</form>
</div>

EOT;


$js="";
$js .= <<<EOT
<script type="text/javascript">
function slvalidateform_{$id}(form)
{
  var errorfound=false
  document.getElementById('slformmsg_{$id}').innerHTML=''
  //document.getElementById('slformmsg_{$id}').style['display']="none"

EOT;
for ($k=0;$k<$numfields;$k++)
{
  $errormsg=str_replace("'","\'",$errormsg_array[$k]);
  $errormsg=str_replace("\n","\\n",$errormsg);
  if (($validation_array[$k]!='notrequired') && ($msgfield[$k]))
  {
  
  
    // email field validation
    if ($sitelokfield_array[$k]=="email")
    {
$js .= <<<EOT

  // Validate email
  document.getElementById('slmsg_{$id}_{$k}').innerHTML=''
  document.getElementById('slmsg_{$id}_{$k}').style['display']="none"  
  var email=document.getElementById('slfieldinput_{$id}_{$k}').value
  email=sltrim_{$id}(email)
  if ((email=='') || (!slvalidateemail_{$id}(email)))
  {
    document.getElementById('slmsg_{$id}_{$k}').innerHTML='{$errormsg}'
    document.getElementById('slmsg_{$id}_{$k}').style['display']="block"  
    if (!errorfound)
      document.getElementById('slfieldinput_{$id}_{$k}').focus()
    errorfound=true
  }

EOT;
    }  


    // verifyemail field validation  
    if ($sitelokfield_array[$k]=="vemail")
    {
      $js .="\n  // Validate verifyemail\n";
      $emailfield=-1;
      for ($j=0;$j<$numfields;$j++)
      {
        if ($sitelokfield_array[$j]=="email")
          $emailfield=$j;
      }
      if ($emailfield>-1)
        $js .= "  var email=document.getElementById('slfieldinput_{$id}_{$emailfield}').value\n  email=sltrim_{$id}(email)
\n";
      else 
        $js .= "  var email=\"\"\n";       
$js .= <<<EOT
  document.getElementById('slmsg_{$id}_{$k}').innerHTML=''
  document.getElementById('slmsg_{$id}_{$k}').style['display']="none"  
  verifyemail=document.getElementById('slfieldinput_{$id}_{$k}').value
  verifyemail=sltrim_{$id}(verifyemail)
  if ((email!='') && (slvalidateemail_{$id}(email)) && (email!=verifyemail))
  {
    document.getElementById('slmsg_{$id}_{$k}').innerHTML='{$errormsg}'
    document.getElementById('slmsg_{$id}_{$k}').style['display']="block"  
    if (!errorfound)
      document.getElementById('slfieldinput_{$id}_{$k}').focus()
    errorfound=true
  }

EOT;
    }  
    
  
    // password field validation  
    if ($sitelokfield_array[$k]=="password")
    {
$js .= <<<EOT

  // Validate password
  document.getElementById('slmsg_{$id}_{$k}').innerHTML=''
  document.getElementById('slmsg_{$id}_{$k}').style['display']="none"  
  var password=document.getElementById('slfieldinput_{$id}_{$k}').value
  password=sltrim_{$id}(password)
  if (password=='')
  {
    document.getElementById('slmsg_{$id}_{$k}').innerHTML='{$errormsg}'
    document.getElementById('slmsg_{$id}_{$k}').style['display']="block"  
    if (!errorfound)
      document.getElementById('slfieldinput_{$id}_{$k}').focus()
    errorfound=true
  }

EOT;
    }  


    // verifypassword field validation  
    if ($sitelokfield_array[$k]=="vpassword")
    {
      $js .= "\n  // Validate verify password\n";
      $passwordfield=-1;
      for ($j=0;$j<$numfields;$j++)
      {
        if ($sitelokfield_array[$j]=="password")
          $passwordfield=$j;
      }
      if ($passwordfield>-1)
        $js .= "  var password=document.getElementById('slfieldinput_{$id}_{$passwordfield}').value\n  password=sltrim_{$id}(password)\n";
      else 
        $js .= "  var password=\"\"\n";       
$js .= <<<EOT
  document.getElementById('slmsg_{$id}_{$k}').innerHTML=''
  document.getElementById('slmsg_{$id}_{$k}').style['display']="none"  
  verifypassword=document.getElementById('slfieldinput_{$id}_{$k}').value
  verifypassword=sltrim_{$id}(verifypassword)
  if ((password!='') && (password!=verifypassword))
  {
    document.getElementById('slmsg_{$id}_{$k}').innerHTML='{$errormsg}'
    document.getElementById('slmsg_{$id}_{$k}').style['display']="block"  
    if (!errorfound)
      document.getElementById('slfieldinput_{$id}_{$k}').focus()
    errorfound=true
  }


EOT;
    }  


    // standard field validation  
    if (($sitelokfield_array[$k]!="email") && ($sitelokfield_array[$k]!="vemail") && ($sitelokfield_array[$k]!="password") && ($sitelokfield_array[$k]!="vpassword") && ($sitelokfield_array[$k]!="label") && (($sitelokfield_array[$k]!="captcha") || (!function_exists("slcaptchahead"))))
    {
      $js .= "  // Validate {$sitelokfield_array[$k]}";
      if (($inputtype_array[$k]!="radio") && ($inputtype_array[$k]!="checkbox"))
      {
$js .= <<<EOT

  document.getElementById('slmsg_{$id}_{$k}').innerHTML=''
  document.getElementById('slmsg_{$id}_{$k}').style['display']="none"
  var value=document.getElementById('slfieldinput_{$id}_{$k}').value
  value=sltrim_{$id}(value)
  if (value=='')
  {
    document.getElementById('slmsg_{$id}_{$k}').innerHTML='{$errormsg}'
    document.getElementById('slmsg_{$id}_{$k}').style['display']="block"  
    if (!errorfound)
      document.getElementById('slfieldinput_{$id}_{$k}').focus()
    errorfound=true
  }


EOT;
      }
      else
      {
$js .= <<<EOT

  document.getElementById('slmsg_{$id}_{$k}').innerHTML=''
  document.getElementById('slmsg_{$id}_{$k}').style['display']="none"
  var checked=slseeifchecked_{$id}('{$sitelokfield_array[$k]}','slfieldinput_{$id}_')
  if (!checked)
  {
    document.getElementById('slmsg_{$id}_{$k}').innerHTML='{$errormsg}'
    document.getElementById('slmsg_{$id}_{$k}').style['display']="block"  
    if (!errorfound)
      document.getElementById('slfieldinput_{$id}_{$k}').focus()
    errorfound=true
  }


EOT;
      }        
    }
  }
}

  $errormsg=str_replace("'","\'",$formerrormsg);
  $errormsg=str_replace("\n","\\n",$errormsg);

$js .= <<<EOT

  if (errorfound)
  {
    document.getElementById('slformmsg_{$id}').innerHTML='{$errormsg}'
    //document.getElementById('slformmsg_{$id}').style['display']="block"    
    return(false)
  }
EOT;
if ($sl_ajaxforms)
{
$js .= <<<EOT
  document.getElementById('slformmsg_{$id}').innerHTML='';
  //document.getElementById('slformmsg_{$id}').style['display']="none";
  //See if any file fields in form by checking enctype
  var slfilefields=false;
  var slajaxavailable=false;
  var slformdataavailable=false
  form.slajaxform.value="0";
  if (form.enctype=="multipart/form-data")
    slfilefields=true;
  if ((window.XMLHttpRequest) && (typeof JSON === "object"))
  {
    slajaxavailable=true;
    var xhr = new XMLHttpRequest();    
    slformdataavailable=(xhr && ('upload' in xhr));
  }
  // If ajax supported but no FormData then only use if no file fields
  if ((!slformdataavailable) && (slfilefields))
    slajaxavailable=false;
  if (slajaxavailable)
  {
    form.slajaxform.value="1";
    xhr.onreadystatechange = function()
    {
      if (xhr.readyState == 4 && xhr.status == 200)
      {
        // Handle callback
        document.getElementById('myButton_{$id}').disabled=false
        document.getElementById('slspinner_{$id}').style['display']="none"
        var data = JSON.parse(xhr.responseText);
        if (data.success)
        {
          window.location=data.redirect;
          return(false);
        }
        else  
        {  
          document.getElementById('slformmsg_{$id}').innerHTML=data.message;
          //document.getElementById('slformmsg_{$id}').style['display']="block"
          return(false);
        }
      }
    };
    // Serialize form
    if (slformdataavailable)
      var formData = new FormData(form);
    else
      var formData=sl_serialize(form);
    var slfrmact=window.location.href;
    document.getElementById('myButton_{$id}').disabled=true
    document.getElementById('slspinner_{$id}').style['display']="block"
    xhr.open("POST", slfrmact, true);
    if (!slformdataavailable)
      xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(formData);
    return(false);
  }

EOT;
}
$js .= <<<EOT
  return(true)
}

function slvalidateemail_{$id}(email)
{
  var ck_email = /^([\w-\'!#$%&\*]+(?:\.[\w-\'!#$%&\*]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,9}(?:\.[a-z]{2})?)$/i
  if (!ck_email.test(email))
    return(false)
  return(true)  
}

function sltrim_{$id}(x)
{
    return x.replace(/^\s+|\s+$/gm,'');
}

function slseeifchecked_{$id}(name,idprefix)
{
  var checked=false
  var controls=document.getElementsByName(name)
  for (i=0;i<controls.length;i++)
  {
    // if not from this form then ignore
    if (controls[i].id.indexOf(idprefix)==-1)
      continue
    if (controls[i].checked)
    {
      checked=true
      break
    } 
  }
  // Also check for field[] if necessary
  if(!checked)
  {
    var controls=document.getElementsByName(name+'[]')
    for (i=0;i<controls.length;i++)
    {
      // if not from this form then ignore
      if (controls[i].id.indexOf(idprefix)==-1)
        continue
      if (controls[i].checked)
      {
        checked=true
        break
      } 
    }  
  }
  return(checked)
}

EOT;
if ($sl_ajaxforms)
{
$js .= <<<EOT
function sl_serialize(form)
{
 if (!form || form.nodeName !== "FORM") {
   return;
 }
 var i, j, q = [];
 for (i = form.elements.length - 1; i >= 0; i = i - 1) {
   if (form.elements[i].name === "") {
     continue;
   }
   switch (form.elements[i].nodeName) {
   case 'INPUT':
     switch (form.elements[i].type) {
     case 'text':
     case 'email':
     case 'number':
     case 'hidden':
     case 'password':
     case 'button':
     case 'reset':
     case 'submit':
       q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
       break;
     case 'checkbox':
     case 'radio':
       if (form.elements[i].checked) {
         q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
       }           
       break;
     case 'file':
       break;
     }
     break;       
   case 'TEXTAREA':
     q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
     break;
   case 'SELECT':
     switch (form.elements[i].type) {
     case 'select-one':
       q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
       break;
     case 'select-multiple':
       for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
         if (form.elements[i].options[j].selected) {
           q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].options[j].value));
         }
       }
       break;
     }
     break;
   case 'BUTTON':
     switch (form.elements[i].type) {
     case 'reset':
     case 'submit':
     case 'button':
       q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
       break;
     }
     break;
   }
 }
 return q.join("&");
}

EOT;
}
$js .= <<<EOT
</script>

EOT;
if ($fullsource)
{
  $js .="<?php\n";
  $js .="if (function_exists('slcaptchahead'))\n";
  $js .="  echo slcaptchahead();\n";
  $js .="?>\n";    
}
else
{
  if (function_exists('slcaptchahead'))
    $js.=slcaptchahead();  
}
// Adjust for RW compatibilty
$html=sl_rwadjust($html);
return(true);
}


?>