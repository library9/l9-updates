// Fix bug where bootstrap doesn't close panels properly after call to collapse
$('#accordion').on('show.bs.collapse', function () {
    $('#accordion .in').collapse('hide');
});

// Switch + and - icons on accordion panels
$('.collapse').on('shown.bs.collapse', function(){
$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
}).on('hidden.bs.collapse', function(){
$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
});




// Remove iCheck from the checkbox on the login form preview
$('#remember').iCheck('destroy');

<!-- Setup minicolor control -->
  var colpick = $('.colorbox').each( function() {
    $(this).minicolors({
      control: $(this).attr('data-control') || 'hue',
      inline: $(this).attr('data-inline') === 'true',
      letterCase: 'uppercase',
      opacity: false,
      changeDelay: 250,
      theme: 'bootstrap'
    });
  });

$('.selectpicker').selectpicker({
  style: 'btn-default',
  size: 'auto',
  dropupAuto: false
});

// Setup on change events for check boxes
$('#includecaptcha').on('ifChecked', function(event){
               updateform();
});
$('#includecaptcha').on('ifUnchecked', function(event){
               updateform();
});

$('#includeremember').on('ifChecked', function(event){
               $('#includeautologin').iCheck('uncheck');
               updateform();
});
$('#includeremember').on('ifUnchecked', function(event){
               updateform();
});

$('#includeautologin').on('ifChecked', function(event){
               $('#includeremember').iCheck('uncheck');
               updateform();
});
$('#includeautologin').on('ifUnchecked', function(event){
               updateform();
});

$('#includeforgot').on('ifChecked', function(event){
               updateform();
});
$('#includeforgot').on('ifUnchecked', function(event){
               updateform();
});

$('#includesignup').on('ifChecked', function(event){
               updateform();
});
$('#includesignup').on('ifUnchecked', function(event){
               updateform();
});

// Run this when the page first loads
var gradientPrefix = getCssValuePrefix('background','linear-gradient(to bottom, #79bcff 5%, #378ee5 100%)');
var radiusPrefix = getCssValuePrefix('border-radius','10px');

// Validate fields on blur

$("#newformname").focusout(function(){
  validateReqdBlur('#newformname','untitled');
});

$("#newbackgroundcolor").focusout(function(){
  validateColorBlur('#newbackgroundcolor','#FFFFFF');
  updatePreview();
});
 
$("#newlabelcolor").focusout(function(){
  validateColorBlur('#newlabelcolor','#1A305E');
  updatePreview();
});

$("#newinputtextcolor").focusout(function(){
  validateColorBlur('#newinputtextcolor','#1A305E');
  updatePreview();
});

$("#newbordercolor").focusout(function(){
  validateColorBlur('#newbordercolor','#378EE5');
  updatePreview();
});

$("#newinputbackcolor").focusout(function(){
  validateColorBlur('#newinputbackcolor','#FFFFFF');
  updatePreview();
});

$("#newmessagecolor").focusout(function(){
  validateColorBlur('#newmessagecolor','#FF0000');
  updatePreview();
});

$("#newlogintextcolor").focusout(function(){
  validateColorBlur('#newlogintextcolor','#FFFFFF');
  updatePreview();
});

$("#newloginbuttoncolorfrom").focusout(function(){
  validateColorBlur('#newloginbuttoncolorfrom','#1A305E');
  updatePreview();
});

$("#newloginbuttoncolorto").focusout(function(){
  validateColorBlur('#newloginbuttoncolorto','#378EE5');
  updatePreview();
});

$("#newbtnbordercolor").focusout(function(){
  validateColorBlur('#newbtnbordercolor','#FFFFFF');
  updatePreview();
});

$("#newforgotcolor").focusout(function(){
  validateColorBlur('#newforgotcolor','#1A305E');
  updatePreview();
});

$("#newsignupcolor").focusout(function(){
  validateColorBlur('#newsignupcolor','#1A305E');
  updatePreview();
});

$("#newlabelsize").focusout(function(){
  validateSizeBlur('#newlabelsize','16');
  updatePreview();
});

$("#newinputtextsize").focusout(function(){
  validateSizeBlur('#newinputtextsize','16');
  updatePreview();
});

$("#newbtnbordersize").focusout(function(){
  validateSizeBlur('#newbtnbordersize','0');
  updatePreview();
});

$("#newmessagesize").focusout(function(){
  validateSizeBlur('#newmessagesize','12');
  updatePreview();
});

$("#newlogintextsize").focusout(function(){
  validateSizeBlur('#newlogintextsize','14');
  updatePreview();
});

$("#newforgotsize").focusout(function(){
  validateSizeBlur('#newforgotsize','12');
  updatePreview();
});

$("#newsignupsize").focusout(function(){
  validateSizeBlur('#newsignupsize','14');
  updatePreview();
});

$("#newmaxformwidth").focusout(function(){
  validateIntegerBlur('#newmaxformwidth',1,999999,'300');
  updatePreview();
});

// Detect which browser prefix to use for the specified CSS value
// (e.g., background-image: -moz-linear-gradient(...);
//        background-image:   -o-linear-gradient(...); etc).
//
// http://stackoverflow.com/questions/15071062/using-javascript-to-edit-css-gradient
function getCssValuePrefix(name, value) {
    var prefixes = ['', '-o-', '-ms-', '-moz-', '-webkit-'];

    // Create a temporary DOM object for testing
    var dom = document.createElement('div');

    for (var i = 0; i < prefixes.length; i++) {
        // Attempt to set the style
        dom.style[name] = prefixes[i] + value;

        // Detect if the style was successfully set
        if (dom.style[name]) {
            return prefixes[i];
        }
        dom.style[name] = '';   // Reset the style
    }
}

function filltype(filltype,togradient)
{
  var filltype=$('#'+filltype).val();
  if (filltype=="solid")
    $('#'+togradient).hide();
  else
    $('#'+togradient).show();
}

function gradientreverse(fromgradient,togradient)
{
  var from=$('#'+fromgradient).val();
  var to=$('#'+togradient).val();
  $('#'+fromgradient).minicolors('value', to);
  $('#'+togradient).minicolors('value', from);
  updatePreview()
}

function updateform()
{
  includecaptcha=$('#includecaptcha').iCheck('update')[0].checked;
  captchasection=document.getElementById('captchasection');
  includeremember=$('#includeremember').iCheck('update')[0].checked;
  remembersection=document.getElementById('remembersection');
  includeautologin=$('#includeautologin').iCheck('update')[0].checked;
  autologinsection=document.getElementById('autologinsection');
  includeforgot=$('#includeforgot').iCheck('update')[0].checked;
  forgotsection=document.getElementById('forgotsection');
  includesignup=$('#includesignup').iCheck('update')[0].checked;
  signupsection=document.getElementById('signupsection');
  if (includecaptcha)
    captchasection.style['display']="block"; 
  else
    captchasection.style['display']="none"; 
  if (includeremember)
    remembersection.style['display']="block"; 
  else
    remembersection.style['display']="none"; 
  if (includeautologin)
    autologinsection.style['display']="block"; 
  else
    autologinsection.style['display']="none"; 
  if (includeforgot)
    forgotsection.style['display']="block"; 
  else
    forgotsection.style['display']="none"; 
  if (includesignup)
    signupsection.style['display']="block"; 
  else
    signupsection.style['display']="none"; 
  updatePreview()  
}

function updatePreview()
{
  verifyform()
  previewobj=document.getElementById('formpreview')
  fonttype=document.getElementById('newloginfont').value
  messagecolor=document.getElementById('newmessagecolor').value
  messagesize=document.getElementById('newmessagesize').value
  messagestyle=document.getElementById('newmessagestyle').value
  usernamelabel=document.getElementById('newusernamelabel').value
  usernameplaceholder=document.getElementById('newusernameplaceholder').value
  passwordlabel=document.getElementById('newpasswordlabel').value
  passwordplaceholder=document.getElementById('newpasswordplaceholder').value
  includecaptcha=$('#includecaptcha').iCheck('update')[0].checked;
  captchalabel=document.getElementById('newcaptchalabel').value
  captchaplaceholder=document.getElementById('newcaptchaplaceholder').value
  includeremember=$('#includeremember').iCheck('update')[0].checked;
  rememberlabel=document.getElementById('newrememberlabel').value
  includeautologin=$('#includeautologin').iCheck('update')[0].checked;
  autologinlabel=document.getElementById('newautologinlabel').value
  labelcolor=document.getElementById('newlabelcolor').value
  labelsize=document.getElementById('newlabelsize').value
  labelstyle=document.getElementById('newlabelstyle').value
  inputtextcolor=document.getElementById('newinputtextcolor').value
  inputtextsize=document.getElementById('newinputtextsize').value
  inputtextstyle=document.getElementById('newinputtextstyle').value
  inputbackcolor=document.getElementById('newinputbackcolor').value
  bordersize=document.getElementById('newbordersize').value
  bordercolor=document.getElementById('newbordercolor').value
  borderradius=document.getElementById('newborderradius').value
  inputpaddingv=document.getElementById('newinputpaddingv').value;
  inputpaddingh=document.getElementById('newinputpaddingh').value;  
  btnlabel=document.getElementById('newlogintext').value
  btnlabelfont=document.getElementById('newlogintextfont').value
  btnlabelstyle=document.getElementById('newlogintextstyle').value
  btnlabelcolor=document.getElementById('newlogintextcolor').value
  btnlabelsize=document.getElementById('newlogintextsize').value
  btncolortype=document.getElementById('newloginbuttonfilltype').value
  btncolorfrom=document.getElementById('newloginbuttoncolorfrom').value
  btnborderstyle=document.getElementById('newbtnborderstyle').value
  btnbordercolor=document.getElementById('newbtnbordercolor').value
  btnbordersize=document.getElementById('newbtnbordersize').value
  btnpaddingv=document.getElementById('newbtnpaddingv').value;
  btnpaddingh=document.getElementById('newbtnpaddingh').value;
  btnradius=document.getElementById('newloginbuttonshape').value
  maxformwidth=document.getElementById('newmaxformwidth').value
  includeforgot=$('#includeforgot').iCheck('update')[0].checked;
  forgottext=document.getElementById('newforgottext').value
  forgottextcolor=document.getElementById('newforgotcolor').value
  forgottextsize=document.getElementById('newforgotsize').value
  forgottextstyle=document.getElementById('newforgotstyle').value
  includesignup=$('#includesignup').iCheck('update')[0].checked;
  signuptext=document.getElementById('newsignuptext').value
  signupurl=document.getElementById('newsignupurl').value
  signuptextcolor=document.getElementById('newsignupcolor').value
  signuptextsize=document.getElementById('newsignupsize').value
  signuptextstyle=document.getElementById('newsignupstyle').value
  bottommargin=document.getElementById('newbottommargin').value

  previewobj.style['backgroundColor']=document.getElementById('newbackgroundcolor').value
  if (document.getElementById('newmaxformwidth').value<200)
    previewobj.style['minWidth']=document.getElementById('newmaxformwidth').value+'px'
  else
    previewobj.style['minWidth']='200px' 
  border='solid'
  if (bordersize==0)
    border='none'
  border+=' '+bordercolor+' '+bordersize+'px' 
  if (btncolortype=="solid")
    btncolorto=btncolorfrom
  else
   btncolorto=document.getElementById('newloginbuttoncolorto').value
  if (inputpaddingv=="0.3em") 
    captchaheight=(inputtextsize*1.75)+(bordersize*2);
  else
  {
    var inputpaddingvnum=inputpaddingv.replace('px','');
    captchaheight=(inputtextsize*1.2)+(inputpaddingvnum*2)+(bordersize*2);    
  }
  captchaheight=captchaheight.toFixed(2);
  if (captchaheight<30) 
    captchaheight=30;
  var captchawidth=4;
  if (inputpaddingh!="0.3em")
  {  
    var inputpaddinghnum=inputpaddingh.replace('px','');
    captchawidth=3+((2*inputpaddinghnum)/inputtextsize);
    captchawidth=captchawidth.toFixed(2);
    if (captchawidth<4) 
      captchawidth=4;
  } 
  forgottext=forgottext.replace(/ /g,"&nbsp;")  
  rememberlabel=rememberlabel.replace(" ","&nbsp;") 
  autologinlabel=autologinlabel.replace(" ","&nbsp;")    
  var htmlcode=''
  htmlcode+='<div style="max-width: '+maxformwidth+'px;'
  if (maxformwidth<200)
    htmlcode+=' min-width: '+maxformwidth+'px;'
  else
    htmlcode+=' min-width: 200px;'
  htmlcode+='"'  
  htmlcode+=' id="slregform">\n'
  htmlcode+='<div class="slformmsg" style="color:'+messagecolor+'; font: '+messagestyle+' '+messagesize+'px '+fonttype+';" style="cursor: pointer;" onclick="previewclick(\'newmessagesize\',\'collapse2\')">'
  htmlcode+='This is the message area'
  htmlcode+='</div>\n'

  // Username
  htmlcode+='<div class="sltextfield" style="margin-bottom: '+bottommargin+'px;">\n'
  htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" for="username" style="cursor: pointer;" onclick="previewclick(\'newusernamelabel\',\'collapse1\')">'+usernamelabel
  htmlcode+='</label>\n'
  htmlcode+='<input id="username" style="color:'+inputtextcolor+'; font: '+inputtextstyle+' '+inputtextsize+'px '+fonttype+'; background-color: '+inputbackcolor+'; border: '+border+'; border-radius: '+borderradius+'px; padding: '+inputpaddingv+' '+inputpaddingh+'; width: 100%" type="text" name="username"'
  if (usernameplaceholder!="")
    htmlcode+=' placeholder="'+usernameplaceholder+'"'
  htmlcode+=' style="cursor: pointer;" onclick="previewclick(\'newusernamelabel\',\'collapse1\')">'        
  htmlcode+='</div>\n'
  htmlcode+='\n'

  // Password
  htmlcode+='<div class="sltextfield" style="margin-bottom: '+bottommargin+'px;">\n'
  htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" for="password" style="cursor: pointer;" onclick="previewclick(\'newpasswordlabel\',\'collapse1\')">'+passwordlabel
  htmlcode+='</label>\n'
  htmlcode+='<input id="password" style="color:'+inputtextcolor+'; font: '+inputtextstyle+' '+inputtextsize+'px '+fonttype+'; background-color: '+inputbackcolor+'; border: '+border+'; border-radius: '+borderradius+'px; padding: '+inputpaddingv+' '+inputpaddingh+'; width: 100%" type="password" name="password"'
  if (passwordplaceholder!="")
    htmlcode+=' placeholder="'+passwordplaceholder+'"'
  htmlcode+=' style="cursor: pointer;" onclick="previewclick(\'newpasswordlabel\',\'collapse1\')">'        
  htmlcode+='</div>\n'
  htmlcode+='\n'
  
  // Remember me
  if (includeremember)
  {
    htmlcode+='<div class="slcbfield" style="margin-bottom: '+bottommargin+'px;">\n'
    htmlcode+='<input id="remember" type="checkbox" style="color:'+inputtextcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+'; background-color: '+inputbackcolor+'; border: '+border+'; border-radius: '+borderradius+'px;" name="remember" value="1"'
  //  if (checked_array[k]=='1')
  //    htmlcode+=' checked="checked"'    
    htmlcode+=' style="cursor: pointer;" onclick="previewclick(\'newrememberlabel\',\'collapse1\')">\n'
    htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" for="remember" style="cursor: pointer;" onclick="previewclick(\'newrememberlabel\',\'collapse1\')">'+rememberlabel
    htmlcode+='</label>\n'
    htmlcode+='</div>\n'
    htmlcode+='\n'
  }

  // Auto Login
  if (includeautologin)
  {
    htmlcode+='<div class="slcbfield" style="margin-bottom: '+bottommargin+'px;">\n'
    htmlcode+='<input id="remember" type="checkbox" style="color:'+inputtextcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+'; background-color: '+inputbackcolor+'; border: '+border+'; border-radius: '+borderradius+'px;" name="remember" value="2"'
  //  if (checked_array[k]=='1')
  //    htmlcode+=' checked="checked"'    
    htmlcode+=' style="cursor: pointer;" onclick="previewclick(\'newautologinlabel\',\'collapse1\')">\n'
    htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" for="remember" style="cursor: pointer;" onclick="previewclick(\'newautologinlabel\',\'collapse1\')">'+autologinlabel
    htmlcode+='</label>\n'
    htmlcode+='</div>\n'
    htmlcode+='\n'
  }

  // Captcha
  if (includecaptcha)
  {
    htmlcode+='<div class="slcaptchafield" style="margin-bottom: '+bottommargin+'px;">\n'
    htmlcode+='<label style="color:'+labelcolor+'; font: '+labelstyle+' '+labelsize+'px '+fonttype+';" for="turing" style="cursor: pointer;" onclick="previewclick(\'newcaptchalabel\',\'collapse1\')">'+captchalabel
    htmlcode+='</label>\n'
    htmlcode+='<input id="turing" maxlength=5 style="color:'+inputtextcolor+'; font: '+inputtextstyle+' '+inputtextsize+'px '+fonttype+'; background-color: '+inputbackcolor+'; border: '+border+'; border-radius: '+borderradius+'px; padding: '+inputpaddingv+' '+inputpaddingh+'; width: '+captchawidth+'em" type="text" name="turing"'
    if (captchaplaceholder!='')
      htmlcode+=' placeholder="'+captchaplaceholder+'"'        
    htmlcode+=' style="cursor: pointer;" onclick="previewclick(\'newcaptchalabel\',\'collapse1\')">\n'
    htmlcode+='&nbsp;<img src="turingimage.php" height="'+captchaheight+'">'
    htmlcode+='</div>\n'
    htmlcode+='\n'
  } 
    
  htmlcode+='\n'
  htmlcode+='<input style="color: '+btnlabelcolor+'; font: '+btnlabelstyle+' '+btnlabelsize+'px '+btnlabelfont+'; background:linear-gradient(to bottom, '+btncolorfrom+' 5%, '+btncolorto+' 100%); border: '+btnborderstyle+' '+btnbordercolor+' '+btnbordersize+'px;border-radius:'+btnradius+'px; padding: '+btnpaddingv+'px '+btnpaddingh+'px ;" type="button" class="myButton" id="myButton" value="'+btnlabel+'" title="Login" style="cursor: pointer;" onclick="previewclick(\'newlogintext\',\'collapse3\')">\n'
  if (includeforgot)
    htmlcode+='<a style="color:'+forgottextcolor+'; font: '+forgottextstyle+' '+forgottextsize+'px '+fonttype+';" id="slforgot" href="javaScript:void(0);" title="Forgot your password? Enter username or email &amp; click link" style="cursor: pointer;" onclick="previewclick(\'newforgottext\',\'collapse4\')">'+forgottext+'</a>\n'

  if (includesignup)
    htmlcode+='<p style="margin-top: '+bottommargin+'px;"id="slsignup"><a style="color:'+signuptextcolor+'; font: '+signuptextstyle+' '+signuptextsize+'px '+fonttype+';" id="signuplink" href="javaScript:void(0);" style="cursor: pointer;" onclick="previewclick(\'newsignuptext\',\'collapse5\')">'+signuptext+'</a></p>\n'

  htmlcode+='</div>\n'


  previewobj.innerHTML=htmlcode

}

function verifyform()
{
  var captchaenabled=TuringLogin;
  var cookielogin=CookieLogin;
  var includecaptcha=$('#includecaptcha').iCheck('update')[0].checked;
  var includeremember=$('#includeremember').iCheck('update')[0].checked;
  var includeautologin=$('#includeautologin').iCheck('update')[0].checked;
  var problems=''
  if ((!includecaptcha) && (captchaenabled==1))
    problems+='<li>'+ADMINET_CAPTCHAPROB1+'</li>\n'
  if ((includecaptcha) && (captchaenabled==0))
    problems+='<li>'+ADMINET_CAPTCHAPROB2+'</li>\n'
  if ((includeremember) && (cookielogin==0))
    problems+='<li>'+ADMINET_REMPROB1+'</li>\n'
  if ((includeremember) && (cookielogin==2))
    problems+='<li>'+ADMINET_REMPROB2+'</li>\n'
  if ((includeautologin) && (cookielogin==0))
    problems+='<li>'+ADMINET_AUTOPROB1+'</li>\n'
  if ((includeautologin) && (cookielogin==1))
    problems+='<li>'+ADMINET_AUTOPROB2+'</li>\n'
  if (problems=='')
    $('#formissues').hide();
  else
  {
    problems="<ul>"+problems+"</ul>\n"  
    $('#formissues').html(problems);
    $('#formissues').show();
  }
}

function previewclick(id,panel)
{
  if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();
  $('#'+panel).collapse('show');
  if (panel!='collapse1')
    $('#collapse1').collapse('hide');
  if (panel!='collapse2')
    $('#collapse2').collapse('hide');
  if (panel!='collapse3')
    $('#collapse3').collapse('hide');
  if (panel!='collapse4')
    $('#collapse4').collapse('hide');
  if (panel!='collapse5')
    $('#collapse5').collapse('hide');
  setTimeout(function() { document.getElementById(id).focus();$("#"+id).get(0).scrollIntoView(); }, 300);  // Allow panel to open before focus
}

var formFixed=true;

function toggleFixed()
{
  if ($('#previewbox').css('position')=='fixed')
  {
    $('#previewbox').css('position','static');
    $('#previewbox').css('width','100%');
    $('#toggleicon').css('opacity',"0.25");
    formFixed=false;
  }
  else
  {
    $('#previewbox').css('position','fixed');
    $('#previewbox').css('width','40%');
    $('#toggleicon').css('opacity',"0.6");
    formFixed=true;
  } 
}

$(window).on('resize', function(){
  var width=document.documentElement.clientWidth || window.innerWidth || document.getElementsByTagName('body')[0].clientWidth;
  if (width>=1200)
  {
    if (formFixed)
    {
      $('#previewbox').css('position','fixed');
      $('#previewbox').css('width','40%');
    }
    else
    {
      $('#previewbox').css('position','static');
      $('#previewbox').css('width','100%');
    }     
  }
  else
  {
    $('#previewbox').css('position','static');
    $('#previewbox').css('width','100%');    
  }
});


      // Validation and saving for form
      $('#loginform').submit(function(event) {
              var data={success:true,message:'',errors:{}};
              //Remove form message
              $('#resultloginform').html('');       
              var savearray = new Array()  
              // Get data and store serialises in form fields
              var savearray = new Array()  
              savearray[0]=document.getElementById('newformname').value
              savearray[1]=document.getElementById('newloginfont').value
              savearray[2]=document.getElementById('newmessagecolor').value
              savearray[3]=document.getElementById('newmessagesize').value
              savearray[4]=document.getElementById('newmessagestyle').value
              savearray[5]=document.getElementById('newlabelcolor').value
              savearray[6]=document.getElementById('newlabelsize').value
              savearray[7]=document.getElementById('newlabelstyle').value
              savearray[8]=document.getElementById('newinputtextcolor').value
              savearray[9]=document.getElementById('newinputtextsize').value
              savearray[10]=document.getElementById('newinputtextstyle').value
              savearray[11]=document.getElementById('newinputbackcolor').value
              savearray[12]=document.getElementById('newbordersize').value
              savearray[13]=document.getElementById('newbordercolor').value
              savearray[14]=document.getElementById('newborderradius').value
              savearray[15]=document.getElementById('newusernamelabel').value
              savearray[16]=document.getElementById('newusernameplaceholder').value
              savearray[17]=document.getElementById('newpasswordlabel').value
              savearray[18]=document.getElementById('newpasswordplaceholder').value
              if ($('#includecaptcha').iCheck('update')[0].checked)
                savearray[19]='1'
              else
                savearray[19]='0'    
              savearray[20]=document.getElementById('newcaptchalabel').value
              savearray[21]=document.getElementById('newcaptchaplaceholder').value
              if ($('#includeremember').iCheck('update')[0].checked)
                savearray[22]='1'
              else
                savearray[22]='0'    
              savearray[23]=document.getElementById('newrememberlabel').value
              if ($('#includeautologin').iCheck('update')[0].checked)
                savearray[24]='1'
              else
                savearray[24]='0'    
              savearray[25]=document.getElementById('newautologinlabel').value
              savearray[26]=document.getElementById('newlogintext').value
              savearray[27]=document.getElementById('newlogintextcolor').value
              savearray[28]=document.getElementById('newlogintextsize').value
              savearray[29]=document.getElementById('newloginbuttonfilltype').value
              savearray[30]=document.getElementById('newloginbuttoncolorfrom').value
              savearray[31]=document.getElementById('newloginbuttoncolorto').value
              savearray[32]=document.getElementById('newloginbuttonshape').value
              if ($('#includeforgot').iCheck('update')[0].checked)
                savearray[33]='1'
              else
                savearray[33]='0'    
              savearray[34]=document.getElementById('newforgottext').value
              savearray[35]=document.getElementById('newforgotcolor').value
              savearray[36]=document.getElementById('newforgotsize').value
              savearray[37]=document.getElementById('newforgotstyle').value
              if ($('#includesignup').iCheck('update')[0].checked)
                savearray[38]='1'
              else
                savearray[38]='0'    
              savearray[39]=document.getElementById('newsignuptext').value
              savearray[40]=document.getElementById('newsignupurl').value
              savearray[41]=document.getElementById('newsignupcolor').value
              savearray[42]=document.getElementById('newsignupsize').value
              savearray[43]=document.getElementById('newsignupstyle').value
              savearray[44]=document.getElementById('newbottommargin').value
              savearray[45]=document.getElementById('newmaxformwidth').value
              savearray[46]=document.getElementById('newbackgroundcolor').value
              savearray[47]=document.getElementById('newlogintextfont').value
              savearray[48]=document.getElementById('newlogintextstyle').value
              savearray[49]=document.getElementById('newbtnbordercolor').value
              savearray[50]=document.getElementById('newbtnbordersize').value
              savearray[51]=document.getElementById('newbtnborderstyle').value
              savearray[52]=document.getElementById('newinputpaddingv').value
              savearray[53]=document.getElementById('newinputpaddingh').value
              savearray[54]=document.getElementById('newbtnpaddingv').value
              savearray[55]=document.getElementById('newbtnpaddingh').value
              jsonString = JSON.stringify(savearray);
              document.getElementById('formstylesfield').value=jsonString 
              // Now call PHP to save form
              var label=showButtonBusy('submit');
              var formData = $('#loginform').serialize();
              $.ajax({
                  type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                  url         : 'adminsaveloginform.php', // the url where we want to POST
                  data        : formData, // our data object
                  dataType    : 'json', // what type of data do we expect back from the server
                  encode      : true
              })
                  // using the done promise callback
            .done(function(data) {
              sessionExpiredReported(data,true);
              // Show validation results from PHP in form
              showValidation(data,'resultloginform');
              hideButtonBusy('submit',label);
              if (data.success)
              {
                // If we were duplicating form we need to change actid to editform and set new actid
                // Alos if we were adding form we should switch form to edit
                var act=$('#act').val();
                if ((act=="duplform") || (act=="addform"))
                {
                  $('#act').val('editform');
                  $('#actid').val(data.formid);
                }
              }
            })

// using the fail promise callback
    .fail(function(data) {
    hideButtonBusy('submit',label);  
    $('#resultloginform').html('<div id="resultloginformmessage" class="alert alert-danger">'+ADMINMSG_NOTSAVED+'</div>');
//        console.log(data);
    });


        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });





