<?php

$curl = curl_init();
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.digikey.com/services/basicsearch/v1/search",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"PartPreference\":\"CT\",\"Quantity\":25,\"PartNumber\":\"P5555-ND\"}",
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "authorization:E5qD4tE7hH3wS0gS8eV1dT1rJ5eC2oJ8fH5vY2iA5pV1rP8pS5",
    "content-type: application/json",
    "x-digikey-locale-currency: REPLACE_THIS_VALUE",
    "x-digikey-locale-language: REPLACE_THIS_VALUE",
    "x-digikey-locale-shiptocountry: REPLACE_THIS_VALUE",
    "x-digikey-locale-site: REPLACE_THIS_VALUE",
    "x-ibm-client-id:29da7c78-dc1e-4013-b820-2e1579a7d447"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo "cURL response #:" .$response;
}