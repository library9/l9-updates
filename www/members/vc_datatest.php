<?php



//// * Example PHP implementation used for the index.html example
// 
//
//// DataTables PHP library
include( "DataTables-1.10.0/extensions/Editor-1.3.0/php/DataTables.php" );
//
//// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Join,
    DataTables\Editor\Validate;
//
//// Build our Editor instance and process the data coming from _POST

Editor::inst( $db,'new_part' )
    ->fields(
	  
        Field::inst( 'new_part.Part_Number' ),
		Field::inst( 'new_part.PN' ),
		
	    Field::inst( 'new_part.Description' ),
	    Field::inst( 'new_part.Manufacturer' ),
		Field::inst( 'new_part.Manufacturer_Part_Number'),
		Field::inst( 'new_part.status'),
		Field::inst( 'new_part.table_name')
		
   
    )
	
    ->process( $_POST )
    ->json();
