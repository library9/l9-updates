<?php 

$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="loggedin_index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>
<link type="text/css" rel="stylesheet" href="css/easy-responsive-tabs.css" />
<div class="side_work">
  <div class="working_area">
    <div class="status_panel">
      <div class="status_sec">
        <div class="statusbar">
          <p>Current Page - <strong>Calculators / Miscellaneous</strong></p>
        </div>
        <div class="returnstat"><a href="calculator.php" class="menu_click">Return</a></div>
      </div>
    </div>
    <div class="app_calculator">
      <div class="calc_buttons"> </div>
      <div id="body-container1">
        <div class="glossary-container misc_calc">
          <ul class="firstUL1">
            <li id="a" class="selected">Skin Effect</li>
            <li id="b">RF Amplifier</li>
            <li id="c">IC 555 Timer</li>
            <li id="d">Pinhole Sizing</li>
            <li id="e">Zener Diode</li>
            <li id="f">Line Of Sight</li>
            <li id="g">Helical Antenna</li>
            <li id="h">Single Layer Coil</li>
            <li id="i">Temperature</li>
            <li id="j">Heat Sink Temperature</li>
          </ul>
        </div>
		<div class="resize_con">
        <div class="content-container">
          <div id="content-for-a" style="background-color:#fff;clear:both;" class="calc_main">
            <div class="calc_content">
              <div class="calc_left">

                <!-----Skin Effect----->
                <form>
					<center>
					 <table align="center" style="margin-top:.3cm" class="capi">
					<tr><th colspan="2">Enter your values:</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
					<tr><td colspan="2" align="center"><img src="images/skin form.bmp" border="0"></td></tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Frequency: </b></font> </td>
                      <td align="left"><input type="text" id="sknfText1" onKeyUp="sknf_calc(this.form)" name="sknfText1" size="7" maxlength="15" >
                        <SELECT id="sknfSelect1" onChange="sknf_calc(this.form)" name="sknfSelect1">
                          <OPTION value="1">Hz</OPTION>
                          <OPTION value="1E3">KHz</OPTION>
                          <OPTION value="1E6" selected>MHz</OPTION>
                          <OPTION value="1E9">GHz</OPTION>
                        </SELECT>
                      </td>
                    </tr>

                    <tr>
                      <td colspan="2" align="center"><input type="reset" value="Clear"></td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
					<tr><th colspan="2">Result</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Skin Depth: </b></font></td>
                        <td align="left"><input type="text" id="sknfText2" onKeyUp="sknf_calc(this.form)" name="sknfText2" size="7" maxlength="15">
                        <SELECT id="sknfSelect2" onChange="sknf_calc(this.form)" name="sknfSelect2">
                          <OPTION value="1">cm</OPTION>
                          <OPTION value="10">mm</OPTION>
                          <OPTION value="10000" selected>um</OPTION>
                          <OPTION value="0.393701">inches</OPTION>
                          <OPTION value="393.7008">mils</OPTION>
                          <OPTION value="285.7045">oz/ft^2</OPTION>
                        </SELECT>
                        </td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                  </table>
				  </center>
                </form>
                <br>
                <!----------------skin effect end--------------------->
                <table border="0" bgcolor="#c3d9ff" align="center">
                  <tr>
                    <input type="hidden" name="client" value="pub-0297548667908210">
                    </input>
                    <input type="hidden" name="forid" value="1">
                    </input>
                    <input type="hidden" name="ie" value="ISO-8859-1">
                    </input>
                    <input type="hidden" name="oe" value="ISO-8859-1">
                    </input>
                    <input type="hidden" name="safe" value="active">
                    </input>
                    <input type="hidden" name="cof" value="GALT:#008000;GL:1;DIV:#FFFFFF;VLC:663399;AH:center;BGC:FFFFFF;LBGC:FFFFFF;ALC:0000FF;LC:0000FF;T:000000;GFNT:0000FF;GIMP:0000FF;LH:49;LW:234;L:http://www.calculatoredge.com/links/calc2.jpg;S:http://;FORID:11">
                    </input>
                    <input type="hidden" name="hl" value="en">
                    </input>
                    </td>
                  </tr>
                </table>
                </form>
              </div>
              <div class="calc_right">
                <h5>Skin Effect</h5>
                <p>Because of skin effect ac current tends to flow on the outside of a connector.  On a wire the current density looks like a hollow tube.  
                  Since the inside isn't used for AC current flow, it makes sense to eliminate as much of the hollow part as possible. 
                  Skin depth is defined as the distance below the surface where the current density has fallen to 1/e or  37% of its value at the surface.</p>
                <div class="calc_foot">
                  <p class="return-to-top">Return to Top</p>
                </div>
              </div>
            </div>
          </div>
          <div id="content-for-b" style="background-color:#fff;clear:both;" class="calc_main">
            <div class="calc_content">
              <div class="calc_left">

                <!----------------RF Amplifier-----start---------------->
                <form name="form"  method="POST" action="" >
					<center>
					 <table align="center" style="margin-top:.3cm" class="capi">
					<tr><th colspan="2">Enter your values:</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
					<tr><td colspan="2" align="center"><img src="images/delta form.bmp" border="0"><br>
                  <img src="images/rollet form.bmp" border="0"></td></tr>

                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Impedance Zo:</b></font> </td>
                      <td align="left"><input type="text" name="Zo" size="7" maxlength="15">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> ohms</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Frequency: </b></font> </td>
                      <td align="left"><input type="text" name="Freq" size="7" maxlength="15">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> MHz</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Source Impedance: </b></font> </td>
                      <td align="left"><input type="text" name="Zsr" size="7" maxlength="15" >
                        <input type="text"  name="Zsi" size="7" maxlength="15">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> j</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Load Impedance: </b></font> </td>
                      <td align="left"><input type="text" name="Zlr" size="7" maxlength="15">
                        <input type="text"  name="Zli" size="7" maxlength="15">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> j</b></font> </td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Source L Match Networks:</b></font></td>
					</tr>
					<tr>	
	                   <td align="right"> <input type="radio" name="Snet" value="2">
                        <font size="2" face="verdana" color="#1f6fa2"><b>Low Pass</b></font></td>
                       <td align="left"><input type="radio" name="Snet" value="1">
					   <font size="2" face="verdana" color="#1f6fa2"><b> High Pass </b></font> </td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center"><font size="2" face="verdana" color="#1f6fa2"><b>Load L Match Networks:</b></font></td>
					 </tr>
					 <tr> 
                        <td align="right"><input type="radio" name="Lnet" value="2">
                        <font size="2" face="verdana" color="#1f6fa2"><b>Low Pass</b></font></td>
                        <td align="left"><input type="radio" name="Lnet" value="1">
                        <font size="2" face="verdana" color="#1f6fa2"><b>High Pass </b></font> </td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center"><font size="2" face="verdana" color="#1f6fa2"><b>S Parameters:</b></font></td>
                     </tr>
					 <tr> 
					    <td align="right"><input type="radio" value="1" name="Coordinates">
                        <font size="2" face="verdana" color="#1f6fa2"><b>Rectangle Co-ordinates</b></font></td>
						<td align="left"><input type="radio" value="2" name="Coordinates">
                        <font size="2" face="verdana" color="#1f6fa2"><b>Polar Co-ordinates </b></font> </td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                    <tr>
                      <td align="center" colspan="2"><font size="2" face="verdana" color="#1f6fa2"><b>S<sub>11</sub>:</b></font>
						<input type="text" name="S11Re" size="7" maxlength="15"  >
                        <input type="text"  name="S11Im" size="7" maxlength="15"></td>
                    </tr>
                    <tr>
                      <td align="center" colspan="2"><font size="2" face="verdana"
        color="#1f6fa2"><b>S<sub>12</sub>:
                        <input type="text" name="S12Re" size="7" maxlength="15">
                        <input type="text"  name="S12Im" size="7" maxlength="15">
                        </b></font> </td>
                    </tr>
                    <tr>
                      <td align="center" colspan="2"><font size="2" face="verdana"
        color="#1f6fa2"><b>S<sub>21</sub>:
                        <input type="text" name="S21Re" size="7" maxlength="15">
                        <input type="text" name="S21Im" size="7" maxlength="15">
                        </b></font> </td>
                    </tr>
                    <tr>
                      <td align="center" colspan="2"><font size="2" face="verdana"
        color="#1f6fa2"><b>S<sub>22</sub>:
                        <input type="text" name="S22Re" size="7" maxlength="15">
                        <input type="text"  name="S22Im" size="7" maxlength="15">
                        </b></font> </td>
                    </tr>
                    <tr>
                      <td  align="right"><input type="Button" value="Calculate" name="Calculate" onClick="CalculateK(this.form)" class="calculate_cal"></td>
                      <td align="left"> <input type="reset" value="Clear" class="clear_cal"> </td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                    <tr><th colspan="3">Result</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Rollet Stability Factor: </b></font> </td>
                      <td align="left"><input type="text" name="K" size="7" maxlength="15" readonly="readonly">
                      </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Maximum Allowable Gain: </b></font> </td>
                      <td align="left"><input type="text" name="MaGain" size="7" maxlength="15" readonly="readonly">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> Gain</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Maximum Allowable Gain: </b></font> </td>
                      <td align="left"><input type="text" name="MaGainDb" size="7" maxlength="15" readonly="readonly">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> Db</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Conjugate Input Reflection: </b></font> </td>
                      <td align="left"><input type="text" name="GammaLm" size="7" maxlength="15" readonly="readonly">
                        <input type="text" name="GammaLa" size="7" maxlength="15" readonly="readonly">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> Polar </b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Conjugate Output Reflection: </b></font> </td>
                      <td align="left"><input type="text" name="GammaSm" size="7" maxlength="15" readonly="readonly">
                        <input type="text"  name="GammaSa" size="7" maxlength="15" readonly="readonly">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> Polar </b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Input Impedances: </b></font> </td>
                      <td align="left"><input type="text" name="Zsmr" size="7" maxlength="15" readonly="readonly">
                        <input type="text" name="Zsmi" size="7" maxlength="15" readonly="readonly" >
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> j</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Output Impedances: </b></font> </td>
                      <td align="left"><input type="text" name="Zlmr" size="7" maxlength="15" readonly="readonly">
                        <input type="text" name="Zlmi" size="7" maxlength="15" readonly="readonly">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> j</b></font> </td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Source L Match Network Components:</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance:</b></font> </td>
                      <td align="left"><input type="text" name="Cs" size="7" maxlength="15" readonly="readonly">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> pf</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"

        color="#1f6fa2"><b>Inductance:</b></font> </td>
                      <td align="left"><input type="text" name="Ls" size="7" maxlength="15" readonly="readonly">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> nH</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Q<sub>s</sub>:</b></font> </td>
                      <td align="left"><input type="text" name="Qs" size="7" maxlength="15" readonly="readonly">
                        </b></font> </td>
                    </tr>
                    <tr>
                      <td align="center" colspan="2"><font size="2" face="verdana"
        color="#1f6fa2"><b>Load L Match Network Components:</b></font> </td>
                    </tr>
                    <tr>
                      <td  align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance:</b></font> </td>
                      <td align="left"><input type="text" name="Cl" size="7" maxlength="15" readonly="readonly">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> pf</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Inductance:</b></font> </td>
                      <td align="left"><input type="text" name="Ll" size="7" maxlength="15" readonly="readonly">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> nH</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Q<sub>L</sub>:</b></font> </td>
                      <td align="left"><input type="text" name="Ql" size="7" maxlength="15" readonly="readonly">
                        </b></font> </td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center"><input type="hidden" name="AmpInIsPar" value="1" readonly="readonly">
                        <input type="hidden" name="AmpOutIsPar" value="1" readonly="readonly"></td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                  </table>
				  </center>
                </form>
                <!--------------RF Amplifier end----------------------->
              </div>
              <div class="calc_right">
                <h5>RF Amplifier</h5>
                <p>A RF amplifier is a type of electronic amplifier used to convert a low-power radio-frequency signal into a larger signal, typically for driving the antenna of 
                  a transmitter. To use the calculator, enter in the amp's S-parameters. The characteristic impedance used in the S-parameter measurements. The frequency at which you 
                  will use the amplifier, the source's and load's impedance, and whether you want high pass or low pas networks on the L-Matching networks.</p>
                <div class="calc_foot">
                  <p class="return-to-top">Return to Top</p>
                </div>
              </div>
            </div>
          </div>
          <div id="content-for-c" style="background-color:#fff;clear:both;" class="calc_main">
            <div class="calc_content">
              <div class="calc_left">
                <table>
                  <tr>
                    <td align="center"><font size="2" face="verdana"
        color="black"><b>Enter value and click on calculate. Result will be displayed.</b> </font> </td>
                  </tr>
                </table>
                <p align="center"></p>
                <br>
                <!----------------IC 555 Timer--start------------------->
                <form method="POST" name="form">
					<center>
					 <table align="center" style="margin-top:.3cm" class="capi">
					<tr><th colspan="2">Enter your values:</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
					<tr><td colspan="2" align="center"><img src="images/tp form.bmp" border="0"><br>
                  <br>
                  <img src="images/tn form.bmp" border="0"><br>
                  <br>
                  <img src="images/freq form.bmp" border="0"></td></tr>

                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistance R<sub>a</sub>:</b></font> </td>
                      <td align="left"><INPUT name="R1" onChange="SetR1(this)" size="9" maxlength="15" type="text">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> K Ohms</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistance R<sub>b</sub>: </b></font> </td>
                      <td align="left"><INPUT name="R2" onChange="SetR2(this)"  size="9" maxlength="15" type="text">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> K Ohms</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance: </b></font> </td>
                      <td align="left"><input type="text" INPUT name="C" onChange="SetC(this)" size="9" maxlength="15">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> Microfarads</b></font> </td>
                    </tr>

                    <tr>
                      <td align="right"><input type="Button" value="Calculate" onClick="Process(this.form)" class="calculate_cal"></td>
                       <td align="left"> <input type="reset" value="Clear" class="clear_cal"></td>
                    </tr>
                    <tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>					
					<tr><th colspan="2">Result</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Positive Time Interval T<sub>p</sub>:</b></font> </td>
                      <td align="left"><input type="text" name="T1" size="10" maxlength="15" readonly="readonly">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> Milliseconds</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Negative Time Interval T<sub>n</sub>:</b></font> </td>
                      <td align="left"><input type="text" name="T2" size="10" maxlength="15" readonly="readonly">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> Milliseconds</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Frequency:</b></font> </td>
                      <td align="left"><input type="text" name="F" size="10" maxlength="15" readonly="readonly">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> Kilohertz</b></font> </td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                  </table>
				  </center>
                </form>
                <!-----------------IC 555 Timer--end------------------>
              </div>
              <div class="calc_right">
                <h5>IC 555 Timer</h5>
                <p>The 555 timer IC is an integrated circuit (chip) used in a variety of timer, pulse generation, and oscillator applications. 
                  The 555 can be used to provide time delays, as an oscillator, and as a flip-flop element. Derivatives provide up to four timing circuits in one package.</p>
                <div class="calc_foot">
                  <p class="return-to-top">Return to Top</p>
                </div>
              </div>
            </div>
          </div>
          <div id="content-for-d" style="background-color:#fff;clear:both;" class="calc_main">
            <div class="calc_content">
              <div class="calc_left">

                <!---------------Pinhole Sizing--start-------------------->
                <form method="POST">
					<center>
					 <table align="center" style="margin-top:.3cm" class="capi">
					<tr><th colspan="2">Enter your values:</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2" ><b>Depth of Camera:</b> <br>
                        <b>(from pinhole to film)</b></font></td>
                      <td align="left"><input type="text" size="5" maxlength="10" name="depth">
                      <select name="scale" size="1" value="2">
                          <option value="0">Inches </option>
                          <option value="1">cm </option>
                          <option value="2">mm </option>
                        </select></td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2" ><b>Aperture Diameter: </b><br>
                        <b>(Leave value at 0 to use optimum)</b></font></td>
                      <td align="left"><input type="text" size="5" maxlength="10" name="hole">
                      <font size="2" face="verdana"
        color="#1f6fa2" ><b>mm</b></font></td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2" ><b>Metered Aperture:</b></font></td>
                      <td align="left"><input type="text" size="5" maxlength="10" name="aperture"></td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2" ><b>Metered Exposure Time:</b></font></td>
                      <td align="left"><input type="text" size="5" maxlength="10" name="num">
                      <font size="2" face="verdana" color="#1f6fa2" ><b>/</b></font>
                        <input type="text" size="5" maxlength="10" name="den" ></td>
                    </tr>
               
					<tr>
					
                    <td align="right">  <input type="button" value="Calculate" onClick="compute(this.form)" class="calculate_cal"></td>
                     <td align="left"> <input type='reset' value="Clear" class="clear_cal"></td>
					</tr>  
					<tr><td colspan="2" style="height:15px;"></td></tr>
					<tr><th colspan="2">Result</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2" ><b>Image Diameter:</b></font></td>
                      <td align="left"><input type="text" size="6" maxlength="10" name="imagesz" readonly="readonly"><input type="text" size="5" maxlength="10" name="imscale" readonly="readonly"></td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2" ><b>Optimum Pinhole Diameter:</b></font></td>
                      <td align="left"><input type="text" size="6" maxlength="10" name="optdia" readonly="readonly"> <font size="2" face="verdana" color="#1f6fa2" ><b>mm</b></font></td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b>F-Stop:</b></font></td>
                      <td align="left"><input type="text" size="6" maxlength="10" name="fstop" readonly="readonly"></td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2" ><b>Exposure Factor:</b></font></td>
                      <td align="left"><input type="text" size="6" maxlength="10" name="exposure" readonly="readonly"></td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2" ><b>Pinhole Exposure Time:</b></font></td>
                      <td align="left"><input type="text" size="6" maxlength="10" name="expmin" readonly="readonly"> <font size="2" face="verdana" color="#1f6fa2" ><b> m</b></font>
                      <input type="text" size="5" maxlength="6" name="expsec" readonly="readonly"> <font size="2" face="verdana" color="#1f6fa2" ><b>secs</b></font></td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>					
                  </table>
				  </center>
                </form>
                <!-------------------Pinhole Sizing-------end----------->
              </div>
              <div class="calc_right">
                <h5>Pinhole Sizing</h5>
                <p>Within limits, a smaller pinhole will result in sharper image resolution because the projected circle of confusion at the image plane is practically the same size as the pinhole. An extremely small hole, however, can produce significant diffraction effects and a less clear image due to the wave properties of light.</p>
                <div class="calc_foot">
                  <p class="return-to-top">Return to Top</p>
                </div>
              </div>
            </div>
          </div>
          <div id="content-for-e" style="background-color:#fff;clear:both;" class="calc_main">
            <div class="calc_content">
              <div class="calc_left">

                <!----------------Zener Diode------start--------------->
                <FORM name="form">
					<center>
					 <table align="center" style="margin-top:.3cm" class="capi">
					<tr><th colspan="2">Enter your values:</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
					<tr><td colspan="2" align="center"><img src="images/zener.jpg" border="0"></td></tr>					
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Maximum Input Voltage: </b></font> </td>
                      <td align="left"><input type="text" name="VinMax" size="7" maxlength="15">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> Volts</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Minimum Input Voltage: </b></font> </td>
                      <td align="left"><input type="text"  name="VinMin" size="7" maxlength="15">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> Volts</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Output Voltage: </b></font> </td>
                      <td align="left"><input type="text" name="Vout" size="7" maxlength="15">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> Volts</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Load Current: </b></font> </td>
                      <td align="left"><input type="text" name="Iload" size="7" maxlength="15">
                        <font size="2" face="verdana"
        color="#1f6fa2"><b> MAmperes</b></font> </td>
                    </tr>

                    <tr>
                      <td align="right"><input type="Button" value="Calculate" onClick="calculate(this.form)" class="calculate_cal"></td>
                       <td align="left"> <input type="reset" value="Clear" class="clear_cal"></td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
					<tr><th colspan="2">Result</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistance: </b></font> </td>
                      <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>
                        <input type="text"  name="R"  size="10" maxlength="45" style="margin-bottom:2px;" readonly="readonly">
                        Ohms &nbsp;
                        <input type="text"  name="Rp"  size="10" maxlength="45" readonly="readonly">
                        Watts </b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Zener: </b></font> </td>
                      <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>
                        <input type="text"  name="vOut"  size="10" maxlength="45" style="margin-bottom:2px;" readonly="readonly">
                        Volts &nbsp;
                        <input type="text"  name="Zp"  size="10" maxlength="45" readonly="readonly">
                        Watts </b></font> </td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>					
                  </table>
				  </center>
				  
                </form>
                <!--------------Zener Diode--end--------------------->
              </div>
              <div class="calc_right">
                <h5>Zener Diode</h5>
                <p>A Zener Diode is an electronic component which can be used to make a very simple voltage regulator circuit. This circuit enables a fixed stable voltage to be 
                  taken from an unstable voltage source such as the battery bank of a renewable energy system which will fluctuate depending on the state of charge of the bank.
                  For any given output voltage and current requirement it will calculate the valueof power rating of the current limiting resistor and zener diode. </p>
                <div class="calc_foot">
                  <p class="return-to-top">Return to Top</p>
                </div>
              </div>
            </div>
          </div>
          <div id="content-for-f" style="background-color:#fff;clear:both;" class="calc_main">
            <div class="calc_content">
              <div class="calc_left">

                <!--------------Line Of Sight--start--------------------->
                <form method="POST" name="form" id="line">
					<center>
					 <table align="center" style="margin-top:.3cm" class="capi">
					<tr><th colspan="2">Enter your values:</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Antenna Height (1<sup>st</sup> Station): </b></font> </td>
                      <td align="left"><input type="text" name="height1" size="7" maxlength="15">
                      </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Antenna Height (2<sup>nd</sup> Station): </b></font> </td>
                      <td align="left"><input type="text" name="height2" size="7" maxlength="15">
                      </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Units: </b></font> </td>
                      <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>
                        <INPUT checked name="units" onClick="calc(this.form)" type="radio">
                        Feet
                        <INPUT name="units" onClick="calc(this.form)" type="radio">
                        Metres</b></font> </td>
                    </tr>

                    <tr>
                      <td align="right"><input type="Button" value="Calculate" onClick="calc11(this.form)" class="calculate_cal"></td>
                       <td align="left"> <input type="reset" value="Clear" class="clear_cal"></td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
					<tr><th colspan="2">Result</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Radio Horizon (1<sup>st</sup> Station): </b></font> </td>
                      <td align="left"><input type="text" name="horiz1" size="9" maxlength="15" readonly="readonly">
                      </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Radio Horizon (2<sup>nd</sup> Station): </b></font> </td>
                      <td align="left"><input type="text"  name="horiz2" size="9" maxlength="15" readonly="readonly">
                      </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Total Line Of Sight: </b></font> </td>
                      <td align="left"><input type="text" name="distance" size="9" maxlength="15" readonly="readonly">
                      </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Units: </b></font> </td>
                      <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>
                        <INPUT checked name="dist" onClick="calc(this.form)" type="radio">
                        Miles
                        <INPUT name="dist"
 onclick="calc(this.form)" type="radio">
                        Kilometres </b></font> </td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>					
                  </table>
				  </center>
                </form>
                <!---------------------Line Of Sight--end-------------->
              </div>
              <div class="calc_right">
                <h5>Line Of Sight</h5>
                <p>Here is a simple line of sight calculator that will do the complicated math for you to determine just how far the horizon is from your HT or
                  your base station antenna at any height above level and flat ground (or calm water) on the VHF/UHF ham bands.To use the calculator, simply type in your antenna
                  height above ground in the appropriate window below and click the "Compute" button. You will see the actual line of sight distance to the horizon in the 
                  "Distance" section before the radio waves are attenuated by the curvature of the Earth.</p>
                <div class="calc_foot">
                  <p class="return-to-top">Return to Top</p>
                </div>
              </div>
            </div>
          </div>
          <div id="content-for-g" style="background-color:#fff;clear:both;" class="calc_main">
            <div class="calc_content">
              <div class="calc_left">

                <!----------------Helical Antenna-start-------------------->
                <form method="POST" name="form">
					<center>
					 <table align="center" style="margin-top:.3cm" class="capi">
					<tr><th colspan="2">Enter your values:</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Wavelength: </b></font> </td>
                      <td align="left"><input type="text" name="lambda" size="7" maxlength="15"><font size="2" face="verdana" color="#1f6fa2"><b> m</b></font> </td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b> Or</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Frequency: </b></font> </td>
                      <td align="left"><input type="text" name="f" size="7" maxlength="15"><font size="2" face="verdana" color="#1f6fa2"><b> MHz</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Number of Turns: </b></font> </td>
                      <td align="left"><input type="text" name="N" size="7" maxlength="15">
                      </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Space between coils: </b></font> </td>
                      <td align="left"><input type="text" name="SL" size="7" maxlength="15"><font size="2" face="verdana" color="#1f6fa2"><b> wavelength</b></font> </td>
                    </tr>

                    <tr>
                      <td align="right"><input type="button" value="Calculate" name="B1" onClick="ComputeH(this.form);" class="calculate_cal"></td>
                       <td align="left"> <input type="reset" value="Clear" class="clear_cal"></td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
					<tr><th colspan="2">Result</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Antenna Gain: </b></font> </td>
                      <td align="left"><input type="text" name="G" size="7" maxlength="15" readonly="readonly"> <font size="2" face="verdana" color="#1f6fa2"><b> dBi</b></font> </td>
                    </tr>
                    <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Impedance: </b></font> </td>
                      <td align="left"><input type="text" name="Z" size="7" maxlength="15" readonly="readonly"><font size="2" face="verdana" color="#1f6fa2"><b> Ohms</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana" color="#1f6fa2"><b> Diameter: </b></font> </td>
                      <td align="left"><input type="text" name="D" size="7" maxlength="15" readonly="readonly"><font size="2" face="verdana"  color="#1f6fa2"><b> cm</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Space between coils: </b></font> </td>
                      <td align="left"><input type="text" name="S" size="7" maxlength="15" readonly="readonly"><font size="2" face="verdana"  color="#1f6fa2"><b> cm</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Length of wire: </b></font> </td>
                      <td align="left"><input type="text" name="L" size="7" maxlength="15" readonly="readonly"><font size="2" face="verdana" color="#1f6fa2"><b> cm</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Half Power Beam Width: </b></font> </td>
                      <td align="left"><input type="text" name="HPBW" size="7" maxlength="15" readonly="readonly"><font size="2" face="verdana" color="#1f6fa2"><b> degrees</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Beam Width First Nulls: </b></font> </td>
                      <td align="left"><input type="text" name="BWFN" size="7" maxlength="15" readonly="readonly"><font size="2" face="verdana"  color="#1f6fa2"><b> degrees</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Apperature: </b></font> </td>
                      <td align="left"><input type="text" name="Ae" size="7" maxlength="15" readonly="readonly"><font size="2" face="verdana" color="#1f6fa2"><b> m<sup>2</sup></b></font> </td>
                    </tr>
                  </table>
                </form>
                <!---------------end----Helical Antenna---------------------->
              </div>
              <div class="calc_right">
                <h5>Helical Antenna</h5>
                <p>Helical antennas invented by John Kraus give a circular polarized wave.  They are one of the easiest to design.  Find a tube with a circumference equal to one
                  wavelength, and wrap wire in a helix spaced a quarter wavelength. The conductor width isn't of great importance in the desing. The greater the number of turns 
                  the greater the directivity or antenna gain.  Receiving and transmitting antennas must be wound in the same direction, since the wave is polarized.</p>
                <div class="calc_foot">
                  <p class="return-to-top">Return to Top</p>
                </div>
              </div>
            </div>
          </div>
          <div id="content-for-h" style="background-color:#fff;clear:both;" class="calc_main">
            <div class="calc_content">
              <div class="calc_left">
 
                <!--------------------Single Layer Coil---start-------------->
        <form METHOD="POST" action="" >
                   <center> <TABLE align="center" style="margin-top:.3cm" class="capi">			  
                      <TBODY>   
                    <tr><th colspan="2">Enter your values:</th></tr>
					<tr><td colspan="2" style="height:15px;width"></td></tr>					  
                        <TR>                    						
                          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Required Inductance (L):</b></TD>
                          <TD align="left"><INPUT name="L" size="7" maxlength="15" type="text">
                          <SELECT name=LM STYLE="width: 64">
                              <OPTION value=0.001>nH 
                              <OPTION value=1 selected>uH </OPTION>
                            </SELECT></TD>
                        </TR>
                        <TR> 
                          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Coil Diameter (D):</b></TD>
                          <TD align="left"><INPUT  name="D" size="7" maxlength="15" type="text">
                          <SELECT name=DM STYLE="width: 64">
                              <OPTION value=1>inches 
                              <OPTION value=0.001>mils 
                              <OPTION value=0.03937 selected>mm </OPTION>
                            </SELECT></TD>
                        </TR>
                        <TR> 
                          <td align="right"><font size="2" face="verdana" color="#1f6fa2"><b> Wire Diameter (d):</b></TD>
                          <TD align="left"><input size="7" maxlength="15" name="d" type="text">
                          <SELECT name=dM STYLE="width: 64">
                              <OPTION value=1>inches 
                              <OPTION value=0.001>mils 
                              <OPTION value=0.03937 selected>mm </OPTION>
                            </SELECT></TD>
                        </TR>
                        <TR> 
                          <TD align="right"><input TYPE="button" NAME="name" VALUE="Calculate" onClick="solvesingle(this.form);" class="calculate_cal"></TD>
                          <TD  align="left"><input TYPE="reset" VALUE="Clear" onClick="clearBoxes(this.form);" class="clear_cal"></TD>
                        </TR>						
					<tr><td colspan="2" style="height:15px;"></td></tr>
					<tr><th colspan="2">Result</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>						
                        <TR> 
                          <td align="right"><font size="2" face="verdana" color="#1f6fa2"><b>Coil Length (l):</font></TD>
                          <TD align="left"><INPUT  name="l" size="9" maxlength="15" readonly="readonly">
                          <SELECT name=lM STYLE="width: 64">
                              <OPTION value=1>inches 
                              <OPTION value=0.001>mils 
                              <OPTION value=0.03937 selected>mm </OPTION>
                            </SELECT></TD>
                        </TR>
                        <TR> 
                          <td align="right"><font size="2" face="verdana" color="#1f6fa2"><b>Number of Turns (N):</font></TD>
                          <TD align="left"><input name="N" size="9" maxlength="15" readonly="readonly"></TD>
  
                        </TR>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                      </TBODY>
                    </TABLE>				
</form></center>
                <!--------------Single Layer Coil--end--------------------->
              </div>
              <div class="calc_right">
                <h5>Single Layer Coil</h5>
                <p>Must calculate the inductance of the air coil having 25 turns of wire wound to a diameter of 0.2 inches and a length of 1 inch.
                  Must calculate the number of turns to have an inductance of 1 microhenries with a coil measuring 8 millimeters in diameter and a length of 2 centimeters.
                  Must calculate the diameter to have an inductance of 10 microhenries with a 10 turn coil and a length of 10 millimeters.</p>
                <div class="calc_foot">
                  <p class="return-to-top">Return to Top</p>
                </div>
              </div>
            </div>
          </div>
          <div id="content-for-i" style="background-color:#fff;clear:both;" class="calc_main">
            <div class="calc_content">
              <div class="calc_left">

                <!---------------temp start---------------------->
                <form name="tempsize" action="temp.htm">

				  <center>
					 <table align="center" style="margin-top:.3cm" class="mistemp">
					<tr><th colspan="4">Enter your values:</th></tr>
					<tr><td colspan="4" style="height:15px;"></td></tr>
					<tr>
						<td align="center">
						<font size="2" face="verdana" color="#1f6fa2"><b>Fahrenheit to Celsius:</b></font><br>
                  <img alt="Temperature" src="images/temp c form.bmp" border="0"  /></td>
						<td align="center" colspan="2">
						<font size="2" face="verdana" color="#1f6fa2"><b>Celsius to Kelvin:</b></font><br>
                 <img alt="Temperature" src="images/temp k form.bmp" border="0" />						
						</td>
						<td align="center">
						<font size="2" face="verdana" color="#1f6fa2"><b>Celsius to Fahrenheit:</b></font><br>
                 <img alt="Temperature" src="images/temp f form.bmp" border="0" />							
						</td>												
					</tr>
					<tr><td colspan="4" style="height:15px;"></td></tr>
					<tr> 
						<th colspan="2" align="center"> Enter Your Values</th>
						<th colspan="2" align="center"> Result</th>						
                    <tr>
					<tr><td colspan="4" style="height:15px;"></td></tr>
					<tr> 
						<td colspan="2" align="center"><input type="text" name="tempfrom" size="7" maxlength="15" onFocus="resetvalue()"></td>
						<td colspan="2" align="center"><input type="text" name="tempto" size="12" maxlength="15"></td>						
                    <tr>	
					<tr><td colspan="4" style="height:15px;"></td></tr>				
                      <td align="center"><font size="2">
                        <input type="button" value="Celsius to Fahrenheit" onclick="tempconv(12)" style="height:2em; width:11em">
                        </font></td>
                      <td align="center" colspan="2"><font size="2">
                        <input type="button" value="Fahrenheit to Celsius" onclick="tempconv(24)" style="height:2em; width:11em">
                        </font></td>
                      <td align="center"><font size="2">
                        <input type="button"  value="Celsius to Kelvin" onClick="tempconv(11)" style="height:2em; width:11em">
                        </font></td>
                    </tr>
                    <tr>
                      <td colspan="4" align="center"> <input type="reset" value="Clear" style="height:2em; width:11em"></td>
                    </tr>
					<tr><td colspan="4" style="height:15px;"></td></tr>
                  </table>
				  </center>
                </form>
                <!--------------temp end----------------------->
              </div>
              <div class="calc_right">
                <h5>Temperature</h5>
                <p>Temperature can be defined as the degree of hotness or coldness of a body or environment. A concept related to the flow of heat from one object or 
                  region of space to another. It is a measure of the average energy of the molecules of a body.A thermometer is a device used to measure temperature. 
                  Galileo invented the first documented thermometer in about 1592.</p>
                <div class="calc_foot">
                  <p class="return-to-top">Return to Top</p>
                </div>
              </div>
            </div>
          </div>
          <div id="content-for-j" style="background-color:#fff;clear:both;" class="calc_main">
            <div class="calc_content">
              <div class="calc_left">

                <!---------------heat sink---start------------------->
                <form method="POST" name="form" id="heat" action="">
					<center>
					 <table align="center" style="margin-top:.3cm" class="capi">
					<tr><th colspan="2">Enter your values:</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Ambient Temperature T<sub>a max</sub>: </b></font> </td>
                      <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>
                        <input type="text" name="Ta" size="7" maxlength="15">
                        C</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Junction Temperature T<sub>j max</sub>:</b></font> </td>
                      <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>
                        <input type="text" name="TJmax" size="7" maxlength="15">
                        C</b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistance (junction to case) R:</b></font> </td>
                      <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>
                        <input type="text" name="Rcase" size="7" maxlength="15">
                        C/Watt </b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistance  R<sub>a</sub>:</b></font> </td>
                      <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>
                        <input type="text" name="R1" size="7" maxlength="15">
                        C/Watt </b></font> </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistance  R<sub>b</sub>:</b></font> </td>
                      <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>
                        <input type="text" name="R2" size="7" maxlength="15">
                        C/Watt </b></font> </td>
                    </tr>

                    <tr>
                      <td colspan="2" align="center"><input type="reset" value="Clear"></td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
					<tr><th colspan="2">Result</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
                    <tr>
                      <td  align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Junction Temperature:</b></font></td>
                     <td align="left">   <input type="text" name="TJ" size="7" maxlength="15">
                       <font size="2" face="verdana"
        color="#1f6fa2"><b> C </b></font>
                        <input type="Button" value="Calculate" onClick="ComputeHeat1(this.form)" style="float:right;" class="calculate_cal">
                      </td>
                    </tr>
                    <tr>
                      <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Power: </b></font></td>
               
 	                    <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b><input type="text" name="P" size="7" maxlength="15"> Watt</b></font>
                        <input type="Button" value="Calculate" onClick="ComputeHeat2(this.form)" style="float:right;" class="calculate_cal">
                      </td>
                    </tr>
                    <tr>
						<td align="right"></td>
                      <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>
                        <input type="text" name="btu" size="7" maxlength="15">
                        BTU / Hour</b></font> </td>
                    </tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>					
                  </table>
				  </center>
				  
                </form>
                <!-----------heat sink end-------------------------->
              </div>
              <div class="calc_right">
                <h5>Heat Sink Temperature</h5>
                <p>This calculator computes the junction temperature of a power electronics device given the maximum ambient temperature, and the heat sink thermal 
                  resistance. The calculator will let you know if the maximum junction temperature could be exceeded for a given power, and likewise will tell you the maximum power 
                  that the device can handle for a given maximum power.</p>
                <div class="calc_foot">
                  <p class="return-to-top">Return to Top</p>
                </div>
              </div>
            </div>
          </div>
        </div>
		</div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL1 li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL1 li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#fff'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL1 li").each(function() { 
			$(".firstUL1 li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#fff'}); //set the background color to light-blue to that div
	 });
});
</script>
<!--script for Skin Effect-->
<script language="javascript">

function sknf_calc(form) {
f_base=document.getElementById("sknfText1").value
f_mult=document.getElementById("sknfSelect1").value
d_mult=document.getElementById("sknfSelect2").value


freq=f_base*f_mult 
dpen=7.6/Math.sqrt(freq)*d_mult


document.getElementById("sknfText2").value=dpen.toPrecision(3)
}

function sknf_save(){
	userCookie.put("sknfText1", document.getElementById("sknfText1").value)
	
	userCookie.put("sknfSelect1", document.getElementById("sknfSelect1").value)
	userCookie.put("sknfSelect2", document.getElementById("sknfSelect2").value)
	userCookie.write()
}

userCookie  = new cookieObject("sknf_form", 365, "/", "sknfText1", "sknfSelect1", "sknfSelect2")
if (userCookie.found) {
	document.getElementById("sknfText1").value=userCookie.get("sknfText1")
	document.getElementById("sknfSelect1").value=userCookie.get("sknfSelect1")
	document.getElementById("sknfSelect2").value=userCookie.get("sknfSelect2")
}
else {
	window.status=window.status+" cookie not found"
}

sknf_calc(form)
window.onunload=function() { sknf_save() }
</script>
<!--script for Skin Effect ends-->
<!--script for RF Amplifier -->
<script language="JavaScript">

function Validate(form) {
	if(!form.S11Re.value || !form.S11Im.value) {
		alert("Enter Value for S11.");
		form.S11Re.focus();
		return false;
	}
	if(!form.S12Re.value || !form.S12Im.value) {
		alert("Enter Value for S12.");
		form.S12Re.focus();
		return false;
	}
	if(!form.S21Re.value || !form.S21Im.value) {
		alert("Enter Value for S21.");
		form.S21Re.focus();
		return false;
	}
	if(!form.S22Re.value || !form.S22Im.value) {
		alert("Enter Value for S22.");
		form.S22Re.focus();
		return false;
	}
	if(!form.Zsr) {
		alert("Enter a value for the real part of the source resistance.");
		form.Zsr.focus();
		return false;
	}
	if(!form.Zlr) {
		alert("Enter a value for the real part of the load resistance.");
		form.Zlr.focus();
		return false;
	}
	if(!form.Freq) {
		alert("Enter a the operating frequency.");
		form.Freq.focus();
		return false;
	}	
	return true;
}

function Complex(r,i) {
  this.r= r
  this.i= i
}

function Polar(m,a) {
  this.m= m
  this.a= a
}

function SeriesToParallel(c1) {
	if(!c1.i) return new Complex(c1.r,1000000000000000000);
	var Q= c1.i/c1.r;
	var Rp= (Q*Q+1)*c1.r;
	var Xp= Rp/Q;
	return new Complex(Rp,Xp); 
}

function Ang(c1) { 
	var a= Math.atan(c1.i/c1.r)/Math.PI*180; 
	// figure out the quadrant
	if(c1.r>=0 && c1.i>=0) return a;     
	if(c1.r<=0 && c1.i>=0) return 180-a; 	
	if(c1.r<=0 && c1.i<=0) return 180+a; 
	if(c1.r>=0 && c1.i<=0) return a;
}

function PolarToRect(p1) {  
	var r= Math.cos(p1.a/180*Math.PI)*p1.m;
	var i= Math.sin(p1.a/180*Math.PI)*p1.m;
	return new Complex(r,i); 
}

function PolarToRect2(m,a) {  
	var r= Math.cos(a/180*Math.PI)*m;
	var i= Math.sin(a/180*Math.PI)*m;
	return new Complex(r,i); 
}

function RectToPolar(c1) {
	return new Polar(Mag(c1),Ang(c1));
}
function Add(c1,c2) { return new Complex(c1.r+c2.r,c1.i+c2.i); }
function Subt(c1,c2) { return new Complex(c1.r-c2.r,c1.i-c2.i); }
function Mult(c1,c2) { return new Complex(c1.r*c2.r-c1.i*c2.i, c1.i*c2.r+c1.r*c2.i); }
function Conj(c1,C2) { return new Complex(c1.r,-c1.i); }

function MagSq(c1) {
	 T1= Mult(c1,Conj(c1)); 
	 return T1.r
}
function Mag(c1)	{ return Math.sqrt(MagSq(c1)); }
function Div(c1,c2) { 
	T1= Mult(c1,Conj(c2));
	T2= Mult(c2,Conj(c2)); 
	return new Complex(T1.r/T2.r,T1.i/T2.r);
}
function Neg(C1) { return new Complex(-C1.r,-C1.i); }

function Log10(num) {
	return Math.log(num)/ Math.log(10);
}

function LMatchImpedance(Z1,Z2,block,w) {
	with(Math){
		var Meg= 1000000;
		var Kilo= 1000;
		var Zp;
		var Zs;
		var Q;
		var Z1IsPar;
		Zp= SeriesToParallel(Z1);
		if(Zp.r/Z2.r>=1) {
			Zs= Z2;
			Z1IsPar= true;
		} else {
			Zp= SeriesToParallel(Z2);
			Zs= Z1;
			Z1IsPar= false;
		}

		Q= sqrt(Zp.r/Zs.r-1);
		Xp= Zp.r/Q;  
		Xs= Q*Zs.r;  
		if(block) { 
			Xs=-Xs;
		} else {
			Xp=-Xp;
		}
		Xsa=  Xs- Zs.i; 
		Xpa= (Xp*Zp.i)/(Zp.i-Xp) ;	
		if(Xpa<0) {
			C= abs( 1/(w*Xpa)*Meg*Meg ); 
			L= abs( Xsa/w*Meg*Kilo ); 
		} else {
			C= abs( 1/(w*Xsa)*Meg*Meg ); 
			L= abs( Xpa/w*Meg*Kilo ); 
		}
	}
	return new Array(C,L,Q,Z1IsPar); 
}

function CalculateK(form) {
	if(!Validate(form)) return;
	with(Math) {
		var Meg= 1000000;
		var Kilo= 1000;
		one= new Complex(1,0);
		var Zo= Number(form.Zo.value);
		var Freq=  Number(form.Freq.value)*Meg; 
		var w= 2*PI*Freq;		
		ZoC= new Complex(Zo,0);
		var Precision= 3;
		Zs= new Complex(Number(form.Zsr.value),Number(form.Zsi.value));
		Zl= new Complex(Number(form.Zlr.value),Number(form.Zli.value));
		
		var Zl_block= true;
		var Zs_block= true;
		if(form.Snet[0].checked) {Zs_block= false;}
		if(form.Lnet[0].checked) {Zl_block= false;}
	
		
		if(form.Coordinates[0].checked) {
			S11= new Complex(Number(form.S11Re.value),Number(form.S11Im.value));
			S12= new Complex(Number(form.S12Re.value),Number(form.S12Im.value));
			S21= new Complex(Number(form.S21Re.value),Number(form.S21Im.value));
			S22= new Complex(Number(form.S22Re.value),Number(form.S22Im.value));
		} else {
			S11= PolarToRect2(Number(form.S11Re.value),Number(form.S11Im.value));
			S12= PolarToRect2(Number(form.S12Re.value),Number(form.S12Im.value));
			S21= PolarToRect2(Number(form.S21Re.value),Number(form.S21Im.value));
			S22= PolarToRect2(Number(form.S22Re.value),Number(form.S22Im.value));
		}
		

		Ds= Subt(Mult(S11,S22),Mult(S12,S21));
		var K= (1+ MagSq(Ds) - MagSq(S11)- MagSq(S22))/(2*Mag(S21)*Mag(S12));
		var B1= 1+ MagSq(S11)- MagSq(S22) - MagSq(Ds);
		var B2= 1+ MagSq(S22)- MagSq(S11) - MagSq(Ds);
	
		if(B1>0) MaGain= Mag(S21)/Mag(S12)*(K-sqrt(K*K-1));
		else MaGain= Mag(S21)/Mag(S12)*(K+sqrt(K*K-1));
		MaGainDb= 10*Log10(MaGain);
		
		C2= Subt(S22,Mult(Ds,Conj(S11)));
		GammaLp= new Polar(0,0);
		if(B2>0) GammaLp.m= Number( (B2-sqrt(B2*B2-4*MagSq(C2)))/(2*Mag(C2)));
		else GammaLp.m= Number( (B2+sqrt(B2*B2-4*MagSq(C2)))/(2*Mag(C2)) );
		
		C2p= RectToPolar(C2);
		GammaLp.a= Number(-C2p.a);
		
		GammaL= new PolarToRect(GammaLp);
		
		
		GammaS= Conj(Add(S11,Div(Mult(Mult(S12,S21),GammaL),Subt(one,Mult(GammaL,S22)))))
		GammaSp= RectToPolar(GammaS);
		
		Zsm= Mult(ZoC,Div(Add(one,GammaS),Subt(one,GammaS)));
		Zlm= Mult(ZoC,Div(Add(one,GammaL),Subt(one,GammaL)));	
		Zsm= Conj(Zsm);
		Zlm= Conj(Zlm);


		ar= LMatchImpedance(Zs,Zsm,Zs_block,w);
		Cs= ar[0];
		Ls= ar[1];
		Qs= ar[2];
		var AmpInIsPar= !ar[3];

		ar= LMatchImpedance(Zl,Zlm,Zl_block,w);
		Cl= ar[0];
		Ll= ar[1];
		Ql= ar[2];	
		var AmpOutIsPar= !ar[3];

		form.K.value= K.toPrecision(Precision);
		form.MaGainDb.value= MaGainDb.toPrecision(Precision);
		form.MaGain.value= MaGain.toPrecision(Precision);
		form.GammaLm.value= GammaLp.m.toPrecision(Precision);
		form.GammaLa.value= GammaLp.a.toPrecision(Precision);
		
		form.GammaSm.value= GammaSp.m.toPrecision(Precision);
		form.GammaSa.value= GammaSp.a.toPrecision(Precision);
		
		form.Zsmr.value= Zsm.r.toPrecision(Precision);
		form.Zsmi.value= Zsm.i.toPrecision(Precision);
		form.Zlmr.value= Zlm.r.toPrecision(Precision);
		form.Zlmi.value= Zlm.i.toPrecision(Precision);

		form.Qs.value= Qs.toPrecision(Precision);
		form.Ql.value= Ql.toPrecision(Precision);
		form.Cs.value= Cs.toPrecision(Precision);	
		form.Ls.value= Ls.toPrecision(Precision);			
		form.Cl.value= Cl.toPrecision(Precision);	
		form.Ll.value= Ll.toPrecision(Precision);		

		form.AmpInIsPar.checked= AmpInIsPar;
		form.AmpOutIsPar.checked= AmpOutIsPar;

	}
}

</script>
<!--script for RF Amplifier  ends-->
<!--script for IC 555 Timer -->
<script language="JavaScript">
<!--
var R1, R2, C, T1, T2, F, RT;

function Process(form)
{
  if(R1<1 || C<.0005) alert ("R1 should be > 1K and C > .005uF");
  if(R1>.99 && R2>0 && C>.0005) {
  RT = (R1*1)+(R2*1);
  T1 = 0.693 * RT * C;
  T1 = Math.round(T1*100000);
  T1 = T1/100000;
  T2 = 0.693 *R2 * C;
  T2 = Math.round(T2*100000);
  T2 = T2/100000;
  F = 1.44/(C* (  (R1*1)+(R2*2)  ));
  F = Math.round(F*10000000);
  F = F/10000000;
  form.T1.value = T1;
  form.T2.value = T2;
  form.F.value = F;
  }
}

function SetR1(R)  { R1 = R.value;}
function SetR2(R)  { R2 = R.value;}
function SetC(CC)   { C = CC.value; }

function ClearForm(form){

  R1=0; R2=0; C=0; T1=0; T2=0; F=0;
  form.R1.value = 0;  form.R2.value = 0;
  form.C.value = 0;   form.T1.value=0;
  form.T2.value=0;    form.F.value =0;
}
// -->
</script>
<!--script for IC 555 Timer  ends-->
<!--script for Pinhole Sizing -->
<script language="JavaScript">
function compute(form) {
 f = form.depth.value;
 A = form.hole.value;
 a = form.aperture.value;
 t = form.num.value / form.den.value;

 if (form.scale.value == "2") {f *= 1;}
 if (form.scale.value == "1") {f *= 10;}
 if (form.scale.value == "0") {f *= 25.4;}
			
imagesz = f * 1.3;
optdia = Math.round( Math.sqrt( 4 * 0.00000056 * f / 1000) * 10000) / 10;
if (form.hole.value == "0") {fstop = f / optdia;} else {fstop = f / A;}
fstop = Math.round(fstop);
exposure = Math.round(fstop * fstop / a / a);
exptime = t * exposure
expmin = Math.round((exptime/60)-0.5);
expsec = exptime - (expmin * 60);
 
 if (form.scale.value == "2") {
  imagesz /= 1
  form.imscale.value = "mm";
 }
 if (form.scale.value == "1") {
  imagesz /= 10;
  form.imscale.value = "cm";
 }
 if (form.scale.value == "0") {
  imagesz /= 25.4;
  form.imscale.value = "inches";
 }
 form.imagesz.value = Math.round(imagesz*10)/10;
 form.optdia.value = optdia;
 form.fstop.value = fstop;
 form.exposure.value = exposure;
 form.expmin.value = expmin;
 form.expsec.value = expsec;
}
   
</script>
<!--script for Pinhole Sizing  ends-->
<!--script for Zener Diode -->
<SCRIPT>
function calculate(zen) {
   vinMin = parseFloat(zen.VinMin.value)
   vinMax = parseFloat(zen.VinMax.value)
   vOut = parseFloat(zen.Vout.value)
   IL = parseFloat(zen.Iload.value)
   if((vOut+0.8) > vinMin) {
      alert("Minimum Input Voltage is too low!")
      return
   }
   R = round(1000 * ((vinMin - vOut)/(IL + 10)))
   Rp = round(Math.pow((vinMax - vOut), 2) / R)
   zen.R.value = R;
   zen.Rp.value = Rp;
   //zen.res.value = "  " + R + " Ohms " +", "+ Rp + " Watts"
   It = 1000 * ((vinMax - vOut) / R)
   Zp = round((It * vOut) / 1000)
    zen.vOut.value = vOut;
   zen.Zp.value = Zp;
   //zen.zen.value="  " + vOut + " Volts " +", "+ Zp + " Watts"
}
function round(n) {
   return parseInt((n * 100) + 0.5) /100
}
</SCRIPT>
<!--script for Zener Diode  ends-->
<!--script for Line Of Sight -->
<SCRIPT language=JavaScript>
<!--
function calc11(page) {
   mast1 = parseFloat(page.height1.value);
   mast2 = parseFloat(page.height2.value);
   if(page.units[1].checked) {
      mast1 = mast1/0.3048;
      mast2 = mast2/0.3048;
   }
   d1 = Math.sqrt(2*mast1);
   d2 = Math.sqrt(2*mast2);
   if(page.dist[1].checked) {
      d1 = parseFloat(d1*1.609);
      d2 = parseFloat(d2*1.609);
      unit = "   Km";
   }  else unit = "   Miles";
   d1 = Math.round(d1);
   d2 = Math.round(d2);
   page.horiz1.value = d1+unit;
   page.horiz2.value = d2+unit
   page.distance.value = Math.round(d1 + d2)+unit;
}
//-->
</SCRIPT>
<!--script for Line Of Sight  ends-->
<!--script for Helical Antenna -->
<script language="JavaScript">


function ComputeH(form) {
	with(Math) {
		var KILO= 1000;
		var CENTO= 100;
		var MEG= KILO*KILO;
		var MICRO= 1/MEG;
		var PICO= 1/(MEG*MEG);
		var NANO= 1/(MEG*KILO);
		var Precision= 3;
		
		var lambda= Number(form.lambda.value);
		var f= Number(form.f.value)*MEG;
		var N= Number(form.N.value);
		var SL= Number(form.SL.value);
		
		if(f>0 && !lambda) {
			lambda= 300000000/f;
			form.lambda.value= lambda.toPrecision(Precision);
		} else if(lambda>0){
			f= 300000000/lambda/MEG;
			form.f.value= f.toPrecision(Precision);
		}
		
		C= lambda;
		CL= (C/lambda);
		Z= 150/sqrt(CL);
		D= lambda/PI;
		S= SL*lambda;
		L= N*sqrt(C*C+SL*SL);  
		G= 10.8 + 10*log( CL*CL*N*SL )/LN10;
		
		HPBW= 52/( CL*sqrt(N*SL) );
		BWFN= 115/( CL*sqrt(N*SL) );
		
		var A= pow(10,G/10);
		Ae= A*lambda*lambda/(4*PI);
	
		D*= CENTO;
		S*= CENTO;
		L*= CENTO;
		
		form.Ae.value= Ae.toPrecision(Precision);		
		form.HPBW.value= HPBW.toPrecision(Precision);
		form.BWFN.value= BWFN.toPrecision(Precision);
		form.D.value= D.toPrecision(Precision);
		form.S.value= S.toPrecision(Precision);	
		form.Z.value= Z.toPrecision(Precision);	
		form.G.value= G.toPrecision(Precision);
		form.L.value= L.toPrecision(Precision);		
	}
}



</script>
<!--script for Helical Antenna  ends-->


<!--script for Single Layer Coil -->
<script LANGUAGE="JavaScript" type="text/javascript">
<!--
var L = 0;
var LM = 0;
var D = 0;
var DM = 0;
var d =0;
var dM = 0;
var l = 0;
var lM = 0;
var N = 0;

function clearBoxes(form)
{
form.L.value = "";
form.D.value = "";
form.d.value = "";
form.l.value = "";
form.N.value = "";
form.L.focus();
}

function solvesingle(form)
{
if(!form.L.value) {alert("\nMissing a value. L value is required!\n"); return;}
else L = eval(form.L.value);
if(L == 0){alert("\nPlease enter L other than zero!\n"); return;}

if(!form.D.value) {alert("\nMissing a value. D value is required!\n"); return;}
else D = eval(form.D.value);
if(D == 0){alert("\nPlease enter D other than zero!\n"); return;}

if(!form.d.value) {alert("\nMissing a value. d value is required!\n"); return;}
else d = eval(form.d.value);
if(d == 0){alert("\nPlease enter d other than zero!\n"); return;}

LM = eval(form.LM.value);
DM = eval(form.DM.value);
dM = eval(form.dM.value);
lM = eval(form.lM.value);

L=L*LM;
D=D*DM;
d=d*dM;

N = ((20*d*L)/(D*D))+Math.sqrt((400*Math.pow((d*L)/(D*D),2))+(18*L)/D);

form.N.value = N;

l = d*N;

form.l.value = l/lM;

form.L.focus();

return;
}
-->
</script>
<!--script for Single Layer Coil  ends-->


<!--script for Temperature -->
<script type="text/javascript">
function tempconv (zog) {
        var num1 , num2

        if (zog==1) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*33.90
                document.tempsize.tempto.value=num1
        }

        if (zog==2) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*14.50
                document.tempsize.tempto.value=num1
        }

        if (zog==3) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*1.133
                document.tempsize.tempto.value=num1
        }

        if (zog==4) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.6720
                document.tempsize.tempto.value=num1
        }

        if (zog==5) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*14.22
                document.tempsize.tempto.value=num1
        }

        if (zog==6) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.003281
                document.tempsize.tempto.value=num1
        }

        if (zog==7) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*6.8948
                document.tempsize.tempto.value=num1
        }

        if (zog==8) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*2.307
                document.tempsize.tempto.value=num1
        }

        if (zog==9) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*2.036
                document.tempsize.tempto.value=num1
        }

        if (zog==10) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*27.677
                document.tempsize.tempto.value=num1
        }

        if (zog==11) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2+273
                document.tempsize.tempto.value=num1
        }

        if (zog==12) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=(num2+17.78)*1.8
                document.tempsize.tempto.value=num1
        }

        if (zog==13) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.0295
                document.tempsize.tempto.value=num1
        }

        if (zog==14) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.8826
                document.tempsize.tempto.value=num1
        }

        if (zog==15) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*304.8
                document.tempsize.tempto.value=num1
        }

        if (zog==16) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.4335
                document.tempsize.tempto.value=num1
        }

        if (zog==17) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.491
                document.tempsize.tempto.value=num1
        }

        if (zog==18) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.03613
                document.tempsize.tempto.value=num1
        }

        if (zog==19) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*1.488
                document.tempsize.tempto.value=num1
        }

        if (zog==20) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.0690
                document.tempsize.tempto.value=num1
        }

        if (zog==21) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.07031
                document.tempsize.tempto.value=num1
        }

        if (zog==22) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.1450
                document.tempsize.tempto.value=num1
        }

        if (zog==23) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=(num2/2)+254.92
                document.tempsize.tempto.value=num1
        }

        if (zog==24) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=(num2-32)*0.5556
                document.tempsize.tempto.value=num1
        }

        if (zog==99) {
                document.tempsize.tempfrom.value=document.tempsize.tempto.value
                document.tempsize.tempto.value=""
        }
}



</script>
<!--script for Temperature  ends-->
<!--script for Heat Sink Temperature  -->
<script language="JavaScript">



function ComputeHeat2(form) {
	with(Math) {
		var Precision= 3;
		
		var Ta= Number(form.Ta.value);
		var TJmax= Number(form.TJmax.value);
		var Rcase= Number(form.Rcase.value);
		var R1= Number(form.R1.value);
		var R2= Number(form.R2.value);
		var TJ= Number(form.TJ.value);
		
		
		P= (TJ-Ta)/(Rcase+R1+R2);
		
		
		
		form.P.value= P.toFixed(Precision);
		
		btu = P * 3.412;
		
		form.btu.value = btu.toFixed(Precision);
	}
}

function ComputeHeat1(form) {
	with(Math) {
		var Precision= 3;
		
		var Ta= Number(form.Ta.value);
		var TJmax= Number(form.TJmax.value);
		var Rcase= Number(form.Rcase.value);
		var R1= Number(form.R1.value);
		var R2= Number(form.R2.value);
		var P= Number(form.P.value);
		
		TJ= P*(Rcase+R1+R2)+Ta;
		

		form.TJ.value= TJ.toFixed(Precision);
	}
}

</script>
<!--script for Heat Sink Temperature   ends-->
