
<?php
$groupswithaccess="ladmin,luser,leditor";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");

include("include.php");  // read css and js folders, sets database variables
session_start();

$table=$_GET['TBL'];
$_SESSION['table']=$table;



include("header.php");

?>

<link rel="stylesheet" type="text/css" href="../css/sortstyle.css">

<style type="text/css" class="include" title="currentStyle">
    @import "DataTables-1.10.0/media/css/jquery.dataTables.css";
    @import "DataTables-1.10.0/extensions/Editor-1.3.0/css/dataTables.editor.css";


    td.details-control {
        background: url('images/editpage.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('images/closefile.png') no-repeat center center;
    }
    TD {font-size:10px}


</style>
<link href="../css/popup.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"> </script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/popup1.js"></script>

 
  <script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/media/js/jquery.js"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/media/js/jquery.dataTables.js"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/Editor-1.3.0/js/dataTables.editor.js"></script>
<script type="text/javascript" charset="utf-8">
var editor;


    $(document).ready(function() {
	 var tbl = '<?php echo $table; ?>';

        editor = new $.fn.dataTable.Editor( {
		    
            "ajax": "vc_datatest.php",
			"table": "#view",
			"fields": [
			   {
                    "label": "Part Number",
                    "name": "new_part.Part_Number"
                    
                },
                {
                     
                     "label": "PN",
                     "name": "new_part.PN"
                    
                 },
				
                {
                    "label": "Description",
                    "name": "new_part.Description"
                },
               
                {
                    "label": "Manufacturer",
                    "name": "new_part.Manufacturer"
                },
                {
                    "label": "Manufacturer PN",
                    "name": "new_part.Manufacturer_Part_Number"
                },
				 {
                    "label": "Category",
                    "name": "new_part.table_name"
                },
                
               {
                "label": "Edit Status",
                "name": "new_part.status",
                "type": "select",
                "ipOpts": [
                    { "label": "GREEN", "value": "green" },
                    { "label": "RED", "value": "red" },
                    { "label": "YELLOW", "value": "yellow" },
                    { "label": "BLUE", "value": "blue" }
                ]
            }
				
            ]
			
			
			
			
			
        } );

	

        // New record
        $('a.editor_create').on('click', function (e) {
            e.preventDefault();

           editor
                .title( 'Create new record' )
                .buttons( { "label": "Add", "fn": function () { editor.submit();
                   } } )
                .create();

        } );

        // Edit record
        $('#view').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();

            editor
                .title( 'Edit record' )
                .buttons( { "label": "Update", "fn": function () { editor.submit() } } )
                .edit( $(this).closest('tr') );
        } );

        // Delete a record (without asking a user for confirmation for this example)
        $('#view').on('click', 'a.editor_remove', function (e) {
            e.preventDefault();

            editor
                .title( 'Delete record' )
                .message( 'Are you sure you wish to remove this record?' )
                .buttons( { "label": "Delete", "fn": function () { editor.submit() } } )
                .remove( $(this).closest('tr') );
        } );

        // DataTables init
		 var fa = 0;
       var table = $('#view').DataTable( {              "sScrollY": "300px",
               dom: "lfrtip",
           "bAutoWidth": false,
                "bProcessing": true,
				 "aLengthMenu": [[10, 50, 100, 500], [10, 50, 100, 500]],
                //"bPaginate": false,
               // "bAutoWidth": false,
				
              ajax: {
               url: "vc_datatest.php",
               type: "POST"
                },
                
                "columns": [
				
				{
                "bSearchable": false,
                "bSortable": false,
                "mRender":function(data, type, full)
                {
                    if ( full.new_part.status === 'yellow')
                    {
                        return '<center><img src="../images/yellow.png"></center>';
                    }
                    else if(full.new_part.status === 'green')
                    {
                        return '<center><img src="../images/green.png" ></center>';
                    }
					  else if(full.new_part.status === 'red')
                    {
                        return '<center><img src="../images/red.png"></center>';
                    }
                }

            },

                {
                "data": "new_part.Part_Number"
                
                },

               
                {
                    "data": "new_part.PN"
                },
               
              
                {
                    "sClass": "center",
                    "data": "new_part.Description"
                },
                {
                    "sClass": "center",
                    "data": "new_part.Manufacturer"
                },
                {
                    "sClass": "center",
                    "data": "new_part.Manufacturer_Part_Number"
                },
				  {
                    "sClass": "center",
                    "data": "new_part.table_name"
                },
                             
              
				
			{
                    "bSearchable": false,
                    "bSortable": false,
                    "mRender":function(data, type, full)
                    {
                        
                            return '<center><a href="" class="editor_edit"><img src="pencil.png" width="16"></a></center>';
                       
                    }

                }
              
                ]
			} );
} );
       
</script>
</head>
<br/>
<div class="status_panel">
	<div class="status_sec">

					<p>Logged in User - <strong><?php echo $slname; ?></strong> 	&nbsp;&nbsp;&nbsp; User Status- <strong><?php if ( $slgroupname[0] === 'ladmin'){?>Admin<?php } else{?>Basic<?php }?></strong>&nbsp;&nbsp;&nbsp; 	Current Page - <strong>Submitted Part </strong></p>


	</div>
</div>
<center>
<table border="0" width="100%" cellpadding="2" cellspacing="2" style="float:left;">
	<tr>

		  <td align="center" colspan="3"><a href="submitted_parts_menu.php" class="menu_click">Return</a>		</td>

	 </tr>
	
</table>
</center>

<center><br />


</center>
<center>
<table border="2" cellspacing="0" cellpadding="2" class="display dataTable" id="view" >
<thead>
	<tr> 
		<th>Status</th>
	    <th >Part Name</th>
		<th >PN</th>	
		<th >Description</th>
		<th >Manufacturer</th>
		<th >Manufacturer PN</th>
		<th>Category</th>
			
		<th>Edit </th>
	</tr>
</thead>

</table>
</center>


	
	 

	<div class="loader"></div>
	<div id="backgroundPopup1"></div>


	




<?php
include("footer.php");
?>

 