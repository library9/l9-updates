<?php
$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");


include("include.php");  // read css and js folders, sets database variables
session_start();

$table=$_GET['TBL'];
$_SESSION['table']=$table;


include("header.php");
include("sidebar.php");

?>

<link rel="stylesheet" type="text/css" href="../css/sortstyle.css">

<style type="text/css" class="include" title="currentStyle">

    @import "DataTables-1.10.0/extensions/Editor-1.3.0/css/dataTables.editor.css";


    td.details-control {
        background: url('images/editpage.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('images/closefile.png') no-repeat center center;
    }
    TD {font-size:10px}


</style>
<link href="css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="css/dataTables.jqueryui.css" type="text/css" rel="stylesheet">
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/TableTools/js/dataTables.tableTools.js"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/TableTools/js/ZeroClipboard.js"></script>
<script src="js/jquery-1.11.1.min.js" language="javascript" type="text/javascript"></script> 
<script src="js/jquery.dataTablesonline.min.js" language="javascript" type="text/javascript"></script>
<script src="js/dataTables.jqueryui.js" language="javascript" type="text/javascript"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/Editor-1.3.0/js/dataTables.editor.js"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    $('#view').DataTable({
	"sdom": 'T<"clear"><"H"lfr>t<"F"ip>',
	  "bInfo" : false,
	 "bPaginate": false
	
	
	});
} );
</script>
</head>
		<div class="side_work">	
    <!-- working area -->
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">
 
			<div class="statusbar"><p>Current Page - <strong>Files / Download</strong></p></div>
			<div class="returnstat"><a href="index.php" class="menu_click">Return</a></div>

	</div>
</div>

<link rel="stylesheet" type="text/css" href="../css/sortstyle.css">
<div class="app_content">
<div class="download_panel"> 
 <div class="script_layout">
<table cellpadding="0" cellspacing="0" id="view" class="display" >
    <thead><tr>
			<th style="color:#000;">Filename</th>
			<th nowrap="nowrap" style="color:#000;">Description</th>
			
			<th style="color:#000;">Download</th>
			</tr>
	</thead>
	<tbody>     
      <tr>
        <td style="color:#000;">MySql ODBC Connector</td>
        <td>Open source exectuable to link the operating system (OS) to databases.</td>
		
        <td><a href="<?php siteloklink('mysql-connector-odbc-3.51.30-win32.msi:files',1); ?>"><img src="images/download_icon.png" /></a></td>
      </tr>
	  <?php if ( $slgroupname[0] === 'ladmin' ){?>
	  <tr>
        <td style="color:#000;">Import Template</td>
        <td>Example of column/row format for correct importing.</td>
		
        <td><a href="<?php siteloklink('import.xls:files',1); ?>"><img src="images/download_icon.png" /></a></td>
      </tr>
	  <?php }?>
	  <tr>
        <td style="color:#000;">ODBC2L9(Win 7 & Win 8 (32 bit))</td>
        <td>Library9.com custom program to automate the setup of ODBC link to L9 database.</td>
		
        <td><a href="<?php siteloklink('ODBC_'.$slcustom15.'.zip:zip',1); ?>"><img src="images/download_icon.png" /></a></td>
      </tr>
	  <tr>
        <td style="color:#000;">Database Library File</td>
        <td>This file is the link between the SQL database and Altium.  This file needs to be in the same folder as the PCB and SCH library files.</td>
		
        <td><a href="<?php siteloklink(''.$slcustom15.'/'.$slcustom15.'.DbLib:files',1); ?>"><img src="images/download_icon.png" /></a></td>
      </tr>
	  <tr>
        <td style="color:#000;">Data Backup SQL</td>
        <td> An SQL format based file containing the data (parameter fields) of the library.</td>
		
        <td><a href="<?php siteloklink(''.$slcustom15.'/'.$slcustom15.'db-backup.sql:files',1); ?>"><img src="images/download_icon.png" /></a></td>
      </tr>
	    <tr>
        <td style="color:#000;">Schematic Library File</td>
        <td>Library file containing the shcematic symbols for the library.  This file needs to be in the same folder as the PCB library file and Database library file.</td>
		
        <td><a href="<?php siteloklink(''.$slcustom15.'/'.$slcustom15.'.SchLib:files',1); ?>"><img src="images/download_icon.png" /></a></td>
      </tr>
	  <tr>
        <td style="color:#000;">PCB Library File</td>
        <td> Library file containing the footprints for the library.  This file needs to be in the same folder as the SCH library file and Database library file.</td>
		
        <td><a href="<?php siteloklink(''.$slcustom15.'/'.$slcustom15.'.PcbLib:files',1); ?>"><img src="images/download_icon.png" /></a></td>
      </tr>
	   <tr>
        <td style="color:#000;">BOM Template</td>
        <td>Example of column/row format for correct importing.</td>
		
        <td><a href="<?php siteloklink('bom.xls:files',1); ?>"><img src="images/download_icon.png" /></a></td>
      </tr>
		  
	  
	  </tbody>
</table>




	<?php /*?><ul>
         <li> <a href="<?php siteloklink('mysql-connector-odbc-3.51.30-win32.msi:files',1); ?>">MySql ODBC Connector</a>	</li>
			<li><a href="<?php siteloklink(''.$slcustom15.'.zip:zip',1); ?>">ODBC2L9(Win 7 only)</a></li>
		  <li> <a href="<?php siteloklink(''.$slcustom15.'/'.$slcustom15.'.DbLib:files',1); ?>">Database Library File</a></li>
		  <li> <a href="<?php siteloklink(''.$slcustom15.'/'.$slcustom15.'db-backup.sql:files',1); ?>">Data Backup SQL</a></li>
		  <li> <a href="<?php siteloklink(''.$slcustom15.'/'.$slcustom15.'.SchLib:files',1); ?>">Schematic Library File</a>	</li>
		  <li>  <a href="<?php siteloklink(''.$slcustom15.'/'.$slcustom15.'.PcbLib:files',1); ?>">PCB Library File</a>	</li>
		  <li style=" margin-top:50px;"><a href="http://library9.com/members/admin.php">Back</a></li>
	</ul>	<?php */?>	
		   

</div>	</div>		
       																																						  <!-- end table -->




</div>
</div>
</div>
<?php

if(!is_dir('docs/'.$slcustom15)){
mkdir('docs/'.$slcustom15);
}if($slcustom1=='basic'){
copy('docs/basic.DbLib', 'docs/'.$slcustom15.'/'.$slcustom15.'.DbLib');
/* copy('docs/custom.DbLib', 'docs/'.$slcustom15.'/'.$slcustom15.'.DbLib');
include 'include.php';
$cat = array("capacitor","connector","diode","ic","misc","oscillator","resistor","transistor");

$txt1='';
$J=1;
for($J=1;$J<=sizeof($cat);$J++){
$value1=$cat[$J-1];
 
$txt1.="
[Table".$J."]
SchemaName=
TableName=".$value1."
Enabled=True
UserWhere=0
UserWhereText=
BrowserOrder_Sorting=
BrowserOrder_Grouping=";

 }

$myfile=fopen('docs/'.$slcustom15.'/'.$slcustom15.'.DbLib', "a");
fwrite($myfile, $txt1);



$txt='';
$i=1;
$k=1;
for($i=1;$i<=sizeof($cat);$i++){
$value=$cat[$i-1];

$columns = array("Part Number", "CPN", "Library Ref","Library Path","Description", "Manufacturer"," Manufacturer Part Number", "Footprint Path","Footprint Ref","Value");
if($value=='capacitor'){array_push($columns,"Tolerance","Voltage Rated","Dielectric Characteristic");}
if($value=='connector'){array_push($columns,"Mounting Type");}
if($value=='diode'){array_push($columns,"Voltage Reverse");}
if($value=='ic'){array_push($columns,"Mounting Type");}
if($value=='misc'){array_push($columns,"Mounting Type");}
if($value=='oscillator'){array_push($columns,"Mounting Type");}
if($value=='resistor'){array_push($columns,"Tolerance");}

for($k=1;$k<=sizeof($columns);$k++){
 $row=$columns[$k-1];
 if($row=='Part Number' ||$row=='Library Ref'||$row=='Description'||$row=='Footprint Ref'){
 $txt.="
[FieldMap".$i."]
Options=FieldName=".$value.".".$row."|TableNameOnly=".$value."|FieldNameOnly=".$row."|FieldType=1|ParameterName=[".$row."]|VisibleOnAdd=False|AddMode=0|RemoveMode=0|UpdateMode=0";
 }
 else{
$txt.="
[FieldMap".$i."]
Options=FieldName=".$value.".".$row."|TableNameOnly=".$value."|FieldNameOnly=".$row."|FieldType=1|ParameterName=[".$row."]|VisibleOnAdd=False|AddMode=1|RemoveMode=0|UpdateMode=0";
}

}

 


 }

$myfile=fopen('docs/'.$slcustom15.'/'.$slcustom15.'.DbLib', "a");
fwrite($myfile, $txt); */

}
else if($slcustom1=='advance'){


copy('docs/custom.DbLib', 'docs/'.$slcustom15.'/'.$slcustom15.'.DbLib');
include 'include.php';
$cn=mysql_connect($host, $username, $password)or die("cannot connect");
$db1=mysql_select_db($slcustom15) or die( "Unable to select database");
 $sql1=mysql_query("select cat_name FROM categories")or die(mysql_error()."update failed");


$txt1='';
$J=1;
while($table=mysql_fetch_array($sql1)){
$value1=$table[0];
 
$txt1.="
[Table".$J."]
SchemaName=
TableName=".$value1."
Enabled=True
UserWhere=0
UserWhereText=
BrowserOrder_Sorting=
BrowserOrder_Grouping=";
$J=$J+1;
 }

$myfile=fopen('docs/'.$slcustom15.'/'.$slcustom15.'.DbLib', "a");
fwrite($myfile, $txt1);

 $sql1=mysql_query("select cat_name FROM categories")or die(mysql_error()."update failed");


$txt='';
$i=1;
while($table=mysql_fetch_array($sql1)){
$value=$table[0];
$sql=mysql_query("SHOW COLUMNS FROM $value")or die(mysql_error()."update failed");

while($row=mysql_fetch_array($sql)){
  $row[0];
  if($row[0]=='Part Number' ||$row[0]=='Library Ref'||$row[0]=='Description'||$row[0]=='Footprint Ref'||$row[0]=='ITEM'){
  $field_type = ($row[0] == 'Part Number') ? "0" : "1";
    $txt.="
    [FieldMap".$i."]
    Options=FieldName=".$value.".".$row[0]."|TableNameOnly=".$value."|FieldNameOnly=".$row[0]."|FieldType=".$field_type."|ParameterName=[".$row[0]."]|VisibleOnAdd=False|AddMode=0|RemoveMode=0|UpdateMode=0";
  }
  else{
  $txt.="
  [FieldMap".$i."]
  Options=FieldName=".$value.".".$row[0]."|TableNameOnly=".$value."|FieldNameOnly=".$row[0]."|FieldType=1|ParameterName=".$row[0]."|VisibleOnAdd=False|AddMode=2|RemoveMode=0|UpdateMode=0";
  }
  $i=$i+1;
}

 


 }

$myfile=fopen('docs/'.$slcustom15.'/'.$slcustom15.'.DbLib', "a");
fwrite($myfile, $txt);
}
$str1=implode("",file('docs/'.$slcustom15.'/'.$slcustom15.'.DbLib'));

$fp1=fopen('docs/'.$slcustom15.'/'.$slcustom15.'.DbLib','w');

//replace something in the file string - this is a VERY simple example   
$str1=str_replace("ESC001", "l9".$slcustom15."",$str1);

fwrite($fp1,$str1,strlen($str1));
fclose($fp1);
if(!file_exists('docs/'.$slcustom15.'/'.$slcustom15.'.SchLib')){
copy('docs/basic.SchLib', 'docs/'.$slcustom15.'/'.$slcustom15.'.SchLib');
}
if(!file_exists('docs/'.$slcustom15.'/'.$slcustom15.'.PcbLib')){
copy('docs/basic.PcbLib', 'docs/'.$slcustom15.'/'.$slcustom15.'.PcbLib');
}
?>
<script>
$( document ).ready(function() { 

 var com_id='<?php echo $slcustom15; ?>';
     
          var req=  $.ajax({
                url : 'export.php',
                type : 'POST',
                async: false,
                data : { 'com_id' : com_id},
                success : function(result){
		
             },
             error : function(e){
             console.log(e);
            }
             });
  



 });
 </script>
