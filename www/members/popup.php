<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.css">
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.js"></script>
</head>
<body>

<div data-role="page">
 <?php include('header.php'); ?>
  <div data-role="main" class="ui-content">
    <a href="#myPopup" data-rel="popup" class="ui-btn ui-btn-inline ui-corner-all">Show Popup</a>

    <div data-role="popup" id="myPopup" class="ui-content">
     <?php

$groupswithaccess="ladmin,luser";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include("include.php");  																																														// read css and js folders, sets database variables

$table=$_GET['TBL'];
																																																		// get passed variable TBL from submit post, call it $table
$rcpn=$_GET['RCPN'];

$PartNumber = $_POST['partnumber'];																																										// read posted variables from submit post, assign string names
$PartName = $_POST['partname'];																																												// ******************                NOTES              ***********************
$LibRef = $_POST['symbol'];																																														// If you are wondering why there are passed variables coming
$Val = $_POST['value'];																																																	// into the add part page of  "", this is because,  I  reread the
$Desc = $_POST['desc'];																																																// users input into the add part page if the user tries to add a part
$Footprint = $_POST['footprint'];																																													// and it exists,  the system will repost all the entered data
$Manu = $_POST['manu'];																																																// back into the add part input fields.  However if coming into 
$ManuPN = $_POST['manu_pn'];																																												// add parts page and there is no data passed from a post,
$Datasheet = $_POST['datasheet'];


$rcarray1=array("resistor","capacitor");

mysql_connect($host,$username,$password);																																						// connect to MySQL with credentials from demo-include.php
@mysql_select_db($database) or die( "Unable to select database");
if(in_array($table, $rcarray1))
{
   $partnumber = $rcpn;
}
else																																								// the systems errors, so blank data ("") is passed initially.
{
    																												// open database $database from demo-include.php

    $sql = mysql_query("SELECT DISTINCT PN FROM $table ORDER BY PN DESC LIMIT 1");
    while ($row=mysql_fetch_array($sql))
    {
        $name1=$row["PN"];     // get three digit prefix from part number
 // echo $name1;																																														 // get last part number from table
        $precc = substr($name1, 0, -3);     // get three digit prefix from part number
////        // echo $precc;
        $postcc = substr($name1, -3); 																																											     // get five digit suffix from part number
////        // echo $postcc;
        $postcc1 = (int)$postcc;  																																			// turn string into integer
////        // echo $postcc1;
        $postcc2 = $postcc1 + 1; 																																															 // increment part number
////        // echo $postcc2;
        $postccd = strlen($postcc2);   																																													 // figure out how many charaters are in the part number
////        // echo $postccd;
////
        if ($postccd == 1)
        {
            $partnumber = $precc . "00" . $postcc2;  																																						     	// three digit part number, add two zeros
        }
        if ($postccd == 2)
        {
            $partnumber = $precc . "0" . $postcc2; 																																								// four digit part number, add one zero
        }
		    }
}


   $sql = mysql_query("SELECT DISTINCT `Footprint_Ref` FROM $table ORDER BY `Footprint_Ref` ASC");												//query database for all distinct footprints order ascending by footprint name
	$options ='<OPTION VALUE=$Footprint></OPTION>';																																		// make string footprint the default option
	while ($row=mysql_fetch_array($sql)) 																																									// get data row
		{ 
		$name=$row["Footprint_Ref"]; 																																												// get footprint from row data																																												
		$options .='<OPTION VALUE="'.$name.'">'.$name.'</OPTION>'; 																												// add footprint to pull-down data
		}

	$sql = mysql_query("SELECT DISTINCT `Library_Ref` FROM $table ORDER BY `Library_Ref` ASC");														//query database for all distinct symbols order ascending by symbol name
	$optionsa ='<OPTION VALUE=$LibRef></OPTION>';
	while ($row=mysql_fetch_array($sql)) 
		{
		$name2=$row["Library_Ref"];	
		$optionsa .='<OPTION VALUE="'.$name2.'">'.$name2.'</OPTION>'; 
		}

mysql_close();

?>

<h2>User- <?php echo $slname; ?></h2>

<br/>
<br/>
<center>
<table border="0" width="50%" cellpadding="2" cellspacing="2">
	<tr>
	<td>Enter URL for Part Number </td> <td><input type="text" name="url" id="input" />&nbsp;&nbsp;
	<input type="button"  value="Get Values" id="url"/></td>
</tr>
</table>
</center>
<br /><br />
<center>
<form name="addpart" action="partadd-submit.php?TBL=<?php echo $table; ?>" method="post">
<table border="0" cellspacing="0" cellpadding="5">
<tbody>
		<tr>
			<td>What is the Demo Part Number?</td>
			<td><input type="text" name="partnumber" id="pn" value="<?php echo $partnumber ; ?>"></td>
		</tr>
		
		<tr>
			<td>What is the Part Name?</td>
			<td><input type="text" name="partname" value=""></td>
		</tr>
		<tr>
			<td>What is the Value?</td>
			<td><input type="text" name="value" value="" id="value"></td>
		</tr>
		<tr>
			<td>What is the Description?</td>
			<td><input type="text" name="desc"  value="" id="des"></td>
		</tr>
		<tr>
			<td>What is the Symbol?</td>
			<td><select name="symbol"><option selected> <?php echo $LibRef; ?></option><?php echo $optionsa; ?></select></td>
		</tr>
		<tr>
			<td>What is the Footprint?</td>
			<td><select name="footprint"><option selected> <?php echo $Footprint; ?></option><?php echo $options; ?></select></td>	
		</tr>
		<tr>
			<td>Who is the Manufacture?</td>
			<td><input type="text" name="manu" value="" id="manufacture"></td>
		</tr>
		<tr>
			<td>What is the Manufacturer Part Number?</td>
			<td><input type="text" name="manu_pn" value="" id="mp"> </td>
		</tr>
		<tr>
			<td>What is the Datasheet Link?</td>
			<td><input type="text" name="datasheet" value="" id="datasheet"></td>
		</tr>
</tbody>
</table>
</center>

<br>
<br>

<center>
<table border="0"  width="50%" cellpadding="2" cellspacing="2">
		<tr>
			<td align="center"><input type="submit" value="Submit Add"></td>
			<td><input type="button" onClick="location.href='view1x0.php?TBL=<?php echo $table; ?>'" value='Cancel'></td>
		</tr>
</table>
</form>
</center>


	
<?php

include("footer.php");

?>
<script>
$( document ).ready(function() {

  var tf=null;
  var tbl = '<?php echo $table; ?>';
      $('#url').click(function() {
var contents = $('#input').val();
            $.ajax({
           type: "POST",
		   async: false,  
           url: "get_all.php",
           data: { url:contents }
            }).done(function( msg ) {
           tf=msg.toString().split("$");
		  
			if(tbl.indexOf('capacitor')> -1)
		   	   {
			    if(tf[5]=='Capacitors')
				{
		   $('#des').val(tf[0]);
		    $('#mp').val(tf[1]);
			 $('#manufacture').val(tf[2]);
			 $('#value').val(tf[6]);
			 $('#datasheet').val(tf[4]);
			 }
			 else
			 {
			 alert("Please enter link for Capacitor");
			 }
			 }
			 
			  else if(tbl=='resistor')
		   	   {
			   if(tf[5].indexOf('Resistors')> -1)
		   	   {
		   $('#des').val(tf[0]);
		    $('#mp').val(tf[1]);
			 $('#manufacture').val(tf[2]);
			 $('#value').val(tf[7]);
			 $('#datasheet').val(tf[4]);
			 }
			 else
			 {
			  alert("Please enter link for Resistor");
			 }
			 }
			   else if(tbl=='connector')
		   	   {
			   if(tf[5].indexOf("Connectors") > -1)
		   	   {
		   $('#des').val(tf[0]);
		    $('#mp').val(tf[1]);
			 $('#manufacture').val(tf[2]);
			 $('#value').val(tf[1]);
			 $('#datasheet').val(tf[4]);
			 }
			 else
			 {
			  alert("Please enter link for Connectors");
			 }
			 }
			 
			 else if(tbl=='ic')
		   	   {
			   if(tf[5].indexOf("Integrated Circuits (ICs)") > -1)
		   	   {
		   $('#des').val(tf[0]);
		    $('#mp').val(tf[1]);
			 $('#manufacture').val(tf[2]);
			 $('#value').val(tf[1]);
			 $('#datasheet').val(tf[4]);
			 }
			 else
			 {
			  alert("Please enter link for Integrated Circuits");
			 }
			 }
			 
			 else if(tbl=='diode')
		   	   {
			   if(tf[8].indexOf('Diodes, Rectifiers')>-1)
		   	   {
		   $('#des').val(tf[0]);
		    $('#mp').val(tf[1]);
			 $('#manufacture').val(tf[2]);
			 $('#value').val(tf[1]);
			 $('#datasheet').val(tf[4]);
			 }
			 else
			 {
			  alert("Please enter link for Diodes");
			 }
			 }
			 
			 	 else if(tbl=='transistor')
		   	   {
			   if(tf[8].indexOf('FET') > -1)
		   	   {
		   $('#des').val(tf[0]);
		    $('#mp').val(tf[1]);
			 $('#manufacture').val(tf[2]);
			 $('#value').val(tf[1]);
			 $('#datasheet').val(tf[4]);
			 }
			 else
			 {
			  alert("Please enter link for Transistors");
			 }
			 }
			 
			 
			 
			 else 
		   	   {
		   $('#des').val(tf[0]);
		    $('#mp').val(tf[1]);
			 $('#manufacture').val(tf[2]);
			 $('#value').val(tf[1]);
			 $('#datasheet').val(tf[4]);
			 }
//			  else if(tbl=='diode' && (tf[5]=='Optoelectronics' || tf[5]=='Discrete Semiconductor Products') )
//		   	   {
//		   $('#des').val(tf[0]);
//		    $('#mp').val(tf[1]);
//			 $('#manufacture').val(tf[2]);
//			 $('#value').val(tf[3]);
//			 $('#datasheet').val(tf[4]);
//			 }
//			  else if(tbl=='ic' && tf[5]=='Integrated Circuits (ICs)')
//		   	   {
//		   $('#des').val(tf[0]);
//		    $('#mp').val(tf[1]);
//			 $('#manufacture').val(tf[2]);
//			 $('#value').val(tf[3]);
//			 $('#datasheet').val(tf[4]);
//			 }
//			 
//			  else if(tbl=='oscillator' && tf[5]=='Capacitors')
//		   	   {
//		   $('#des').val(tf[0]);
//		    $('#mp').val(tf[1]);
//			 $('#manufacture').val(tf[2]);
//			 $('#value').val(tf[3]);
//			 $('#datasheet').val(tf[4]);
//			 }
			 
			// else if(tbl=='transistor' && tf[5]=='Discrete Semiconductor Products')
//		   	   {
//		   $('#des').val(tf[0]);
//		    $('#mp').val(tf[1]);
//			 $('#manufacture').val(tf[2]);
//			 $('#value').val(tf[3]);
//			 $('#datasheet').val(tf[4]);
//			 }
        });

    
});
});


</script>
    </div>
  </div>

 
</div> 

</body>
</html>
