<?php 

$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="loggedin_index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>
 <link type="text/css" rel="stylesheet" href="css/easy-responsive-tabs.css" />
		<div class="side_work">	
			<div class="working_area">
	<div class="status_panel">
		<div class="status_sec">
	
				<div class="statusbar"><p>Current Page - <strong>Tools / Calculators</strong></p></div>
				<div class="returnstat"><a href="index.php" class="menu_click">Return</a></div>
	
		</div>
	</div>
<div class="app_calcmain">

   	<div class="calc_buttons">
	

		<center>
		<table  cellpadding="0" cellspacing="0" border="0" width="59%">
			<tr>
				<td><a href="voltage.php">VOLTAGE</a></td>
				<td><a href="ohm.php">LAW's</a></td>
				<td><a href="pcbcontrol.php">PCB's</a></td>
				
			</tr>
			<tr>
				<td><a href="impedance.php">IMPEDANCE</a></td>
				<td><a href="attenuators.php">ATTENUATORS</a></td>
				<td><a href="rlcf.php">RLCF</a></td>
				
			</tr>
			<tr>
				<td><a href="filters.php">FILTERS</a></td>
				<td><a href="resistance.php">RESISTANCE</a></td>
				<td><a href="transistors.php">TRANSISTORS</a></td>

			</tr>
			<tr>
				<td><a href="inductance.php">INDUCTANCE</a></td>
				<td><a href="capacitance.php">CAPACITANCE</a></td>
				<td><a href="misc.php">MISC</a></td>			
			</tr>
					
		</table>
		</center>
		
	
	</div>
            


    </div>
</div>
</div>
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL1 li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL1 li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#e9e9e9'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL1 li").each(function() { 
			$(".firstUL1 li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#e9e9e9'}); //set the background color to light-blue to that div
	 });
});
</script>