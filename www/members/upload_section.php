<?php
$groupswithaccess = "ladmin";
$loginpage = "../index.php";
$logoutpage = "../index.php";
require_once("../slpw/sitelokpw.php"); 

include("include.php");  // read css and js folders, sets database variables
session_start();
$target_dir = "docs/" . $slcustom15;

if (isset($_POST["upload_PCBLIB"])) {
    $target_file = $target_dir . basename($_FILES["file"]["name"]);
    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
        echo "The file " . basename($_FILES["file"]["name"]) . " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
if (isset($_POST["upload_SCHLIB"])) {
    $target_file = $target_dir . basename($_FILES["files"]["name"]);
    if (move_uploaded_file($_FILES["files"]["tmp_name"], $target_file)) {
        echo "The file " . basename($_FILES["files"]["name"]) . " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

include("header.php");
include("sidebar.php");

?>

    <link rel="stylesheet" type="text/css" href="../css/sortstyle.css">


    <style type="text/css" class="include" title="currentStyle">
        @import "DataTables-1.10.0/extensions/Editor-1.3.0/css/dataTables.editor.css";
        @import "DataTables-1.10.0/extensions/TableTools/css/dataTables.tableTools.css";


        td.details-control {
            background: url('images/editpage.png') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('images/closefile.png') no-repeat center center;
        }

        TD {
            font-size: 10px
        }

        #abc {
            font-family: "lucida grande", tahoma, verdana, arial, sans-serif;
            background: none repeat scroll 0 0 #FFFFFF;
            border: 10px solid rgba(220, 220, 220, 0.5);
            border-radius: 6px 6px 6px 6px;
            color: #333333;
            display: none;
            font-size: 14px;
            left: 50%;
            margin-left: -410px;
            position: fixed;
            top: 18%;
            width: 600px;
            z-index: 2;
        }

        .custom_panel .showhim {
            float: left;
            margin: 10px 5px 0 5px;
            width: 350px;
        }

        .custom_panel .showme {
            float: left;
            margin: 10px 5px 0 5px;
            width: 273px;
            height: 126px;
        }

        .custom_panel .showhim:hover .showme {
            display: block;
            float: left;
        }

        .custom_panel .showhim .showme {
            display: none !important;
            float: left;
            margin-left: 15px;
            font-size: 12px;
            background-color: #f1f1f1;
            padding: 15px;
        }
    </style>
    <link href="css/popup.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/jquery-ui.css" type="text/css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.css" type="text/css" rel="stylesheet">
    <script src="js/jquery-1.11.1.min.js" language="javascript" type="text/javascript"></script>
    <script type="text/javascript" src="js/popup.js"></script>
    <script type="text/javascript" src="js/jquery.min_online.js">
    </script>

    </head>

    <div class="side_work">
        <div class="working_area">
            <div class="status_panel">
                <div class="status_sec">

                    <div class="statusbar">
                        <p>Current Page -
                            <strong>Admin Area / Upload Area</strong>
                        </p>
                    </div>
                    <div class="returnstat">
                        <a href="admin.php" class="menu_click">Return</a>
                    </div>

                </div>
            </div>
            <div class="app_content">
                
                <div class="upload_main">

                    <div class="upload_left">
                        <table width="50%" cellspacing="2" cellpadding="2" border="0" class="custom_addnew">
                            <tbody>
                                <tr>
                                    <td align="center" style=" padding-bottom:25px;">
                                        <a href="#" class="pcbshow">
                                            <input type="submit" value="Upload PCBLIB" class="l9_icon">
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div id="toPopup" class="pcbpopup">

                            <div class="close closepcb"></div>
                            <div id="popup_content">

                                <div class="custom_panel">

                                    <div class="bom_form">
                                        <div class="up_bar">
                                            <p>Upload PCBLIB</p>
                                        </div>
                                        <div class="up_mid">
                                            <form name="custom_pcb" method="post" id="custom_pcb" action='' enctype="multipart/form-data">
                                                <center>
                                                    <table width="70%" cellpadding="0" cellspacing="0" class="bomname">
                                                        <tr>
                                                            <td>
                                                                <label>PCBLIB Name:</label>
                                                            </td>
                                                            <td colspan="2">
                                                                <input type="text" name="pcb_name" id="pcb_name" value="">&nbsp;&nbsp;
                                                                <img src="images/faq.png" height="20px" width="20px" title="THIS WILL BE DISPLAYED AS FILE NAME">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Please choose a file:</td>
                                                            <td colspan="2">
                                                                <input name="file" id="file" type="file" value="" accept=".PcbLib" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </center>
                                        </div>
                                        <div class="up_bottom">
                                            <center>
                                                <table width="90%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <input type="submit" id="upload_section" name="upload_PCBLIB" value="Upload" />
                                                        </td>
                                                        <!--	<td><a href="docs/bom.xls"><input type="button"  value="Download"/></a><div class="showhim"><a href="#"><img src="images/faq.png"></a><div class="showme">1.Click on Download button<br>2.Excel File will be downloaded<br>3.Fill information under the given columns correctly.<br>4.Upload Excel file to update your inventory list.<br>5.Quantity cannot be blank,either put a number or 0.<br>6.File should be in Excel 97-2003(xls) Format.</div></div></td>-->

                                                    </tr>
                                                </table>
                                            </center>

                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div id="backgroundPopup"></div>
                        <!-- upload left end -->
                    </div>


                    <div class="upload_rightnew">
                        <table width="50%" cellspacing="2" cellpadding="2" border="0" class="custom_addnew">
                            <tbody>
                                <tr>
                                    <td align="center" style=" padding-bottom:25px;">
                                        <a href="#" class="topopup2">
                                            <input type="button" class="l9new_icon" value="Upload SCHLIB">
                                        </a>
                                    </td>



                                </tr>

                            </tbody>
                        </table>

                         <div id="toPopup" class="schpopup">

                            <div class="close closesch"></div>
                            <div id="popup_content">

                                <div class="custom_panel">

                                    <div class="bom_form">
                                        <div class="up_bar">
                                            <p>Upload SCHLIB</p>
                                        </div>
                                        <div class="up_mid">
                                            <form name="custom_sch" method="post" id="custom_sch" action='' enctype="multipart/form-data">
                                                <center>
                                                    <table width="70%" cellpadding="0" cellspacing="0" class="bomname">
                                                        <tr>
                                                            <td>
                                                                <label>SCHLIB Name:</label>
                                                            </td>
                                                            <td colspan="2">
                                                                <input type="text" name="sch_name" id="sch_name" value="">&nbsp;&nbsp;
                                                                <img src="images/faq.png" height="20px" width="20px" title="THIS WILL BE DISPLAYED AS FILE NAME">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Please choose a file:</td>
                                                            <td colspan="2">
                                                                <input name="file" id="file" type="file" value="" accept=".SchLib" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </center>
                                        </div>
                                        <div class="up_bottom">
                                            <center>
                                                <table width="90%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <input type="submit" id="upload_section" name="upload_SCHLIB" value="Upload" /> 
                                                        </td>
                                                        <!--	<td><a href="docs/bom.xls"><input type="button"  value="Download"/></a><div class="showhim"><a href="#"><img src="images/faq.png"></a><div class="showme">1.Click on Download button<br>2.Excel File will be downloaded<br>3.Fill information under the given columns correctly.<br>4.Upload Excel file to update your inventory list.<br>5.Quantity cannot be blank,either put a number or 0.<br>6.File should be in Excel 97-2003(xls) Format.</div></div></td>-->

                                                    </tr>
                                                </table>
                                            </center>

                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        <!-- upload right end -->
                    </div>

                    <!-- end of upload-main -->
                </div>


            </div>
        </div>
    </div>
    <script>
        $(".closepcb").click(function () {
            $(".pcbpopup").hide();
            $("#backgroundPopup").fadeOut("normal");
            // function close pop up
            $('#pcb_name,#file').val('');
        });

         $(".closesch").click(function () {
            $(".pcbpopup").hide();
            $("#backgroundPopup").fadeOut("normal");
            // function close pop up
            $('#pcb_name,#file').val('');
        });


        $("a.pcbshow").click(function () {
            $("#toPopup").fadeIn(0500); // fadein popup div    
            $("#backgroundPopup").css("opacity", "0.2"); // css opacity, supports IE7, IE8
            $("#backgroundPopup").fadeIn(0001);
            return false;
        });

        $("a.schshow").click(function () {
            $(".schpopup").fadeIn(0500); // fadein popup div    
            $("#backgroundPopup").css("opacity", "0.2"); // css opacity, supports IE7, IE8
            $("#backgroundPopup").fadeIn(0001);
            return false;
        });

        $("div#backgroundPopup").click(function () {
            $(".pcbpopup").fadeOut("normal");
            $("#backgroundPopup").fadeOut("normal");
            $('#bom_name,#file').val('');
        });

        $('#custom_pcb').submit(function (thisform) {
            var file_name = $(thisform).find("#file").val();
            alert(file_name);


            var button = this;
            hideButton(button);

            var file = document.getElementById("file");

            /* Create a FormData instance */
            var formData = new FormData();
            /* Add the file */
            formData.append("file", file.files[0]);

            $.ajax({
                url: "upload_section.php",
                type: "POST",
                data: formData,
                contentType: false,
                success: function (response) {

                    console.log(response);
                    $("#toPopup").fadeOut("normal");
                    $('#bom_name,#file').val('');
                    showButton(button);
                    $("#backgroundPopup").fadeOut("normal");
                    $('#example').DataTable().ajax.reload();


                }
            });
        });

        function hideButton(button) {
            $(button).hide().after('<img src="../images/loading.gif" alt="loading"/>');
        }

        function showButton(button) {
            $(button).next('img').hide();
            $(button).show();

        }
    </script>