<?php 

$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>
 <link type="text/css" rel="stylesheet" href="css/easy-responsive-tabs.css" />
		<div class="side_work">	
			<div class="working_area">
	<div class="status_panel">
		<div class="status_sec">
	
				<div class="statusbar"><p>Current Page - <strong>Calculators / Inductance</strong></p></div>
				<div class="returnstat"><a href="calculator.php" class="menu_click">Return</a></div>
	
		</div>
	</div>
<div class="app_calculator">

  <div class="calc_buttons">
  	
  </div> 
            
<div id="body-container1">
                  <div class="glossary-container">
   					 <ul class="firstUL1">
						<li id="a" class="selected">Air Core Inductor Inductance </li>
						<li id="b">Microstrip Inductor </li>
						<li id="c">L-C Resonance</li>
					</ul>
				 </div>	
  <div class="content-container">

    <div id="content-for-a" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">
												

   <!----------------Air Core Inductor Inductance-----start-----------> 
<form method="POST" name="form" action="">
				<center>
				 <table align="center" style="margin-top:.3cm" class="induct">
					<tr><th colspan="2">Enter your values:</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Coil Diameter: </b></font> </td>
                <td align="left">    <input type="text" name="d1" size="7" maxlength="15"><font size="2" face="verdana"
        color="#1f6fa2"><b> inches</b></font>  </td>
           </tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Coil Length: </b></font> </td>
                   <td align="left">  <input type="text" name="len1" size="7" maxlength="15"><font size="2" face="verdana"
        color="#1f6fa2"><b>  inches</b></font></td>
         </tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Number Of Turns: </b></font> </td>
                   <td align="left">   <input type="text" name="n1" size="7" maxlength="15"></td>
            </tr>
			  
       
			  
		<tr>
            <td align="right"><input type="Button" value="Calculate" onClick="Compute11(this.form)" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
		 			<tr><td colspan="2" style="height:15px;"></td></tr>
			  
					<tr><th colspan="2">Results</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
		
  		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Inductance:</b></font></td> 
                    <td align="left"><input type="text" name="L1" size="7" maxlength="15" readonly="readonly">
            <font size="2" face="verdana"
        color="#1f6fa2"><b>uH </b></font> </td></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
        </table>
		</center>
      </form>
	  
	  <!----------------------Air Core Inductor Inductance-------------end----------->
			</div>
			<div class="calc_right">
				<h5>Air Core Inductor Inductance</h5>
				<p>The Air Core Inductor Inductance is a tool which calculates the inductance of an air core inductor.</p>
		
										
				 <div class="calc_foot"><p class="return-to-top">Return to Top</p></div>
			</div>
			
		</div>	
     
     
    </div>
	<div id="content-for-b" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

	 <!----------------Microstrip Inductor-----start-----------> 
<form method="POST" name="form">  
				<center>
				 <table align="center" style="margin-top:.3cm" class="induct">
					<tr><th colspan="2">Enter your values:</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Strip Width: </b></font> </td>
                  <td align="left">  <input type="text" name="w2" size="7" maxlength="15">
             <font size="2" face="verdana"
        color="#1f6fa2"><b> inches</b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Strip Length: </b></font> </td>
                <td align="left">    <input type="text" name="b2" size="7" maxlength="15">
               <font size="2" face="verdana"
        color="#1f6fa2"><b>  inches</b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Distance: </b></font> </td>
                 <td align="left">   <input type="text"  name="h2" size="7" maxlength="15">
            <font size="2" face="verdana"
        color="#1f6fa2"><b>  inches</b></font> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" value="Calculate" name="B1" onClick="Compute22(this.form)" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
		  			<tr><td colspan="2" style="height:15px;"></td></tr>
					<tr><th colspan="2">Result</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>

		
  		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Inductance:</b></font> </td>
                <td align="left">    <input type="text" name="L2" size="7" maxlength="15" readonly="readonly">
           <font size="2" face="verdana"
        color="#1f6fa2"><b> uH </b></font> </td></tr>
			<tr><td colspan="2" style="height:15px;"></td></tr>
			<tr><td colspan="2" style="height:15px;"></td></tr>
        </table>
		</center>
      </form>			
<!----------------Microstrip Inductor-----end-----------> 
		</div>
		<div class="calc_right">
			<h5>Microstrip Inductor</h5>
			<p>This calculator computes the inductance of a microstrip etched on a PCB.which according to the ARRL handbook is:L= 0.00508*b*(ln(2*b/(w+h))+.5+0.2235*(w+h)/b)
where w is width of the strip in inches,b is the length in inches,h is the distance between the strip and the ground plane, and L is inductance in uH.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
<div id="content-for-c" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">


  <!----------------------L-C Resonance--------------------start----------->
 <FORM NAME="form" id="frm3">
				<center>
				 <table align="center" style="margin-top:.3cm" class="induct">
					<tr><th colspan="2">Enter your values:</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
		
        <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance:</b></font></td>
                    <td align="left"><INPUT TYPE=text NAME="parm1" size="10" maxlength="15">
					<SELECT NAME="C_units"> 
				<OPTION >Microfarads
				<OPTION >Nanofarads
				<OPTION >Picofarads
				</SELECT>
              </td></tr>

        <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Inductance:</b></font> </td>
                   <td align="left"> <INPUT TYPE=text NAME="parm2" size="10" maxlength="15">
					<SELECT NAME="L_units" SIZE=1><OPTION>Henries
				<OPTION>Millihenries
				<OPTION>Microhenries
				<OPTION>Nanohenries
				</SELECT>
             </td></tr>

			  
		<tr>
            <td align="right"><input type="button" value="Calculate" name="B1" onClick="compute33(this.form)" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
		  		<tr><td colspan="2" style="height:15px;"></td></tr>
					<tr><th colspan="2">Result</th></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>
		   <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b> Frequency (MHz):</b></font> </td>
              <td align="left">  <INPUT TYPE=text NAME="result" size="10" maxlength="25" readonly="readonly" >
             <SELECT NAME="F_units" SIZE=1>
			<OPTION>Herz
			<OPTION>Kiloherz
			<OPTION>Megaherz
			<OPTION>Gigaherz
			</SELECT></td></tr>
			<tr><td colspan="2" style="height:15px;"></td></tr>
      </table>
	  </center>

      </form>		
<!----------------------L-C Resonance--------------------start----------->
		</div>
		<div class="calc_right">
			<h5>L-C Resonance</h5>
			<p>When an inductor or capacitor are placed in series or parallel they will have a resonant frequency which is determined by the design equation below. 
  LC resonant circuits are useful as notch filters or band pass filters.  The are also found in oscillator circuits. </p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
	
				

		
						
			
        </div>
  
</div>

    </div>
</div>
</div>
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL1 li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL1 li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#fff'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL1 li").each(function() { 
			$(".firstUL1 li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#fff'}); //set the background color to light-blue to that div
	 });
});
</script>

<!--script for Air Core Inductor Inductance   -->
<script language="JavaScript">
window.Compute11 = function(form)
{
           var d1= '';
		   var n1= '';
		   var len1= '';
           
	with(Math) {
		 d1= Number(form.d1.value);
		 n1= Number(form.n1.value);
		 len1= Number(form.len1.value);
		var L1='';
		var Precision= 5;
		
		L1= d1*d1*n1*n1/(18*d1+40*len1);
		
		form.L1.value= L1.toFixed(Precision);		
	}
}


</script>

<!--script for Air Core Inductor Inductance ends -->


<!--script for Microstrip Inductor    -->
<script language="JavaScript">
function Compute22(form) {
	with(Math) {
		var b2= Number(form.b2.value);
		var w2= Number(form.w2.value);
		var h2= Number(form.h2.value);
		
		var L2;
		var Precision= 5;
		
		L2= 0.00508*b2*(log(2*b2/(w2+h2))+.5+0.2235*(w2+h2)/b2);
		

		form.L2.value= L2.toFixed(Precision);
	}
}


</script>

<!--script for Microstrip Inductor  ends-->

<!--script for L-C Resonance    -->

<script type="text/javascript">
function compute33(form) {
var Precision=7;
cscale = 
1/(1000000 * (Math.pow(10, 3*(document.forms.frm3.C_units.selectedIndex))))  

lscale = 
1/(Math.pow(10, 3*(document.forms.frm3.L_units.selectedIndex)))

fscale = 
1/(Math.pow(10, 3*(document.forms.frm3.F_units.selectedIndex)))

cin = parseFloat(form.parm1.value)
lin = parseFloat(form.parm2.value)

c = cin*cscale
l = lin*lscale

f = 1/(2*Math.PI*Math.sqrt(l*c))

form.result.value  = (f*fscale).toPrecision(Precision)

}
</script>
<!--script for L-C Resonance  ends-->

