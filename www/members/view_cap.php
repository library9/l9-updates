<?php
$groupswithaccess="ladmin,luser,leditor";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");


include("include.php");  // read css and js folders, sets database variables
session_start();

$table=$_GET['TBL'];
$_SESSION['table']=$table;


include("header.php");
echo "<h2> User - " . $slname . "</h2>";
?>
<link rel="stylesheet" type="text/css" href="../css/sortstyle.css">

<style type="text/css" class="include" title="currentStyle">
    @import "DataTables-1.10.0/media/css/jquery.dataTables.css";
    @import "DataTables-1.10.0/extensions/Editor-1.3.0/css/dataTables.editor.css";


    td.details-control {
        background: url('images/editpage.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('images/closefile.png') no-repeat center center;
    }
    TD {font-size:10px}


</style>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/media/js/jquery.js"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/media/js/jquery.dataTables.js"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/Editor-1.3.0/js/dataTables.editor.js"></script>
<script type="text/javascript" charset="utf-8">
var editor;
    $(document).ready(function() {
	
	
	 editor = new $.fn.dataTable.Editor( {
            "ajax": "mi_datacap.php",
			"table": "#view",
            "fields": [
				{
				    "label": "Part Number",
                "name": "<?php echo $table; ?>.Part_Number"
			               
                },              
                {     "label": "PN",
                     "name": "<?php echo $table; ?>.PN"
                                   
                 },				  
				 {
				      "type": "hidden",
                     "label": "Library Ref",
                     "name": "<?php echo $table; ?>.Library_Ref",
					 "def": "<?php echo $table; ?>.Library_Ref"
                     
                 },
				
                {    "label": "Description",
                    "name": "<?php echo $table; ?>.Description"
					  
                },
				  {						   
                    "label": "Manufacturer",
                    "name": "<?php echo $table; ?>.Manufacturer"
				
                },
                {
				    "label": "Manufacturer PN",
                    "name": "<?php echo $table; ?>.Manufacturer_Part_Number"
                },
				{	"type": "hidden",    
                    "label": "Datasheet",
                    "name": "<?php echo $table; ?>.ComponentLink1URL",
					"def": "<?php echo $table; ?>.ComponentLink1URL"
					 
                },
				
				  {		       
                    "label": "Temp-Qual",
                    "name": "<?php echo $table; ?>.Temp_Qual"
                }
				
				
            ]
        } );

        // New record
        $('a.editor_create').on('click', function (e) {
            e.preventDefault();

           editor
                .title( 'Create new record' )
                .buttons( { "label": "Add", "fn": function () { editor.submit();
                   } } )
                .create();

        } );

        // Edit record
        $('#view').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();

            editor
                .title( 'Edit record' )
                .buttons( { "label": "Update", "fn": function () { editor.submit() } } )
                .edit( $(this).closest('tr') );
        } );

        // Delete a record (without asking a user for confirmation for this example)
        $('#view').on('click', 'a.editor_remove', function (e) {
            e.preventDefault();

            editor
                .title( 'Delete record' )
                .message( 'Are you sure you wish to remove this record?' )
                .buttons( { "label": "Delete", "fn": function () { editor.submit() } } )
                .remove( $(this).closest('tr') );
        } );

	
	
	 
        // DataTables init
     

	
	
	 var table = $('#view').DataTable( {              "sScrollY": "300px",
               dom: "lfrtip",
           "bAutoWidth": false,
                "bProcessing": true,
				
                //"bPaginate": false,
               // "bAutoWidth": false,
				
              ajax: {
               url: "mi_datacap.php",
               type: "POST"
                },
                
                "columns": [
				 

                {
                "data": "<?php echo $table; ?>.Part_Number"
                
                },

               
                {
                    "data": "<?php echo $table; ?>.PN"
                },
				
               
                {
				         "bVisible": false,
                         "sClass": "center",
                        "data": "<?php echo $table; ?>.Library_Ref"
                },
				
               {
                    "sClass": "center",
                    "data": "<?php echo $table; ?>.Description"
                },
				  
                {
                    "sClass": "center",
                    "data": "<?php echo $table; ?>.Manufacturer"
                },
                {
                    "sClass": "center",
                    "data": "<?php echo $table; ?>.Manufacturer_Part_Number"
                },
				  
                
                {
                    "data": "<?php echo $table; ?>.Temp_Qual"
                },
				 {
                "bSortable": true,
                "mRender": function (data, type, full) {
                   return '<a href="'+full.<?php echo $table; ?>.ComponentLink1URL+'" target="_blank">Datasheet</a>';
                    }
                },
				  {
                    "bSearchable": false,
                    "bSortable": false,
                    "mRender":function(data, type, full)
                    {
                        if ( '<?php echo $slusername; ?>' === 'adm-paul')
                        {
                            return '<center><a href="" class="editor_edit"><img src="pencil.png" width="16"></a>&nbsp&nbsp&nbsp<a href="" class="editor_remove"><img src="delete2.png" width="16"></a></center>';
                        }
                         else
                        {
						  return '<center><img src="pencil-grey.png" width="16">&nbsp&nbsp&nbsp<img src="delete2-grey.png" width="16"></a></center>';
						}
                    }

                }
              
                ]
			} );
	
    
} );
       
</script>
</head>
<br/>
<center>
<table border="0" width="50%" cellpadding="2" cellspacing="2">
	<tr>
		  <td align="center"><a href="index.php" class="menu_click">Main Menu</a>		</td>
	 </tr>
</table>
</center>

<center><br />
<table border="0" width="50%" cellpadding="2" cellspacing="2">
	<tr>
		<td align="center">
		<?php if($table==capacitor){ ?><a href="http://demo-library.com/members/partpreadd.php?TBL=<?php echo $table?>"><img src="../images/addon/capacitoradd.jpg" height="35px" width="180px"/></a>
		<?php } 
		else if($table==resistor){ ?><a href="http://demo-library.com/members/partpreadd.php?TBL=<?php echo $table?>"><img src="../images/addon/resistoradd.jpg" height="35px" width="180px"/></a>
		<?php }
		else if($table==misc){ ?><a href="http://demo-library.com/members/partadd.php?TBL=<?php echo $table?>"><img src="../images/addon/miscadd.jpg" height="35px" width="180px"/></a>
		
	<?php	}
	else if($table==connector){ ?><a href="http://demo-library.com/members/partadd.php?TBL=<?php echo $table?>"><img src="../images/addon/connectoradd.jpg" height="35px" width="180px"/></a>
	<?php	}
	else if($table==ic){ ?><a href="http://demo-library.com/members/partadd.php?TBL=<?php echo $table?>"><img src="../images/addon/icadd.jpg" height="35px" width="180px"/></a>
	<?php	}
	else if($table==diode){ ?><a href="http://demo-library.com/members/partadd.php?TBL=<?php echo $table?>"><img src="../images/addon/diodeadd.jpg" height="35px" width="180px"/></a>
	<?php	}
	else if($table==transistor){ ?><a href="http://demo-library.com/members/partadd.php?TBL=<?php echo $table?>"><img src="../images/addon/transistoradd.jpg" height="35px" width="180px"/></a>
	<?php	}
	else if($table==oscillator){ ?><a href="http://demo-library.com/members/partadd.php?TBL=<?php echo $table?>"><img src="../images/addon/oscillatoradd.jpg" height="35px" width="180px"/></a>
	<?php	}?>
		</td>
		
	</tr>
</table>

</center>
<center>
<table border="2" cellspacing="0" cellpadding="2" class="display dataTable" id="view" style="text-align:center;">
<thead>
	<tr> 
		
	  
		
		<th>Part Number</th>
		<th>PN</th>
		<th>Library Ref</th>	
		<th>Description</th>
		<th>Manufacturer</th>
		<th>Manufacturer Part Number</th>
	    <th>Temp-Qual</th>
		<th>DataSheet</th>
		  <th>Admin</th>
	</tr>
</thead>

</table>
</center>

<?php
include("footer.php");
?>