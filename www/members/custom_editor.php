
<?php
$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");

include("include.php");  // read css and js folders, sets database variables
session_start();

$table=$_GET['TBL'];
$_SESSION['table']=$table;



include("header.php");
 include("sidebar.php"); 

?>

<link rel="stylesheet" type="text/css" href="../css/sortstyle.css">

<style type="text/css" class="include" title="currentStyle">
    @import "DataTables-1.10.0/extensions/Editor-1.3.0/css/dataTables.editor.css";


    td.details-control {
        background: url('images/editpage.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('images/closefile.png') no-repeat center center;
    }
    TD {font-size:10px}


</style>
<link href="../css/popup.css" rel="stylesheet" type="text/css" media="all" />
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/jqueryui/dataTables.jqueryui.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"> </script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/popup1.js"></script>

 
<script src="//code.jquery.com/jquery-1.11.1.min.js" language="javascript" type="text/javascript"></script> 
<script src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" language="javascript" type="text/javascript"></script>
<script src="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/jqueryui/dataTables.jqueryui.js" language="javascript" type="text/javascript"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/Editor-1.3.0/js/dataTables.editor.js"></script>
<script type="text/javascript" charset="utf-8">
var editor;


    $(document).ready(function() {
	

        editor = new $.fn.dataTable.Editor( {
		    
            "ajax": "cus_data.php",
			"table": "#view",
            "fields": [
                {
                    "label": "Part Number",
                    "name": "Part_Number"
                    
                },
                {
                     "type": "readonly",
                     "label": "CPN",
                     "name": "CPN"
                    
                 },
				 {
				     
                     "label": "Library Ref",
                     "name": "Library_Ref",
					 "def": "Library_Ref"
                     
                 },
               
				 
                {
                    "label": "Description",
                    "name": "Description"
                },
              
                {
                    "label": "Manufacturer",
                    "name": "Manufacturer"
                },
                {
                    "label": "Manufacturer PN",
                    "name": "Manufacturer_Part_Number"
                },
              
				  {
                    "label": "Library_Path",
                    "name": "Library_Path"
                    
                },                
				 
				 {				      
                     "label": "Category",
                     "name": "Category"
					                     
                 },
                
                {
                    "label": "Comment",
                    "name": "Comment"
                },
               
                {
                    "label": "Component_Kind",
                    "name": "Component_Kind"
                },

                {
                    "label": "Component_Type",
                    "name": "Component_Type"
                },
              {
                    "label": "Designator",
                    "name": "Designator"
                    
                },
              
				   {
                    "label": "Footprint",
                    "name": "Footprint"
                    
                },
              
				 
                
                {
                    "label": "Manufacturer1",
                    "name": "Manufacturer1"
                },
               
                {
                    "label": "Manufacturer_Part_Number1",
                    "name": "Manufacturer_Part_Number1"
                },
                {
                    "label": "Packaging",
                    "name": "Packaging"
                },
              {
                    "label": "Pin_Count",
                    "name": "Pin_Count"
                    
                },
                {
                     
                     "label": "Signal_Integrity",
                     "name": "Signal_Integrity"
                    
                 },
				  {
                    "label": "Simulation",
                    "name": "Simulation"
                    
                },
                {
                     
                     "label": "Supplier",
                     "name": "Supplier"
                    
                 },
				 
				 {
				      
                     "label": "Supplier_Part_Number",
                     "name": "Supplier_Part_Number"
					                     
                 },
              
                {
                    "label": "Footprint_Path",
                    "name": "Footprint_Path"
                },
                {
                    "label": "Footprint_Ref",
                    "name": "Footprint_Ref"
                },
             
              {
                    "label": "Supplier_Part_Number1",
                    "name": "Supplier_Part_Number1"
                    
                },
                {
                     
                     "label": "Supplier1",
                     "name": "Supplier1"
                    
                 },
				  {
                    "label": "ComponentLink1Description",
                    "name": "ComponentLink1Description"
                    
                },
                
				 {
				      
                    "label": "Datasheet",
                    "name": "ComponentLink1URL",
					 "def": "ComponentLink1URL"
                },
				<?php
	mysql_connect($host, $username, $password)or die("cannot connect");
@mysql_select_db($slcustom15) or die( "Unable to select database");
	
	 $res1 = mysql_query("select alias_for_field,field_name from custom_alias where cat_name= '$table'") or die();
	
    while ($row1=mysql_fetch_array($res1))
    { $col=$row1['alias_for_field'];
	$val=$row1['field_name'];?>
	
               {
				      
                    "label": "<?php echo $col; ?>",
                    "name": "<?php echo $val; ?>",
					 "def": "<?php echo $val; ?>"
                },
	<?php	} ?>
               {
                "label": "Edit Status",
                "name": "status",
                "type": "select",
                "ipOpts": [
                    { "label": "GREEN-APPROVED", "value": "green" },
                    { "label": "RED-UNAPPROVED", "value": "red" },
                    { "label": "YELLOW-SUBMITTED", "value": "yellow" },
                    { "label": "BLUE-PROTOTYPE", "value": "blue" }
                ]
            }
			
				
				 
				
            ]
        } );
    var openVals;
    editor
	 .on( 'open', function () {
            // Store the values of the fields on open
            openVals = JSON.stringify( editor.get() );
			
        } )
	

        // New record
        $('a.editor_create').on('click', function (e) {
            e.preventDefault();

           editor
                .title( 'Create new record' )
                .buttons( { "label": "Add", "fn": function () { editor.submit();
                   } } )
                .create();

        } );

        // Edit record
        $('#view').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();
            var $tds = $(this).closest('tr').find('td');
			var updt='';
            editor
                .title( 'Edit record' )
                .buttons( { "label": "Update", "fn": function () { editor.submit()
              function revision(){
				var mpn=$tds.eq(5).text();
					var pn=$tds.eq(2).text();
					
				 if ( openVals !== JSON.stringify( editor.get() ) ) {
			var post_data=JSON.stringify( editor.get() );
			var data1=JSON.parse(post_data);
			var json = JSON.parse(openVals);
			/* var arr = $.map(json, function(el,a) { 
               

			console.log(el);
			console.log(a);})
			 */
			 $.each(json, function (i,v)
				{
				  $.each(data1,function(val,ele){
				    if(i==val){
					 if(v!==ele){
                       updt=updt+'@'+val+': Old Value-'+v+' ,New Value- '+ele;
					   
					 }
					}
				  
				  });
				});
				$.ajax({
                    url : 'updatecustom.php?TBL=<?php echo $table; ?>',
                    type : 'get',
					  async: false,
                    data : {
					    'pn':pn,
                        'mpn' : mpn,
                        'updt' : updt                    

                    },
                    success : function(result){
						
                         $('#view').DataTable().ajax.reload(); 

                    },
                    error : function(e){
                        console.log(e);
                    }
                });
				
               
            }				
				}	
            setTimeout( revision, 1500 ); 





				} } )
                .edit( $(this).closest('tr') );
        } );

        // Delete a record (without asking a user for confirmation for this example)
        $('#view').on('click', 'a.editor_remove', function (e) {
            e.preventDefault();
              var $tds = $(this).closest('tr').find('td');
							var mpn=$tds.eq(5).text();
							var pn=$tds.eq(2).text();
							var cat_name='<?php echo $_GET['TBL']; ?>';
							
            editor
                .title( 'Delete record' )
                .message( 'Are you sure you wish to remove this record?' )
                .buttons( { "label": "Delete", "fn": function () { editor.submit()
                 $.ajax({
                    url : 'del_part1.php',
                    type : 'get',
					
                    data : {
                       'mpn' : mpn,
						'pn' : pn,
                        'cat_name' : cat_name,                      

                    },
                    success : function(result){
					 
                        console.log(result);

                    },
                    error : function(e){
                        console.log(e);
                    }
                });



				} } )
                .remove( $(this).closest('tr') );
        } );

        // DataTables init
		 var fa = 0;
       var table = $('#view').DataTable( {              
             "sdom": 'T<"clear"><"H"lfr>t<"F"ip>',
           "bAutoWidth": false,
		   "bSort": false,
                "bProcessing": true,
				 "aLengthMenu": [[10, 50, 100, 500], [10, 50, 100, 500]],
                //"bPaginate": false,
               // "bAutoWidth": false,
				
              ajax: {
               url: "cus_data.php",
               type: "POST"
                },
                
                "columns": [
 {
                 "sClass": "center",
				"defaultContent": " ",
                "bSearchable": false,
                "bSortable": false,
                "mRender":function(data, type, full)
                {
                    if ( full.status === 'yellow')
                    {
                        return '<center><img src="../images/yellow.png" title="SUBMITTED '+full.date+'"></center>';
                    }
                    else if(full.status === 'green')
                    {
                        return '<center><img src="../images/green.png" title="ACTIVE" ></center>';
                    }
					  else if(full.status === 'red')
                    {
                        return '<center><img src="../images/red.png" title="ISSUE"></center>';
                    }else if(full.status === 'blue')
                    {
                        return '<center><img src="../images/blue.png" title="PROTOTYPE"></center>';
                    }
                }

            },
                {
				 "sClass": "center",
				"defaultContent": " ",
                "data": "Part_Number"
                
                },

               
                {
				 "sClass": "center",
				"defaultContent": " ",
                    "data": "CPN"
                },
               
                {
				         "defaultContent": " ",
				         "bVisible": false,
                         "sClass": "center",
                        "data": "Library_Ref"
                },
				
                {
				   "defaultContent": " ",
                    "sClass": "center",
                    "data": "Description"
                },
                {
				"defaultContent": " ",
                    "sClass": "center",
                    "data": "Manufacturer"
                },
                {
				"defaultContent": " ",
                    "sClass": "center",
                    "data": "Manufacturer_Part_Number"
                },
                             
              
				 
				  {
                    "bSearchable": false,
                    "bSortable": false,
                    "mRender":function(data, type, full)
                    {
                        if ( '<?php echo $slgroupname[0]; ?>' === 'ladmin')
                        {
                            return '<center><a href="" class="editor_edit"><img src="images/pencil.png" width="16"></a>&nbsp&nbsp&nbsp<a href="" class="editor_remove"><img src="images/delete2.png" width="16"></a></center>';
                        }
                         else
                        {
						  return '<center><img src="images/pencil-grey.png" width="16">&nbsp&nbsp&nbsp<img src="images/delete2-grey.png" width="16"></a></center>';
						}
                    }

                }
              
                ]
			} );
} );
       
</script>
</head>

		<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">

					<div class="statusbar"><p>Current Page - <strong>Parts Editor</strong></p></div>
					<div class="returnstat"><a href="part_editor.php" class="menu_click">Return</a></div>


	</div>
</div>
<div class="app_content">
<center>
<table cellspacing="0" cellpadding="0" class="display"  id="view" >
<thead>
	<tr> 
		<th >Status</th>
	    <th >Part Name</th>
		<th >CPN</th>
		<th  >Library Ref</th>
		<th >Description</th>
		<th >Manufacturer</th>
		<th >Manufacturer PN</th>
       			
		
		<th>Admin</th>
	</tr>
</thead>

</table>
</center>

</div>
</div>
</div>
