<?php 
$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>
 <link type="text/css" rel="stylesheet" href="css/easy-responsive-tabs.css" />
		<div class="side_work">	
			<div class="working_area">
	<div class="status_panel">
		<div class="status_sec">
	
				<div class="statusbar"><p>Current Page - <strong>Calculators / Voltage</strong></p></div>
				<div class="returnstat"><a href="calculator.php" class="menu_click">Return</a></div>
	
		</div>
	</div>
<div class="app_calculator">

  <div class="calc_buttons">
  	
  </div> 
            
<div id="body-container1">
                  <div class="glossary-container">
   					 <ul class="firstUL1">
						<li id="a" class="selected">Voltage-Current-Resistance-Power</li>
						<li id="b">Voltage Drop</li>
						<li id="c">Voltage Divider</li>

						
					</ul>
				 </div>	
  <div class="content-container">

    <div id="content-for-a" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">

				<FORM METHOD=POST>
				<center>
				<table align="center" class="vcrp">
				<tr><th colspan="3">Enter any two values:</th></tr>
				<tr><td colspan="3" style="height:15px;"></td></tr>
				<tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b >Voltage (Volts):</b></font></td>
			 <td align="left"><INPUT TYPE ="text" SIZE=9 NAME="E" VALUE="" maxlength="15"> </td></tr>
			  <tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b >Current (Amps):</b></font></td>
			<td align="left"><INPUT TYPE ="text" SIZE=9  NAME="I" VALUE="" maxlength="15"> </td></tr>
			 <tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b >Resistance (Ohms):</b></font></td>
			<td align="left"> <INPUT TYPE ="text" SIZE=9 NAME="R" VALUE="" maxlength="15" ></td></tr>
			 <tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b >Power (Watts):</b></font></td>
					<td align="left"><INPUT TYPE="text" SIZE=9  NAME="P" VALUE="" maxlength="15" ></td></tr>
	
				<tr><td align="right"><INPUT TYPE="button" NAME="name" VALUE="Calculate"
						onClick="solve(this.form);" class="calculate_cal"></td>
				<td align="left"><INPUT TYPE="reset" VALUE="Clear"
						onClick="clearBoxes(this.form);" class="clear_cal"></td></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
					</table>
				</center>	
				
				</FORM>			
			
			</div>
			<div class="calc_right">
				<h5>Voltage-Current-Resistance-Power</h5>
				<p>This calculator perform calculations associated with Voltage,Current,Resistance and Power.Enter any two known values and press "calculate" to solve for the two others.</p>
										
				 <div class="calc_foot"><p class="return-to-top">Return to Top</p></div>
			</div>
			
		</div>	
     
     
    </div>
	<div id="content-for-b" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

			
			<form>
			<center>
			<table class="vdrop" align="center">
				<tr><th colspan="2">Enter your values:</th></tr>
				<tr><td colspan="2" style="height:15px;"></td></tr>
				<tr>
					<td align="center" colspan="2">
					<table class="innervdrop">
					<tr><td align="center"><input NAME="CuAl" TYPE="radio" checked><label>&nbsp;Copper</label></td>
					<td align="center"> <input NAME="phs" TYPE="radio" checked><label>&nbsp;Single Phase</label></td>
					<td align="center"><input NAME="CuAl" TYPE="radio"><label>&nbsp;Aluminum</label></td>
					<td align="center"><input NAME="phs" TYPE="radio"><label>&nbsp;Three Phase</label></td></tr>
					</table>
					</td>
				</tr>
	
				<tr>
				<td align="center" colspan="2">
					<table class="innervdrop">
				 		  <tr><td align="center"><input NAME="vd" TYPE="radio" VALUE="6.2"><label>&nbsp;208 Volts</label></td>
				          <td align="center"><input NAME="vd" TYPE="radio" VALUE="3.6" checked><label>&nbsp;120 Volts</label></td>
						  <td align="right"><input NAME="vd" TYPE="radio" VALUE="7.2"><label>&nbsp;240 Volts</label></td>
						  <td align="center"><input NAME="vd" TYPE="radio" VALUE="8.3"><label>&nbsp;277 Volts</label></td>	
						  <td align="center"><input NAME="vd" TYPE="radio" VALUE="14.4"><label>&nbsp;480 Volts</label></td></tr>						
					</table>
				</td>
				
				</tr>					
				<tr><td colspan="3" style="height:15px;"></td></tr>
			  <tr>
			  	<td  align="right" style="padding-left:0px;"> <font size="2" face="verdana" color="#1f6fa2" ><b >Distance in feet, one-way:</b></font></td><td align="left"><input NAME="dist" SIZE="7" maxlength="15" TYPE="text" VALUE ></td>
				</tr>
				<tr>
			 	<td  align="right" style="padding-left:0px;"><font size="2" face="verdana" color="#1f6fa2" ><b >Total amperage of circuit:</b></font></td><td align="left"><input NAME="amps" SIZE="7" maxlength="15" TYPE="text" VALUE </td>
			</tr>
			<tr>	
				<td align="center" colspan="2"> <input TYPE="button" VALUE="Calculate"  onClick="main();"  class="calculate_cal"></td>
			</tr>	
			<tr><td colspan="3" style="height:15px;"></td></tr>
			<tr><th colspan="3">Result</th></tr>
			<tr><td colspan="3" style="height:15px;"></td></tr>
			<tr>
			<td align="center" colspan="2"><input NAME="wire" SIZE="10" TYPE="text" VALUE readonly="readonly"></td></tr>
			<tr><td align="center" colspan="2"><font size="2" face="verdana" color="#1f6fa2" ><b >This is your minimum wire size.</b></font></td>
			</tr>
			<tr><td colspan="3" style="height:15px;"></td></tr>   
			</table>
			</center>
			</form>		
		
		
		</div>
		<div class="calc_right">
		<h5>Voltage Drop</h5>
		<p>In situations where the circuit conductors span large distances, the voltage drop is calculated. If the voltage drop is too great, the circuit conductor must be
increased to maintain the current between the points. The calculations for a single-phase circuit and a three-phase circuit differ slightly</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
		<div id="content-for-c" style="background-color:#fff; clear:both;" class="calc_main">
			<div class="calc_content">
			<div class="calc_left">

						  
						 <center> 
						  <table align="center" class="vcrp">
		  					<tr><th colspan="2" align="center">Enter any three values:</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>
							<tr><td align="center" colspan="2"><p align="center"><img src="images/volt.jpg" border="0"></p></td></tr>
							
						  <tr><td align="center"> <p><font size="2" face="verdana" color="#1f6fa2" ><b> V<sub>in</sub> = Input Voltage  
						  <br>  V<sub>out</sub> = Output Voltage</b></font></p></td>
						  <td align="center">  <p  align="center"><img src="images/voltdivider form.bmp" border="0"></p></td>
						  </tr>

					
				<FORM Seq="1" method="post" name="net">
				
				<tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b>V<sub>in</sub>:</b></font></td>
			  <td align="left"><font size="2" face="verdana" color="#1f6fa2" ><b><input NAME="I" SIZE="9" maxlength="15" TYPE="text"> Volts</b></font></td></tr>
			 
			  <tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b>R<sub>a</sub>:</b></font></td>
			  <td align="left"><font size="2" face="verdana" color="#1f6fa2" ><b><input NAME="R1" SIZE="9" maxlength="15" TYPE="text" VALUE > Ohms</b></font></td></tr>
			 
			  <tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b>R<sub>b</sub>:</b></font></td>
			  <td align="left"><font size="2" face="verdana" color="#1f6fa2" ><b><input NAME="R2" SIZE="9" maxlength="15" TYPE="text" VALUE > Ohms</b></font>
			  
					</td></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>						
						  					<tr><th colspan="2" align="center">Result:</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>	 
			  <tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b>V<sub>out</sub>:</b></font></td>
			  <td align="left"><font size="2" face="verdana" color="#1f6fa2" ><b>
			  <input NAME="O" SIZE="9" maxlength="15" TYPE="text" VALUE readonly="readonly" > Volts</b></font></td></tr>
			  
			<tr><td colspan="2" style="height:15px;"></td></tr>  
			  
			  <tr><td align="right" ><INPUT TYPE="button"  VALUE="Calculate"
						onClick="getValues()"  name="Compute" class="calculate_cal"></td>
				<td align="left">		
				<INPUT TYPE="reset" VALUE="Clear"
						onClick="ClearAll()" class="clear_cal"></td></tr>
			<tr><td colspan="3" style="height:15px;"></td></tr>  						
					</table>
					</center>
					
					
			
				
						</form>			
			
			
			
			</div>
		<div class="calc_right">
		<h5>Voltage Divider</h5>
						 	<p>Voltage divider (also known as a potential divider) is a passive linear circuit that produces an output voltage (Vout) that is a fraction of its 
input voltage (Vin). Voltage division is the result of distributing the input voltage among the components of the divider. A simple example of a voltage divider is two resistors connected in series, with the input voltage applied across the resistor pair and the output voltage emerging from the connection between them.</p>
		<div class="calc_foot"><p class="return-to-top">Return to Top</p></div>	  
		</div>
			
		</div>		
     
    
    </div>
	
	
				
			
        </div>
  
</div>

    </div>
</div>
</div>
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL1 li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL1 li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#fff'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL1 li").each(function() { 
			$(".firstUL1 li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#fff'}); //set the background color to light-blue to that div
	 });
});
</script>



<!--script for Voltage-Current-Resistance-Power -->
<SCRIPT LANGUAGE="JavaScript">


	
	var E = 0;
	var I = 0;
	var R = 0;
	var P = 0;

	
	function clearBoxes(form)
		{
		form.E.value = "";
		form.I.value = "";
		form.R.value = "";
		form.P.value = "";

		form.E.focus();        
		}

	
	function showresults(form)
		{
		form.E.value = E;
		form.I.value = I;
		form.R.value = R;
		form.P.value = P;
		}

	
	function solve(form)
		{
		var i = 4;

		
		if(!form.E.value) E = 0;
			else E = eval(form.E.value);
		if(E == 0) i--;

		if(!form.I.value) I = 0; 
			else I = eval(form.I.value);
		if(I == 0) i--;

		if(!form.R.value) R = 0;
			else R = eval(form.R.value);
		if(R == 0) i--;

		if(!form.P.value) P = 0;
			else P = eval(form.P.value);
		if(P == 0) i--;

		
		if(i == 0) return;      
		if(i != 2)
			{
				
			alert("\nError in values provided!  Enter only Two\n");
			return;
			}

		
		if(E != 0)
			{
			
			if(I != 0)
				{
								
				R = E / I;
				P = E * I;				
				}
			else if (R != 0)
				{
								
				I =  E / R;
				P = (E * E) / R;				
				}
			else if (P != 0)
				{
								
				I = P / E;
				R = (E * E) / P;				
				}

			
			showresults(form);			
			form.E.focus();
			return;     
			}

		
		if(I != 0)
			{
			
			if (R != 0)
				{
								
				E = I * R;
				P = (I * I) * R;				
				}
			else if (P != 0)
				{
								
				E = P / I;
				R = P / (I * I);			
				}

			showresults(form);			
			form.I.focus();
			return;
			}

		
						
		I = Math.sqrt(P / R);
		E = Math.sqrt(P * R);			

		
		showresults(form);			
		form.R.focus();
		return;
	}


</SCRIPT>
<!--script for Voltage-Current-Resistance-Power -->


<SCRIPT language=JavaScript>
function cursor() {
	document.net.I.focus()
}
function getValues() {
	if (isNaN(document.net.I.value)) {
		alert ("Enter value for Input Voltage")
		document.net.I.focus()
		document.net.I.select()
		return false
	}
	if (isNaN(document.net.R1.value)) {
		alert ("Enter value for R1")
		document.net.R1.focus()
		document.net.R1.select()
		return false
	}
	if (isNaN(document.net.R2.value)) {
		alert ("Enter value for R2")
		document.net.R2.focus()
		document.net.R2.select()
		return false
	}
	if (isNaN(document.net.O.value)) {
		alert ("Enter value for Output Voltage")
		document.net.O.focus()
		document.net.O.select()
		return false
	}
	I = parseFloat(document.net.I.value)
	R1 = parseFloat(document.net.R1.value)
	R2 = parseFloat(document.net.R2.value) 
	O = parseFloat(document.net.O.value) 
	if (document.net.I.value == "" && document.net.R1.value !="" && document.net.R2.value!="" && document.net.O.value!="") {
	    I = O * (R1 + R2) / R2
	    document.net.I.value = Math.round(I * 1e3) / 1e3
	    return false
	}
	if (document.net.R1.value == "" && document.net.R2.value !="" && document.net.I.value!="" && document.net.O.value!="") {
	    R1 = (I * R2 / O) - R2
	    document.net.R1.value = Math.round(R1 * 100) / 100
	    return false
	}

	if (document.net.R2.value == "" && document.net.R1.value !="" && document.net.I.value!="" && document.net.O.value!="") {
	    R2 = O * R1 / (I - O)
	    document.net.R2.value = Math.round(R2 * 100) / 100
	    return false
	}
	if (document.net.O.value == "" && document.net.I.value !="" && document.net.R1.value!="" && document.net.R2.value!="") {
	    O = I * R2 / (R1 + R2)
	    document.net.O.value = Math.round(O * 1e3) / 1e3
	    return false
	}
}		
	
function ClearAll() {
	document.net.I.value = ""
	document.net.R1.value = ""
	document.net.R2.value = ""	
	document.net.O.value = ""	
	document.net.I.focus()
}
</SCRIPT>


<!--script for voltage drop-->


<script LANGUAGE="JAVASCRIPT">


var vp;								
var i;								
var l;								
var kCuAl;							
var Cm;								
var phase;							
var L = new Array(2580,4110,6530,10380,16510,26240,41740,52620,66360,83690,105600,
				133100,167800,211600,250000,300000,350000,400000,500000,
				600000,700000,750000,800000,900000,1000000,1250000,1500000,
				1750000);
var H = new Array(4110,6530,10380,16510,26240,41740,52620,66360,83690,105600,133100,
				167800,211600,250000,300000,350000,400000,500000,600000,
				700000,750000,800000,900000,1000000,1250000,1500000,1750000,
				2000000);
var S = new Array("14","12","10","8","6","4","3","2","1","1/0","2/0","3/0",
				"4/0","250 mcm","300 mcm","350 mcm","400 mcm","500 mcm","600 mcm",
				"700 mcm","750 mcm","800 mcm","900 mcm","1000 mcm","1250 mcm",
				"1500 mcm","1750 mcm","2000 mcm");

function main()
{
	if(document.forms[1].dist.value =="" || document.forms[1].amps.value =="")
	{
		alert("You must enter both distance and amperage");
	}
	else
	{
		drop();
	}
}

function drop()
{
	with(document.forms[1])
	{
		for (var n=0; n<=vd.length; n++)
		{
			if (vd[n].checked)
			{
				vp=(vd[n].value);
				break;
			}
		}
		l=parseFloat(dist.value);
		i=parseFloat(amps.value);
		(CuAl[0].checked)?(kCuAl=12.9):(kCuAl=21.2);
		(phs[0].checked)?(phase=2):(phase=1.732);
	}
		Cm=(phase*kCuAl*l*i)/vp;
		calcsz();
}

function calcsz()
{
	with(document.forms[1].wire)
	{
		for(x=0; x<=L.length; x++)
		{
			if(Cm<=L[0])
			{
				(value=S[x]);
				break;
			}
			else
			{
				if (Cm>L[x] && Cm<=H[x])
				{
					(value=S[x]);
					break;
				}
				
				if(Cm>H[27])
				{
					alert("Out of Range\n"+"Use smaller values");
					break;
				}
			}
		}
	}
}


function clearForm()
{
	with(document.forms[1])
	{
		for(x=0; x<=elements.length; x++)
		{
			vd[0].checked=true;
			CuAl[0].checked=true;
			phs[0].checked=true;
			dist.value="";
			amps.value="";
			wire.value="";
			break;
		}
	}
}
//-->
</script>
<!--script for voltage drop-->