<?php 

$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>
 <link type="text/css" rel="stylesheet" href="css/easy-responsive-tabs.css" />
		<div class="side_work">	
			<div class="working_area">
	<div class="status_panel">
		<div class="status_sec">
	
				<div class="statusbar"><p>Current Page - <strong>Calculators / PCB Control</strong></p></div>
				<div class="returnstat"><a href="calculator.php" class="menu_click">Return</a></div>
	
		</div>
	</div>
<div class="app_calculator">

  <div class="calc_buttons">

  </div> 
            
<div id="body-container1">
                  <div class="glossary-container">
   					 <ul class="firstUL1">
						<li id="a">PCB Thermal</li>
						<li id="b">PCB Trace</li>	
						<li id="c">PCB Via</li>
	
					</ul>
				 </div>	
  <div class="content-container">

    
	
		<div id="content-for-a" class="calc_main">
			<div class="calc_content">
			<div class="calc_left">
	             <div id="pcb-thermal-calc-container"></div>
							<script>
								(function() {
									var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
									calc.src = 'js/calc-pcbthermal.js';
									(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
								})();
							</script>
			</div>
			<div class="calc_right">
				<h5>PCB Thermal</h5>
				<p>The PCB Thermal Calculator is designed to estimate the junction temperatures of components which use exposed-pad packages. The data used in the PCB Thermal Calculator is generated from measured laboratory data combined with thermal-modeling analysis that uses a series of thousands of data points. The result of the correlation is used to create the curves and the data points that appear in the PCB Thermal Calculator. The PCB Thermal Calculator also supports the input of the board temperature as a reference point. In this case, the junction temperature results are estimated independently of the PCB layout.</p>
				<div class="calc_foot"><p class="return-to-top">Return to Top</p></div>
			</div>	
			</div>		
     

    </div>
	
	<div id="content-for-b"  class="calc_main">
				<div class="calc_content">
			<div class="calc_left">
						<div id="pcb-trace-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calc-pcbtrace.js';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script> 
					</div>
					<div class="calc_right">
						<h5>PCB Trace</h5>
						<p>As the current in the PCB traces rises, the wires carrying the current gets hotter. If we keep increasing the current, a point will come where it will be unsafe or undesirable for the temperature to increase. A wider trace will have higher current carrying capacity.The Calculator calculates the required PCB trace width for a given current.It calculates the characteristic impedance and per-unit-length parameters of typical printed circuit board trace geometries.</p>
					<div class="calc_foot"><p class="return-to-top">Return to Top</p></div>						
					</div>
				</div>	
     

    </div>
	
	<div id="content-for-c"  class="calc_main">
			<div class="calc_content" class="calc_main">
			<div class="calc_left">
                    <div id="pcb-via-calc-container"></div>

                  <script>
                          (function() {
                           var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
                          calc.src = 'js/calc-pcbvia.js';
                           (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
                         })();
                  </script>
				 </div> 
					<div class="calc_right">
						<h5>PCB Via</h5>
						<p>The Via current carying calculator allows you to work out current carying capcity of a via and is based on the IPC-2152 - "Standard for Determining Current 
Carrying Capacity in Printed Board Design".</p>
						<div class="calc_foot"><p class="return-to-top">Return to Top</p></div>
					</div>	
				</div>					 
     

    </div>
		
	
        </div>
  
</div>

    </div>
</div>
</div>
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL1 li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL1 li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#e9e9e9'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL1 li").each(function() { 
			$(".firstUL1 li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#e9e9e9'}); //set the background color to light-blue to that div
	 });
});
</script>