<?php
$groupswithaccess="ladmin,luser";
$loginpage="loggedin_index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");


include("include.php");  // read css and js folders, sets database variables
include("sidebar.php");


$fn=$_GET['fn'];

include("header.php");
mysql_connect($host,$username,$password);
mysql_select_db($slcustom15) or die( "Unable to select database");
$query="select * from bom where bom_name = '".$fn."'";
$result=mysql_query($query);
$num=mysql_numrows($result); 

?>
		<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">

<div class="statusbar">			<p>Current Page - <strong>BOM-INFO</strong></p></div>
<div class="returnstat"><a href="bom.php" class="menu_click">Return</a></div>

	</div>
</div>
<div class="app_content">
<style type="text/css" class="include" title="currentStyle">

    @import "DataTables-1.10.0/extensions/Editor-1.3.0/css/dataTables.editor.css";
	@import "DataTables-1.10.0/extensions/TableTools/css/dataTables.tableTools.css";


    td.details-control {
        background: url('images/editpage.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('images/closefile.png') no-repeat center center;
    }
    TD {font-size:10px}


</style>
<link rel="stylesheet" type="text/css" href="../css/sortstyle.css">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/jqueryui/dataTables.jqueryui.css" type="text/css" rel="stylesheet">
<script src="//code.jquery.com/jquery-1.11.1.min.js" language="javascript" type="text/javascript"></script> 
<script src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" language="javascript" type="text/javascript"></script>
<script src="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/jqueryui/dataTables.jqueryui.js" language="javascript" type="text/javascript"></script>
<script class="include" type="text/javascript" charset="utf-8" src="js/ZeroClipboard.js"></script>

<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/TableTools/js/dataTables.tableTools.js"></script>
<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
	$('#parts').dataTable( {
		 "sdom": 'T<"clear"><"H"lfr>t<"F"ip>',
		"aaSorting": [[ 1, "asc" ]],
		"aLengthMenu": [[10, 50, 100, 500], [10, 50, 100, 500]],
		"oTableTools": {
			
			 "aButtons": [
                {
                    "sExtends": "copy",
                    "mColumns": [  0,1,2,3,4,5]
                },
                {
                    "sExtends": "csv",
                    "mColumns": [  0,1,2,3,4,5]
                },
                {
                    "sExtends": "xls",
                    "mColumns": [  0,1,2,3,4,5 ]
                },
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "landscape",
					"sPdfMessage": "Library9 BOM",
                    "mColumns": [ 0,1,2,3,4,5 ]
                },
                {
                    "sExtends": "print",
                    "sMessage": "Library9 BOM"
                    
                },
            ]
		}
      
		
	} );
} );
</script>
<table  cellspacing="0" cellpadding="0" class="display " id="parts">
<thead>
	<tr> 
	
	<th>Bom Name</th>
	<th>Reference Designator</th>
	<th>Description</th>
	<th>Manufacture</th>
	<th>Manufacturer Part Number</th>
	<th>Quantity</th>
	</tr></thead>
	<tbody>	
<?php
$i=0;
while ($i < $num) {
$PartName=mysql_result($result,$i,'bom_name');
$PartNumber=mysql_result($result,$i,'reference_designator');
$Desc=mysql_result($result,$i,'description');
$SchSym=mysql_result($result,$i,'manufacture');
$Footprint=mysql_result($result,$i,'mpn');

$Datasheet=mysql_result($result,$i,"quantity");
?>

	<tr> 
		<td><center><?php echo $PartName; ?></center></td>
		<td nowrap><center><?php echo $PartNumber; ?></center></td>
		<td><center><?php echo $Desc; ?></center></td>
		<td><center><?php echo $SchSym; ?></center></td>
		<td><center><a href="where_used.php?MPN=<?php echo $Footprint; ?>"><?php echo $Footprint; ?></a></center></td>
		
		<td><center><?php echo $Datasheet; ?></center></td>
	</tr>
	
<?php
++$i;
} 
?>
</tbody></table>

</div>
</div>
</div>

</body>
</html>
