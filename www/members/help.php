<?php
$groupswithaccess="ladmin,luser";
$loginpage="loggedin_index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
session_start();


$_SESSION['com_id']=$slcustom15;


?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Library9.com</title>
<link href="styles.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="shortcut icon" href="../images/favicon.png"/>

<script type="text/javascript" src="../javascript/jquery-1.6.1.min.js"></script>
<!-- for navigation -->
<script type="text/javascript" src="../javascript/script-navigation.js"></script>

<!--[if IE 9 ]>
 <link href="../css/ie.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->

</head>
<body>
<?php include("header.php") ?>
<?php include("sidebar.php") ?>
		<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">
<div class="statusbar"><p> Current Page - <strong>Help</strong></p></div>
<div class="returnstat"><a href="index.php" class="menu_click">Return</a></div>
	</div>
</div>


<div class="app_help">
<h2 style="margin-top:10px; margin-bottom:15px;">Frequently Asked Questions</h2>
<ul id='faqList'>

<li>
<p class='question'>How to add a new part?</p>
<div class='answer'>

<ul class='list_normal'>
<li><strong>* </strong>Look up for the part that u want add.</li>
<li><strong>* </strong><strong>“Copy” </strong>the Manufacturer Part Number.</li>
<li><strong>* </strong>Go to the desire component (e.g. <strong>Resistor, Capacitor, etc.</strong>) u want to add.</li>
<li><strong>* </strong>Click on the “<strong>RESISTOR+</strong>”</li>
<li><strong>* </strong>Pop will appear “<strong>Paste</strong>” the Manufacturer Part Number and click on “<strong>GET VALUE</strong>”.</li>
<li><strong>* </strong>Select “<strong>Manually Fill Values</strong>”, click in the column in front of “<strong>What is the Symbol</strong>” and select it form the given options.</li>
<li><strong>* </strong>Click on “<strong>SUBMIT ADD</strong>”</li>
<li><strong>* </strong>Will give u a prompt that the part is added.</li>
</ul>

</div>
</li>
<li>
<p class='question'>How to change password?</p>
<div class='answer'>

<ul class='list_normal'>
<li><strong>* </strong>Click on the “<strong>MY ACCOUNT</strong>”.</li>
<li><strong>* </strong>Dropdown click on “<strong>ACCOUNT INFO</strong>”.</li>
<li><strong>* </strong>Will give you a Window to edit the Information.</li>

</ul>

</div>
</li>
<li>
<p class='question'>How to create a BOM (Billing Of Materials)?</p>
<div class='answer'>
<ul class='list_normal'>
<li><strong>* </strong>Click on “<strong>FILES</strong>”.</li>
<li><strong>* </strong>Dropdown click on “<strong>DOWNLOADS</strong>”.</li>
<li><strong>* </strong>Gives you a Window click on down arrow in front of “<strong>BOM TEMPLATE</strong>”.</li>
<li><strong>* </strong>Open the downloaded file and add the information column wise.</li>
<li><strong>* </strong>Save the file.</li>
</ul>
</div>
</li>
<li>
<p class='question'>How to add a BOM (Billing Of Materials)?</p>
<div class='answer'>
<ul class='list_normal'>
<li><strong>* </strong>Click on “<strong>LIBRARY</strong>”.</li>
<li><strong>* </strong>Dropdown click on “<strong>BOM</strong>”.</li>
<li><strong>* </strong>Gives you a Window click on “<strong>UPLOAD BOM+</strong>”.</li>
<li><strong>* </strong>Popup will appear enter “<strong>BOM NAME</strong>” (file name) and “<strong>CHOOSE FILE</strong>” (Path of the file we need to upload).</li>
<li><strong>* </strong>Click “<strong>UPLOAD</strong>”.</li>
<li><strong>* </strong>Will get you back to the same Window and the file is uploaded.</li>
</ul>
</div>
</li>
<li>
<p class='question'>How to Backup Data?</p>
<div class='answer'>
<ul class='list_normal'>
<li><strong>* </strong>Click on “<strong>FILES</strong>”.</li>
<li><strong>* </strong>Dropdown click on “<strong>DOWNLOADS</strong>”.</li>
<li><strong>* </strong>Gives you a Window click on down arrow in front of “<strong>DATA BACKUP SQL</strong>”.</li>
</ul>
</div>
</li>
<li>
<p class='question'>Setup L9 database with Altium</p>
<div class='answer'>
<ul class='list_normal'>
<li><strong>* </strong>Go to “<strong>FILES</strong>”.</li>
<li><strong>* </strong>Click on  “<strong>DOWNLOAD</strong>”.</li>
<li><strong>* </strong>5 main files that we need to download are  “<strong>Database Library File”, “MySql ODBC Connector”, “ODBC2L9”, “PCB Library File” and “Schematic Library</strong>”.</li>
<li><strong>* </strong>First install  “<strong>MySql ODBC Connector</strong>.</li>
<li><strong>* </strong>Unzip  “<strong>ODBC2L9</strong>”.It is going to contain a file with the company name you have created, open the folder and find “<strong>ODBC2L9.exe</strong>”. Run that file. Will prompt “<strong>Successful</strong>”.</li>
<li><strong>* </strong>Run Altium, go to the library and install the "<strong>Database Library File</strong>".</li>
<li><strong>* </strong>Select the component and place it accordingly.</li>
</ul>
</div>
</li>
</ul>

																																											  <!-- end table -->
</div>
</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="js/jquery.simpleFAQ.js"></script>
<script src="js/jquery.quicksilver.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#faqList').simpleFAQ();
  });
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
