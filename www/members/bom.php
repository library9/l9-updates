<?php
$groupswithaccess = "ladmin,luser,leditor";
$loginpage = "../index.php";
$logoutpage = "../index.php";
require_once("../slpw/sitelokpw.php");


include("include.php");  // read css and js folders, sets database variables
session_start();

$table = $_GET['TBL'];
$_SESSION['table'] = $table;


include("header.php");
include("sidebar.php");

?>

    <style type="text/css" class="include" title="currentStyle">
        < !-- @import "DataTables-1.10.0/extensions/Editor-1.3.0/css/dataTables.editor.css";
        -->td.details-control {
            background: url('images/editpage.png') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('images/closefile.png') no-repeat center center;
        }

        TD {
            font-size: 10px
        }
    </style>
    <style type="text/css" class="include" title="currentStyle">
        .custom_panel .showhim {
            float: left;
            margin: 10px 5px 0 5px;
            width: 350px;
        }

        .custom_panel .showme {
            float: left;
            margin: 10px 5px 0 5px;
            width: 273px;
            height: 126px;
        }

        .custom_panel .showhim:hover .showme {
            display: block;
            float: left;
        }

        .custom_panel .showhim .showme {
            display: none !important;
            float: left;
            margin-left: 15px;
            font-size: 12px;
            background-color: #f1f1f1;
            padding: 15px;
        }
    </style>
    <link href="css/popup.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/jquery-ui.css" type="text/css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="js/popup.js"></script>
    <script type="text/javascript" src="js/jquery.min_online.js">
    </script>

    <script src="js/jquery.dataTablesonline.min.js" language="javascript" type="text/javascript"></script>
    <script src="js/dataTables.jqueryui.js" language="javascript" type="text/javascript"></script>

    <script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/Editor-1.3.0/js/dataTables.editor.js"></script>
    <script type="text/javascript" charset="utf-8">
        var editor;


        $(document).ready(function () {

            editor = new $.fn.dataTable.Editor({

                "ajax": "bom_data.php",
                "table": "#example",
                "fields": [{
                        "label": "Part Number",
                        "name": "bom_filename.date"

                    },
                    {

                        "label": "PN",
                        "name": "bom_filename.bom_name"

                    }


                ]
            });

            // New record
            $('a.editor_create').on('click', function (e) {
                e.preventDefault();

                editor
                    .title('Create new record')
                    .buttons({
                        "label": "Add",
                        "fn": function () {
                            editor.submit();
                        }
                    })
                    .create();

            });

            // Edit record
            $('#example').on('click', 'a.editor_edit', function (e) {
                e.preventDefault();

                editor
                    .title('Edit record')
                    .buttons({
                        "label": "Update",
                        "fn": function () {
                            editor.submit()
                        }
                    })
                    .edit($(this).closest('tr'));
            });

            // Delete a record (without asking a user for confirmation for this example)
            $('#example').on('click', 'a.editor_remove', function (e) {
                e.preventDefault();
                
                editor
                    .title('Delete record')
                    .message('Are you sure you wish to remove this record?')
                    .buttons({
                        "label": "Delete",
                        "fn": function () {
                            editor.submit()
                        }
                    })
                    .remove($(this).closest('tr'));
                
                
            });

            // DataTables init
            var fa = 0;
            var table = $('#example').DataTable({
                "sdom": 'T<"clear"><"H"lfr>t<"F"ip>',
                "bAutoWidth": false,
                "bProcessing": true,
                "aLengthMenu": [
                    [10, 50, 100, 500],
                    [10, 50, 100, 500]
                ],
                //"bPaginate": false,
                // "bAutoWidth": false,


                ajax: {
                    url: "bom_data.php",
                    type: "POST"
                },

                "columns": [
                    {
                        "sClass": "center",
                        "defaultContent": " ",
                        "data": "bom_filename.date"
                    },
                    {
                        "sClass": "center",
                        "bSortable": true,
                        "mRender": function (data, type, full) {
                            return '<a href="bom_info.php?fn=' + full.bom_filename.bom_name +
                                '">' + full.bom_filename.bom_name + '</a>';
                        }
                    },
                    {
                        "bSearchable": false,
                        "bSortable": false,
                        "mRender": function (data, type, full) {
                            if ('<?php echo $slgroupname[0]; ?>' === 'ladmin') {
                                return '<center><a href="" id="test" class="editor_remove" filename="'+full.bom_filename.bom_name+'"><img src="images/delete2.png" width="16"></a></center>';
                            } else {
                                return '<center><img src="images/delete2-grey.png" width="16"></a></center>';
                            }
                        }

                    }

                ]

            });

        });
    </script>
    </head>
    <div class="side_work">
        <div class="working_area">
            <div class="status_panel">
                <div class="status_sec">

                    <div class="statusbar">
                        <p> Current Page -
                            <strong>Library / BOM</strong>
                        </p>
                    </div>

                    <div class="returnstat">
                        <a href="index.php" class="menu_click">Return</a>
                    </div>
                </div>
            </div>
            <div class="app_bom">
                <center>
                    <br />
                    <table border="0" width="50%" cellpadding="2" cellspacing="2" class="custom_add">

                        <tr>


                            <!-- User chooses MISC to view/delete/add/edit -->


                            <td align="center" style=" padding-bottom:25px;">
                                <!-- User chooses MISC to view/delete/add/edit -->
                                <a href="#" class="topopup">
                                    <input type="submit" value="Upload Bom" class="l9_icon">
                                </a>
                            </td>



                        </tr>

                    </table>

                </center>
                <center>
                    <table cellspacing="0" cellpadding="0" id="example" width="100%">
                        <thead>
                            <tr>

                                <th>Date</th>
                                <th>BOM Name</th>
                                <th>Admin</th>

                            </tr>
                        </thead>

                    </table>
                </center>
                <div id="toPopup">

                    <div class="close"></div>
                    <div id="popup_content">

                        <div class="custom_panel">

                            <div class="bom_form">
                                <div class="up_bar">
                                    <p>Upload BOM</p>
                                </div>
                                <div class="up_mid">
                                    <form name="custom_pn" method="post" id="custom_pn" action='' enctype="multipart/form-data">
                                        <center>
                                            <table width="70%" cellpadding="0" cellspacing="0" class="bomname">
                                                <tr>
                                                    <td>
                                                        <label>BOM Name:</label>
                                                    </td>
                                                    <td colspan="2">
                                                        <input type="text" name="bom_name" id="bom_name" value="">&nbsp;&nbsp;
                                                        <img src="images/faq.png" height="20px" width="20px"
                                                            title="THIS WILL BE DISPLAYED AS FILE NAME">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Please choose a file:</td>
                                                    <td colspan="2">
                                                        <input name="file" id="file" type="file" accept="application/vnd.ms-excel"
                                                        />
                                                    </td>
                                                </tr>
                                            </table>
                                        </center>
                                </div>
                                <div class="up_bottom">
                                    <center>
                                        <table width="90%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <input type="submit" value="Upload" name="submit" />
                                                </td>
                                                <!--	<td><a href="docs/bom.xls"><input type="button"  value="Download"/></a><div class="showhim"><a href="#"><img src="images/faq.png"></a><div class="showme">1.Click on Download button<br>2.Excel File will be downloaded<br>3.Fill information under the given columns correctly.<br>4.Upload Excel file to update your inventory list.<br>5.Quantity cannot be blank,either put a number or 0.<br>6.File should be in Excel 97-2003(xls) Format.</div></div></td>-->

                                            </tr>
                                        </table>
                                    </center>

                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div id="backgroundPopup"></div>
            </div>
        </div>
    </div>
    <script>
        $("div.close").click(function () {
            $("#toPopup").hide(); // function close pop up
            $('#bom_name,#file').val('');
        });


        $("a.topopup").click(function () {
            $("#toPopup").fadeIn(0500); // fadein popup div    
            $("#backgroundPopup").css("opacity", "0.2"); // css opacity, supports IE7, IE8
            $("#backgroundPopup").fadeIn(0001);
            return false;
        });

        $("div#backgroundPopup").click(function () {
            $("#toPopup").fadeOut("normal");
            $("#backgroundPopup").fadeOut("normal");
            $('#bom_name,#file').val('');
        });

        $('#custom_pn').submit(function () {
            var bom_name = $('#bom_name').val();

            var button = this;
            hideButton(button);

            var file = document.getElementById("file");

            /* Create a FormData instance */
            var formData = new FormData();
            /* Add the file */
            formData.append("file", file.files[0]);

            $.ajax({
                url: "upload_bom.php?bom=" + bom_name,
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function (response) {

                    console.log(response);
                    $("#toPopup").fadeOut("normal");
                    $('#bom_name,#file').val('');
                    showButton(button);
                    $("#backgroundPopup").fadeOut("normal");
                    $('#example').DataTable().ajax.reload();


                }
            });
        });

        function hideButton(button) {
            $(button).hide().after('<img src="../images/loading.gif" alt="loading"/>');
        }

        function showButton(button) {
            $(button).next('img').hide();
            $(button).show();

        }
    </script>