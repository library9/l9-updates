
<?php
$groupswithaccess="ladmin,luser,leditor";
$loginpage="loggedin_index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
$actual_link = 'http://'.$_SERVER['HTTP_HOST']
?>

		<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">

			<div class="statusbar"><p>Current Page - <strong>Admin Area / Add Category</strong></p></div>
			<div class="returnstat"><a href="admin.php" class="menu_click">Return</a></div>

	</div>
</div>
<div class="app_addcat">

<div class="subscription_pg">
	<h2>Add New Category</h2>
	<center>
<table border="0" cellspacing="0" cellpadding="5" align="center">
<tbody>
		<tr>
			<div class="padded-bottom">
				<span class="form-group-header">Category Naming Setup</span>
				<br>
				<p class="category_cpn_example" style="text-align:center;">Example CPN For Category: <span id="example_cpn">-</span></p>
			</div>
		</tr>
        <tr>
			<td><label>Category Name</label></td>
			<td ><input type="text" name="category" id="category" value="" title="Please Avoid Special Characters.!!" required></td>
		</tr>
		<tr>
		<tr>
			<td><label>Text Prefix For Part</label></td>
			<td ><input type="text" class="watch_changes_category" name="prefix1" id="prefix1" value="" maxlength="6" required></td>
		</tr>
		<tr>
			<td><label>Additional Prefix for PN</label></td>
			<td ><input type="text" class="watch_changes_category" name="prefix2" id="prefix2" value="" required></td>
		</tr>
		<tr>
			<td><label>Separator</label></td>
			<td ><input type="text" name="separator" id="separator" value="-" disabled="disabled" required></td>
		</tr>
		<tr>
			<td><label>Postfix Numbering</label></td>
			<td ><select name="postfix" value="" id="postfix" class="watch_changes_category" required>
			       <option value="00001">00000</option>			
                   <option value="0001">0000</option>
                   <option value="001">000</option>
                   </select>                   
			</td>			
		</tr>


		<tr>
			
			<td><input type="hidden" name="company_id" value="<?php echo $slcustom15;?>" id="company_id" ></td>
			
		</tr>
		
</tbody>
</table>
<br><br>
<table border="0" cellspacing="0" cellpadding="5" align="center">
	<tbody>
			<tr>
				<div class="padded-bottom">
					<span class="form-group-header">Part Naming</span>
					<br>
					<p class="category_cpn_example" style="text-align:center;">Example Part Name For Category: <span id="example_pn">-</span></p>
				</div>
			</tr>

			<tr>
				<td><label>Part Type</label></td>
				<td ><select name="part_name_type" value="" id="part_name_type" class="watch_changes_part_number" required>
					<option value="resistor">Resistor</option>			
					<option value="capacitor">Capacitor</option>
					<option value="other">Other</option>
					</select>                   
				</td>			
			</tr>

			<tr>
				<td><label>Part Prefix</label></td>
				<td ><input type="text" name="part_name_prefix" id="part_name_prefix" value="" class="watch_changes_part_number" maxlength="6" required></td>
			</tr>

			<!-- Fields for 'other' part type -->
			<tr class="other_group partfields">
				<td><label>Part Manufacturer</label></td>
				<td ><input type="text" name="part_name_manu" id="part_name_manu" value="MANUFACTURERNAME"  required disabled></td>
			</tr>

			<tr class="other_group partfields">
				<td><label>Manufacturer Part Number</label></td>
				<td ><input type="text" name="part_name_mpn" id="part_name_mpn" value="Manu Part #"  required disabled></td>
			</tr>
			<!-- /End other fields -->

			<!-- Fields for 'resistor' part_type -->
			<tr class="resistor_group partfields">
				<td><label>Resistor Value</label></td>
				<td ><input type="text" name="part_name_resistor_value" id="part_name_resistor_value" value="200.0"  required disabled></td>
			</tr>

			<tr class="resistor_group partfields">
				<td><label>Resistor Tolerance</label></td>
				<td ><input type="text" name="part_name_resistor_tolerance" id="part_name_resistor_tolerance" value="&#177;5%"  required disabled></td>
			</tr>

			<tr class="resistor_group partfields">
				<td><label>Resistor Footprint</label></td>
				<td ><input type="text" name="part_name_resistor_footprint" id="part_name_resistor_footprint" value="0402"  required disabled></td>
			</tr>
			<!-- /End resistor fields -->

			<!-- Fields for 'capacitor' part_type -->
			<tr class="capacitor_group partfields">
				<td><label>Capacitor Value</label></td>
				<td ><input type="text" name="part_name_capacitor_value" id="part_name_capacitor_value" value="0.0"  required disabled></td>
			</tr>

			<tr class="capacitor_group partfields">
				<td><label>Capacitor Voltage</label></td>
				<td ><input type="text" name="part_name_capacitor_voltage" id="part_name_capacitor_voltage" value="50V"  required disabled></td>
			</tr>

			<tr class="capacitor_group partfields">
				<td><label>Capacitor Footprint</label></td>
				<td ><input type="text" name="part_name_capacitor_footprint" id="part_name_capacitor_footprint" value="0402"  required disabled></td>
			</tr>
			<!-- /End capacitor fields -->

			<tr class="submit_pn">
				<td align="center"><input type="submit" value="Add Category" name="submit" class="custom_pn_submit" id="custom_pn_submit" ></td>
			</tr>
	</tbody>
</table>
</center>
		
</div>



		

       
    
    
</div>
</div>
</div>
<!---------------------escape special character--------------------------->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!------------------------------------------------------------------------>
         

<script>
$( document ).ready(function() {
	$("#part_name_type").change(function(){
		var part_fields_to_show = $(this).val();
		
		$(".partfields").hide();
		$("."+part_fields_to_show+"_group").show();
		
		var fill_part_prefix = "";

		if(part_fields_to_show == "resistor") {
			fill_part_prefix = "RES";
		} else if (part_fields_to_show == "capacitor") {
			fill_part_prefix = "CAP";
		}

		$("#part_name_prefix").val(fill_part_prefix);
	});

	$("#part_name_type").change();

	$(".watch_changes_category").change(function(){
		var cat_text_prefix = $("#prefix1").val();
		var cat_text_prefix_add = $("#prefix2").val();
		var cat_separator = "-";
		var cat_postfix = $("#postfix").val();

		var example_cpn = cat_text_prefix + cat_text_prefix_add + cat_separator + cat_postfix;
		
		$("#example_cpn").text(example_cpn);

	});

	$(".watch_changes_part_number").change(function(){
		var pn_type = $("#part_name_type").val();
		var pn_prefix = $("#part_name_prefix").val();
		var example_pn = "";

		if(pn_type == "resistor") {
			var pn_res_val = $("#part_name_resistor_value").val();
			var pn_res_tol = $("#part_name_resistor_tolerance").val();
			var pn_res_fp = $("#part_name_resistor_footprint").val();

			example_pn = pn_prefix + " " + pn_res_val + " - " + pn_res_tol + " - " + pn_res_fp;
		} else if(pn_type == "capacitor") {
			var pn_cap_val = $("#part_name_capacitor_value").val();
			var pn_cap_volt = $("#part_name_capacitor_voltage").val();
			var pn_cap_fp = $("#part_name_capacitor_footprint").val();

			example_pn = pn_prefix + " " + pn_cap_val + " - " + pn_cap_volt + " - " + pn_cap_fp;
		} else {
			example_pn = pn_prefix + " - ManufacturerName - ManufacturerPN";
		}

		$("#example_pn").text(example_pn);
	});	
	
});

$('#custom_pn_submit').click(function() {

var yourInput = $("#category").val();
re = /[`~!@#$%^&*()|+\=?;:'",.<>\{\}\[\]\\\/]/gi;
var isSplChar = re.test(yourInput);
if(isSplChar)
{
	alert("Please Avoid Special Characters");
	location.reload(); 
}
else{
	// Category/CPN Stuff
	var prefix1=$('#prefix1').val();
	prefix1=prefix1.toUpperCase();
	var prefix2=$('#prefix2').val();
	var postfix= $('#postfix').val();
	var separator=$('#separator').val();
	var category=$('#category').val();
	category =category.replace(/ /g,"_");
	
	var prefix=prefix1+prefix2;
	var pn=prefix1+prefix2+separator+postfix;
			
	// Part Name Stuff
	var part_prefix = $('#part_name_prefix').val();
	var part_type = $('#part_name_type').val();

 	var req=  $.ajax({
		url : 'add_customPn.php',
		type : 'POST',
		async: false,
		data : { 'prefix' : prefix,'postfix' : postfix,'category' : category,'pn' : pn,'separator':separator, 'part_prefix' : part_prefix, 'part_type' : part_type},
		success : function(result){
			//alert(result);
		result=result.trim();
		if(result=='PN HAS BEEN ADDED'){
		window.location.href = "<?php echo $actual_link;?>/members/custom_message.php?cat="+category;}
		else{
		alert(result);
		}
		},
		error : function(e){
		console.log(e);
		}

		});
		}
});


</script>