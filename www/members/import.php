<?php
$groupswithaccess="ladmin,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");


include("include.php");  // read css and js folders, sets database variables
session_start();

$table=$_GET['TBL'];
$_SESSION['table']=$table;



include("header.php");
include("sidebar.php");
include("include.php");

mysql_connect($host,$username,$password);																																						// connect to MySQL with credentials from demo-include.php
@mysql_select_db('zadmin_l9') or die( "Unable to select database");
$sql1=mysql_query("select * from slconfig")or die( die(mysql_error()."update failed")or die( "Unable to select database");
$row1=mysql_fetch_array($sql1);
$apikey=$row1['octoapikey'];
?>
	<div class="status_panel">
		<div class="status_sec">

			<div class="statusbar">
				<p>Current Page -
					<strong>Admin Area / Library Management / Import
						<strong>
				</p>
			</div>
			<div class="returnstat">
				<a href="admin.php" class="menu_click">Return</a>
			</div>

		</div>
	</div>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js">
	</script>

	<div class="side_work">
		<div class="working_area">
			<div class="status_panel">

			</div>
			<div class="app_bom">


				<div class="custom_panel">


					<form name="custom_pn" method="post" id="custom_pn" action='' enctype="multipart/form-data">
						<center>
							<table width="70%" cellpadding="0" cellspacing="0" class="bomname">

								<tr>
									<td>Please choose a file:</td>
									<td colspan="2">
										<input name="file" id="file" type="file" />
									</td>

									<td>
										<input type="button" value="Upload" name="submit" id='cus_pn' />
									</td>
									<!--	<td><a href="docs/bom.xls"><input type="button"  value="Download"/></a><div class="showhim"><a href="#"><img src="images/faq.png"></a><div class="showme">1.Click on Download button<br>2.Excel File will be downloaded<br>3.Fill information under the given columns correctly.<br>4.Upload Excel file to update your inventory list.<br>5.Quantity cannot be blank,either put a number or 0.<br>6.File should be in Excel 97-2003(xls) Format.</div></div></td>-->

								</tr>

							</table>


						</center>


					</form>

				</div>
				<center>
					<div id='import_report'></div>
				</center>
			</div>

		</div>

	</div>

	</div>


	<script>
		$('#cus_pn').click(function () {
			var apikey = '<?php echo $apikey;?>';

			var button = this;
			hideButton(button);

			var file = document.getElementById("file");
			var report_name;
			/* Create a FormData instance */
			var formData = new FormData();
			/* Add the file */
			formData.append("file", file.files[0]);

			$.ajax({
				url: "import_data.php",
				type: "POST",
				data: formData,
				processData: false,
				contentType: false,
				success: function (response) {
					//console.log(response);

					if (apikey == '') {

						showButton(button);
					} else {
						var obj = JSON.parse(response);
						//console.log(obj.length);

						//obj.forEach(function(entry) {
						for (var i = 0; i < obj.length; i++) {
							var manu_pn;
							var alert_msg = '';
							var tbl;
							var info;
							var info1;
							var manu;
							var valueComp;
							var des;
							var foot;
							var datasheet;
							var data_rev;
							var life;
							var rohs;
							var PartName;
							var com_doc;
							tf = obj[i].split("*");

							// console.log(tf);
							var mp = tf[0];
							var cat = tf[1];
							var partnum = tf[2];
							var sym = tf[3];
							var footgiven = tf[4];
							var row_count = tf[5];
							report_name = tf[6];
							tbl = cat.toLowerCase();
							// console.log(tbl);

							var url = 'https://octopart.com/api/v3/parts/match';
							url += '?apikey=' + apikey;
							url += '&include[]=specs';
							url += '&include[]=datasheets';
							url += '&include[]=descriptions';
							url += '&include[]=compliance_documents';
							url += '&callback=?';

							var queries = [{
								'mpn': mp
							}];

							var args = {
								queries: JSON.stringify(queries)
							};

							$.getJSON(url, args, function (response) {
								//console.log(response);
								$.each(response['results'], function (i, data) {

									info1 = data['hits'];
									if (tbl == 'resistor') {

										if (data['hits'] == 0) {
											infoinfo = 'not found';
											manu_pn = mp;
										} else {
											var part = data['items'][0]['specs'];
											var manu = data['items'][0]['manufacturer']['name'];
											var manu_pn = data['items'][0]['mpn'];

											if (typeof part['lifecycle_status'] == 'undefined') {
												life = '';

											} else {
												life = part['lifecycle_status']['value'][0];
												// console.log("life cycle:"+ life);
											}
											if (typeof part['rohs_status'] == 'undefined') {
												rohs = '';

											} else {
												rohs = part['rohs_status']['value'][0];

												// console.log("rohs:"+rohs);
											}
											if (footgiven !== '') {
												foot = footgiven;
											} else {
												if (typeof part['case_package'] == 'undefined') {
													foot = '';

												} else {
													foot = part['case_package']['value'][0];
													//console.log('R'+foot);
													foot = 'R' + foot;
												}
											}
											if (typeof part['resistance'] == 'undefined') {
												valueComp = '';
											} else {
												valueComp = part['resistance']['value'][0];

												if (valueComp == '0.0') {
													//  console.log('value:ZERO');
													valueComp = valueComp + ' - ';

												} else if (valueComp >= 1000) {

													valueComp = valueComp / 1000 + 'K';

													//console.log(valueComp);
													valueComp = valueComp + ' - ';
												} else {
													// console.log("value:"+valueComp);
													valueComp = valueComp + ' - ';
												}


											}
											if (typeof part['resistance_tolerance'] == 'undefined') {
												var tolrance = '';
											} else {
												var tolrance = part['resistance_tolerance']['value'];
												tolrance = tolrance + ' - ';
												// console.log(tolrance);
											}

											if (typeof part['power_rating'] == 'undefined') {
												var pr = '';
											} else {
												var pr = part['power_rating']['value'][0];
												//console.log(pr);
											}
											if (valueComp == '' && tolrance == '' && pr == '') {
												alert_msg = alert_msg + 'Description ';
											} else {
												des = 'RES ' + valueComp + tolrance + pr;
												//console.log("Descriprtion:"+des);
											}
											if (partnum !== '') {
												PartName = partnum;
											} else {
												if (valueComp == '' && tolrance == '' && foot == '') {
													alert_msg = alert_msg + 'PartName ';
													PartName = manu_pn;
												} else {
													PartName = valueComp + tolrance + foot;

													// console.log("partname:"+PartName);
												}
											}
											if (typeof data['items'][0]['datasheets'][0] == 'undefined') {
												alert_msg = alert_msg + 'Datasheet ';
											} else {
												datasheet = data['items'][0]['datasheets'][0]['url'];

												// data_rev = data['items'][0]['datasheets'][0]['metadata']['last_updated'];
												// console.log(datasheet+"   :"+data_rev);
											}
											if (typeof data['items'][0]['compliance_documents'][0] == 'undefined') {
												com_doc = ''
											} else {
												com_doc = data['items'][0]['compliance_documents'][0]['url'];

												// console.log(com_doc);

											}

										}
										if (alert_msg != '') {
											alert(alert_msg + ' is not found in octopart please fill this manually');
										}

									} else if (tbl == 'capacitor') {

										if (data['hits'] == 0) {
											info = 'not found';
											manu_pn = mp;
										} else {
											var part = data['items'][0]['specs'];

											manu = data['items'][0]['manufacturer']['name'];
											manu_pn = data['items'][0]['mpn'];

											//	console.log("manu:"+ manu+"  "+manu_pn);

											if (footgiven !== '') {
												foot = footgiven;
											} else {
												if (typeof part['case_package'] == 'undefined') {
													foot = '';
												} else {
													foot = part['case_package']['value'][0];
													foot = 'C' + foot;
													//	console.log("foot=C"+foot);
												}
											}
											if (typeof part['lifecycle_status'] == 'undefined') {
												life = '';

											} else {
												life = part['lifecycle_status']['value'][0];
												//   console.log("life cycle:"+life);    
											}
											if (typeof part['rohs_status'] == 'undefined') {
												rohs = '';

											} else {
												rohs = part['rohs_status']['value'][0];

												//  console.log("rohs:"+rohs);
											}
											if (typeof part['capacitance'] == 'undefined') {
												var finVal = '';
											} else {
												valueComp = part['capacitance']['value'][0];
												var dec = valueComp.substring(0, valueComp.indexOf("e"));
												var sci = valueComp.substring(valueComp.indexOf("e") + 1);
												var finVal = '';
												if (Math.abs(sci) > 6) {
													var dif = Math.abs(sci) - 6;
													var finVal = dec * Math.pow(10, -1 * dif) + 'uF';

													var postcc = finVal.substring(finVal.indexOf(".") + 1, finVal.indexOf("uF"));
													if (postcc.length >= 6) {
														var finVal = Math.round(postcc.substring(0, 6));

														finVal = finVal + 'pF';
														$('#value').val(finVal);

													} else {
														$('#value').val(finVal);
													}
												} else if (Math.abs(sci) <= 6) {
													var dif = 6 - Math.abs(sci);
													var finVal = dec * Math.pow(10, dif) + 'uF';

													var postcc = finVal.substring(finVal.indexOf(".") + 1);
													if (postcc.length >= 6) {
														var finVal = Math.round(postcc.substring(0, 6));

														finVal = finVal + 'pF';
														$('#value').val(finVal);
													} else {
														$('#value').val(finVal);
													}
												}
												var finVal = finVal + ' - ';
												valueComp = finVal;
												//console.log("value:"+valueComp);
											}
											if (typeof part['capacitance_tolerance'] == 'undefined') {
												var tolrance = '';
											} else {
												var tolrance = part['capacitance_tolerance']['value'];
												var tolrance = tolrance + ' - ';
												//  console.log("tolrance:"+tolrance);
											}
											if (typeof part['voltage_rating_dc'] == 'undefined') {
												var pr = '';
											} else {
												var pr = part['voltage_rating_dc']['value'][0];
												var pr = pr + ' - ';
												// console.log("pr:"+pr);
											}

											if (typeof part['dielectric_characteristic'] == 'undefined') {
												var dm = '';
											} else {
												var dm = part['dielectric_characteristic']['value'][0];
												//console.log("dm:"+dm);
											}
											if (finVal == '' && tolrance == '' && pr == '' && dm == '') {
												des = ''
											} else {
												des = 'CAP ' + finVal + pr + tolrance + dm;

												//console.log("des:"+des);
											}
											if (partnum !== '') {
												PartName = partnum;
											} else {
												if (finVal == '' && tolrance == '' && pr == '' && foot == '') {
													alert_msg = alert_msg + 'PartName ';
													PartName = manu_pn;
												} else {
													PartName = finVal + pr + tolrance + foot;
													//console.log("PartName:"+PartName);
												}
											}
											if (typeof data['items'][0]['datasheets'][0] == 'undefined') {
												alert_msg = alert_msg + 'Datasheet ';
											} else {
												datasheet = data['items'][0]['datasheets'][0]['url'];
												

												// data_rev = data['items'][0]['datasheets'][0]['metadata']['last_updated'];
											}
											if (typeof data['items'][0]['compliance_documents'][0] == 'undefined') {
												com_doc = ''
											} else {
												com_doc = data['items'][0]['compliance_documents'][0]['url'];
												// console.log("com_doc:"+com_doc);
											}
										}

									} else if (tbl == 'diode') {

										if (data['hits'] == 0) {
											info = 'not found';
											manu_pn = mp;
										} else {
											manu = data['items'][0]['manufacturer']['name'];
											manu_pn = data['items'][0]['mpn'];
											if (partnum !== '') {
												PartName = partnum;
											} else {
												PartName = data['items'][0]['mpn'];
											}
											var part = data['items'][0]['specs'];

											// console.log(manu_pn+'  '+manu);

											var part = data['items'][0]['specs'];
											valueComp = data['items'][0]['mpn'];

											if (typeof part['lifecycle_status'] == 'undefined') {
												life = '';

											} else {
												life = part['lifecycle_status']['value'][0];
												//console.log(life);
											}
											if (typeof part['rohs_status'] == 'undefined') {
												rohs = '';

											} else {
												rohs = part['rohs_status']['value'][0];

												//  console.log(rohs);
											}
											if (typeof part['voltage_rating_dc'] == 'undefined') {
												alert_msg = alert_msg + 'Description ';
											} else {
												var pr = part['voltage_rating_dc']['value'];
												des = 'DIODE ' + pr;
												//console.log(des);
											}

											$('#part_name').val(data['items'][0]['mpn']);
											if (footgiven !== '') {
												foot = footgiven;
											} else {
												if (typeof part['case_package'] == 'undefined') {
													foot = '';
												} else {
													foot = part['case_package']['value'][0];
													//  console.log(foot);
												}
											}

											if (typeof data['items'][0]['datasheets'][0] == 'undefined') {
												datasheet = '';
											} else {
												datasheet = data['items'][0]['datasheets'][0]['url'];

												// data_rev = data['items'][0]['datasheets'][0]['metadata']['last_updated'];
											}
											if (typeof data['items'][0]['compliance_documents'][0] == 'undefined') {
												com_doc = ''
											} else {
												com_doc = data['items'][0]['compliance_documents'][0]['url'];

												//	console.log(com_doc);
											}
										}
									} else {
										// Custom part numbers will be handled here, unless they make a 'resistors', 'capacitors' or 'diode' table
										// Consider commenting those chunks out and forcing to here
										if (data['hits'] == 0) {
											info = 'not found';
											manu_pn = mp;
										} else {
											manu = data['items'][0]['manufacturer']['name'];

											//console.log("mpn:"+manu);
											manu_pn = data['items'][0]['mpn'];
											//console.log("mpn:"+data['items'][0]['mpn']);

											if (partnum !== '') {
												PartName = partnum;
											} else {
												PartName = data['items'][0]['mpn'];
											}
											// console.log("value:"+valueComp);						  
											var part = data['items'][0]['specs'];

											if (typeof data['items'][0]['descriptions'][0] == 'undefined') {
												des = '';
											} else {
												des = data['items'][0]['descriptions'][0]['value'];
												// console.log("des:"+des);
											}
											if (footgiven !== '') {
												foot = footgiven;
											} else {
												if (typeof part['case_package'] == 'undefined') {
													foot = '';
												} else {
													foot = part['case_package']['value'][0];
													//  console.log("footprint:"+foot);
												}
											}
											if (typeof data['items'][0]['datasheets'][0] == 'undefined') {
												datasheet = '';
											} else {
												datasheet = data['items'][0]['datasheets'][0]['url'];
												//console.log("datasheet:"+datasheet);
												if (data['items'][0]['datasheets'][0]['metadata'] == null) {} else {
													// data_rev = data['items'][0]['datasheets'][0]['metadata']['last_updated'];
												}

												//console.log(data_rev);
											}
											if (typeof part['lifecycle_status'] == 'undefined') {
												life = '';

											} else {
												life = part['lifecycle_status']['value'][0];
												// console.log("lc:"+life);
											}
											if (typeof part['rohs_status'] == 'undefined') {
												rohs = '';

											} else {
												rohs = part['rohs_status']['value'][0];

												// console.log("rohs:"+rohs);
											}
											if (typeof data['items'][0]['compliance_documents'][0] == 'undefined') {
												com_doc = ''
											} else {
												com_doc = data['items'][0]['compliance_documents'][0]['url'];

												//	console.log("com_doc:"+com_doc);
											}

										}
									}

									var req = $.ajax({
										url: 'import_multiple.php',
										type: 'POST',
										data: {
											'row_count': row_count,
											'mp': mp,
											'info1': info1,
											'PartName': PartName,
											'com_doc': com_doc,
											'manu_pn': manu_pn,
											'manu': manu,
											'des': des,
											'foot': foot,
											'datasheet': datasheet,
											'data_rev': data_rev,
											'life': life,
											'rohs': rohs,
											'tbl': tbl,
											'report_name': report_name
										},
										success: function (result) {

											console.log(result);



										},
										error: function (e) {
											console.log(e);
										}
									});

								});
							});




						}

						$('#import_report').append('<a href="import/' + report_name +
							'.txt" download><input type="button" value="download report"></a>');
						showButton(button);
					}

				},
				error: function (e) {

					console.log(e);

				}
			});


		});

		function hideButton(button) {
			$(button).hide().after('<img src="../images/loading.gif" alt="loading"/>');
		}

		function showButton(button) {
			$(button).next('img').hide();
			$(button).show();

		}
	</script>