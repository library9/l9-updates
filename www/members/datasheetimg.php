<?php
$groupswithaccess="ladmin";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");


?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Library9.com</title>
<link href="styles.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript" src="../javascript/jquery-1.6.1.min.js"></script>
<!-- for navigation -->
<script type="text/javascript" src="../javascript/script-navigation.js"></script>
<!--[if IE 9 ]>
 <link href="../css/ie.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->

</head>
<body>
<?php include("header.php") ?>
<?php include("sidebar.php") ?>
		<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">

			<div class="statusbar"><p>Current Page - <strong>Admin Area / Upload Datasheet Images</strong></p></div>
			<div class="returnstat"><a href="admin.php" class="menu_click">Return</a></div>

	</div>
</div>
<div class="app_content">
<div style="width:100%; float:left; text-align:center;">

<?php if($slcustom1=='basic'){?>

<table border="0" width="100%" cellpadding="2" cellspacing="2" class="index_table">  																<!-- start table with no border, 50 % of screen width, 2 pixels spaces between cell content and cell wall, 2 pixels between cells -->	
    <tr> 																																										<!-- start table row -->
        <td align="center" style=" padding-bottom:25px;">																																		<!-- start table cell, center cell -->
            <form name="001" action="datasheets_img.php?TBL=resistor" method="post">   								<!-- start form, open page with Resistors as passed TBL variable, pass (post) data method -->
                <input type="submit" value="RESISTOR" class="res_icon">																				<!-- create submit button with text  on the button as value -->
            </form>																																							<!-- close form -->
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="006" action="datasheets_img.php?TBL=capacitor" method="post">  								<!-- User chooses Diodes to view/delete/add/edit -->
                <input type="submit" value="CAPACITOR" class="cap_icon">
            </form>
		</td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="010" action="datasheets_img.php?TBL=misc" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="MISC" class="misc_icon">
            </form>
        </td>				
    </tr>																																										<!-- end table row -->

    <tr>
        <td align="center" style=" padding-bottom:25px;">
            <form name="011" action="datasheets_img.php?TBL=connector" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="CONNECTOR" class="con_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="012" action="datasheets_img.php?TBL=ic" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="IC" class="ic_icon">
            </form>
        </td>	
        <td align="center" style=" padding-bottom:25px;">
            <form name="013" action="datasheets_img.php?TBL=diode" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="DIODE" class="diode_icon">
            </form>
        </td>			
    </tr>

    <tr>
        <td align="center" style=" padding-bottom:25px;">
            <form name="014" action="datasheets_img.php?TBL=transistor" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="TRANSISTOR" class="tran_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="015" action="datasheets_img.php?TBL=oscillator" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="OSCILLATOR" class="osc_icon">
            </form>
        </td>	
        
		</td>			
    </tr></table>
	<?php }
	
	else{
	 ?> <table border="0" width="100%" cellpadding="2" cellspacing="2" class="inner_table" align="center">  		
	  <tr>
       <?php 
		include("include.php"); 
		mysql_connect($host, $username, $password)or die("cannot connect");
        @mysql_select_db($slcustom15) or die( "Unable to select database");
          $result1 = mysql_query("SELECT cat_name FROM categories")or die(mysql_error()."update failed");
		  $i=0;

        while ($row=mysql_fetch_array($result1)) 																																									
		{ 
         
		$name1=$row["cat_name"]; 
		$name = str_replace('_', ' ', $name1);  
		if($i == 0){
        echo"<TR>";
    } ?>																																																																														
		 																												
	    <td align="center" style=" padding-bottom:25px;">
		<form name="015" action="datasheets_img.php?TBL=<?php echo $name;?>" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="<?php echo $name;?>" class="l9_icon">
			  </form> </td><?php   $i++;
			  if($i == 3)
    {
        $i = 0;
        echo"</tr>";
    }}?>  			
		
	 
	  <?php }?>
	  
</table>

	