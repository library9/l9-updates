<html>
<head>
<title>Skin Effect Calculator - Calculate Skin Depth</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content= "calculators, engineering calculators, electrical engineering calculators,  mechanical engineering calculators, physics calculators, optical engineering calculators, electronic calculators, civil engineering calculators, resistance, impedance, voltage, current, capacitance, Ohm's 
law, color code, power, resistor, capacitor, speed, distance, time, chemical periodic table, refrigeration">
<META 
content="calculators,  engineering calculators...." 
name=description>

<script language="javascript">

function sknf_calc() {
f_base=document.getElementById("sknfText1").value
f_mult=document.getElementById("sknfSelect1").value
d_mult=document.getElementById("sknfSelect2").value


freq=f_base*f_mult 
dpen=7.6/Math.sqrt(freq)*d_mult


document.getElementById("sknfText2").value=dpen.toPrecision(3)
}

function sknf_save(){
	userCookie.put("sknfText1", document.getElementById("sknfText1").value)
	
	userCookie.put("sknfSelect1", document.getElementById("sknfSelect1").value)
	userCookie.put("sknfSelect2", document.getElementById("sknfSelect2").value)
	userCookie.write()
}

userCookie  = new cookieObject("sknf_form", 365, "/", "sknfText1", "sknfSelect1", "sknfSelect2")
if (userCookie.found) {
	document.getElementById("sknfText1").value=userCookie.get("sknfText1")
	document.getElementById("sknfSelect1").value=userCookie.get("sknfSelect1")
	document.getElementById("sknfSelect2").value=userCookie.get("sknfSelect2")
}
else {
	window.status=window.status+" cookie not found"
}

sknf_calc()
window.onunload=function() { sknf_save() }
</script> 

</head>
<body bgcolor="#CCCCCC"  bottommargin="0" topmargin="0">
<table width="940" align="center" bgcolor="white">
<tr><td>
<table width="766" align="left">
        
           
        <tr>
          <td><table width="649" cellpadding="4" align="center">
  
  
               <tr> <td width="635" align="center"><font size="3" face="verdana"
        color="darkblue"><strong><b>Skin Effect Calculator</b></strong></font></td>
      </tr>
	  <tr><td><hr align="center" size="2" color="#FF0000"></td></tr>
	   <tr><td valign="top">



</td></tr>
<tr><td height="9"></td></tr>
	 
	  <tr>
                <td align="center"><font size="2" face="verdana"
        color="black"><b>Enter value and select unit. Result 
                  will be displayed.</b> </font> </td>
              </tr>
    </table>
	      
	 <p align="center"><img src="images/skin form.bmp" border="0"></p>
     
<form>  
         <table align="center">
		<tr>
                  <td colspan="3"><font size="2" face="verdana"
        color="red"><strong><b>Enter your value:</b></strong></font></td>
                </tr>
		<tr><td height="7"></td></tr>
		
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Frequency: </b></font> </td>
                   <td>  <input type="text" id="sknfText1" onkeyup="sknf_calc()" name="sknfText1" size="7" maxlength="15" >
              <SELECT id="sknfSelect1" onchange="sknf_calc()" name="sknfSelect1">
					<OPTION value="1">Hz</OPTION>
					<OPTION value="1E3">KHz</OPTION>
					<OPTION value="1E6" selected>MHz</OPTION>

					<OPTION value="1E9">GHz</OPTION>
				</SELECT> </td></tr>
			  
          <tr><td height="9"></td></tr>
			  
		<tr>
            <td colspan="3">
			<input type="reset" value="Clear" style="margin-left:3cm"></td>
      
          </tr>
		   <tr><td height="5"></td></tr>
			  
		<tr>
		<td colspan="3"><font size="2" face="verdana"
        color="red"><b>Result:</b></font> 
		</td></tr>
		
  		<tr>
		          <td colspan="3"><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Skin Depth:
                    <input type="text" id="sknfText2" onkeyup="sknf_calc()" name="sknfText2" size="7" maxlength="15">
           <SELECT id="sknfSelect2" onchange="sknf_calc()" name="sknfSelect2">
					<OPTION value="1">cm</OPTION>
					<OPTION value="10">mm</OPTION>
					<OPTION value="10000" selected>um</OPTION>
					<OPTION value="0.393701">inches</OPTION>
					<OPTION value="393.7008">mils</OPTION>

					<OPTION value="285.7045">oz/ft^2</OPTION>
				</SELECT></b></font> </td></tr>
        </table></form>
		<br>
		<!-- SiteSearch Google -->



<table border="0" bgcolor="#c3d9ff" align="center">


<tr>



<input type="hidden" name="client" value="pub-0297548667908210"></input>

<input type="hidden" name="forid" value="1"></input>

<input type="hidden" name="ie" value="ISO-8859-1"></input>

<input type="hidden" name="oe" value="ISO-8859-1"></input>

<input type="hidden" name="safe" value="active"></input>

<input type="hidden" name="cof" value="GALT:#008000;GL:1;DIV:#FFFFFF;VLC:663399;AH:center;BGC:FFFFFF;LBGC:FFFFFF;ALC:0000FF;LC:0000FF;T:000000;GFNT:0000FF;GIMP:0000FF;LH:49;LW:234;L:http://www.calculatoredge.com/links/calc2.jpg;S:http://;FORID:11"></input>

<input type="hidden" name="hl" value="en"></input>

</td></tr></table>

</form>


</body>
</html>