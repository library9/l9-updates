<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Convert Units of Pressure - Atmospheres, Feet of Water, Inches of Mercury, PSI, Inches of Water, KSC, Bars, Kilopascals, KSM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content= "calculators, engineering calculators, electrical engineering calculators,  mechanical engineering calculators, physics calculators, optical engineering calculators, electronic calculators, civil engineering calculators, resistance, impedance, voltage, current, capacitance, Ohm's 
law, color code, power, resistor, capacitor, speed, distance, time, chemical periodic table, refrigeration">
<META 
content="calculators,  engineering calculators...." 
name=description>

<script language="JavaScript" type="text/javascript">



function tempconv (zog) {
        var num1 , num2
        if (zog==1) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*33.90
                document.tempsize.tempto.value=num1
        }

        if (zog==2) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*14.50
                document.tempsize.tempto.value=num1
        }

        if (zog==3) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*1.133
                document.tempsize.tempto.value=num1
        }

        if (zog==4) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.6720
                document.tempsize.tempto.value=num1
        }

        if (zog==5) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*14.22
                document.tempsize.tempto.value=num1
        }

        if (zog==6) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.003281
                document.tempsize.tempto.value=num1
        }

        if (zog==7) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*6.8948
                document.tempsize.tempto.value=num1
        }

        if (zog==8) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*2.3067
                document.tempsize.tempto.value=num1
        }

        if (zog==9) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*2.036
                document.tempsize.tempto.value=num1
        }

        if (zog==10) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*27.677
                document.tempsize.tempto.value=num1
        }

        if (zog==11) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2+273
                document.tempsize.tempto.value=num1
        }

        if (zog==12) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=(num2+17.78)*1.8
                document.tempsize.tempto.value=num1
        }

        if (zog==13) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.0295
                document.tempsize.tempto.value=num1
        }

        if (zog==14) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.8826
                document.tempsize.tempto.value=num1
        }

        if (zog==15) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*304.8
                document.tempsize.tempto.value=num1
        }

        if (zog==16) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.4335
                document.tempsize.tempto.value=num1
        }

        if (zog==17) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.491
                document.tempsize.tempto.value=num1
        }

        if (zog==18) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.03613
                document.tempsize.tempto.value=num1
        }

        if (zog==19) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*1.488
                document.tempsize.tempto.value=num1
        }

        if (zog==20) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.0690
                document.tempsize.tempto.value=num1
        }

        if (zog==21) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.07031
                document.tempsize.tempto.value=num1
        }

        if (zog==22) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.1450
                document.tempsize.tempto.value=num1
        }

        if (zog==23) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2+460
                document.tempsize.tempto.value=num1
        }

        if (zog==24) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=(num2-32)*0.5556
                document.tempsize.tempto.value=num1
        }

        if (zog==99) {
                document.tempsize.tempfrom.value=document.tempsize.tempto.value
                document.tempsize.tempto.value=""
        }
}


//  -->
</script>
</head>
<body bgcolor="#CCCCCC"  style="margin-top:0cm; margin-bottom:0cm">
<table width="940" align="center" bgcolor="white">

       
        <tr> 
          <td> <table align="center">
              <tr> 
                <td> 
				




				 </td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><table width="649" cellpadding="4" align="center">
  
  
        <tr><td width="635" align="center"><font size="3" face="verdana"
        color="darkblue"><strong><b>Convert Units of Pressure</b></strong></font></td>
      </tr>
	  <tr><td><hr align="center" size="2" style="color:#FF0000"></td></tr>
	   <tr><td valign="top">





</td></tr>
<tr><td height="9"></td></tr>
	  <tr><td><div align="center"><font size="2" face="verdana"
        color="black"><b>Enter value and select a conversion from the buttons 
                    below and result will be displayed.</b> </font> </div></td></tr>
    </table>

    
<form name="tempsize" action="pressure.htm">
	
	<p align="center"><font size="2" face="verdana"
        color="#FF0000"><b>Enter Value:</b></font>
    <input type="text" name="tempfrom" size="7" maxlength="10" onfocus="resetvalue()"> 
      
	   <b><font size="2" face="verdana" color="#FF0000" style="text-align:center;">Result:</font></b>
	   <input type="text" name="tempto" size="12" maxlength="15"> </p>
	
      
      <table cellpadding="8" align="center">
        <tr>
          <td align="center"><font size="2"><input type="button"
          value="Atmospheres to Feet of Water" onclick="tempconv(1)" style="height:2em; width:16em"></font></td>
          <td><font size="2"><input type="button" value="Feet of Water to Atmospheres"
          onclick="tempconv(13)" style="height:2em; width:16em"></font></td>
		  <td ><font size="2"><input type="button"
          value="Feet of Water to Inches of Mercury" onclick="tempconv(14)" style="height:2em; width:16em"></font></td>
        </tr>
        <tr>
          <td align="center"><font size="2"><input type="button"
          value="Inches of Mercury to Feet of Water" onclick="tempconv(3)" style="height:2em; width:16em"></font></td>
          <td><font size="2"><input type="button"
          value="Inches of Mercury to PSI" onclick="tempconv(17)" style="height:2em; width:16em"></font></td>
          <td><input type="button" value="PSI to Inches of Mercury"
          onclick="tempconv(9)" style="height:2em; width:16em"></td>
        </tr>
        <tr>
          <td align="center"><font size="2"><input type="button"
          value="Feet of Water to PSI" onclick="tempconv(16)" style="height:2em; width:16em"></font></td>
          <td><font size="2"><input type="button" value="PSI to Feet of Water"
          onclick="tempconv(8)" style="height:2em; width:16em"></font></td>
		  <td><font size="2"><input type="button" value="PSI to Inches of Water"
          onclick="tempconv(10)" style="height:2em; width:16em"></font></td>
        </tr>
        <tr>
          <td align="center"><font size="2"><input type="button"
          value="Inches of Water to PSI" onclick="tempconv(18)" style="height:2em; width:16em"></font></td>
          <td ><font size="2"><input type="button" value="KSC to PSI"
          onclick="tempconv(5)" style="height:2em; width:16em"></font></td> 
		  <td><font size="2"><input type="button" value="Bars to PSI" onclick="tempconv(2)" style="height:2em; width:16em"></font></td>
          
        </tr>
        <tr>
		<td align="center"><font size="2"><input type="button" value="PSI to Bars"
          onclick="tempconv(20)" style="height:2em; width:16em"> </font></td>
		  <td><font size="2"><input type="button" value="PSI to KSC" onclick="tempconv(21)" style="height:2em; width:16em"></font></td>
          <td><font size="2"><input type="button"
          value="Kilopascals to PSI" onclick="tempconv(22)" style="height:2em; width:16em"></font></td>
          
        </tr>
        <tr>
		<td align="center"><font size="2"><input type="button" value="PSI to Kilopascals"
          onclick="tempconv(7)" style="height:2em; width:16em"></font></td>
		   <td><font size="2"><input type="button" value="KSM to Feet of Water"
          onclick="tempconv(6)" style="height:2em; width:16em"></font></td>
          <td><font size="2"><input type="button"
          value="Feet of Water to KSM" onclick="tempconv(15)" style="height:2em; width:16em"></font></td>
         
        </tr>
        <tr>
		<td align="center"></td>
          <td><input type="reset" value="Clear" style="height:2em; width:16em"></td>
          
        
		<td></td></tr>
      </table>
     
    </form></td>
        </tr>
	
		<tr><td>
		<!-- SiteSearch Google -->

<form method="get" action="http://www.calculatoredge.com/display.htm" target="_top">

<table border="0" bgcolor="#c3d9ff" align="center">



<tr>


<input type="hidden" name="client" value="pub-0297548667908210"> 

<input type="hidden" name="forid" value="1"> 

<input type="hidden" name="ie" value="ISO-8859-1"> 

<input type="hidden" name="oe" value="ISO-8859-1"> 

<input type="hidden" name="safe" value="active"> 

<input type="hidden" name="cof" value="GALT:#008000;GL:1;DIV:#FFFFFF;VLC:663399;AH:center;BGC:FFFFFF;LBGC:FFFFFF;ALC:0000FF;LC:0000FF;T:000000;GFNT:0000FF;GIMP:0000FF;LH:49;LW:234;L:http://www.calculatoredge.com/links/calc2.jpg;S:http://;FORID:11"> 

<input type="hidden" name="hl" value="en"> 

</td></tr></table>

</form>


		</td></tr>
		
		
		
		</table>