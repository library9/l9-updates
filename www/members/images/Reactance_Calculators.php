<html>
<head>
<title>Reactance Calculators - Calculates Capcitance and Inductance from reactance, Inductive Reactance and Capacitive Reactance</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content= "calculators, engineering calculators, electrical engineering calculators,  mechanical engineering calculators, physics calculators, optical engineering calculators, electronic calculators, civil engineering calculators, resistance, impedance, voltage, current, capacitance, Ohm's 
law, color code, power, resistor, capacitor, speed, distance, time, chemical periodic table, refrigeration">
<META 
content="calculators,  engineering calculators...." 
name=description>

<script type="text/javascript">

function calcLC() {
  var freq = form1.Fx.value
  var reac = form1.Xx.value

  var C = 1000000/(2*Math.PI*freq*reac);
  var L = reac*1000 / (2*Math.PI*freq)

  form1.cap.value = Math.round(C*10)/10; 
  form1.ind.value = Math.round(L*100)/100;

  
}

</script>
<script type="text/javascript">
function calcXL() {
  var freq1 = form2.Fx1.value
  var ind1 = form2.ind1.value

  var XL1 = 2*Math.PI*freq1*ind1 / 1000

  form2.XL1.value = Math.round(XL1*100)/100;

  
}

</script>
<script type="text/javascript">

function calcXC() {
  var freq2 = form3.Fx2.value
  var cap2 = form3.cap2.value

  var XC2 = 1000000 / (2*Math.PI*freq2*cap2);

  form3.XC2.value = Math.round(XC2*100)/100; 
}


</script>

<style type="text/css">
span
{
background-color: #FFFFCC
}
</style>

</head>
<body bgcolor="#CCCCCC"  bottommargin="0" topmargin="0">
<table width="940" align="center" bgcolor="white">
<tr><td>
<table width="766" align="left">
        </td>
        </tr>
        <tr>
          <td><table width="649" cellpadding="4" align="center">
  
  
                <td width="635" align="center"><font size="3" face="verdana"
        color="darkblue"><strong><b>Reactance Calculators</b></strong></font></td>
      </tr>
	  <tr><td><hr align="center" size="2" color="#FF0000"></td></tr>
	   <tr><td valign="top">

</td></tr>
<tr><td height="9"></td></tr>
	  <tr><td align="center"><font size="2" face="verdana"
        color="black"><b>Enter value and click on calculate. Result will be displayed.</b> </font>

</td></tr>
    </table><br>
	
	<A HREF="#reactance" style="text-decoration:none;margin-left:5cm"><font size="2" face="verdana"
        color="blue">Calculate Capacitance and Inductance from Reactance</font></A><br>

 <A HREF="#inductive" style="text-decoration:none;margin-left:5cm"><font size="2" face="verdana"
        color="blue">Calculate Inductive Reactance</font></A><br>
 <A HREF="#capacitive" style="text-decoration:none;margin-left:5cm"><font size="2" face="verdana"
        color="blue">Calculate Capacitive Reactance</font></A><br>
		
		<br><!-- SiteSearch Google -->



<table border="0" bgcolor="#e6e6e6" align="center">



<tr>






<input type="hidden" name="client" value="pub-0297548667908210"></input>

<input type="hidden" name="forid" value="1"></input>

<input type="hidden" name="ie" value="ISO-8859-1"></input>

<input type="hidden" name="oe" value="ISO-8859-1"></input>

<input type="hidden" name="safe" value="active"></input>

<input type="hidden" name="cof" value="GALT:#008000;GL:1;DIV:#FFFFFF;VLC:663399;AH:center;BGC:FFFFFF;LBGC:FFFFFF;ALC:0000FF;LC:0000FF;T:000000;GFNT:0000FF;GIMP:0000FF;LH:49;LW:234;L:http://www.calculatoredge.com/links/calc2.jpg;S:http://;FORID:11"></input>

<input type="hidden" name="hl" value="en"></input>

</td></tr></table>

</form>

<!-- SiteSearch Google -->
		<FORM name="form1"><br>
		<A NAME="reactance"></A>
	
	<p  align="center"><font size="2" face="verdana"
        color="darkblue"><b><span>Calculate Capacitance and Inductance from Reactance</span></b></font></p>  
		
		<p align="center"><img src="images/reactance L form.bmp" border="0"><br>
		<img src="images/reactance C form.bmp" border="0"></p>  

     <table align="center" style="margin-top:.3cm">
		<tr><td colspan="3"><font size="2" face="verdana"
        color="red"><strong><b>Enter your values:</b></strong></font></td></tr>
		<tr><td height="7"></td></tr>
		
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Frequency: </b></font> </td>
                  <td>  <input type="text" NAME="Fx" size="7" maxlength="15" >
             <font size="2" face="verdana"
        color="darkblue"><b> Hz</b></font> </td></tr>
			  
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Reactance: </b></font> </td>
                 <td>   <input type="text" NAME="Xx" size="7" maxlength="15">
             <font size="2" face="verdana"
        color="darkblue"><b>   ohms</b></font> </td></tr>
			  
			  
          <tr><td height="10"></td></tr>
			  
		<tr>
            <td colspan="3"><input type="Button" value="Calculate"  onClick="calcLC();" style="margin-left:1cm">
			<input type="reset" value="Clear" style="margin-left:1cm"></td>
      
          </tr>
		   <tr><td height="10"></td></tr>
			  
		<tr>
		<td colspan="3"><font size="2" face="verdana"
        color="red"><b>Results:</b></font> 
		</td></tr>
		
  		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Inductance:</b></font> </td>
                    <td> <input type="text" NAME="ind" size="7" maxlength="15">
           <font size="2" face="verdana"
        color="darkblue"><b>  mH </b></font> </td></tr>
			
					<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Capacitance:</b></font> </td>
                   <td>  <input type="text" NAME="cap" size="7" maxlength="15">
           <font size="2" face="verdana"
        color="darkblue"><b>  &micro;f </b></font> </td></tr>
        </table>
      </form>
	  <hr align="center" size="1" color="#339966" width="80%">
	<br> 
	
	  <FORM name="form2"> 
	<A NAME="inductive"></A>
	
	    <p align="center"><font size="2" face="verdana"
        color="darkblue"><b><span>Inductive Reactance Calculator</span></b></font></p> 
		
		
		<p align="center"><img  src="images/ind reactance form.bmp" border="0"></p>  
  

    <table align="center" style="margin-top:.3cm">
		<tr><td colspan="2"><font size="2" face="verdana"
        color="red"><strong><b>Enter your values:</b></strong></font></td></tr>
		<tr><td height="7"></td></tr>
		
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Frequency: </b></font> </td>
                    <td> <input type="text" NAME="Fx1" size="7" maxlength="15">
            <font size="2" face="verdana"
        color="darkblue"><b>   Hz</b></font> </td></tr>
			  
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Inductance: </b></font> </td>
                    <td> <input type="text" NAME="ind1" size="7" maxlength="15">
             <font size="2" face="verdana"
        color="darkblue"><b>    mH</b></font> </td></tr>
			  
			  
          <tr><td height="10"></td></tr>
			  
		<tr>
            <td colspan="2"><input type="Button" value="Calculate"   onClick="calcXL();" style="margin-left:1cm">
			<input type="reset" value="Clear" style="margin-left:1cm"></td>
      
          </tr>
		   <tr><td height="10"></td></tr>
			  
		<tr>
		<td colspan="2"><font size="2" face="verdana"
        color="red"><b>Result:</b></font> 
		</td></tr>
		
  		<tr>
		          <td colspan="2"><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Inductive Reactance X<sub>L</sub>:
                    <input type="text" NAME="XL1" size="7" maxlength="15">
            Ohms </b></font> </td></tr>
        </table>
      </form>
	 <hr align="center" size="1" color="#339966" width="80%">
	<br> 
	
	  <FORM name="form3"> 
	  <A NAME="capacitive"></A>
	  
	   <p align="center"><font size="2" face="verdana"
        color="darkblue"><b><span>Capacitive Reactance Calculator</span></b></font></p>  
		
		<p align="center"><img  src="images/cap reactance form.bmp" border="0"></p>    
<table align="center" style="margin-top:.3cm">
		<tr><td colspan="3"><font size="2" face="verdana"
        color="red"><strong><b>Enter your values:</b></strong></font></td></tr>
		<tr><td height="7"></td></tr>
		
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Frequency: </b></font> </td>
                  <td>  <input type="text" NAME="Fx2" size="7" maxlength="15">
              <font size="2" face="verdana"
        color="darkblue"><b>  Hz</b></font> </td></tr>
			  
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Capacitance: </b></font> </td>
                  <td>  <input type="text" NAME="cap2" size="7" maxlength="15">
               <font size="2" face="verdana"
        color="darkblue"><b>   &micro;f</b></font> </td></tr>
			  
			  
          <tr><td height="15"></td></tr>
			  
		<tr>
            <td colspan="3"><input type="Button" value="Calculate"  onClick="calcXC();" style="margin-left:1cm">
			<input type="reset" value="Clear" style="margin-left:1cm"></td> </tr>
		   <tr><td height="15"></td></tr>
			  
		<tr>
		<td colspan="3"><font size="2" face="verdana"
        color="red"><b>Result:</b></font> 
		</td></tr>
		
  		<tr>
		          <td colspan="3"><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Capacitive Reactance X<sub>C</sub>:
                    <input type="text" NAME="XC2" size="7" maxlength="15">
            Ohms </b></font> </td></tr>
        </table></form>
		<br>
<table style="margin-left:1cm"><tr><td>





		</td>
		<td>
		<P style="margin-left:1cm; margin-right:.5cm"><font size="2" face="verdana"
        color="black">
		 Reactance arises from the presence of inductance and capacitance within a circuit.  
It is generally denoted by symbol X and its SI unit is Ohm.

</font></p></td></tr></table>
		<P style="margin-left:1.7cm; margin-right:.5cm"><font size="2" face="verdana"
        color="black">
		

If X&gt;0, the reactance is said to be inductive.<br>
If X&lt;0, the reactance is said to be capacitive.

<br><br>
Inductive reactance X<sub>L</sub> is proportional to the frequency and the inductance.
<br><br>
Capacitive reactance X<sub>C</sub> is inversely proportional to the frequency and the  
capacitance.
 <br><br>
</font></P>
     
	  </td>
        </tr>
        <tr> 
          <td> <table align="center">
              <tr> 
                <td> </td>
              </tr>
            </table></td>
        </tr>
		<tr><td>
</td></tr>
      </table>
<table align="right">
<tr><td>

</td></tr>
	<tr><td>
<table align="right" width="160" height="600">
  <tr> 
    <td valign="top">
	



</td>
  </tr>
</table>

</td></tr></table>
</td></tr>
</table>
</body>
</html>