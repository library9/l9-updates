<html>
<head>
<title>Voltage Divider Calculator</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content= "calculators, engineering calculators, electrical engineering calculators,  mechanical engineering calculators, physics calculators, optical engineering calculators, electronic calculators, civil engineering calculators, resistance, impedance, voltage, current, capacitance, Ohm's 
law, color code, power, resistor, capacitor, speed, distance, time, chemical periodic table, refrigeration">
<META 
content="calculators,  engineering calculators...." 
name=description>


<SCRIPT language=JavaScript>
function cursor() {
	document.net.I.focus()
}
function getValues() {
	if (isNaN(document.net.I.value)) {
		alert ("Enter value for Input Voltage")
		document.net.I.focus()
		document.net.I.select()
		return false
	}
	if (isNaN(document.net.R1.value)) {
		alert ("Enter value for R1")
		document.net.R1.focus()
		document.net.R1.select()
		return false
	}
	if (isNaN(document.net.R2.value)) {
		alert ("Enter value for R2")
		document.net.R2.focus()
		document.net.R2.select()
		return false
	}
	if (isNaN(document.net.O.value)) {
		alert ("Enter value for Output Voltage")
		document.net.O.focus()
		document.net.O.select()
		return false
	}
	I = parseFloat(document.net.I.value)
	R1 = parseFloat(document.net.R1.value)
	R2 = parseFloat(document.net.R2.value) 
	O = parseFloat(document.net.O.value) 
	if (document.net.I.value == "" && document.net.R1.value !="" && document.net.R2.value!="" && document.net.O.value!="") {
	    I = O * (R1 + R2) / R2
	    document.net.I.value = Math.round(I * 1e3) / 1e3
	    return false
	}
	if (document.net.R1.value == "" && document.net.R2.value !="" && document.net.I.value!="" && document.net.O.value!="") {
	    R1 = (I * R2 / O) - R2
	    document.net.R1.value = Math.round(R1 * 100) / 100
	    return false
	}

	if (document.net.R2.value == "" && document.net.R1.value !="" && document.net.I.value!="" && document.net.O.value!="") {
	    R2 = O * R1 / (I - O)
	    document.net.R2.value = Math.round(R2 * 100) / 100
	    return false
	}
	if (document.net.O.value == "" && document.net.I.value !="" && document.net.R1.value!="" && document.net.R2.value!="") {
	    O = I * R2 / (R1 + R2)
	    document.net.O.value = Math.round(O * 1e3) / 1e3
	    return false
	}
}		
	
function ClearAll() {
	document.net.I.value = ""
	document.net.R1.value = ""
	document.net.R2.value = ""	
	document.net.O.value = ""	
	document.net.I.focus()
}
</SCRIPT>
</head>
<body bgcolor="#CCCCCC"  bottommargin="0" topmargin="0">
<table width="940" align="center"  bgcolor="white" >
<tr><td>
<table width="766" align="left">
       
        
        
        
        <tr>
          <td><table width="649" cellpadding="4" align="center">
  
  
                <td  align="center"><font size="3" face="verdana"
        color="darkblue"><strong><b>Voltage Divider Calculator</b></strong></font></td>
      </tr>
	  <tr><td><hr align="center" size="2" color="#FF0000"></td></tr>
	

	  
    </table>

    
            <p align="center"><font size="2" face="verdana"
        color="black"><b>Enter any three of the following values and then click 
              calculate button.</b></font></p>
			  <p align="center"><img src="images/volt.jpg" border="0"></p>
			  
			  <table align="center"><tr><td> <p><font size="2" face="verdana" color="darkblue" ><b> V<sub>in</sub> = Input Voltage  
			  <br>  V<sub>out</sub> = Output Voltage</b></font></p></td></tr></table>
			 
			  
			  <p  align="center"><img src="images/voltdivider form.bmp" border="0"></p>
	<FORM Seq="1" method="post" name="net">
	<table align="center" style="margin-top:.1cm">
	<tr><td><font size="2" face="verdana" color="red" ><b>V<sub>in</sub>:</b></font></td>
  <td><font size="2" face="verdana" color="red" ><b><input NAME="I" SIZE="9" maxlength="15" TYPE="text"  > Volts</b></font></td></tr>
 
  <tr><td><font size="2" face="verdana" color="red" ><b>R<sub>a</sub>:</b></font></td>
  <td><font size="2" face="verdana" color="red" ><b><input NAME="R1" SIZE="9" maxlength="15" TYPE="text" VALUE > Ohms</b></font></td></tr>
 
  <tr><td><font size="2" face="verdana" color="red" ><b>R<sub>b</sub>:</b></font></td>
  <td><font size="2" face="verdana" color="red" ><b><input NAME="R2" SIZE="9" maxlength="15" TYPE="text" VALUE > Ohms</b></font>
  
		</td></tr>
		 
  <tr><td><font size="2" face="verdana" color="red" ><b>V<sub>out</sub>:</b></font></td>
  <td><font size="2" face="verdana" color="red" ><b>
  <input NAME="O" SIZE="9" maxlength="15" TYPE="text" VALUE > Volts</b></font></td></tr>
  
  <tr><td height="12"></td></tr>
  
  <tr><td colspan="3"><INPUT TYPE="button"  VALUE="Calculate"
			onClick="getValues()"  name="Compute">
	<INPUT TYPE="reset" VALUE="Clear"
			onClick="ClearAll()" style="margin-left:1cm"></td></tr>
		</table>
		
		

	
			</form>
			<br>
</body>
</html>