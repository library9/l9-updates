<html>
<head>
<title>Wheatstone Bridge Calculator - Calculate Bridge Voltage and Resistance</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content= "calculators, engineering calculators, electrical engineering calculators,  mechanical engineering calculators, physics calculators, optical engineering calculators, electronic calculators, civil engineering calculators, resistance, impedance, voltage, current, capacitance, Ohm's 
law, color code, power, resistor, capacitor, speed, distance, time, chemical periodic table, refrigeration">
<META 
content="calculators,  engineering calculators...." 
name=description>

<script language="JavaScript">


function Compute1(form) {
	with(Math) {
		var Precision= 3;
		var Vb;
		
		var Vin= Number(form.Vin.value);
		var R1= Number(form.R1.value);
		var R2= Number(form.R2.value);
		var R3= Number(form.R3.value);
		var Rx= Number(form.Rx.value);

		Vb= Vin*(Rx/(R3+Rx) - R2/(R1+R2));

		form.Vb.value= Vb.toPrecision(Precision);
	}
}

function Compute2(form) {
	with(Math) {
		var Precision= 3;
		var Rx;
		
		var Vin= Number(form.Vin.value);
		var R1= Number(form.R1.value);
		var R2= Number(form.R2.value);
		var R3= Number(form.R3.value);
		var Vb= Number(form.Vb.value);
		
		//Rx= (R2*R3 + R3*(R1+R2)*(Vb/Vin)) / (R1+ (R1+R2)*(Vb/Vin));

		Rx= (R2*R3)/ R1;

		form.Rx.value= Rx.toPrecision(Precision);
	}
}

</script>

</head>
<body bgcolor="#CCCCCC"  bottommargin="0" topmargin="0">
<table width="940" align="center" bgcolor="white">
<tr><td>
<table width="766" align="left">
       
       
   
    
	</td>
        </tr>
        <tr>
          <td><table width="649" cellpadding="4" align="center">
  
  
        <td width="635" align="center"><font size="3" face="verdana"
        color="darkblue"><strong><b>Wheatstone Bridge Calculator</b></strong></font></td>
      </tr>
	  <tr><td><hr align="center" size="2" color="#FF0000"></td></tr>
	   <tr><td valign="top">



</td></tr>
<tr><td height="9"></td></tr>
	  <tr><td align="center"><font size="2" face="verdana"
        color="black"><b>Enter value and click on calculate. Result will be displayed.</b> </font>

</td></tr>
    </table>
	        <p align="center"><img src="images/bridge.jpg" border="0"></p>
			
			 <p align="center"><img src="images/bridge rx form.bmp" border="0"><br>
			<img src="images/bridge vb form.bmp" border="0"> </p>

    
<form method="POST" name="form">

       <table align="center">
		<tr><td colspan="3"><font size="2" face="verdana"
        color="red"><strong><b>Enter your values:</b></strong></font></td></tr>
		<tr><td height="7"></td></tr>
           <tr>
		<td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Input Voltage V<sub>in</sub>:</b></font> </td>
		<td colspan="2"><font size="2" face="verdana"
        color="darkblue"><b><input type="text" name="Vin" size="7" maxlength="15">
              V</b></font> </td></tr>
			  
			  <tr>
		<td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Resistance R<sub>a</sub>:</b></font> </td>
		<td colspan="2"><font size="2" face="verdana"
        color="darkblue"><b><input type="text" name="R1" size="7" maxlength="15">
              ohms</b></font> </td></tr>
			  
			  <tr>
		<td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Resistance R<sub>b</sub>:</b></font> </td>
		<td colspan="2"><font size="2" face="verdana"
        color="darkblue"><b><input type="text" name="R2" size="7" maxlength="15">
              ohms</b></font> </td></tr>
			  
		 <tr>
		<td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Resistance R<sub>c</sub>:</b></font> </td>
		<td colspan="2"><font size="2" face="verdana"
        color="darkblue"><b><input type="text" name="R3" size="7" maxlength="15">
              ohms</b></font> </td></tr>
			  
			  <tr><td height="15"></td></tr>
			  
		<tr>
            <td colspan="3">
			<input type="reset" value="Clear" style="margin-left:1cm"></td>
      
          </tr>
		   <tr><td height="15"></td></tr>
			  
		<tr>
		<td colspan="3"><font size="2" face="verdana"
        color="red"><b>Results:</b></font> 
		</td></tr>
		
		<tr>
		<td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Resistance R<sub>x</sub>: </b></font></td>
	<td>	<input type="text" name="Rx" size="7" maxlength="15"></td>
            <td> <font size="2" face="verdana"
        color="darkblue"><b> ohms</b></font> </td>
			 <td> <input type="button" value="Calculate" name="B2" onClick="Compute2(document.form);"></td></tr>
			  
		<tr>
		<td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Bridge Voltage V<sub>b</sub>:</b></font></td>
		<td><input type="text" name="Vb" size="7" maxlength="15"></td>
             <td> <font size="2" face="verdana"
        color="darkblue"><b>    V</b></font></td>
		  <td> <input type="button" value="Calculate" name="B1" onClick="Compute1(document.form);"></td></tr>
         </table></form>
		 
	
</body>
</html>