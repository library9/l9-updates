<html>
<head>
<title>Velocity of Sound Calculator</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content= "calculators, engineering calculators, electrical engineering calculators,  mechanical engineering calculators, physics calculators, optical engineering calculators, electronic calculators, civil engineering calculators, resistance, impedance, voltage, current, capacitance, Ohm's 
law, color code, power, resistor, capacitor, speed, distance, time, chemical periodic table, refrigeration">
<META 
content="calculators,  engineering calculators...." 
name=description>

<SCRIPT language=JavaScript>
function calc1(theForm) 
	{ 
		var kapa =
		Math.round(theForm.t6.value*100)/100; var pk = kapa /(kapa - 1);
		theForm.t1.value=Math.round(theForm.t1.value*10)/10;
		theForm.t2.value=Math.round(theForm.t2.value*100)/100;
		theForm.t3.value=Math.round(theForm.t3.value*10)/10;
		theForm.t4.value=Math.round(theForm.t4.value*10)/10;
		theForm.t5.value=Math.round(theForm.t1.value*theForm.t3.value/theForm.t2.value/101325*100000)/100000;
		theForm.t6.value=Math.round(theForm.t5.value*theForm.t4.value*100000)/100000;
		theForm.t7.value=Math.round((1/theForm.t5.value)*10000)/10000; } function
		dfault1(theForm) { theForm.t1.value=298; theForm.t2.value=1;
		theForm.t3.value=286.7; theForm.t4.value=1; theForm.t5.value=0.84319;
		theForm.t6.value=0.84319; theForm.t7.value=1.186; } function calc2(theForm) {
		theForm.t8.value=Math.round(theForm.t8.value*10)/10;
		theForm.t9.value=Math.round(theForm.t9.value*100)/100;
		theForm.t10.value=Math.round(theForm.t10.value*10)/10;
		theForm.t11.value=Math.round(Math.pow(theForm.t8.value*theForm.t9.value*theForm.t10.value,0.5)*10)/10;
		theForm.t12.value=Math.round(3.281*theForm.t11.value*10)/10; } function
		dfault2(theForm) { theForm.t8.value=298; theForm.t9.value=1.4;
		theForm.t10.value=286.7; theForm.t11.value=345.8; theForm.t12.value=1134.6;
}
</SCRIPT>

</head>
<body bgcolor="#CCCCCC"  bottommargin="0" topmargin="0">
<table width="940" align="center" bgcolor="white">
<tr><td>
<table width="766" align="left">
       
        
        <tr>
          <td><table width="649" cellpadding="4" align="center">
  
  
                <td width="635" align="center"><font size="3" face="verdana"
        color="darkblue"><strong><b>Velocity of Sound</b></strong></font></td>
      </tr>
	  <tr><td><hr align="center" size="2" color="#FF0000"></td></tr>
	   <tr><td valign="top">


</td></tr>
<tr><td height="9"></td></tr>
	  <tr><td align="center"><font size="2" face="verdana"
        color="black"><b>Enter value and click on calculate. Result will be displayed.</b> </font>

</td></tr>
    </table>
	  <p style="margin-left:3cm"><img src="images/ms form.bmp" border="0"> <br>
	  <img src="images/ft form.bmp" border="0"></p>     

    
<FORM>
        <table align="center">
		<tr><td><font size="2" face="verdana"
        color="red"><strong><b>Enter your values:</b></strong></font></td></tr>
		<tr><td height="7"></td></tr>
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Temperature:  </b></font>
                  </td> <td> <input type="text" name=t8 size="7" maxlength="15"> </td>
             <td><font size="2" face="verdana"
        color="darkblue"><b> Kelvin</b></font> </td></tr>
			  
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Gamma:  </b></font> </td>
                 <td>  <input type="text" name=t9 size="7" maxlength="15"></td> 
        </tr>
			  
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Gas Constant: </b></font> </td>
                 <td>   <input type="text" name=t10 size="7" maxlength="15" > </td>
             <td><font size="2" face="verdana"
        color="darkblue"><b> J/kg*K</b></font> </td></tr>
			 
          <tr><td height="15"></td></tr>
			  
		<tr>
            <td colspan="3"><INPUT onclick=calc2(this.form) type=button value=Calculate name=B3 style="margin-left:1cm">
			<input type="reset" value="Clear" style="margin-left:1cm"></td>
      
          </tr>
		   <tr><td height="15"></td></tr>
			  
		<tr>
		<td colspan="3"><font size="2" face="verdana"
        color="red"><b>Results:</b></font> 
		</td></tr>
		
  		<tr><td colspan="3"><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Velocity of Sound: 
                   <input name=t11 SIZE="10" maxlength="24" >
          m/s</b></font> </td></tr>
		   
		 		<tr><td colspan="3"><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Velocity of Sound: 
                   <input name=t12 SIZE="10" maxlength="24">
           ft/s</b></font> </td></tr>
		
        </table>
      </form>
	

</body>
</html>