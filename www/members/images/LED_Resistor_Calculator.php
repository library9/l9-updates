<html>
<head>
<title>LED Resistor Calculator</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content= "calculators, engineering calculators, electrical engineering calculators,  mechanical engineering calculators, physics calculators, optical engineering calculators, electronic calculators, civil engineering calculators, resistance, impedance, voltage, current, capacitance, Ohm's 
law, color code, power, resistor, capacitor, speed, distance, time, chemical periodic table, refrigeration">
<META 
content="calculators,  engineering calculators...." 
name=description>

<script language="JavaScript">

function log10(x) {
	with(Math) {
		return log(x)/log(10);
	}
}

function Compute1(form) {
	with(Math) {
		var Vf= Number(form.Vf.value);
		var Vs= Number(form.Vs.value);
		var Imax= Number(form.Imax.value);
		var R;
		var Precision=3;
		
		R= (Vs-Vf)/Imax*1000;
		
		form.R.value= R.toPrecision(Precision);		
	}
}



</script>


</head>
<body bgcolor="#CCCCCC"  bottommargin="0" topmargin="0">
<table width="940" align="center" bgcolor="white">
<tr><td>
<table width="766" align="left">

        <tr>
          <td>
	       

     <br>
<form method="POST" name="form">  
        <table align="center">
		<tr><td colspan="3"><font size="2" face="verdana"
        color="red"><strong><b>Enter your values:</b></strong></font></td></tr>
		<tr><td height="7"></td></tr>
		
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Source Voltage: </b></font> </td>
                    <td> <input type="text" name="Vs" size="7" maxlength="15">
              <font size="2" face="verdana"
        color="darkblue"><b>Volts</b></font> </td></tr>
			  
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Forward LED voltage: </b></font> </td>
                   <td>  <input type="text" name="Vf" size="7" maxlength="15">
               <font size="2" face="verdana"
        color="darkblue"><b> Volts</b></font> </td></tr>
			  
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">LED Current: </b></font> </td>
                   <td>  <input type="text" name="Imax" size="7" maxlength="15">
            <font size="2" face="verdana"
        color="darkblue"><b>  miliAmps</b></font> </td></tr>
			  
          <tr><td height="15"></td></tr>
			  
		<tr>
            <td colspan="3"><input type="Button" value="Calculate" name="B1" onClick="Compute1(document.form);" style="margin-left:1cm">
			<input type="reset" value="Clear" style="margin-left:1cm"></td>
      
          </tr>
		   <tr><td height="15"></td></tr>
			  
		<tr>
		<td colspan="3"><font size="2" face="verdana"
        color="red"><b>Results:</b></font> 
		</td></tr>
		
  		<tr>
		          <td colspan="3"><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Resistor:
                    <input type="text" name="R" size="7" maxlength="15">
            Ohms </b></font> </td></tr>
        </table>
      </form></td>
        </tr>
		<tr><td><table style="margin-left:1cm"><tr><td>
		<script type="text/javascript"><!--

google_ad_client = "pub-0297548667908210";

/*  */

google_ad_slot = "1323180162";

google_ad_width = 234;

google_ad_height = 60;

google_cpa_choice = ""; // on file

//-->

</script>

<script type="text/javascript"

src="http://pagead2.googlesyndication.com/pagead/show_ads.js">

</script>


		</td>
		<td>
		
</body>
</html>