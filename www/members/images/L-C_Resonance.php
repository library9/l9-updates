<html>
<head>
<title>Inductance Capacitance Resonance Calculator</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content= "calculators, engineering calculators, electrical engineering calculators,  mechanical engineering calculators, physics calculators, optical engineering calculators, electronic calculators, civil engineering calculators, resistance, impedance, voltage, current, capacitance, Ohm's 
law, color code, power, resistor, capacitor, speed, distance, time, chemical periodic table, refrigeration">
<META 
content="calculators,  engineering calculators...." 
name=description>
<script type="text/javascript">
function compute(nawobj) {
var Precision=7;
cscale = 
1/(1000000 * (Math.pow(10, 3*(document.form.C_units.selectedIndex))))   

lscale = 
1/(Math.pow(10, 3*(document.form.L_units.selectedIndex)))

fscale = 
1/(Math.pow(10, 3*(document.form.F_units.selectedIndex)))

cin = parseFloat(nawobj.parm1.value)
lin = parseFloat(nawobj.parm2.value)

c = cin*cscale
l = lin*lscale

f = 1/(2*Math.PI*Math.sqrt(l*c))

nawobj.result.value  = (f*fscale).toPrecision(Precision)

}
</script>
</head>
<body bgcolor="#CCCCCC"  bottommargin="0" topmargin="0">
<table width="940" align="center" bgcolor="white">
<tr><td>
<table width="766" align="left">
       
        
        
        <tr>
          <td><table width="649" cellpadding="4" align="center">
  
  
        <td width="635" align="center"><font size="3" face="verdana"
        color="darkblue"><strong><b>L-C Resonance Calculator</b></strong></font></td>
      </tr>
	  <tr><td><hr align="center" size="2" color="#FF0000"></td></tr>
	   <tr><td valign="top">



</td></tr>
<tr><td height="9"></td></tr>
	  <tr><td align="center"><font size="2" face="verdana"
        color="black"><b>Enter any two of the following values and press the Calculate button.</b> </font>

</td></tr>
    </table>
	       
<br>
  
 <FORM NAME="form">
      
    <table align="center">
		<tr><td colspan="3"><font size="2" face="verdana"
        color="red"><strong><b>Enter your values:</b></strong></font></td></tr>
		<tr><td height="7"></td></tr>
		
        <tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Capacitance:
                    <INPUT TYPE=text NAME="parm1" size="10" maxlength="15">
					<SELECT NAME="C_units"> 
				<OPTION>Microfarads
				<OPTION>Nanofarads
				<OPTION>Picofarads
				</SELECT>
              </b></font> </td></tr>

        <tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Inductance:
                    <INPUT TYPE=text NAME="parm2" size="10" maxlength="15" style="margin-left:.2cm">
					<SELECT NAME="L_units" SIZE=1><OPTION>Henries
				<OPTION>Millihenries
				<OPTION>Microhenries
				<OPTION>Nanohenries
				</SELECT>
             </b></font> </td></tr>
			  
        <tr><td height="15"></td></tr>
			  
		<tr>
            <td><input type="button" value="Calculate" name="B1" onClick="compute(this.form)" style="margin-left:1cm">
			<input type="reset" value="Clear" style="margin-left:1cm"></td>
      
          </tr>
		   <tr><td height="15"></td></tr>
		   <tr>
		<td><font size="2" face="verdana"
        color="red"><b>Results:</b></font> 
		</td></tr>
		   <tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm"> Frequency (MHz):
                    <INPUT TYPE=text NAME="result" size="10" maxlength="25" >
             <SELECT NAME="F_units" SIZE=1>
			<OPTION>Herz
			<OPTION>Kiloherz
			<OPTION>Megaherz
			<OPTION>Gigaherz
			</SELECT></b></font> </td></tr>
      </table>

      </form>      </td>
        </tr>
		<tr><td><table style="margin-left:1cm"><tr><td>





		</td>
		<td>
		
</body>
</html>