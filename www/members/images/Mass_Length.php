<html>
<head>
<title>Convert Units of Mass, Length, and Area - Centimeters, Feets, Meters, Millimeters, Yards, Sq. Centimeters, Sq. Feet, Kilograms, Pounds</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content= "calculators, engineering calculators, electrical engineering calculators,  mechanical engineering calculators, physics calculators, optical engineering calculators, electronic calculators, civil engineering calculators, resistance, impedance, voltage, current, capacitance, Ohm's 
law, color code, power, resistor, capacitor, speed, distance, time, chemical periodic table, refrigeration">
<META 
content="calculators,  engineering calculators...." 
name=description>

<script language="JavaScript">
function massconv (zog) {
        var num1 , num2

// Centimeters to Feet
        if (zog==1) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*0.0328
                document.masssize.massto.value=num1
        }

// Centimeters to Inches
        if (zog==2) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*0.3937
                document.masssize.massto.value=num1
        }

// Centimeters to Yards
        if (zog==3) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*0.01094
                document.masssize.massto.value=num1
        }

// Kilograms to Pounds
        if (zog==4) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*2.205
                document.masssize.massto.value=num1
        }

// Meters to Feet
        if (zog==5) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*3.281
                document.masssize.massto.value=num1
        }

// Meters to Yards
        if (zog==6) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*1.094
                document.masssize.massto.value=num1
        }

// Millimeters to Inches
        if (zog==7) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*0.03937
                document.masssize.massto.value=num1
        }

// Square Centimeters to Square Feet
        if (zog==8) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*0.001076
                document.masssize.massto.value=num1
        }

// Square Centimeters to Square Inches
        if (zog==9) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*0.1550
                document.masssize.massto.value=num1
        }

// Feet to Centimeters
        if (zog==10) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*30.48
                document.masssize.massto.value=num1
        }

// Feet to Meters
        if (zog==11) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*0.3048
                document.masssize.massto.value=num1
        }

// Inches to Centimeters
        if (zog==12) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*2.54
                document.masssize.massto.value=num1
        }

// Inches to Millimeters
        if (zog==13) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*25.40
                document.masssize.massto.value=num1
        }

// Pounds to Kilograms
        if (zog==14) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*0.4536
                document.masssize.massto.value=num1
        }

// Square Feet to Square Centimeters
        if (zog==15) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*929.03
                document.masssize.massto.value=num1
        }

// Square Inches to Square Centimeters
        if (zog==16) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*6.452
                document.masssize.massto.value=num1
        }

// Yards to Centimeters
        if (zog==17) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*91.44
                document.masssize.massto.value=num1
        }

// Yards to Meters
        if (zog==18) {
                num2=parseFloat(document.masssize.massfrom.value)
                num1=num2*0.9144
                document.masssize.massto.value=num1
        }

// Transfer Result to Input
        if (zog==99) {
                document.masssize.massfrom.value=document.masssize.massto.value
                document.masssize.massto.value=""
        }
}


// Stop hiding the script -->
</script>
</head>
<body bgcolor="#CCCCCC"  bottommargin="0" topmargin="0">
<table width="940" align="center" bgcolor="white">

     
        <tr>
          <td><table width="649" cellpadding="4" align="center">
  <tr>
  
        <td align="center"><font size="3" face="verdana"
        color="darkblue"><strong><b>Convert Units of Mass, Length, and Area</b></strong></font></td>
      </tr>
	  <tr><td><hr align="center" size="2" color="red"></td></tr>
	   <tr><td valign="top">



</td></tr>
<tr><td height="9"></td></tr>
	  <tr><td><div align="center"><font size="2" face="verdana"
        color="black"><b>Enter value and select a conversion from the buttons 
                    below and result will be displayed.</b> </font> </div></td></tr>
    </table>
	 <form name="masssize">
      <p align="center"><font size="2" face="verdana"
        color="#FF0000"><b>Enter Value:</b></font>
    
	  <input type="text" name="massfrom" size="12" maxlength="15" onfocus="resetvalue()">  
	  <b align="center"><font size="2" face="verdana" color="#FF0000">Result:</font></b>
	  <input type="text" name="massto" size="12" maxlength="15"></p>
      <table cellpadding="8" align="center">
        <tr>
          <td  align="center"><font size="2"><input type="button"
          value="Centimeters to Feet" onclick="massconv(1)" style="height:2em; width:14em"></font></td>
          <td><font size="2"><input type="button" value="Feet to Centimeters"
          onclick="massconv(10)" style="height:2em; width:14em"></font></td>
		   <td valign="top"><font size="2"><input type="button" value="Inches to Centimeters"
          onclick="massconv(12)" style="height:2em; width:14em"></font></td>
        </tr>
        <tr>
          <td align="center"><font size="2"><input type="button"
          value="Centimeters to Inches" onclick="massconv(2)" style="height:2em; width:14em"></font></td>
         <td><font size="2"><input type="button"
          value="Millimeters to Inches" onclick="massconv(7)" style="height:2em; width:14em"></font></td>
          <td ><font size="2"><input type="button" value="Inches to Millimeters"
          onclick="massconv(13)" style="height:2em; width:14em"></font></td>
        </tr>
        <tr>
          <td  align="center"><font size="2"><input type="button"
          value="Centimeters to Yards" onclick="massconv(3)" style="height:2em; width:14em"></font></td>
          <td><font size="2"><input type="button" value="Yards to Centimeters"
          onclick="massconv(17)" style="height:2em; width:14em"></font></td>
		  <td valign="top" align="right"><font size="2"><input type="button" value="Meters to Feet"
          onclick="massconv(5)" style="height:2em; width:14em"></font></td>
        </tr>
        <tr>
          
          <td align="center"><font size="2"><input type="button" value="Feet to Meters"
          onclick="massconv(11)" style="height:2em; width:14em"></font></td>
        
          <td><font size="2"><input type="button" value="Meters to Yards"
          onclick="massconv(6)" style="height:2em; width:14em"></font></td>
          <td><font size="2"><input type="button" value="Yards to Meters"
          onclick="massconv(18)" style="height:2em; width:14em"></font></td>
        </tr>
        <tr>
          <td align="center"><font size="2"><input type="button"
          value="Sq. Centimeters to Sq. Feet" onclick="massconv(8)" style="height:2em; width:14em"></font></td>
          <td><font size="2"><input type="button" value="Sq. Feet to Sq. Centimeters"
          onclick="massconv(15)" style="height:2em; width:14em"></font></td>
		  <td valign="top"><font size="2"><input type="button" value="Sq. Inches to Sq. Centimeters"
          onclick="massconv(16)" style="height:2em; width:14em"></font></td>
        </tr>
        <tr>
          <td align="center"><font size="2"><input type="button"
          value="Sq. Centimeters to Sq. Inches" onclick="massconv(9)" style="height:2em; width:14em"></font></td>
          
       
          <td ><font size="2"><input type="button"
          value="Kilograms to Pounds" onclick="massconv(4)" style="height:2em; width:14em"></font></td>
          <td ><font size="2"><input type="button" value="Pounds to Kilograms"
          onclick="massconv(14)" style="height:2em; width:14em"></font></td>
        </tr>
		<tr><td><input type="hidden" value="hidden" name="hidden" style="height:2em; width:14em"></td>	
		<td><input type="reset" value="Clear" name="reset" style="height:2em; width:14em"></td></tr>
      </table>
      
    </form>





</body>
</html>