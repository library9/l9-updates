<html>
<head>
<title>Microstrip Inductor Calculator - Calculates Inductance of Microstrip </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content= "calculators, engineering calculators, electrical engineering calculators,  mechanical engineering calculators, physics calculators, optical engineering calculators, electronic calculators, civil engineering calculators, resistance, impedance, voltage, current, capacitance, Ohm's 
law, color code, power, resistor, capacitor, speed, distance, time, chemical periodic table, refrigeration">
<META 
content="calculators,  engineering calculators...." 
name=description>

<script language="JavaScript">



function Compute(form) {
	with(Math) {
		var b= Number(form.b.value);
		var w= Number(form.w.value);
		var h= Number(form.h.value);
		
		var L;
		var Precision= 5;
		
		L= 0.00508*b*(log(2*b/(w+h))+.5+0.2235*(w+h)/b);
		

		form.L.value= L.toFixed(Precision);
	}
}


</script>

</head>
<body bgcolor="#CCCCCC"  bottommargin="0" topmargin="0">
<table width="940" align="center" bgcolor="white">
<tr><td>
<table width="766" align="left">
     
        
          
 
 

 


				 </td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><table width="649" cellpadding="4" align="center">
  
  
        <td width="635" align="center"><font size="3" face="verdana"
        color="darkblue"><strong><b>Microstrip Inductor Calculator</b></strong></font></td>
      </tr>
	  <tr><td><hr align="center" size="2" color="#FF0000"></td></tr>
	   <tr><td valign="top">


</td></tr>
<tr><td height="9"></td></tr>
	  <tr><td align="center"><font size="2" face="verdana"
        color="black"><b>Enter value and click on calculate. Result will be displayed.</b> </font>

</td></tr>
    </table>
	       

     <br>
<form method="POST" name="form">  
        <table align="center">
		<tr><td colspan="3"><font size="2" face="verdana"
        color="red"><strong><b>Enter your values:</b></strong></font></td></tr>
		<tr><td height="7"></td></tr>
		
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Strip Width: </b></font> </td>
                  <td>  <input type="text" name="w" size="7" maxlength="15">
             <font size="2" face="verdana"
        color="darkblue"><b> inches</b></font> </td></tr>
			  
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Strip Length: </b></font> </td>
                <td>    <input type="text" name="b" size="7" maxlength="15">
               <font size="2" face="verdana"
        color="darkblue"><b>  inches</b></font> </td></tr>
			  
		<tr>
		          <td><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Distance: </b></font> </td>
                 <td>   <input type="text"  name="h" size="7" maxlength="15">
            <font size="2" face="verdana"
        color="darkblue"><b>  inches</b></font> </td></tr>
			  
          <tr><td height="15"></td></tr>
			  
		<tr>
            <td colspan="3"><input type="Button" value="Calculate" name="B1" onClick="Compute(document.form)" style="margin-left:1cm">
			<input type="reset" value="Clear" style="margin-left:1cm"></td>
      
          </tr>
		   <tr><td height="15"></td></tr>
			  
		<tr>
		<td colspan="3"><font size="2" face="verdana"
        color="red"><b>Results:</b></font> 
		</td></tr>
		
  		<tr>
		          <td colspan="3"><font size="2" face="verdana"
        color="darkblue"><b style="margin-left:.5cm">Inductance:
                    <input type="text" name="L" size="7" maxlength="15">
            uH </b></font> </td></tr>
        </table>
      </form></td>
        </tr>
		<tr><td><table style="margin-left:1cm"><tr><td>
		




		</td>
		<td>
		<!-- SiteSearch Google -->



<!-- SiteSearch Google -->
		</td>
		</tr></table></td></tr>
        <tr> 
          <td> </td>
        </tr>
		<tr></tr>
		<tr><td>
		</td></tr>
      </table>
<table align="right">
<tr><td>

</td></tr>
	<tr><td>
<table align="right" width="160" height="600">
  <tr> 
    <td valign="top">


</td>
  </tr>
</table>

</td></tr></table>
</td></tr>
</table>
</body>
</html>