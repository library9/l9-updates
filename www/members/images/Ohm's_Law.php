<html>
<head>
<title>Ohm's Law Calculator - Calculates Watts, Amperes, Volts, Ohms</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content= "calculators, engineering calculators, electrical engineering calculators,  mechanical engineering calculators, physics calculators, optical engineering calculators, electronic calculators, civil engineering calculators, resistance, impedance, voltage, current, capacitance, Ohm's 
law, color code, power, resistor, capacitor, speed, distance, time, chemical periodic table, refrigeration">
<META 
content="calculators,  engineering calculators...." 
name=description>

<script language="JavaScript">

function divide_by_zero_error(form) {
  form.result.value = ""
  alert("Error: you cannot divide by zero")
}

function compute_watts(form) {
  if ((form.e.value != "") && (form.i.value != "") && (form.r.value != "")) {
    form.result.value = 0
    alert("Please enter only two of E, I, and R")
    }
  else if (((form.e.value == "") && (form.i.value == "") && (form.r.value == "")) ||
           ((form.e.value == "") && (form.i.value == "") && (form.r.value != "")) ||
           ((form.e.value == "") && (form.i.value != "") && (form.r.value == "")) ||
           ((form.e.value != "") && (form.i.value == "") && (form.r.value == ""))) {
    form.result.value = 0
    alert("Please enter two of E, I, and R")
    }
  else if (form.e.value == "")
    form.result.value = eval(form.i.value) * eval(form.i.value) * eval(form.r.value)
  else if (form.i.value == "") {
    if (eval(form.r.value) == 0)
      divide_by_zero_error(form)
    else
      form.result.value = eval(form.e.value) * eval(form.e.value) / eval(form.r.value)
  }
  else if (form.r.value == "")
    form.result.value = eval(form.e.value) * eval(form.i.value)
}
 


function compute_amps(form) {
  if ((form.ee.value != "") && (form.ww.value != "") && (form.rr.value != "")) {
    form.resulta.value = ""
    alert("Please enter only two of E, W, and R")
    }
  else if (((form.ee.value == "") && (form.ww.value == "") && (form.rr.value == "")) ||
           ((form.ee.value == "") && (form.ww.value == "") && (form.rr.value != "")) ||
           ((form.ee.value == "") && (form.ww.value != "") && (form.rr.value == "")) ||
           ((form.ee.value != "") && (form.ww.value == "") && (form.rr.value == ""))) {
    form.resulta.value = ""
    alert("Please enter two of E, W, and R")
    }
  else if (form.cb.checked) {
    if (form.rr.value != "")
      alert("Only enter E and W when using the 3-phase calculation")
    else if (eval (form.ee.value) == 0)
      divide_by_zero_error(form)
    else
      form.resulta.value = eval(form.ww.value) / (eval(form.ee.value) * 1.73)
  }

  else if (form.ee.value == "") {
    if (eval(form.rr.value) == 0)
      divide_by_zero_error(form)
    else
      form.resulta.value = Math.sqrt(eval(form.ww.value) / eval(form.rr.value))
  }
  else if (form.ww.value == "") {
    if (eval(form.rr.value) == 0)
      divide_by_zero_error(form)
    else
      form.resulta.value = eval(form.ee.value) / eval(form.rr.value)
  }
  else if (form.rr.value == "") {
    if (eval(form.ee.value) == 0)
      divide_by_zero_error(form)
    else
      form.resulta.value = eval(form.ww.value) / eval(form.ee.value)
  }
}
 

function compute_volts(form) {
  if ((form.wv.value != "") && (form.iv.value != "") && (form.rv.value != "")) {
    form.resultv.value = 0
    alert("Please enter only two of W, I, and R")
    }
  else if (((form.wv.value == "") && (form.iv.value == "") && (form.rv.value == "")) ||
           ((form.wv.value == "") && (form.iv.value == "") && (form.rv.value != "")) ||
           ((form.wv.value == "") && (form.iv.value != "") && (form.rv.value == "")) ||
           ((form.wv.value != "") && (form.iv.value == "") && (form.rv.value == ""))) {
    form.resultv.value = 0
    alert("Please enter two of W, I, and R")
    }
  else if (form.wv.value == "")
    form.resultv.value = eval(form.iv.value) * eval(form.rv.value)
  else if (form.iv.value == "")
    form.resultv.value = Math.sqrt(eval(form.wv.value) * eval(form.rv.value))
  else if (form.rv.value == "") {
    if (eval(form.iv.value) == 0)
      divide_by_zero_error(form)
    else
      form.resultv.value = eval(form.wv.value) / eval(form.iv.value)
  }
}
 


function compute_ohms(form) {
  if ((form.eo.value != "") && (form.io.value != "") && (form.wo.value != "")) {
    form.resulto.value = 0
    alert("Please enter only two of E, I, and W")
    }
  else if (((form.eo.value == "") && (form.io.value == "") && (form.wo.value == "")) ||
           ((form.eo.value == "") && (form.io.value == "") && (form.wo.value != "")) ||
           ((form.eo.value == "") && (form.io.value != "") && (form.wo.value == "")) ||
           ((form.eo.value != "") && (form.io.value == "") && (form.wo.value == ""))) {
    form.resulto.value = 0
    alert("Please enter two of E, I, and W")
    }
  else if (form.eo.value == "") {
    if (eval(form.io.value) == 0)
      divide_by_zero_error(form)
    else
      form.resulto.value = eval(form.wo.value) / eval(form.io.value) * eval(form.io.value)
  }
  else if (form.io.value == "") {
    if (eval(form.wo.value) == 0)
      divide_by_zero_error(form)
    else
      form.resulto.value = eval(form.eo.value) * eval(form.eo.value) / eval(form.wo.value)
  }
  else if (form.wo.value == "") {
    if (eval(form.io.value) == 0)
      divide_by_zero_error(form)
    else
      form.resulto.value = eval(form.eo.value) / eval(form.io.value)
  }
}

</script>
<head>

<style type="text/css">
table.one
{
border: medium solid darkblue;

}

table.two
{
border: medium solid darkblue 
}

table.three
{
border: medium solid darkblue 
}

table.four
{
border: medium solid darkblue 
}
</style>

</head>
<body bgcolor="#CCCCCC"  bottommargin="0" topmargin="0">
<table width="940" align="center" bgcolor="white" >
<tr><td>
<table width="766" align="left">


    
       
  
        <tr>
          <td><table width="649" cellpadding="4" align="center">
  
  
        <td width="635" align="center"><font size="3" face="verdana"
        color="darkblue"><strong><b >Ohm's Law Calculator</b></strong></font></td>
      </tr>
	  <tr><td><hr align="center" size="2" color="#FF0000"></td></tr>
	   <tr><td valign="top">


</td></tr>
<tr><td height="9"></td></tr>
    </table>

    
<table align="center" width="85%">
	
<form>
<tr>
    <td width="340">
<table width="95%" height="227"   class="one">
          <tr>
            <td> <font size="2" face="Verdana" color="darkblue"><b style="margin-left:1cm" >Calculate 
              Watts w/ Ohm's Law:</b></font> <br>
<br>
<FONT SIZE="2" face="Verdana" color="red">
<B style="margin-left:1cm">Enter any two of: E, I, and R</B><br><br>
<b style="margin-left:0.1cm">E:</b>
<INPUT TYPE="text" NAME="e" SIZE=6  maxlength="10">
<b style="margin-left:0.1cm">I:</b>
<INPUT TYPE="text" NAME="i" SIZE=6 maxlength="10" >
<b style="margin-left:0.1cm">R:</b>
<INPUT TYPE="text" NAME="r" SIZE=6 maxlength="10"><br><br>
<INPUT TYPE="button"  VALUE="Calculate Watts" ONCLICK="compute_watts(this.form)" style="margin-left:2.5cm"><br>
<BR>
<b style="margin-left:0.5cm">Watts:</b>
<INPUT TYPE="text" NAME="result" SIZE=15 >
<BR><br><br>
</FONT></td></tr>
</table>
</td>
<td width="340">
<table  width="95%" height="227" class="two">
<tr><td><font size="2" face="Verdana" color="darkblue"><b style="margin-left:1cm" >Calculate Amps w/ Ohm's Law:</b></font>
<br>
<br>
<FONT SIZE="2" face="Verdana" color="red">
<B style="margin-left:1cm">Enter any two of: E, W, and R</B><br><br>
<b style="margin-left:0.1cm">E:</b>
<INPUT TYPE="text" NAME="ee" SIZE=6  maxlength="10">
<b style="margin-left:0.1cm">W:</b>
<INPUT TYPE="text" NAME="ww" SIZE=6 maxlength="10" >
<b style="margin-left:0.1cm">R:</b>
<INPUT TYPE="text" NAME="rr" SIZE=6 maxlength="10" >
<b style="margin-left:3cm">3-Phase:</b>
<input type="checkbox" name="cb" checked><br>
<br>
<INPUT TYPE="button" VALUE="Calculate Amperes" ONCLICK="compute_amps(this.form)" style="margin-left:2.5cm"><br>
<BR>
<b style="margin-left:0.5cm">Amperes:</b>
<INPUT TYPE="text" NAME="resulta" SIZE=15 >
<BR><br>
</FONT></td>


</tr>
</table>
</td></tr>
<tr>
      <td height="15"></td> 
      <td height="15"></td>
    </tr>
<tr><td width="340">
<table   width="95%" height="227" class="one">
<tr><td><font size="2" face="Verdana" color="darkblue"><b style="margin-left:1cm" >Calculate Volts w/ Ohm's Law:</b></font>
<br>
<br>
<FONT SIZE="2" face="Verdana" color="red">
<B style="margin-left:1cm">Enter any two of: W, I, and R</B><br><br>
<b style="margin-left:0.1cm">W:</b>
<INPUT TYPE="text" NAME="wv" SIZE=6  maxlength="10">
<b style="margin-left:0.1cm">I:</b>
<INPUT TYPE="text" NAME="iv" SIZE=6  maxlength="10">
<b style="margin-left:0.1cm">R:</b>
<INPUT TYPE="text" NAME="rv" SIZE=6 maxlength="10" >
<br><br>
<INPUT TYPE="button" VALUE="Calculate Volts" ONCLICK="compute_volts(this.form)" style="margin-left:2.5cm"><br><br>
<b style="margin-left:0.5cm">Volts:</b>
<INPUT TYPE="text" NAME="resultv" SIZE=15 >
<BR><br>
</FONT></td></tr>
</table>
</td>
<td width="340">
<table  width="95%" height="227" class="four">
<tr><td><font size="2" face="Verdana" color="darkblue"><b style="margin-left:1cm" >Calculate Ohms w/ Ohm's Law:</b></font>
<br>
<br>
<FONT SIZE="2" face="Verdana" color="red">
<B style="margin-left:1cm">Enter any two of: E, I, and W</B><br><br>
<b style="margin-left:0.1cm">E:</b>
<INPUT TYPE="text" NAME="eo" SIZE=6  maxlength="10">
<b style="margin-left:0.1cm">I:</b>
<INPUT TYPE="text" NAME="io" SIZE=6  maxlength="10">
<b style="margin-left:0.1cm">W:</b>
<INPUT TYPE="text" NAME="wo" SIZE=6 maxlength="10" >
<br><br>
<INPUT TYPE="button" VALUE="Calculate Volts" ONCLICK="compute_ohms(this.form)" style="margin-left:2.5cm"><br><br>
<b style="margin-left:0.5cm">Ohms:</b>
<INPUT TYPE="text" NAME="resulto" SIZE=15 >
<BR><br>
</FONT></td></tr>
</table>
</td></tr>

</FORM>
</table></td>
        </tr>
		
		
		


</body>
</html>