<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Convert Units of Temperature - Celsius, Fahrenheit, Kelvin</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content= "calculators, engineering calculators, electrical engineering calculators,  mechanical engineering calculators, physics calculators, optical engineering calculators, electronic calculators, civil engineering calculators, resistance, impedance, voltage, current, capacitance, Ohm's 
law, color code, power, resistor, capacitor, speed, distance, time, chemical periodic table, refrigeration">
<META 
content="calculators,  engineering calculators...." 
name=description>

<script type="text/javascript">
function tempconv (zog) {
        var num1 , num2

        if (zog==1) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*33.90
                document.tempsize.tempto.value=num1
        }

        if (zog==2) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*14.50
                document.tempsize.tempto.value=num1
        }

        if (zog==3) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*1.133
                document.tempsize.tempto.value=num1
        }

        if (zog==4) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.6720
                document.tempsize.tempto.value=num1
        }

        if (zog==5) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*14.22
                document.tempsize.tempto.value=num1
        }

        if (zog==6) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.003281
                document.tempsize.tempto.value=num1
        }

        if (zog==7) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*6.8948
                document.tempsize.tempto.value=num1
        }

        if (zog==8) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*2.307
                document.tempsize.tempto.value=num1
        }

        if (zog==9) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*2.036
                document.tempsize.tempto.value=num1
        }

        if (zog==10) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*27.677
                document.tempsize.tempto.value=num1
        }

        if (zog==11) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2+273
                document.tempsize.tempto.value=num1
        }

        if (zog==12) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=(num2+17.78)*1.8
                document.tempsize.tempto.value=num1
        }

        if (zog==13) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.0295
                document.tempsize.tempto.value=num1
        }

        if (zog==14) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.8826
                document.tempsize.tempto.value=num1
        }

        if (zog==15) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*304.8
                document.tempsize.tempto.value=num1
        }

        if (zog==16) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.4335
                document.tempsize.tempto.value=num1
        }

        if (zog==17) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.491
                document.tempsize.tempto.value=num1
        }

        if (zog==18) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.03613
                document.tempsize.tempto.value=num1
        }

        if (zog==19) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*1.488
                document.tempsize.tempto.value=num1
        }

        if (zog==20) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.0690
                document.tempsize.tempto.value=num1
        }

        if (zog==21) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.07031
                document.tempsize.tempto.value=num1
        }

        if (zog==22) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=num2*0.1450
                document.tempsize.tempto.value=num1
        }

        if (zog==23) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=(num2/2)+254.92
                document.tempsize.tempto.value=num1
        }

        if (zog==24) {
                num2=parseFloat(document.tempsize.tempfrom.value)
                num1=(num2-32)*0.5556
                document.tempsize.tempto.value=num1
        }

        if (zog==99) {
                document.tempsize.tempfrom.value=document.tempsize.tempto.value
                document.tempsize.tempto.value=""
        }
}


//  -->
</script>
</head>
<body bgcolor="#CCCCCC"  style="margin-top:0cm; margin-bottom:0cm">
<table width="940" align="center" bgcolor="white">
<tr><td>
<table width="766" align="left">
 
 
     
     
        <tr>
          <td><table width="649" cellpadding="4" align="center">
  
  
       <tr> <td width="635" align="center"><font size="3" face="verdana"
        color="darkblue"><strong><b>Convert Units of Temperature</b></strong></font></td>
      </tr>
	   <tr><td><hr align="center" size="2" style="color:#FF0000"></td></tr>
	    <tr><td valign="top">

</td></tr>
<tr><td height="9"></td></tr>
	  <tr><td><font size="2" face="verdana"
        color="black"><b>Enter value and select a conversion from the buttons below 
	  and result will be displayed.</b> </font>

</td></tr>
    </table>

 <p style="margin-left:1.5cm"><font size="2" face="verdana"
        color="darkblue"><b>Fahrenheit to Celsius: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Celsius to Kelvin:
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Celsius to Fahrenheit:</b></font><br>
		<img alt="Temperature" src="images/temp c form.bmp" border="0" style="margin-left:.5cm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<img alt="Temperature" src="images/temp k form.bmp" border="0" style="margin-left:1.5cm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<img alt="Temperature" src="images/temp f form.bmp" border="0" style="margin-left:.5cm"></p>
 
<br>
<form name="tempsize" action="temp.htm">
	
	<p align="center"><font size="2" face="verdana"
        color="#FF0000"><b>Enter Value:</b></font>
    <input type="text" name="tempfrom" size="7" maxlength="10" onfocus="resetvalue()"> 
      
	   <b><font size="2" face="verdana" color="#FF0000" style="text-align:center;">Result:</font></b>
	   <input type="text" name="tempto" size="12" maxlength="15"> </p>
	
      
      <table cellpadding="8" align="center">
       
       
        
        <tr>
		<td align="center"><font size="2"><input type="button" value="Celsius to Fahrenheit"
          onclick="tempconv(12)" style="height:2em; width:11em"></font></td>
		  <td><font size="2"><input type="button" value="Fahrenheit to Celsius"
          onclick="tempconv(24)" style="height:2em; width:11em"></font></td>
          <td><font size="2"><input type="button"
          value="Celsius to Kelvin" onclick="tempconv(11)" style="height:2em; width:11em"></font></td>
          
        </tr>
        <tr>
		
          <td><font size="2">&nbsp;</font></td>
          <td><input type="reset" value="Clear" style="height:2em; width:11em"></td>
        </tr>
		
      </table>
	 
    </form></td>
        </tr>
		
		<tr><td height="10"></td></tr>
		<tr><td>
		<!-- SiteSearch Google -->




</body>
</html>