<html>
<head>
<title>Parallel Resistor Calculator</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<META name="keywords" content= "calculators, engineering calculators, electrical engineering calculators,  mechanical engineering calculators, physics calculators, optical engineering calculators, electronic calculators, civil engineering calculators, resistance, impedance, voltage, current, capacitance, Ohm's 
law, color code, power, resistor, capacitor, speed, distance, time, chemical periodic table, refrigeration">
<META 
content="calculators,  engineering calculators...." 
name=description>

<script LANGUAGE="JavaScript">
<!--
var R1 = 0;
var R2 = 0;
var R = 0;
var R1M = 0;
var R2M = 0;
var RM = 0;

function clearBoxes(form)

{
form.R1.value = "";
form.R2.value = "";
form.R.value = "";
form.R1.focus();
}
function clearR1(form)
{
form.R1.value = "";
form.R1.focus();
}
function clearR2(form)
{
form.R2.value = "";
form.R2.focus();
}
function clearR(form)
{
form.R.value = "";
form.R.focus();
}
function solve(form)
{
var i = 3;
if(!form.R1.value) R1 = 0;
else R1 = eval(form.R1.value);
if(R1 == 0) i--;
if(!form.R2.value) R2 = 0; 
else R2 = eval(form.R2.value);
if(R2 == 0) i--;
if(!form.R.value) R = 0;
else R = eval(form.R.value);
if(R == 0) i--;
R1M = eval(form.R1M.value);
R2M = eval(form.R2M.value);
RM = eval(form.RM.value);
R1=R1*R1M;
R2=R2*R2M;
R=R*RM;
if(i == 0) return;      
if(i == 1)
{
alert("\nMissing a value. Two values are required!\n");
return;
}
if(R1 == 0)
{
R1 = (R*R2)/(R2-R);
form.R1.value = R1/R1M;
form.R1.focus();
return;     
}
if(R2 == 0)
{   
R2 = (R*R1)/(R1-R);
form.R2.value = R2/R2M;
form.R2.focus();
return;
}
R = (R1*R2)/(R1+R2);
form.R.value = R/RM;
form.R.focus();
return;
}
-->
</script>



</head>
<body bgcolor="#CCCCCC"  bottommargin="0" topmargin="0">
<table width="940" align="center" bgcolor="white">
<tr><td>
<table width="766" align="left">

       
  
     
        <tr>
          <td><table width="649" cellpadding="4" align="center">
  
  
        <td width="635" align="center"><font size="3" face="verdana"
        color="darkblue"><strong><b>Parallel Resistor Calculator</b></strong></font></td>
      </tr>
	  <tr><td><hr align="center" size="2" color="#FF0000"></td></tr>
	   <tr><td valign="top">




</td></tr>
<tr><td height="9"></td></tr>
	  <tr><td align="center"><font size="2" face="verdana"
        color="black"><b>Enter any two of the following values and press the Calculate button.</b> </font>

</td></tr>
    </table>

 <br>
<p align="center"><img src="images/parckt.jpg"></p>   <br>
<form METHOD="POST">
<table align="center">

<TR>
<TD><font size="2" face="verdana"
        color="darkblue" ><b style="margin-left:.5cm">Resistor R<sub><font size="2">a:</font></sub></b></font></td>


<td><INPUT size=8  maxlength="12" name="R1">
<SELECT name=R1M >
<OPTION value=0.000001>µohm
<OPTION value=0.001>mohm
<OPTION value=1 selected>ohm
<OPTION value=1000>Kohm
<OPTION value=1000000>Mohm
</OPTION></SELECT></TD>

</TR>
<TR>
<TD>
<font size="2" face="verdana"
        color="darkblue" ><b style="margin-left:.5cm">Resistor R<sub><font size="2">b:</font></sub></b></font></td>

<td><INPUT size=8 name="R2">
<SELECT name=R2M >
<OPTION value=0.000001>µohm
<OPTION value=0.001>mohm

<OPTION value=1 selected>ohm
<OPTION value=1000>Kohm
<OPTION value=1000000>Mohm
</OPTION></SELECT>
</TD>
</TR>
<TR>
<TD>
<font size="2" face="verdana"
        color="darkblue" ><b style="margin-left:.5cm">Parallel Resistance R<sub><font size="2">p:</font></sub></b></font></td>

 <td><input size=8 name="R">
<SELECT name=RM >
<OPTION value=0.000001>µohm
<OPTION value=0.001>mohm
<OPTION value=1 selected>ohm
<OPTION value=1000>Kohm
<OPTION value=1000000>Mohm
</OPTION></SELECT></TD>


</TR>
<tr><td height="5"></td></tr>
<TR>

<TD align=center colspan="3"><input TYPE="button" NAME="name" VALUE="Calculate" onClick="solve(this.form);">
<input TYPE="reset" VALUE="Clear" onClick="clearBoxes(this.form);" style="margin-left:1cm"></TD>
</TR></TABLE>
</form></td>
        </tr>
		<tr><td><table style="margin-left:1cm"><tr>
		<td>
		
</body>
</html>