<?php
$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
session_start();
$table=$_SESSION['table'];


//// * Example PHP implementation used for the index.html example
// 
//
//// DataTables PHP library
include( "DataTables-1.10.0/extensions/Editor-1.3.0/php/DataTables.php" );
//
//// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Join,
    DataTables\Editor\Validate;
//
//// Build our Editor instance and process the data coming from _POST

Editor::inst( $db,'new_part' )
    ->fields(
	  
         Field::inst( 'new_part.Part_Number' ),
		Field::inst( 'new_part.CPN' ),
		Field::inst( 'new_part.Library_Ref'),
	    Field::inst( 'new_part.Description' ),
	    Field::inst( 'new_part.Manufacturer' ),
		Field::inst( 'new_part.Manufacturer_Part_Number'),
		Field::inst( 'new_part.Library_Path'),
		Field::inst( 'new_part.Category'),
	    Field::inst( 'new_part.Comment' ),
	    Field::inst( 'new_part.Component_Kind' ),
		Field::inst( 'new_part.Component_Type'),
		Field::inst( 'new_part.Designator' ),
		Field::inst( 'new_part.Footprint' ),		
		Field::inst( 'new_part.Manufacturer1'),
	    Field::inst( 'new_part.Manufacturer_Part_Number1' ),
	  
		Field::inst( 'new_part.Pin_Count'),
		Field::inst( 'new_part.Signal_Integrity' ),
		Field::inst( 'new_part.Simulation' ),
		Field::inst( 'new_part.Supplier'),
		Field::inst( 'new_part.Supplier_Part_Number'),
	    Field::inst( 'new_part.Footprint_Path' ),
		Field::inst( 'new_part.Footprint_Ref' ),
		Field::inst( 'new_part.Supplier_Part_Number1'),
		Field::inst( 'new_part.Supplier1' ),
		Field::inst( 'new_part.ComponentLink1Description' ),
	   Field::inst( 'new_part.ComponentLink1URL' ),
	  
	    Field::inst( 'new_part.table_name'),	
        Field::inst( 'new_part.status')	 
    )
	->process( $_POST )
    ->json();
