
<?php
$groupswithaccess="ladmin,luser";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");

include("include.php");  // read css and js folders, sets database variables
session_start();

$table=$_GET['TBL'];
$_SESSION['table']=$table;



include("header.php");

?>

<link rel="stylesheet" type="text/css" href="../css/sortstyle.css">

<style type="text/css" class="include" title="currentStyle">
    @import "DataTables-1.10.0/media/css/jquery.dataTables.css";
    @import "DataTables-1.10.0/extensions/Editor-1.3.0/css/dataTables.editor.css";


    td.details-control {
        background: url('images/editpage.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('images/closefile.png') no-repeat center center;
    }
    TD {font-size:10px}


</style>
<link href="../css/popup.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"> </script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/popup1.js"></script>

 
  <script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/media/js/jquery.js"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/media/js/jquery.dataTables.js"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/Editor-1.3.0/js/dataTables.editor.js"></script>
<script type="text/javascript" charset="utf-8">
var editor;


    $(document).ready(function() {
	

        editor = new $.fn.dataTable.Editor( {
		    
            "ajax": "mi_datatest.php",
			"table": "#view",
            "fields": [
              
				 
				
            ]
        } );

	

        // New record
        $('a.editor_create').on('click', function (e) {
            e.preventDefault();

           editor
                .title( 'Create new record' )
                .buttons( { "label": "Add", "fn": function () { editor.submit();
                   } } )
                .create();

        } );

        // Edit record
        $('#view').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();

            editor
                .title( 'Edit record' )
                .buttons( { "label": "Update", "fn": function () { editor.submit() } } )
                .edit( $(this).closest('tr') );
        } );

        // Delete a record (without asking a user for confirmation for this example)
        $('#view').on('click', 'a.editor_remove', function (e) {
            e.preventDefault();

            editor
                .title( 'Delete record' )
                .message( 'Are you sure you wish to remove this record?' )
                .buttons( { "label": "Delete", "fn": function () { editor.submit() } } )
                .remove( $(this).closest('tr') );
        } );

        // DataTables init
		 var fa = 0;
       var table = $('#view').DataTable( {              "sScrollY": "300px",
               dom: "lfrtip",
           "bAutoWidth": false,
                "bProcessing": true,
				 "aLengthMenu": [[10, 50, 100, 500], [10, 50, 100, 500]],
                //"bPaginate": false,
               // "bAutoWidth": false,
				
              ajax: {
               url: "mi_data.php",
               type: "POST"
                },
                
                "columns": [

                {
                "data": "<?php echo $table; ?>.Part_Number"
                
                },

               
                {
                    "data": "<?php echo $table; ?>.PN"
                },
               
                {
				         "bVisible": false,
                         "sClass": "center",
                        "data": "<?php echo $table; ?>.Library_Ref"
                },
				
                {
                    "sClass": "center",
                    "data": "<?php echo $table; ?>.Description"
                },
                {
                    "sClass": "center",
                    "data": "<?php echo $table; ?>.Manufacturer"
                },
                {
                    "sClass": "center",
                    "data": "<?php echo $table; ?>.Manufacturer_Part_Number"
                },
                             
              
				 {
				  "sClass": "center",
                "bSortable": true,
                "mRender": function (data, type, full) {
                    return '<a href="'+full.<?php echo $table; ?>.ComponentLink1URL+'" target="_blank">Datasheet</a>';
                    }
                },
				  {
                "bSearchable": false,
                "bSortable": false,
                "mRender":function(data, type, full)
                {
                    if ( full.<?php echo $table; ?>.status === 'yellow')
                    {
                        return '<center><img src="../images/yellow.png" width="16"></center>';
                    }
                    else if(full.<?php echo $table; ?>.status === 'green')
                    {
                        return '<center><img src="../images/green.png" width="16"></center>';
                    }
					  else if(full.<?php echo $table; ?>.status === 'red')
                    {
                        return '<center><img src="../images/red.png" width="16"></center>';
                    }
                }

            }
              
                ]
			} );
} );
       
</script>
</head>
<br/>
<div class="status_panel">
	<div class="status_sec">

					<p>Logged in User - <strong><?php echo $slname; ?></strong> 	&nbsp;&nbsp;&nbsp; User Status- <strong><?php if ( $slgroupname[0] === 'ladmin'){?>Admin<?php } else{?>Basic<?php }?></strong>&nbsp;&nbsp;&nbsp; 	Current Page - <strong><?php if($table==capacitor){ ?>Capacitor
		<?php } 
		else if($table==resistor){ ?>Resistor
		<?php }
		else if($table==misc){ ?>Misc
		
	<?php	}
	else if($table==connector){ ?>Connector
	<?php	}
	else if($table==ic){ ?>IC
	<?php	}
	else if($table==diode){ ?>Diode
	<?php	}
	else if($table==transistor){ ?>Transistor
	<?php	}
	else if($table==oscillator){ ?>Oscillator
	<?php	}?> </strong></p>


	</div>
</div>
<center>
<table border="0" width="100%" cellpadding="2" cellspacing="2" style="float:left;">
	<tr>

		  <td align="center" colspan="3"><a href="part_editor.php" class="menu_click">Editor Main Menu</a>		</td>

	 </tr>
	
</table>
</center>

<center><br />


</center>
<center>
<table border="2" cellspacing="0" cellpadding="2" class="display dataTable" id="view" >
<thead>
	<tr> 
		
	    <th >Part Name</th>
		<th >PN</th>
		<th  >Library Ref</th>
		<th >Description</th>
		<th >Manufacturer</th>
		<th >Manufacturer PN</th>
       			
		<th>Datasheet</th>
		<th>Status</th>
	</tr>
</thead>

</table>
</center>



   
<?php
include("footer.php");
?>
