<?php
$groupswithaccess="ladmin,luser,leditor";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
session_start();
$MPN=$_SESSION['MPN'];

//// * Example PHP implementation used for the index.html example
// 
//
//// DataTables PHP library
include( "DataTables-1.10.0/extensions/Editor-1.3.0/php/DataTables.php" );
//
//// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Join,
    DataTables\Editor\Validate;
//
//// Build our Editor instance and process the data coming from _POST

Editor::inst( $db,'bom' )
    ->fields(
	  
         Field::inst( 'bom.bom_name' ),
		Field::inst( 'bom.reference_designator' ),
		Field::inst( 'bom.description'),
	    Field::inst( 'bom.manufacture' ),
	    Field::inst( 'bom.quantity' ),
		Field::inst( 'bom.mpn')
		
    )
	->where( $key = 'bom.mpn', $value = $MPN, $op = '=' )
	
    ->process( $_POST )
    ->json();
?>