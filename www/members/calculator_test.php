<?php 

$groupswithaccess="ladmin,luser,leditor";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>
 <link type="text/css" rel="stylesheet" href="css/easy-responsive-tabs.css" />
		<div class="side_work">	
			<div class="working_area">
	<div class="status_panel">
		<div class="status_sec">
	
				<div class="statusbar"><p>Current Page - <strong>Calculators / PCB Control</strong></p></div>
				<div class="returnstat"><a href="calculator.php" class="menu_click">Return</a></div>
	
		</div>
	</div>
<div class="app_calculator">

  <div class="calc_buttons">
  	
  </div> 
            
<div id="body-container1">
                  <div class="glossary-container">
   					 <ul class="firstUL1">
						<li id="a" class="selected">Pi Attenuator</li>
						<li id="b">Tee Attenuator</li>
					</ul>
				 </div>	
  <div class="content-container">

    <div id="content-for-a" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">
				<iframe src="http://library9.com/members/PiAttenuator.php" height="649px" width="700px" scrolling="no" border="0" align="center" seamless>
                </iframe>
			</div>
			<div class="calc_right">
									 	<p>Pi AttenuatorA measure of the ability of a configuration of materials to store electric charge. In a capacitor, capacitance depends on the size of the plates, the type of insulator, and the amount of space between the plates. Most electrical components display capacitance to some degree; even the spaces between components of a circuit have a natural capacitance. Capacitance is measured in farads.</p>
										
				 <div class="calc_foot"><p class="return-to-top">Return to Top</p></div>
			</div>
		</div>	
   </div>
	
						
			
        </div>
  
</div>

    </div>
</div>
</div>
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL1 li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL1 li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#fff'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL1 li").each(function() { 
			$(".firstUL1 li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#fff'}); //set the background color to light-blue to that div
	 });
});
</script>

<script LANGUAGE="JavaScript">
<!--
var A = 0;
var ZI = 0;
var ZIM = 0;
var ZO =0;
var ZOM = 0;
var R1 = 0;
var R1M = 0;
var R2 = 0;
var R2M = 0;
var R3 = 0;
var R3M = 0;

function clearBoxes(form)
{
form.A.value = "";
form.ZI.value = "";
form.ZO.value = "";
form.R1.value = "";
form.R2.value = "";
form.R3.value = "";
form.A.focus();
}

function solve(form)
{
if(!form.A.value) {alert("\nEnter value of Attenuation\n"); return;}
else A = eval(form.A.value);
if(A == 0){alert("\nPlease enter Attenuation other than zero!\n"); return;}

if(!form.ZI.value) {alert("\nEnter value of Zi\n"); return;}
else ZI = eval(form.ZI.value);
if(ZI == 0){alert("\nPlease enter Zi other than zero!\n"); return;}

if(!form.ZO.value) {alert("\nEnter value of Zo\n"); return;}
else ZO = eval(form.ZO.value);
if(ZO == 0){alert("\nPlease enter Zo other than zero!\n"); return;}

ZIM = eval(form.ZIM.value);
ZOM = eval(form.ZOM.value);
R1M = eval(form.R1M.value);
R2M = eval(form.R2M.value);
R3M = eval(form.R3M.value);

ZI=ZI*ZIM;
ZO=ZO*ZOM;

R2 = 0.5*(Math.pow(10,A/10)-1)*Math.sqrt((ZI*ZO)/Math.pow(10,A/10));

form.R2.value = R2/R2M;

R3 = 1/(((Math.pow(10,A/10)+1)/(ZO*(Math.pow(10,A/10)- 1)))-(1/R2));

form.R3.value = R3/R3M;

R1 = 1/(((Math.pow(10,A/10)+1)/(ZI*(Math.pow(10,A/10)- 1)))-(1/R2));

form.R1.value = R1/R1M;

form.A.focus();

return;
}
-->
</script>


