
<?php
$groupswithaccess="ladmin,luser,leditor";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");

include("include.php");  // read css and js folders, sets database variables
session_start();

$table=$_GET['TBL'];
$_SESSION['table']=$table;



include("header.php");
include("sidebar.php");

?>

<link rel="stylesheet" type="text/css" href="../css/sortstyle.css">

<style type="text/css" class="include" title="currentStyle">

    @import "DataTables-1.10.0/extensions/Editor-1.3.0/css/dataTables.editor.css";


    td.details-control {
        background: url('images/editpage.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('images/closefile.png') no-repeat center center;
    }
    TD {font-size:10px}


</style>
<link href="css/popup.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="css/dataTables.jqueryui.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/popup1.js"></script>

 <script type="text/javascript" src="js/jquery.min_online.js"> </script>

<script src="js/jquery-1.11.1.min.js" language="javascript" type="text/javascript"></script> 
<script src="js/jquery.dataTablesonline.min.js" language="javascript" type="text/javascript"></script>
<script src="js/dataTables.jqueryui.js" language="javascript" type="text/javascript"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/Editor-1.3.0/js/dataTables.editor.js"></script>
<script type="text/javascript" charset="utf-8">
var editor;


    $(document).ready(function() {
	 var tbl = '<?php echo $table; ?>';
	
        editor = new $.fn.dataTable.Editor( {
		    
            "ajax": "submit_data.php",
			"table": "#view",
			"fields": [
			   {
                    "label": "Part Number",
                    "name": "new_part.Part_Number"
                    
                },
                {
                     
                     "label": "CPN",
                     "name": "new_part.CPN"
                    
                 },
				 {
				     
                     "label": "Library Ref",
                     "name": "new_part.Library_Ref",
					 "def": "new_part.Library_Ref"
                     
                 },
                  {
				      "type": "hidden",
                     "label": "company_id",
                     "name": "new_part.company_id",
					 "def": "new_part.company_id"
                     
                 },
				 {
				      "type": "hidden",
                     "label": "Category",
                     "name": "new_part.Category",
					 "def": "new_part.Category"
                     
                 },
                {
                    "label": "Description",
                    "name": "new_part.Description"
                },
               
                {
                    "label": "Manufacturer",
                    "name": "new_part.Manufacturer"
                },
                {
                    "label": "Manufacturer PN",
                    "name": "new_part.Manufacturer_Part_Number"
                },
              
				  {
                    "label": "Library Path",
                    "name": "new_part.Library_Path"
                    
                },
                
				 
				 {
				      
                     "label": "Category",
                     "name": "new_part.table_name"
					                     
                 },
                
                {
                    "label": "Comment",
                    "name": "new_part.Comment"
                },
               
                {
                    "label": "Component Kind",
                    "name": "new_part.Component_Kind"
                },

                {
                    "label": "Component Type",
                    "name": "new_part.Component_Type"
                },
              {
                    "label": "Designator",
                    "name": "new_part.Designator"
                    
                },
             
				   {
                    "label": "Footprint",
                    "name": "new_part.Footprint"
                    
                },              
				 
				
                
                {
                    "label": "Manufacturer1",
                    "name": "new_part.Manufacturer1"
                },
               
                {
                    "label": "Manufacturer Part Number1",
                    "name": "new_part.Manufacturer_Part_Number1"
                },
                
              {
                    "label": "Pin Count",
                    "name": "new_part.Pin_Count"
                    
                },
                {
                     
                     "label": "Signal Integrity",
                     "name": "new_part.Signal_Integrity"
                    
                 },
				  {
                    "label": "Simulation",
                    "name": "new_part.Simulation"
                    
                },
                {
                     
                     "label": "Supplier",
                     "name": "new_part.Supplier"
                    
                 },
				 
				 {
				      
                     "label": "Supplier Part Number",
                     "name": "new_part.Supplier_Part_Number"
					                     
                 },
              
                {
                    "label": "Footprint Path",
                    "name": "new_part.Footprint_Path"
                },
                {
                    "label": "Footprint Ref",
                    "name": "new_part.Footprint_Ref"
                },
           
              
              {
                    "label": "Supplier Part Number1",
                    "name": "new_part.Supplier_Part_Number1"
                    
                },
                {
                     
                     "label": "Supplier1",
                     "name": "new_part.Supplier1"
                    
                 },
				  
				  {
                    "label": "ComponentLink1Description",
                    "name": "new_part.ComponentLink1Description"
                    
                },
                
				 {
				      
                    "label": "Datasheet",
                    "name": "new_part.ComponentLink1URL",
					 "def": "new_part.ComponentLink1URL"
                },
				  {
                   "type": "hidden",
				  "label": "Edit Status",
                "name": "new_part.status"
                
                
            }
				
            ]
			
			
			
			
			
        } );

	var openVals;
    editor
	 .on( 'open', function () {
            // Store the values of the fields on open
            openVals = JSON.stringify( editor.get() );
			
        } )

        // New record
        $('a.editor_create').on('click', function (e) {
            e.preventDefault();

           editor
                .title( 'Create new record' )
                .buttons( { "label": "Add", "fn": function () { editor.submit();
                   } } )
                .create();

        } );

        // Edit record
        $('#view').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();
          var $tds = $(this).closest('tr').find('td');
		  var updt='';
        //var $invid = $(this).closest('tr').attr('id').toString().substring(4);
            editor
                .title( 'Edit record' )
                .buttons( { "label": "Update", "fn": function (data, type, full) { editor.submit();
				//alert($tds.eq(0).html().indexOf('yellow'));
				 function status(){
				var mpn=$tds.eq(5).text();
				var pn=$tds.eq(2).text();
		    var cat_name=$tds.eq(6).text();
			if ( openVals !== JSON.stringify( editor.get() ) ) {
			var post_data=JSON.stringify( editor.get() );
			var data1=JSON.parse(post_data);
			var json = JSON.parse(openVals);
			/* var arr = $.map(json, function(el,a) { 
               

			console.log(el);
			console.log(a);})
			 */
			 $.each(json, function (i,v)
				{
				  $.each(data1,function(val,ele){
				    if(i==val){
					 if(v!==ele){
                       updt=updt+'@'+val+': Old Value-'+v+' ,New Value- '+ele;
					   
					 }
					}
				  
				  });
				});
				
            }				

                $.ajax({
                    url : 'new_partadd.php',
                    type : 'GET',					
                    data : {
                        'mpn' : mpn,
                        'cat_name' : cat_name,  
						'pn':pn,
						'updt':updt

                    },
                    success : function(result){
					
                        console.log(result);
						 $('#view').DataTable().ajax.reload(); 

                    },
                    error : function(e){
                        console.log(e);
                    }
                });
				}
		setTimeout( status, 1500 ); 
            } } )
                .edit( $(this).closest('tr') );
				
				
        } );


        // Delete a record (without asking a user for confirmation for this example)
        $('#view').on('click', 'a.editor_remove', function (e) {
            e.preventDefault();
             var $tds = $(this).closest('tr').find('td');
							var mpn=$tds.eq(5).text();
							var cat_name=$tds.eq(6).text();
            editor
                .title( 'Delete record' )
                .message( 'Are you sure you wish to remove this record?' )
                .buttons( { "label": "Delete", "fn": function () { editor.submit()
				 
								$.ajax({
									url : 'del_part.php',
									type : 'get',
									
									data : {
										'mpn' : mpn,
										'cat_name' : cat_name                    

									},
									success : function(result){
									 
										console.log(result);

									},
									error : function(e){
										console.log(e);
									}
								});



				} } )
                .remove( $(this).closest('tr') );
        } );

        // DataTables init
		 var fa = 0;
       var table = $('#view').DataTable( {              
                "sdom": 'T<"clear"><"H"lfr>t<"F"ip>',
           "bAutoWidth": false,
                "bProcessing": true,
				 "aLengthMenu": [[10, 50, 100, 500], [10, 50, 100, 500]],
                //"bPaginate": false,
               // "bAutoWidth": false,
				
              ajax: {
               url: "submit_data.php",
               type: "POST"
                },
                
                "columns": [
				
				{
                "bSearchable": false,
                "bSortable": false,
                "mRender":function(data, type, full)
                {
                    if ( full.new_part.status === 'yellow')
                    {
                        return '<center><img src="../images/yellow.png"></center>';
                    }
                    else if(full.new_part.status === 'green')
                    {
                        return '<center><img src="../images/green.png" ></center>';
                    }
					  else if(full.new_part.status === 'red')
                    {
                        return '<center><img src="../images/red.png"></center>';
                    }
					 else if(full.new_part.status === 'blue')
                    {
                        return '<center><img src="../images/blue.png"></center>';
                    }
                }

            },

                { "sClass": "center",
                "data": "new_part.Part_Number"
                
                },

               
                {  
                    "data": "new_part.CPN"
                },            
             
				
                {
                    "sClass": "center",
                    "data": "new_part.Description"
                },
                {
                    "sClass": "center",
                    "data": "new_part.Manufacturer"
                },
                {
                    "sClass": "center",
                    "data": "new_part.Manufacturer_Part_Number"
                },
                   {
                    "sClass": "center",
                    "data": "new_part.table_name"
                },     
              
				 {"bVisible": false,
				  "sClass": "center",
                "bSortable": true,
                "mRender": function (data, type, full) {
                    return '<a href="'+full.new_part.ComponentLink1URL+'" target="_blank">Datasheet</a>';
                    }
                },
				  
			
			{
                    "bSearchable": false,
                    "bSortable": false,
                    "mRender":function(data, type, full)
                    {
                        
                            return '<center><a href="" class="editor_edit"><img src="images/pencil.png" width="16"></a>&nbsp&nbsp<a href="" class="editor_remove"><img src="images/delete2.png" width="16"></a></center>';
                       
                    }

                }
              
                ]
			} );
} );
       
</script>
</head>

		<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">

					<div class="statusbar"><p>Current Page - <strong>Submitted Part</strong></p></div>

<div class="returnstat"><a href="index.php" class="menu_click">Return</a></div>					


	</div>
</div>
<div class="app_content">

<center>
<table cellspacing="0" cellpadding="0" class="display" id="view" >
<thead>
	<tr> 
		<th>Status</th>
	    <th >Part Name</th>
		<th >CPN</th>		
		<th >Description</th>
		<th >Manufacturer</th>
		<th >Manufacturer PN</th>
       	<th >Category</th>		
		<th>Datasheet</th>
		<th>Edit </th>
	</tr>
</thead>

</table>
</center>


	
	 

	<div class="loader"></div>
	<div id="backgroundPopup1"></div>


	



</div>
</div>
</div>
 