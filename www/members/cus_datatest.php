<?php
$groupswithaccess="ladmin,luser,leditor";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");

//$table='Resistor';

session_start();
$table=$_SESSION['table'];
//// * Example PHP implementation used for the index.html example
// 
//
//// DataTables PHP library
include( "DataTables-1.10.0/extensions/Editor-1.3.0/php/DataTables.php" );
//
//// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Join,
    DataTables\Editor\Validate;
//
//// Build our Editor instance and process the data coming from _POST




Editor::inst( $db,$table,'ITEM' )
    ->fields(
	  
        Field::inst( 'Part_Number' ),
		Field::inst( 'PN' ),
		Field::inst( 'Library_Ref'),
	    Field::inst( 'Description' ),
	    Field::inst( 'Manufacturer' ),
		Field::inst( 'Manufacturer_Part_Number'),		
		Field::inst( 'Library_Path'),
		Field::inst( 'Category'),
	    Field::inst( 'Comment' ),
	    Field::inst( 'Component_Kind' ),
		Field::inst( 'Component_Type'),		
		Field::inst( 'Designator' ),
		Field::inst( 'Footprint' ),
		Field::inst( 'Library_Reference' ),
		Field::inst( 'Manufacturer1'),
	    Field::inst( 'Manufacturer_Part_Number1' ),
	    Field::inst( 'Packaging' ),
		Field::inst( 'Pin_Count'),
		Field::inst( 'Signal_Integrity' ),
		Field::inst( 'Simulation' ),
		Field::inst( 'Supplier'),
		Field::inst( 'Supplier_Part_Number'),	 
	    Field::inst( 'Footprint_Path' ),
		Field::inst( 'Footprint_Ref' ),
		Field::inst( 'ComponentLink1Description' ),
	    Field::inst( 'ComponentLink1URL' ),
        Field::inst( 'status'),	 
	    Field::inst( 'custom1' ),
		Field::inst( 'custom2' ),
		Field::inst( 'custom3'),
	    Field::inst( 'custom4' ),
	    Field::inst( 'custom5' ),
		Field::inst( 'custom6'),		
		Field::inst( 'custom7'),
		Field::inst( 'custom8'),
	    Field::inst( 'custom9' ),
	    Field::inst( 'custom10' ),
		Field::inst( 'custom11'),		
		Field::inst( 'custom12' ),
		Field::inst( 'custom13' ),
		Field::inst( 'custom14' ),
		Field::inst( 'custom15'),
	    Field::inst( 'custom16' ),
	    Field::inst( 'custom17' ),
		Field::inst( 'custom18'),
		Field::inst( 'custom19' ),
		Field::inst( 'custom20' )
   
    )
	->where( $key = 'status', $value = 'green', $op = '=' )
    ->process( $_POST )
    ->json();





