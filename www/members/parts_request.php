<?php
$groupswithaccess="ladmin,luser,leditor";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");

include("include.php");  // read css and js folders, sets database variables
session_start();

$table=$_GET['TBL'];
$_SESSION['table']=$table;
$PN=$_GET['PN'];

session_start();
$_SESSION['PN']=$PN;



include("header.php");
include("sidebar.php");

?>

<link rel="stylesheet" type="text/css" href="../css/sortstyle.css">

<style type="text/css" class="include" title="currentStyle">

    @import "DataTables-1.10.0/extensions/Editor-1.3.0/css/dataTables.editor.css";


    td.details-control {
        background: url('images/editpage.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('images/closefile.png') no-repeat center center;
    }
    TD {font-size:10px}
	#abc {
    font-family: "lucida grande",tahoma,verdana,arial,sans-serif;
    background: none repeat scroll 0 0 #FFFFFF;
    border: 10px solid rgba(220,220,220,0.5);
    border-radius: 6px 6px 6px 6px;
    color: #333333;
    display: none;
    font-size: 14px;
    left: 50%;
    margin-left: -410px;
    position: fixed;
    top: 18%;
    width: 600px;
    z-index: 2;


</style>
<link href="css/popup.css" rel="stylesheet" type="text/css" media="all" />
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/jqueryui/dataTables.jqueryui.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"> </script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/popup1.js"></script>

 
 <script src="//code.jquery.com/jquery-1.11.1.min.js" language="javascript" type="text/javascript"></script> 
<script src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" language="javascript" type="text/javascript"></script>
<script src="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/jqueryui/dataTables.jqueryui.js" language="javascript" type="text/javascript"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/Editor-1.3.0/js/dataTables.editor.js"></script>
<script type="text/javascript" charset="utf-8">
var editor;


    $(document).ready(function() {
	

        editor = new $.fn.dataTable.Editor( {
		    
            "ajax": "log_data.php",
			"table": "#view",
            "fields": [
                
				 {
				      "type": "hidden",
                     "label": "company_id",
                     "name": "component_name",
					 "def": "component_name"
                     
                 },
				  {
				      "type": "hidden",
                     "label": "company_id",
                     "name": "date",
					 "def": "date"
                     
                 },
             {
                "label": "Edit Status",
                "name": "status",
                "type": "select",
                "ipOpts": [
                   
                    { "label": "Pending", "value": "Pending" },
                    { "label": "Done", "value": "Done" }
                ]
            }
				 
				
            ]
        } );

	var openVals;
    editor
	 .on( 'open', function () {
            // Store the values of the fields on open
            openVals = JSON.stringify( editor.get() );
			
        } )

        // New record
        $('a.editor_create').on('click', function (e) {
            e.preventDefault();

           editor
                .title( 'Create new record' )
                .buttons( { "label": "Add", "fn": function () { editor.submit();
                   } } )
                .create();

        } );

        // Edit record
        $('#view').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();
           
            editor
                .title( 'Edit record' )
                .buttons( { "label": "Update", "fn": function () { editor.submit()
                	    

				} } )
                .edit( $(this).closest('tr') );
        } );

        // Delete a record (without asking a user for confirmation for this example)
        $('#view').on('click', 'a.editor_remove', function (e) {
            e.preventDefault();
           
            editor
                .title( 'Delete record' )
                .message( 'Are you sure you wish to remove this record?' )
                .buttons( { "label": "Delete", "fn": function () { editor.submit()
                             
				} } )
                .remove( $(this).closest('tr') );
        } );

        // DataTables init
		 var fa = 0;

       var table = $('#view').DataTable( {             
        "sdom": 'T<"clear"><"H"lfr>t<"F"ip>',
           "bAutoWidth": false,
                "bProcessing": true,
				 "aLengthMenu": [[10, 50, 100, 500], [10, 50, 100, 500]],
                //"bPaginate": false,
               // "bAutoWidth": false,
				
              ajax: {
               url: "log_data.php",
               type: "POST"
                },
                
                "columns": [

                {
				    "sClass": "center",
                "data": "component_name"
                
                },

               
                {
				    "sClass": "center",
                    "data": "date"
                },               
               
               	{
                    "sClass": "center",
                    "data": "status"
                },
				{
                    "bSearchable": false,
                    "bSortable": false,
                    "mRender":function(data, type, full)
                    {
                        
                            return '<center><a href="" class="editor_edit"><img src="pencil.png" width="16"></a></center>';
                        
                    }

                }
              
                ]
			} );
		

} );

    </script>
</head>
<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">

					<div class="statusbar"><p>Current Page - <strong>Parts Log </strong></p></div>

	<div class="returnstat"><a href="index.php" class="menu_click">Return</a></div>
	</div>
</div>


<div class="app_content">

<br><br>
<center>
<table  cellspacing="0" cellpadding="0" class="display" id="view" >
<thead>
	<tr> 
		
	    <th >Manufacturer Part Number</th>
		<th >Date</th>
		<th >Status</th>
		<th >Edit Status</th>
		
	</tr>
</thead>

</table>
