<?php 

$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="loggedin_index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>
 <link type="text/css" rel="stylesheet" href="css/easy-responsive-tabs.css" />
		<div class="side_work">	
			<div class="working_area">
	<div class="status_panel">
		<div class="status_sec">
	
				<div class="statusbar"><p>Current Page - <strong>Calculators / Resistance</strong></p></div>
				<div class="returnstat"><a href="calculator.php" class="menu_click">Return</a></div>
	
		</div>
	</div>
<div class="app_calculator">

  <div class="calc_buttons">
  	
  </div> 
            
<div id="body-container1">
                  <div class="glossary-container res_calc">
   					 <ul class="firstUL1">
						<li id="a" class="selected">Resistor Color Code</li>
						<li id="b">Non-Inverting Operational Amplifier</li>
						<li id="c">Parallel Resistor</li>
						<li id="d">Wheatstone Bridge</li>
						<li id="e">LED Resistor </li>
						<li id="f">Inverting Op-Amp Resistor Calculator</li>												
						
					</ul>
				 </div>	
  <div class="content-container">
<!------------Resistor Color Code---------------->
    <div id="content-for-a" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">
	<center>		
	<table align="center" class="resist">
		  					<tr><th colspan="2" align="center">Select colors from select box</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>	

<tr><td colspan="2" align="center">
<!-----FORM1------>
<FORM name="dropbox">
<SELECT name=bandone> 
<OPTION value=101 selected>Strip 1
<OPTION value=101>	Black
<OPTION value=102>	Brown

<OPTION value=103>	Red
<OPTION value=104>	Orange
<OPTION value=105>	Yellow
<OPTION value=106>	Green
<OPTION value=107>	Blue
<OPTION value=108>	Violet
<OPTION value=109>	Gray
<OPTION value=110>	White

</option></select>

   
<SELECT name=bandtwo style="margin-left:.6cm"> 
<OPTION value=201 selected>Strip 2
<OPTION value=201>	Black
<OPTION value=202>	Brown
<OPTION value=203>	Red
<OPTION value=204>	Orange
<OPTION value=205>	Yellow
<OPTION value=206>	Green
<OPTION value=207>	Blue

<OPTION value=208>	Violet
<OPTION value=209>	Gray
<OPTION value=210>	White

</option></select>
   
<SELECT name=bandthree style="margin-left:.6cm"> 
<OPTION value=301 selected>Strip 3
<OPTION value=301>	Black
<OPTION value=302>	Brown
<OPTION value=303>	Red
<OPTION value=304>	Orange

<OPTION value=305>	Yellow
<OPTION value=306>	Green
<OPTION value=307>	Blue
<OPTION value=308>	Violet
<OPTION value=309>	Gold
<OPTION value=310>	Silver
</option></select>

   
<SELECT name=bandfour style="margin-left:.6cm"> 
<OPTION value=401 selected>Tolerance

<OPTION value=401>	None
<OPTION value=402>	Silver
<OPTION value=403>	Gold

</option></select>

</form>
</td>
</tr>



<tr> <td align="center" colspan="2"><INPUT onclick=calc() type=button value="Calculate" name=ENTER class="calculate_cal" class="calculate_cal"></td></tr>
    
    <!------------FORM2----------------> 
<tr><td colspan="2" style="height:15px;"></td></tr>		       
<tr><th colspan="2" align="center">Result</th></tr>
<tr><td colspan="2" style="height:15px;"></td></tr>		       
<tr><td colspan="2" align="center"><FORM name=outbox> 
<INPUT type=text size="30" maxlength="70" name=z1> 
</form>												
</td></tr>
<tr><td colspan="2" style="height:15px;"></td></tr>	
</table>
</center>
			</div>
			<div class="calc_right">
			<h5>Resistor Color Code</h5>
				<p>This calculator solves for 4, 5 or 6 band resistors and is quite simple to use. To calculate a four band resistor value, use the middle four "drop" boxes then 
click on the "Calc 4 Band" button</p>

										
				 <div class="calc_foot"><p class="return-to-top">Return to Top</p></div>
			</div>
			
		</div>	
     
     
    </div>
	<!------------Resistor Color Code---END------------->
	
	<!-------Non-Inverting Operational Amplifier Resistor-START---->
	<div id="content-for-b" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">
	<center>		
	<table align="center" class="resist">
		  					<tr><th colspan="2" align="center">Enter Value</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>	
							<tr><td colspan="2" align="center"><img src="images/opampnon.jpg" border="0"></td>

	<!----form1--->
	 
<form method="POST" name= "form" id="frm2"> 

		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Vout: </b></font> </td>
                 <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>   <input type="text" name="Vout" size="7" maxlength="15" >
              Volts</b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Gain: </b></font> </td>
                  <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>  <input type="text" name="A" size="7" maxlength="15">
               Volts</b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>R<sub>g</sub>: </b></font> </td>
                 <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>   <input type="text" name="R1" size="7" maxlength="15">
             KOhms</b></font> </td></tr>
			 
			 <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>V<sub>1</sub>: </b></font> </td>
                  <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>  <input type="text" name="V1" size="7" maxlength="15">
            Volts</b></font> </td></tr>
			 
			 <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>V<sub>2</sub>: </b></font> </td>
                   <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b> <input type="text" name="V2" size="7" maxlength="15">
             Volts</b></font> </td></tr>
			 
			 <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>V<sub>p</sub>: </b></font> </td>
                 <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>   <input type="text"  name="Vp" size="7" maxlength="15">
              Volts</b></font> </td></tr>
			 
			 <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>V<sub>n</sub>: </b></font> </td>
                  <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>  <input type="text" name="Vn" size="7" maxlength="15">
              Volts</b></font> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" value="Calculate" name="Calculate" onClick="Compute1(this.form)" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
						<tr><td colspan="2" style="height:15px;"></td></tr>	
		  					<tr><th colspan="2" align="center">Result</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>	
		
  		<tr>
		          <td align="right" style="width:40%!important;"><font size="2" face="verdana"
        color="#1f6fa2"><b>R<sub>f</sub>:</b></font> </td>
                    <td align="left" style="width:60% !important;"><input type="text" name="R2" size="7" maxlength="15">
             <font size="2" face="verdana"
        color="#1f6fa2"> KOhms </b></font> </td></tr>
			
				<tr>
		          <td align="right" style="width:40%!important;" ><font size="2" face="verdana"
        color="#1f6fa2"><b>R<sub>a</sub>:</b></font>
                    <td align="left" style="width:60% !important;"><input type="text" name="R3" size="7" maxlength="15">
             <font size="2" face="verdana"
        color="#1f6fa2"> KOhms </b></font> </td></tr>
			
				<tr>
		          <td align="right" style="width:40%!important;"><font size="2" face="verdana"
        color="#1f6fa2"><b>R<sub>b</sub>:</b></font> </td>
                    <td align="left" style="width:60% !important;"><input type="text" name="R4" size="7" maxlength="15">
           <font size="2" face="verdana"
        color="#1f6fa2"> KOhms </b></font> </td></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
        </table>
		</center>
      </form>				
		
		</div>
		<div class="calc_right">
		<h5>Non-Inverting Operational Amplifier Resistor</h5>
		<p>This calculator determines the bias and feedback resistors for a non-inverting op-amp, given the gain and desired output bias point. There are many free parameters to the design so enter the value of R1, which will scale the other resistors.  Use V1 as the input for the inverting Op-Amp, and V2 as a voltage offset if needed.  Set V2 to zero if no offset is required. </p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
	
	<!-------Non-Inverting Operational Amplifier Resistor---END-->
	
	<!-----Parallel Resistor-----START---->
<div id="content-for-c" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">
	<center>		
	<table align="center" class="resist">
		  					<tr><th colspan="2" align="center">Enter Any Two Values</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>	
							<tr><td colspan="2" align="center"><img src="images/parckt.jpg" /></td>





<tr>
<form METHOD="POST">
<td align="right"><font size="2" face="verdana"
        color="#1f6fa2" ><b>Resistor R<sub><font size="2">a:</font></sub></b></font></td>


<td align="left"><INPUT size=8  maxlength="12" name="R1" type="text">
<SELECT name=R1M >
<OPTION value=0.000001>µohm
<OPTION value=0.001>mohm
<OPTION value=1 selected>ohm
<OPTION value=1000>Kohm
<OPTION value=1000000>Mohm
</OPTION></SELECT></td>

</tr>
<tr>
<td align="right">
<font size="2" face="verdana"
        color="#1f6fa2" ><b>Resistor R<sub><font size="2">b:</font></sub></b></font></td>

<td align="left"><INPUT size=8 name="R2" type="text">
<SELECT name=R2M >
<OPTION value=0.000001>µohm
<OPTION value=0.001>mohm

<OPTION value=1 selected>ohm
<OPTION value=1000>Kohm
<OPTION value=1000000>Mohm
</OPTION></SELECT>
</td>
</tr>
					

<tr>
<td align="right">
<font size="2" face="verdana"
        color="#1f6fa2" ><b>Parallel Resistance R<sub><font size="2">p:</font></sub></b></font></td>

 <td align="left"><input size=8 name="R" type="text">
<SELECT name=RM >
<OPTION value=0.000001>µohm
<OPTION value=0.001>mohm
<OPTION value=1 selected>ohm
<OPTION value=1000>Kohm
<OPTION value=1000000>Mohm
</OPTION></SELECT></TD>


</tr>

<tr>

<td align="right"><input TYPE="button" NAME="name" VALUE="Calculate" onClick="solve(this.form);" class="calculate_cal"></td>
<td align="left"><input TYPE="reset" VALUE="Clear" onClick="clearBoxes(this.form);" class="clear_cal"></td>
</tr>
<tr><td colspan="2" style="height:15px;"></td></tr>	
</form>
</table>
</center>

	
				
		
		</div>
		<div class="calc_right">
		<h5>Parallel Resistor</h5>
		<p>Two resistors in parallel and the resulting total resistance: Two of the same value, also show the equation that the results are always half. That makes it easier when
designing circuits or prototyping. With caps it's always double, then again caps just simply add up in parallel.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
		<!-----Parallel Resistor-----END---->
		
		<!----------------Wheatstone Bridge--START------------->
	<div id="content-for-d" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">
	
	<form method="POST" name="form" id="frm4">
	<center>
	<table align="center" class="resist">
	
		  					<tr><th colspan="2" align="center">Enter Any Two Values</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>	
							<tr><td colspan="2" align="center"><img src="images/bridge.jpg" border="0"></td></tr>
							<tr><td colspan="2" align="center"><img src="images/bridge rx form.bmp" border="0"><br>
			<img src="images/bridge vb form.bmp" border="0"></td></tr>							
  
		<tr>
		<td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Input Voltage V<sub>in</sub>:</b></font> </td>
		<td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b><input type="text" name="Vin" size="7" maxlength="15">
              V</b></font> </td></tr>
			  
			  <tr>
		<td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistance R<sub>a</sub>:</b></font> </td>
		<td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b><input type="text" name="R1" size="7" maxlength="15">
              ohms</b></font> </td></tr>
			  
			  <tr>
		<td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistance R<sub>b</sub>:</b></font> </td>
		<td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b><input type="text" name="R2" size="7" maxlength="15">
              ohms</b></font> </td></tr>
			  
		 <tr>
		<td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistance R<sub>c</sub>:</b></font> </td>
		<td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b><input type="text" name="R3" size="7" maxlength="15">
              ohms</b></font> </td></tr>
		  
		<tr>
            <td colspan="2" align="center">
			<input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
			  
			<tr><td colspan="2" style="height:15px;"></td></tr>	
		  					<tr><th colspan="2" align="center">Result</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>	
		
		<tr>
		<td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistance R<sub>x</sub>: </b></font></td>
		<td align="left"><input type="text" name="Rx" size="7" maxlength="15">
            <font size="2" face="verdana"
        color="#1f6fa2"><b> ohms</b></font> 
			 <input type="button" value="Calculate" name="B2" onClick="ComputeW(this.form);" style="float:right;" class="calculate_cal"></td></tr>
			  
		<tr>
		<td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Bridge Voltage V<sub>b</sub>:</b></font>
		<td align="left"><input type="text" name="Vb" size="7" maxlength="15">
             <font size="2" face="verdana"
        color="#1f6fa2"><b>    V</b></font>
		  <input type="button" value="Calculate" name="B1" onClick="ComputeW1(this.form);" style="float:right;" class="calculate_cal"></td></tr>
		  <tr><td colspan="2" style="height:15px;"></td></tr>	
         </table>
		 </center>
		 </form>
			
		
		</div>
		<div class="calc_right">
			<h5>Wheatstone Bridge</h5>
			<p>A Wheatstone bridge is a measuring instrument invented by Samuel Hunter Christie in 1833 and improved and popularized by Sir Charles Wheatstone in 1843. The Wheatstone bridge is an electrical bridge circuit used to measure resistance. The Wheatstone bridge network consists of four resistors Ra, Rb, Rc and Rx interconnected. If the ratio of the two resistances Rb / Ra is equal to the ratio of the two Rx / Rc, then the voltage across bridge Vb will be zero.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
	<!----------------Wheatstone Bridge--end------------>
	
	<!----------------LED Resistor----start------> 
	<div id="content-for-e" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

<form method="POST" name="form">  
	<center>
	<table align="center" class="resist">
	
		  					<tr><th colspan="2" align="center">Enter Values</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>	
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Source Voltage: </b></font> </td>
                    <td align="left"> <input type="text" name="Vs" size="7" maxlength="15">
              <font size="2" face="verdana"
        color="#1f6fa2"><b>Volts</b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Forward LED voltage: </b></font> </td>
                   <td align="left">  <input type="text" name="Vf" size="7" maxlength="15">
               <font size="2" face="verdana"
        color="#1f6fa2"><b> Volts</b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>LED Current: </b></font> </td>
                   <td align="left">  <input type="text" name="Imax" size="7" maxlength="15">
            <font size="2" face="verdana"
        color="#1f6fa2"><b>  miliAmps</b></font> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" value="Calculate" name="B1" onClick="ComputeLED(this.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>
		  					<tr><th colspan="2" align="center">Results</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>	
		
  		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistor:</b></font></td>
             <td align="left">       <input type="text" name="R" size="7" maxlength="15" readonly="readonly">
           <font size="2" face="verdana"
        color="#1f6fa2"> Ohms </b></font> </td></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>	
        </table>
		</center>
      </form>
		
		</div>
		<div class="calc_right">
			<h5>LED Resistor</h5>
			<p>LEDs are current controlled devices, meaning that the intensity of their light output is proportional to the current passing through them.  The calculations are simple and based on ohms law.  But first it should be noted that LEDs are rated with a nominal forward voltage drop.  If an LED is connect up such that it is emitting light, a multimeter can be used to measure a voltage drop across the LED.  This voltage drop usually varies from 1.5V up to 5V, depending on the power output of the LED, and also the color.  Low power LEDs tend to have low voltage drops, and high power LEDS have the higher voltage drops.   Likewise blue or white LEDs tend have higher voltage drops, than red LEDs. </p>

		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>		
<!----------------LED Resistor----end------> 	

<!------Inverting Op-Amp Resistor Calculator----start--->
<div id="content-for-f" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

<form method="POST" name= "form"> 
	<center>
	<table align="center" class="resist">
	
		  					<tr><th colspan="2" align="center">Enter Values</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>	
							<tr><td colspan="2" align="center"><img src="images/opampinv.jpg" border="0"></td></tr>
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Vout: </b></font> </td>
                  <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b> <input type="text" name="Vout" size="7" maxlength="15" >
              Volts</b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Gain: </b></font> </td>
                    <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b> <input type="text" name="A" size="7" maxlength="15">
               Volts</b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>R<sub>a</sub>: </b></font> </td>
                   <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>  <input type="text" name="R1" size="7" maxlength="15">
             KOhms</b></font> </td></tr>
			 
			 <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>V<sub>1</sub>: </b></font> </td>
                   <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>  <input type="text" name="V1" size="7" maxlength="15">
            Volts</b></font> </td></tr>
			 
			 <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>V<sub>2</sub>: </b></font> </td>
                   <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>  <input type="text" name="V2" size="7" maxlength="15">
             Volts</b></font> </td></tr>
			 
			 <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>V<sub>p</sub>: </b></font> </td>
                   <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>  <input type="text"  name="Vp" size="7" maxlength="15">
              Volts</b></font> </td></tr>
			 
			 <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>V<sub>n</sub>: </b></font> </td>
                 <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>    <input type="text" name="Vn" size="7" maxlength="15">
              Volts</b></font> </td></tr>

			  
		<tr>
            <td  align="right"><input type="Button" value="Calculate" name="Calculate" onClick="Compute2(this.form)" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
		</tr>	
      <tr><td colspan="2" style="height:15px;"></td></tr>
		  					<tr><th colspan="2" align="center">Result</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>	
		
  		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>R<sub>f</sub>:</b></font></td>
                    <td align="left"><input type="text" name="R2" size="7" maxlength="15" readonly="readonly">
           <font size="2" face="verdana"
        color="#1f6fa2"> KOhms </b></font> </td></tr>
			
				<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>R<sub>b</sub>:</b></font></td>
                    <td align="left"><input type="text" name="R3" size="7" maxlength="15" readonly="readonly">
           <font size="2" face="verdana"
        color="#1f6fa2"> KOhms </b></font></td></tr>
			
				<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>R<sub>g</sub>:</b></font> </td>
                   <td align="left"> <input type="text" name="R4" size="7" maxlength="15" readonly="readonly">
           <font size="2" face="verdana"
        color="#1f6fa2"> KOhms </b></font> </td></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>	
        </table>
		</center>
      </form>
		
		</div>
		<div class="calc_right">
		<h5>Inverting Op-Amp Resistor Calculator</h5>
							 	<p>This calculator determines the bias and feedback resistors for an op-amp, given the gain and desired output bias point.  There are many free parameters to the design 
so enter the value of R1, which will scale the other resistors.  Use V1 as the input for the inverting Op-Amp, and V2 as a voltage offset if needed.  Set V2 to zero if 
no offset is required.</p>

		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>		
						
	<!------Inverting Op-Amp Resistor Calculator--end----->		
        </div>
  
</div>

    </div>
</div>
</div>
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL1 li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL1 li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#fff'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL1 li").each(function() { 
			$(".firstUL1 li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#fff'}); //set the background color to light-blue to that div
	 });
});
</script>

<!--script for Resistor Color Code  -->

<SCRIPT>
var one=0;
var two=0;
var three=0;
var four=0;
var val1=0;
var val2=0;
var val3=0;
var val4=0;
var res1=0;
var res2=0;
var res3=0;
var res4=0;
var resout=0;
var restnce=0;
</script>
<SCRIPT language=JavaScript>
function calc(){
restnce=0;
resout=0;
one=(document.dropbox.bandone.options[document.dropbox.bandone.selectedIndex].value)
two=(document.dropbox.bandtwo.options[document.dropbox.bandtwo.selectedIndex].value)
three=(document.dropbox.bandthree.options[document.dropbox.bandthree.selectedIndex].value)
four=(document.dropbox.bandfour.options[document.dropbox.bandfour.selectedIndex].value)

val1=((one-100)*3)-2;
val2=((two-200)*3)+28;
val3=((three-300)*3)+58;
val4=((four-400)*3)+88;

var mn = new Array 
(
101	,	0	,	"Black",
102	,	10	,	"Brown",
103	,	20	,	"Red",
104	,	30	,	"Orange",
105	,	40	,	"Yellow",
106	,	50	,	"Green",
107	,	60	,	"Blue",
108	,	70	,	"Violet",
109	,	80	,	"Gray",
110	,	90	,	"White",

201	,	0	,	"Black",
202	,	1	,	"Brown",
203	,	2	,	"Red",
204	,	3	,	"Orange",
205	,	4	,	"Yellow",
206	,	5	,	"Green",
207	,	6	,	"Blue",
208	,	7	,	"Violet",
209	,	8	,	"Gray",
210	,	9	,	"White",

301	,	1	,	"Black",
302	,	10	,	"Brown",
303	,	100	,	"Red",
304	,	1000	,	"Orange",
305	,	10000	,	"Yellow",
306	,	100000	,	"Green",
307	,	1000000	,	"Blue",
308	,	10000000	,"Violet",
309	,	.1	,	"Gold",
310	,	.01	,	"Silver",

401	,	20	,	"None",
402	,	10	,	"Silver",
403	,	5	,	"Gold"
);
res1=mn[val1];res2=mn[val2];res3=mn[val3];res4=mn[val4];
restnce=(res1+res2)*res3;
resout=restnce;
if(restnce>1000){resout=(restnce/1000) + " K"}
if(restnce>1000000){resout=(restnce/1000000) + " MEG"}
document.outbox.z1.value=" "+(resout)+ " ohms    and   " +(res4)+" % tolerance";
}
</script>

<!--script for Resistor Color Code   ends-->

<!--script for Non-Inverting Operational Amplifier Resistor  -->
<script language="JavaScript">

function Validate(form) {
	if(!form.Zo.value) {
		alert("Enter Value for Zo.");
		form.Zo.focus();
		return false;
	}
	return true;
}


function Compute1(form) {
	with(Math) {
		var Vout = Number(document.forms.frm2.Vout.value);
		var A = Number(document.forms.frm2.A.value);
		var R1 = Number(document.forms.frm2.R1.value);
		var V1= Number(document.forms.frm2.V1.value);
		var V2= Number(document.forms.frm2.V2.value);	
		var Vp = Number(document.forms.frm2.Vp.value);
		var Vn = Number(document.forms.frm2.Vn.value);
		var R2;
		var R3;
		var R4;
		var Vout1; 
		var Vout2; 
		var Precision= 3;
		
		if(A<0) A= -A;
		
		if(Vn==Vp) Vn= -Vp;
		if(Vn>Vp) {
			temp=Vp;
			Vp= Vn;
			Vn= temp;
		}
		if(Vn>0.0) Vn= 0;
		
		R3= R1;
		Vout1= A*V1;
		Vout2= Vout-Vout1;
		R4= A*R3*V2/Vout2;
		R2=  A*(R1+R4)/R4*R1 - R1;

		form.R2.value= R2.toPrecision(Precision);
		form.R3.value= R3.toPrecision(Precision);
		form.R4.value= R4.toPrecision(Precision);
		form.Vp.value= Vp.toPrecision(Precision);
		form.Vn.value= Vn.toPrecision(Precision);
		form.A.value= A.toPrecision(Precision);
	}
}


</script>


<!--script for Non-Inverting Operational Amplifier Resistor   ends-->

<!--script for Parallel Resistor  -->
<script LANGUAGE="JavaScript">
<!--
var R1 = 0;
var R2 = 0;
var R = 0;
var R1M = 0;
var R2M = 0;
var RM = 0;

function clearBoxes(form)

{
form.R1.value = "";
form.R2.value = "";
form.R.value = "";
form.R1.focus();
}
function clearR1(form)
{
form.R1.value = "";
form.R1.focus();
}
function clearR2(form)
{
form.R2.value = "";
form.R2.focus();
}
function clearR(form)
{
form.R.value = "";
form.R.focus();
}
function solve(form)
{
var i = 3;
if(!form.R1.value) R1 = 0;
else R1 = eval(form.R1.value);
if(R1 == 0) i--;
if(!form.R2.value) R2 = 0; 
else R2 = eval(form.R2.value);
if(R2 == 0) i--;
if(!form.R.value) R = 0;
else R = eval(form.R.value);
if(R == 0) i--;
R1M = eval(form.R1M.value);
R2M = eval(form.R2M.value);
RM = eval(form.RM.value);
R1=R1*R1M;
R2=R2*R2M;
R=R*RM;
if(i == 0) return;      
if(i == 1)
{
alert("\nMissing a value. Two values are required!\n");
return;
}
if(R1 == 0)
{
R1 = (R*R2)/(R2-R);
form.R1.value = R1/R1M;
form.R1.focus();
return;     
}
if(R2 == 0)
{   
R2 = (R*R1)/(R1-R);
form.R2.value = R2/R2M;
form.R2.focus();
return;
}
R = (R1*R2)/(R1+R2);
form.R.value = R/RM;
form.R.focus();
return;
}
-->
</script>


<!--script for Parallel Resistor   ends-->

<!--script for Wheatstone Bridge  -->

<script language="JavaScript">


function ComputeW1(form) {
	with(Math) {
		var Precision= 3;
		var Vb;
		
		var Vin= Number(document.forms.frm4.Vin.value);
		var R1= Number(document.forms.frm4.R1.value);
		var R2= Number(document.forms.frm4.R2.value);
		var R3= Number(document.forms.frm4.R3.value);
		var Rx= Number(document.forms.frm4.Rx.value);

		Vb= Vin*(Rx/(R3+Rx) - R2/(R1+R2));

		form.Vb.value= Vb.toPrecision(Precision);
	}
}

function ComputeW(form) {
	with(Math) {
		var Precision= 3;
		var Rx;
		
		var Vin= Number(document.forms.frm4.Vin.value);
		var R1= Number(document.forms.frm4.R1.value);
		var R2= Number(document.forms.frm4.R2.value);
		var R3= Number(document.forms.frm4.R3.value);
		var Vb= Number(document.forms.frm4.Vb.value);
		
		//Rx= (R2*R3 + R3*(R1+R2)*(Vb/Vin)) / (R1+ (R1+R2)*(Vb/Vin));

		Rx= (R2*R3)/ R1;

		form.Rx.value= Rx.toPrecision(Precision);
	}
}

</script>

<!--script for Wheatstone Bridge   ends-->

<!--script for LED Resistor   -->
<script language="JavaScript">

function log10(x) {
	with(Math) {
		return log(x)/log(10);
	}
}

function ComputeLED(form) {
	with(Math) {
		var Vf= Number(form.Vf.value);
		var Vs= Number(form.Vs.value);
		var Imax= Number(form.Imax.value);
		var R;
		var Precision=3;
		
		R= (Vs-Vf)/Imax*1000;
		
		form.R.value= R.toPrecision(Precision);		
	}
}



</script>


<!--script for LED Resistor   ends-->

<!--script for Inverting Op-Amp Resistor Calculator   -->
<script language="JavaScript">

function Validate(form) {
	if(!form.Zo.value) {
		alert("Enter Value for Zo.");
		form.Zo.focus();
		return false;
	}
	return true;
}


function Compute2(form) {
	with(Math) {
		var Vout = Number(form.Vout.value);
		var A = Number(form.A.value);
		var R1 = Number(form.R1.value);
		var V1= Number(form.V1.value);
		var V2= Number(form.V2.value);	
		var Vp = Number(form.Vp.value);
		var Vn = Number(form.Vn.value);
		var R2;
		var R3;
		var R4;
		var Vout1; 
		var Vout2; 
		var Precision= 3;
		
		if(A>0) A= -A;
		
		if(Vn==Vp) Vn= -Vp;
		if(Vn>Vp) {
			temp=Vp;
			Vp= Vn;
			Vn= temp;
		}
		if(Vn>0.0) Vn= 0;

		R2= -A*R1;
		R3= R1;
		Vout1= A*V1;
		Vout2= Vout-Vout1;
		
		
		R4=   R3*(1-Vout2*R1/((R1+R2)*V2))/ (Vout2*R1/((R1+R2)*V2));
		
		form.R2.value= R2.toPrecision(Precision);
		form.R3.value= R3.toPrecision(Precision);
		form.R4.value= R4.toPrecision(Precision);
		form.Vp.value= Vp.toPrecision(Precision);
		form.Vn.value= Vn.toPrecision(Precision);
		form.A.value= A.toPrecision(Precision);
	}
}


</script>
<!--script for Inverting Op-Amp Resistor Calculator   ends-->

