
(function() {
    ohms_form();
    compute_watts(watts);
    compute_amperes(amperes);
    compute_volts(volts);
    compute_ohms(ohms);
})();

function ohms_form() {
    var div = document.getElementById('ohms-law');
    div.innerHTML = '\
<table align="center" style="margin-top:.3cm" class="lawcal">\
    <tr>\
        <td style="vertical-align:top">\
<form name="watts"><table class="innerlawcal">\
    <tr><th colspan="3">Calculate Watts</th></tr>\
	<tr><td style="height:15px" colspan="3"></td></tr>\
    <tr><td><input type="text" name="v" size="7" value="2" onkeyup="compute_watts(this.form);" />V</td>\
        <td><input type="text" name="i" size="7" value="2" onkeyup="compute_watts(this.form);" />A</td>\
        <td><input type="text" name="r" size="7" value="2" onkeyup="compute_watts(this.form);" />&Omega;</td></tr>\
    <tr><td><label>W = V &middot; I =</label></td>\
        <td colspan="2"><input type="text" name="VI" size="24" readonly="readonly" /></td></tr>\
    <tr><td><label>W = I&sup2;· R =</label></td>\
        <td colspan="2"><input type="text" name="I2R" size="24" readonly="readonly" /></td></tr>\
    <tr><td><label>W = V&sup2; / R =</label></td>\
        <td colspan="2"><input type="text" name="V2dR" size="24" readonly="readonly" /></td></tr>\
		<tr><td style="height:15px" colspan="3"></td></tr>\
</table></form>\
        </td>\
        <td style="vertical-align:top">\
<form name="amperes"><table class="innerlawcal">\
    <tr><th colspan="3">Calculate Amperes</th></tr>\
	<tr><td style="height:15px" colspan="3"></td></tr>\
    <tr><td colspan="3">Is it 3-Phase? <input type="checkbox" name="cb" checked="checked" onchange="compute_amperes(this.form);" /></td></tr>\
    <tr><td><input type="text" name="v" size="7" value="2" onkeyup="compute_amperes(this.form);" />V</td>\
        <td><input type="text" name="w" size="7" value="2" onkeyup="compute_amperes(this.form);" />W</td>\
        <td><input type="text" name="r" size="7" value="2" onkeyup="compute_amperes(this.form);" />&Omega;</td></tr>\
    <tr><td><label>I = V / R = </label></td>\
        <td colspan="2"><input type="text" name="VR" size="24" readonly="readonly" /></td></tr>\
    <tr><td><label id="equationI2"></label></td>\
        <td colspan="2"><input type="text" name="WV" size="24" readonly="readonly" /></td></tr>\
    <tr><td><label>I = &radic;(W / R) = </label></td>\
        <td colspan="2"><input type="text" name="sWdR" size="24" readonly="readonly" /></td></tr>\
		<tr><td style="height:15px" colspan="3"></td></tr>\
</table></form>\
        </td>\
    </tr>\
    <tr>\
        <td style="vertical-align:top">\
<form name="volts"><table class="innerlawcal">\
    <tr><th colspan="3">Calculate Volts</th></tr>\
	<tr><td style="height:15px" colspan="3"></td></tr>\
    <tr><td><input type="text" name="w" size="7" value="2" onkeyup="compute_volts(this.form);" />W</td>\
        <td><input type="text" name="i" size="7" value="2" onkeyup="compute_volts(this.form);" />A</td>\
        <td><input type="text" name="r" size="7" value="2" onkeyup="compute_volts(this.form);" />&Omega;</td></tr>\
    <tr><td><label>V = I &middot; R = </label></td>\
        <td colspan="2"><input type="text" name="IR" size="24" readonly="readonly" /></td></tr>\
    <tr><td><label>V = &radic;(W &middot; R) = </label></td>\
        <td colspan="2"><input type="text" name="sWR" size="24" readonly="readonly" /></td></tr>\
    <tr><td><label>V = W / I = </label></td>\
        <td colspan="2"><input type="text" name="WdI" size="24" readonly="readonly" /></td></tr>\
		<tr><td style="height:15px" colspan="3"></td></tr>\
</table></form>\
        </td>\
        <td style="vertical-align:top">\
<form name="ohms"><table class="innerlawcal">\
    <tr><th colspan="3">Calculate Ohms</th></tr>\
	<tr><td style="height:15px" colspan="3"></td></tr>\
    <tr><td><input type="text" name="v" size="7" value="2" onkeyup="compute_ohms(this.form);" />V</td>\
        <td><input type="text" name="i" size="7" value="2" onkeyup="compute_ohms(this.form);" />A</td>\
        <td><input type="text" name="w" size="7" value="2" onkeyup="compute_ohms(this.form);" />W</td></tr>\
    <tr><td><label>R = V / A = </label></td>\
        <td colspan="2"><input type="text" name="VdA" size="24" readonly="readonly" /></td></tr>\
    <tr><td><label>R = V&sup2; / W = </label></td>\
        <td colspan="2"><input type="text" name="V2dW" size="24" readonly="readonly" /></td></tr>\
    <tr><td><label>R = W / I&sup2; = </label></td>\
        <td colspan="2"><input type="text" name="WdI2" size="24" readonly="readonly" /></td></tr>\
		<tr><td style="height:15px" colspan="3"></td></tr>\
</table></form>\
        </td>\
    </tr>\
</table>';

  
}

function compute_watts(f) {
    var V = parseFloat(f.v.value);
    var I = parseFloat(f.i.value);
    var R = parseFloat(f.r.value);
    
    f.VI.value = V * I;
    f.I2R.value = I * I * R;
    f.V2dR.value = V * V / R;
}

function compute_amperes(f) {
    var W = parseFloat(f.w.value);
    var V = parseFloat(f.v.value);
    var R = parseFloat(f.r.value);
    
    f.VR.value = V / R;
    if (document.amperes.cb.checked) {
        document.getElementById("equationI2").innerHTML = "I = W / (1.73 &middot; V) = ";
        f.WV.value = W / (V * 1.73);
    } else {
        document.getElementById("equationI2").innerHTML = "I = W / V = ";
        f.WV.value = W / V;
    }
    f.sWdR.value = Math.sqrt(W / R);
}

function compute_volts(f) {
    var W = parseFloat(f.w.value);
    var I = parseFloat(f.i.value);
    var R = parseFloat(f.r.value);
    
    f.IR.value = I * R;
    f.sWR.value = Math.sqrt(W * R);
    f.WdI.value = W / I;
}

function compute_ohms(f) {
    var V = parseFloat(f.v.value);
    var I = parseFloat(f.i.value);
    var W = parseFloat(f.w.value);
    
    f.VdA.value = V / I;
    f.V2dW.value = V * V / W;
    f.WdI2.value = W / (I * I);
}