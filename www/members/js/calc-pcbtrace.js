(function() {
    var div = document.getElementById('pcb-trace-calc-container');
    div.innerHTML = '\
<form name="pcbtraceform">\
	<center>\
    <table width="100%">\
        <tr>\
            <th rowspan="2">Inputs:</th>\
            <td>Current</td>\
            <td><input onkeyup="pcb_trace_width(this.form);" type="text" value="10" name="current"></td>\
            <td>A</td></tr>\
        <tr>\
            <td>Thickness</td>\
            <td><input onkeyup="pcb_trace_width(this.form);" type="text" value="2" name="thickness"></td>\
            <td><select onchange="pcb_trace_width(this.form);" name="thicknessUnit">\
                    <option value="0.0035">oz/ft^2</OPTION>\
                    <option value="2.54e-3">mil</option>\
                    <option value="0.1" selected="selected">mm</option>\
                    <option value="1e-4">um</option>\
                </select></td></tr>\
        <tr><td colspan="4"</td></tr>\
        <tr>\
            <th rowspan="3">Optional inputs:</th>\
            <td>Temperature rise</td>\
            <td><input onkeyup="pcb_trace_width(this.form);" type="text" value="10" name="tempRise"></td>\
            <td><select onchange="pcb_trace_width(this.form);" name="tempRiseUnit">\
                    <option value="1" selected="selected">&deg;C</option>\
                    <option value="0.5555">&deg;F</option>\
                </select></td></tr>\
        <tr>\
            <td>Ambient temperature</td>\
            <td><input onkeyup="pcb_trace_width(this.form);" type="text" value="25" name="ambientTemp"></td>\
            <td><select onchange="pcb_trace_width(this.form);" name="ambientTempUnit">\
                    <option value="1" selected="selected">&deg;C</option>\
                    <option value="0.5555">&deg;F</option>\
                </select></td></tr>\
        <tr>\
            <td>Trace length</td>\
            <td><input onkeyup="pcb_trace_width(this.form);" type="text" value="1" name="length"></td>\
            <td><select onchange="pcb_trace_width(this.form)" name="lengthUnit">\
                    <option value="2.539999">inch</option>\
                    <option value="30.48037">feet</option>\
                    <option value="0.0025399">mil</option>\
                    <option value="0.1" selected="selected">mm</option>\
                    <option value="0.0001">um</option>\
                    <option value="1">cm</option>\
                    <option value="100">m</option>\
                </select></td></tr>\
        <tr><td colspan="4"></td></tr>\
        <tr>\
            <th rowspan="4">Results for internal layers:</th>\
            <td>Required trace width</td>\
            <td><input type="text" name="width_int" readonly="readonly" /></td>\
            <td><select onchange="pcb_trace_width(this.form);" name="width_intUnit">\
                    <option value="0.002539">mil</option>\
                    <option value="0.1" selected="selected">mm</option>\
                    <option value="0.0001">um</option>\
                </select></td></tr>\
        <tr>\
            <td>Resistance</td>\
            <td><input type="text" name="resistance_int" readonly="readonly" /></td>\
            <td>&Omega;</td></tr>\
        <tr>\
            <td>Voltage drop</td>\
            <td><input type="text" name="voltage_int" readonly="readonly" /></td>\
            <td>V</td>\
        </tr>\
        <tr>\
            <td>Power loss</td>\
            <td><input type="text" name="power_int" readonly="readonly" /></td>\
            <td>W</td></tr>\
        <tr><td colspan="4"></td></tr>\
        <tr>\
            <th rowspan="4">Results for external layers in air:</th>\
            <td>Required trace width</td>\
            <td><input type="text" name="width_ext" readonly="readonly" /></td>\
            <td><select onchange="pcb_trace_width(this.form);" name="width_extUnit">\
                    <option value="0.002539">mil</option>\
                    <option value="0.1" selected="selected">mm</option>\
                    <option value="0.0001">um</option>\
                </select></td></tr>\
        <tr>\
            <td>Resistance</td>\
            <td><input name="resistance_ext" readonly="readonly" type="text" /></td>\
            <td>&Omega;</td></tr>\
        <tr>\
            <td>Voltage drop</td>\
            <td><input name="voltage_ext" readonly="readonly" type="text" /></td>\
            <td>V</td></tr>\
        <tr>\
            <td>Power loss</td>\
            <td><input name="power_ext" readonly="readonly" type="text" /></td>\
            <td>W</td></tr>\
    </table>\
	</center>\
</form>';

   

    pcb_trace_width(pcbtraceform);
})();

function pcb_trace_width(f) {
    var current = parseFloat(f.current.value);
    var tempRise = parseFloat(f.tempRise.value) * parseFloat(f.tempRiseUnit.options[f.tempRiseUnit.selectedIndex].value);
    var thickness = parseFloat(f.thickness.value) * parseFloat(f.thicknessUnit.options[f.thicknessUnit.selectedIndex].value);
    var ambientTemp = parseFloat(f.ambientTemp.value);
    var ambientTempUnit = f.ambientTempUnit.options[f.ambientTempUnit.selectedIndex].value;
    if (ambientTempUnit != 1){
        ambientTemp -= 32;
        ambientTemp *= 5/9;
    }

    var length = parseFloat(f.length.value) * parseFloat(f.lengthUnit.options[f.lengthUnit.selectedIndex].value);

    var rho = 1.7e-6;
    var alpha = 3.9e-3;
    var area_int = Math.pow((current / (0.024 * Math.pow(tempRise, 0.44))), 1/0.725);
    area_int *= 2.54 * 2.54 / 1e6;
    var width_int = area_int / thickness;
    width_int /= f.width_intUnit.value;
    var temperature = 1 * ambientTemp + 1 * tempRise;
    var resistance_int = (rho * length / area_int) * (1 + alpha * (temperature-25));
    var voltage_int = resistance_int * current;
    var power_int = current * current * resistance_int;
    var area_ext = Math.pow((current / (0.048 * Math.pow(tempRise, 0.44))), 1/0.725);
    area_ext *= 2.54 * 2.54 / 1e6;
    var width_ext = area_ext / thickness;
    width_ext /= f.width_extUnit.value;
    var resistance_ext = (rho * length / area_ext) * (1 + alpha * (temperature-25));
    var voltage_ext = resistance_ext * current;
    var power_ext = current * current * resistance_ext;

    f.width_int.value = width_int.toPrecision(3);
    f.resistance_int.value = resistance_int.toPrecision(3);
    f.voltage_int.value = voltage_int.toPrecision(3);
    f.power_int.value = power_int.toPrecision(3);
    
    f.width_ext.value = width_ext.toPrecision(3);
    f.resistance_ext.value = resistance_ext.toPrecision(3);
    f.voltage_ext.value = voltage_ext.toPrecision(3);
    f.power_ext.value = power_ext.toPrecision(3);
}