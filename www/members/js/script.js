/**
 * Created by CAM on 4/24/14.
 */
// Code goes here
var editor;
$(function() {
    editor = new $.fn.dataTable.Editor( {
            "ajaxUrl": "http://harddrivebrokers.com/members/tf_data.php",
            "domTable": "#trading_floor",
            "fields": [
                {
                    "label": "Type",
                    "name": "Type",
                    "type": "select",
                    "ipOpts": [
                        { "label": "RFQ", "value": "RFQ" },
                        { "label": "WTB", "value": "WTB" },
                        { "label": "WTS", "value": "WTS" }
                    ]
                },
                {
                    "label": "Part Number",
                    "name": "Part_Number",
                    "type": "text"
                },
                {
                    "label": "MFG",
                    "name": "MFG",
                    "type": "text"
                },
                {
                    "label": "Price",
                    "name": "Price",
                    "type": "text"
                },
                {
                    "label": "QTY",
                    "name": "Qty",
                    "type": "text"
                },
                {
                    "label": "Condition",
                    "name": "Cond",
                    "type": "select",
                    "ipOpts": [
                        { "label": "NEW", "value": "NEW" },
                        { "label": "REF", "value": "REF" },
                        { "label": "USED", "value": "USED" }
                    ]
                },
                {
                    "label": "Description",
                    "name": "Description",
                    "type": "text"
                }
            ]
        } );

        // Edit record
        $('#trading_floor').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();

            editor.edit(
                $(this).parents('tr')[0],
                'Edit record',
                { "label": "Update", "fn": function () { editor.submit() } }
            );
        } );

        // Delete a record (without asking a user for confirmation)
        $('#trading_floor').on('click', 'a.editor_remove', function (e) {
            e.preventDefault();

            editor.remove( $(this).parents('tr')[0], '123', false, false );
            editor.submit();
        } );
} );

$(function() {
    $('#trading_floor').dataTable({
        "aoColumnDefs": [{
            "aTargets": [0],
            "sClass": "hiddenID"
            },
            {
            "aTargets": [1],
            "bSearchable": false,
            "bSortable": false,
            "sClass": "center",
            "mRender":function(data,type,full){
                return '<a href="loggedin_companies.php?cv='+full[0]+'"><img src="http://harddrivebrokers.com/members/images/look.png" border=0 width=16 height=16></a>';
            }},
            {
            "aTargets": [2],
            "bSearchable": false,
            "bSortable": false,
            "sClass": "center",
            "mRender":function(data,type,full){
                return '<a href="loggedin_member_connect.php?mc='+full[0]+'"><img src="http://harddrivebrokers.com/members/images/msg.png" border=0 width=16 height=16></a>';
            }},
            {
            "aTargets": [3],
            "bSearchable": true,
            "bSortable": true,
            "sClass": "center",
            "mRender":function(data,type,full){
                return full[3];
             }},
            {
                "aTargets": [4]
            },
            {
                "aTargets": [5]
            },
            {
                "aTargets": [6]
            },
            {
                "aTargets": [7]
            },
            {
                "aTargets": [8]
            },
            {
                "aTargets": [9]
            },
            {
                "aTargets": [10]
            },
            {
                "aTargets": [11]
            },
            {
             "aTargets": [12],
             "bSearchable": false,
             "bSortable": false,
             "sClass": "center",
             "mRender":function(){
                 return '<a href="" class="editor_edit"><img src="http://harddrivebrokers.com/members/images/edit.png" width="16"></a>&nbsp&nbsp&nbsp<a href="" class="editor_remove"><img src="http://harddrivebrokers.com/members/images/delete.png" width="16"></a>';
             }},
         ]
    });

});





