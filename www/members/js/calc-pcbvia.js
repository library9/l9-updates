(function() {
    var div = document.getElementById('pcb-via-calc-container');
    div.innerHTML = '\
<form name="pcbviaform">\
	<center>\
    <table width="100%">\
        <tr>\
            <th rowspan="3">Inputs:</th>\
            <td>Hole dia</td>\
            <td><input onkeyup="pcb_via_calc(this.form);" type="text" value="18" name="dia" /></td>\
            <td>\
                <select onchange="pcb_via_calc(this.form);" name="diaUnits">\
                    <option value="1">mil</option>\
                    <option value="39.37" selected="selected">mm</option>\
                    <option value="0.03937">um</option>\
                </select>\
            </td></tr>\
        <tr>\
            <td>Plating thickness</td>\
            <td><input onkeyup="pcb_via_calc(this.form);" type="text" value="1" name="thickness" /></td>\
            <td>\
                <select onchange="pcb_via_calc(this.form);" name="thicknessUnits">\
                    <option value="1">mil</option>\
                    <option value="39.37" selected="selected">mm</option>\
                    <option value="0.03937">um</option>\
                </select>\
            </td></tr>\
        <tr>\
            <td>Via length</td>\
            <td><input onkeyup="pcb_via_calc(this.form);" type="text" value="60" name="length" /></td>\
            <td>\
                <select onchange="pcb_via_calc(this.form);" name="lengthUnits">\
                    <option value="1">mil</option>\
                    <option value="39.37" selected="selected">mm</option>\
                    <option value="0.03937">um</option>\
                </select>\
            </td></tr>\
        <tr><td colspan="4"></td></tr>\
        <tr>\
            <th rowspan="2">Additional Inputs:</th>\
            <td>Applied current</td>\
            <td><input onkeyup="pcb_via_calc(this.form);" type="text" value="1" name="current" /></td>\
            <td>A</td></tr>\
        <tr>\
            <td>Plating resistivity</td>\
            <td><input onkeyup="pcb_via_calc(this.form);" type="text"  value="1.9e-6" name="resistivity" /></td>\
            <td>&Omega;cm</td></tr>\
        <tr><td colspan="4"></td></tr>\
        <tr>\
            <th rowspan="5">Results:</th>\
            <td>Resistance</td>\
            <td><input type="text" name="resistance" readonly="readonly" /></td>\
            <td>&Omega;</td></tr>\
        <tr>\
            <td>Voltage drop</td>\
            <td><input type="text" name="voltage_drop" readonly="readonly" /></td>\
            <td>V</td></tr>\
        <tr>\
            <td>Power loss</td>\
            <td><input type="text" name="power_loss" readonly="readonly" /></td>\
            <td>W</td></tr>\
        <tr>\
            <td>Estimated ampacity</td>\
            <td><input type="text" name="ampacity" readonly="readonly" /></td>\
            <td>A</td></tr>\
        <tr>\
            <td>Thermal resistance</td>\
            <td><input type="text" name="thermal_resistance" readonly="readonly" /></td>\
            <td>&deg;C/W</td></tr>\
    </table>\
	</center>\
</form>';

  

    pcb_via_calc(pcbviaform);
})();

function pcb_via_calc(f) {
    // Getting input
    var dia = parseFloat(f.dia.value) * parseFloat(f.diaUnits.options[f.diaUnits.selectedIndex].value);
    var thickness = parseFloat(f.thickness.value) * parseFloat(f.thicknessUnits.options[f.thicknessUnits.selectedIndex].value);
    var length = parseFloat(f.length.value) * parseFloat(f.lengthUnits.options[f.lengthUnits.selectedIndex].value);
    var current = parseFloat(f.current.value);
    var resistivity = parseFloat(f.resistivity.value * 1000 / 2.54);

    // Calculating
    var rhoth = 98;
    var area = Math.PI * (dia + thickness) * thickness;
    var resistance = resistivity *  length / area;
    var thermal_resistance = rhoth * length / area;
    var voltage_drop = current * resistance;
    var power_loss = current * voltage_drop;

    // IPC-2221 external layers constants
    var k = 0.048;
    var b = 0.44;
    var c = 0.725;
    var delta_t = 10;

    var ampacity = k * Math.pow(delta_t, b) * Math.pow(area, c);

    // Output
    f.resistance.value = resistance.toPrecision(3);
    f.voltage_drop.value = voltage_drop.toPrecision(3);
    f.power_loss.value = power_loss.toPrecision(3);
    f.thermal_resistance.value = thermal_resistance.toPrecision(3);
    f.ampacity.value = ampacity.toPrecision(3);
}