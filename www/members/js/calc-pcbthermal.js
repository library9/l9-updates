(function() {
    var div = document.getElementById('pcb-thermal-calc-container');
    div.innerHTML = '\
<form name="pcbthermalform">\
	<center>\
    <table width="100%">\
        <tr>\
            <th rowspan="6">Inputs:</th>\
            <td>Power dissipation</td>\
            <td><input onkeyup="pcb_thermal_calc(this.form)" type="text" value="1" name="powerDissipation" /></td>\
            <td>W</td></tr>\
        <tr>\
            <td>Temperature junction</td>\
            <td><input onkeyup="pcb_thermal_calc(this.form)" type="text" value="125" name="tempJunction" /></td>\
            <td>&deg;C</td></tr>\
        <tr>\
            <td>Temperature ambient</td>\
            <td><input onkeyup="pcb_thermal_calc(this.form)" type="text" value="25" name="tempAmbient" /></td>\
            <td>&deg;C</td></tr>\
        <tr>\
            <td>TR<sup><a href="#footnote" id="ref">1</a></sup> of junction to case</td>\
            <td><input onkeyup="pcb_thermal_calc(this.form)" type="text" value="10" name="thermalJunction" /></td>\
            <td>&deg;C/W</td></tr>\
        <tr>\
            <td>TR<sup><a href="#footnote">1</a></sup> of case to surface</td>\
            <td><input onkeyup="pcb_thermal_calc(this.form)" type="text" value="0" name="thermalCase" /></td>\
            <td>&deg;C/W</td></tr>\
        <tr>\
            <td>TR<sup><a href="#footnote">1</a></sup> of a 1 cmÂ² surface to ambient</td>\
            <td><input onkeyup="pcb_thermal_calc(this.form)" type="text" value="1000" name="thermalSurface" /></td>\
            <td>cm&sup2;&middot;&deg;C/W</td></tr>\
        <tr><td colspan="4"></td></tr>\
            <tr><th>Result:</th>\
            <td>Area</td>\
            <td><input type="text" name="area" readonly="readonly" /></td>\
            <td>cm&sup2;</td></tr>\
        <tr><td colspan="4"></td></tr>\
        <tr><td colspan="4"></td></tr>\
    </table>\
	</center>\
</form>';

   
    pcb_thermal_calc(pcbthermalform);
})();

function pcb_thermal_calc(f) {
    var powerDissipation = parseFloat(f.powerDissipation.value);
    var tempJunction = parseFloat(f.tempJunction.value);
    var tempAmbient = parseFloat(f.tempAmbient.value);
    var thermalJunction = parseFloat(f.thermalJunction.value);
    var thermalCase = parseFloat(f.thermalCase.value);
    var thermalSurface = parseFloat(f.thermalSurface.value);

    var area = powerDissipation * thermalSurface / (tempJunction - tempAmbient - powerDissipation * (1 * thermalJunction + 1 * thermalCase));

    f.area.value = area.toPrecision(8);
}