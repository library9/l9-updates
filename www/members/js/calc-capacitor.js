(function() {
    var div = document.getElementById('capacitor-calc-container');
    div.innerHTML = '\
<form name="capacitorform">\
<center>\
    <table align="center" style="margin-top:.3cm" class="capi">\
										<tr><th colspan="3">Enter your values:</th></tr>\
									<tr><td colspan="3" style="height:15px;"></td></tr>\
        <tr><td>Capacitance</td>\
            <td><input type="text" name="capacitance" value="3" onkeyup="calc_capacitor(this.form);" /></td>\
            <td>\
                <select name="capacitanceUnit" onchange="calc_capacitor(this.form);">\
                    <option value="1" selected="selected">F</option>\
                    <option value="1000000000000">pF</option>\
                    <option value="1000000000">nF</option>\
                    <option value="1000000">Î¼F</option>\
                    <option value="1000">mF</option>\
                </select>\
            </td></tr>\
        <tr><td>Voltage</td>\
            <td><input type="text" name="voltage" value="3" onkeyup="calc_capacitor(this.form);" /></td>\
            <td>\
                <select name="voltageUnit" onchange="calc_capacitor(this.form);">\
                    <option value="1" selected="selected">V</option>\
                    <option value="1000000000">nV</option>\
                    <option value="1000000">Î¼V</option>\
                    <option value="1000">mV</option>\
                    <option value="0.001">kV</option>\
                    <option value="1e-06">MV</option>\
                    <option value="1e-09">GV</option>\
                </select>\
            </td></tr>\
        <tr><td colspan="3"><hr /></td></tr>\
        <tr><td>Stored energy</td>\
            <td><input type="text" name="energy" readonly="readonly" /></td>\
            <td>\
                <select name="energyUnit" onchange="calc_capacitor(this.form);">\
                    <option value="1" selected="selected">J</option>\
                    <option disabled>- SI UNITS -</option>\
                    <option value="1000000000">nJ</option>\
                    <option value="1000000">Î¼J</option>\
                    <option value="1000">mJ</option>\
                    <option value="0.001">kJ</option>\
                    <option value="1e-06">MJ</option>\
                    <option value="1e-09">GJ</option>\
                    <option disabled>-- IMPERIAL --</option>\
                    <option value="0.238845897">cal</option>\
                    <option value="0.000238845897">kcal</option>\
                    <option value="0.737562149">ftÂ·pdf</option>\
                    <option value="0.237303604">ftÂ·pdl</option>\
                    <option value="0.000948317">BTU</option>\
                    <option disabled>- SCIENTIFIC -</option>\
                    <option value="6.2415115443e+18">eV</option>\
                    <option value="6.2415115443e+15">keV</option>\
                    <option value="2.2937126584e+17">Hartrees (au)</option>\
                    <option value="6.02214179e+20">kJ/mol</option>\
                    <option value="5.03411820099269e+22">wavenumber</option>\
                    <option value="1.50919066933814e+30">kHz photon</option>\
                    <option value="1.50919066933814e+27">MHz photon</option>\
                    <option value="1.50919066933814e+24">GHz photon</option>\
                    <option value="1.50919066933814e+21">THz photon</option>\
                    <option disabled>--- OTHER ---</option>\
                    <option value="10000000">erg</option>\
                    <option value="2.77777777777778e-07">kWh</option>\
                    <option value="1.633986928e-10">barrels of oil</option>\
                    <option value="3.4120842375e-11">tons of coal</option>\
                    <option value="2.3900573614e-10">tons of TNT</option>\
                    <option value="2.3900573614e-13">ktons of TNT</option>\
                    <option value="2.3900573614e-16">Mtons of TNT</option>\
                    <option value="1.1950286807e-18">Krakatoas</option>\
                </select>\
            </td></tr>\
        <tr><td>Stored charge</td>\
            <td><input type="text" name="charge" readonly="readonly" /></td>\
            <td>\
                <select onchange="calc_capacitor(this.form);" name="chargeUnit">\
                    <option value="1" selected="selected">C</option>\
                    <option disabled>- SI UNITS -</option>\
                    <option value="1000000">&micro;C</option>\
                    <option value="1">mC</option>\
                    <option value="0.001">kC</option>\
                    <option value="1e-06">MC</option>\
                    <option disabled>--- OTHER ---</option>\
                    <option value="6.2415097445e+18">e</option>\
                    <option value="1.036426899e-05">Faraday</option>\
                </select>\
            </td></tr>\
				<tr><td colspan="3" style="height:15px;"></td></tr>\
    </table>\
	</center>\
</form>';
    
   
    calc_capacitor(capacitorform);
})();

function calc_capacitor(f) {
    var capacitance = parseFloat(f.capacitance.value) / parseFloat(f.capacitanceUnit.options[f.capacitanceUnit.selectedIndex].value);

    var voltage = parseFloat(f.voltage.value) / parseFloat(f.voltageUnit.options[f.voltageUnit.selectedIndex].value);

    var energy = 0.5 * capacitance * voltage * voltage;
    var energyUnit = parseFloat(f.energyUnit.options[f.energyUnit.selectedIndex].value);
    energy *= energyUnit;
    f.energy.value = energy.toPrecision(8);

    var charge = capacitance * voltage;
    var chargeUnit = parseFloat(f.chargeUnit.options[f.chargeUnit.selectedIndex].value);
    charge *= chargeUnit;
    f.charge.value = charge.toPrecision(8);
}