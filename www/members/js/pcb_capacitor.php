<?php 

$groupswithaccess="ladmin,luser";
$loginpage="loggedin_index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>

		<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">

			<div class="statusbar"><p>Current Page - <strong>Calculators</strong></p></div>
			<div class="returnstat"><a href="calculator.php" class="menu_click">Return</a></div>

	</div>
</div>
<div class="app_content">
<div id="capacitor-calc-container"></div>
</div>
<script>
    (function() {
        var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
        calc.src = 'js/calc-capacitor.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
    })();
</script>