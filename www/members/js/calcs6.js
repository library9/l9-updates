(function () {
    var scripts = document.getElementsByTagName('script');
    scripts = Array.prototype.slice.call(scripts);
    
    var myScript = scripts.filter(function f(e) {
        return e.src.indexOf("calcs6.js") != -1;
    });

    var queryString = myScript[0].src.replace(/^[^\?]+\??/, '');

    var params = parseQuery(queryString);

    function parseQuery(query) {
        var Params = new Object();
        if (!query) return Params; // return empty object
        var Pairs = query.split(/[;&]/);
        for (var i = 0; i < Pairs.length; i++) {
            var KeyVal = Pairs[i].split('=');
            if (!KeyVal || KeyVal.length != 2) continue;
            var key = unescape(KeyVal[0]);
            var val = unescape(KeyVal[1]);
            val = val.replace(/\+/g, ' ');
            Params[key] = val;
        }
        return Params;
    }

    switch (params['calc']) {
    case 'one':
        microstip_impedance_form();
        microstrip(microstripimpedancecalc);
        break;
    case 'two':
        embedded_microstip_impedance_form();
        emb_microstrip(embeddedmicrostripimpedance);
        break;
    case 'three':
        symmetric_stipline_impedance_form();
        stripline(symmetricstriplineimpedance);
        break;
    case 'four':
        asymmetric_stripline_impedance_form();
        asym_stripline(asymetricstriplineimpedance);
        break;
    case 'five':
        wire_microstrip_impedance_form();
        wire_microstrip(wiremicrostripimpedance);
        break;
    case 'six':
        wire_stripline_impedance_form();
        wire_stripline(wirestriplineimpedance);
        break;
    case 'seven':
        edge_coupled_microstrip_impedance_form();
        ec_microstrip(edgecoupledmicrostripimpedance);
        break;
    case 'eight':
        edge_coupled_stripline_impedance_form();
        ec_stripline(edgecoupledstriplineimpedance);
        break;
    case 'nine':
        broadside_coupled_stripline_impedance_form();
        bc_stripline(broadsidecoupledstriplineimpedance);
        break;
    }
})();

function microstip_impedance_form() {
    var div = document.getElementById('microstrip-impedance-calc-container');
    div.innerHTML = '\
<form name="microstripimpedancecalc">\
    <table>\
        <tr>\
            <th rowspan="4">Inputs:</th>\
            <td>Trace Thickness (T)</td>\
            <td><input id="thick" type="text" size="12" name="thick" onkeyup="microstrip(this.form);" value="1.2"/></td>\
            <td><select id="thick_unit" onchange="microstrip(this.form);" name="thick_unit">\
                <option value="1.379" >oz/ft^2</option>\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Height (H)</td>\
            <td><input id="sub_height" type="text" size="12" name="sub_height" onkeyup="microstrip(this.form);" value="63"/></td>\
            <td><select id="sub_height_unit" onchange="microstrip(this.form);" name="sub_height_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Trace Width (W)</td>\
            <td><input id="width" type="text" size="12" name="width" onkeyup="microstrip(this.form);" value="10"/></td>\
            <td><select id="width_unit" onchange="microstrip(this.form);" name="width_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Dielectric (Er)</td>\
            <td><input id="er" type="text" size="12" name="er" onkeyup="microstrip(this.form);" value="4" /></td>\
        </td>\
        </tr>\
        <tr>\
            <td colspan="4"><hr /></td>\
        </tr>\
        <tr>\
            <th>Results:</th>\
            <td>Impedance (Z)</td>\
            <td><input type="text" size="12" name="impedance" readonly="readonly" /></td>\
        </tr>\
    </table>\
</form>';


}

function embedded_microstip_impedance_form() {
    var div = document.getElementById('embedded-microstrip-impedance-calc-container');
    div.innerHTML = '\
<form name="embeddedmicrostripimpedance">\
    <table>\
        <tr>\
            <th rowspan="5">Inputs:</th>\
            <td>Trace Thickness (T)</td>\
            <td><input id="thick" type="text" size="12" name="thick" onkeyup="emb_microstrip(this.form);" value="1.2"/></td>\
            <td><select id="thick_unit" onchange="emb_microstrip(this.form);" name="thick_unit">\
                <option value="1.379" >oz/ft^2</option>\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Height (H)</td>\
            <td><input id="sub_height1" type="text" size="12" name="sub_height1" onkeyup="emb_microstrip(this.form);" value="63"/></td>\
            <td><select id="sub_height1_unit" onchange="emb_microstrip(this.form);" name="sub_height1_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td></tr>\
        <tr>\
            <td>Substrate Height (H)</td>\
            <td><input id="sub_height2" type="text" size="12" name="sub_height2" onkeyup="emb_microstrip(this.form);" value="63"/></td>\
            <td><select id="sub_height2_unit" onchange="emb_microstrip(this.form);" name="sub_height2_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td></tr>\
        <tr>\
            <td>Trace Width (W)</td>\
            <td><input id="width" type="text" size="12" name="width" onkeyup="emb_microstrip(this.form);" value="10"/></td>\
            <td><select id="width_unit" onchange="emb_microstrip(this.form);" name="width_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td></tr>\
        <tr>\
            <td>Substrate Dielectric (Er)</td>\
            <td><input id="er" type="text" size="12" name="er" onkeyup="emb_microstrip(this.form);" value="4" /></td>\
        </td>\
        </tr>\
        <tr><td colspan="4"><hr /></td></tr>\
        <tr>\
            <th rowspan="4">Results:</th>\
            <td>Impedance (Z)</td>\
            <td><input type="text" size="12" name="impedance" readonly="readonly" /></td></tr>\
    </table>\
</form>';

 
}

function symmetric_stipline_impedance_form() {
    var div = document.getElementById('symmetric-stripline-impedance-calc-container');
    div.innerHTML = '\
<form name="symmetricstriplineimpedance">\
    <table>\
        <tr>\
            <th rowspan="4">Inputs:</th>\
            <td>Trace Thickness (T)</td>\
            <td><input id="thick" type="text" size="12" name="thick" onkeyup="stripline(this.form);" value="1.2"/></td>\
            <td><select id="thick_unit" onchange="stripline(this.form);" name="thick_unit">\
                <option value="1.379" >oz/ft^2</option>\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Height (H)</td>\
            <td><input id="sub_height" type="text" size="12" name="sub_height" onkeyup="stripline(this.form);" value="63"/></td>\
            <td><select id="sub_height_unit" onchange="stripline(this.form);" name="sub_height_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Trace Width (W)</td>\
            <td><input id="width" type="text" size="12" name="width" onkeyup="stripline(this.form);" value="10"/></td>\
            <td><select id="width_unit" onchange="stripline(this.form);" name="width_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Dielectric (Er)</td>\
            <td><input id="er" type="text" size="12" name="er" onkeyup="stripline(this.form);" value="4" /></td>\
        </td>\
        </tr>\
        <tr>\
            <td colspan="4"><hr /></td>\
        </tr>\
        <tr>\
            <th rowspan="4">Results:</th>\
            <td>Impedance (Z)</td>\
            <td><input type="text" size="12" name="impedance" readonly="readonly" /></td>\
        </tr>\
    </table>\
</form>';
}

function asymmetric_stripline_impedance_form() {
    var div = document.getElementById('asymmetric-stripline-impedance-calc-container');
    div.innerHTML = '\
 <form name="asymetricstriplineimpedance">\
    <table>\
        <tr>\
            <th rowspan="5">Inputs:</th>\
            <td>Trace Thickness (T)</td>\
            <td><input id="thick" type="text" size="12" name="thick" onkeyup="asym_stripline(this.form);" value="1.2"/></td>\
            <td><select id="thick_unit" onchange="asym_stripline(this.form);" name="thick_unit">\
                <option value="1.379" >oz/ft^2</option>\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Height (H)</td>\
            <td><input id="sub_height1" type="text" size="12" name="sub_height1" onkeyup="asym_stripline(this.form);" value="63"/></td>\
            <td><select id="sub_height1_unit" onchange="asym_stripline(this.form);" name="sub_height1_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Height (H)</td>\
            <td><input id="sub_height2" type="text" size="12" name="sub_height2" onkeyup="asym_stripline(this.form);" value="63"/></td>\
            <td><select id="sub_height2_unit" onchange="asym_stripline(this.form);" name="sub_height2_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Trace Width (W)</td>\
            <td><input id="width" type="text" size="12" name="width" onkeyup="asym_stripline(this.form);" value="10"/></td>\
            <td><select id="width_unit" onchange="asym_stripline(this.form);" name="width_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Dielectric (Er)</td>\
            <td><input id="er" type="text" size="12" name="er" onkeyup="asym_stripline(this.form);" value="4" /></td>\
        </td>\
        </tr>\
        <tr>\
            <td colspan="4"><hr /></td>\
        </tr>\
        <tr>\
            <th rowspan="4">Results:</th>\
            <td>Impedance (Z)</td>\
            <td><input type="text" size="12" name="impedance" readonly="readonly" /></td>\
        </tr>\
    </table>\
</form>';
}

function wire_microstrip_impedance_form() {
    var div = document.getElementById('wire-microstrip-impedance-calc-container');
    div.innerHTML = '\
<form name="wiremicrostripimpedance">\
    <table>\
        <tr>\
            <th rowspan="3">Inputs:</th>\
            <td>Trace Thickness (T)</td>\
            <td><input id="thick" type="text" size="12" name="thick" onkeyup="wire_microstrip(this.form);" value="1.2"/></td>\
            <td><select id="thick_unit" onchange="wire_microstrip(this.form);" name="thick_unit">\
                <option value="1.379" >oz/ft^2</option>\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Height (H)</td>\
            <td><input id="sub_height" type="text" size="12" name="sub_height" onkeyup="wire_microstrip(this.form);" value="63"/></td>\
            <td><select id="sub_height_unit" onchange="wire_microstrip(this.form);" name="sub_height_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Dielectric (Er)</td>\
            <td><input id="er" type="text" size="12" name="er" onkeyup="wire_microstrip(this.form);" value="4" /></td>\
        </td>\
        </tr>\
        <tr>\
            <td colspan="4"><hr /></td>\
        </tr>\
        <tr>\
            <th rowspan="4">Results:</th>\
            <td>Impedance (Z)</td>\
            <td><input type="text" size="12" name="impedance" readonly="readonly" /></td>\
        </tr>\
    </table>\
</form>';
};

function wire_stripline_impedance_form() {
    var div = document.getElementById('wire-stripline-impedance-calc-container');
    div.innerHTML = '\
<form name="wirestriplineimpedance">\
    <table>\
        <tr>\
            <th rowspan="3">Inputs:</th>\
            <td>Trace Thickness (T)</td>\
            <td><input id="thick" type="text" size="12" name="thick" onkeyup="wire_stripline(this.form);" value="1.2"/></td>\
            <td><select id="thick_unit" onchange="wire_stripline(this.form);" name="thick_unit">\
                <option value="1.379" >oz/ft^2</option>\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Height (H)</td>\
            <td><input id="sub_height" type="text" size="12" name="sub_height" onkeyup="wire_stripline(this.form);" value="63"/></td>\
            <td><select id="sub_height_unit" onchange="wire_stripline(this.form);" name="sub_height_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Dielectric (Er)</td>\
            <td><input id="er" type="text" size="12" name="er" onkeyup="wire_stripline(this.form);" value="4" /></td>\
        </td>\
        </tr>\
        <tr>\
            <td colspan="4"><hr /></td>\
        </tr>\
        <tr>\
            <th rowspan="4">Results:</th>\
            <td>Impedance (Z)</td>\
            <td><input type="text" size="12" name="impedance" readonly="readonly" /></td>\
        </tr>\
    </table>\
</form>';
};

function edge_coupled_microstrip_impedance_form() {
    var div = document.getElementById('edge-coupled-microstrip-impedance-calc-container');
    div.innerHTML = '\
<form name="edgecoupledmicrostripimpedance">\
    <table>\
        <tr>\
            <th rowspan="5">Inputs:</th>\
            <td>Trace Thickness (T)</td>\
            <td><input id="thick" type="text" size="12" name="thick" onkeyup="ec_microstrip(this.form);" value="1.2"/></td>\
            <td><select id="thick_unit" onchange="ec_microstrip(this.form);" name="thick_unit">\
                <option value="1.379" >oz/ft^2</option>\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
            </tr>\
        <tr>\
            <td>Substrate Height (H)</td>\
            <td><input id="sub_height" type="text" size="12" name="sub_height" onkeyup="ec_microstrip(this.form);" value="63"/></td>\
            <td><select id="sub_height_unit" onchange="ec_microstrip(this.form);" name="sub_height_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Trace Width (W)</td>\
            <td><input id="width" type="text" size="12" name="width" onkeyup="ec_microstrip(this.form);" value="10"/></td>\
            <td><select id="width_unit" onchange="ec_microstrip(this.form);" name="width_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Trace Spacing (S)</td>\
            <td><input id="space" type="text" size="12" name="space" onkeyup="ec_microstrip(this.form);" value="63"/></td>\
            <td><select id="space_unit" onchange="ec_microstrip(this.form);" name="space_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Dielectric (Er)</td>\
            <td><input id="er" type="text" size="12" name="er" onkeyup="ec_microstrip(this.form);" value="4" /></td>\
        </tr>\
        <tr>\
            <td colspan="4"><hr /></td>\
        </tr>\
        <tr>\
            <th rowspan="4">Results:</th>\
            <td>Odd (Z)</td>\
            <td><input type="text" size="12" name="odd" readonly="readonly" /></td>\
        </tr>\
        <tr>\
            <td>Common (Z)</td>\
            <td><input type="text" size="12" name="common" readonly="readonly" /></td>\
        </tr>\
        <tr>\
            <td>Even (Z)</td>\
            <td><input type="text" size="12" name="even" readonly="readonly" /></td>\
        </tr>\
        <tr>\
            <td>Differential (Z)</td>\
            <td><input type="text" size="12" name="differential" readonly="readonly" /></td>\
        </tr>\
    </table>\
</form>';
};

function edge_coupled_stripline_impedance_form() {
    var div = document.getElementById('edge-coupled-stripline-impedance-calc-container');
    div.innerHTML = '\
<form name="edgecoupledstriplineimpedance">\
    <table>\
        <tr>\
            <th rowspan="5">Inputs:</th>\
            <td>Trace Thickness (T)</td>\
            <td><input id="thick" type="text" size="12" name="thick" onkeyup="ec_stripline(this.form);" value="1.2"/></td>\
            <td><select id="thick_unit" onchange="ec_stripline(this.form);" name="thick_unit">\
                <option value="1.379" >oz/ft^2</option>\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Height (H)</td>\
            <td><input id="sub_height" type="text" size="12" name="sub_height" onkeyup="ec_stripline(this.form);" value="63"/></td>\
            <td><select id="sub_height_unit" onchange="ec_stripline(this.form);" name="sub_height_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Trace Width (W)</td>\
            <td><input id="width" type="text" size="12" name="width" onkeyup="ec_stripline(this.form);" value="10"/></td>\
            <td><select id="width_unit" onchange="ec_stripline(this.form);" name="width_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Trace Spacing (S)</td>\
            <td><input id="space" type="text" size="12" name="space" onkeyup="ec_stripline(this.form);" value="63"/></td>\
            <td><select id="space_unit" onchange="ec_stripline(this.form);" name="space_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Dielectric (Er)</td>\
            <td><input id="er" type="text" size="12" name="er" onkeyup="ec_stripline(this.form);" value="4" /></td>\
        </tr>\
        <tr>\
            <td colspan="4"><hr /></td>\
        </tr>\
        <tr>\
            <th rowspan="4">Results:</th>\
            <td>Odd (Z)</td>\
            <td><input type="text" size="12" name="odd" readonly="readonly" /></td>\
        </tr>\
        <tr>\
            <td>Common (Z)</td>\
            <td><input type="text" size="12" name="common" readonly="readonly" /></td>\
        </tr>\
        <tr>\
            <td>Even (Z)</td>\
            <td><input type="text" size="12" name="even" readonly="readonly" /></td>\
        </tr>\
        <tr>\
            <td>Differential (Z)</td>\
            <td><input type="text" size="12" name="differential" readonly="readonly" /></td>\
        </tr>\
    </table>\
</form>';
};

function broadside_coupled_stripline_impedance_form() {
    var div = document.getElementById('broadside-coupled-stripline-impedance-calc-container');
    div.innerHTML = '\
<form name="broadsidecoupledstriplineimpedance">\
    <table>\
        <tr>\
            <th rowspan="5">Inputs:</th>\
            <td>Trace Thickness (T)</td>\
            <td><input id="thick" type="text" size="12" name="thick" onkeyup="bc_stripline(this.form);" value="1.2"/></td>\
            <td><select id="thick_unit" onchange="bc_stripline(this.form);" name="thick_unit">\
                <option value="1.379" >oz/ft^2</option>\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Height (H)</td>\
            <td><input id="sub_height" type="text" size="12" name="sub_height" onkeyup="bc_stripline(this.form);" value="63"/></td>\
            <td><select id="sub_height_unit" onchange="bc_stripline(this.form);" name="sub_height_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Trace Width (W)</td>\
            <td><input id="width" type="text" size="12" name="width" onkeyup="bc_stripline(this.form);" value="10"/></td>\
            <td><select id="width_unit" onchange="bc_stripline(this.form);" name="width_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Trace Spacing (S)</td>\
            <td><input id="space" type="text" size="12" name="space" onkeyup="bc_stripline(this.form);" value="2"/></td>\
            <td><select id="space_unit" onchange="bc_stripline(this.form);" name="space_unit">\
                <option value="1" selected="selected">mil</option>\
                <option value="393">cm</option>\
                <option value="39.3">mm</option>\
                <option value="0.0393">um</option>\
                <option value="1000">inch</option>\
            </select></td>\
        </tr>\
        <tr>\
            <td>Substrate Dielectric (Er)</td>\
            <td><input id="er" type="text" size="12" name="er" onkeyup="bc_stripline(this.form);" value="4" /></td>\
        </tr>\
        <tr>\
            <td colspan="4"><hr /></td>\
        </tr>\
        <tr>\
            <th rowspan="4">Results:</th>\
            <td>Odd (Z)</td>\
            <td><input type="text" size="12" name="odd" readonly="readonly" /></td>\
        </tr>\
        <tr>\
            <td>Common (Z)</td>\
            <td><input type="text" size="12" name="common" readonly="readonly" /></td>\
        </tr>\
        <tr>\
            <td>Even (Z)</td>\
            <td><input type="text" size="12" name="even" readonly="readonly" /></td>\
        </tr>\
        <tr>\
            <td>Differential (Z)</td>\
            <td><input type="text" size="12" name="differential" readonly="readonly" /></td>\
        </tr>\
    </table>\
</form>';

   
};

function microstrip(f) {
    var t = parseFloat(f.thick.value) * f.thick_unit.options[f.thick_unit.selectedIndex].value;
    var h = parseFloat(f.sub_height.value) * f.sub_height_unit.options[f.sub_height_unit.selectedIndex].value;
    var w = parseFloat(f.width.value) * f.width_unit.options[f.width_unit.selectedIndex].value;
    var er = parseFloat(f.er.value);

    trace = microstrip_calc_r1(w, t, h, er);

    f.impedance.value = trace.zo.toPrecision(4);
}

function microstrip_calc_r1(w, t, h, er) {
    var er_temp1 = ((er + 1) / 2) + ((er - 1) / 2) * (Math.sqrt(w / (w + 12 * h)) + 0.04 * Math.pow((1 - w / h), 2));
    var er_temp2 = ((er + 1) / 2) + ((er - 1) / 2) * (Math.sqrt(w / (w + 12 * h)));
    var er_eff = 0;
    if ((w / h) < 1) {
        er_eff = er_temp1;
    } else {
        er_eff = er_temp2;
    }
    var w_eff = w + (t / Math.PI) * Math.log(4 * Math.exp(1) / Math.sqrt(Math.pow((t / h), 2) + Math.pow((t / (w * Math.PI + 1.1 * t * Math.PI)), 2))) * ((er_eff + 1) / 2 * er_eff);
    var no = 377;
    var zo = 0;
    var temp1 = Math.sqrt((16 * Math.pow((h / w_eff), 2) * Math.pow(((14 * er_eff + 8) / (11 * er_eff)), 2) + ((er_eff + 1) / (2 * er_eff)) * Math.pow((Math.PI), 2)));
    var temp2 = 4 * (h / w_eff) * ((14 * er_eff + 8) / (11 * er_eff));
    var temp3 = 4 * (h / w_eff);
    var temp4 = (no / (2 * Math.PI * Math.sqrt(2) * Math.sqrt(er_eff + 1)))
    if (((w / h) < 0.1) && ((w / h) > 3) && (h <= 0) && (w <= 0) && (t <= 0) && (er < 1) && (er > 15)) {
        zo = 0;
    } else {
        zo = (87 * Math.log(5.98 * (h / 1000) / (0.8 * (w / 1000) + (t / 1000) * 1)) * (1 - (((h / 1000) + (t / 1000)) * 1 - (h / 1000) * 1 - (t / 1000) * 1) / 0.1)) / Math.sqrt(er * 1 + 1.41);
    }
    return {
        zo: zo,
        er_eff: er_eff
    };
}

function emb_microstrip(f) {
    var t = parseFloat(f.thick.value) * f.thick_unit.options[f.thick_unit.selectedIndex].value;
    var h1 = parseFloat(f.sub_height1.value) * f.sub_height1_unit.options[f.sub_height1_unit.selectedIndex].value;
    var h2 = parseFloat(f.sub_height2.value) * f.sub_height2_unit.options[f.sub_height2_unit.selectedIndex].value;
    var w = parseFloat(f.width.value) * f.width_unit.options[f.width_unit.selectedIndex].value;
    var er = parseFloat(f.er.value);

    trace = embedded_microstrip(w, t, h1, h2, er);

    f.impedance.value = trace.zo.toPrecision(4);
}

function embedded_microstrip(w, t, h1, h2, er) {
    var zo_surf = microstrip_calc(w, t, h1, er);
    var b = h2 - h1;
    var zo_embed = zo_surf.zo * (1 / Math.sqrt((Math.exp((-2 * b) / h1)) + (er / zo_surf.er_eff) * (1 - Math.exp((-2 * b) / h1))))
    return {
        zo: zo_embed
    };
}

function microstrip_calc(w, t, h, er) {
    var er_temp1 = ((er + 1) / 2) + ((er - 1) / 2) * (Math.sqrt(w / (w + 12 * h)) + 0.04 * Math.pow((1 - w / h), 2));
    var er_temp2 = ((er + 1) / 2) + ((er - 1) / 2) * (Math.sqrt(w / (w + 12 * h)));
    var er_eff = 0;
    if ((w / h) < 1) {
        er_eff = er_temp1;
    } else {
        er_eff = er_temp2;
    }
    var w_eff = w + (t / Math.PI) * Math.log(4 * Math.exp(1) / Math.sqrt(Math.pow((t / h), 2) + Math.pow((t / (w * Math.PI + 1.1 * t * Math.PI)), 2))) * ((er_eff + 1) / 2 * er_eff);
    var no = 377;
    var temp1 = Math.sqrt((16 * Math.pow((h / w_eff), 2) * Math.pow(((14 * er_eff + 8) / (11 * er_eff)), 2) + ((er_eff + 1) / (2 * er_eff)) * Math.pow((Math.PI), 2)));
    var temp2 = 4 * (h / w_eff) * ((14 * er_eff + 8) / (11 * er_eff));
    var temp3 = 4 * (h / w_eff);
    var temp4 = (no / (2 * Math.PI * Math.sqrt(2) * Math.sqrt(er_eff + 1)))
    var zo = temp4 * Math.log(1 + temp3 * (temp2 + temp1));
    return {
        zo: zo,
        er_eff: er_eff
    };
}

function stripline(f) {
    var t = parseFloat(f.thick.value) * f.thick_unit.options[f.thick_unit.selectedIndex].value;
    var h = parseFloat(f.sub_height.value) * f.sub_height_unit.options[f.sub_height_unit.selectedIndex].value;
    var w = parseFloat(f.width.value) * f.width_unit.options[f.width_unit.selectedIndex].value;
    var er = parseFloat(f.er.value);
    trace = sym_stripline(w, t, h, er);
    f.impedance.value = trace.zo.toPrecision(3)
}

function sym_stripline(w, t, h, er) {
    var zo = 0;
    m = (6 * h) / (3 * h + t);
    w_eff = w + (t / Math.PI) * Math.log(Math.exp(1) / Math.sqrt(Math.pow((t / (4 * h + t)), 2) + Math.pow(((Math.PI * t) / (4 * (w + 1.1 * t))), m)));
    no = 377;
    zo_ss = (no / (2 * Math.PI * Math.sqrt(er))) * Math.log(1 + ((8 * h) / (Math.PI * w_eff)) * (((16 * h) / (Math.PI * w_eff)) + Math.sqrt(Math.pow(((16 * h) / (Math.PI * w_eff)), 2) + 6.27)));
    b = 2 * h + t;
    D = (w / 2) * (1 + (t / (Math.PI * w)) * (1 + Math.log((4 * Math.PI * w) / t)) + .551 * Math.pow((t / w), 2));
    zo_ss_t2 = (60 / Math.sqrt(er)) * Math.log((4 * b) / (Math.PI * D));
    theta = ((2 * b) / (b - t)) * Math.log((2 * b - t) / (b - t)) - (t / (b - t)) * Math.log((2 * b * t - Math.pow(t, 2)) / (Math.pow((b - t), 2)));
    zo_ss_w2 = 94.15 / (((w / b) / (1 - (t / b))) + (theta / Math.PI));
    if (((w / b) < .35) && ((t / b) <= .25) && ((t / w) <= .11)) {
        zo = zo_ss_t2;
    } else {
        zo = zo_ss_w2;
    }

    return {
        zo: zo
    };
}

function asym_stripline(f) {
    var t = parseFloat(f.thick.value) * f.thick_unit.options[f.thick_unit.selectedIndex].value;
    var h1 = parseFloat(f.sub_height1.value) * f.sub_height1_unit.options[f.sub_height1_unit.selectedIndex].value;
    var h2 = parseFloat(f.sub_height2.value) * f.sub_height2_unit.options[f.sub_height2_unit.selectedIndex].value;
    var w = parseFloat(f.width.value) * f.width_unit.options[f.width_unit.selectedIndex].value;
    var er = parseFloat(f.er.value);
    trace = asym_stripline_calc(w, t, h1, h2, er);
    f.impedance.value = trace.zo.toPrecision(3)
}

function asym_stripline_calc(w, t, h1, h2, er) {

    var h_eff = (h1 + h2) / 2;
    var m = (6 * h_eff) / (3 * h_eff + t);

    var zo_ss_h_eff = sym_stripline(w, t, h_eff, 1);

    var zo_ss_h1 = sym_stripline(w, t, h1, 1);

    var zo_ss_h2 = sym_stripline(w, t, h2, 1);

    var zo_air = 2 * ((zo_ss_h1.zo * zo_ss_h2.zo) / (zo_ss_h1.zo + zo_ss_h2.zo));
    var delta_zo_air = .0325 * Math.PI * Math.pow(zo_air, 2) * Math.pow(Math.abs(.5 - .5 * ((2 * h1 + t) / (h1 + h2 + t))), 2.2) * Math.pow(Math.abs((t + w) / (h1 + h2 + t)), 2.9);
    var zo_as = (1 / Math.sqrt(er)) * (zo_ss_h_eff.zo - delta_zo_air);
    return {
        zo: zo_as
    };
}

function wire_microstrip(f) {
    var d = parseFloat(f.thick.value) * f.thick_unit.options[f.thick_unit.selectedIndex].value;
    var h = parseFloat(f.sub_height.value) * f.sub_height_unit.options[f.sub_height_unit.selectedIndex].value;
    var er = parseFloat(f.er.value);
    trace = wire_microstrip_calc(d, h, er);
    f.impedance.value = trace.zo.toPrecision(3);
}

function wire_microstrip_calc(d, h, er) {
    no = 377;
    er_eff1 = ((er + 1) / 2) + ((er - 1) / 2) * (Math.sqrt(d / (d + 12 * h)) + .04 * Math.pow((1 - (d / h)), 2));
    er_eff2 = ((er + 1) / 2) + ((er - 1) / 2) * (Math.sqrt(d / (d + 12 * h)));
    if ((d / h) < 1) {
        er_eff = er_eff1;
    } else {
        er_eff = er_eff2;
    }
    temp = (2 * h + d) / d;
    zo_wm = (no / (2 * Math.PI * Math.sqrt(er_eff))) * Math.log(temp + Math.sqrt(temp * temp - 1));
    return {
        zo: zo_wm
    };
}

function wire_stripline(f) {
    var d = parseFloat(f.thick.value) * f.thick_unit.options[f.thick_unit.selectedIndex].value;
    var h = parseFloat(f.sub_height.value) * f.sub_height_unit.options[f.sub_height_unit.selectedIndex].value;
    var er = parseFloat(f.er.value);
    trace = wire_stripline_calc(d, h, er);
    f.impedance.value = trace.zo.toPrecision(3);
}

function wire_stripline_calc(d, h, er) {
    no = 377;
    zo_ws = (no / (2 * Math.PI * Math.sqrt(er))) * Math.log((4 * h) / (Math.PI * d));
    return {
        zo: zo_ws
    };
}

function ec_microstrip(f) {
    var t = parseFloat(f.thick.value) * f.thick_unit.options[f.thick_unit.selectedIndex].value;
    var h = parseFloat(f.sub_height.value) * f.sub_height_unit.options[f.sub_height_unit.selectedIndex].value;
    var w = parseFloat(f.width.value) * f.width_unit.options[f.width_unit.selectedIndex].value;
    var s = parseFloat(f.space.value) * f.space_unit.options[f.space_unit.selectedIndex].value;
    var er = parseFloat(f.er.value);
    trace = ec_microstrip_calc(w, t, h, s, er);
    f.odd.value = trace.zo_odd.toPrecision(3);
    f.even.value = trace.zo_even.toPrecision(3);
    f.common.value = trace.zo_common.toPrecision(3);
    f.differential.value = trace.zo_diff.toPrecision(3);
}

function ec_microstrip_calc(w, t, h, s, er) {
    var no = 377;
    var er_eff1 = ((er + 1) / 2) + ((er - 1) / 2) * (Math.sqrt(w / (w + 12 * h)) + .04 * Math.pow((1 - (w / h)), 2));
    var er_eff2 = ((er + 1) / 2) + ((er - 1) / 2) * (Math.sqrt(w / (w + 12 * h)));
    var er_eff = 0;
    if ((w / h) < 1) {
        er_eff = er_eff1;
    } else {
        er_eff = er_eff2;
    }
    var u = w / h;
    var a0 = .7287 * (er_eff - .5 * (er + 1)) * (1 - Math.exp(-.179 * u));
    var b0 = (.747 * er) / (.15 + er);
    var c0 = b0 - (b0 - .207) * Math.exp(-.414 * u);
    var d0 = .593 + .694 * Math.exp(-.562 * u);
    var g = s / h;
    var w_eff = w + (t / Math.PI) * Math.log((4 * Math.exp(1)) / Math.sqrt(Math.pow((t / h), 2) + Math.pow((t / (w * Math.PI + 1.1 * t * Math.PI)), 2))) * ((er_eff + 1) / (2 * er_eff));
    var er_eff_o = (.5 * (er + 1) + a0 - er_eff) * Math.exp(-c0 * Math.pow(g, d0)) + er_eff;
    var zo_surf = (no / (2 * Math.PI * Math.sqrt(2) * Math.sqrt(er_eff + 1))) * Math.log(1 + 4 * (h / w_eff) * (4 * (h / w_eff) * ((14 * er_eff + 8) / (11 * er_eff)) + Math.sqrt(16 * Math.pow((h / w_eff), 2) * Math.pow(((14 * er_eff + 8) / (11 * er_eff)), 2) + ((er_eff + 1) / (2 * er_eff)) * Math.pow(Math.PI, 2))));
    var q1 = .8695 * Math.pow(u, .194);
    var q2 = 1 + .7519 * g + .189 * Math.pow(g, 2.31);
    var q3 = .1975 + Math.pow((16.6 + Math.pow((8.4 / g), 6)), -.387) + (1 / 241) * Math.log((Math.pow(g, 10)) / (1 + Math.pow((g / 3.4), 10)));
    var q4 = 2 * q1 / (q2 * (Math.exp(-g) * Math.pow(u, q3) + (2 - Math.exp(-g)) * Math.pow(u, -q3)));
    var q5 = 1.794 + 1.14 * Math.log(1 + (.638 / (g + .517 * Math.pow(g, 2.43))));
    var q6 = .2305 + (1 / 281.3) * Math.log(Math.pow(g, 10) / (1 + Math.pow((g / 5.8), 10))) + (1 / 5.1) * Math.log(1 + .598 * Math.pow(g, 1.154));
    var q7 = (10 + 190 * Math.pow(g, 2)) / (1 + 82.3 * Math.pow(g, 3));
    var q8 = Math.exp(-6.5 - .95 * Math.log(g) - Math.pow((g / .15), 5));
    var q9 = Math.log(q7) * (q8 + (1 / 16.5));
    var q10 = (1 / q2) * (q2 * q4 - q5 * Math.exp(Math.log(u) * q6 * Math.pow(u, -q9)));
    var zo_odd = (zo_surf * Math.sqrt(er_eff / er_eff_o)) / (1 - (zo_surf / no) * q10 * Math.sqrt(er_eff));
    var v = ((u * (20 + Math.pow(g, 2))) / (10 + Math.pow(g, 2))) + g * Math.exp(-g);
    var aev = 1 + ((Math.log((Math.pow(v, 4) + Math.pow((v / 52), 2)) / (Math.pow(v, 4) + .432))) / 49) + ((Math.log(1 + Math.pow((v / 18.1), 3))) / 18.7);
    var beer = .564 * Math.pow(((er - .9) / (er + 3)), .053);
    var er_eff_e = .5 * (er + 1) + .5 * (er - 1) * (1 + Math.pow((10 / v), (-aev * beer)));
    var zo_even = (zo_surf * Math.sqrt(er_eff / er_eff_e)) / (1 - (zo_surf / no) * q4 * Math.sqrt(er_eff));
    var zo_common = zo_even / 2;
    var zo_diff = zo_odd * 2;

    return {
        zo_odd: zo_odd,
        zo_even: zo_even,
        zo_common: zo_common,
        zo_diff: zo_diff
    };
}

function sym_stripline(w, t, h, er) {

    m = (6 * h) / (3 * h + t);
    w_eff = w + (t / Math.PI) * Math.log(Math.exp(1) / Math.sqrt(Math.pow((t / (4 * h + t)), 2) + Math.pow(((Math.PI * t) / (4 * (w + 1.1 * t))), m)));
    no = 377;
    zo_ss = (no / (2 * Math.PI * Math.sqrt(er))) * Math.log(1 + ((8 * h) / (Math.PI * w_eff)) * (((16 * h) / (Math.PI * w_eff)) + Math.sqrt(Math.pow(((16 * h) / (Math.PI * w_eff)), 2) + 6.27)));
    b = 2 * h + t;
    D = (w / 2) * (1 + (t / (Math.PI * w)) * (1 + Math.log((4 * Math.PI * w) / t)) + .551 * Math.pow((t / w), 2));
    zo_ss_t2 = (60 / Math.sqrt(er)) * Math.log((4 * b) / (Math.PI * D));
    theta = ((2 * b) / (b - t)) * Math.log((2 * b - t) / (b - t)) - (t / (b - t)) * Math.log((2 * b * t - Math.pow(t, 2)) / (Math.pow((b - t), 2)));
    zo_ss_w2 = 94.15 / (((w / b) / (1 - (t / b))) + (theta / Math.PI));
    if (((w / b) < .35) && ((t / b) <= .25) && ((t / w) <= .11)) {
        zo = zo_ss_t2;
    } else {
        zo = zo_ss_w2;
    }

    return {
        zo: zo
    };
}

function bangbang(x) {
    var result, i, x2;

    result = 1;
    if (x == 0 && x == (-1))
        return (result);

    x2 = x / 2;
    for (i = 0; i < x2; i++) {
        result *= x;
        x -= 2;
    }

    return result;
}

function tanh(x) {
    var result = ((Math.exp(2 * x) - 1) / (Math.exp(2 * x) + 1));
    return (result);
}

function coth(x) {
    var result = ((Math.exp(2 * x) + 1) / (Math.exp(2 * x) - 1));
    return (result);
}

function sech(x) {
    var result = 2 / (Math.exp(x) + Math.exp(-x));
    return (result);
}

function eliptical(k) {
    var temp = 1
    var n = 0;
    for (n = 1; n <= 20; n = n + 1) {
        var a = 2 * n - 1;
        var b = 2 * n;
        var fa = bangbang(a);
        var fb = bangbang(b);
        var temp = temp + Math.pow((fa / fb), 2) * Math.pow(k, b);
    }
    var result = Math.PI * temp / 2;
    return (result);
}

function ec_stripline(f) {
    var t = parseFloat(f.thick.value) * f.thick_unit.options[f.thick_unit.selectedIndex].value;
    var h = parseFloat(f.sub_height.value) * f.sub_height_unit.options[f.sub_height_unit.selectedIndex].value;
    var w = parseFloat(f.width.value) * f.width_unit.options[f.width_unit.selectedIndex].value;
    var s = parseFloat(f.space.value) * f.space_unit.options[f.space_unit.selectedIndex].value;
    var er = parseFloat(f.er.value);
    trace = ecs_stripline_calc(w, t, s, h, er)
    f.odd.value = trace.zo_odd.toPrecision(3);
    f.even.value = trace.zo_even.toPrecision(3);
    f.common.value = trace.zo_common.toPrecision(3);
    f.differential.value = trace.zo_diff.toPrecision(3);
}

function ecs_stripline_calc(w, t, s, b, er) {
    var no = 377;
    var h = (b - t) / 2;
    var ke = tanh((Math.PI * w) / (2 * b)) * tanh((Math.PI / 2) * (w + s) / b);
    var ko = tanh((Math.PI * w) / (2 * b)) * coth((Math.PI / 2) * (w + s) / b);
    var ke_prime = Math.sqrt(1 - Math.pow(ke, 2));
    var ko_prime = Math.sqrt(1 - Math.pow(ko, 2));
    var Z0_e_0 = (30 * Math.PI / Math.sqrt(er)) * (eliptical(ke_prime) / eliptical(ke));
    var Z0_o_0 = (30 * Math.PI / Math.sqrt(er)) * (eliptical(ko_prime) / eliptical(ko));
    var Zo_ss = sym_stripline(w, t, h, er);
    var cf_tb = (.0885 * er / Math.PI) * (((2 * b) / (b - t)) * Math.log((2 * b - t) / (b - t)) - (t / (b - t)) * Math.log((Math.pow(b, 2) / Math.pow((b - t), 2)) - 1));
    var cf_0 = (.0885 * er / Math.PI) * 2 * Math.log(2);
    var k_ideal = sech((Math.PI * w) / (2 * b));
    var k_ideal_prime = tanh((Math.PI * w) / (2 * b));
    var Z0_ideal = (no / (4 * Math.sqrt(er))) * (eliptical(k_ideal) / eliptical(k_ideal_prime));
    var temp1 = 1 / (((1 / Zo_ss.zo)) - (cf_tb / cf_0) * ((1 / (Z0_o_0)) - (1 / (Z0_ideal))));
    var temp2 = s / t;
    var zo_odd_1 = 1 / (((1 / Zo_ss.zo)) - (cf_tb / cf_0) * ((1 / (Z0_o_0)) - (1 / (Z0_ideal))));
    var zo_odd_2 = 1 / ((2 / (Z0_o_0)) - (1 / Z0_ideal) - (2 / (no * er)) * (cf_tb - cf_0 - (er / s)));
    var zo_odd = 0;

    if ((s / t) >= (5)) {
        zo_odd = zo_odd_1;
    } else {
        zo_odd = zo_odd_2;
    }


    var zo_even = 1 / ((1 / (Zo_ss.zo)) - (cf_tb / cf_0) * ((1 / (Z0_ideal)) - (1 / (Z0_e_0))));
    var zo_common = zo_even / 2;
    var zo_diff = zo_odd * 2;
    var A1 = 1 / Zo_ss.zo;
    var A2 = cf_tb / cf_0;
    var A3_even = 1 / Z0_ideal;
    var A3_odd = 1 / Z0_o_0;
    var A4_even = 1 / Z0_e_0;
    var A4_odd = 1 / Z0_ideal;
    return {
        zo_odd: zo_odd,
        zo_even: zo_even,
        zo_common: zo_common,
        zo_diff: zo_diff
    };
}

function findk(w, b, s) {

    k = .005;
    min_error = 100;
    real_k = 100;
    loops = 1000;
    step = 1 / loops;
    for (n = 1; n <= loops; n = n + 1) {
        R = Math.sqrt((((k * b) / s) - 1) / ((b / (k * s)) - 1));
        w_b = w / b;
        expression = (1 / Math.PI) * (Math.log((1 + R) / (1 - R)) - (s / b) * Math.log((1 + R / k) / (1 - (R / k))));
        error = Math.abs(w_b - expression);
        if (error < min_error) {
            real_k = k;
            min_error = error;
        }
        k = k + step;
    }

    return real_k
}

function BCS_o(w, b, s, er, k) {
    ZO_o = 293.9 / (Math.sqrt(er)) * (s / b) * (1 / inv_tanh(k));
    return ZO_o;
}

function inv_tanh(x) {
    ans = .5 * (Math.log((1 + x) / (1 - x)));
    return ans;
}

function BCS_e(w, b, s, er, k) {
    k_p = Math.sqrt(1 - Math.pow(k, 2));
    ZO_e = (377 / Math.sqrt(er)) * (eliptical(k_p) / eliptical(k));
    return ZO_e;
}

function eliptical(k) {
    temp = 1
    for (n = 1; n <= 20; n = n + 1) {
        a = 2 * n - 1;
        b = 2 * n;
        fa = bangbang(a);
        fb = bangbang(b);
        temp = temp + Math.pow((fa / fb), 2) * Math.pow(k, b);
    }
    result = Math.PI * temp / 2;
    return (result);
}

function bangbang(x) {
    var result, i, x2;

    result = 1;
    if (x == 0 && x == (-1))
        return (result);

    x2 = x / 2;
    for (i = 0; i < x2; i++) {
        result *= x;
        x -= 2;
    }

    return result;
}

function bc_stripline(f) {
    var t = parseFloat(f.thick.value) * parseFloat(f.thick_unit.options[f.thick_unit.selectedIndex].value);
    var b = parseFloat(f.sub_height.value) * parseFloat(f.sub_height_unit.options[f.sub_height_unit.selectedIndex].value);
    var w = parseFloat(f.width.value) * parseFloat(f.width_unit.options[f.width_unit.selectedIndex].value);
    var s = parseFloat(f.space.value) * parseFloat(f.space_unit.options[f.space_unit.selectedIndex].value);
    var er = parseFloat(f.er.value);
    trace = bc_stripline_calc(w, t, s, b, er)
    f.odd.value = trace.zo_odd.toPrecision(3);
    f.even.value = trace.zo_even.toPrecision(3);
    f.common.value = trace.zo_common.toPrecision(3);
    f.differential.value = trace.zo_diff.toPrecision(3);
}

function bc_stripline_calc(w, t, s, b, er) {
    var k = findk(w, b, s);
    var zo_odd = BCS_o(w, b, s, er, k);
    var zo_even = BCS_e(w, b, s, er, k);
    var zo_common = zo_even / 2;
    var zo_diff = zo_odd * 2;
    return {
        zo_odd: zo_odd,
        zo_even: zo_even,
        zo_common: zo_common,
        zo_diff: zo_diff
    };
}