<?php
$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
session_start();
$_SESSION['com_id']=$slcustom15;
$_SESSION['user_name']=$slusername;


?>

<!DOCTYPE html>  
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Library9.com</title>
<link href="styles.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="shortcut icon" href="../images/favicon.png"/>

<script type="text/javascript" src="../javascript/jquery-1.6.1.min.js"></script>
<!-- for navigation -->
<script type="text/javascript" src="../javascript/script-navigation.js"></script>
<!--[if IE 9 ]>
 <link href="../css/ie.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->

</head>
<body>
<?php include("header.php") ?>
<?php include("sidebar.php") ?>
<?php
// Correct database to connect to
mysql_connect($host,$username,$password);																																						// connect to MySQL with credentials from demo-include.php
@mysql_select_db('zadmin_l9') or die( "Unable to select database");?>

		<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">

			<p>Current Page - <strong>Library / Parts Library</strong></p>

	</div>
</div>


<div class="app_index">



<?php 

 if ( $slcustom1 == 'basic'){
 if ( $slgroupname[0] === 'ladmin' || $slgroupname[0] === 'llib')
                        { ?>
<table border="0" width="100%" cellpadding="2" cellspacing="2" class="index_table">  																<!-- start table with no border, 50 % of screen width, 2 pixels spaces between cell content and cell wall, 2 pixels between cells -->
	
    <tr> 																																										<!-- start table row -->
        <td align="center" style=" padding-bottom:25px;">																																		<!-- start table cell, center cell -->
            <form name="001" action="view1x0.php?TBL=resistor" method="post">   								<!-- start form, open page with Resistors as passed TBL variable, pass (post) data method -->
                <input type="submit" value="RESISTOR" class="res_icon">																				<!-- create submit button with text  on the button as value -->
            </form>																																							<!-- close form -->
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="006" action="view1x0.php?TBL=capacitor" method="post">  								<!-- User chooses Diodes to view/delete/add/edit -->
                <input type="submit" value="CAPACITOR" class="cap_icon">
            </form>
		</td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="010" action="view1x0.php?TBL=misc" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="MISC" class="misc_icon">
            </form>
        </td>				 
    </tr>																																										<!-- end table row -->

    <tr>
        <td align="center" style=" padding-bottom:25px;">
            <form name="011" action="view1x0.php?TBL=connector" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="CONNECTOR" class="con_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="012" action="view1x0.php?TBL=ic" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="IC" class="ic_icon">
            </form>
        </td>	
        <td align="center" style=" padding-bottom:25px;">
            <form name="013" action="view1x0.php?TBL=diode" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="DIODE" class="diode_icon">
            </form>
        </td>			
    </tr>

    <tr>
        <td align="center" style=" padding-bottom:25px;">
            <form name="014" action="view1x0.php?TBL=transistor" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="TRANSISTOR" class="tran_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="015" action="view1x0.php?TBL=oscillator" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="OSCILLATOR" class="osc_icon">
            </form>
        </td>	
        <td align="center" style=" padding-bottom:25px;">
			<form name="all" action="viewall.php" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
			<input type="submit" value="ALL PARTS" class="allp_icon">
			</form>
		</td>			
    </tr>
	
	    <tr>
        <td align="center" style=" padding-bottom:25px;">&nbsp;
			
        </td>
       <td align="center" style=" padding-bottom:25px;">
             <!-- <form name="015" action="admin.php" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <!--<input type="submit" value="ADMIN AREA" class="download_icon">
            </form>-->
        </td>	 
        <td align="center" style=" padding-bottom:25px;">&nbsp;
			
		</td>			
    </tr>



</table>

<?php } else if ( $slgroupname[0] === 'luser'){?>

<table border="0" width="100%" cellpadding="2" cellspacing="2" class="index_table">  																<!-- start table with no border, 50 % of screen width, 2 pixels spaces between cell content and cell wall, 2 pixels between cells -->
    <tr> 																																										<!-- start table row -->
        <td align="center" style=" padding-bottom:25px;">																																		<!-- start table cell, center cell -->
            <form name="001" action="view1x0.php?TBL=resistor" method="post">   								<!-- start form, open page with Resistors as passed TBL variable, pass (post) data method -->
                <input type="submit" value="RESISTOR" class="res_icon">																				<!-- create submit button with text  on the button as value -->
            </form>																																							<!-- close form -->
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="006" action="view1x0.php?TBL=capacitor" method="post">  								<!-- User chooses Diodes to view/delete/add/edit -->
                <input type="submit" value="CAPACITOR" class="cap_icon">
            </form>
		</td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="010" action="view1x0.php?TBL=misc" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="MISC" class="misc_icon">
            </form>
        </td>				
    </tr>																																										<!-- end table row -->

    <tr>
        <td align="center" style=" padding-bottom:25px;">
            <form name="011" action="view1x0.php?TBL=connector" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="CONNECTOR" class="con_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="012" action="view1x0.php?TBL=ic" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="IC" class="ic_icon">
            </form>
        </td>	
        <td align="center" style=" padding-bottom:25px;">
            <form name="013" action="view1x0.php?TBL=diode" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="DIODE" class="diode_icon">
            </form>
        </td>			
    </tr>

    <tr>
        <td align="center" style=" padding-bottom:25px;">
            <form name="014" action="view1x0.php?TBL=transistor" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="TRANSISTOR" class="tran_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="015" action="view1x0.php?TBL=oscillator" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="OSCILLATOR" class="osc_icon">
            </form>
        </td>	
        <td align="center" style=" padding-bottom:25px;">
			<form name="all" action="viewall.php" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
			<input type="submit" value="ALL PARTS" class="allp_icon">
			</form>
		</td>			
    </tr>
	
	    <tr>
        <td align="center" style=" padding-bottom:25px;">&nbsp;
			
        </td>
       
        <td align="center" style=" padding-bottom:25px;">&nbsp;
			
		</td>			
    </tr>



</table>


<?php }
else if ( $slgroupname[0] === 'leditor')
                        { ?>
<table border="0" width="100%" cellpadding="2" cellspacing="2" class="index_table">  																<!-- start table with no border, 50 % of screen width, 2 pixels spaces between cell content and cell wall, 2 pixels between cells -->
	
    <tr> 																																										<!-- start table row -->
        <td align="center" style=" padding-bottom:25px;">																																		<!-- start table cell, center cell -->
            <form name="001" action="view1x0.php?TBL=resistor" method="post">   								<!-- start form, open page with Resistors as passed TBL variable, pass (post) data method -->
                <input type="submit" value="RESISTOR" class="res_icon">																				<!-- create submit button with text  on the button as value -->
            </form>																																							<!-- close form -->
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="006" action="view1x0.php?TBL=capacitor" method="post">  								<!-- User chooses Diodes to view/delete/add/edit -->
                <input type="submit" value="CAPACITOR" class="cap_icon">
            </form>
		</td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="010" action="view1x0.php?TBL=misc" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="MISC" class="misc_icon">
            </form>
        </td>				 
    </tr>																																										<!-- end table row -->

    <tr>
        <td align="center" style=" padding-bottom:25px;">
            <form name="011" action="view1x0.php?TBL=connector" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="CONNECTOR" class="con_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="012" action="view1x0.php?TBL=ic" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="IC" class="ic_icon">
            </form>
        </td>	
        <td align="center" style=" padding-bottom:25px;">
            <form name="013" action="view1x0.php?TBL=diode" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="DIODE" class="diode_icon">
            </form>
        </td>			
    </tr>

    <tr>
        <td align="center" style=" padding-bottom:25px;">
            <form name="014" action="view1x0.php?TBL=transistor" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="TRANSISTOR" class="tran_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="015" action="view1x0.php?TBL=oscillator" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="OSCILLATOR" class="osc_icon">
            </form>
        </td>	
        <td align="center" style=" padding-bottom:25px;">
			<form name="all" action="viewall.php" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
			<input type="submit" value="ALL PARTS" class="allp_icon">
			</form>
		</td>			
    </tr>
	
	    <tr>
        <td align="center" style=" padding-bottom:25px;">&nbsp;
			
        </td>
       <td align="center" style=" padding-bottom:25px;">
             <!-- <form name="015" action="admin.php" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <!--<input type="submit" value="ADMIN AREA" class="download_icon">
            </form>-->
        </td>	 
        <td align="center" style=" padding-bottom:25px;">&nbsp;
			
		</td>			
    </tr>



</table>

<?php } }

 if ( $slcustom1 == 'advance'){
 if ( $slgroupname[0] === 'ladmin' || $slgroupname[0] === 'llib')
                        { ?>
<center><table border="0" width="80%" cellpadding="2" cellspacing="2" class="inner_table">  																
<!-- start table with no border, 50 % of screen width, 2 pixels spaces between cell content and cell wall, 2 pixels between cells -->
	                           
							    <!-------------------------------done by nisha--------------------------->
    <tr>
       
		<?php 
		include("include.php"); 
		mysql_connect($host, $username, $password)or die("cannot connect");
        @mysql_select_db($slcustom15) or die( "Unable to select database");
		
          $result1 = mysql_query("SELECT cat_name FROM categories")or die(mysql_error()."update failed");
		  $i=0;

        while ($row=mysql_fetch_array($result1)) 																																									
		{ 
         
		$name1=$row["cat_name"]; 
		$name = str_replace("_"," ","$name1");
		
		if($i == 0){
        echo"<TR>";
    } ?>																																																																														
		 																												
	    <td align="center" style=" padding-bottom:25px;">
		<form name="015" action="view_custom.php?TBL=<?php echo $name1;?>" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="<?php echo $name;?>" class="l9_icon">
			  </form></td><?php   $i++;
			  if($i == 3)
    {
        $i = 0;
        echo"</tr>";
    }}?>  																																		<!-- end table row --
                              <!---------------------------------------------------------------------------->
    
	
	</table>
<center>
<br>
<center><table border="0" width="53%" cellpadding="2" cellspacing="2" class="inner_table">  
	    <tr>
		
        <td align="center" style=" padding-bottom:25px;">
		<form name="015" action="viewall.php" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="ALL PARTS" class="allp_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <!--<form name="015" action="admin.php" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <!--<input type="submit" value="ADMIN AREA" class="download_icon">
            </form> -->
        </td>	

    </tr>



</table>
<center>

<?php }


 else if ( $slgroupname[0] === 'luser'){?>


<table border="0" width="100%" cellpadding="2" cellspacing="2" class="inner_table">  																<!-- start table with no border, 50 % of screen width, 2 pixels spaces between cell content and cell wall, 2 pixels between cells -->
																																									<!-- end table row -->

         <tr>
       
		<?php 
		include("include.php"); 
		mysql_connect($host, $username, $password)or die("cannot connect");
        @mysql_select_db($slcustom15) or die( "Unable to select database");
          $result1 = mysql_query("SELECT cat_name FROM categories")or die(mysql_error()."update failed");
		  $i=0;

        while ($row=mysql_fetch_array($result1)) 																																									
		{ 
         
		$name1=$row["cat_name"]; 
		$name = str_replace('_', ' ', $name1); 
		if($i == 0){
        echo"<TR>";
    } ?>																																																																														
		 																												
	    <td align="center" style=" padding-bottom:25px;">
		<form name="015" action="view_custom.php?TBL=<?php echo $name1;?>" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="<?php echo $name;?>" class="l9_icon">
			  </form></td><?php   $i++;
			  if($i == 3)
    {
        $i = 0;
        echo"</tr>";
    }}?>  		
 
		</table>
<table border="0" width="53%" cellpadding="2" cellspacing="2" class="inner_table">  
	    <tr>
        <td align="center" style=" padding-bottom:25px;">
		<form name="015" action="viewall.php" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="ALL PARTS" class="download_icon">
            </form>
        </td>
       
        <td align="center" style=" padding-bottom:25px;">&nbsp;
			
		</td>			
    </tr>



</table>






<?php }
if ( $slgroupname[0] === 'leditor')
                        { ?>
<center><table border="0" width="80%" cellpadding="2" cellspacing="2" class="inner_table">  																
<!-- start table with no border, 50 % of screen width, 2 pixels spaces between cell content and cell wall, 2 pixels between cells -->
	                           
							    <!-------------------------------done by nisha--------------------------->
    <tr>
       
		<?php 
		include("include.php"); 
		mysql_connect($host, $username, $password)or die("cannot connect");
        @mysql_select_db($slcustom15) or die( "Unable to select database");
		
          $result1 = mysql_query("SELECT cat_name FROM categories")or die(mysql_error()."update failed");
		  $i=0;

        while ($row=mysql_fetch_array($result1)) 																																									
		{ 
         
		$name1=$row["cat_name"]; 
		$name = str_replace("_"," ","$name1");
		
		if($i == 0){
        echo"<TR>";
    } ?>																																																																														
		 																												
	    <td align="center" style=" padding-bottom:25px;">
		<form name="015" action="view_custom.php?TBL=<?php echo $name1;?>" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="<?php echo $name;?>" class="l9_icon">
			  </form></td><?php   $i++;
			  if($i == 3)
    {
        $i = 0;
        echo"</tr>";
    }}?>  																																		<!-- end table row --
                              <!---------------------------------------------------------------------------->
    
	
	</table>
<center>
<br>
<center><table border="0" width="53%" cellpadding="2" cellspacing="2" class="inner_table">  
	    <tr>
		
        <td align="center" style=" padding-bottom:25px;">
		<form name="015" action="viewall.php" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="ALL PARTS" class="allp_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <!--<form name="015" action="admin.php" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <!--<input type="submit" value="ADMIN AREA" class="download_icon">
            </form> -->
        </td>	

    </tr>



</table>
<center>

<?php }}?>																																												  <!-- end table -->
</div>
</div>
</div>

