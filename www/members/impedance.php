<?php 

$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>
 <link type="text/css" rel="stylesheet" href="css/easy-responsive-tabs.css" />
		<div class="side_work">	
			<div class="working_area">
	<div class="status_panel">
		<div class="status_sec">
	
				<div class="statusbar"><p>Current Page - <strong>Calculators / Impedance Control</strong></p></div>
				<div class="returnstat"><a href="calculator.php" class="menu_click">Return</a></div>
	
		</div>
	</div>
<div class="app_calculator">

  <div class="calc_buttons">

  </div> 
            
<div id="body-container1">
                  <div class="glossary-container impedance_calc">
   					 <ul class="firstUL1">

						<li id="f">Microstrip</li>
						<li id="g">Edge coupled Microstrip</li>
						<li id="h">Embedded Microstrip</li>
						<li id="i">Symmetric stripline</li>	
						<li id="j">Asymmetric stripline</li>
						<li id="k">Wire Microstrip</li>
						<li id="l">Wire stripline</li>
						<li id="m">Edge coupled stripline</li>
						<li id="n">Broadside coupled stripline</li>	
						
					</ul>
				 </div>	
  <div class="content-container">

    
	<div id="content-for-f" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">
			<div id="microstrip-impedance-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calcs.js?calc=one';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script>
			</div>
				<div class="calc_right">
					<h5>Microstrip Impedance</h5>
					<p>A Microstrip is constructed with a flat conductor suspended over a ground plane. The conductor and ground plane are separated by a dielectric. The surface Microstrip transmission line also has free space (air) as the dielectric above the conductor. This structure can be built in materials other than printed circuit boards, but will always consist of a conductor separated from a ground plane by some dielectric material.</p>
					<div class="calc_foot"><p class="return-to-top">Return to Top</p></div>					
				</div>
			
			</div>			
     
     
    </div>	
	<div id="content-for-g" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">	

				<div id="edge-coupled-microstrip-impedance-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calcs1.js?calc=seven';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script>
			</div>
				<div class="calc_right">
					<h5>Edge coupled Microstrip Impedance</h5>
					<p>An edge couple differential Microstrip transmission line is constructed with two traces referenced to the same reference plane. There is a dielectric material between them. There is also some coupling between the lines. This coupling is one of the features of differential traces. Usually it is good practice to match differential trace length and to keep the distances between the traces consistent. Also avoid placing vias and other structures between these traces.</p>
					<div class="calc_foot"><p class="return-to-top">Return to Top</p></div>					
				</div>
			
			</div>				
						
     

    </div>
	<div id="content-for-h" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">		

				<div id="embedded-microstrip-impedance-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calcs2.js?calc=two';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script>
			</div>	
				<div class="calc_right">
					<h5>Embedded Microstrip Impedance</h5>
					<p>An embedded Microstrip is constructed with a flat conductor suspended over a ground plane. The conductor and ground plane are separated by a dielectric. There is also a layer of dielectric material above the conductor. One case of an embedded Microstrip transmission line is a Microstrip trace with solder mask.</p>
					<div class="calc_foot"><p class="return-to-top">Return to Top</p></div>					
				</div>
			
			</div>				
										
   
   </div>	
	<div id="content-for-i" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">	
			<div id="symmetric-stripline-impedance-calc-container"></div>
							<script>
								(function() {
									var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
									calc.src = 'js/calcs3.js?calc=three';
									(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
								})();
							</script>
			</div>
				<div class="calc_right">
					<h5>Symmetric Stripline Impedance</h5>
					<p>A strip-line is constructed with a flat conductor suspended between two ground planes. The conductor and ground planes are separated by a dielectric. One advantage of the strip-line is that there is an improve isolation between adjacent traces when compared with the microstrip.</p>
					<div class="calc_foot"><p class="return-to-top">Return to Top</p></div>					
				</div>
			
			</div>				
						

    </div>	
	<div id="content-for-j" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">		
	
				<div id="asymmetric-stripline-impedance-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calcs4.js?calc=four';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script>
			</div>	
				<div class="calc_right">
					<h5>Asymmetric Stripline Impedance</h5>
					<p>A strip-line is constructed with a flat conductor suspended between two ground planes. The conductor and ground planes are separated by a dielectric. The distance between the conductor and the planes is not the same for both reference planes. This structure will most likely be manufactured with the printed circuit board process.</p>
					<div class="calc_foot"><p class="return-to-top">Return to Top</p></div>					
				</div>
			
			</div>					

    </div>	
	<div id="content-for-k" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">		
		
				<div id="wire-microstrip-impedance-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calcs5.js?calc=five';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script>
			</div>	
				<div class="calc_right">
					<h5>Wire Microstrip Impedance</h5>
					<p>A wire microstrip is constructed with a round conductor suspended over a ground plane. The conductor and ground plane are separated with a dielectric. As with the standard microstrip trace, an effective dielectric constant is calculated because air is on one side of the trace where another dielectric is between the wire and the ground plane.</p>
					<div class="calc_foot"><p class="return-to-top">Return to Top</p></div>					
				</div>
			
			</div>						
    </div>	
	<div id="content-for-l" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">	

				<div id="wire-stripline-impedance-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calcs6.js?calc=six';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script>
				</div>
				<div class="calc_right">
					<h5>Wire Stripline Impedance</h5>
					<p>A wire stripline is constructed with a round conductor suspended between two ground planes. The conductor and ground planes are separated with a dielectric. This calculator assumes the distance between the two reference planes to be an equal distance.</p>
					<div class="calc_foot"><p class="return-to-top">Return to Top</p></div>					
				</div>
			
			</div>					

    </div>				
	<div id="content-for-m" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">		
				<div id="edge-coupled-stripline-impedance-calc-container"></div>
					<script>
						(function() {
							var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
							calc.src = 'js/calcs7.js?calc=eight';
							(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
						})();
					</script>
			</div>		
				<div class="calc_right">
					<h5>Edge Coupled Stripline Impedance</h5>
					<p>An edge couple differential symmetric stripline transmission line is constructed with two traces referenced to the same reference planes above and below the traces. There is a dielectric material between them. There is also some coupling between the lines. This coupling is one of the features of differential traces. Usually it is good practice to match differential trace length and to keep the distances between the traces consistent. Also avoid placing vias and other structures between these traces.</p>
					<div class="calc_foot"><p class="return-to-top">Return to Top</p></div>					
				</div>
			
			</div>				
    </div>		
	<div id="content-for-n" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">		
	
				<div id="broadside-coupled-stripline-impedance-calc-container"></div>
					<script>
						(function() {
							var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
							calc.src = 'js/calcs8.js?calc=nine';
							(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
						})();
					</script>
			</div>		
				<div class="calc_right">
					<h5>Broadside Coupled Stripline Impedance</h5>
					<p>Formulas are given for the even- and odd-mode characteristic impedances of shielded coupled strip-transmission-line configurations that are especially useful when close coupling is desired. The cross sections considered are thin broadside-coupled strips either parallel or perpendicular to the ground planes. The derivations are outlined, with particular attention given to the underlying assumption that restricts the use of the formulas to cases of close coupling.</p>
					<div class="calc_foot"><p class="return-to-top">Return to Top</p></div>					
				</div>
			
			</div>						
    </div>				
		
					 
    
      

				
			
        </div>
  
</div>

    </div>
</div>
</div>
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL1 li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL1 li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#e9e9e9'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL1 li").each(function() { 
			$(".firstUL1 li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#e9e9e9'}); //set the background color to light-blue to that div
	 });
});
</script>