<?php
$groupswithaccess="ladmin,luser,leditor";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php';
include("sidebar.php");
$cat=$_GET['cat']; 
?>

		<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">

				<div class="statusbar"><p>Current Page - <strong>Add Category</strong></p></div>
				<div class="returnstat"><a href="admin.php" class="menu_click">Return</a></div>

	</div>
</div>


<div class="app_content">		
<div class="contact_left">

			<center><p align="justify" style="font-family:Arial, Helvetica, sans-serif;line-height:25px">Category <b><?php echo $cat;?> </b>has been added successfully. The following are the default fields.
            If you want to add some more fields, you can add <br> from custom fields in Admin Area.</p></center>
           <div class="script_layout">
   <center>  <table width="75%" cellpadding="5%" >
             <tr><td>1. Part Number</td><td>10. CPN</td></tr>
             <tr><td>2. Category</td><td>11. Comment</td></tr>
             <tr><td>3. Component Kind</td><td>12. Component Type</td></tr>
             <tr><td>4. Manufacturer</td><td>13. Footprint Library Reference</td></tr>
             <tr><td>5. Packaging</td><td>14. Manufacturer Part Number</td></tr>
             <tr><td>6. Signal Integrity</td><td>15. Pin Count</td></tr>
             <tr><td>7. Simulation</td><td>16. Supplier</td></tr>
             <tr><td>8. Footprint Path </td><td>17. Footprint Ref</td></tr>
             <tr><td>9. Component Link URL</td><td></tr>
            </table></center>
            </div>
				<div class="clear"></div>
		         </div>
	</div>
	    
    
</div>
</div>
</div>
</div>

 
 
 
