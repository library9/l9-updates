

<?php
/*
* exif.php
* This script gets meta headers from image files.
*
* (For use with command line)
* Usage Usage: php exif.php <image>
* Example: php exif.php beach.jpg
*/
function show_help()
{
echo "Usage: php exif.php <image>\nExample: php exif.php beach.jpg\nSupported formats: jpeg/jpg and tiff\n\n";
}
function show_data($img)
{
$exif = exif_read_data($img, 0, true);
echo $exif === false ? "\nNo header data\n" : "\nFound image headers\n";
foreach ($exif as $key => $value) {
echo "[".$key."] => ".$value."\n";
if (is_array($value)) {
foreach ($value as $k => $v) {
echo "\t[".$k."] => ".$v."\n";
if (is_array($v)) {
foreach ($v as $a => $b) {
echo "\t\t[".$a."] => ".$b."\n";
}
}
}
}
}
echo "\n";
}
if ($argv[1] == "" || $argv[1] == "-h") {
echo "\nShowing Help...\n";
echo "**".$argv[1];
show_help();
}
else if (
strpos($argv[1], ".jpg") ||
strpos($argv[1], ".JPG") ||
strpos($argv[1], ".jpeg") ||
strpos($argv[1], ".JPEG") ||
strpos($argv[1], ".tif") ||
strpos($argv[1], ".TIF") ||
strpos($argv[1], ".tiff") ||
strpos($argv[1], ".TIFF")
) {
$img_path = $argv[1];
show_data($img_path);
}
else {
die ("\nUnrecognized argument or unsupported file format. Use -h for help.\n");
}
show_data('R0010819.JPG');
?> 