<?php
$groupswithaccess="ladmin,luser";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");



//// * Example PHP implementation used for the index.html example
// 
//
//// DataTables PHP library
include( "DataTables-1.10.0/extensions/Editor-1.3.0/php/DataTables.php" );
//
//// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Join,
    DataTables\Editor\Validate;
//
//// Build our Editor instance and process the data coming from _POST

Editor::inst( $db,'part_upload' )
    ->fields(
	  
         Field::inst( 'part_upload.date' ),
		Field::inst( 'part_upload.filename' )
		
    )
	

    ->process( $_POST )
    ->json();
?>