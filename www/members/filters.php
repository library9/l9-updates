<?php 

$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>
 <link type="text/css" rel="stylesheet" href="css/easy-responsive-tabs.css" />
		<div class="side_work">	
			<div class="working_area">
	<div class="status_panel">
		<div class="status_sec">
	
				<div class="statusbar"><p>Current Page - <strong>Calculators / Filters</strong></p></div>
				<div class="returnstat"><a href="calculator.php" class="menu_click">Return</a></div>
	
		</div>
	</div>
<div class="app_calculator">

  <div class="calc_buttons">
  	
  </div> 
            
<div id="body-container1">
                  <div class="glossary-container fil_calc">
   					 <ul class="firstUL1">
						<li id="a" class="selected">HF Filter </li>
						<li id="b">Butterworth Pi LC Low Pass</li>
						<li id="c">Butterworth Pi LC High Pass</li>
						<li id="d">Chebyshev Pi LC Low Pass</li>
						<li id="e">Chebyshev Pi LC High Pass</li>
						<li id="f">Butterworth Tee LC Low Pass</li>												
						<li id="g">Butterworth Tee LC High Pass</li>	
						<li id="h">Chebyshev Tee LC Low Pass</li>
						<li id="i">Chebyshev Tee LC High Pass</li>
						<li id="j">Equal Component B/worth Low Pass</li>
						<li id="k">Equal Component B/worth High Pass</li>
						<li id="l">Sallen-Key Butterworth Low Pass</li>												
						<li id="m">Sallen-Key Butterworth High Pass</li>							
						
					</ul>
				 </div>	
  <div class="content-container">

    <div id="content-for-a" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">

							
							
						
							 
					
					 <!---form1  HF Filter---> 
					 <FORM id='frm1' > 
					 	<center>
							 <table align="center" class="filt">
							<tr><th colspan="2" align="center">Enter Values:</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>
							<tr> <td colspan="2" align="center"><img src="images/hf filter.jpg" border="0"></td></tr>
							<tr><td colspan="2" align="center"><p><font size="2" face="verdana"
							color="#1f6fa2"><b>R<sub>in</sub> = Input Resistance <br> 
							R<sub>out</sub> = Output Resistance</b></font></p></td></tr>
							<tr>		  <td align="right"><font size="2" face="verdana"
							color="#1f6fa2"><b>Frequency: </b></font> </td>
										 <td align="left"> <input type="text" name="freq11"  size="7" maxlength="15">
								  <font size="2" face="verdana"
							color="#1f6fa2"><b>MHertz</b></font> </td></tr>
								  
							<tr>
							<tr>	 
							 <td align="right"><font size="2" face="verdana"
							color="#1f6fa2"><b>Input Resistance: </b></font> </td>
							 <td align="left"><input type="text" name="Rin11" size="7" maxlength="15">
								   <font size="2" face="verdana"
							color="#1f6fa2"><b> Ohms</b></font> </td></tr>
								  
							<tr>
									  <td align="right"><font size="2" face="verdana"
							color="#1f6fa2"><b>Output Resistance: </b></font> </td>
										 <td align="left"> <input type="text" name="Rout11" size="7" maxlength="15">
								 <font size="2" face="verdana"
							color="#1f6fa2"><b> Ohms</b></font> </td></tr>
								 
								 <tr>
									  <td align="right"><font size="2" face="verdana"
							color="#1f6fa2"><b> Q - Value:</b></font> </td>
										  <td align="left"><input type="text"  name="Qf11" size="7" maxlength="15">
								</td></tr>

								  
							<tr>
								<td align="right"><input type="Button" value="Calculate" onClick="calc(this.form)"  class="calculate_cal"></td>
								<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
						  
							  </tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>								  
							<tr><th colspan="2" align="center">Result:</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>
							
									<tr>
									  <td align="center"><font size="2" face="verdana"
							color="#1f6fa2"><b>Inductance: 
									  
								 </b></font></td>
								 <td align="center"><font size="2" face="verdana"
							color="#1f6fa2"><b>Capacitance: 
									  
								 </b></font> </td></tr>
							
					  <tr>
									  <td align="center"><font size="2" face="verdana"
							color="#1f6fa2"><b> L<sub>1</sub>: 
										<input type="text"  name="La" size="7" maxlength="15" readonly="readonly">
								 uH</b></font> </td>
								 
								 <td align="center"><font size="2" face="verdana"
							color="#1f6fa2"><b>C<sub>1</sub>: 
										<input type="text"  name="Ca" size="7" maxlength="15" readonly="readonly">
								 pF</b></font> </td></tr>
								 
								 <tr>
									  <td align="center"><font size="2" face="verdana"
							color="#1f6fa2"><b> L<sub>2</sub>: 
										<input type="text"  name="Lb" size="7" maxlength="15" readonly="readonly">
								 uH</b></font> </td>
								 
								 <td align="center"><font size="2" face="verdana"
							color="#1f6fa2"><b>C<sub>2</sub>:
										<input type="text"  name="Cb" size="7" maxlength="15" readonly="readonly">
								 pF</b></font> </td></tr>
								 
								 <tr><td></td>
									  <td align="center"><font size="2" face="verdana"
							color="#1f6fa2"><b>C<sub>3</sub>:
										<input type="text"  name="Cc" size="7" maxlength="15" readonly="readonly">
								 pF</b></font> </td></tr>
					<tr><td colspan="2" style="height:15px;"></td></tr>								 
							</table>
							</center>
						  </form>			
													

			</div>
			<div class="calc_right">
				<h5>HF Filter</h5>
				<p>Reactance arises from the presence of inductance and capacitance within a circuit. It is generally denoted by symbol X and its SI unit is Ohm.</p>
				<p>If X>0, the reactance is said to be inductive.</p>
				<p>If X<0, the reactance is said to be capacitive.</p>
				<p>Inductive reactance XL is proportional to the frequency and the inductance.</p>
				<p>Capacitive reactance XC is inversely proportional to the frequency and the capacitance. </p>
										
				 <div class="calc_foot"><p class="return-to-top">Return to Top</p></div>
			</div>
			
		</div>	
     
     
    </div>
	<div id="content-for-b" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

	 <!------form2 Butterworth Pi LC Low--->
<form METHOD="POST" > 
	<center>
        <table align="center" class="filt">
		<tr><th colspan="2" align="center">Enter Values:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		<tr><td colspan="2" align="center"><img src="images/bw pi low.jpg" border="0"></td>
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Cutoff Frequency: </b></font> </td>
                   <td align="left"> <input type="text" name="c" size="7" maxlength="15">
              <SELECT name=FM >
			<OPTION value=1>Hz
			<OPTION value=1000>KHz
			<OPTION value=1000000 selected>MHz
			<OPTION value=1000000000>GHz
			</OPTION></SELECT></td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Impedance Z<sub>0</sub>:</b></font> </td>
                  <td align="left">  <input type="text" name="z" size="7" maxlength="15">
              <SELECT name=ZM >
				<OPTION value=1 selected>ohm
				<OPTION value=1000>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT></td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Number of Components:</b></font> </td> 
                 <td align="left">   <input type="text" name="n" size="7" maxlength="15">
          <font size="2" face="verdana"
        color="#1f6fa2"><b>  (1-11)</b></font> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" NAME="name" VALUE="Calculate" onClick="solve1(this.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>

			  
		<tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>		
		<tr><th colspan="2" align="center">Results:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		</td></tr>
		
  		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Inductance:</b></font> </td>
		
	<td align="center">	<font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance: 
                   
            </b></font></td></tr>
		
		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=LM>

				<OPTION value=0.000000001 selected>nH
				<OPTION value=0.000001>µH
				<OPTION value=0.001>mH
				<OPTION value=1>H
				</OPTION></SELECT></b></font>  </td>
				
				<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=CM >
				<OPTION value=0.000000000001 selected>pF
				<OPTION value=0.000000001>nF
				<OPTION value=0.000001>µF
				<OPTION value=0.001>mF
				<OPTION value=1>F
				</OPTION></SELECT></b></font> </td></tr>
		
		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>1</sub>: 
                    <input type="text" name="y2" size="12" maxlength="15" readonly="readonly">
            </b></font>  </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>1</sub>: 
                    <input type="text" name="x1" size="12" maxlength="15" readonly="readonly">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>2</sub>: 
                    <input type="text" name="y4" size="12" maxlength="15" readonly="readonly">
            </b></font>  </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>2</sub>: 
                    <input type="text" name="x3" size="12" maxlength="15" readonly="readonly">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>3</sub>: 
                    <input type="text" name="y6" size="12" maxlength="15" readonly="readonly">
            </b></font>  </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>3</sub>: 
                    <input type="text" name="x5" size="12" maxlength="15" readonly="readonly">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>4</sub>: 
                    <input type="text" name="y8" size="12" maxlength="15" readonly="readonly">
            </b></font>  </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>4</sub>: 
                    <input type="text" name="x7" size="12" maxlength="15" readonly="readonly">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>5</sub>: 
                    <input type="text" name="y10" size="12" maxlength="15" readonly="readonly">
            </b></font>  </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>5</sub>: 
                    <input type="text" name="x9" size="12" maxlength="15" readonly="readonly">
            </b></font> </td></tr>
			
			<tr><td></td>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>6</sub>: 
                    <input type="text" name="x11" size="12" maxlength="15" readonly="readonly">
            </b></font> </td></tr>
				<tr><td colspan="2" style="height:15px;"></td></tr>
				
			</table>
			</center>
			</form>			
				
		
		</div>
		<div class="calc_right">
		<h5>Butterworth Pi LC Low Pass Filter</h5>
		<p>The Butterworth type filter was first described by the British engineer Stephen Butterworth.A PI filter is a filter that has a series element and two
parallel elements connected in the shape of the Greek letter PI.Butterworth filter are characterized by a constant gain (flat response) across the midband of the circuit and a 20 dB per decade roll-off rate for each pole contained in the circuit.A Low pass filter is a filter that passes low-frequency signals but attenuates signals with frequencies higher than the cutoff frequency.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
<div id="content-for-c" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

	 <!------form3--->
<form  METHOD="POST"> 
		<center>
         <table align="center" class="filt">
		<tr><th colspan="2" align="center">Enter Values:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		<tr><td colspan="2" align="center"><img src="images/bw pi high.jpg"  border="0"></td>
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Cutoff Frequency: </b></font> </td>
                   <td align="left">  <input type="text" name="c" size="7" maxlength="15">
              <SELECT name=FM >
			<OPTION value=1>Hz
			<OPTION value=1000>KHz
			<OPTION value=1000000 selected>MHz
			<OPTION value=1000000000>GHz
			</OPTION></SELECT></td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Impedance Z<sub>0</sub>:</b></font> </td>
                   <td align="left">   <input type="text" name="z" size="7" maxlength="15">
              <SELECT name=ZM >
				<OPTION value=1 selected>ohm
				<OPTION value=1000>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Number of Components: </b></font> </td>
                 <td align="left">     <input type="text" name="n" size="7" maxlength="15">
           <font size="2" face="verdana"
        color="#1f6fa2"><b> (1-11)</b></font> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" NAME="name" VALUE="Calculate" onClick="solve2(this.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>		  
		<tr><th colspan="2" align="center">Result</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		
		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance:  </b></font></td>
                   
           <td align="center">  <font size="2" face="verdana"
        color="#1f6fa2"><b>Inductance:</b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=CM >
				<OPTION value=0.000000000001 selected>pF
				<OPTION value=0.000000001>nF
				<OPTION value=0.000001>µF
				<OPTION value=0.001>mF
				<OPTION value=1>F
				</OPTION></SELECT> </b></font></td>
				
			 <td align="center"> 	<font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=LM>

				<OPTION value=0.000000001 selected>nH
				<OPTION value=0.000001>µH
				<OPTION value=0.001>mH
				<OPTION value=1>H
				</OPTION></SELECT></b></font></td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>1</sub>: 
                    <input type="text" name="s2" size="12" maxlength="15">
            </b></font> </td>
			
			 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>1</sub>: 
                    <input type="text" name="t1" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>2</sub>: 
                    <input type="text" name="s4" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>2</sub>: 
                    <input type="text" name="t3" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>3</sub>: 
                    <input type="text" name="s6" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>3</sub>: 
                    <input type="text" name="t5" size="12" maxlength="15">
            </b></font></td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>4</sub>: 
                    <input type="text" name="s8" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>4</sub>: 
                    <input type="text" name="t7" size="12" maxlength="15">
            </b></font></td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>5</sub>: 
                    <input type="text" name="s10" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>5</sub>: 
                    <input type="text" name="t9" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr><td></td>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>6</sub>: 
                    <input type="text" name="t11" size="12" maxlength="15">
            </b></font> </td></tr>
			
		<tr><td colspan="2" style="height:15px;"></td></tr>
		</table>
		</center>
		</form>		
					
				
		
		</div>
		<div class="calc_right">
		<h5>Butterworth Pi LC High Pass Filter</h5>
	 	<p>The Butterworth type filter was first described by the British engineer Stephen Butterworth.API filter is a filter that has a series element and two parallel elements connected in the shape of the Greek letter PI.Butterworth filter are characterized by a constant gain (flat response) across the midband of the circuit and a 20 dB per decade roll-off rate for each pole contained in the circuit.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
	<div id="content-for-d" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

	 <!------form4--->
<form  METHOD="POST"> 
			<center>
         <table align="center" class="filt">
		<tr><th colspan="2" align="center">Enter Values:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		<tr><td colspan="2" align="center"><img src="images/bw pi low1.jpg" border="0"></td>
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Cutoff Frequency:</b></font> </td> 
                  <td align="left">  <input type="text" name="c" size="7" maxlength="15">
              <SELECT name=FM >
			<OPTION value=1>Hz
			<OPTION value=1000>KHz
			<OPTION value=1000000 selected>MHz
			<OPTION value=1000000000>GHz
			</OPTION></SELECT> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Impedance Z<sub>0</sub>:</b></font> </td>
                 <td align="left">   <input type="text" name="z" size="7" maxlength="15">
              <SELECT name=ZM >
				<OPTION value=1 selected>ohm
				<OPTION value=1000>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT> </td></tr>
				
			<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Frequency Response Ripple: </b></font> </td>
                   <td align="left"> <input type="text" name="r" size="7" maxlength="15">
            <font size="2" face="verdana"
        color="#1f6fa2"><b>db</b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Number of Components:</b></font> </td> 
                   <td align="left"> <input type="text" name="n" size="7" maxlength="15">
             <font size="2" face="verdana"
        color="#1f6fa2"><b>(1-11)</b></font> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" NAME="name" VALUE="Calculate" onClick="solve3(this.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal" /></td>   
          </tr>

			  
		<tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		<tr><th colspan="2" align="center">Result</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		</td></tr>
		
  		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Inductance:</b></font> </td>
		
		<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance: 
        </b></font>
		</td></tr>
		
		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=LM>

				<OPTION value=0.000000001 selected>nH
				<OPTION value=0.000001>µH
				<OPTION value=0.001>mH
				<OPTION value=1>H
				</OPTION></SELECT></b></font> </td>
				
				<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=CM >
				<OPTION value=0.000000000001 selected>pF
				<OPTION value=0.000000001>nF
				<OPTION value=0.000001>µF
				<OPTION value=0.001>mF
				<OPTION value=1>F
				</OPTION></SELECT></b></font> </td></tr>
		
		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>1</sub>: 
                    <input type="text" name="y2" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>1</sub>: 
                    <input type="text" name="x1" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>2</sub>: 
                    <input type="text" name="y4" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>2</sub>: 
                    <input type="text" name="x3" size="12" maxlength="15">
            </b></font></td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>3</sub>: 
                    <input type="text" name="y6" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>3</sub>: 
                    <input type="text" name="x5" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>4</sub>: 
                    <input type="text" name="y8" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>4</sub>: 
                    <input type="text" name="x7" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>5</sub>: 
                    <input type="text" name="y10" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>5</sub>: 
                    <input type="text" name="x9" size="12" maxlength="15">
            </b></font></td></tr>
			
			<tr><td></td>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>6</sub>: 
                    <input type="text" name="x11" size="12" maxlength="15">
            </b></font> </td></tr>
	<tr><td colspan="2" style="height:15px;"></td></tr>				
			</table>
			</center>
			
			</form>
						
		
		</div>
		<div class="calc_right">
		<h5>Chebyshev Pi LC Low Pass Filter</h5>
		<p>The Chebyshev type filter is named in honor of Pafnuty Chebyshev because their mathematical characteristics are derived from Chebyshev polynomials.
A PI filter is a filter that has a series element and two parallel elements connected in the shape of the Greek letter PI.A Low pass filter is a filter that passes low-frequency signals but attenuates signals with frequencies higher than the cutoff frequency. </p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
<div id="content-for-e" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

	 <!------form5--->
<form   METHOD="POST"> 
		<center>
         <table align="center" class="filt">
		<tr><th colspan="2" align="center">Enter Values:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		<tr><td colspan="2" align="center"><img src="images/bw pi high1.jpg" border="0"></td>
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Cutoff Frequency: </b></font> </td>
                     <td align="left"><input type="text" name="c" size="7" maxlength="15">
              <SELECT name=FM >
			<OPTION value=1>Hz
			<OPTION value=1000>KHz
			<OPTION value=1000000 selected>MHz
			<OPTION value=1000000000>GHz
			</OPTION></SELECT> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Impedance Z<sub>0</sub>:</b></font> </td>
                   <td align="left">  <input type="text" name="z" size="7" maxlength="15">
              <SELECT name=ZM >
				<OPTION value=1 selected>ohm
				<OPTION value=1000>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT> </td></tr>
				
			<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Frequency Response Ripple:</b></font> </td> 
                     <td align="left"><input type="text" name="r" size="7" maxlength="15">
           <font size="2" face="verdana"
        color="#1f6fa2"><b> db</b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Number of Components:</b></font> </td> 
                    <td align="left"> <input type="text" name="n" size="7" maxlength="15">
           <font size="2" face="verdana"
        color="#1f6fa2"><b>  (1-11)</b></font> </td></tr>

			  
		<tr>
            <td  align="right"><input type="Button" NAME="name" VALUE="Calculate" onClick="solve4(this.form);" class="calculate_cal"></td>
			<td><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		<tr><th colspan="2" align="center">Result:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance: 
                   
            </b></font>  </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Inductance:</b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=CM >
				<OPTION value=0.000000000001 selected>pF
				<OPTION value=0.000000001>nF
				<OPTION value=0.000001>µF
				<OPTION value=0.001>mF
				<OPTION value=1>F
				</OPTION></SELECT></b></font>  </td>
				
				<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=LM>

				<OPTION value=0.000000001 selected>nH
				<OPTION value=0.000001>µH
				<OPTION value=0.001>mH
				<OPTION value=1>H
				</OPTION></SELECT></b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>1</sub>: 
                    <input type="text" name="s2" size="12" maxlength="15">
            </b></font>  </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>1</sub>: 
                    <input type="text" name="t1" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>2</sub>: 
                    <input type="text" name="s4" size="12" maxlength="15">
            </b></font>  </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>2</sub>: 
                    <input type="text" name="t3" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>3</sub>: 
                    <input type="text" name="s6" size="12" maxlength="15">
            </b></font>  </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>3</sub>: 
                    <input type="text" name="t5" size="12" maxlength="15">
            </b></font></td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>4</sub>: 
                    <input type="text" name="s8" size="12" maxlength="15">
            </b></font>  </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>4</sub>: 
                    <input type="text" name="t7" size="12" maxlength="15">
            </b></font></td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>5</sub>: 
                    <input type="text" name="s10" size="12" maxlength="15">
            </b></font>  </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>5</sub>: 
                    <input type="text" name="t9" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr><td></td>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>6</sub>: 
                    <input type="text" name="t11" size="12" maxlength="15">
            </b></font> </td></tr>
			<tr><td colspan="2" style="height:15px;"></td></tr>
				</table>
				</center>
				</form>
		
		</div>
		<div class="calc_right">
		<h5>Chebyshev Pi LC High Pass Filter</h5>
		<p>The Chebyshev type filter is named in honor of Pafnuty Chebyshev because their mathematical characteristics are derived from Chebyshev polynomials.
A PI filter is a filter that has a series element and two parallel elements connected in the shape of the Greek letter PI.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
	<div id="content-for-f" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

	 <!------form6--->
<form  name="form" METHOD="POST"> 
		<center>
         <table align="center" class="filt">
		<tr><th colspan="2" align="center">Enter Values:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Cutoff Frequency: </b></font> </td>
                   <td align="left">  <input type="text" name="c" size="7" maxlength="15">
             <SELECT name=FM>
				<OPTION value=1>Hz
				<OPTION value=1000>KHz
				<OPTION value=1000000 selected>MHz
				<OPTION value=1000000000>GHz
				</OPTION></SELECT> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Impedance Z<sub>0</sub>:</b></font> </td>
                 <td align="left">    <input type="text" name="z" size="7" maxlength="15">
             <SELECT name=ZM>
				<OPTION value=1 selected>ohm
				<OPTION value=1000>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Number of Components:</b></font> </td> 
                 <td align="left">    <input type="text" name="n" size="7" maxlength="15">
           <font size="2" face="verdana"
        color="#1f6fa2"><b> (1-11)</b></font> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" NAME="name" VALUE="Calculate" onClick="solve5(this.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		<tr><th colspan="2" align="center">Result</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		
  		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Inductance:</b></font> </td>
		
		 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance:</b></font> </td></tr>
		
		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=LM>
			<OPTION value=0.000000001 selected>nH
			<OPTION value=0.000001>µH
			<OPTION value=0.001>mH
			<OPTION value=1>H
			</OPTION></SELECT></b></font></td>
			
			 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=CM>
			<OPTION value=0.000000000001 selected>pF
			<OPTION value=0.000000001>nF
			<OPTION value=0.000001>µF
			<OPTION value=0.001>mF
			<OPTION value=1>F
			</OPTION></SELECT></b></font> </td></tr>
		
		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>1</sub>: 
                    <input type="text" name="t1" size="12" maxlength="15" readonly="readonly">
            </b></font> </td>
			
			 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>1</sub>: 
                    <input type="text" name="s2" size="12" maxlength="15" readonly="readonly">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>2</sub>: 
                    <input type="text" name="t3" size="12" maxlength="15" readonly="readonly">
            </b></font></td> 
			
			 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>2</sub>: 
                    <input type="text" name="s4" size="12" maxlength="15" readonly="readonly"> </b></font></td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>3</sub>: 
                    <input type="text" name="t5" size="12" maxlength="15" readonly="readonly">
            </b></font> </td>
			
			 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>3</sub>: 
                    <input type="text" name="s6" size="12" maxlength="15" readonly="readonly">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>4</sub>: 
                    <input type="text" name="t7" size="12" maxlength="15" readonly="readonly">
            </b></font> </td>
			 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>4</sub>: 
                    <input type="text" name="s8" size="12" maxlength="15" readonly="readonly">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>5</sub>: 
                    <input type="text" name="t9" size="12" maxlength="15" readonly="readonly">
            </b></font> </td>
			
			 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>5</sub>: 
                    <input type="text" name="s10" size="12" maxlength="15" readonly="readonly">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>6</sub>: 
                    <input type="text" name="t11" size="12" maxlength="15" readonly="readonly">
            </b></font> </td></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>		
				</table></center>	</form>

			
		
		</div>
		<div class="calc_right">
		<h5>Butterworth Tee LC Low Pass Filter</h5>
							 	<p>The Butterworth type filter was first described by the British engineer Stephen Butterworth. A Tee filter is a filter that has a two series elements and a parallel element connected in the shape of letter T.Butterworth filter are characterized by a constant gain (flat response) across the midband of the circuit and a 20 dB per decade roll-off rate for each pole contained in the circuit.</p>

		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>			
	<div id="content-for-g" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

	 <!------form7--->
<form   METHOD="POST" id="high"> 
		<center>
         <table align="center" class="filt">
		<tr><th colspan="2" align="center">Enter Values:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		<tr><td colspan="2" align="center"><img src="images/bw tee high.jpg"  border="0"></td>
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Cutoff Frequency: </b></font> </td>
                   <td align="left">  <input type="text" name="c" size="7" maxlength="15">
              <SELECT name=FM >
			<OPTION value=1>Hz
			<OPTION value=1000>KHz
			<OPTION value=1000000 selected>MHz
			<OPTION value=1000000000>GHz
			</OPTION></SELECT> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Impedance Z<sub>0</sub>:</b></font> </td>
                  <td align="left">   <input type="text" name="z" size="7" maxlength="15">
              <SELECT name=ZM >
				<OPTION value=1 selected>ohm
				<OPTION value=1000>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Number of Components: </b></font> </td>
                   <td align="left">  <input type="text" name="n" size="7" maxlength="15">
            <font size="2" face="verdana"
        color="#1f6fa2"><b>(1-11)</b></font> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" NAME="name" VALUE="Calculate" onClick="solve6(this.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
		</tr>
			<tr><td colspan="2" style="height:15px;"></td></tr>	
		<tr><th colspan="2" align="center">Result:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		
		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance:   </b></font></td>
                   
           
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Inductance:</b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=CM >
				<OPTION value=0.000000000001 selected>pF
				<OPTION value=0.000000001>nF
				<OPTION value=0.000001>µF
				<OPTION value=0.001>mF
				<OPTION value=1>F
				</OPTION></SELECT></b></font></td> 
				
				<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=LM>

				<OPTION value=0.000000001 selected>nH
				<OPTION value=0.000001>µH
				<OPTION value=0.001>mH
				<OPTION value=1>H
				</OPTION></SELECT></b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>1</sub>: 
                    <input type="text" name="x1" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>1</sub>: 
                    <input type="text" name="y2" size="12" maxlength="15">
            </b></font></td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>2</sub>: 
                    <input type="text" name="x3" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>2</sub>: 
                    <input type="text" name="y4" size="12" maxlength="15">
            </b></font></td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>3</sub>: 
                    <input type="text" name="x5" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>3</sub>: 
                    <input type="text" name="y6" size="12" maxlength="15">
            </b></font></td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>4</sub>: 
                    <input type="text" name="x7" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>4</sub>: 
                    <input type="text" name="y8" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>5</sub>: 
                    <input type="text" name="x9" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>5</sub>: 
                    <input type="text" name="y10" size="12" maxlength="15">
            </b></font></td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>6</sub>: 
                    <input type="text" name="x11" size="12" maxlength="15">
            </b></font> </td></tr>
			<tr><td colspan="2" style="height:15px;"></td></tr>
			</table>
			</center>
			</form>			
			
			 				
		
		</div>
		<div class="calc_right">
		<h5>Butterworth Tee LC High Pass Filter</h5>
		<p>The Butterworth type filter was first described by the British engineer Stephen Butterworth. A Tee filter is a filter that has a two series elements and a parallel element connected in the shape of letter T.Butterworth filter are characterized by a constant gain (flat response) across the midband of the circuit and a 20 dB per decade roll-off rate for each pole contained in the circuit.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
	</div>
	<div id="content-for-h" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

	 <!------form8--->
<form  METHOD="POST"> 
		<center>
         <table align="center" class="filt">
		<tr><th colspan="2" align="center">Enter Values:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		<tr><td colspan="2" align="center"><img src="images/bw tee low1.jpg"  border="0"></td>
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Cutoff Frequency: </b></font> </td>
                   <td align="left">  <input type="text" name="c" size="7" maxlength="15">
              <SELECT name=FM >
			<OPTION value=1>Hz
			<OPTION value=1000>KHz
			<OPTION value=1000000 selected>MHz
			<OPTION value=1000000000>GHz
			</OPTION></SELECT> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Impedance Z<sub>0</sub>:</b></font> </td>
                   <td align="left">  <input type="text" name="z" size="7" maxlength="15">
              <SELECT name=ZM >
				<OPTION value=1 selected>ohm
				<OPTION value=1000>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT> </td></tr>
				
			<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Frequency Response Ripple: </b></font> </td>
                     <td align="left"><input type="text" name="r" size="7" maxlength="15">
           <font size="2" face="verdana"
        color="#1f6fa2"><b> db</b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Number of Components: </b></font> </td>
                   <td align="left"> <input type="text" name="n" size="7" maxlength="15">
             <font size="2" face="verdana"
        color="#1f6fa2"><b>(1-11)</b></font> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" NAME="name" VALUE="Calculate" onClick="solve7(this.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>

			  
		<tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>		
		<tr><th colspan="2" align="center">Results:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		</td></tr>
		
  		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Inductance:</b></font> </td>
		
		<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance: 
         </b></font> </td></tr>
		
		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=LM>
			<OPTION value=0.000000001 selected>nH
			<OPTION value=0.000001>µH
			<OPTION value=0.001>mH
			<OPTION value=1>H
			</OPTION></SELECT></b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=CM>
			<OPTION value=0.000000000001 selected>pF
			<OPTION value=0.000000001>nF
			<OPTION value=0.000001>µF
			<OPTION value=0.001>mF
			<OPTION value=1>F
			</OPTION></SELECT></b></font> </td></tr>
		
		<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>1</sub>: 
                    <input type="text" name="t1" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>1</sub>: 
                    <input type="text" name="s2" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>2</sub>: 
                    <input type="text" name="t3" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>2</sub>: 
                    <input type="text" name="s4" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>3</sub>: 
                    <input type="text" name="t5" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>3</sub>: 
                    <input type="text" name="s6" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>4</sub>: 
                    <input type="text" name="t7" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>4</sub>: 
                    <input type="text" name="s8" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>5</sub>: 
                    <input type="text" name="t9" size="12" maxlength="15">
            </b></font> </td>
			
			<td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>5</sub>: 
                    <input type="text" name="s10" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>6</sub>: 
                    <input type="text" name="t11" size="12" maxlength="15">
            </b></font> </td></tr>
			<tr><td colspan="2" style="height:15px;"></td></tr>
			</table>
			
			</center>
			</form>		
			
			 				
		
		</div>
		<div class="calc_right">
			<h5>Chebyshev Tee LC Low Pass Filter</h5>
			<p>A Tee filter is a filter that has a two series elements and a parallel element connected in the shape of letter T.Chebyshev filters are analog or digital filters having  a steeper roll-off and more passband ripple or stop-band ripple than Butterworth filters. Chebyshev filters have the property that they minimize the error between the  idealized filter characteristic and the actual over the range of the filter, but with ripples in the passband.A Low pass filter is a filter that passes low-frequency signals but attenuates signals with frequencies higher than the cut-off frequency.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
	</div>	
	<div id="content-for-i" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">


	 <!------form9--->
<form   METHOD="POST"> 
		<center>
         <table align="center" class="filt">
		<tr><th colspan="2" align="center">Enter Values:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		<tr><td colspan="2" align="center"><img src="images/bw tee high1.jpg"  border="0"></td>
		
		<tr>
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Cutoff Frequency: </b></font> </td>
                    <td align="left"> <input type="text" name="c" size="7" maxlength="15">
              <SELECT name=FM >
			<OPTION value=1>Hz
			<OPTION value=1000>KHz
			<OPTION value=1000000 selected>MHz
			<OPTION value=1000000000>GHz
			</OPTION></SELECT></td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Impedance Z<sub>0</sub>:</b></font> </td>
                   <td align="left">  <input type="text" name="z" size="7" maxlength="15">
              <SELECT name=ZM >
				<OPTION value=1 selected>ohm
				<OPTION value=1000>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT></td></tr>
				
			<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Frequency Response Ripple: </b></font> </td>
                    <td align="left"> <input type="text" name="r" size="7" maxlength="15">
            <font size="2" face="verdana"
        color="#1f6fa2"><b>db</b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Number of Components: </b></font> </td>
                    <td align="left"> <input type="text" name="n" size="7" maxlength="15">
           <font size="2" face="verdana"
        color="#1f6fa2"><b> (1-11)</b></font> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" NAME="name" VALUE="Calculate" onClick="solve8(this.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
		<tr><th colspan="2" align="center">Result</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance: 
                   
            </b></font> </td>
			
			 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Inductance:</b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=CM >
				<OPTION value=0.000000000001 selected>pF
				<OPTION value=0.000000001>nF
				<OPTION value=0.000001>µF
				<OPTION value=0.001>mF
				<OPTION value=1>F
				</OPTION></SELECT></b></font> </td>
				
				 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>Unit : <SELECT name=LM>

				<OPTION value=0.000000001 selected>nH
				<OPTION value=0.000001>µH
				<OPTION value=0.001>mH
				<OPTION value=1>H
				</OPTION></SELECT></b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>1</sub>: 
                    <input type="text" name="x1" size="12" maxlength="15">
            </b></font></td> 
			
			 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>1</sub>: 
                    <input type="text" name="y2" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>2</sub>: 
                    <input type="text" name="x3" size="12" maxlength="15">
            </b></font> </td>
			
			 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>2</sub>: 
                    <input type="text" name="y4" size="12" maxlength="15">
            </b></font></td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>3</sub>: 
                    <input type="text" name="x5" size="12" maxlength="15">
            </b></font> </td>
			
			 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>3</sub>: 
                    <input type="text" name="y6" size="12" maxlength="15">
            </b></font> </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>4</sub>: 
                    <input type="text" name="x7" size="12" maxlength="15">
            </b></font></td>
			
			 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>4</sub>: 
                    <input type="text" name="y8" size="12" maxlength="15">
            </b></font>  </td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>5</sub>: 
                    <input type="text" name="x9" size="12" maxlength="15">
            </b></font> </td>
			
			 <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>L<sub>5</sub>: 
                    <input type="text" name="y10" size="12" maxlength="15">
            </b></font></td></tr>
			
			<tr>
		          <td align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b>C<sub>6</sub>: 
                    <input type="text" name="x11" size="12" maxlength="15">
            </b></font> </td></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>			
			</table>
			</center>
			</form>		
			 				
		
		</div>
		<div class="calc_right">
			<h5>Chebyshev Tee LC High Pass Filter</h5>
			<p>A Tee filter is a filter that has a two series elements and a parallel element connected in the shape of letter T. Chebyshev filters are analog or digital filters having a steeper roll-off and more passband ripple or stopband ripple than Butterworth filters. Chebyshev filters have the property that they minimize the error between the idealized filter characteristic and the actual over the range of the filter, but with ripples in the passband.Chebyshev filters provide sharper rate of attenuation beyond the -3 db point. Chebyshev filters are more sensitive to component tolerances than Butterworth filters. </p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
	</div>		
	
	<div id="content-for-j" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">


	  <center>
         <table align="center" class="filt">
		<tr><td colspan="2" style="height:15px;"></td></tr>	
		<tr><td colspan="2" align="center"><img src="images/equal comp low.jpg" border="0"></td>
	  
	  <tr><td align="center" colspan="2">  <p><font size="2" face="verdana"
        color="#1f6fa2"><b>R<sub>a</sub> = Filter Resistor <br>  R<sub>b</sub> = Filter Resistor <br>
	  (R<sub>a</sub> = R<sub>b</sub>)<br> C<sub>a</sub> = Filter Capacitor <br> 
	  C<sub>b</sub> = Filter Capacitor <br> (C<sub>a</sub> = C<sub>b</sub>)<br>
	  R<sub>c</sub> = Feedback Resistor <br> R<sub>d</sub> = Feedback Resistor </b></font></p>    </td></tr>

   <!------form10----->
<form  name="form">  

		<tr><td colspan="2" style="height:15px;"></td></tr>		
		<tr><th colspan="2" align="center">Enter Value</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>		
				<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Cutoff Frequency: </b></font> </td>
                  <td align="left"> <input type="text" name="F" size="12" maxlength="15">
              <SELECT name=FM>
				<OPTION value=1>Hz
				<OPTION value=1000 selected>KHz
				<OPTION value=1000000>MHz
				</OPTION></SELECT></td>
				</tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>		
		<tr><th colspan="2" align="center">Results<br />(Enter any one value - Resistor or Capacitor and <br>
		 any one value - Resistor R<sub>c</sub> or Resistor R<sub>d</sub>)</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>

		
  <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistor (R<sub>a</sub> = R<sub>b</sub>): </b></font> </td>
                    <td align="left"><input type="text" name="R" size="12" maxlength="15">
               <SELECT name=RM>
				<OPTION value=1>ohm
				<OPTION value=1000 selected>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT> </td></tr>
				
				 <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitor (C<sub>a</sub> = C<sub>b</sub>): </b></font> </td>
                  <td align="left">  <input type="text" name="C" size="12" maxlength="15">
              <SELECT name=CM>
				<OPTION value=0.000000000001>pF
				<OPTION value=0.000000001 selected>nF
				<OPTION value=0.000001>µF
				<OPTION value=0.001>mF
				
				<OPTION value=1>F
				</OPTION></SELECT> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistor R<sub>c</sub>: </b></font> </td>
                 <td align="left">   <input type="text" name="R3" size="12" maxlength="15">
            <SELECT name=R3M>
				<OPTION value=1>ohm
				<OPTION value=1000 selected>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT> </td></tr>
			
			<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistor R<sub>d</sub>: </b></font> </td>
                  <td align="left">  <input type="text" name="R4" size="12" maxlength="15">
            <SELECT name=R4M>
			<OPTION value=1>ohm
			<OPTION value=1000 selected>Kohm
			<OPTION value=1000000>Mohm
			</OPTION></SELECT> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" NAME="name" VALUE="Calculate" onClick="solve9(this.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
		  <tr><td colspan="2" style="height:15px;"></td></tr>
        </table>
		</center>
      </form>		
			 				
		
		</div>
		<div class="calc_right">
			<h5>Equal Component Butterworth Low Pass Filter</h5>
			<p>Enter Fc (required) and ONE of the component values in each colored group (R1 or C1 and R3 or R4) to calculate the remaining two filter component values.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
	</div>		
	<div id="content-for-k" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

	  <center>
         <table align="center" class="filt">
		<tr><td colspan="2" style="height:15px;"></td></tr>	
		<tr><td colspan="2" align="center"><img src="images/equal comp high.jpg" border="0"></td>
	  
	  
	  <tr><td colspan="2" align="center"> <p><font size="2" face="verdana"
        color="#1f6fa2"><b>R<sub>a</sub> = Filter Resistor <br>  R<sub>b</sub> = Filter Resistor <br>
	  (R<sub>a</sub> = R<sub>b</sub>)<br> C<sub>a</sub> = Filter Capacitor <br> 
	  C<sub>b</sub> = Filter Capacitor <br> (C<sub>a</sub> = C<sub>b</sub>)<br>
	  R<sub>c</sub> = Feedback Resistor <br> R<sub>d</sub> = Feedback Resistor </b></font></p> </td></tr>
	    

     <br>
	 <!------form11--->
<form method="POST" >  
       
		<tr><td colspan="2" style="height:15px;"></td></tr>		
		<tr><th colspan="2" align="center">Enter Value</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>	
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Cutoff Frequency: </b></font> </td>
                 <td align="left"> <input type="text" name="F" size="12" maxlength="15">
              <SELECT name=FM>
				<OPTION value=1>Hz
				<OPTION value=1000 selected>KHz
				<OPTION value=1000000>MHz
				</OPTION></SELECT></td></tr>
			  
		<tr><td colspan="2" style="height:15px;"></td></tr>		
		<tr><th colspan="2" align="center">Result<br />(Enter any one value - Resistor or Capacitor and <br>
		 any one value - Resistor R<sub>c</sub> or Resistor R<sub>d</sub>)</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>	

		
  <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistor (R<sub>a</sub> = R<sub>b</sub>): </b></font> </td>
                   <td align="left"> <input type="text" name="R" size="12" maxlength="15">
               <SELECT name=RM>
				<OPTION value=1>ohm
				<OPTION value=1000 selected>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT> </td></tr>
				
				 <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitor (C<sub>a</sub> = C<sub>b</sub>): </b></font> </td>
                  <td align="left">  <input type="text" name="C" size="12" maxlength="15">
              <SELECT name=CM>
				<OPTION value=0.000000000001>pF
				<OPTION value=0.000000001 selected>nF
				<OPTION value=0.000001>µF
				<OPTION value=0.001>mF
				
				<OPTION value=1>F
				</OPTION></SELECT></td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistor R<sub>c</sub>: </b></font> </td>
                  <td align="left">  <input type="text" name="R3" size="12" maxlength="15">
            <SELECT name=R3M>
				<OPTION value=1>ohm
				<OPTION value=1000 selected>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT> </td></tr>
			
			<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistor R<sub>d</sub>: </b></font> </td>
                   <td align="left"> <input type="text" name="R4" size="12" maxlength="15">
            <SELECT name=R4M>
			<OPTION value=1>ohm
			<OPTION value=1000 selected>Kohm
			<OPTION value=1000000>Mohm
			</OPTION></SELECT> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" NAME="name" VALUE="Calculate" onClick="solve10(this.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal" ></td>
      
          </tr>
		  <tr><td colspan="2" style="height:15px;"></td></tr>
        </table>
		</center>
      </form>
					
			
			 				
		
		</div>
		<div class="calc_right">
			<h5>Equal Component Butterworth High Pass Filter</h5>
	 	<p>Enter Fc (required) and ONE of the component values in each colored group (R1 or C1 and R3 or R4) to calculate the remaining two filter component values.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
	</div>	
	
	<div id="content-for-l" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

	  <center>
         <table align="center" class="filt">
		<tr><td colspan="2" style="height:15px;"></td></tr>	
		<tr><td colspan="2" align="center"><img src="images/sk_low2.jpg" border="0"></td></tr>
	
	  
	<tr><td align="center" colspan="2"> <p><font size="2" face="verdana"
        color="#1f6fa2"><b>R<sub>a</sub> = Filter Resistor <br>  R<sub>b</sub> = Filter Resistor <br>
	 C<sub>a</sub> = Filter Capacitor <br> 
	  C<sub>b</sub> = Filter Capacitor  <br>(C<sub>a</sub> = C<sub>b</sub>) <br> </b></font></p>  </td></tr>
	 <tr><td align="center" colspan="2"><img src="images/sk_low.bmp" border="0"><br>  
	   <img src="images/sk_low1.bmp"  border="0"></td></tr>

<!------form12--->    
<form method="POST" name="form">  
		<tr><td colspan="2" style="height:15px;"></td></tr>		
		<tr><th colspan="2" align="center">Enter Value</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>	
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Cutoff Frequency:</b></font></td> 
                  <td align="left">  <input type="text" name="F" size="10" maxlength="15">
              <SELECT name=FM>
				<OPTION value=1>Hz
				<OPTION value=1000 selected>KHz
				<OPTION value=1000000>MHz
				</OPTION></SELECT> </td></tr>
			  
		<tr><td colspan="2" style="height:15px;"></td></tr>		
		<tr><th colspan="2" align="center">Result<br />(Enter any one value - Resistor or C<sub>a</sub> or C<sub>b</sub>)</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>	

		
  <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistor (R<sub>a</sub> = R<sub>b</sub>): </b></font> </td>
                   <td align="left">  <input type="text" name="R" size="10" maxlength="15">
               <SELECT name=RM>
				<OPTION value=1>ohm
				<OPTION value=1000 selected>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitor C<sub>a</sub>: </b></font> </td>
                    <td align="left"> <input type="text" name="C1" size="10" maxlength="15">
            <SELECT name=C1M>
			<OPTION value=0.000000000001>pF
			<OPTION value=0.000000001 selected>nF
			<OPTION value=0.000001>µF
			<OPTION value=0.001>mF
			
			<OPTION value=1>F
			</OPTION></SELECT> </td></tr>
			
			<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitor C<sub>b</sub>: </b></font> </td>
                  <td align="left">   <input type="text" name="C2" size="10" maxlength="15">
            <SELECT name=C2M STYLE="width: 64">
			<OPTION value=0.000000000001>pF
			<OPTION value=0.000000001 selected>nF
			<OPTION value=0.000001>µF
			<OPTION value=0.001>mF
			<OPTION value=1>F
			</OPTION></SELECT> </td></tr>

		<tr>
            <td align="right"><input type="Button" NAME="name" VALUE="Calculate" onClick="solve11(this.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
		  <tr><td colspan="2" style="height:15px;"></td></tr>	
        </table>
		</center>
		
		</form>
			 				
		
		</div>
		<div class="calc_right">
		<h5>Sallen-Key Butterworth Low Pass Filter</h5>
		<p>The circuit produces a 2-pole lowpass response using two resistors, two capacitors and a unity-gain buffer amplifier. This filter topology is also known as a voltage 
controlled voltage source (VCVS) filter.The Sallen-Key filter is a simple active filter based on op-amps stages, which is ideal for filtering audio frequencies. 
A Low pass filter is a filter that passes low-frequency signals but attenuates signals with frequencies higher than the cutoff frequency.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
	</div>	
	<div id="content-for-m" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

	  <center>
         <table align="center" class="filt">
		<tr><td colspan="2" style="height:15px;"></td></tr>	
		<tr><td colspan="2" align="center"><img src="images/sk high.jpg" border="0"></td></tr>

	  
	  <tr><td colspan="2" align="center"> <p><font size="2" face="verdana"
        color="#1f6fa2"><b>R<sub>a</sub> = Filter Resistor <br>  R<sub>b</sub> = Filter Resistor <br>
	 C<sub>a</sub> = Filter Capacitor <br> 
	  C<sub>b</sub> = Filter Capacitor  <br>(C<sub>a</sub> = C<sub>b</sub>) <br> </b></font></p>  </td></tr>
	 
	  <tr><td colspan="2" align="center"><img src="images/sk2 form.bmp" border="0"><br>  
	   <img src="images/sk3 form.bmp"  border="0"></td></tr> 
	   
<!------form13--->
    
<form method="POST" >  
 
		<tr><td colspan="2" style="height:15px;"></td></tr>		
		<tr><th colspan="2" align="center">Enter Value</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>	
		
		<tr>
		          <td  align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Cutoff Frequency: </b></font></td>
		<td align="left">    <input type="text" name="F" size="10" maxlength="15">
              <SELECT name=FM>
				<OPTION value=1>Hz
				<OPTION value=1000 selected>KHz
				<OPTION value=1000000>MHz
				</OPTION></SELECT> </td></tr>

				<tr><td colspan="2" style="height:15px;"></td></tr>		
		<tr><th colspan="2" align="center">Result <br />(Enter any one value - R<sub>a</sub> 
                    or R<sub>b</sub> or Capacitor)</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>		  
	
  <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistor R<sub>a</sub>:  </b></font> </td>
                   <td align="left">  <input type="text" name="R2" size="10" maxlength="15">
               <SELECT name=R2M>
			<OPTION value=1>ohm
			<OPTION value=1000 selected>Kohm
			<OPTION value=1000000>Mohm
			</OPTION></SELECT>
			 </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Resistor R<sub>b</sub>:  </b></font> </td>
		 <td align="left"><input type="text" name="R1" size="10" maxlength="15">
           <SELECT name=R1M>
				<OPTION value=1>ohm
				<OPTION value=1000 selected>Kohm
				<OPTION value=1000000>Mohm
				</OPTION></SELECT>        
       </td></tr>
			
			<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitor (C<sub>a</sub> = C<sub>b</sub>):  </b></font> </td>
                   <td align="left">  <input type="text" name="C" size="10" maxlength="15" >
            <SELECT name=CM>
			<OPTION value=0.000000000001>pF
			<OPTION value=0.000000001 selected>nF
			<OPTION value=0.000001>µF
			<OPTION value=0.001>mF
			<OPTION value=1>F
			</OPTION></SELECT> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" NAME="name" VALUE="Calculate" onClick="solve12(this.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
		  <tr><td colspan="2" style="height:15px;"></td></tr>
        </table>
		</center>
		</form>		
			
			 				
		
		</div>
		<div class="calc_right">
			<h5>Sallen-Key Butterworth High Pass Filter</h5>
			<p>The circuit produces a 2-pole highpass response using two resistors, two capacitors and a unity-gain buffer amplifier. This filter topology is also known as a voltage controlled voltage source (VCVS) filter. A High pass filter is a filter that passes high frequencies, but attenuates frequencies lower than the cutoff frequency.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
	</div>					
						
			
        </div>
  
</div>

    </div>
</div>
</div>
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>

<script language="javascript" type="text/javascript">
$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL1 li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL1 li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#fff'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL1 li").each(function() { 
			$(".firstUL1 li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#fff'}); //set the background color to light-blue to that div
	 });
});
</script>
<!--script for hf filer-->

<SCRIPT language=JavaScript>
La = "   "; Lb = "  "; Ca = "   "; Cb = "   "; Cc="   ";
var fa = 100; var fb = fa;var m = 1000000;
function init() {
   a="";  b=""; c="";  d=""; e="";  bg = 0; c1 = "white"; c2 = "white";
   backgr(); info = 0;var  m = 1000000; var fa = 100; var fb = fa;
   La = "   "; Lb = "  "; Ca = "   "; Cb = "   "; Cc="   ";
   document.forms.frm1.freq11.focus();
   display();
}
function backgr() {
   bg==0 ? document.bgColor = c1 : document.bgColor = c2;
   bg = bg^1;
}
function display() {
  t = "";
  pr("      Constant - K Network\r\n");
  pr("          "+La+"         "+Lb);
  pr("    +--+--(((((((--+--(((((((---+--+");
  pr("      _|_         _|_          _|_");
  pr(" R in ~|~         ~|~          ~|~  R out");
  pr("       |           |            |");
  pr("    +--+-----------+------------+--+");
  pr("    "+Ca+"      "+Cb+"       "+Cc);
 
}
function pr(text) {
	 a = ""+La;
	 b = ""+Lb;
	 c = ""+Ca;
	 d = ""+Cb;
	 e = ""+Cc;
	 

    document.forms.frm1.La.value=a;
    document.forms.frm1.Lb.value=b;
	document.forms.frm1.Ca.value=c;
    document.forms.frm1.Cb.value=d;
	document.forms.frm1.Cc.value=e;
    
}
function calc(form) {
    
    var f = document.forms.frm1.freq11.value;
    var rin = document.forms.frm1.Rin11.value;
    var ro = document.forms.frm1.Rout11.value;
    var fac = document.forms.frm1.Qf11.value;
   if((f =="") || (rin =="") || (ro =="") || (fac =="")) return;
   f = parseFloat(f); rin = parseFloat(rin); ro = parseFloat(ro);
   qf = parseInt(fac,10);
   xcb = ro;
   xca = rin * Math.sqrt((ro/rin)/(((qf*qf)+1)-(ro/rin)));
   xla = ((qf*ro)+((rin*ro)/xca))/((qf*qf)+1);
   xcc = ro/qf;
   xcd = ro * Math.sqrt(1/(qf*qf));
   xlb = ((qf*ro)+((ro*ro)/xcb))/((qf*qf)+1);
   La = xla/(6.284*f); 
   if(La < 1) fa = fa*100; 
   La = (parseInt((La*fa)+0.5)/fa);
   Lb = xlb/(6.284*f);
   if(Lb < 1) fb = fb*100;
   Lb = (parseInt((Lb*fb)+0.5)/fb);
   fa = 100; fb = fa;
   with(Math) {   
      Ca = (ceil(m/(6.284*f*xca)));
      Cb1 = ceil(m/(6.284*f*xcb));
      Cb2 = ceil(m/(6.284*f*xcc));
      Cc  = (ceil(m/(6.284*f*xcd)));
   }
   Cb  = (Cb1 + Cb2);
   display(); info = 1;
}
</SCRIPT>


<!--script for hf filer-->

<!--script for Butterworth Pi LC Low Pass Filter -->

<script LANGUAGE="JavaScript">

var FM = 0;
var ZM = 0;
var CM = 0;
var LM = 0;

function clearBoxes1(form)
{
form.c.value = "";
form.z.value = "";
form.n.value = "";
form.c.focus();
}

function MakeArray(j){
this.length = j;
var i;
for(i=1; i<=j; i++)
this[i] = "";
}

function solve1(f){
var i;

c = eval(f.c.value);
z = eval(f.z.value);
n = eval(f.n.value);
FM = eval(f.FM.value);
ZM = eval(f.ZM.value);
CM = eval(f.CM.value);
LM = eval(f.LM.value);
c = c*FM;
z = z*ZM;

if (n<1 || f.n.value==""){
n = 1;
f.n.value = n;
}
if (n>11){
n = 11;
f.n.value = n;
}
if (c<1 || f.c.value==""){
c = 1000;
f.c.value = c/FM;
}
if (z<=0 || f.z.value==""){
z = 50;
f.z.value = z/ZM;
}

w = 2*Math.PI*c;

var g = new MakeArray(12);
var x = new MakeArray(12);
var y = new MakeArray(12);
	var Precision=7;
for(i=1; i<=n; i++)
{
k = (2*i-1)*Math.PI/(2*n);
g[i] = 2*Math.sin(k);
x[i] = g[i]/(z*w);
y[i] = g[i]*z/w;
}

f.x1.value = (x[1]/CM).toPrecision(Precision);
f.y2.value = (y[2]/LM).toPrecision(Precision);
f.x3.value = (x[3]/CM).toPrecision(Precision);
f.y4.value = (y[4]/LM).toPrecision(Precision);
f.x5.value = (x[5]/CM).toPrecision(Precision);
f.y6.value = (y[6]/LM).toPrecision(Precision);
f.x7.value = (x[7]/CM).toPrecision(Precision);
f.y8.value = (y[8]/LM).toPrecision(Precision);
f.x9.value = (x[9]/CM).toPrecision(Precision);
f.y10.value = (y[10]/LM).toPrecision(Precision);
f.x11.value = (x[11]/CM).toPrecision(Precision);

}


</script>


<!--script for Butterworth Pi LC Low Pass Filter -->

<!--script for Butterworth Pi LC High Pass Filter -->

<script LANGUAGE="JavaScript">
<!--
var FM = 0;
var ZM = 0;
var CM = 0;
var LM = 0;

function clearBoxes(form)
{
form.c.value = "";
form.z.value = "";
form.n.value = "";
form.c.focus();
}

function MakeArray(j){
this.length = j;
var i;
for(i=1; i<=j; i++)
this[i] = "";
}

function solve2(f){
var i;

c = eval(f.c.value);
z = eval(f.z.value);
n = eval(f.n.value);
FM = eval(f.FM.value);
ZM = eval(f.ZM.value);
CM = eval(f.CM.value);
LM = eval(f.LM.value);
c = c*FM;
z = z*ZM;
if (n<1 || f.n.value==""){
n = 1;
f.n.value = n;
}
if (n>11){
n = 11;
f.n.value = n;
}
if (c<1 || f.c.value==""){
c = 1000;
f.c.value = c/FM;
}
if (z<=0 || f.z.value==""){
z = 50;
f.z.value = z/ZM;
}
w = 2*Math.PI*c;
var g = new MakeArray(12);
var s = new MakeArray(12);
var t = new MakeArray(12);
var Precision=7;
for(i=1; i<=n; i++){
k = (2*i-1)*Math.PI/(2*n);
g[i] = 2*Math.sin(k);
s[i] = 1/(g[i]*z*w);
t[i] = z/(g[i]*w);
}
f.t1.value = (t[1]/LM).toPrecision(Precision);
f.s2.value = (s[2]/CM).toPrecision(Precision);
f.t3.value = (t[3]/LM).toPrecision(Precision);
f.s4.value = (s[4]/CM).toPrecision(Precision);
f.t5.value = (t[5]/LM).toPrecision(Precision);
f.s6.value = (s[6]/CM).toPrecision(Precision);
f.t7.value = (t[7]/LM).toPrecision(Precision);
f.s8.value = (s[8]/CM).toPrecision(Precision);
f.t9.value = (t[9]/LM).toPrecision(Precision);
f.s10.value = (s[10]/CM).toPrecision(Precision);
f.t11.value = (t[11]/LM).toPrecision(Precision);
}

-->
</script>

<!--script for Butterworth Pi LC High Pass Filter -->

<!--script for Chebyshev Pi LC Low Pass Filter -->

<script LANGUAGE="JavaScript">
<!--
var FM = 0;
var ZM = 0;
var CM = 0;
var LM = 0;

function clearBoxes(form)
{
form.c.value = "";
form.r.value = "";
form.z.value = "";
form.n.value = "";
form.c.focus();
}

function MakeArray(j){
this.length = j;
var i;
for(i=1; i<=j; i++)
this[i] = "";
}
function solve3(f){
var i;
c = eval(f.c.value);
r = eval(f.r.value);
z = eval(f.z.value);
n = eval(f.n.value);
FM = eval(f.FM.value);
ZM = eval(f.ZM.value);
CM = eval(f.CM.value);
LM = eval(f.LM.value);
c = c*FM;
z = z*ZM;
if (n<1 || f.n.value==""){
n = 1;
f.n.value = n;
}
if (n>11){
n = 11;
f.n.value = n;
}
am = Math.round((n+0.1)/2)*2-1-n;
if (am<0){
n = n+1;
f.n.value = n;
}
if (c<1 || f.c.value==""){
c = 1000;
f.c.value = c/FM;
}
if (r<0.01 || f.r.value==""){
r = 0.01;
f.r.value = r;
}
if (z<=0 || f.z.value==""){
z = 50;
f.z.value = z/ZM;
}
w = 2*Math.PI*c;
rr = r/17.37;
e2x = Math.exp(2*rr);
coth = (e2x+1)/(e2x-1);
bt = Math.log(coth);
btn = bt/(2*n);
gn = (Math.exp(btn)-Math.exp(-btn))/2;
var a = new MakeArray(12);
var b = new MakeArray(12);
var g = new MakeArray(12);
var x = new MakeArray(12);
var y = new MakeArray(12);
var Precision=7;
for(i=1; i<=n; i++){
k = (2*i-1)*Math.PI/(2*n);
a[i] = Math.sin(k);
k2 = Math.PI*i/n;
k3 = Math.sin(k2);
b[i] = Math.pow(gn,2)+Math.pow(k3,2);
}
g[1] = 2*a[1]/gn;
for(i=2; i<=n; i++){
g[i] = (4*a[i-1]*a[i])/(b[i-1]*g[i-1]);
}
for(i=1; i<=n; i++){
x[i] = g[i]/(z*w);
y[i] = g[i]*z/w;
}
f.x1.value = (x[1]/CM).toPrecision(Precision);
f.y2.value = (y[2]/LM).toPrecision(Precision);
f.x3.value = (x[3]/CM).toPrecision(Precision);
f.y4.value = (y[4]/LM).toPrecision(Precision);
f.x5.value = (x[5]/CM).toPrecision(Precision);
f.y6.value = (y[6]/LM).toPrecision(Precision);
f.x7.value = (x[7]/CM).toPrecision(Precision);
f.y8.value = (y[8]/LM).toPrecision(Precision);
f.x9.value = (x[9]/CM).toPrecision(Precision);
f.y10.value = (y[10]/LM).toPrecision(Precision);
f.x11.value = (x[11]/CM).toPrecision(Precision);
}
-->
</script>



<!--script for Chebyshev Pi LC Low Pass Filter -->

<!--script for Chebyshev Pi LC High Pass Filter   -->

<script LANGUAGE="JavaScript">
<!--
var FM = 0;
var ZM = 0;
var CM = 0;
var LM = 0;

function clearBoxes(form)
{
form.c.value = "";
form.r.value = "";
form.z.value = "";
form.n.value = "";
form.c.focus();
}

function MakeArray(j){
this.length = j;
var i;
for(i=1; i<=j; i++)
this[i] = "";
}

function solve4(f){
var i;
c = eval(f.c.value);
r = eval(f.r.value);
z = eval(f.z.value);
n = eval(f.n.value);
FM = eval(f.FM.value);
ZM = eval(f.ZM.value);
CM = eval(f.CM.value);
LM = eval(f.LM.value);
c = c*FM;
z = z*ZM;
if (n<1 || f.n.value==""){
n = 1;
f.n.value = n;
}
if (n>11){
n = 11;
f.n.value = n;
}
am = Math.round((n+0.1)/2)*2-1-n;
if (am<0){
n = n+1;
f.n.value = n;
}
if (c<1 || f.c.value==""){
c = 1000;
f.c.value = c/FM;
}
if (r<0.01 || f.r.value==""){
r = 0.01;
f.r.value = r;
}
if (z<=0 || f.z.value==""){
z = 50;
f.z.value = z/ZM;
}
w = 2*Math.PI*c;
rr = r/17.37;
e2x = Math.exp(2*rr);
coth = (e2x+1)/(e2x-1);
bt = Math.log(coth);
btn = bt/(2*n);
gn = (Math.exp(btn)-Math.exp(-btn))/2;
var a = new MakeArray(12);
var b = new MakeArray(12);
var g = new MakeArray(12);
var s = new MakeArray(12);
var t = new MakeArray(12);
var Precision=7;
for(i=1; i<=n; i++){
k = (2*i-1)*Math.PI/(2*n);
a[i] = Math.sin(k);
k2 = Math.PI*i/n;
k3 = Math.sin(k2);
b[i] = Math.pow(gn,2)+Math.pow(k3,2);
}
g[1] = 2*a[1]/gn;
for(i=2; i<=n; i++){
g[i] = (4*a[i-1]*a[i])/(b[i-1]*g[i-1]);
}
for(i=1; i<=n; i++){
s[i] = 1/(g[i]*z*w);
t[i] = z/(g[i]*w);
}
f.t1.value = (t[1]/LM).toPrecision(Precision);
f.s2.value = (s[2]/CM).toPrecision(Precision);
f.t3.value = (t[3]/LM).toPrecision(Precision);
f.s4.value = (s[4]/CM).toPrecision(Precision);
f.t5.value = (t[5]/LM).toPrecision(Precision);
f.s6.value = (s[6]/CM).toPrecision(Precision);
f.t7.value = (t[7]/LM).toPrecision(Precision);
f.s8.value = (s[8]/CM).toPrecision(Precision);
f.t9.value = (t[9]/LM).toPrecision(Precision);
f.s10.value = (s[10]/CM).toPrecision(Precision);
f.t11.value = (t[11]/LM).toPrecision(Precision);
}

-->
</script>

<!--script for Chebyshev Pi LC High Pass Filter   -->

<!--script for Butterworth Tee LC Low Pass Filter   -->
<script LANGUAGE="JavaScript">

var FM = 0;
var ZM = 0;
var CM = 0;
var LM = 0;

function clearBoxes(form)
{
form.c.value = "";
form.z.value = "";
form.n.value = "";
form.c.focus();
}

function MakeArray(j){
this.length = j;
var i;
for(i=1; i<=j; i++)
this[i] = "";
}

function solve5(f){
var i;

c = eval(f.c.value);
z = eval(f.z.value);
n = eval(f.n.value);
FM = eval(f.FM.value);
ZM = eval(f.ZM.value);
CM = eval(f.CM.value);
LM = eval(f.LM.value);
c = c*FM;
z = z*ZM;

if (n<1 || f.n.value==""){
n = 1;
f.n.value = n;
}
if (n>11){
n = 11;
f.n.value = n;
}
if (c<1 || f.c.value==""){
c = 1000000000;
f.c.value = c/FM;
}
if (z<=0 || f.z.value==""){
z = 50;
f.z.value = z/ZM;
}

w = 2*Math.PI*c;

var g = new MakeArray(12);
var s = new MakeArray(12);
var t = new MakeArray(12);
var Precision=7;
for(i=1; i<=n; i++)
{
k = (2*i-1)*Math.PI/(2*n);
g[i] = 2*Math.sin(k);
s[i] = g[i]/(z*w);
t[i] = g[i]*z/w;
}

f.t1.value = (t[1]/LM).toPrecision(Precision);
f.s2.value = (s[2]/CM).toPrecision(Precision);
f.t3.value = (t[3]/LM).toPrecision(Precision);
f.s4.value = (s[4]/CM).toPrecision(Precision);
f.t5.value = (t[5]/LM).toPrecision(Precision);
f.s6.value = (s[6]/CM).toPrecision(Precision);
f.t7.value = (t[7]/LM).toPrecision(Precision);
f.s8.value = (s[8]/CM).toPrecision(Precision);
f.t9.value = (t[9]/LM).toPrecision(Precision);
f.s10.value = (s[10]/CM).toPrecision(Precision);
f.t11.value = (t[11]/LM).toPrecision(Precision);
}

</script>


<!--script for Butterworth Tee LC Low Pass Filter   -->

<!--script for Butterworth Tee LC High Pass Filter   -->

<script LANGUAGE="JavaScript">
<!--
var FM = 0;
var ZM = 0;
var CM = 0;
var LM = 0;

function clearBoxes(form)
{
document.forms.high.c.value = "";
document.forms.high.z.value = "";
document.forms.high.n.value = "";
document.forms.high.c.focus();
}

function MakeArray(j){
this.length = j;
var i;
for(i=1; i<=j; i++)
this[i] = "";
}

function solve6(form){
var i;
n = eval(document.forms.high.n.value);
c = eval(document.forms.high.c.value);
z = eval(document.forms.high.z.value);
if (n<1 || document.forms.high.n.value==""){
n = 1;
document.forms.high.n.value = n;
}
if (n>11){
n = 11;
document.forms.high.n.value = n;
}
if (c<1 || document.forms.high.c.value==""){
c = 1000;
document.forms.high.c.value = c;
}
if (z<=0 ||document.forms.high.z.value==""){
z = 50;
document.forms.high.z.value = z;
}
w = 2*Math.PI*c/1000;
var g = new MakeArray(12);
var s = new MakeArray(12);
var t = new MakeArray(12);
var x = new MakeArray(12);
var y = new MakeArray(12);
var Precision=7;
for(i=1; i<=n; i++){
k = (2*i-1)*Math.PI/(2*n);
g[i] = 2*Math.sin(k);
x[i] = 1000/(g[i]*z*w);
y[i] = z/(g[i]*w);
s[i] = x[i];
t[i] = y[i];
}
document.forms.high.x1.value = (x[1]).toPrecision(Precision);
document.forms.high.y2.value = (y[2]).toPrecision(Precision);
document.forms.high.x3.value = (x[3]).toPrecision(Precision);
document.forms.high.y4.value = (y[4]).toPrecision(Precision);
document.forms.high.x5.value = (x[5]).toPrecision(Precision);
document.forms.high.y6.value = (y[6]).toPrecision(Precision);
document.forms.high.x7.value = (x[7]).toPrecision(Precision);
document.forms.high.y8.value = (y[8]).toPrecision(Precision);
document.forms.high.x9.value = (x[9]).toPrecision(Precision);
document.forms.high.y10.value = (y[10]).toPrecision(Precision);
document.forms.high.x11.value = (x[11]).toPrecision(Precision);
document.forms.high.t1.value = (t[1]).toPrecision(Precision);
document.forms.high.s2.value = (s[2]).toPrecision(Precision);
document.forms.high.t3.value = (t[3]).toPrecision(Precision);
document.forms.high.s4.value = (s[4]).toPrecision(Precision);
document.forms.high.t5.value = (t[5]).toPrecision(Precision);
document.forms.high.s6.value = (s[6]).toPrecision(Precision);
document.forms.high.t7.value = (t[7]).toPrecision(Precision);
document.forms.high.s8.value = (s[8]).toPrecision(Precision);
document.forms.high.t9.value = (t[9]).toPrecision(Precision);
document.forms.high.s10.value = (s[10]).toPrecision(Precision);
document.forms.high.t11.value = (t[11]).toPrecision(Precision);
}

-->
</script>



<!--script for Butterworth Tee LC High Pass Filter   -->

<!--script for Chebyshev Tee LC Low Pass Filter  -->
<script LANGUAGE="JavaScript">
<!--
var FM = 0;
var ZM = 0;
var CM = 0;
var LM = 0;

function clearBoxes(form)
{
form.c.value = "";
form.r.value = "";
form.z.value = "";
form.n.value = "";
form.c.focus();
}

function MakeArray(j){
this.length = j;
var i;
for(i=1; i<=j; i++)
this[i] = "";
}

function solve7(f){
var i;
c = eval(f.c.value);
r = eval(f.r.value);
z = eval(f.z.value);
n = eval(f.n.value);
FM = eval(f.FM.value);
ZM = eval(f.ZM.value);
CM = eval(f.CM.value);
LM = eval(f.LM.value);
c = c*FM;
z = z*ZM;
if (n<1 || f.n.value==""){
n = 1;
f.n.value = n;
}
if (n>11){
n = 11;
f.n.value = n;
}
am = Math.round((n+0.1)/2)*2-1-n;
if (am<0){
n = n+1;
f.n.value = n;
}
if (c<1 || f.c.value==""){
c = 1000000000;
f.c.value = c/FM;
}
if (r<0.01 || f.r.value==""){
r = 0.01;
f.r.value = r;
}
if (z<=0 || f.z.value==""){
z = 50;
f.z.value = z/ZM;
}
w = 2*Math.PI*c;
rr = r/17.37;
e2x = Math.exp(2*rr);
coth = (e2x+1)/(e2x-1);
bt = Math.log(coth);
btn = bt/(2*n);
gn = (Math.exp(btn)-Math.exp(-btn))/2;
var a = new MakeArray(12);
var b = new MakeArray(12);
var g = new MakeArray(12);
var s = new MakeArray(12);
var t = new MakeArray(12);
var Precision=7;
for(i=1; i<=n; i++){
k = (2*i-1)*Math.PI/(2*n);
a[i] = Math.sin(k);
k2 = Math.PI*i/n;
k3 = Math.sin(k2);
b[i] = Math.pow(gn,2)+Math.pow(k3,2);
}
g[1] = 2*a[1]/gn;
for(i=2; i<=n; i++){
g[i] = (4*a[i-1]*a[i])/(b[i-1]*g[i-1]);
}
for(i=1; i<=n; i++){
s[i] = g[i]/(z*w);
t[i] = g[i]*z/w;
}
f.t1.value = (t[1]/LM).toPrecision(Precision);
f.s2.value = (s[2]/CM).toPrecision(Precision);
f.t3.value = (t[3]/LM).toPrecision(Precision);
f.s4.value = (s[4]/CM).toPrecision(Precision);
f.t5.value = (t[5]/LM).toPrecision(Precision);
f.s6.value = (s[6]/CM).toPrecision(Precision);
f.t7.value = (t[7]/LM).toPrecision(Precision);
f.s8.value = (s[8]/CM).toPrecision(Precision);
f.t9.value = (t[9]/LM).toPrecision(Precision);
f.s10.value = (s[10]/CM).toPrecision(Precision);
f.t11.value = (t[11]/LM).toPrecision(Precision);
}

-->
</script>

<!--script for Chebyshev Tee LC Low Pass Filter  -->

<!--script for Chebyshev Tee LC High Pass Filter  -->

<script LANGUAGE="JavaScript">
<!--
var FM = 0;
var ZM = 0;
var CM = 0;
var LM = 0;

function clearBoxes(form)
{
form.c.value = "";
form.r.value = "";
form.z.value = "";
form.n.value = "";
form.c.focus();
}

function MakeArray(j){
this.length = j;
var i;
for(i=1; i<=j; i++)
this[i] = "";
}
function solve8(f){
var i;
c = eval(f.c.value);
r = eval(f.r.value);
z = eval(f.z.value);
n = eval(f.n.value);
FM = eval(f.FM.value);
ZM = eval(f.ZM.value);
CM = eval(f.CM.value);
LM = eval(f.LM.value);
c = c*FM;
z = z*ZM;
if (n<1 || f.n.value==""){
n = 1;
f.n.value = n;
}
if (n>11){
n = 11;
f.n.value = n;
}
am = Math.round((n+0.1)/2)*2-1-n;
if (am<0){
n = n+1;
f.n.value = n;
}
if (c<1 || f.c.value==""){
c = 1000;
f.c.value = c/FM;
}
if (r<0.01 || f.r.value==""){
r = 0.01;
f.r.value = r;
}
if (z<=0 || f.z.value==""){
z = 50;
f.z.value = z/ZM;
}
w = 2*Math.PI*c;
rr = r/17.37;
e2x = Math.exp(2*rr);
coth = (e2x+1)/(e2x-1);
bt = Math.log(coth);
btn = bt/(2*n);
gn = (Math.exp(btn)-Math.exp(-btn))/2;
var a = new MakeArray(12);
var b = new MakeArray(12);
var g = new MakeArray(12);
var x = new MakeArray(12);
var y = new MakeArray(12);
var Precision=7;
for(i=1; i<=n; i++){
k = (2*i-1)*Math.PI/(2*n);
a[i] = Math.sin(k);
k2 = Math.PI*i/n;
k3 = Math.sin(k2);
b[i] = Math.pow(gn,2)+Math.pow(k3,2);
}
g[1] = 2*a[1]/gn;
for(i=2; i<=n; i++){
g[i] = (4*a[i-1]*a[i])/(b[i-1]*g[i-1]);
}
for(i=1; i<=n; i++){
x[i] = 1/(g[i]*z*w);
y[i] = z/(g[i]*w);
}
f.x1.value = (x[1]/CM).toPrecision(Precision);
f.y2.value = (y[2]/LM).toPrecision(Precision);
f.x3.value = (x[3]/CM).toPrecision(Precision);
f.y4.value = (y[4]/LM).toPrecision(Precision);
f.x5.value = (x[5]/CM).toPrecision(Precision);
f.y6.value = (y[6]/LM).toPrecision(Precision);
f.x7.value = (x[7]/CM).toPrecision(Precision);
f.y8.value = (y[8]/LM).toPrecision(Precision);
f.x9.value = (x[9]/CM).toPrecision(Precision);
f.y10.value = (y[10]/LM).toPrecision(Precision);
f.x11.value = (x[11]/CM).toPrecision(Precision);
}
-->
</script>



<!--script for Chebyshev Tee LC High Pass Filter  -->

<!--script for Equal Component Butterworth Low Pass Filter   -->
<script LANGUAGE="JavaScript">
<!--
var FM = 0;
var RM = 0;
var CM = 0;
var R3M = 0;
var R4M = 0;
var Precision=7;
function clearBoxes(form)
{
form.F.value = "";
form.R.value = "";
form.C.value = "";
form.R3.value = "";
form.R4.value = "";
form.F.focus();
}

function solve9(f){
F = eval(f.F.value);
R = eval(f.R.value);
C = eval(f.C.value);
R3 = eval(f.R3.value);
R4 = eval(f.R4.value);
FM = eval(f.FM.value);
RM = eval(f.RM.value);
CM = eval(f.CM.value);
R3M = eval(f.R3M.value);
R4M = eval(f.R4M.value);
F = F*FM;
R = R*RM;
C = C*CM;
R3 = R3*R3M;
R4 = R4*R4M;
if (f.F.value==""){
F = 1000;
f.F.value = (F/FM).toPrecision(Precision);
}
if (f.R.value==""){
if (f.C.value==""){
R = 10000;
f.R.value = (R/RM).toPrecision(Precision);
C = 0.159/(F*R);
f.C.value = (C/CM).toPrecision(Precision);

} else {f.C.value = (C/CM).toPrecision(Precision); R = 0.159/(F*C); f.R.value = (R/RM).toPrecision(Precision);}

} else {f.R.value = (R/RM).toPrecision(Precision); C = 0.159/(F*R); f.C.value = (C/CM).toPrecision(Precision);}

if (f.R3.value==""){
if (f.R4.value==""){
R3 = 10000;
f.R3.value = (R3/R3M).toPrecision(Precision);
R4 = 0.585*R3;
f.R4.value = (R4/R4M).toPrecision(Precision);

} else {f.R4.value = (R4/R4M).toPrecision(Precision); R3 = R4/0.585; f.R3.value = (R3/R3M).toPrecision(Precision);}

} else {f.R3.value = (R3/R3M).toPrecision(Precision); R4 = 0.585*R3; f.R4.value = (R4/R4M).toPrecision(Precision);}

}

-->
</script>
<!--script for Equal Component Butterworth Low Pass Filter   -->

<!--script for Equal Component Butterworth High Pass Filter   -->

<script LANGUAGE="JavaScript">
<!--
var FM = 0;
var RM = 0;
var CM = 0;
var R3M = 0;
var R4M = 0;
var Precision=7;
function clearBoxes(form)
{
form.F.value = "";
form.R.value = "";
form.C.value = "";
form.R3.value = "";
form.R4.value = "";
form.F.focus();
}

function solve10(f){
F = eval(f.F.value);
R = eval(f.R.value);
C = eval(f.C.value);
R3 = eval(f.R3.value);
R4 = eval(f.R4.value);
FM = eval(f.FM.value);
RM = eval(f.RM.value);
CM = eval(f.CM.value);
R3M = eval(f.R3M.value);
R4M = eval(f.R4M.value);
F = F*FM;
R = R*RM;
C = C*CM;
R3 = R3*R3M;
R4 = R4*R4M;
if (f.F.value==""){
F = 1000;
f.F.value = (F/FM).toPrecision(Precision);
}
if (f.R.value==""){
if (f.C.value==""){
R = 10000;
f.R.value = (R/RM).toPrecision(Precision);
C = 0.159/(F*R);
f.C.value = (C/CM).toPrecision(Precision);

} else {f.C.value = (C/CM).toPrecision(Precision); R = 0.159/(F*C); f.R.value = (R/RM).toPrecision(Precision);}

} else {f.R.value = (R/RM).toPrecision(Precision); C = 0.159/(F*R); f.C.value = (C/CM).toPrecision(Precision);}

if (f.R3.value==""){
if (f.R4.value==""){
R3 = 10000;
f.R3.value =(R3/R3M).toPrecision(Precision);
R4 = 0.585*R3;
f.R4.value = (R4/R4M).toPrecision(Precision);

} else {f.R4.value = (R4/R4M).toPrecision(Precision); R3 = R4/0.585; f.R3.value = (R3/R3M).toPrecision(Precision);}

} else {f.R3.value = (R3/R3M).toPrecision(Precision); R4 = 0.585*R3; f.R4.value = (R4/R4M).toPrecision(Precision);}

}

-->
</script>


<!--script for Equal Component Butterworth High Pass Filter   -->

<!--script for Sallen-Key Butterworth Low Pass Filter   -->

<script LANGUAGE="JavaScript">
<!--
var FM = 0;
var RM = 0;
var C1M = 0;
var C2M = 0;
var Precision=7;
function clearBoxes(form)
{
form.F.value = "";
form.R.value = "";
form.C1.value = "";
form.C2.value = "";
form.F.focus();
}

function solve11(f){
F = eval(f.F.value);
R = eval(f.R.value);
C1 = eval(f.C1.value);
C2 = eval(f.C2.value);
FM = eval(f.FM.value);
RM = eval(f.RM.value);
C1M = eval(f.C1M.value);
C2M = eval(f.C2M.value);
F = F*FM;
R = R*RM;
C1 = C1*C1M;
C2 = C2*C2M;
if (f.F.value==""){
F = 1000;
f.F.value = (F/FM).toPrecision(Precision);
}
if (f.R.value==""){
if (f.C1.value==""){
if (f.C2.value==""){
R = 10000;
f.R.value = (R/RM).toPrecision(Precision);
C2 = 0.1125/(F*R);
f.C2.value = (C2/C2M).toPrecision(Precision);
C1 = C2*2;
f.C1.value = (C1/C1M).toPrecision(Precision);
return;
} else {f.C2.value = (C2/C2M).toPrecision(Precision); C1 = C2*2; 
f.C1.value = (C1/C1M).toPrecision(Precision); R = 0.1125/(F*C2); f.R.value = (R/RM).toPrecision(Precision); return;}

} else {f.C1.value = (C1/C1M).toPrecision(Precision); C2 = C1/2;
 f.C2.value = (C2/C2M).toPrecision(Precision); R = 0.1125/(F*C2); f.R.value = (R/RM).toPrecision(Precision); return;}

} else {f.R.value = (R/RM).toPrecision(Precision); C2 = 0.1125/(F*R);
 f.C2.value = (C2/C2M).toPrecision(Precision); C1 = C2*2; f.C1.value = (C1/C1M).toPrecision(Precision); return;}

}

-->
</script>

<!--script for Sallen-Key Butterworth Low Pass Filter   -->
<!--script for Sallen-Key Butterworth High Pass Filter   -->
<script LANGUAGE="JavaScript">
<!--
var FM = 0;
var R1M = 0;
var R2M = 0;
var CM = 0;
var Precision=7;
function clearBoxes(form)
{
form.F.value = "";
form.R1.value = "";
form.R2.value = "";
form.C.value = "";
form.F.focus();
}

function solve12(f){
F = eval(f.F.value);
R1 = eval(f.R1.value);
R2 = eval(f.R2.value);
C = eval(f.C.value);
FM = eval(f.FM.value);
R1M = eval(f.R1M.value);
R2M = eval(f.R2M.value);
CM = eval(f.CM.value);
F = F*FM;
R1 = R1*R1M;
R2 = R2*R2M;
C = C*CM;
if (f.F.value==""){
F = 1000;
f.F.value = (F/FM).toPrecision(Precision);
}
if (f.R1.value==""){
if (f.R2.value==""){
if (f.C.value==""){
R1 = 10000;
f.R1.value = (R1/R1M).toPrecision(Precision);
R2 = R1*2;
f.R2.value = (R2/R2M).toPrecision(Precision);
C = 0.1125/(F*R1);
f.C.value = (C/CM).toPrecision(Precision);
return;
} else {f.C.value = (C/CM).toPrecision(Precision); R1 = 0.1125/(F*C); f.R1.value = (R1/R1M).toPrecision(Precision); 
R2 = R1*2; f.R2.value = (R2/R2M).toPrecision(Precision); return;}

} else {f.R2.value = (R2/R2M).toPrecision(Precision); R1 = R2/2; f.R1.value = (R1/R1M).toPrecision(Precision);
 C = 0.1125/(F*R1); f.C.value = (C/CM).toPrecision(Precision); return;}

} else {f.R1.value = (R1/R1M).toPrecision(Precision); R2 = R1*2; f.R2.value = (R2/R2M).toPrecision(Precision);
 C = 0.1125/(F*R1); f.C.value = (C/CM).toPrecision(Precision); return;}

}

-->
</script>

<!--script for Sallen-Key Butterworth High Pass Filter   -->
