<?php
$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include("header.php");  // read css and js folders, sets database variables
include("sidebar.php");

?>

<link rel="stylesheet" type="text/css" href="../css/sortstyle.css">

<style type="text/css" class="include" title="currentStyle">

    @import "DataTables-1.10.0/extensions/Editor-1.3.0/css/dataTables.editor.css";


    td.details-control {
        background: url('images/editpage.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('images/closefile.png') no-repeat center center;
    }
    TD {font-size:10px}


</style>
<link href="css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="css/dataTables.jqueryui.css" type="text/css" rel="stylesheet">
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/TableTools/js/dataTables.tableTools.js"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/TableTools/js/ZeroClipboard.js"></script>

<script src="js/jquery-1.11.1.min.js" language="javascript" type="text/javascript"></script> 
<script src="js/jquery.dataTablesonline.min.js" language="javascript" type="text/javascript"></script>
<script src="js/dataTables.jqueryui.js" language="javascript" type="text/javascript"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/Editor-1.3.0/js/dataTables.editor.js"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    $('#view').DataTable();
} );
</script>
</head>

		<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
  <div class="status_sec">
   <div class="statusbar"> <p>Current Page - <strong>Tools / Templates </strong></p></div>
	  <div class="returnstat"><a href="index.php" class="menu_click">Return</a></div>
  </div>
</div>


<div class="app_content">


<center>


<div class="upload_right1">

  
  <table cellpadding="0" cellspacing="0" id="view" class="display">
	<thead>	<tr>
			<th style="color:#000;">Filename</th>
			<th nowrap="nowrap" style="color:#000;">Description</th>
			<th style="color:#000;">Author</th>
			<th style="color:#000;">Tool</th>
			<th style="color:#000;">Download</th>
			</tr>
	</thead>
	<tbody>     
      <tr>
        <td>AltiumPCBProjectTemplate 2013-11-10</td>
       <td style="white-space: normal; text-align:justify;; text-align:justify;"><p align="justify">PCB Project Template is sample project template for Altium Designer with all necessary settings of source documents and settings of output generation process for 2 layer and 4 layer boards. It should make starting of a new project easier</td>
        <td>Petr Tosovsky</td><td>Altium</td><td><center><a href="<?php siteloklink('AltiumPCBProjectTemplate.zip:scripts',1); ?>"><img src="images/download_icon.png" /></a></center></td>
      </tr></tbody>
    </table>
  </div>

</div>
</div>
</div>
