<?php
$groupswithaccess="ladmin,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");


?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Library9.com</title>
<link href="styles.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript" src="../javascript/jquery-1.6.1.min.js"></script>
<!-- for navigation -->
<script type="text/javascript" src="../javascript/script-navigation.js"></script>
<!--[if IE 9 ]>
 <link href="../css/ie.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->

</head>
<body>
<?php include("header.php") ?>
<?php include("sidebar.php") ?>
		<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">

			<div class="statusbar"><p>Current Page - <strong>Admin Area / Library Management / Part Editor</strong></p></div>
			<div class="returnstat"><a href="library_mgt.php" class="menu_click">Return</a></div>

	</div>
</div>
<div class="app_content">
<div style="width:100%; float:left; text-align:center;">


	<?php if($slcustom1=='basic'){ if($slname=='Tim Add'){?>
	
	<table border="0" width="100%" cellpadding="2" cellspacing="2" class="index_table">  																<!-- start table with no border, 50 % of screen width, 2 pixels spaces between cell content and cell wall, 2 pixels between cells -->	
    <tr> 																																										<!-- start table row -->
        <td align="center" style=" padding-bottom:25px;">																																		<!-- start table cell, center cell -->
            <form name="001" action="view_resistor_tim.php?TBL=resistor" method="post">   								<!-- start form, open page with Resistors as passed TBL variable, pass (post) data method -->
                <input type="submit" value="RESISTOR" class="res_icon">																				<!-- create submit button with text  on the button as value -->
            </form>																																							<!-- close form -->
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="006" action="view_capacitor_tim.php?TBL=capacitor" method="post">  								<!-- User chooses Diodes to view/delete/add/edit -->
                <input type="submit" value="CAPACITOR" class="cap_icon">
            </form>
		</td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="010" action="view_misc_tim.php?TBL=misc" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="MISC" class="misc_icon">
            </form>
        </td>				
    </tr>																																										<!-- end table row -->

    <tr>
        <td align="center" style=" padding-bottom:25px;">
            <form name="011" action="view_connector_tim.php?TBL=connector" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="CONNECTOR" class="con_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="012" action="view_ic_tim.php?TBL=ic" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="IC" class="ic_icon">
            </form>
        </td>	
        <td align="center" style=" padding-bottom:25px;">
            <form name="013" action="view_diode_tim.php?TBL=diode" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="DIODE" class="diode_icon">
            </form>
        </td>			
    </tr>

    <tr>
        <td align="center" style=" padding-bottom:25px;">
            <form name="014" action="view_transistor_tim.php?TBL=transistor" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="TRANSISTOR" class="tran_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="015" action="view_oscillator_tim.php?TBL=oscillator" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="OSCILLATOR" class="osc_icon">
            </form>
        </td>	
        
		</td>			
    </tr></table>
	
	<?php }else{?>
<table border="0" width="100%" cellpadding="2" cellspacing="2" class="index_table">  																<!-- start table with no border, 50 % of screen width, 2 pixels spaces between cell content and cell wall, 2 pixels between cells -->	
    <tr> 																																										<!-- start table row -->
        <td align="center" style=" padding-bottom:25px;">																																		<!-- start table cell, center cell -->
            <form name="001" action="view_resistor.php?TBL=resistor" method="post">   								<!-- start form, open page with Resistors as passed TBL variable, pass (post) data method -->
                <input type="submit" value="RESISTOR" class="res_icon">																				<!-- create submit button with text  on the button as value -->
            </form>																																							<!-- close form -->
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="006" action="view_capacitor.php?TBL=capacitor" method="post">  								<!-- User chooses Diodes to view/delete/add/edit -->
                <input type="submit" value="CAPACITOR" class="cap_icon">
            </form>
		</td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="010" action="view_misc.php?TBL=misc" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="MISC" class="misc_icon">
            </form>
        </td>				
    </tr>																																										<!-- end table row -->

    <tr>
        <td align="center" style=" padding-bottom:25px;">
            <form name="011" action="view_connector.php?TBL=connector" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="CONNECTOR" class="con_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="012" action="view_ic.php?TBL=ic" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="IC" class="ic_icon">
            </form>
        </td>	
        <td align="center" style=" padding-bottom:25px;">
            <form name="013" action="view_diode.php?TBL=diode" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="DIODE" class="diode_icon">
            </form>
        </td>			
    </tr>

    <tr>
        <td align="center" style=" padding-bottom:25px;">
            <form name="014" action="view_transistor.php?TBL=transistor" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="TRANSISTOR" class="tran_icon">
            </form>
        </td>
        <td align="center" style=" padding-bottom:25px;">
            <form name="015" action="view_oscillator.php?TBL=oscillator" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="OSCILLATOR" class="osc_icon">
            </form>
        </td>	
        <td align="center" style=" padding-bottom:25px;">
            <form name="015" action="#" method="post">	
			<?php 
include("include.php"); 
		mysql_connect($host, $username, $password)or die("cannot connect");
        @mysql_select_db($slcustom15) or die( "Unable to select database");
          $resultn = mysql_query("SELECT autofill_value FROM footprint_autofill")or die(mysql_error()."update failed");
		  
		  while ($row=mysql_fetch_array($resultn)) 																																									
		{ 
         
		 $newvalue=$row["autofill_value"]; 
		
		}?>			<!-- User chooses MISC to view/delete/add/edit -->
                  <input type='checkbox' name="footprint_autofill" value="autofill" class='autofill_check'<?php if($newvalue=='autofill'){echo "checked";}?> >               
			   <input type="submit" value="FOOTPRINT AUTOFILL" class='autofill_icon' disabled>
            </form>
        </td>	
		</td>			
    </tr></table>
	<?php }}
	
	else{
	 ?> <table border="0" width="100%" cellpadding="2" cellspacing="2" class="inner_table" align="center">  		
	  <tr>
       <?php 
		include("include.php"); 
		mysql_connect($host, $username, $password)or die("cannot connect");
        @mysql_select_db($slcustom15) or die( "Unable to select database");
          $result1 = mysql_query("SELECT cat_name FROM categories")or die(mysql_error()."update failed");
		  $i=0;

        while ($row=mysql_fetch_array($result1)) 																																									
		{ 
         
		$name1=$row["cat_name"]; 
		$name = str_replace('_', ' ', $name1);  
		if($i == 0){
        echo"<TR>";
    } ?>																																																																														
		 																												
	    <td align="center" style=" padding-bottom:25px;">
		<form name="015" action="custom_editor.php?TBL=<?php echo $name1;?>" method="post">		 									  <!-- User chooses MISC to view/delete/add/edit -->
                <input type="submit" value="<?php echo $name;?>" class="l9_icon">
			  </form> </td><?php   $i++;
			  if($i == 3)
    {
        $i = 0;
        echo"</tr>";
    }}?>  			
		
	 
	  <?php }?>
	  
</table>







																																									  <!-- end table -->
</div>
</div>
</div>
</div>
   <script>
$( document ).ready(function() { 
$(".autofill_check").click(function() {
	if($(".autofill_check").is(':checked'))
{
	var value='autofill';	
} 
else {
	var value='manual'; 	
	}
	
 
          var req=  $.ajax({
                url : 'footprint_autofill.php',
                type : 'POST',
                async: false,
                data : { 'value' : value},
                success : function(result){
		alert("Settings for footprint autofill saved sucessfully.");
             },
             error : function(e){
             console.log(e);
            }
             });
    
});


 });
 </script>
     
