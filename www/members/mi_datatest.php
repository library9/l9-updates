<?php
$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
session_start();
$table=$_SESSION['table'];



//// * Example PHP implementation used for the index.html example
// 
//
//// DataTables PHP library
include( "DataTables-1.10.0/extensions/Editor-1.3.0/php/DataTables.php" );
//
//// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Join,
    DataTables\Editor\Validate;
//
//// Build our Editor instance and process the data coming from _POST

if($table=='resistor'){
Editor::inst( $db,'resistor','ITEM' )
    ->fields(
	  
        Field::inst( 'resistor.Part_Number' ),
		Field::inst( 'resistor.CPN' ),
		Field::inst( 'resistor.Library_Ref'),
	    Field::inst( 'resistor.Description' ),
	    Field::inst( 'resistor.Manufacturer' ),
		Field::inst( 'resistor.Manufacturer_Part_Number'),
		Field::inst( 'resistor.PrePN' ),
		Field::inst( 'resistor.PostPN' ),
		Field::inst( 'resistor.Library_Path'),
		Field::inst( 'resistor.Category'),
	    Field::inst( 'resistor.Comment' ),
	    Field::inst( 'resistor.Component_Kind' ),
		Field::inst( 'resistor.Component_Type'),
		Field::inst( 'resistor.TEMPCO'),  
		Field::inst( 'resistor.Designator' ),
		Field::inst( 'resistor.Footprint' ),		
		Field::inst( 'resistor.Manufacturer1'),
	    Field::inst( 'resistor.Manufacturer_Part_Number1' ),
	    Field::inst( 'resistor.Packaging' ),
		Field::inst( 'resistor.Pin_Count'),
		Field::inst( 'resistor.Signal_Integrity' ),
		Field::inst( 'resistor.Simulation' ),
		Field::inst( 'resistor.Supplier'),
		Field::inst( 'resistor.Supplier_Part_Number'),
	    Field::inst( 'resistor.Tolerance' ),
	    Field::inst( 'resistor.Footprint_Path' ),
		Field::inst( 'resistor.Footprint_Ref' ),
		Field::inst( 'resistor.Value'),
		Field::inst( 'resistor.Height'),  
		Field::inst( 'resistor.Size_Dimension' ),
		Field::inst( 'resistor.Composition' ),
		Field::inst( 'resistor.Number_of_Terminations'),
	    Field::inst( 'resistor.Power_Watts' ),
	    Field::inst( 'resistor.Resistance_Ohms' ),
		Field::inst( 'resistor.Supplier_Part_Number1'),
		Field::inst( 'resistor.Supplier1' ),
		Field::inst( 'resistor.company_id' ),	
		 Field::inst( 'resistor.part_img' ),
		Field::inst( 'resistor.sym_img'),
		Field::inst( 'resistor.pcb_img' ),
		Field::inst( 'resistor.threeD_img' ),		
		Field::inst( 'resistor.ComponentLink1Description' ),
	 Field::inst( 'resistor.ComponentLink1URL' ),
	  Field::inst( 'resistor.revision' ),
	  Field::inst( 'resistor.date' ),

Field::inst( 'resistor.status')	 
	  
   
    )
		

    ->process( $_POST )
    ->json();
}
else if($table=='capacitor'){
Editor::inst( $db,'capacitor','ITEM' )
   ->fields(
	   
       Field::inst( 'capacitor.Part_Number' ),
		Field::inst( 'capacitor.CPN' ),
		Field::inst( 'capacitor.PrePN' ),
		Field::inst( 'capacitor.PostPN' ),
		Field::inst( 'capacitor.Library_Ref'),
		Field::inst( 'capacitor.Library_Path'),
		Field::inst( 'capacitor.Category'),
	    Field::inst( 'capacitor.Comment' ),
	    Field::inst( 'capacitor.Component_Kind' ),
		Field::inst( 'capacitor.Component_Type'),
	    Field::inst( 'capacitor.Description' ),
		Field::inst( 'capacitor.Designator' ),
		Field::inst( 'capacitor.Footprint' ),
	    Field::inst( 'capacitor.Manufacturer' ),
		Field::inst( 'capacitor.Manufacturer_Part_Number'),
		Field::inst( 'capacitor.Manufacturer1'),
	    Field::inst( 'capacitor.Manufacturer_Part_Number1' ),
		Field::inst( 'capacitor.Operating_Temperature'),  
		Field::inst( 'capacitor.Package_Case'),  
	    Field::inst( 'capacitor.Packaging' ),
		Field::inst( 'capacitor.Pin_Count'),		
		Field::inst( 'capacitor.Signal_Integrity' ),
		Field::inst( 'capacitor.Simulation' ),
		Field::inst( 'capacitor.Supplier'),
		Field::inst( 'capacitor.Supplier_Part_Number'),
		Field::inst( 'capacitor.Supplier_Part_Number1'),
		Field::inst( 'capacitor.Supplier1' ),
	    Field::inst( 'capacitor.Tolerance' ),
	    Field::inst( 'capacitor.Footprint_Path' ),
		 Field::inst( 'capacitor.Footprint_Ref' ),
		Field::inst( 'capacitor.Value'),
		Field::inst( 'capacitor.Capacitance'),  
		Field::inst( 'capacitor.Size_Dimension' ),
		Field::inst( 'capacitor.Thickness_Max' ),
		Field::inst( 'capacitor.Voltage_Rated'),
	    Field::inst( 'capacitor.Height' ),
	    Field::inst( 'capacitor.Lead_Spacing' ),		
		Field::inst( 'capacitor.ComponentLink1Description' ),
	 Field::inst( 'capacitor.ComponentLink1URL' ),
	 Field::inst( 'capacitor.company_id' ),
	 Field::inst( 'capacitor.revision' ),
	 Field::inst( 'capacitor.date' ),
	  Field::inst( 'capacitor.part_img' ),
		Field::inst( 'capacitor.sym_img'),
		Field::inst( 'capacitor.pcb_img' ),
		Field::inst( 'capacitor.threeD_img' ),

Field::inst( 'capacitor.status')	 
	  
   
    )
     
    ->process( $_POST )
    ->json();
}
else if($table=='misc'){
Editor::inst( $db,'misc','ITEM' )
     ->fields(
	   Field::inst( 'misc.Part_Number' ),
		Field::inst( 'misc.CPN' ),		
		Field::inst( 'misc.Library_Ref'),		
		Field::inst( 'misc.Library_Path'),
		Field::inst( 'misc.Category'),
	    Field::inst( 'misc.Comment' ),
	    Field::inst( 'misc.Component_Kind' ),
		Field::inst( 'misc.Component_Type'),
	    Field::inst( 'misc.Description' ),
		Field::inst( 'misc.Designator' ),
		Field::inst( 'misc.Footprint' ),
	    Field::inst( 'misc.Manufacturer' ),
		Field::inst( 'misc.Manufacturer_Part_Number'),
		Field::inst( 'misc.Manufacturer1'),
	    Field::inst( 'misc.Manufacture_Part_Number1' ),
		Field::inst( 'misc.Mounting_Type'), 
	    Field::inst( 'misc.Operating_Temperature'),  
		Field::inst( 'misc.Package_Case'), 	
	    Field::inst( 'misc.Packaging' ),
		Field::inst( 'misc.Pin_Count'),
		Field::inst( 'misc.RoHS' ),	
		Field::inst( 'misc.Signal_Integrity' ),
		Field::inst( 'misc.Simulation' ),
		Field::inst( 'misc.Supplier'),
		Field::inst( 'misc.Supplier_Part_Number'),
		Field::inst( 'misc.Supplier_Part_Number1'),
		Field::inst( 'misc.Supplier1' ),	  
	    Field::inst( 'misc.Footprint_Path' ),
		 Field::inst( 'misc.Footprint_Ref' ),
	     Field::inst( 'misc.Height' ),	
		 Field::inst( 'misc.ComponentLink1Description' ),
	     Field::inst( 'misc.ComponentLink1URL' ),
		 Field::inst( 'misc.company_id' ),
		Field::inst( 'misc.revision' ),
		Field::inst( 'misc.date' ),
		Field::inst( 'misc.part_img' ),
		Field::inst( 'misc.sym_img'),
		Field::inst( 'misc.pcb_img' ),
		Field::inst( 'misc.threeD_img' ),
	
         Field::inst( 'misc.status' )	 
   
    )
	     

    ->process( $_POST )
    ->json();
}
else if($table=='connector'){
Editor::inst( $db,'connector','ITEM' )
    ->fields(
	  
    Field::inst( 'connector.Part_Number' ),
		Field::inst( 'connector.CPN' ),		
		Field::inst( 'connector.Library_Ref'),		
		Field::inst( 'connector.Library_Path'),
		Field::inst( 'connector.Category'),
	    Field::inst( 'connector.Comment' ),
	    Field::inst( 'connector.Component_Kind' ),
		Field::inst( 'connector.Component_Type'),
	    Field::inst( 'connector.Description' ),
		Field::inst( 'connector.Designator' ),
		Field::inst( 'connector.Footprint' ),
	    Field::inst( 'connector.Manufacturer' ),
		Field::inst( 'connector.Manufacturer_Part_Number'),
		Field::inst( 'connector.Manufacturer1'),
	    Field::inst( 'connector.Manufacturer_Part_Number1' ),
		Field::inst( 'connector.Mounting_Type'), 
		Field::inst( 'connector.Package_Case'),  
	    Field::inst( 'connector.Packaging' ),
		Field::inst( 'connector.Pin_Count'),
		Field::inst( 'connector.Signal_Integrity' ),
		Field::inst( 'connector.Simulation' ),
		Field::inst( 'connector.Supplier'),
		Field::inst( 'connector.Supplier_Part_Number'),
		Field::inst( 'connector.Supplier_Part_Number1'),
		Field::inst( 'connector.Supplier1' ),	  
	    Field::inst( 'connector.Footprint_Path' ),
	    Field::inst( 'connector.Footprint_Ref' ),
		Field::inst( 'connector.Height' ),
		Field::inst( 'connector.Value'),		
		Field::inst( 'connector.ComponentLink1Description' ),
	    Field::inst( 'connector.ComponentLink1URL' ),
		Field::inst( 'connector.company_id' ),
	    Field::inst( 'connector.revision' ),
	    Field::inst( 'connector.date' ),
		Field::inst( 'connector.part_img' ),
		Field::inst( 'connector.sym_img'),
		Field::inst( 'connector.pcb_img' ),
		Field::inst( 'connector.threeD_img' ),
	
		Field::inst( 'connector.status' )
	  
   
    )

    ->process( $_POST )
    ->json();
}
else if($table=='ic'){
Editor::inst( $db,'ic','ITEM' )
     ->fields(
	 
      Field::inst( 'ic.Part_Number' ),
		Field::inst( 'ic.CPN' ),
		Field::inst( 'ic.PrePN' ),
		Field::inst( 'ic.PostPN' ),
		Field::inst( 'ic.Library_Ref'),
		Field::inst( 'ic.Value'),
		Field::inst( 'ic.Library_Path'),
		Field::inst( 'ic.Category'),
	    Field::inst( 'ic.Comment' ),
	    Field::inst( 'ic.Component_Kind' ),
		Field::inst( 'ic.Component_Type'),
	    Field::inst( 'ic.Description' ),
		Field::inst( 'ic.Designator' ),
		Field::inst( 'ic.Footprint' ),
	    Field::inst( 'ic.Manufacturer' ),
		Field::inst( 'ic.Manufacturer_Part_Number'),
		Field::inst( 'ic.Manufacturer1'),
	    Field::inst( 'ic.Manufacturer_Part_Number1' ),
		Field::inst( 'ic.Mounting_Type'),  
	    Field::inst( 'ic.Packaging' ),
		Field::inst( 'ic.Pin_Count'),		
		Field::inst( 'ic.Signal_Integrity' ),
		Field::inst( 'ic.Simulation' ),
		Field::inst( 'ic.Supplier'),
		Field::inst( 'ic.Supplier_Part_Number'),
		Field::inst( 'ic.Supplier_Part_Number1'),
		Field::inst( 'ic.Supplier1' ),	  
	    Field::inst( 'ic.Footprint_Path' ),
		Field::inst( 'ic.Footprint_Ref' ),
		Field::inst( 'ic.Type' ),		
		Field::inst( 'ic.Supplier_Device_Package'),  
		Field::inst( 'ic.Minimum_Orde_Quantity' ),
		Field::inst( 'ic.Reeling' ),
		Field::inst( 'ic.Product_Type'),
	    Field::inst( 'ic.Mounting_Style' ),	   		
		Field::inst( 'ic.ComponentLink1Description' ),
	    Field::inst( 'ic.ComponentLink1URL' ),
	    Field::inst( 'ic.company_id' ),
	    Field::inst( 'ic.revision' ),
	    Field::inst( 'ic.date' ),
	     Field::inst( 'ic.part_img' ),
		Field::inst( 'ic.sym_img'),
		Field::inst( 'ic.pcb_img' ),
		Field::inst( 'ic.threeD_img' ),

Field::inst( 'ic.status' )	 
   
    )
    ->process( $_POST )
    ->json();
}
else if($table=='diode'){
Editor::inst( $db,'diode','ITEM' )
     ->fields(
	   
       Field::inst( 'diode.Part_Number' ),
		Field::inst( 'diode.CPN' ),		
		Field::inst( 'diode.Library_Ref'),		
		Field::inst( 'diode.Library_Path'),
		Field::inst( 'diode.Category'),
	    Field::inst( 'diode.Comment' ),
	    Field::inst( 'diode.Component_Kind' ),
		Field::inst( 'diode.Component_Type'),
	    Field::inst( 'diode.Description' ),
		Field::inst( 'diode.Designator' ),
		Field::inst( 'diode.Footprint' ),
	    Field::inst( 'diode.Manufacturer' ),
		Field::inst( 'diode.Impedance_Max_Zzt' ),
		Field::inst( 'diode.Manufacturer_Part_Number'),
		Field::inst( 'diode.Manufacturer1'),
	    Field::inst( 'diode.Manufacturer_Part_Number1' ),
		Field::inst( 'diode.Mounting_Type'), 
		Field::inst( 'diode.Supplier'),
		Field::inst( 'diode.Supplier_Part_Number'),
		Field::inst( 'diode.Supplier_Part_Number1'),
		Field::inst( 'diode.Supplier1' ),	
	     Field::inst( 'diode.Package_Case'),  
	    Field::inst( 'diode.Packaging' ),
		Field::inst( 'diode.Pin_Count'),
	    Field::inst( 'diode.RoHS'),
		Field::inst( 'diode.Signal_Integrity' ),
		Field::inst( 'diode.Simulation' ),
	    Field::inst( 'diode.Tolerance' ),
	    Field::inst( 'diode.Footprint_Path' ),
		Field::inst( 'diode.Footprint_Ref' ),
		Field::inst( 'diode.Reverse_Recovery_Time_trr' ),
	    Field::inst( 'diode.Size_Dimension' ),
	    Field::inst( 'diode.Voltage_Reverse' ),
		Field::inst( 'diode.ESR_Equivalent_Series_Resistance' ),
		Field::inst( 'diode.Capacitance' ),
		Field::inst( 'diode.Height' ),
		Field::inst( 'diode.Value'),		
		Field::inst( 'diode.ComponentLink1Description' ),
	    Field::inst( 'diode.ComponentLink1URL' ),
		 Field::inst( 'diode.company_id' ),
		Field::inst( 'diode.revision' ),
		Field::inst( 'diode.date' ),
		 Field::inst( 'diode.part_img' ),
		Field::inst( 'diode.sym_img'),
		Field::inst( 'diode.pcb_img' ),
		Field::inst( 'diode.threeD_img' ),
	
Field::inst( 'diode.status' )		
		
		       
	  
   
    )
	
    ->process( $_POST )
    ->json();
}
else if($table=='transistor'){
Editor::inst( $db,'transistor','ITEM' )
    ->fields(
	   
      Field::inst( 'transistor.Part_Number' ),
		Field::inst( 'transistor.CPN' ),		
		Field::inst( 'transistor.Library_Ref'),		
		Field::inst( 'transistor.Library_Path'),
		Field::inst( 'transistor.Category'),
	    Field::inst( 'transistor.Comment' ),
	    Field::inst( 'transistor.Component_Kind' ),
		Field::inst( 'transistor.Component_Type'),
	    Field::inst( 'transistor.Description' ),
		Field::inst( 'transistor.Designator' ),
		Field::inst( 'transistor.Footprint' ),
		Field::inst( 'transistor.Impedance_Max_Zzt' ),
	    Field::inst( 'transistor.Manufacturer' ),
		Field::inst( 'transistor.Manufacturer_Part_Number'),
		Field::inst( 'transistor.Manufacturer1'),
	    Field::inst( 'transistor.Manufacturer_Part_Number1' ),
		Field::inst( 'transistor.Mounting_Type'), 
	    Field::inst( 'transistor.Packaging' ),
		Field::inst( 'transistor.Pin_Count'),
		Field::inst( 'transistor.RoHS'),  
		Field::inst( 'transistor.Signal_Integrity' ),
		Field::inst( 'transistor.Simulation' ),
		Field::inst( 'transistor.Supplier'),
		Field::inst( 'transistor.Supplier_Part_Number'),
		Field::inst( 'transistor.Supplier_Part_Number1'),
		Field::inst( 'transistor.Supplier1' ),	  
	    Field::inst( 'transistor.Footprint_Path' ),
	    Field::inst( 'transistor.Footprint_Ref' ),
		Field::inst( 'transistor.Height' ),
		Field::inst( 'transistor.Value'),		
		Field::inst( 'transistor.ComponentLink1Description' ),
	    Field::inst( 'transistor.ComponentLink1URL' ),
		Field::inst( 'transistor.company_id' ),
		Field::inst( 'transistor.revision' ),
		Field::inst( 'transistor.date' ),
		 Field::inst( 'transistor.part_img' ),
		Field::inst( 'transistor.sym_img'),
		Field::inst( 'transistor.pcb_img' ),
		Field::inst( 'transistor.threeD_img' ),
		
        Field::inst( 'transistor.status' )		
    

		       
   
    )
	
    ->process( $_POST )
    ->json();
}
else if($table=='oscillator'){
Editor::inst( $db,'oscillator','ITEM' )
   ->fields(
	   
        Field::inst( 'oscillator.Part_Number' ),
		Field::inst( 'oscillator.CPN' ),		
		Field::inst( 'oscillator.Library_Ref'),		
		Field::inst( 'oscillator.Library_Path'),
		Field::inst( 'oscillator.Category'),
	    Field::inst( 'oscillator.Comment' ),
	    Field::inst( 'oscillator.Component_Kind' ),
		Field::inst( 'oscillator.Component_Type'),
	    Field::inst( 'oscillator.Description' ),
		Field::inst( 'oscillator.Designator' ),
		Field::inst( 'oscillator.Footprint' ),
	    Field::inst( 'oscillator.Manufacturer' ),
		Field::inst( 'oscillator.Manufacturer_Part_Number'),
		Field::inst( 'oscillator.Manufacturer1'),
	    Field::inst( 'oscillator.Manufacturer_Part_Number1' ),
		Field::inst( 'oscillator.Mounting_Type'),
		Field::inst( 'oscillator.Operating_Temperature'),
	    Field::inst( 'oscillator.Package_Case'),  
        Field::inst( 'oscillator.Packaging' ),
		Field::inst( 'oscillator.Pin_Count'),
		Field::inst( 'oscillator.Signal_Integrity' ),
		Field::inst( 'oscillator.Simulation' ),
		Field::inst( 'oscillator.Supplier'),
		Field::inst( 'oscillator.Supplier_Part_Number'),
		Field::inst( 'oscillator.Supplier_Part_Number1'),
		Field::inst( 'oscillator.Supplier1' ),	  
	    Field::inst( 'oscillator.Footprint_Path' ),
	    Field::inst( 'oscillator.Footprint_Ref' ),
		Field::inst( 'oscillator.Height' ),
		Field::inst( 'oscillator.ComponentLink1Description' ),
	    Field::inst( 'oscillator.ComponentLink1URL' ),
		Field::inst( 'oscillator.company_id' ),	
        Field::inst( 'oscillator.revision' ),	
        Field::inst( 'oscillator.date' ),	
        Field::inst( 'oscillator.part_img' ),
		Field::inst( 'oscillator.sym_img'),
		Field::inst( 'oscillator.pcb_img' ),
		Field::inst( 'oscillator.threeD_img' ),	
    		
        Field::inst( 'oscillator.status' )
	  
   
    )
	
    ->process( $_POST )
    ->json();
}



