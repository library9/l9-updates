<?php 

$groupswithaccess="ladmin,luser,leditor";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>
 <link type="text/css" rel="stylesheet" href="css/easy-responsive-tabs.css" />
		<div class="side_work">	
			<div class="working_area">
	<div class="status_panel">
		<div class="status_sec">
	
				<div class="statusbar"><p>Current Page - <strong>Calculators</strong></p></div>
				<div class="returnstat"><a href="index.php" class="menu_click">Return</a></div>
	
		</div>
	</div>
<div class="app_calculator">

   	<div class="calc_buttons">
		<h3>CALCULATOR</h3>
		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
		<center>
		<table  cellpadding="0" cellspacing="0" border="0" width="80%">
			<tr>
				<td><a href="capacitance.php">Capacitance</a></td>
				<td><a href="ohm.php">Ohm's Law</a></td>
				<td><a href="pcbconrol.php">PCB Control</a></td>
				<td><a href="impedance.php">Impedance Control</a></td>
			</tr>
						<tr>
				<td><a href="#">Capacitance</a></td>
				<td><a href="#">Ohm's Law</a></td>
				<td><a href="#">PCB Contro</a></td>
				<td><a href="#">Impedance Control</a></td>
			</tr>
			<tr>
				<td><a href="#">Capacitance</a></td>
				<td><a href="#">Ohm's Law</a></td>
				<td><a href="#">PCB Contro</a></td>
				<td><a href="#">Impedance Control</a></td>
			</tr>
						<tr>
				<td><a href="#">Capacitance</a></td>
				<td><a href="#">Ohm's Law</a></td>
				<td><a href="#">PCB Contro</a></td>
				<td><a href="#">Impedance Control</a></td>
			</tr>
			
			<tr>
				<td><a href="#">Capacitance</a></td>
				<td><a href="#">Ohm's Law</a></td>
				<td><a href="#">PCB Contro</a></td>
				<td><a href="#">Impedance Control</a></td>
			</tr>
						<tr>
				<td><a href="#">Capacitance</a></td>
				<td><a href="#">Ohm's Law</a></td>
				<td><a href="#">PCB Contro</a></td>
				<td><a href="#">Impedance Control</a></td>
			</tr>
			<tr>
				<td><a href="#">Capacitance</a></td>
				<td><a href="#">Ohm's Law</a></td>
				<td><a href="#">PCB Contro</a></td>
				<td><a href="#">Impedance Control</a></td>
			</tr>
						<tr>
				<td><a href="#">Capacitance</a></td>
				<td><a href="#">Ohm's Law</a></td>
				<td><a href="#">PCB Contro</a></td>
				<td><a href="#">Impedance Control</a></td>
			</tr>
												
			
		
		</table>
		</center>
		
	
	</div>
            
<!--<div id="body-container1">
                  <div class="glossary-container">
   					 <ul class="firstUL1">
						<li id="a" class="selected">Capacitance</li>
						<li id="b">Ohm's Law</li>
						<li id="c">PCB Thermal</li>
						<li id="d">PCB Trace</li>	
						<li id="e">PCB Via</li>
						<li id="f">Microstrip impedance </li>
						<li id="g">Edge coupled microstrip impedance</li>
						<li id="h">Embedded microstrip impedance</li>
						<li id="i">Symmetric stripline impedance</li>	
						<li id="j">Asymmetric stripline impedance</li>
						<li id="k">Wire microstrip impedance </li>
						<li id="l">Wire stripline impedance</li>
						<li id="m">Edge coupled stripline impedance</li>
						<li id="n">Broadside coupled stripline impedance</li>	
						
					</ul>
				 </div>	
  <div class="content-container">

    <div id="content-for-a" style="background-color:#e9e9e9">
	                    <div id="capacitor-calc-container"></div>

                 <script>
                           (function() {
                             var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
                             calc.src = 'js/calc-capacitor.js';
                              (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
                            })();
                      </script>
     
      <p class="return-to-top">Return to Top</p>
    </div>
	<div id="content-for-b">
                   <div id="ohms-law"></div>
				<script>
					(function() {
						var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
						calc.src = 'js/calc-ohmslaw.js';
						(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
					})();
				</script>
     
      <p class="return-to-top">Return to Top</p>
    </div>
		<div id="content-for-c">
	             <div id="pcb-thermal-calc-container"></div>
							<script>
								(function() {
									var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
									calc.src = 'js/calc-pcbthermal.js';
									(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
								})();
							</script>
     
      <p class="return-to-top">Return to Top</p>
    </div>
	
	<div id="content-for-d">
						<div id="pcb-trace-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calc-pcbtrace.js';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script> 
     
      <p class="return-to-top">Return to Top</p>
    </div>
	
	<div id="content-for-e">
                    <div id="pcb-via-calc-container"></div>

                  <script>
                          (function() {
                           var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
                          calc.src = 'js/calc-pcbvia.js';
                           (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
                         })();
                  </script>
     
      <p class="return-to-top">Return to Top</p>
    </div>
	<div id="content-for-f" >
			<div id="microstrip-impedance-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calcs.js?calc=one';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script>
     
      <p class="return-to-top">Return to Top</p>
    </div>	
	<div id="content-for-g">

				<div id="edge-coupled-microstrip-impedance-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calcs1.js?calc=seven';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script>
     
      <p class="return-to-top">Return to Top</p>
    </div>
	<div id="content-for-h">

				<div id="embedded-microstrip-impedance-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calcs2.js?calc=two';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script>
     
      <p class="return-to-top">Return to Top</p>
    </div>	
	<div id="content-for-i" >
			<div id="symmetric-stripline-impedance-calc-container"></div>
							<script>
								(function() {
									var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
									calc.src = 'js/calcs3.js?calc=three';
									(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
								})();
							</script>
     
      <p class="return-to-top">Return to Top</p>
    </div>	
	<div id="content-for-j">
				<div id="asymmetric-stripline-impedance-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calcs4.js?calc=four';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script>
     
      <p class="return-to-top">Return to Top</p>
    </div>	
	<div id="content-for-k">

		
				<div id="wire-microstrip-impedance-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calcs5.js?calc=five';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script>
      <p class="return-to-top">Return to Top</p>
    </div>	
	<div id="content-for-l" >

				<div id="wire-stripline-impedance-calc-container"></div>
						<script>
							(function() {
								var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
								calc.src = 'js/calcs6.js?calc=six';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
							})();
						</script>
      <p class="return-to-top">Return to Top</p>
    </div>				
	<div id="content-for-m">
				<div id="edge-coupled-stripline-impedance-calc-container"></div>
					<script>
						(function() {
							var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
							calc.src = 'js/calcs7.js?calc=eight';
							(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
						})();
					</script>
      <p class="return-to-top">Return to Top</p>
    </div>		
	<div id="content-for-n">
	
				<div id="broadside-coupled-stripline-impedance-calc-container"></div>
					<script>
						(function() {
							var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
							calc.src = 'js/calcs8.js?calc=nine';
							(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
						})();
					</script>
      <p class="return-to-top">Return to Top</p>
    </div>				
		
					 
    
      

				
			
        </div>
  
</div>-->

    </div>
</div>
</div>
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL1 li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL1 li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#e9e9e9'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL1 li").each(function() { 
			$(".firstUL1 li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#e9e9e9'}); //set the background color to light-blue to that div
	 });
});
</script>