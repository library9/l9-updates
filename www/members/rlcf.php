<?php 

$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="loggedin_index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>
 <link type="text/css" rel="stylesheet" href="css/easy-responsive-tabs.css" />
		<div class="side_work">	
			<div class="working_area">
	<div class="status_panel">
		<div class="status_sec">
	
				<div class="statusbar"><p>Current Page - <strong>Calculators / RLCF</strong></p></div>
				<div class="returnstat"><a href="calculator.php" class="menu_click">Return</a></div>
	
		</div>
	</div>
<div class="app_calculator">

  <div class="calc_buttons">
  	
  </div> 
            
<div id="body-container1">
                  <div class="glossary-container rlcf_calc">
   					 <ul class="firstUL1">
						<li id="a" class="selected">Capacitance and Inductance from Reactance</li>
						<li id="b">Inductive Reactance</li>
						<li id="c">Capacitive Reactance</li>
						<li id="d">Capacitance-Frequency-Inductance</li>
						<li id="e">Resistance-Frequency-Capacitance</li>
						<li id="f">Frequency and Wavelength</li>												
						<li id="g">Inductance(Electrode/Straight Wire)</li>	
						
					</ul>
				 </div>	
  <div class="content-container">

    <div id="content-for-a" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">
			<div class="calc_left">
					<FORM name="form1">
				<p  align="center"><font size="2" face="verdana"
					color="#1f6fa2"><b><span>Calculate Capacitance and Inductance from Reactance</span></b></font></p>  
					
					<p align="center"><img src="images/reactance L form.bmp" border="0"><br>
					<img src="images/reactance C form.bmp" border="0"></p>  
			<center>
				 <table align="center" style="margin-top:.3cm" class="rclf_tab">
					<tr><th colspan="3">Enter your values:</th></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
			
					<tr>
							  <td align="right"><font size="2" face="verdana"
					color="#1f6fa2"><b style="margin-left:.5cm">Frequency: </b></font> </td>
							  <td align="left">  <input type="text" NAME="Fx" size="7" maxlength="15" >
						 <font size="2" face="verdana"
					color="#1f6fa2"><b> Hz</b></font> </td></tr>
						  
					<tr>
							  <td align="right"><font size="2" face="verdana"
					color="#1f6fa2"><b style="margin-left:.5cm">Reactance: </b></font> </td>
							 <td align="left">   <input type="text" NAME="Xx" size="7" maxlength="15">
						 <font size="2" face="verdana"
					color="#1f6fa2"><b>   ohms</b></font> </td></tr>

						  
					<tr>
						<td align="right"><input type="Button" value="Calculate"  onClick="calcLC();" class="calculate_cal" ></td>
						<td align="left"><input type="reset" value="Clear"  class="clear_cal"></td>
				  
					  </tr>
		<tr><td colspan="3" style="height:15px;"></td></tr>
						  
					<tr>
					<th colspan="3">Results:</th></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
					
					<tr>
							  <td align="right"><font size="2" face="verdana"
					color="#1f6fa2"><b style="margin-left:.5cm">Inductance:</b></font> </td>
								<td align="left"> <input type="text" NAME="ind" size="7" maxlength="15" readonly="readonly">
					   <font size="2" face="verdana"
					color="#1f6fa2"><b>  mH </b></font> </td></tr>
						
								<tr>
							  <td align="right"><font size="2" face="verdana"
					color="#1f6fa2"><b style="margin-left:.5cm">Capacitance:</b></font> </td>
							   <td align="left">  <input type="text" NAME="cap" size="7" maxlength="15" readonly="readonly">
					   <font size="2" face="verdana"
					color="#1f6fa2"><b>  &micro;f </b></font> </td></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
					</table>
				</center>	
				  </form>			
													

			</div>
			<div class="calc_right">
				<h5>Capacitance and Inductance from Reactance</h5>
				<p>Reactance arises from the presence of inductance and capacitance within a circuit. It is generally denoted by symbol X and its SI unit is Ohm.</p>
				<p>If X>0, the reactance is said to be inductive.</p>
				<p>If X<0, the reactance is said to be capacitive.</p>
				<p>Inductive reactance XL is proportional to the frequency and the inductance.</p>
				<p>Capacitive reactance XC is inversely proportional to the frequency and the capacitance. </p>
										
				 <div class="calc_foot"><p class="return-to-top">Return to Top</p></div>
			</div>
			
		</div>	
     
     
    </div>
	<div id="content-for-b" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">
			<FORM name="form2"> 
			
					<p align="center"><font size="2" face="verdana"
					color="#1f6fa2"><b><span>Inductive Reactance Calculator</span></b></font></p> 
					
					
					<p align="center"><img  src="images/ind reactance form.bmp" border="0"></p>  
			  
			<center>
					 <table align="center" style="margin-top:.3cm" class="rclf_tab">
					<tr><th colspan="2">Enter your values:</th></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
					
					<tr>
							  <td align="right"><font size="2" face="verdana"
					color="#1f6fa2"><b style="margin-left:.5cm">Frequency: </b></font> </td>
								<td align="left"> <input type="text" NAME="Fx1" size="7" maxlength="15">
						<font size="2" face="verdana"
					color="#1f6fa2"><b>   Hz</b></font> </td></tr>
						  
					<tr>
							  <td align="right"><font size="2" face="verdana"
					color="#1f6fa2"><b style="margin-left:.5cm">Inductance: </b></font> </td>
								<td align="left"> <input type="text" NAME="ind1" size="7" maxlength="15">
						 <font size="2" face="verdana"
					color="#1f6fa2"><b>    mH</b></font> </td></tr>
							  
					<tr>
						<td align="right"><input type="Button" value="Calculate"   onClick="calcXL();" class="calculate_cal"></td>
						<td align="left"><input type="reset" value="Clear"  class="clear_cal"></td>
				  
					  </tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
						  
					<tr>
					<th colspan="2">Result:</th></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
					<tr>
							  <td colspan="2"><font size="2" face="verdana"
					color="#1f6fa2"><b style="margin-left:.5cm">Inductive Reactance X<sub>L</sub>:
								<input type="text" NAME="XL1" size="7" maxlength="15" readonly="readonly">
						Ohms </b></font> </td></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>	
					</table>
				</center>	
				  </form>
				
		
		</div>
		<div class="calc_right">
				<h5>Calculate Inductive Reactance</h5>
				<p>Inductance is the characteristic of an electrical conductor that opposes change in current. A physical analogy to Inductance would be moving a heavy load. It takes more work to start the load moving that it does to keep the load moving, as in reverse it takes more work to stop that load moving than it does to keep it moving. This is because that load possesses Inertia, just as current possesses Inductance in an electrical circuit.
As an alternating current flows through an inductor, it is continuously reversing itself.This causes the same inertial cemf effect. This opposing force to the flow of alternating current is called Inductive Reactance because it is the reaction an inductor presents to alternating current.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
<div id="content-for-c" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">
		
			<FORM name="form3"> 
			 
				   <p align="center"><font size="2" face="verdana"
					color="#1f6fa2"><b><span>Capacitive Reactance Calculator</span></b></font></p>  
					
					<p align="center"><img  src="images/cap reactance form.bmp" border="0"></p> 
					<center>   
					<table align="center" style="margin-top:.3cm" class="rclf_tab">
					<tr><th colspan="3">Enter your values:</th></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
					
					<tr>
							  <td align="right"><font size="2" face="verdana"
					color="#1f6fa2"><b style="margin-left:.5cm">Frequency: </b></font> </td>
							  <td align="left">  <input type="text" NAME="Fx2" size="7" maxlength="15">
						  <font size="2" face="verdana"
					color="#1f6fa2"><b>  Hz</b></font> </td></tr>
					
						  
					<tr>
							  <td align="right"><font size="2" face="verdana"
					color="#1f6fa2"><b style="margin-left:.5cm">Capacitance: </b></font> </td>
							  <td align="left">  <input type="text" NAME="cap2" size="7" maxlength="15">
						   <font size="2" face="verdana"
					color="#1f6fa2"><b>   &micro;f</b></font> </td></tr>

						  
					<tr>
						<td align="right"><input type="Button" value="Calculate"  onClick="calcXC();" class="calculate_cal" ></td>
						<td align="left"><input type="reset" value="Clear"  class="clear_cal"></td> </tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
					   
						  
					<tr>
					<th colspan="3">Result:</th></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
					<tr>
							  <td colspan="3"><font size="2" face="verdana"
					color="#1f6fa2"><b style="margin-left:.5cm">Capacitive Reactance X<sub>C</sub>:
								<input type="text" NAME="XC2" size="7" maxlength="15" readonly="readonly">
						Ohms </b></font> </td></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
					</table>
					</center>
					</form>		
				
		
		</div>
		<div class="calc_right">
				<h5>Calculate Capacitive Reactance</h5>
				<p>Capacitors also present an opposition to a.c. When an alternating current flows through a capacitor each plate changes polarity according to the frequency.  As electrons move from plate to plate they are limited to that plates storage ability (capacitance). Therefore at a higher frequency the maximum number of electrons stored will change plate more often, which means more current will flow. Also, as the capacitance increases, more electrons change plate every cycle, and more current will flow.An increase in frequency will cause a decrease in the capacitor's opposition and allow more current to flow, and an increase in capacitance will cause a decrease in the capacitors opposition and thus allowing more current to flow.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
	<div id="content-for-d" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">
	
				  <!----form4--->
		<FORM METHOD=POST>
		<center>
		<table align="center" class="rclf_tab">
		<tr><th colspan="3">Enter your values:</th></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
		<tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b>Capacitance (uF):</b></font></td>
	  <td align="left"><input NAME="C" SIZE="12" maxlength="15" TYPE="text" VALUE> </td></tr>
	  
	  <tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b>Frequency (Hz):</b></font></td>
	  <td align="left"><input NAME="F" SIZE="12" maxlength="15" TYPE="text" VALUE> </td></tr>
	  		<tr><td align="right"><INPUT TYPE="button" NAME="name" VALUE="Calculate"
				onClick="solve11(this.form);"  class="calculate_cal"></td>
				<td align="left">
		<INPUT TYPE="reset" VALUE="Clear"
				onClick="clearBoxes(this.form);"  class="clear_cal"></td></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
					   
						  
					<tr>
					<th colspan="3">Result:</th></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>				
	  <tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b>Inductance (Henrys):</b></font></td>
	  <td align="left"><input NAME="L" SIZE="12" maxlength="15" TYPE="text" VALUE  readonly="readonly"> 
	  
			</td></tr>

				<tr><td colspan="3" style="height:15px;"></td></tr>
			</table>
		</center>	
	
			
	
		
		</FORM>				
		
		</div>
		<div class="calc_right">
		<h5>Capacitance-Frequency-Inductance</h5>
							 	<p>A measure of the ability of a configuration of materials to store electric charge. In a capacitor, capacitance depends on the size of the plates, the type of insulator, and the amount of space between the plates. Most electrical components display capacitance to some degree; even the spaces between components of a circuit have a natural capacitance. Capacitance is measured in farads.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
<div id="content-for-e" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

			<FORM METHOD=POST>
			<center>
			<table align="center" class="rclf_tab">
			<tr><th colspan="3">Enter your values:</th></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>		
			<tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b>Resistance (Ohms) :</b></font></td>
			<td align="left"><INPUT TYPE ="text" SIZE=12 NAME="Resistance" VALUE="" maxlength="15" ></td></tr>
			
		  <tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b>Frequency (Hz)  :</b></font></td>
			<td align="left"><INPUT SIZE=12 NAME="Frequency" VALUE="" maxlength="15" type="text"></td></tr>
				
							<tr><td align="right"><INPUT TYPE="button" NAME="name" VALUE="Calculate"
					onClick="solve(this.form);"  class="calculate_cal"></td>
			<td align="left"><INPUT TYPE="reset" VALUE="Clear"
					onClick="clearBoxes(this.form);"  class="clear_cal"></td></tr>		   
					<tr><td colspan="3" style="height:15px;"></td></tr>	  
					<tr>
					<th colspan="3">Result:</th></tr>
					<tr><td colspan="3" style="height:15px;"></td></tr>
		  <tr><td align="right"><font size="2" face="verdana" color="#1f6fa2" ><b>Capacitance (uF):</b></font></td>
			<td align="left"><INPUT  SIZE=12  NAME="Capacitance" VALUE="" maxlength="15" type="text" readonly="readonly"></td></tr>
<tr><td colspan="3" style="height:15px;"></td></tr>

				</table>
			</center>
				
		
			</FORM>				
		
		</div>
		<div class="calc_right">
		<h5>Resistance-Frequency-Capacitance</h5>
							 	<p>The reactance is large at low frequencies and small at high frequencies. For steady DC which is zero frequency, reactance is infinite (total opposition), hence the rule that capacitors pass AC but block DC. For example a 1 µF capacitor has a reactance of 3.2 kΩ for a 50 Hz signal, but when the frequency is higher at 10 kHz its reactance is only 16.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>
	<div id="content-for-f" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

			<center><table class="rclf_tab">	
			  <tr>
                <td align="center"><img src="images/freq.bmp" border="0"></td>
              </tr> </table>
			  </center>
			  <center>
			  
	  <table align="center" class="rclf_tab"><tr><td><p><font size="2" face="verdana"
        color="#1f6fa2"><b>F= Frequency <br>
                    C= Speed Of Light <br>
                    <img src="images/lamda.bmp" border="0">= Wavelength</b></font></p>
         </td></tr></table>
		 </center>
			 

		
		

<form NAME="boxes">
<center>
	<table class="rclf_tab">
              <tr><th colspan="2">Enter your value:</th></tr>
			  <tr><td colspan="3" style="height:15px;"></td></tr>
			  <tr><td align="center"><input TYPE="text" NAME="b0" SIZE=7 maxlength="30"></td></tr>	
			 <tr><td colspan="3" style="height:15px;"></td></tr>
	</table>			
	</center>
	<br />
<p align="center">

<input TYPE="button"  VALUE="Centimeters" SIZE =" 5" ONCLICK=" choice=1; laybels();freq(); prnt();return true">  
<font size="2" face="verdana"
        color="black"><b style="margin-left:.2cm">or</b></font>

<input TYPE="button"  VALUE="Feet"  SIZE =" 5" ONCLICK=" choice=2;laybels(); freq();prnt(); return true" style="margin-left:.2cm">
<font size="2" face="verdana"
        color="black"><b style="margin-left:.2cm">or</b></font>  
<input TYPE="button"  VALUE="Meters"  SIZE =" 5" ONCLICK=" choice=3; laybels();freq();prnt(); return true" style="margin-left:.2cm">

<font size="2" face="verdana"
        color="black"><b style="margin-left:.2cm">or</b></font>  
<input TYPE="button"  VALUE="Hertz"  SIZE =" 5" ONCLICK=" choice=4; laybels();wave(); prnt();return true" style="margin-left:.2cm">
<font size="2" face="verdana"
        color="black"><b style="margin-left:.2cm">or</b></font>  
<input TYPE="button"  VALUE="KiloHertz"  SIZE =" 5" ONCLICK=" choice=5; laybels();wave(); prnt();return true" style="margin-left:.2cm"> 
<font size="2" face="verdana"
        color="black"><b style="margin-left:.2cm">or</b></font> 
<input TYPE="button"  VALUE="MegaHertz"  SIZE =" 5" ONCLICK=" choice=6; laybels();wave(); prnt();return true" style="margin-left:.2cm">
</p>

<center>
<table align="center" class="rclf_tab">
		<tr><th colspan="2">Results:</th></tr>
<tr><td colspan="3" style="height:15px;"></td></tr>
		
<tr><td colspan="2" align="center"><input TYPE="text" NAME="b0a" SIZE="50" maxlength="75"  readonly="readonly">

</td></tr>

<tr><td align="right">
<input TYPE="text" NAME="b1" SIZE=15 maxlength="30"  readonly="readonly"></td>
<td align="left">                    <input TYPE="text" NAME="b1a" SIZE=7 maxlength="30" readonly="readonly">
</td></tr>
<tr><td align="right">

<input TYPE="text" NAME="b2" SIZE=15 maxlength="30"  readonly="readonly"></td>
              <td align="left">      <input TYPE="text" NAME="b2a" SIZE=7 maxlength="30" readonly="readonly" >
</td></tr>
<tr><td align="right">
<input TYPE="text" NAME="b3" SIZE=15 maxlength="30"  readonly="readonly"></td>
<td align="left">
                    <input TYPE="text" NAME="b3a" SIZE=7 maxlength="30" readonly="readonly">

</td></tr>
<tr><td colspan="3" style="height:15px;"></td></tr>
</table>
</center>
</form>
</center>

<FORM NAME="box">

<INPUT TYPE="hidden" NAME="b9" VALUE=5 SIZE="7">

</form>
				
		
		</div>
		<div class="calc_right">
		<h5>Frequency and Wavelength</h5>
							 	<p>Frequency is a measure of the number of occurrences of a repeating event per unit time.</p>
								<p>SI unit of frequency is Hertz (Hz), named after the German physicist Heinrich Hertz. One hertz means one cycle per second, 100 Hz means one hundred cycles per second, and so on </p>
								<p>Wavelength is the distance between repeating units of propagating wave of a given frequency. Wavelength can be represented by symbol Lambda. Examples of wave like phemomena are light, water waves and sound waves.</p>
								<p>Wavelength can be expressed in any unit of Length such as Meters, Feet, Centimeters etc. </p>

		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>			
	<div id="content-for-g" style="background-color:#fff;clear:both;" class="calc_main">
		<div class="calc_content">	
   		<div class="calc_left">

					   
			
			
			 <form method="POST" name="form" action=""> 
			 <center>
					 <table align="center" class="rclf_tab">
					<tr><th colspan="3">Enter your values:</th></tr>

					 <tr><td colspan="3" style="height:15px;"></td></tr>
					<tr>
							  <td align="right"><font size="2" face="verdana"
					color="#1f6fa2"><b style="margin-left:.5cm">Length of Wire: </b></font> </td>
							<td align="left"><font size="2" face="verdana"
					color="#1f6fa2"><b>    <input type="text" name="b" size="7" maxlength="15">
						  Inches</b></font> </td></tr>
						  
					<tr>
							  <td align="right"><font size="2" face="verdana"
					color="#1f6fa2"><b>Diameter of Wire: </b></font> </td>
							  <td align="left"><font size="2" face="verdana"
					color="#1f6fa2"><b>    <input type="text" name="a" size="7" maxlength="15">
							Inches</b></font> </td></tr>
						  
						  
					<tr>
						<td  align="right"><input type="Button" value="Calculate" onClick="Compute(document.form)"  class="calculate_cal"></td>
						<td align="left"><input type="reset" value="Clear"  class="clear_cal"></td>
				  
					  </tr>

					<tr><td colspan="3" style="height:15px;"></td></tr> 
					<tr>
					<th  colspan="2">Result:</th></tr>
					
					<tr>
							  <td  align="right"><font size="2" face="verdana"
					color="#1f6fa2"><b >Inductance:</b></font></td>
								<td align="left"><input type="text" name="L" size="7" maxlength="15" readonly="readonly">
						<font size="2" face="verdana"
					color="#1f6fa2"><b>uH</b></font> </td></tr>
				<tr><td colspan="3" style="height:15px;"></td></tr>	
					</table>
				</center>	
				  </form>				
		
		</div>
		<div class="calc_right">
		<h5>Inductance of Straight Wire</h5>
		<p>This calculator is use to calculate inductance of a straight wire conductor. To compute the inductance, enter the values of length and diameter of the wire.</p>
		 <div class="calc_foot"> <p class="return-to-top">Return to Top</p></div>    
		</div>
		
		</div>
		
     

    </div>	
						
			
        </div>
  
</div>

    </div>
</div>
</div>
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL1 li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL1 li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#fff'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL1 li").each(function() { 
			$(".firstUL1 li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#fff'}); //set the background color to light-blue to that div
	 });
});
</script>
<!-----Capacitance and Inductance from Reactance--->
<script type="text/javascript">

function calcLC() {
  var freq = form1.Fx.value
  var reac = form1.Xx.value

  var C = 1000000/(2*Math.PI*freq*reac);
  var L = reac*1000 / (2*Math.PI*freq)

  form1.cap.value = Math.round(C*10)/10; 
  form1.ind.value = Math.round(L*100)/100;

  
}

</script>

<!------------Inductive Reactance----->
<script type="text/javascript">
function calcXL() {
  var freq1 = form2.Fx1.value
  var ind1 = form2.ind1.value

  var XL1 = 2*Math.PI*freq1*ind1 / 1000

  form2.XL1.value = Math.round(XL1*100)/100;

  
}

</script>
<!------Capacitive Reactance----->
<script type="text/javascript">

function calcXC() {
  var freq2 = form3.Fx2.value
  var cap2 = form3.cap2.value

  var XC2 = 1000000 / (2*Math.PI*freq2*cap2);

  form3.XC2.value = Math.round(XC2*100)/100; 
}
</script>

<!-----Capacitance-Frequency-Inductance------>
<SCRIPT LANGUAGE="JavaScript">
	
	var C = 0;
	var F = 0;
	var L = 0;

	
	function clearBoxes(form)
		{
	
		form.C.value = "";	
		form.F.value = "";
		form.L.value = "";

	
		form.C.focus();       
		}

	
	function solve11(form)
		{
		var i = 3;

		

		if(!form.C.value) C = 0;
			else C = eval(form.C.value);
		if(C == 0) i--;

		if(!form.F.value) F = 0; 
			else F = eval(form.F.value);
		if(F == 0) i--;

		if(!form.L.value) L = 0;
			else L = eval(form.L.value);
		if(L == 0) i--;

		
		if(i == 0) return;      
		if(i == 1)
			{
			
			alert("\nMissing a value. Two are required!\n");
			return;
			}

		

	
		if(C == 0)
			{
			 
			form.C.value = 
                        25330/F/F/L;
			form.C.focus();
			return;     
			}

	
		if(F == 0)
			{   
			 
			form.F.value =
                        159.154/Math.sqrt(L*C);
			form.F.focus();
			return;
			}

		
		 
		form.L.value =
                25330/F/F/C;        
		form.L.focus();

	
		return;
		}

	
	
//  -->
</script>

<!---------------Resistance-Frequency-Capacitance------------------->
<SCRIPT LANGUAGE="JavaScript">

	var BelsConstant = 159154;

	
	var r = 0;
	var f = 0;
	var c = 0;

	
	function clearBoxes(form)
		{
		
		form.Resistance.value = "";	
		form.Capacitance.value = "";
		form.Frequency.value = "";

		
		form.Resistance.focus();        
		}

	
	function solve(form)
		{
		var i = 3;

		

		if(!form.Resistance.value) r = 0;
			else r = eval(form.Resistance.value);
		if(r == 0) i--;

		if(!form.Frequency.value) f = 0; 
			else f = eval(form.Frequency.value);
		if(f == 0) i--;

		if(!form.Capacitance.value) c = 0;
			else c = eval(form.Capacitance.value);
		if(c == 0) i--;

		
		if(i == 0) return;      
		if(i == 1)
			{
				
			alert("\nMissing a value. Two are required!\n");
			return;
			}

		
		if(r == 0)
			{
			
			form.Resistance.value = (BelsConstant / c) / f;
			form.Resistance.focus();
			return;     
			}

		
		if(f == 0)
			{   
			
			form.Frequency.value = (BelsConstant / c) / r;
			form.Frequency.focus();
			return;
			}

		
		form.Capacitance.value = BelsConstant / (f * r);        
		form.Capacitance.focus();

		
		return;
		}

</SCRIPT>
<script language = JavaScript>
var c = 29979245800;
var h = 4.13566E-15;
var choice=0;
var inval=0;
var freq=0;
var wave=0;
var e=0;
var ct=0;
var sigfig=5;
var fctra=new Array(0,1,30.48,100,1,1000,1000000);
var label1=new Array(" ", "Wavelength", "Wavelength", "Wavelength", "Frequency", "Frequency", "Frequency");
var label2=new Array(" ", " cm", " feet", " mt", " hz", " khz", " mhz");
var freqfctr=new Array(0,1,1000,1000000);
var freqfctr2=new Array(0,1,30.48,100);
var outpt=new Array();


</script>

<script language = JavaScript>
function laybels()
{inval=document.boxes.b0.value;sigfig=(document.box.b9.value)-1;
document.boxes.b0a.value=label1[choice] + " = " + inval + " "+label2[choice] +" converted to "+label1[7-choice] + " = ";
}
</script>

<script language = JavaScript>
function freq()
{document.boxes.b1a.value=label2[4];document.boxes.b2a.value=label2[5];document.boxes.b3a.value=label2[6];
fctr=fctra[choice];

inval=inval*fctr;e=(h*c)/inval;outpt[4]=e;
ct=1;
while(ct<4)
{outpt[ct] = c/(inval*freqfctr[ct]);ct=ct+1};
}
</script>

<script language = JavaScript>
function wave()
{document.boxes.b1a.value=label2[1];document.boxes.b2a.value=label2[2];document.boxes.b3a.value=label2[3];
fctr=fctra[choice];

inval=inval*fctr;
ct=4;
while(ct<7)
{outpt[ct-3] = (c/(inval*freqfctr2[ct-3]));ct=ct+1}
outpt[4]=(h*inval);
}
</script>

<script language = JavaScript>
function prnt()
{ct=1;
while(ct<5)
{if(sigfig>-1){outpt[ct]=outpt[ct].toExponential(sigfig)};
if(outpt[ct]>.001 && outpt[ct]<1000){outpt[ct]=outpt[ct]*1;}ct=ct+1}

document.boxes.b1.value=" "+outpt[1];
document.boxes.b2.value=" "+outpt[2];
document.boxes.b3.value=" "+outpt[3];
document.boxes.b4.value=" & has a photon energy of"
document.boxes.b4a.value=" "+outpt[4] + " electron volts";
}
</script>

<script language="JavaScript">



function Compute(form) {
	with(Math) {
		var a= Number(form.a.value);
		var b= Number(form.b.value);
		
		var L;
		var Precision= 3;
		
		L= 0.00508*b*(log(2*b/a)-0.75);

		form.L.value= L.toFixed(Precision);
	}
}


</script>
