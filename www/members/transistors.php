<?php 

$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>
 <link type="text/css" rel="stylesheet" href="css/easy-responsive-tabs.css" />
		<div class="side_work">	
			<div class="working_area">
	<div class="status_panel">
		<div class="status_sec">
	
				<div class="statusbar"><p>Current Page - <strong><span class="blue_color">Calculators / Transistors</span></strong></p></div>
				<div class="returnstat"><a href="calculator.php" class="menu_click">Return</a></div>
	
		</div>
	</div>
<div class="app_calculator">

  <div class="calc_buttons">
  


  </div> 
            
<div id="body-container1">
                  <div class="glossary-container">
   					 <ul class="firstUL1">
						<li id="a" class="selected">BJT Transistor Bias Voltage</li>
											
					</ul>
				 </div>	
  <div class="content-container">

    <div id="content-for-a" style="background-color:#fff" class="calc_main">
		<div class="calc_content">
						   <div class="calc_left">

			
<form method="POST" name="form">  
	<center>
	<table align="center" class="resist">
	
		  					<tr><th colspan="2" align="center">Enter Values</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>	
		
		<tr>
		          <td  align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">Base Bias Type: </b></font> </td>
		<td align="left"><select size="1" name="BiasType" onChange= "ShowBiasType(this.form);">
                <option value="1" selected>Series Resistor</option>
                <option value="2">Voltage Divider</option>
              </select>
             <font size="2" face="verdana"
        color="#1f6fa2"><b>Volts</b></font> </td></tr>
	
			  
			  <tr><td colspan="2" align="center"><div id="i1" style="display: none;">
      <img border="0" src="images/voltdivider.jpg"><br><br>
	  <font size="2" face="verdana"
        color="#1f6fa2"><b >R<sub>c</sub> = Collector Resistance <br> R<sub>e</sub> = Emitter Resistance
     <br> V<sub>s</sub> = Supply Voltage <br> V<sub>c</sub> = Collector Voltage 
	 <br> V<sub>e</sub> = Emitter Voltage  <br> V<sub>b</sub> = Base Voltage</b></font>
 <br><br><img src="images/volt1 form.bmp" border="0"><br><br>
	 <img src="images/volt form.bmp" border="0"> <br> <br>
	 <font size="2" face="verdana"
        color="#1f6fa2"><b >I<sub>c</sub> = Collector gain x I<sub>b</sub> <br> <br> V<sub>e</sub> = I<sub>C</sub> x R<sub>e</sub>
     <br> <br> V<sub>b</sub> =  V<sub>e</sub> x Base to Emitter Drop <br> <br> V<sub>c</sub> = V<sub>s</sub> -  I<sub>C</sub> x R<sub>c</sub>
	 <br> <br> If (V<sub>in</sub> &lt; V<sub>b</sub>) then, <br> <br></b><b style="margin-left:.5cm"> V<sub>c</sub> = V<sub>s</sub> </b><br> 
	 <br> <b style="margin-left:.5cm">V<sub>e</sub> = 0 </b>
	 <br> <br> <b style="margin-left:.5cm"> V<sub>b</sub> = Base to Emitter Drop </b>
	 <br> <br><b style="margin-left:.5cm"> I<sub>c</sub> = 0  </b><br> <br><b style="margin-left:.5cm"> I<sub>b</sub> = 0</b></font>

      
      </div>
      
      
      <div id="i2" style="display: block;">
      <img border="0" src="images/seriesresistor.jpg">
		<br><br>
	  <font size="2" face="verdana"
        color="#1f6fa2"><b > R<sub>b</sub> = Base Resistance <br> V<sub>in</sub> = Input Voltage <br>
		 R<sub>c</sub> = Collector Resistance <br> R<sub>e</sub> = Emitter Resistance
     <br> V<sub>s</sub> = Supply Voltage <br> V<sub>c</sub> = Collector Voltage 
	 <br> V<sub>e</sub> = Emitter Voltage  <br> V<sub>b</sub> = Base Voltage</b></font><br>
	 <br>
	 <img src="images/series1 form.bmp" border="0"> <br><br>
	 <font size="2" face="verdana"
        color="#1f6fa2"><b >I<sub>c</sub> = Collector gain x I<sub>b</sub> <br> <br> V<sub>e</sub> = I<sub>C</sub> x R<sub>e</sub>
     <br> <br> V<sub>b</sub> =  V<sub>e</sub> x Base to Emitter Drop <br> <br> V<sub>c</sub> = V<sub>s</sub> -  I<sub>C</sub> x R<sub>c</sub>
	 <br> <br> If (V<sub>c</sub> &lt; V<sub>e</sub>) then, <br> <br></b><b style="margin-left:.5cm">  <img src="images/series2  form.bmp" border="0"> </b><br> 
	 <br> <b style="margin-left:.5cm">V<sub>c</sub> = V<sub>e</sub> </b>
	 <br> <br> <b style="margin-left:.5cm"> V<sub>b</sub> = V<sub>e</sub> + Base to Emitter Drop </b>
	 <br> <br><b style="margin-left:.5cm">  <img src="images/series3  form.bmp" border="0"></b></font>

      
      </div> </td></tr>
	  
		<tr>
		          <td colspan="2" align="center"><div id="a1"  style="display: block;"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">Base Resistance (R<sub>b</sub>): 
                      <input type="text" name="Rb" size="7" maxlength="15"  style="margin-left:.8cm">
                      </b><b>K Ohms</b><b style="margin-left:.5cm"> </b></font></div>
                    <div id="a2"  style="display: block;"> <font size="2" face="verdana"
        color="#1f6fa2"></font></div> </td></tr>
			  
		<tr>
		          <td colspan="2" align="center"><div id="b1"  style="display: block;"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">Input Voltage (V<sub>in</sub>): 
                      <input type="text" name="Vin" size="7" maxlength="15"  style="margin-left:0.82cm">
                      </b><b>Volts</b><b style="margin-left:.5cm"> </b></font></div>
                    <div id="b2"  style="display: block;"> <font size="2" face="verdana"
        color="#1f6fa2"></font></div> </td></tr>	  
		
		<tr>
		         <td colspan="2" align="center"><div id="c1"  style="display: none;"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">R<sub>a: 
                      <input type="text" name="R1" size="7" maxlength="15"  style="margin-left:4.45cm">
                      </sub></b><b>K ohms</b><b style="margin-left:.5cm"><sub> 
                      </sub></b></font></div>
                    <div id="c2"  style="display: none;"> <font size="2" face="verdana"
        color="#1f6fa2"></font></div> </td></tr>	 
          
			  
		<tr>
		          <td colspan="2" align="center"><div id="d1"  style="display: none;"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">R<sub>b: 
                      <input type="text" name="R2" size="7" maxlength="15"  style="margin-left:4.45cm">
                      </sub></b><b>K ohms</b><b style="margin-left:.5cm"><sub> 
                      </sub></b></font></div>
                    <div id="d2"  style="display: none;"> <font size="2" face="verdana"
        color="#1f6fa2"></font></div> </td></tr>
	  
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">Collector Resistance (R<sub>c</sub>): </b></font> </td>
                  <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b >    <input type="text" name="Rc" size="7" maxlength="15">
             K ohms</b></font> </td></tr>
			  	 
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">Emitter Resistance (R<sub>e</sub>): </b></font> </td>
                  <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b >    <input type="text" name="Re" size="7" maxlength="15">
             K ohms</b></font> </td></tr>
			 
			<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">Supply Voltage (V<sub>s</sub>):</b></font> </td>
                   <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b >   <input type="text" name="VP" size="7" maxlength="15">
             Volts</b></font> </td></tr>
			 
			<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">Current gain: </b></font> </td>
                  <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b >    <input type="text" name="Beta" size="7" maxlength="15">
             </b></font> </td></tr>
			 
			<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">Base to Emitter Drop: </b></font> </td>
                  <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b >    <input type="text" name="Vbe" size="7" maxlength="15">
             Volts</b></font> </td></tr>

		
		<tr>
            <td  align="right"><input type="Button" value="Calculate" onClick="Compute(document.form)" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>

			  				<tr><td colspan="2" style="height:15px;"></td></tr>
		  					<tr><th colspan="2" align="center">Result</th></tr>
							<tr><td colspan="2" style="height:15px;"></td></tr>	
		
  		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">Collector Voltage (V<sub>c</sub>):</b></font> </td>
                    <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b >  <input type="text" name="Vc" size="7" maxlength="15" readonly="readonly">
            Volts</b></font> </td></tr>
			
				<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">Emitter Voltage (V<sub>e</sub>):</b></font> </td>
                    <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b >  <input type="text" name="Ve" size="7" maxlength="15" readonly="readonly">
            Volts</b></font> </td></tr>
			
				<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">Base Voltage (V<sub>b</sub>):</b></font> </td>
                   <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b >   <input type="text" name="Vb" size="7" maxlength="15" readonly="readonly">
            Volts</b></font> </td></tr>
			
				<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">Collector Voltage (I<sub>c</sub>):</b></font> </td>
                 <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b >     <input type="text" name="Ic" size="7" maxlength="15" readonly="readonly">
           mA</b></font> </td></tr>
			
				<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">Base Voltage (I<sub>b</sub>):</b></font> </td>
                  <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b >    <input type="text" name="Ib"  size="7" maxlength="15" readonly="readonly">
            mA</b></font> </td></tr>
			
				<tr>
		          <td colspan="2" align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm">
                    <input type="hidden" name="Saturated" value="ON">
           </b></font> </td></tr>
			
				<tr>
		          <td colspan="2" align="center"><font size="2" face="verdana"
        color="#1f6fa2"><b style="margin-left:.5cm"> <input type="hidden" name="CutOff" value="ON">
           </b></font> </td></tr>
				<tr><td colspan="2" style="height:15px;"></td></tr>
        </table>
		</center>
		</form>

						 </div>
						 <div class="calc_right">
						 <h5>BJT Transistor Bias Voltage</h5>
							<p>This calculator,compute all of the bias values of the transistor circuit, given the supply voltage, and the base voltage, and all of the 
resistor values.  The beta and Vd transistor parameters, can be measured, or gathered from a data sheet. If unknown, the default values can be used, since 
the circuit is normally fairly insensitive to these values.</p>
							 <div class="calc_foot"><p class="return-to-top">Return to Top</p></div>
						 </div> 
     				
		</div>
    </div>
	
			
		
        </div>
  
</div>

    </div>
</div>
</div>
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL1 li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL1 li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#fff'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL1 li").each(function() { 
			$(".firstUL1 li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#fff'}); //set the background color to light-blue to that div
	 });
});
</script>

<script language="JavaScript">


function ShowBiasType(form) {
	var type= Number(form.BiasType.value);
	switch(type) {
		case 2:
			document.getElementById('a1').style.display= "none";
			document.getElementById('a2').style.display= "none";
			document.getElementById('b1').style.display= "none";
			document.getElementById('b2').style.display= "none";	

			document.getElementById('c1').style.display= "block";
			document.getElementById('c2').style.display= "block";
			document.getElementById('d1').style.display= "block";
			document.getElementById('d2').style.display= "block";
			
			document.getElementById('i1').style.display= "block";
			document.getElementById('i2').style.display= "none";
		break;
		case 1:
			document.getElementById('a1').style.display= "block";
			document.getElementById('a2').style.display= "block";
			document.getElementById('b1').style.display= "block";
			document.getElementById('b2').style.display= "block";
			
			document.getElementById('c1').style.display= "none";
			document.getElementById('c2').style.display= "none";
			document.getElementById('d1').style.display= "none";
			document.getElementById('d2').style.display= "none";			
			
			document.getElementById('i1').style.display= "none";
			document.getElementById('i2').style.display= "block";
		break;
	}

}


function Compute(form) {
	with(Math) {
		var KILO= 1000;
		var Rb= Number(form.Rb.value)*KILO;
		var Rc= Number(form.Rc.value)*KILO;
		var Re= Number(form.Re.value)*KILO;
		var R1= Number(form.R1.value)*KILO;
		var R2= Number(form.R2.value)*KILO;
		var Beta= Number(form.Beta.value);
		var Vbe= Number(form.Vbe.value);
		var VP= Number(form.VP.value);
		var Vin= Number(form.Vin.value);
		var Precision= 5;
		
		
		if(R1) {
				Rb= R1*R2/(R1+R2);
				Vin= VP*R2/(R1+R2);
		}		

		Ib= (Vin-Vbe)/(Rb+Beta*Re);
		Ic= Beta*Ib;
		Ve= Ic*Re;
		Vb= Ve+Vbe;
		Vc= VP - Ic*Rc;
		


		
		if(Vc<Ve) {
			form.Saturated.checked= true;
			form.CutOff.checked= false;
		
			Ve= VP*Re/(Re+Rc);
			Vc= Ve;
			Vb= Ve+Vbe;
			Ic= VP/(Re+Rc);
		} else if(Vin<Vb) {
			form.CutOff.checked= true;
			form.Saturated.checked= false;
			Vc= VP;
			Ve= 0;
			Vb= Vbe;
			Ic= 0;
			Ib= 0;
			
		}else {
			form.Saturated.checked= false;
			form.CutOff.checked= false;
		}
		
		
		
		Ib*= KILO; 
		Ic*= KILO;

		form.Ib.value= Ib.toPrecision(Precision);
		form.Ic.value= Ic.toPrecision(Precision);
		form.Ve.value= Ve.toPrecision(Precision);
		form.Vb.value= Vb.toPrecision(Precision);
		form.Vc.value= Vc.toPrecision(Precision);
		
		
	}
}


</script>

