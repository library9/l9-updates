<?php
session_start();
$table=$_SESSION['table'];


//// * Example PHP implementation used for the index.html example
// 
//
//// DataTables PHP library
include( "DataTables-1.10.0/extensions/Editor-1.3.0/php/DataTables.php" );
//
//// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Join,
    DataTables\Editor\Validate;
//
//// Build our Editor instance and process the data coming from _POST

Editor::inst( $db,'capacitor','ITEM' )
   ->fields(
	   
        Field::inst( 'capacitor.Part_Number' ),
		Field::inst( 'capacitor.PN' ),
		Field::inst( 'capacitor.Library_Ref'),
	    Field::inst( 'capacitor.Description' ),
		
	    Field::inst( 'capacitor.Manufacturer' ),
		Field::inst( 'capacitor.Manufacturer_Part_Number'),	    
        Field::inst( 'capacitor.ComponentLink1URL' ),
		Field::inst( 'capacitor.Temp_Qual' )
		  
   
    )
    ->process( $_POST )
    ->json();