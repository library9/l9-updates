﻿<?php
include("include.php");  // read css and js folders, sets database variables

$table=$_GET['TBL'];
$item=$_GET['ITM'];

mysql_connect($host,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$result = mysql_query("SELECT ITEM,`Part Number`, PN, Description, `Library Ref`,`Footprint Ref`, Manufacturer,`Manufacturer Part Number`, ComponentLink1URL FROM $table WHERE ITEM=$item");
if (!$result) {
    echo 'Could not run query: ' . mysql_error();
    exit;
}
$row = mysql_fetch_row($result);

$Item=$row[0];
$PartName=$row[1];
$PartNumber=$row[2];
$Desc=$row[3];
$SchSym=$row[4];
$Footprint=$row[5];
$Manu=$row[6];
$ManuPN=$row[7];
$Datasheet=$row[8];

mysql_close();
?>

<br/><br/>
<center><h2><b>DELETE <?php echo strtoupper($table); ?></b></h2></center>

<center>
<table id='partdelete' border="2" cellspacing="2" cellpadding="2" class="sortable">
<form name="partdel" action="partdelete-done.php?ITM=<?php echo $item; ?>&TBL=<?php echo $table; ?>" method="post">
<tr> 
<th><font face="Arial, Helvetica, sans-serif">ITEM</font></th>
<th><font face="Arial, Helvetica, sans-serif">Part Name</font></th>
<th><font face="Arial, Helvetica, sans-serif">PN</font></th>
<th><font face="Arial, Helvetica, sans-serif">Description</font></th>
<th><font face="Arial, Helvetica, sans-serif">Schematic symbol</font></th>
<th><font face="Arial, Helvetica, sans-serif">Footprint</font></th>
<th><font face="Arial, Helvetica, sans-serif">Manufacturer</font></th>
<th><font face="Arial, Helvetica, sans-serif">Manufacturer PN</font></th>
<th><font face="Arial, Helvetica, sans-serif">Datasheet</font></th>
</tr>
<tr> 
<td><center><font face="Arial, Helvetica, sans-serif"><?php echo "$Item";?></font></center></td>
<td><font face="Arial, Helvetica, sans-serif"><?php echo "$PartName"; ?></font></td>
<td nowrap><font face="Arial, Helvetica, sans-serif"><?php echo "$PartNumber"; ?></font></td>
<td><font face="Arial, Helvetica, sans-serif"><?php echo "$Desc"; ?></font></td>
<td><font face="Arial, Helvetica, sans-serif"><?php echo "$SchSym"; ?></font></td>
<td><font face="Arial, Helvetica, sans-serif"><?php echo "$Footprint"; ?></font></td>
<td><font face="Arial, Helvetica, sans-serif"><?php echo "$Manu"; ?></font></td>
<td><font face="Arial, Helvetica, sans-serif"><?php echo "$ManuPN"; ?></font></td>
<td><font face="Arial, Helvetica, sans-serif"><center>Datasheet</font></center></td>
</tr>
</table>
</center>

<br><br>
<center>
<table border="0"  width="50%" cellpadding="2" cellspacing="2">
	<tr>
	<td align="center"><input type="submit" value="Submit Delete"></td>
	<td><input type="button" onClick="location.href='view1x0.php?TBL=<?php echo $table; ?>'" value='Cancel'></td>
	</form>
	</tr>
</table>
</center>
