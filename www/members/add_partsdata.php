<?php
$groupswithaccess="ladmin,luser,leditor";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
session_start();
$MPN=$_SESSION['PN'];

//// * Example PHP implementation used for the index.html example
// 
//
//// DataTables PHP library
include( "DataTables-1.10.0/extensions/Editor-1.3.0/php/data.php" );
//
//// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Join,
    DataTables\Editor\Validate;
//
//// Build our Editor instance and process the data coming from _POST

Editor::inst( $db,'l9_parts','ITEM' )
    ->fields(
	  
         Field::inst( 'Part_Number' ),
		Field::inst( 'PN' ),
	    Field::inst( 'Description' ),
	    Field::inst( 'Manufacturer' ),
		Field::inst( 'Manufacturer_Part_Number')
		
    )
	->where( $key = 'Manufacturer_Part_Number', $value = $MPN, $op = '=' )
	
    ->process( $_POST )
    ->json();

?>