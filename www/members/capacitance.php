<?php 

$groupswithaccess="ladmin,luser,leditor,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");
include 'header.php'; 
include("sidebar.php");
?>
 <link type="text/css" rel="stylesheet" href="css/easy-responsive-tabs.css" />
		<div class="side_work">	
			<div class="working_area">
	<div class="status_panel">
		<div class="status_sec">
	
				<div class="statusbar"><p>Current Page - <strong><span class="blue_color">Calculators / Capacitance</span></strong></p></div>
				<div class="returnstat"><a href="calculator.php" class="menu_click">Return</a></div>
	
		</div>
	</div>
<div class="app_calculator">

  <div class="calc_buttons">
  


  </div> 
            
<div id="body-container1">
                  <div class="glossary-container cap_calc">
   					 <ul class="firstUL1">
						<li id="a" class="selected">Capacitance</li>
						<li id="b">Capacitor Energy and Time Constant</li>
						<li id="c">Capacitor Parallel Plate Capacitance </li>
						<li id="d">Polyester Capacitor Color Code</li>												
					</ul>
				 </div>	
  <div class="content-container">

    <div id="content-for-a" style="background-color:#fff" class="calc_main">
		<div class="calc_content">
						   <div class="calc_left">
						   
								<div id="capacitor-calc-container"></div>
		
								 <script>
								   (function() {
									 var calc = document.createElement('script'); calc.type = 'text/javascript'; calc.async = true;
									 calc.src = 'js/calc-capacitor.js';
									  (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(calc);
									})();
							  </script>
						 </div>
						 <div class="calc_right">
						 <h5>Capacitance</h5>
							<p>A measure of the ability of a configuration of materials to store electric charge. In a capacitor, capacitance depends on the size of the plates, the type of insulator, and the amount of space between the plates. Most electrical components display capacitance to some degree; even the spaces between components of a circuit have a natural capacitance. Capacitance is measured in farads.</p>
							 <div class="calc_foot"><p class="return-to-top">Return to Top</p></div>
						 </div> 
     				
		</div>
    </div>
	
<div id="content-for-b" style="background-color:#fff" class="calc_main">
		<div class="calc_content">
						   <div class="calc_left">

<form method="POST" name="form">
								<center>
								 <table align="center" style="margin-top:.3cm" class="capi">
									<tr><th colspan="2">Enter your values:</th></tr>
									<tr><td colspan="2" style="height:15px;"></td></tr>
		
		<tr>
		<td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Voltage across capacitor:</b></font> </td>
		<td align="left"><input type="text" name="Vc" size="7" maxlength="15">
             <font size="2" face="verdana"
        color="#1f6fa2"><b> V</b></font> </td></tr>

        <tr>
		<td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance:</b></font> </td>
		<td align="left"><input type="text" name="C" size="7" maxlength="15">
              <font size="2" face="verdana"
        color="#1f6fa2"><b> uF</b></font> </td></tr>
          
          <tr>
		<td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Load Resistance:</b></font> </td>
		<td align="left"><input type="text" name="R" size="7" maxlength="15">
             <font size="2" face="verdana"
        color="#1f6fa2"><b>  Ohms</b></font> </td></tr>

			  
		<tr>
            <td align="right"><input type="button" value="Calculate" name="B1" onClick="Compute(document.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
								  	<tr><td colspan="2" style="height:15px;"></td></tr>
									<tr><th colspan="2">Result</th></tr>
									<tr><td colspan="2" style="height:15px;"></td></tr>
			  

		<tr>
		<td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Time Constant:</b></font></td>
		<td align="left"><input type="text" name="TC" size="7" maxlength="15" readonly="readonly" >
              <font size="2" face="verdana"
        color="#1f6fa2"><b>seconds</b></font> </td></tr>  
		
		<tr>
		<td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Energy:</b></font> </td>
	
		<td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b><input type="text" name="En" size="7" maxlength="15"readonly="readonly">
              Joules </b></font> </td></tr>
			  
			  <tr>
		<td align="right"></td>
		<td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b><input type="text" name="En1" size="7" maxlength="15" readonly="readonly"><font size="2" face="verdana"
        color="#1f6fa2"><b>BTU</b></font> </td></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>	
		
          
        </table>
		</center>
      </form>
						 </div>
						 <div class="calc_right">
						 <h5>Capacitor Energy and Time Constant</h5>
							<p>This calculator computes the energy in a capacitor, given the voltage across it. If you specify a load resistor it will also compute the time constant, or the time until the voltage drops to 37%, or charges to 63%. </p>
							 <div class="calc_foot"><p class="return-to-top">Return to Top</p></div>
						 </div> 
     				
		</div>
    </div>
	
<div id="content-for-c" style="background-color:#fff" class="calc_main">
		<div class="calc_content">
						   <div class="calc_left">

<FORM METHOD=POST> 
		<center>
		<table align="center" style="margin-top:.3cm" class="capi">
		<tr><th colspan="2">Enter your values:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Dielectric Constant: </b></font> </td> 
                    <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b> <input type="text" NAME="k" size="7" maxlength="15">
              </b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Single Capacitor Plate Area:  </b></font> </td>
                    <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>  <input type="text" NAME="A" size="7" maxlength="15">
               Sq Inches</b></font> </td></tr>
			  
		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Dielectric Depth:  </b></font> </td>
                   <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>   <input type="text" NAME="d" size="7" maxlength="15">
             Inches</b></font> </td></tr>
			  
			  <tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Total Number Of Capacitor Plates:  </b></font> </td>
                    <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b>  <input type="text" NAME="N" size="7" maxlength="15" >
             </b></font> </td></tr>

			  
		<tr>
            <td align="right"><input type="Button" value="Calculate" NAME="name" onClick="solve(this.form);" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear" class="clear_cal"></td>
      
          </tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>  
		<tr><th colspan="2">Result</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		
  		<tr>
		          <td align="right"><font size="2" face="verdana"
        color="#1f6fa2"><b>Capacitance:</b></font></td>
                    <td align="left"><font size="2" face="verdana"
        color="#1f6fa2"><b><input type="text" NAME="C" size="7" maxlength="15" readonly="readonly">
            pF </b></font> </td></tr>
			<tr><td colspan="2" style="height:15px;"></td></tr>
        </table>
		</center>
      </form>
						 </div>
						 <div class="calc_right">
						 <h5>Capacitor Parallel Plate Capacitance</h5>
							<p>This calculator computes the capacitance between two parallel plates.Small valued capacitors can be etched into a PCB for RF applications, but under most circumstances it is more cost effective to use discrete capacitors.</p>
							 <div class="calc_foot"><p class="return-to-top">Return to Top</p></div>
						 </div> 
     				
		</div>
    </div>
	
<div id="content-for-d" style="background-color:#fff" class="calc_main">
		<div class="calc_content">
						   <div class="calc_left">

<FORM name="res">  
		<center>
		<table align="center" style="margin-top:.3cm" class="capi">
		<tr><th colspan="2">Enter your values:</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
		<tr>
		          <td align="center" colspan="2"><font size="2" face="verdana"
        color="#1f6fa2"><b><SELECT name="first" onChange="c()">
			  <OPTION selected>Strip 1
			  <OPTION>Black
			  <OPTION>Brown
			  <OPTION>Red
			  <OPTION>Orange
			  <OPTION>Yellow
			  <OPTION>Green
			  <OPTION>Blue
			  <OPTION>Violet
			  <OPTION>Grey
			  <OPTION>White</OPTION>
			
			 </SELECT>
              </b></font> 
			  
			  <font size="2" face="verdana"
        color="#1f6fa2"><b><SELECT name="second" onChange="c()">
			  <OPTION selected>Strip 2
			  <OPTION>Black
			  <OPTION>Brown
			  <OPTION>Red
			  <OPTION>Orange
			  <OPTION>Yellow
			  <OPTION>Green
			  <OPTION>Blue
			  <OPTION>Violet
			  <OPTION>Grey
			  <OPTION>White</OPTION>
			 </SELECT>
               </b></font> 
			   
			   <font size="2" face="verdana"
        color="#1f6fa2"><b><SELECT name="mult" onChange="c()">
			  <OPTION selected>Strip 3
			  <OPTION>Black
			  <OPTION>Brown
			  <OPTION>Red
			  <OPTION>Orange
			  <OPTION>Yellow
			  <OPTION>Green
			  <OPTION>Blue
			  <OPTION>Violet
			  <OPTION>Grey
			  <OPTION>White</OPTION>
			 </SELECT>
             </b></font> 
			 
			 <font size="2" face="verdana"
        color="#1f6fa2"><b> <SELECT name="tol" onChange="c()">
			  <option selected>Tolerance
			  <OPTION>Black %
			  <OPTION>White %</OPTION>
			 </SELECT>
             </b></font> </td></tr>

		<tr><td colspan="2" style="height:15px;"></td></tr>	  
		<tr>
            <td align="right"><input type="Button" value="Calculate" onClick="c()" class="calculate_cal"></td>
			<td align="left"><input type="reset" value="Clear"  class="clear_cal">
			</td>
		<tr><td colspan="2" style="height:15px;"></td></tr>      
		<tr><th colspan="2">Result</th></tr>
		<tr><td colspan="2" style="height:15px;"></td></tr>
			  
		
  		<tr>
		          <td align="center" colspan="2"> <input type="text" name="ans" size="35" maxlength="50" readonly="readonly"></td></tr>
				  		<tr><td colspan="2" style="height:15px;"></td></tr>	
        </table>
		</center>
      </form>
						 </div>
						 <div class="calc_right">
						 <h5>Polyester Capacitor Color Code</h5>
							<p>This calculator is designed to give the value of color coded poly capacitors.They come in various shapes and types. Most capacitors actually have the numeric 
values stamped on them.The capacitor's first and second bands are the significant number bands, the third is the multiplier, followed by the percentage tolerance band and the voltage band. Select the band values from the individual fields. After clicking on Calculate, the calculator will use the selected conditions for all of the bands and try to make a calculation.</p>
							 <div class="calc_foot"><p class="return-to-top">Return to Top</p></div>
						 </div> 
     				
		</div>
    </div>			
		
        </div>
  
</div>

    </div>
</div>
</div>
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL1 li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL1 li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#fff'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL1 li").each(function() { 
			$(".firstUL1 li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#ffffff'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#fff'}); //set the background color to light-blue to that div
	 });
});
</script>


<!-- Scrip capacitor energy and time constant-->

<script type="text/javascript">
function Compute(form) {
	with(Math) {
		var micro= 0.000001;
		var Precision= 3;
		var En;
				
		var Vc= Number(form.Vc.value);
		var C= Number(form.C.value)*micro;
		var R= Number(form.R.value);
		
		if(R) {
			TC= R*C;
			form.TC.value= TC.toPrecision(Precision);
		}
		
		En= 1*Vc*Vc*C/2;
				
		form.En.value= En.toPrecision(Precision);
		
		En1 = En * 0.00094781712;
		form.En1.value = En1.toPrecision(Precision);
	}
}



</script>


<!-- Scrip capacitor energy and time constant-->

<!-- Scrip capacitor parallel plate-->

<SCRIPT LANGUAGE="JavaScript">

			var k = 0;
			var A = 0;
			var d = 0;
			var N = 0;
			var C = 0;

        function ResetValues(form)
                {
					form.k.value = "";
					form.A.value = "";
					form.d.value = "";
					form.N.value = "";
					form.C.value = "";

						form.k.focus(); 
				}

        function showresults(form)
			{
				 form.k.value = k;
                form.A.value = A;
                form.d.value = d;
                form.N.value = N;
                form.C.value = C;
		    }

        function solve(form)
			{
				var i = 5;
                if(!form.k.value) k = 0;

                        else k = eval(form.k.value);

                if(k == 0) i--;

                if(!form.A.value) A = 0; 

                        else A = eval(form.A.value);

                if(A == 0) i--;

                if(!form.d.value) d = 0;

						else d = eval(form.d.value);

                if(d == 0) i--;

                if(!form.N.value) N = 0;

                        else N = eval(form.N.value);

                if(N == 0) i--;

                if(!form.C.value) C = 0;

                        else C = eval(form.C.value);

                if(C == 0) i--;
		C = (0.2235 * ((k * A) / d)) * (N - 1);
                showresults(form);  
                form.d.focus();
                return;
			}


</SCRIPT>
<!-- Scrip capacitor parallel plate-->
<!-- Scrip poliyester capacitor-->

<SCRIPT language=JavaScript>
<!--
function c() {
   t = 20; nF = 1000; uF = nF*nF;
   n1 = document.res.first.selectedIndex;
   n2 = document.res.second.selectedIndex;
   n3 = document.res.mult.selectedIndex;
   idx =document.res.tol.selectedIndex;
   if(idx == 1) t = 10;
  
   result = ((n1 * 10)+n2)*(Math.pow(10,n3));
   if(result == "") return;

   if(result < nF) fact = "pF";
   if((result >= nF) && (result < uF)) {
      result = result/nF;
      fact = "nF";
   }
   if(result >= uF) {
      result = result/uF;
      fact = "uF";
   }
   r1 = result - ((t * result)/100);
   r2 = result + ((t * result)/100);
   r1 = parseInt((r1*100)+0.5)/100;
   r2 = parseInt((r2*100)+0.5)/100;
   
   txt = " "+r1+" to "+r2+" "+fact;
   txt = txt + "    ("+result+" "+fact+"  +/-"+t+"%)";
   document.res.ans.value = txt;
}
//-->
</SCRIPT>

<!-- Scrip poliyester capacitor-->