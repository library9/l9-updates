﻿<?php
include("include.php");  // read css and js folders, sets database variables

$table=$_GET['TBL'];
$item=$_GET['ITM'];

mysql_connect($host,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$result = mysql_query("SELECT PN, `Part Number`, Description, `Library Ref`, Value, `Footprint Ref`, Manufacturer,`Manufacturer Part Number`, ComponentLink1URL FROM $table WHERE ITEM=$item");

if (!$result) {
    echo 'Could not run query: ' . mysql_error();
    exit;
}

$row = mysql_fetch_row($result);

mysql_close();
?>

<br/><br/>
<center><h2><b>EDIT <?php echo strtoupper($table); ?></b></h2></center>

<center>
<form name="editpart" action="partedit-done.php?ITM=<?php echo $item; ?>&TBL=<?php echo $table; ?>" method="post">
<table border="0" cellspacing="2" cellpadding="2">
<tbody>
     <tr>
      <td>Part Number</td>
	  <td><input type="text" name="partnumber" value="<?php echo "$row[0]"?>"></td>
    </tr>
    <tr>
      <td>Part Name</td>
	  <td><input type="text" name="partname" value="<?php echo "$row[1]"?>"></td>
    </tr>
    <tr>
      <td>Description</td>
      <td><input type="text" name="desc" value="<?php echo "$row[2]"?>"></td>
    </tr>
    <tr>
      <td>Schematic Symbol</td>
      <td><input type="text" name="sym" value="<?php echo "$row[3]"?>"></td>
    </tr>
    <tr>
      <td>Value</td>
      <td><input type="text" name="value" value="<?php echo "$row[4]"?>"></td>
    </tr>
	<tr>
      <td>Footrint</td>
      <td><input type="text" name="footprint" value="<?php echo "$row[5]"?>"></td>
    </tr>
	<tr>
      <td>Manufacturer</td>
      <td><input type="text" name="manu" value="<?php echo "$row[6]"?>"></td>
    </tr>
	<tr>
      <td>Manufacturer Part Number</td>
      <td><input type="text" name="manu_pn" value="<?php echo "$row[7]"?>"></td>
    </tr>
	<tr>
      <td>PDF Link</td>
      <td><input type="text" name="pdflink" value="<?php echo "$row[8]"?>"></td>
    </tr>
	<tr>
</tbody>
</table>
</center>
<br><br>
<center>
<table border="0"  width="50%" cellpadding="2" cellspacing="2">
	<tr>
		<td align="center"><input type="submit" value="Submit Edit"></td>
			<td><input type="button" onClick="location.href='view1x0.php?TBL=<?php echo $table; ?>'" value='Cancel'></td>
			</form>
		</tr>
</table>
</center>

