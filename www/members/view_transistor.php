
<?php
$groupswithaccess="ladmin,llib";
$loginpage="../index.php";
$logoutpage="../index.php";
require_once("../slpw/sitelokpw.php");

include("include.php");  // read css and js folders, sets database variables
session_start();

$table=$_GET['TBL'];
$_SESSION['table']=$table;



include("header.php");
include("sidebar.php");
?>

<link rel="stylesheet" type="text/css" href="../css/sortstyle.css">

<style type="text/css" class="include" title="currentStyle">

    @import "DataTables-1.10.0/extensions/Editor-1.3.0/css/dataTables.editor.css";


    td.details-control {
        background: url('images/editpage.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('images/closefile.png') no-repeat center center;
    }
    TD {font-size:10px}


</style>
<link href="css/popup.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="css/dataTables.jqueryui.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="js/jquery.min_online.js"> </script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/popup1.js"></script>

 
 <script src="js/jquery-1.11.1.min.js" language="javascript" type="text/javascript"></script> 
<script src="js/jquery.dataTablesonline.min.js" language="javascript" type="text/javascript"></script>
<script src="js/dataTables.jqueryui.js" language="javascript" type="text/javascript"></script>
<script class="include" type="text/javascript" charset="utf-8" src="DataTables-1.10.0/extensions/Editor-1.3.0/js/dataTables.editor.js"></script>
<script type="text/javascript" charset="utf-8">
var editor;


    $(document).ready(function() {
	

        editor = new $.fn.dataTable.Editor( {
		    
            "ajax": "mi_data.php",
			"table": "#view",
            "fields": [
                {
                    "label": "Part Number",
                    "name": "<?php echo $table; ?>.Part_Number"
                    
                },
                {
                     
                     "label": "CPN",
                     "name": "<?php echo $table; ?>.CPN"
                    
                 },				 
				 
				
				  {
                    "label": "Library_Path",
                    "name": "<?php echo $table; ?>.Library_Path"
                    
                },
					 {
				      
                     "label": "Category",
                     "name": "<?php echo $table; ?>.Category"
					                     
                 },
                
                {
                    "label": "Comment",
                    "name": "<?php echo $table; ?>.Comment"
                },
               
                {
                    "label": "Component_Kind",
                    "name": "<?php echo $table; ?>.Component_Kind"
                },

                {
                    "label": "Component_Type",
                    "name": "<?php echo $table; ?>.Component_Type"
                },
                {
				      "type": "hidden",
                     "label": "company_id",
                     "name": "<?php echo $table; ?>.company_id",
					 "def": "<?php echo $table; ?>.company_id"
                     
                 },
				 {
				      "type": "hidden",
                     "label": "company_id",
                     "name": "<?php echo $table; ?>.date",
					 "def": "<?php echo $table; ?>.date"
                     
                 },
                {
                    "label": "Description",
                    "name": "<?php echo $table; ?>.Description"
                },
				  {
                    "label": "Designator",
                    "name": "<?php echo $table; ?>.Designator"
                    
                },
               
				   {
                    "label": "Footprint",
                    "name": "<?php echo $table; ?>.Footprint"
                    
                },             
				 
				 {				      
                     "label": "Library Reference",
                     "name": "<?php echo $table; ?>.Library_Ref"
					                     
                 },
				  {
                    "label": "Impedance(Max)Zzt",
                    "name": "<?php echo $table; ?>.Impedance_Max_Zzt"
                },
                {
                    "label": "Manufacturer",
                    "name": "<?php echo $table; ?>.Manufacturer"
                },
                {
                    "label": "Manufacturer PN",
                    "name": "<?php echo $table; ?>.Manufacturer_Part_Number"
                },
                
                {
                    "label": "Manufacturer1",
                    "name": "<?php echo $table; ?>.Manufacturer1"
                },
               
                {
                    "label": "Manufacturer_Part_Number1",
                    "name": "<?php echo $table; ?>.Manufacturer_Part_Number1"
                },
				 {
                    "label": "Mounting_Type",
                    "name": "<?php echo $table; ?>.Mounting_Type"
                },
				 {
                    "label": "Package_Case",
                    "name": "<?php echo $table; ?>.Package_Case"
                },
				
                {
                    "label": "Packaging",
                    "name": "<?php echo $table; ?>.Packaging"
                },
              {
                    "label": "Pin_Count",
                    "name": "<?php echo $table; ?>.Pin_Count"
                    
                },
				  {
                    "label": "RoHS",
                    "name": "<?php echo $table; ?>.RoHS"
                    
                },
						
                {                     
                     "label": "Signal_Integrity",
                     "name": "<?php echo $table; ?>.Signal_Integrity"
                    
                 },
				  {
                    "label": "Simulation",
                    "name": "<?php echo $table; ?>.Simulation"
                    
                },
				  {
                     
                     "label": "Supplier",
                     "name": "<?php echo $table; ?>.Supplier"
                    
                 },
				 
				 {
				      
                     "label": "Supplier_Part_Number",
                     "name": "<?php echo $table; ?>.Supplier_Part_Number"
					                     
                 },
				   {
                    "label": "Supplier_Part_Number1",
                    "name": "<?php echo $table; ?>.Supplier_Part_Number1"
                    
                },
                {
                     
                     "label": "Supplier1",
                     "name": "<?php echo $table; ?>.Supplier1"
                    
                 },
                 
                {
                    "label": "Footprint_Path",
                    "name": "<?php echo $table; ?>.Footprint_Path"
                },
                {
                    "label": "Footprint_Ref",
                    "name": "<?php echo $table; ?>.Footprint_Ref"
                },
            
				   {
                     
                     "label": "Height",
                     "name": "<?php echo $table; ?>.Height"
                    
                 },
				  {
                     
                     "label": "Value",
                     "name": "<?php echo $table; ?>.Value"
                    
                 },			 
                   
				   {
                    "label": "Revision",
                    "name": "<?php echo $table; ?>.revision"
                    
                },
				
				  {
                    "label": "ComponentLink1Description",
                    "name": "<?php echo $table; ?>.ComponentLink1Description"
                    
                },
                {
				      
                    "label": "Datasheet",
                    "name": "<?php echo $table; ?>.ComponentLink1URL",
					 "def": "<?php echo $table; ?>.ComponentLink1URL"
                },
				/* {
                "label": "Starter Include",
                "name": "<?php echo $table; ?>.starter",
                "type": "select",
                "ipOpts": [
                    { "label": "0", "value": "0" },
                    { "label": "1", "value": "1" }
                ]
            }, */
               {
                "label": "Edit Status",
                "name": "<?php echo $table; ?>.status",
                "type": "select",
                "ipOpts": [
                    { "label": "GREEN-APPROVED", "value": "green" },
                    { "label": "RED-UNAPPROVED", "value": "red" },
                    { "label": "YELLOW-SUBMITTED", "value": "yellow" },
                    { "label": "ORANGE-IMPORTED", "value": "orange" },
                    { "label": "BLUE-PROTOTYPE", "value": "blue" }
                ]
            }
				
				 
				
            ]
        } );
var openVals;
    editor
	 .on( 'open', function () {
            // Store the values of the fields on open
            openVals = JSON.stringify( editor.get() );
			
        } )
	

        // New record
        $('a.editor_create').on('click', function (e) {
            e.preventDefault();

           editor
                .title( 'Create new record' )
                .buttons( { "label": "Add", "fn": function () { editor.submit();
                   } } )
                .create();

        } );

        // Edit record
        $('#view').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();
            var $tds = $(this).closest('tr').find('td');
			var updt='';
            editor
                .title( 'Edit record' )
                .buttons( { "label": "Update", "fn": function () { editor.submit()
                  function revision(){
				var mpn=$tds.eq(5).text();
					var pn=$tds.eq(2).text();
					
				 if ( openVals !== JSON.stringify( editor.get() ) ) {
			var post_data=JSON.stringify( editor.get() );
			var data1=JSON.parse(post_data);
			var json = JSON.parse(openVals);
			/* var arr = $.map(json, function(el,a) { 
               

			console.log(el);
			console.log(a);})
			 */
			 $.each(json, function (i,v)
				{
				  $.each(data1,function(val,ele){
				    if(i==val){
					 if(v!==ele){
                       updt=updt+'@'+val+': Old Value-'+v+' ,New Value- '+ele;
					   
					 }
					}
				  
				  });
				});
				$.ajax({
                    url : 'revision_data.php?TBL=<?php echo $table; ?>',
                    type : 'get',
					  async: false,
                    data : {
					    'pn':pn,
                        'mpn' : mpn,
                        'updt' : updt                    

                    },
                    success : function(result){
					      console.log(result);
                         $('#view').DataTable().ajax.reload(); 

                    },
                    error : function(e){
                        console.log(e);
                    }
                });
				
               
            }				
				}	
            setTimeout( revision, 1500 ); 


				} } )
                .edit( $(this).closest('tr') );
        } );

        // Delete a record (without asking a user for confirmation for this example)
        $('#view').on('click', 'a.editor_remove', function (e) {
            e.preventDefault();
               var $tds = $(this).closest('tr').find('td');
							var mpn=$tds.eq(5).text();
				 var pn=$tds.eq(2).text();
				 var cat_name='<?php echo $table;?>';
            editor
                .title( 'Delete record' )
                .message( 'Are you sure you wish to remove this record?' )
                .buttons( { "label": "Delete", "fn": function () { editor.submit()
                              $.ajax({
										url : 'del_part1.php',
										type : 'get',
										
										data : {
											'mpn' : mpn ,
                                            'cat_name' : cat_name,                                       
                                            'pn' : pn 											

										},
                    success : function(result){
					 
                        console.log(result);

                    },
                    error : function(e){
                        console.log(e);
                    }
                });


				} } )
                .remove( $(this).closest('tr') );
        } );

        // DataTables init
		 var fa = 0;
       var table = $('#view').DataTable( {            
                "sdom": 'T<"clear"><"H"lfr>t<"F"ip>',
           "bAutoWidth": false,
                "bProcessing": true,
				 "aLengthMenu": [[10, 50, 100, 500], [10, 50, 100, 500]],
                //"bPaginate": false,
               // "bAutoWidth": false,
				
              ajax: {
               url: "mi_data.php",
               type: "POST"
                },
                
                "columns": [
{
                "bSearchable": false,
                "bSortable": false,
                "mRender":function(data, type, full)
                {
                    if ( full.<?php echo $table; ?>.status === 'yellow')
                    {
                        return '<center><img src="../images/yellow.png" title="SUBMITTED'+full.<?php echo $table; ?>.date+'"></center>';
                    }
                    else if(full.<?php echo $table; ?>.status === 'green')
                    {
                        return '<center><img src="../images/green.png" title="ACTIVE" ></center>';
                    }
					else if(full.<?php echo $table; ?>.status === 'orange')
                    {
                        return '<center><img src="../images/orange.png" title="Imported" ></center>';
                    }
					  else if(full.<?php echo $table; ?>.status === 'red')
                    {
                        return '<center><img src="../images/red.png" title="ISSUE"></center>';
                    }else if(full.<?php echo $table; ?>.status === 'blue')
                    {
                        return '<center><img src="../images/blue.png" title="PROTOTYPE"></center>';
                    }
                }

            },
                {
                "data": "<?php echo $table; ?>.Part_Number"
                
                },

               
                {
                    "data": "<?php echo $table; ?>.CPN"
                },
               
                {
				         "bVisible": false,
                         "sClass": "center",
                        "data": "<?php echo $table; ?>.Library_Ref"
                },
				
                {
                    "sClass": "center",
                    "data": "<?php echo $table; ?>.Description"
                },
                {
                    "sClass": "center",
                    "data": "<?php echo $table; ?>.Manufacturer"
                },
                {
                    "sClass": "center",
                    "data": "<?php echo $table; ?>.Manufacturer_Part_Number"
                },
				  {
                    "bSearchable": false,
                    "bSortable": false,
                    "mRender":function(data, type, full)
                    {
                        if ( '<?php echo $slgroupname[0]; ?>' === 'ladmin' || '<?php echo $slgroupname[0]; ?>' === 'llib')
                        {
                            return '<center><a href="" class="editor_edit"><img src="images/pencil.png" width="16"></a>&nbsp&nbsp&nbsp<a href="" class="editor_remove"><img src="images/delete2.png" width="16"></a></center>';
                        }
                         else
                        {
						  return '<center><img src="images/pencil-grey.png" width="16">&nbsp&nbsp&nbsp<img src="images/delete2-grey.png" width="16"></a></center>';
						}
                    }

                }
              
                ]
			} );
} );
       
</script>
</head>
		<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">

					<div class="statusbar"><p>Current Page - <strong>Parts Editor / <?php if($table==capacitor){ ?>Capacitor
		<?php } 
		else if($table==resistor){ ?>Resistor
		<?php }
		else if($table==misc){ ?>Misc
		
	<?php	}
	else if($table==connector){ ?>Connector
	<?php	}
	else if($table==ic){ ?>IC
	<?php	}
	else if($table==diode){ ?>Diode
	<?php	}
	else if($table==transistor){ ?>Transistor
	<?php	}
	else if($table==oscillator){ ?>Oscillator
	<?php	}?> </strong></p></div>

<div class="returnstat"><a href="part_editor.php" class="menu_click">Return</a></div>
	</div>
</div>
<div class="app_content">
<center>
<table  cellspacing="0" cellpadding="0" class="display" id="view" >
<thead>
	<tr> 
		<th >Status</th>
	    <th >Part Name</th>
		<th >PN</th>
		<th  >Library Ref</th>
		<th >Description</th>
		<th >Manufacturer</th>
		<th >Manufacturer PN</th>
		<th>Admin</th>
	</tr>
</thead>

</table>
</center>


</div>
</div>
</div>

 