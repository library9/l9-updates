
<?php
$groupswithaccess="PUBLIC";
$loginredirect=2;
require_once("../slpw/sitelokpw.php");

?>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Library9.com</title>
<link rel="shortcut icon" href="images/favicon.png"/>
<link href="../styles.css" rel="stylesheet" type="text/css" media="screen" />
<!--<script type="text/javascript" src="javascript/jquery-1.6.1.min.js"></script>-->
   <script src="js/modernizr-2.6.1.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
	 
    
    <script src="members/js/lean-slider.js"></script>
	  <script src="js/lean-slider.js"></script>

<style type="text/css">
	* {
		margin: 0;
		padding: 0;
	}
	/* @group Customer Login Form */
	/* customer login boxes */
	h1.loginboxcust {
		padding: 0 0 0 0;
		margin: -10px 0 20px 0;
		color: #ebebeb;
		font: bold 30px Arial,Helvetica,sans-serif;
		text-align: left;
	}
	label.loginboxcust {
		display: inline-block;
		position: relative;
		width: 96px;
		text-align: left;
		margin: 0 0 0 0;
		padding: 10px 10px 0 0;
		vertical-align: middle;luser
		text-transform: capitalize;
		font-variant: small-caps;
		font-family: "Lucida Grande",Lucida,Verdana,sans-serif;
		color: #646464;
	}
	input.loginboxcust {
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 2px 3px 3px #0f0f0f;
		-moz-box-shadow: 2px 3px 3px #0f0f0f;
		box-shadow: 2px 3px 3px #0f0f0f;
		display: inline-block;
		position: relative;
		font-size: 14px;
		width: 204px;
		height: 20px;
		line-height: 20px;
		padding: 2px;
		margin: 10px 0 0 0;
		border: 1px solid #0d2c52;
		background-color: #c4c4c4;
		font-size: 16px;
		color: #444545;
		vertical-align: middle;
	}
	input.loginboxcust:focus {
		border: 1px solid #00b1ed;
		background-color: #eff2f1;
	}
	/* end login boxes for customer page */
	

	/* @group LogoutButton	 */
		a.buttonlogout {
			-moz-box-shadow:inset 0px 1px 0px 0px #f29c93;
			-webkit-box-shadow:inset 0px 1px 0px 0px #f29c93;
			box-shadow:inset 0px 1px 0px 0px #f29c93;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #fe1a00), color-stop(1, #ce0100) );
			background:-moz-linear-gradient( center top, #fe1a00 5%, #ce0100 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fe1a00', endColorstr='#ce0100');
			background-color:#fe1a00;
			-moz-border-radius:6px;
			-webkit-border-radius:6px;
			border-radius:6px;
			border:1px solid #d83526;
			display:inline-block;
			color:#ffffff;
			font-family:arial;
			font-size:14px;
			font-weight:bold;
			padding:6px 24px;
			text-decoration:none;
			text-shadow:1px 1px 0px #b23e35;
		}
		a.buttonlogout:hover {
			background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ce0100), color-stop(1, #fe1a00) );
			background:-moz-linear-gradient( center top, #ce0100 5%, #fe1a00 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ce0100', endColorstr='#fe1a00');
			background-color:#ce0100;
		}
		a.buttonlogout:active {
			position:relative;
			top:1px;
		}
			a.buttonpassword {
			-moz-box-shadow:inset 0px 1px 0px 0px #f29c93;
			-webkit-box-shadow:inset 0px 1px 0px 0px #f29c93;
			box-shadow:inset 0px 1px 0px 0px #f29c93;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #fe1a00), color-stop(1, #ce0100) );
			background:-moz-linear-gradient( center top, #fe1a00 5%, #ce0100 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fe1a00', endColorstr='#ce0100');
			background-color:#fe1a00;
			-moz-border-radius:6px;
			-webkit-border-radius:6px;
			border-radius:6px;
			border:1px solid #d83526;
			display:inline-block;
			color:#ffffff;
			font-family:arial;
			font-size:11px;
			font-weight:bold;
			height: 15px;
			line-height: 15px;
			padding: 4px 10px;
			text-decoration:none;
			text-shadow:1px 1px 0px #b23e35;
			margin-top: 5px;
		margin-left: 108px;
		}
		a.buttonpassword:hover {

		}
		a.buttonpassword:active {
			position:relative;
			top:1px;
		}

	input[type="submit"] {
	
		-moz-border-radius:4px;
		-webkit-border-radius:4px;
		border-radius:4px;
		border:1px solid #84bbf3;
		display:inline-block;
		color:#fff;
		font-family:arial;
		font-size:14px;
		font-weight:bold;
		padding:8px 0px;
		text-decoration:none;

		margin-top: 10px;
		margin-left:0px;
		width:210px;
	}
	input[type="submit"]:hover {

		background-color:#3792bf;
	}
	input[type="submit"]:active {
		position:relative;
		top:1px;
		}
	/* @end */
	</style>

</head>
<body>
<div id="header-ff">
<div id="headercontainer">

	
    </div><!-- end navigation -->
</div>
<div id="content">
<div id="logoutcontentcontainer">
<br>
<div id="droplineMenu">
<ul id="menuOuter">
	<div class="menu_logo">
<a href="../index.php">	<img src="../images/nav_logo.png" alt="Demo Library" border="0" width="45" height="40"></a>
	</div>
	<li class="lv1-li">
	<!--[if lte IE 6]><a class="lv1-a" href="#url"><table><tr><td><![endif]-->
		<ul>
			<li><a href="../services.php"><b>SERVICES</b></a></li>
			<!--<li><a href="http://library9.com/about.php"><b>ABOUT US</b>[if gte IE 7]><!--><!--<![endif]-->
			<!--[if lte IE 6]><table><tr><td><![endif]-->
		<!--	<div><b></b>
				<ul>
					<li><a href="#url">PCB Trace Width Calculator</a></li>
					<li><a href="#url">Wire Parameter Calculator</a></li>
					<li><a href="#url">Transmission Line Calculator</a></li>
					</ul>
			</div>-->
			<!--[if lte IE 6]></td></tr></table></a><![endif]-->
			</li>
				<li><a href="../contact.php"><b>CONTACT</b></a></li>
				<li><a href="../support.php"><b>SUPPORT</b></a></li>
			<li><a href="register.php"><b>REGISTER</b><!--[if gte IE 7]><!--></a><!--<![endif]-->
			<!--[if lte IE 6]><table><tr><td><![endif]-->
			<!--<div><b></b>
				<ul>
					<li><a href="#url">parts</a></li>
					<li><a href="#url">library</a></li>
					<li><a href="#url">BOM</a></li>
					<li><a class="last" href="#url">Where Used</a></li>
						<li><a class="last" href="#url">Part building service</a></li>
				</ul>
			</div>-->
			<!--[if lte IE 6]></td></tr></table></a><![endif]-->
			</li>
			
			
			
			
		</ul>
	<!--[if lte IE 6]></td></tr></table></a><![endif]-->
	</li>
<!--	<div class="menu_search1">
		<form>
		<input type="text" placeholder="search">
		<input type="button" value="" src="../images/search_icon.png" width="20" height="20">
		</form>
	</div>-->
</ul>
</div>
<br /><br /><br />
<div class="middle_container">
			<div class="container">
			<div class="pageheading"><h2>Terms of use</h2></div>

					<div class="common_content">
<p>This Agreement is by and between you and Library9.com. The Library9.com web site (this &quot;Site&quot;) is offered for your use subject to your acceptance of the following terms and conditions (&quot;Terms of Use&quot;). Please read them carefully. Your use of this Site constitutes your agreement to these Terms of Use. In this Agreement, &quot;you&quot; refers to an individual accessing this Site, regardless of any particular Library9.com products or services purchased or licensed. To the extent applicable, &quot;you&quot; also includes the corporation or other legal entity (your &quot;Company&quot;), if any, on whose behalf you are accessing this Site.</p>
</div>
 
			<div class="pageheading"><h2>Changes to Terms of Use</h2></div>

<div class="common_content">
 
<p>This Agreement may be updated by Library9.com from time to time. All such updates and changes are effective immediately upon notice thereof, which we may give by any means, including, but not limited to, posting a revised version of this Agreement or other notice on this Site. You should review this Agreement often to stay informed of changes that may affect you, as your continued use of this Site signifies your continuing consent to be bound by the terms of this Agreement. We expressly reserve the right to make any changes to this Agreement, or to the Site and its contents, at any time without prior notice to you.</p>
</div>
 
			<div class="pageheading"><h2>Privacy Policy</h2></div>

 <div class="common_content">
<p>Information that you provide or that we collect about you and your Company through your access to and use of this Site is subject to our Privacy Policy, the terms of which are hereby incorporated into this Agreement by reference.</p>
</div>
 


			<div class="pageheading"><h2>Copyright and Trademark Notices</h2></div>
<div class="common_content">
 
<p>This Site, all Content and the copyright therein is owned by Library9,LLC and/or third party product or service providers. Library9.com and other names and designations of Library9,LLC or any of its services referenced on this Site are each service marks or trademarks of Library9,LLC. Other company names and other product or service names or designations referenced on this Site may be the service marks or trademarks (or registered service marks or trademarks) of their respective owners. All rights in this Site and the Content are reserved by Library9.com.</p>
</div>
 
 

			<div class="pageheading"><h2>Access, Registration and Use</h2></div>
<div class="common_content"> 
<p>You agree that all information that you provide to Library9.com in connection with your access to, use of and registration with the Site is true, accurate and complete to the best of your knowledge, belief and ability, including information regarding products you list on the Site, and that you will maintain and routinely update such information to keep it true, accurate and complete at all times. We reserve the right to terminate this Agreement and to refuse, restrict or discontinue service or access to the Site, or any portion or features of this Site, to you or any other person or entity, for any reason or for no reason whatsoever, at any time, and without notice or liability, including, but not limited to, termination of your use of the Site, in the event that any information provided by you is, at such time or at any time thereafter, untrue, inaccurate or incomplete or if you otherwise fail to comply with the terms and conditions of this Agreement or any license, purchase order or other agreement that you have with us.</p>
</div>
 
 
 

			<div class="pageheading"><h2>Product Listings and other Submissions</h2></div>
 <div class="common_content">
 
<p>Library9.com does not sponsor or endorse any of the products listed on this Site and is not affiliated with the manufacturers or vendors of those products Library9.com is not responsible for the accuracy, content or any aspect of the products listings posted on the Site. Library9.com disclaims all liability and make no representations or warranties for any products or services made available, sold or provided to you by any third party through the Site. Your sale or purchase of, or offers to sell or purchase products or services through the Site is subject to the terms and conditions agreed to by you and the respective seller or buyer. By submitting information, product listings or any other material to the Site or to Library9.com, you warrant that you have the right to submit such material and expressly grant Library9.com a royalty-free, perpetual, irrevocable, non-exclusive right and license to use, make and have made, reproduce, modify, adapt, publish, translate and distribute such material (in whole or in part) worldwide and to incorporate it in other works in any form, media or technology now known or hereafter developed, subject to our Privacy Policy, as set forth on this Site.</p>
</div>
 
 
 

			<div class="pageheading"><h2>Compliance with Law</h2></div>
 <div class="common_content">
 
<p>You access and use the Site and the materials, products and services available on or through the Site or Library9.com at your own initiative and risk. In connection with your access to and use of the Site and that of any person authorized by you to use the Site, you are responsible for compliance with all applicable laws, regulations and policies of all relevant jurisdictions. Recognizing the global nature of the Internet, you agree to comply with all applicable local rules regarding online conduct and acceptable content. You expressly agree to comply with all applicable laws regarding the transmission of data or information out of the country in which you reside. You further agree not to list any products for sale that you know or should know are counterfeit or infringe any rights of third parties. These assurances and commitments by you shall survive termination of this Agreement.</p>
</div>
 
 
 

			<div class="pageheading"><h2>Security</h2></div>
 <div class="common_content">
<p>Library9.com takes commercially reasonable measures to secure and protect information transmitted to and from the Site, however we cannot and do not guarantee that any such communications or any electronic commerce conducted on or through the Site is or will be totally secure. You are responsible for maintaining the confidentiality of any login or ID (your &quot;User ID&quot;) and password, and you are fully responsible for all access and any activity that occurs through use of your User ID or password. You agree to immediately notify Library9.com of any unauthorized use of your User ID or password or any other breach of Site security of which you become aware. Library9.com cannot and will not be liable for any loss or damage arising from any unauthorized access or use of your User ID and password.</p>
</div>
 
 
 

			<div class="pageheading"><h2>Copyright Agent</h2></div>
 <div class="common_content">
<p>Library9.com has registered a designated agent with the United States Copyright Office pursuant to the Digital Millennium Copyright Act (17 U.S.C. § 512(c)) and if you believe that your copyrighted material is being used on this Site without required permission, please notify Library9.com&acute;s designated agent at:</p>
</div>
 
 
 

			<div class="pageheading"><h2>Library9.com</h2></div>
<div class="common_content"> 
<p>Attn:   support</p>
 
<p>Address:  3720 Gattis School Road # 800-210 Round Rock, Texas 78664 </p>
 
<p>Email: support@Library9.com</p>
</div>
 
 			<div class="pageheading"><h2>Disclaimer of Warranties</h2></div>
 <div class="common_content">
 
<p>You understand that your access to and use of the Site and all materials, products and services available through it are at your own initiative and risk. It is your responsibility to take precautions to ensure that any information, materials, software, products or data that you access, use, download, upload, purchase or otherwise obtain on or through the Site are: (i) up-to-date, accurate, complete, reliable, genuine, and suitable to and appropriate for the purpose for which you, or your Company, may desire to use them. ALL MATERIALS, PRODUCTS AND SERVICES AVAILABLE ON OR THROUGH THIS SITE ARE PROVIDED &quot;AS IS&quot; AND &quot;AS AVAILABLE,&quot; WITHOUT ANY WARRANTIES OR GUARANTIES WHATSOEVER, WHETHER EXPRESS OR IMPLIED. Library9.com DISCLAIMS ALL WARRANTIES, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND INFRINGEMENT. Library9.com DOES NOT REPRESENT, WARRANT OR COVENANT THAT THIS SITE AND THE MATERIALS, PRODUCTS AND SERVICES AVAILABLE ON OR THROUGH THIS SITE AND ARE OR WILL BE ACCURATE, CURRENT, COMPLETE, RELIABLE, APPROPRIATE FOR ANY PARTICULAR USE TO WHICH YOU, OR YOUR COMPANY, MAY CHOOSE TO PUT THEM, OR THAT THEY ARE OR WILL BE AVAILABLE ON AN UNINTERRUPTED AND ERROR-FREE BASIS, THAT DEFECTS WILL BE CORRECTED, OR THAT THIS SITE AND THE MATERIALS, PRODUCTS AND SERVICES AVAILABLE ON OR THROUGH THIS SITE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.</p>
</div>
 
 
 

			<div class="pageheading"><h2>Limitation of Liability</h2></div>
			<div class="common_content">
 
<p>NEITHER Library9.com, ITS SUBSIDIARIES, CONTRACTORS, SUPPLIERS, CO-BRANDERS AND OTHER SIMILAR ENTITIES, NOR THE OFFICERS, DIRECTORS, EMPLOYEES, REPRESENTATIVES AND AGENTS OF ANY OF THE FOREGOING SHALL BE LIABLE TO YOU, YOUR COMPANY, OR ANY THIRD PARTY FOR ANY LOSS, COST, DAMAGE OR OTHER INJURY, WHETHER IN CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR OTHERWISE, ARISING OUT OF OR IN CONNECTION WITH: (A) YOUR USE, NEGLIGENT USE OR NON-USE, OR YOUR RELIANCE ON OR FAILURE TO RELY ON THE SITE, THE CONTENT, AND THE MATERIALS, PRODUCTS AND SERVICES ACCESSIBLE, ACCESSED OR USED ON OR THROUGH THE SITE OR Library9.com; (B) Library9.com’S PERFORMANCE OF OR FAILURE TO PERFORM ITS OBLIGATIONS UNDER OR IN CONNECTION WITH THIS AGREEMENT, OR (C) YOUR PURCHASE, OFFER TO PURCHASE, SALE, OFFER FOR SALE, OR USE OF ANY GOODS OR SERVICES PROVIDED BY THIRD PARTIES THROUGH Library9.com. UNDER NO CIRCUMSTANCES SHALL Library9.com BE LIABLE TO YOU, YOUR COMPANY OR ANY THIRD PARTY FOR ANY INDIRECT, CONSEQUENTIAL, INCIDENTAL, PUNITIVE, SPECIAL OR SIMILAR DAMAGES OR COSTS (INCLUDING, BUT NOT LIMITED TO, LOST PROFITS OR DATA, LOSS OF GOODWILL, LOSS OF OR DAMAGE TO PROPERTY, LOSS OF USE, BUSINESS INTERRUPTION AND CLAIMS OF THIRD PARTIES, ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT, THE USE, NEGLIGENT USE OR NON-USE OF MATERIALS, PRODUCTS AND SERVICES AVAILABLE ON OR THROUGH THE SITE OR Library9.COM, THE USE, COPYING OR DISPLAY OF THIS SITE OR THE CONTENT, TRANSMISSION OF INFORMATION TO OR FROM THE SITE OVER THE INTERNET, OR ANY OTHER CAUSE BEYOND THE CONTROL OF Library9.com, EVEN IF Library9.com WAS ADVISED, KNEW OR SHOULD HAVE KNOWN OF THE POSSIBILITY OF SUCH DAMAGES OR COSTS. IN A jurisdiction that does not allow the exclusion or limitation of liability for certain damages, the liability of Library9.com shall be limited in accordance with this Agreement to the fullest extent permitted by law. Without limiting any of the foregoing, if Library9.com is found liable to you or any third party AS A RESULT OF any claims or OTHER matters arising under or in connection with this Agreement, Library9.com&acute;s aggregate and maximum liability for all such CLAIMS AND OTHER matters in any calendar year shall not exceed $100.</p>
</div>
 
 
 

			<div class="pageheading"><h2>Indemnification</h2></div>
 <div class="common_content">
<p>You agree to indemnify, defend and hold Library9.com harmless from and against all claims, demands, suits or other proceedings, and all resulting loss, damage, liability, cost and expense (including reasonable attorneys&acute; fees), made by any third party due to or arising out of content, data or information you submit, post to or transmit through the Site, your access to and use, negligent use or non-use of the Content, the Site and other materials, products and services available on or through the Site and Library9.com, your violation of this Agreement or your violation of any rights of another. We reserve, and you grant to us, the right to assume exclusive defense and control of any matter subject to indemnification by you. All rights and duties of indemnification set forth herein shall survive termination of this Agreement.</p>
 
 </div>
 

			<div class="pageheading"><h2>Governing Law, Jurisdiction, Venue</h2></div>
 <div class="common_content">
<p>These Terms of Use will be governed by and construed in accordance with the laws of the State of Texas, without giving effect to the principles of conflicts of law. You hereby irrevocably consent to the exclusive jurisdiction and venue of any state or federal court located in Travis County in the State of Texas, United States of America, with respect to all disputes arising out of or relating to the use of this Site or these Terms of Use. Your use of this Site is unauthorized in any jurisdiction that does not give effect to all of the provisions of these Terms of Use, including, but not limited to, the provisions of this paragraph.</p>
 </div>
 
 

			<div class="pageheading"><h2>Miscellaneous</h2></div>
 <div class="common_content">
<p>These Terms of Use and the Library9.com Privacy Policy referenced herein (as each may be revised and amended from time to time), collectively constitute the entire agreement with respect to your access to and use of the Site and the materials, products and services available on or through the Site or Library9.com. These Terms of Use do not confer any rights, remedies or benefits upon any person other than you and Library9.com. Library9.com may assign its rights and duties under this Agreement at any time to any party without notice. You may not assign this Agreement without the prior written consent of Library9.com. These Terms of Use shall be binding on and inure to the benefit of the parties hereto and their respective successors and assigns. Should any provision of these Terms of Use be held to be void, invalid, unenforceable or illegal by a proper legal authority, the validity and enforceability of the other provisions shall not be affected.</p>
 </div>
 
 

			<div class="pageheading"><h2>Acknowledgement:</h2></div>
<div class="common_content">
<p>YOU AGREE TO BE BOUND BY THE TERMS AND CONDITIONS OF THIS AGREEMENT AND THE Library9.com PRIVACY POLICY REFERENCED HEREIN.</p>
</div>
	
  
</div><!-- end contentcontainer -->
</div></div>
<!-- end content -->
<div id="footer">
<div id="footercontainer" style="font-size:11px;">
		<div class="bottom_nav">
			<a href="../services.php">Services</a>
			
			<a href="../contact.php">Contact</a>
			<a href="../support.php">Support</a>
			<a href="register.php">Register</a>						
			<a href="#">Privacy Policy</a>			
			<a href="#">Terms of Use</a>

		</div>

  
<p>&copy;Copyright 2014 Library9.com All rights reserved. Library9.com, the Library9.com logo and all other Library9.com marks contained herein are trademarks of Elise Software Corporation.
   <br>Altium Designer, Copyright &copy; 2014 Altium Limited<br>
   </p>
<div class="pay_icons">
						<!--<a href="#"><img src="../images/paypal.jpg" width="56" height="35"></a>
			<a href="#"><img src="../images/authorize.png" width="44" height="35"></a>-->
			<a href="#"><span id="siteseal"><script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=bDIBmMC3oEV0owpDZHkoWvisVCWWHJNRU90SEVMrX85rymT7Icco7BUsG0Q"></script></span></a>	
			<a href="http://www.bbb.org/central-texas/business-reviews/computer-software-publishers-and-developers/elise-software-corporation-in-lago-vista-tx-90111906" ><img src="images/bbb.png" width="80" height="41" ></a>	
			<a href="#"><img src="images/ipc.jpg" width="77" height="42"/></a>	
			<a href="#"><img src="../images/octopart_badge.jpg" width="125" height="42" /></a>				
</div> 
  
    <div class="clear"></div>
</div><!-- end footercontainer --></div>
</body>

</html>



