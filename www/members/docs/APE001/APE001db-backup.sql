create database APE001;

DROP TABLE bom;

CREATE TABLE `bom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bom_name` varchar(45) DEFAULT NULL,
  `reference_designator` varchar(225) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `manufacture` varchar(225) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `mpn` varchar(225) DEFAULT NULL,
  `price` varchar(225) DEFAULT NULL,
  `priceat` varchar(225) DEFAULT NULL,
  `vendor` varchar(225) DEFAULT NULL,
  `sku` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE bom_filename;

CREATE TABLE `bom_filename` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `bom_name` varchar(45) DEFAULT NULL,
  `filename` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE capacitor;

CREATE TABLE `capacitor` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `PrePN` int(11) DEFAULT NULL,
  `PostPN` int(11) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Operating_Temperature` varchar(255) DEFAULT NULL,
  `Package_Case` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(45) DEFAULT NULL,
  `Tolerance` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Capacitance` varchar(255) DEFAULT NULL,
  `Size_Dimension` varchar(255) DEFAULT NULL,
  `Thickness_Max` varchar(255) DEFAULT NULL,
  `Voltage_Rated` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Height` varchar(255) DEFAULT NULL,
  `Lead_Spacing` varchar(255) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `Dielectric_Material` varchar(225) DEFAULT NULL,
  `Dielectric_Characteristic` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

INSERT INTO capacitor VALUES("1","8pF - 50.0 - �5% - 0603","APE000002-001","2","1","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 8pF - 50.0 - �5% - Ceramic","","","Kemet","C0603C829J5GACTU","","","-55�C to +125�C ","C0603","Reel Unmarked","","","","","","",""," �5%","APE001.PcbLib","C0603","8pF","8pF","1.60mm x 0.80mm","0.80 � 0.07mm"," 50V","Datasheet","http://datasheet.octopart.com/C0603C829J5GACTU-Kemet-datasheet-10540709.pdf","","","green","Compliant","Not Listed by Manufacturer","APE001","CC1003_C0G   4/30/201","A","2018-02-01","Lead Free","Ceramic","C0G/NP0","partimg/C0603C829J5GACTU-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("2","18pF - 50.0 - �5% - 0603","APE000002-002","2","2","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 18pF - 50.0 - �5% - Ceramic","","","Kemet","C0603C180J5GACTU","","","-55�C to +125�C ","C0603","Reel Unmarked","2","","","","","",""," �5%","APE001.PcbLib","C0603","18pF","18pF","1.60mm x 0.80mm","0.80 � 0.07mm"," 50 V","Datasheet","http://datasheet.octopart.com/C0603C180J5GACTU-Kemet-datasheet-22084244.pdf","","","green","Compliant","Not Listed by Manufacturer","APE001","CC101_COMM_SMD  11/25/2013 ","A","2018-02-01","Lead Free","Ceramic","C0G/NP0","partimg/C0603C180J5GACTU-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("3","22pF - 50.0 - �5% - 0603","APE000002-003","2","3","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 22pF - 50.0 - �5% - Ceramic","","","Kemet","C0603C220J5GACTU","","","-55�C to +125�C ","C0603","Reel Unmarked","2","","","","","",""," �5%","APE001.PcbLib","C0603","22pF","22pF","1.60mm x 0.80mm","0.80 � 0.07mm"," 50 V","Datasheet","http://datasheet.octopart.com/C0603C220J5GACTU-Kemet-datasheet-22084244.pdf","0.0008mm","","green","Compliant","Not Listed by Manufacturer","APE001","CC101_COMM_SMD 11/25/2013","A","2018-02-01","Lead Free","Ceramic","C0G/NP0","partimg/C0603C220J5GACTU-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("4","27pF - 50.0 - �5% - 0603","APE000002-004","2","4","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 27pF - 50.0 - �5% - Ceramic","","","Kemet","C0603C270J5GACTU","","","-55�C to +125�C ","0603","Tape & Reel ","2","","","","","",""," �5%","APE001.PcbLib","C0603","27pF","27pF","1.60mm x 0.80mm","0.87mm"," 50 V","Datasheet","http://datasheet.octopart.com/C0603C270J5GACTU-Kemet-datasheet-22084244.pdf"," 0.0008mm","","green","Compliant","Not Listed by Manufacturer","APE001","CC101_COMM_SMD 11/25/2013","A","2018-02-01","Lead Free","Ceramic","C0G/NP0","partimg/C0603C270J5GACTU-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("5","0.001uF - 50.0 - �5% - 0603","APE000002-005","2","5","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 0.001uF - 50.0 - �5% - Ceramic","","","Kemet","C0603C102J5GACTU","","","-55�C to +125�C ","0603","Tape & Reel ","2","","","","","",""," �5%","APE001.PcbLib","C0603","0.001uF","0.001uF","1.60mm x 0.80mm","0.87mm"," 50 V","Datasheet","http://datasheet.octopart.com/C0603C102J5GACTU-Kemet-datasheet-22084244.pdf","0.0008mm","","green","Compliant","Not Listed by Manufacturer","APE001","CC101_COMM_SMD  11/25/2013","A","2018-02-01","Lead Free","Ceramic","C0G/NP0","partimg/C0603C102J5GACTU-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("6","0.0047uF - 50.0 - �5% - 0603","APE000002-006","2","6","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 0.0047uF - 50.0 - �5% - ","","","AVX Interconnect / Elco","06035C472JAT2A","","","-55�C to +125�C ","0603","Tape & Reel ","2","","","","","",""," �5%","APE001.PcbLib","C0603","0.0047uF","0.0047uF","1.60mm x 0.81mm"," 0.90mm","50V","Datasheet","http://datasheet.octopart.com/06035C472JAT2A-AVX-datasheet-12509786.pdf","0.0008mm","","green","Compliant","Not Listed by Manufacturer","APE001","Unknown","A","2018-02-01","Lead Free","","X7R","partimg/06035C472JAT2A-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("7","0.01uF - 50.0 - �5% - 0603","APE000002-007","2","7","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 0.01uF - 50.0 - �5% - ","","","AVX Interconnect / Elco","06035C103JAT2A","","","-55�C to +125�C ","0603","Tape & Reel ","2","","","","","",""," �5%","APE001.PcbLib","C0603","0.01uF","0.01uF","1.60mm x 0.81mm"," 0.90mm","50V","Datasheet","http://datasheet.octopart.com/06035C103JAT2A-AVX-datasheet-26446220.pdf","0.0008mm","","green","Compliant","Not Listed by Manufacturer","APE001","Unknown","A","2018-02-01","Lead Free",""," X7R","partimg/06035C103JAT2A-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("8","0.047uF - 50.0 - �5% - 0603","APE000002-008","2","8","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 0.047uF - 50.0 - �5% - ","","","AVX Interconnect / Elco","06035C473JAT2A","","","-55�C to +125�C ","0603","Tape & Reel ","","","","","","",""," �5%","APE001.PcbLib","C0603","0.047uF","0.047uF","1.60mm x 0.81mm"," 0.90mm","50V","Datasheet","http://datasheet.octopart.com/Commercial-Surface-Mount-Chips---X7R-Dielectric-AVX-datasheet-family-100.pdf","","","green","Compliant","Active","APE001","Unknown","A","2018-02-01","Lead Free",""," X7R","partimg/06035C473JAT2A-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("9","0.1uF - 50.0 - �5% - 0603","APE000002-009","2","9","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 0.1uF - 50.0 - �5% - Ceramic Multilayer","","","AVX Interconnect / Elco","06035C104JAT2A","","","-55�C to +125�C ","0603","Tape & Reel ","2","","","","","",""," �5%","APE001.PcbLib","C0603","0.1uF","0.1uF","1.60mm x 0.81mm"," 0.90mm","50V","Datasheet","http://datasheet.octopart.com/06035C104JAT2A-AVX-datasheet-26446220.pdf","","","green","Compliant","Active","APE001","Unknown","A","2018-02-01","Lead Free"," Ceramic Multilayer"," X7R","partimg/06035C104JAT2A-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("10","1uF - 16.0 - �10% - 0603","APE000002-010","2","10","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 1uF - 16.0 - �10% - Ceramic Multilayer","","","AVX Interconnect / Elco","0603YC105KAT2A","","","-55�C to +125�C ","0603","Tape & Reel ","2","","","","","",""," �10%","APE001.PcbLib","C0603","1uF","1uF","1.60mm x 0.81mm"," 0.90mm","16V","Datasheet","http://datasheet.octopart.com/0603YC105KAT2A-AVX-datasheet-26446220.pdf","","","green","Compliant","Active","APE001","Unknown","A","2018-02-01","Lead Free","Ceramic Multilayer"," X7R","partimg/0603YC105KAT2A-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("11","10uF - 16.0 - �10% - 1206","APE000002-011","2","11","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 10uF - 16.0 - �10% - Ceramic Multilayer","","","AVX Interconnect / Elco","1206YC106KAT2A","","","-55�C to +125�C ","1206","Tape & Reel ","2","","","","","",""," �10%","APE001.PcbLib","C1206","10uF","10uF","3.20mm x 1.60mm","1.78mm","16V ","Datasheet","http://datasheet.octopart.com/1206YC106KAT2A-AVX-datasheet-12587061.pdf","","","green","Compliant","Active","APE001","Unknown","A","2018-02-01","Lead Free","Ceramic Multilayer","X7R","partimg/1206YC106KAT2A-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C1206-PCB.jpg","3dimg/C1206-3D.jpg");
INSERT INTO capacitor VALUES("12","22uF - 25.0 - �10% - 1210","APE000002-012","2","12","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 22uF - 25.0 - �10% - Ceramic Multilayer","","","Murata","GRM32ER71E226KE15L","","","-55�C to +125�C ","1210","Tape & Reel ","2","","","","","",""," �10%","APE001.PcbLib","C1210","22uF","22uF","3.20mm x 2.50mm","2.70mm","25V","Datasheet","http://datasheet.octopart.com/GRM32ER71E226KE15L-Murata-datasheet-22122223.pdf","2.5mm","","green","Compliant","Active","APE001","Unknown","A","2018-02-01","Lead Free","Ceramic Multilayer"," X7R","partimg/GRM32ER71E226KE15L-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C1210-PCB.jpg","3dimg/C1210-3D.jpg");
INSERT INTO capacitor VALUES("13","47uF - 10.0 - �10% - 1210","APE000002-013","2","13","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 47uF - 10.0 - �10% - Ceramic Multilayer","","","Murata","GRM32ER71A476KE15L","","","-55�C to +125�C ","1210","Tape & Reel ","2","","","","","",""," �10%","APE001.PcbLib","C1210","47uF","47uF","3.20mm x 2.50mm","2.70mm","10V","Datasheet","http://datasheet.octopart.com/GRM32ER71A476KE15L-Murata-datasheet-8814637.pdf","2.5mm","","green","Compliant","Active","APE001","FM7500U-092-NOV09      muRata GRM Hi-Cap 2009_11","A","2018-02-01","Lead Free","Ceramic Multilayer"," X7R","partimg/GRM32ER71A476KE15L-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C1210-PCB.jpg","3dimg/C1210-3D.jpg");
INSERT INTO capacitor VALUES("14","1uF - 35.0 - �10% - 1206","APE000002-014","2","14","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 1uF - 35.0 - �10% - Tantalum","","","AVX Interconnect / Elco","TAJA105K035RNJ","","","-55�C to +125�C ","1206","Tape & Reel ","2","","","","","",""," �10%","APE001.PcbLib","C1206","1uF","1uF","3.20mm x1.60mm","","35V","Datasheet","http://datasheet.octopart.com/TAJA105K035RNJ-AVX-datasheet-12511273.pdf","1.80 mm","","green","Compliant","Active","APE001","Unknown","A","2018-02-01","Lead Free","Tantalum","","partimg/TAJA105K035RNJ-PIC.jpg","partsymbol/CAPPOL-SCH.jpg","partfootprint/C1206-PCB.jpg","3dimg/C1206-3D.jpg");
INSERT INTO capacitor VALUES("15","22uF - 2.0 - �10% - 7343-31","APE000002-015","2","15","CAP","APE001.SchLib","","=Value","Standard","Standard","CAP 22uF - 2.0 - �10% - Tantalum","","","Sprague","293D226X9020D2TE3","","","- 55 �C to + 85 �C","7343-31","Tape & Reel ","2","","","","","",""," �10%","APE001.PcbLib","C7343-31","22uF","22uF","7.30mm x 4.30mm","","6.3 V","Datasheet","http://datasheet.octopart.com/293D226X9020D2TE3-Vishay-datasheet-12545925.pdf","3.10mm","","green","Compliant","Active","APE001","Revision: 30-Aug-12","A","2018-02-01","Lead Free","Tantalum","","partimg/293D226X9020D2TE3-PIC.jpg","partsymbol/CAPPOL-SCH.jpg","partfootprint/C7343-31-PCB.jpg","3dimg/C7343-31-3D.jpg");





DROP TABLE connector;

CREATE TABLE `connector` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(255) DEFAULT NULL,
  `Package_Case` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Height` varchar(10) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

INSERT INTO connector VALUES("1","RASM722X","APE000004-001","RASM722X","APE001.SchLib","","=Value","Standard","Standard","JACK SOCKET, DC; Gender:Receptacle; Current Rating:5A; Connector Mounting:PCB; Contact Termination:Solder; SVHC:No SVHC (19-Dec-2011); Connector Mounting Orientation:PCB; Connector Type:Power Entry; Mounting Type:PC Board; No. of Outlets:1; Termination Me","","","Switchcraft / Conxall","RASM722X","","","Surface Mount","","Bulk","","","","","","","","APE001.PcbLib","RASM722X","Datasheet","http://datasheet.octopart.com/RASM722X-Switchcraft-datasheet-11630130.pdf","","","","green","Compliant","Not Listed by Manufacturer","","APE001"," 05/02/2006 ","A","2018-02-01","Lead Free","partimg/RASM722X-PIC.jpg","partsymbol/RASM722X-SCH.jpg","partfootprint/RASM722X-PCB.jpg","3dimg/RASM722X-3D.jpg");
INSERT INTO connector VALUES("4","UX60-MB-5ST","APE000004-004","UX60-MB-5ST","APE001.SchLib","","=Value","Standard","Standard","Conn USB 2.0 Type A RCP 5 POS 0.8mm Solder RA SMD 5 Terminal 1 Port T/R","","","Hirose","UX60-MB-5ST","","","Surface Mount","","Tape and Reel","","","","","","","","APE001.PcbLib","UX60-MB-5ST","Datasheet","http://datasheet.octopart.com/UX60-MB-5ST-Hirose-datasheet-8329595.pdf","","","","green","Compliant","Not Listed by Manufacturer","","APE001","Unknown","A","2018-02-01","Lead Free","partimg/UX60-MB-5ST-PIC.jpg","partsymbol/UX60-MB-5ST-SCH.jpg","partfootprint/UX60-MB-5ST-PCB.jpg","");
INSERT INTO connector VALUES("5","10103594-0001LF","APE000004-005","10103594-0001LF","APE001.SchLib","","=Value","Standard","Standard","CONN RCPT STD MICRO USB TYPE B","","","Framatome Connectors","10103594-0001LF","","","Surface Mount","","Tape and Reel","","","","","","","","APE001.PcbLib","10103594-0001LF","Datasheet","http://datasheet.octopart.com/10103594-0001LF-FCI-datasheet-10887844.pdf","","","","green","Compliant","Not Listed by Manufacturer","","APE001","Unknown","A","2018-02-01","Lead Free","partimg/10103594-0001LF-PIC.jpg","partsymbol/10103594-0001LF-SCH.jpg","partfootprint/10103594-0001LF-PCB.jpg","");
INSERT INTO connector VALUES("6","480-37-1000","APE000004-006","480-37-1000","APE001.SchLib","","=Value","Standard","Standard","Universal Serial Bus (USB) Shielded I/O Plug, Type A, Right Angle, Surface Mount, 0.76µm Gold (Au) Plating, PCB Thickness 1.20mm-1.60mm, Tray Package, Lead-Free","","","Molex","480-37-1000","","","Surface Mount","","Tray","","","","","","","","APE001.PcbLib","480-37-1000","Datasheet","http://datasheet.octopart.com/48037-1000-Molex-datasheet-142259.pdf","","","","green","Compliant","Active","","APE001","GC.DGN REV A 96/06/12 LEVEL 3 ","A","2018-02-01","Lead Free","partimg/480-37-1000-PIC.jpg","partsymbol/480-37-1000-SCH.jpg","partfootprint/480-37-1000-PCB.jpg","");
INSERT INTO connector VALUES("7","61729-1011BLF","APE000004-007","61729-1011BLF","APE001.SchLib","","=Value","Standard","Standard","Conn USB Type B RCP 4 POS 2.5mm Solder RA Thru-Hole 4 Terminal 1 Port Tube","","","FCI","61729-1011BLF","","","Through Hole","","Tubes","","","","","","","","APE001.PcbLib","61729-1011BLF","Datasheet","http://datasheet.octopart.com/61729-1011BLF-Areva-datasheet-8782292.pdf","","","","green","Compliant","Not Listed by Manufacturer","","APE001","Jul 28, 2010","A","2018-02-01","Lead Free","partimg/61729-1011BLF-PIC.jpg","partsymbol/61729-1011BLF-SCH.jpg","partfootprint/61729-1011BLF-PCB.jpg","");
INSERT INTO connector VALUES("8","1747981-1","APE000004-008","1747981-1","APE001.SchLib","","=Value","Standard","Standard","HDMI Connectors Product Highlights: Size = Standard Type A Series Receptacle 1 Ports Terminate To Printed Circuit Board","","","TE Connectivity / AMP","1747981-1","","","Surface Mount","","Tape and Reel","","","","","","","","APE001.PcbLib","1747981-1","Datasheet","http://datasheet.octopart.com/1747981-1-Tyco-Electronics-datasheet-10742577.pdf","","","","green","Compliant","Not Listed by Manufacturer","","APE001","ECO-11-005033 ","A","2018-02-01","Lead Free","partimg/1747981-1-PIC.jpg","partsymbol/1747981-1-SCH.jpg","partfootprint/1747981-1-PCB.jpg","");
INSERT INTO connector VALUES("9","PJRAN1X1U01X","APE000004-009","PJRAN1X1U01X","APE001.SchLib","","=Value","Standard","Standard","CONNECTOR, PHONO, JACK, 1WAY; Series:-;; CONNECTOR, PHONO, JACK, 1WAY; Series:-; Gender:Jack; No. of Contacts:1; Connector Color:Black; Contact Termination:Solder; Connector Body Material:Metal; Contact Plating:Tin; Color:Black; Connector Mounting:PCB","","","Switchcraft","PJRAN1X1U01X","","","Through Hole","","","","","","","","","","APE001.PcbLib","PJRAN1X1U01X","Datasheet","http://datasheet.octopart.com/PJRAN1X1U01X-Switchcraft-datasheet-10098733.pdf","","","","green","Compliant","Not Listed by Manufacturer","","APE001","ECO-25181 1-13-2006","A","2018-02-01","Lead Free","partimg/PJRAN1X1U01X-PIC.jpg","partsymbol/PJRAN1X1U01X-SCH.jpg","partfootprint/PJRAN1X1U01X-PCB.jpg","");
INSERT INTO connector VALUES("10","5555764-1","APE000004-010","5555764-1","APE001.SchLib","","=Value","Standard","Standard","Conn RJ-45 F 8 POS 1.27mm Solder RA SMD 8 Terminal 1 Port Tray Cat 3","","","TE Connectivity / AMP","5555764-1","","","Surface Mount","","Tray","","","","","","","","APE001.PcbLib","5555764-1","Datasheet","http://datasheet.octopart.com/5555764-1-TE-Connectivity---AMP-datasheet-21774921.pdf","12.70mm","","","green","Compliant","Active","","APE001","Unknown","A","2018-02-01","Lead Free","partimg/5555764-1-PIC.jpg","partsymbol/5555764-1-SCH.jpg","partfootprint/5555764-1-PCB.jpg","");
INSERT INTO connector VALUES("11","U.FL-R-SMT(10)","APE000004-011","U.FL","APE001.SchLib","","=Value","Standard","Standard","U.FL Series 50 Ohm Receptacle Male Pins Ultra Small SMT Coaxial Connector","","","Hirose","U.FL-R-SMT(10)","","","Surface Mount","","Tape and Reel","","","","","","","","APE001.PcbLib","U.FL-R-SMT(10)","Datasheet","http://datasheet.octopart.com/U.FL-R-SMT%2810%29-Hirose-datasheet-8324213.pdf","1.9mm or 2","","","green","Compliant","Not Listed by Manufacturer","","APE001","Unknown","A","2018-02-01","Lead Free","partimg/U.FL-R-SMT(10)-PIC.jpg","partsymbol/U.FL-SCH.jpg","partfootprint/U.FL-R-SMT(10)-PCB.jpg","");
INSERT INTO connector VALUES("13","5-534237-8","APE000004-013","5-534237-8","APE001.SchLib","","=Value","Standard","Standard","Conn Socket Strip SKT 10 POS 2.54mm Solder ST Thru-Hole Tube","","","TE Connectivity / AMP","5-534237-8","","","Through Hole","","Tube","","","","","","","","APE001.PcbLib","5-534237-8","Datasheet","http://datasheet.octopart.com/5-534237-8-Tyco-Electronics-datasheet-11077762.pdf","","","","green","Compliant","Not Listed by Manufacturer","","APE001","Revised 8-08 ","A","2018-02-01","Lead Free","partimg/5-534237-8-PIC.jpg","partsymbol/5-534237-8-SCH.jpg","partfootprint/5-534237-8-PCB.jpg","3dimg/5-534237-8-3D.jpg");
INSERT INTO connector VALUES("14","9-146281-0","APE000004-014","9-146281-0","APE001.SchLib","","=Value","Standard","Standard","AMPMODU 40 Position Through-Hole Single Row Straight 2.54 mm Breakaway Header","","","TE Connectivity / AMP","9-146281-0","","","Through Hole","","Bulk","","","","","","","","APE001.PcbLib","9-146281-0","Datasheet","http://datasheet.octopart.com/9-146281-0-Tyco-Electronics-datasheet-11077762.pdf","","","","green","Compliant","Not Listed by Manufacturer","","APE001","Revised 8-08 ","A","2018-02-01","Lead Free","partimg/9-146281-0-PIC.jpg","partsymbol/9-146281-0-SCH.jpg","partfootprint/9-146281-0-PCB.jpg","3dimg/9-146281-0-3D.jpg");
INSERT INTO connector VALUES("15","95278-101A06LF","APE000004-015","95278-101A06LF","APE001.SchLib","","=Value","Standard","Standard","Unshrouded Header, SMT, Double Row, 6 Positions, 2.54 mm Pitch, Vertical","","","FCI","95278-101A06LF","","","Surface Mount","","Tape and Reel","","","","","","","","APE001.PcbLib","95278-101A06LF","Datasheet","http://datasheet.octopart.com/95278-101A06LF-Areva-datasheet-8459731.pdf","","","","green","Compliant","Not Listed by Manufacturer","","APE001","Dec 12, 2008","A","2018-02-01","Lead Free","partimg/95278-101A06LF-PIC.jpg","partsymbol/95278-101A06LF-SCH.jpg","partfootprint/95278-101A06LF-PCB.jpg","");
INSERT INTO connector VALUES("16","BA2032SM","APE000004-016","BA2032SM","APE001.SchLib","","=Value","Standard","Standard","HOLDER COIN CELL 2032 EJECT SMD","","","MPD","BA2032SM","","","PCB, Surface Mount","","","","","","","","","","APE001.PcbLib","BA2032SM","Datasheet","http://datasheet.octopart.com/BA2032SM-MPD-datasheet-14434172.pdf","6.70mm","","","green","Compliant","Not Listed by Manufacturer","","APE001","Unknown","A","2018-02-01","Lead Free","partimg/BA2032SM-PIC.jpg","partsymbol/BA2032SM-SCH.jpg","partfootprint/BA2032SM-PCB.jpg","");
INSERT INTO connector VALUES("17","B2P-VH(LF)(SN)","APE000004-017","B2P-VH(LF)(SN)","APE001.SchLib","","=Value","Standard","Standard","HEADER, PCB, VERTICAL, 2WAY; Connector Type:Wire to Board; Series:VH; Contact Termination:Crimp; Gender:Header; No. of Contacts:2; No. of Rows:1; Pitch Spacing:3.96mm; Contact Plating:Tin; Contact Material:Brass; SVHC:No SVHC (15-Dec-2010); Approval Bodie","","","JST","B2P-VH(LF)(SN)","","","Through Hole","","Bulk","","","","","","","","APE001.PcbLib","B2P-VH(LF)(SN)","Datasheet","http://datasheet.octopart.com/B2P-VH%28LF%29%28SN%29-JST-datasheet-540220.pdf","16.5mm","","","green","Compliant","Not Listed by Manufacturer","","APE001","Unknown","A","2018-02-01","Lead Free","partimg/B2P-VH(LF)(SN)-PIC.jpg","partsymbol/B2P-VH(LF)(SN)-SCH.jpg","partfootprint/B2P-VH(LF)(SN)-PCB.jpg","3dimg/B2P-VH(LF)(SN)-3D.jpg");
INSERT INTO connector VALUES("19","B3P-VH(LF)(SN)","APE000004-019","B3P-VH(LF)(SN)","APE001.SchLib","","=Value","Standard","Standard","HEADER, PCB, VERTICAL, 3WAY; Connector Type:Wire to Board; Series:VH; Contact Termination:Through Hole Vertical; Gender:Header; No. of Contacts:3; No. of Rows:1; Pitch Spacing:3.96mm; Contact Plating:Tin; Contact Material:Brass; SVHC:No SVHC (15-Dec-2010)","","","JST","B3P-VH(LF)(SN)","","","Through Hole","","Bulk","","","","","","","","APE001.PcbLib","B3P-VH(LF)(SN)","Datasheet","http://datasheet.octopart.com/B3P-VH%28LF%29%28SN%29-JST-datasheet-5332680.pdf","9.4mm","","","green","Compliant","Not Listed by Manufacturer","","APE001","Unknown","A","2018-02-01","Lead Free","partimg/B3P-VH(LF)(SN)-PIC.jpg","partsymbol/B3P-VH(LF)(SN)-SCH.jpg","partfootprint/B3P-VH(LF)(SN)-PCB.jpg","3dimg/B3P-VH-3D.jpg");
INSERT INTO connector VALUES("22","S2B-XH-A(LF)(SN)","APE000004-022","S2B-XH-A(LF)(SN)","APE001.SchLib","","=Value","Standard","Standard","XH Series 2.5mm 2 Position Through Hole Shrouded Header Connector","","","JST","S2B-XH-A(LF)(SN)","","","Through Hole, Right Angle","","Bulk","","","","","","","","APE001.PcbLib","S2B-XH-A(LF)(SN)","Datasheet","http://datasheet.octopart.com/S2B-XH-A%28LF%29%28SN%29-JST-datasheet-8325564.pdf","9.8mm","","","green","Compliant","Not Listed by Manufacturer","","APE001","Unknown","A","2018-02-01","Lead Free","partimg/S2B-XH-A(LF)(SN)-PIC.jpg","partsymbol/S2B-XH-A(LF)(SN)-SCH.jpg","partfootprint/S2B-XH-A(LF)(SN)-PCB.jpg","3dimg/S2B-XH-A-3D.jpg");
INSERT INTO connector VALUES("24","S3B-XH-A(LF)(SN)","APE000004-024","S3B-XH-A(LF)(SN)","APE001.SchLib","","=Value","Standard","Standard","XH Series 3 Position 2.5 mm Pitch Through Hole Side Entry Shrouded Header","","","JST","S3B-XH-A(LF)(SN)","","","Through Hole, Right Angle","","Bulk","","","","","","","","APE001.PcbLib","S3B-XH-A(LF)(SN)","Datasheet","http://datasheet.octopart.com/S3B-XH-A%28LF%29%28SN%29-JST-datasheet-8325564.pdf","9.8mm","","","green","Compliant","Not Listed by Manufacturer","","APE001","Unknown","A","2018-02-01","Lead Free","partimg/S3B-XH-A(LF)(SN)-PIC.jpg","partsymbol/S3B-XH-A(LF)(SN)-SCH.jpg","partfootprint/S3B-XH-A(LF)(SN)-PCB.jpg","3dimg/S3B-XH-A-3D.jpg");
INSERT INTO connector VALUES("26","S4B-XH-A(LF)(SN)","APE000004-026","S4B-XH-A(LF)(SN)","APE001.SchLib","","=Value","Standard","Standard","XH Series 4 Position 2.5 mm Through Hole Right Angle Press Fit Shrouded Header","","","JST","S4B-XH-A(LF)(SN)","","","Through Hole, Right Angle","","Bulk","","","","","","","","APE001.PcbLib","S4B-XH-A(LF)(SN)","Datasheet","http://datasheet.octopart.com/S4B-XH-A%28LF%29%28SN%29-JST-datasheet-12510042.pdf","9.8mm","","","green","Compliant","Not Listed by Manufacturer","","APE001","Unknown","A","2018-02-01","Lead Free","partimg/S4B-XH-A(LF)(SN)-PIC.jpg","partsymbol/S4B-XH-A(LF)(SN)-SCH.jpg","partfootprint/S4B-XH-A(LF)(SN)-PCB.jpg","3dimg/S4B-XH-A-1-3D.jpg");
INSERT INTO connector VALUES("29","S2B-PH-K-S(LF)(SN)","APE000004-029","S2B-PH-K-S(LF)(SN)","APE001.SchLib","","=Value","Standard","Standard","PH Series 2 Position 2.0 mm Through Hole Side Entry Shrouded Header","","","JST","S2B-PH-K-S(LF)(SN)","","","Through Hole, Right Angle","","Bulk ","","","","","","","","APE001.PcbLib","S2B-PH-K-S(LF)(SN)","Datasheet","http://datasheet.octopart.com/S2B-PH-K-S%28LF%29%28SN%29-JST-datasheet-8324864.pdf","8.0mm","","","green","Compliant","Not Listed by Manufacturer","","APE001","Unknown","A","2018-02-01","Lead Free","partimg/S2B-PH-K-S(LF)(SN)-PIC.jpg","partsymbol/S2B-PH-K-S(LF)(SN)-SCH.jpg","partfootprint/S2B-PH-K-S(LF)(SN)-PCB.jpg","3dimg/S2B-PH-K-S-3D.jpg");
INSERT INTO connector VALUES("30","S2B-PH-SM4-TB(LF)(SN)","APE000004-030","S2B-PH-SM4-TB(LF)(SN)","APE001.SchLib","","=Value","Standard","Standard","HEADER, SIDE ENTRY, SMD, 2WAY; Connector Type:Wire to Board; Series:PH; Contact Termination:Surface Mount Right Angle; Gender:Header; No. of Contacts:2; No. of Rows:1; Pitch Spacing:2mm; Contact Plating:Tin; Contact Material:Brass; SVHC:No SVHC (15-Dec-20","","","JST","S2B-PH-SM4-TB(LF)(SN)","","","Surface Mount","","Reel and Tape","","","","","","","","APE001.PcbLib","S2B-PH-SM4-TB(LF)(SN)","Datasheet","http://datasheet.octopart.com/S2B-PH-SM4-TB%28LF%29%28SN%29-JST-datasheet-8324864.pdf","8.0mm","","","green","Compliant","Not Listed by Manufacturer","","APE001","Unknown","A","2018-02-01","Lead Free","partimg/S2B-PH-SM4-TB(LF)(SN)-PIC.jpg","partsymbol/S2B-PH-SM4-TB(LF)(SN)-SCH.jpg","partfootprint/S2B-PH-SM4-TB(LF)(SN)-PCB.jpg","3dimg/S2B-PH-SM4-TB(LF)(SN)-3D.jpg");
INSERT INTO connector VALUES("34","1803277","APE000004-034","1803277","APE001.SchLib","","=Value","Standard","Standard","HEADER, RIGHT ANGLE, 3.81MM, 2WAY; Connector Type:Wire-to-Board; Gender:Pin; Ways, No. of:2; Lead Spacing:3.81mm; Termination Method:Solder; Series:MC; Rows, No. of:2; Current Rating:8A; Pitch:3.81mm; Voltage Rating, AC:160V ;RoHS Compliant: Yes","","","Phoenix Contact","1803277","","","Through Hole","","Bulk","","","","","","","","APE001.PcbLib","1803277","Datasheet","http://datasheet.octopart.com/1803277-Phoenix-Contact-datasheet-23713020.pdf","11.1mm","","","green","Compliant","Not Listed by Manufacturer","","APE001","11.11.2013","A","2018-02-01","Lead Free","partimg/1803277-PIC.jpg","partsymbol/1803277-SCH.jpg","partfootprint/1803277-PCB.jpg","3dimg/1803277-3D.jpg");
INSERT INTO connector VALUES("35","35RASMT4BHNTRX","APE000004-035","35RASMT4BHNTRX","APE001.SchLib","","=Value","Standard","Standard","Conn 3.5MM Stereo Jack F 3 POS Solder RA SMD 5 Terminal 1 Port T/R","","","Switchcraft","35RASMT4BHNTRX","","","Surface Mount","","Reel and Tape","2","","","","","","","APE001.PcbLib","35RASMT4BHNTRX","Datasheet","http://datasheet.octopart.com/35RASMT4BHNTRX-Switchcraft-datasheet-17726097.pdf","","","","green","Compliant","Not Listed by Manufacturer","","APE001","ECO-26972 6-20-2012","A","2018-02-01","Lead Free","partimg/35RASMT4BHNTRX-PIC.jpg","partsymbol/35RASMT4BHNTRX-SCH.jpg","partfootprint/35RASMT4BHNTRX-PCB.jpg","");





DROP TABLE default_values;

CREATE TABLE `default_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value_type` varchar(200) NOT NULL,
  `category` varchar(200) NOT NULL,
  `default_val` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO default_values VALUES("1","symbol","resistor","RES");
INSERT INTO default_values VALUES("2","symbol","capacitor","CAP");
INSERT INTO default_values VALUES("3","symbol","misc","FUSE_SMT");
INSERT INTO default_values VALUES("4","symbol","connector","10103594-0001LF");
INSERT INTO default_values VALUES("5","symbol","ic","AMP");
INSERT INTO default_values VALUES("6","symbol","diode","LED");
INSERT INTO default_values VALUES("7","symbol","transistor","NPN");
INSERT INTO default_values VALUES("8","symbol","oscillator","CRYSTAL4");
INSERT INTO default_values VALUES("9","footprint","resistor","R0603");
INSERT INTO default_values VALUES("10","footprint","capacitor","C0603");
INSERT INTO default_values VALUES("11","footprint","misc","B3U");
INSERT INTO default_values VALUES("12","footprint","connector","480-37-1000");
INSERT INTO default_values VALUES("13","footprint","ic","CC3000");
INSERT INTO default_values VALUES("14","footprint","diode","LED-0603-AMBER");
INSERT INTO default_values VALUES("15","footprint","transistor","TO-252");
INSERT INTO default_values VALUES("16","footprint","oscillator","4-SMD");





DROP TABLE diode;

CREATE TABLE `diode` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Impedance_Max_Zzt` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Package_Case` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Tolerance` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `Reverse_Recovery_Time_trr` varchar(255) DEFAULT NULL,
  `Size_Dimension` varchar(255) DEFAULT NULL,
  `Height` varchar(255) DEFAULT NULL,
  `Voltage_Reverse` varchar(255) DEFAULT NULL,
  `ESR_Equivalent_Series_Resistance` varchar(255) DEFAULT NULL,
  `Capacitance` varchar(255) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  `Peak_wavelength` varchar(225) DEFAULT NULL,
  `Lens_type` varchar(225) DEFAULT NULL,
  `Luminous_Intesity` varchar(225) DEFAULT NULL,
  `Viewing_Angle` varchar(225) DEFAULT NULL,
  `Polarity` varchar(225) DEFAULT NULL,
  `Current_Rating` varchar(225) DEFAULT NULL,
  `Power_rating` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO diode VALUES("1","HSMG-C190","APE000006-001","LED","","APE001.SchLib","","=Value","Standard","Standard","LED 570NM GREEN DIFF 0603 SMD","","","","Avago","HSMG-C190","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm","APE001.PcbLib","LED-0603-GREEN","http://datasheet.octopart.com/HSMG-C190-Avago-datasheet-10307877.pdf","Datasheet","5ns","1.60mm L x 0.80mm W","0.80mm","5v","","9pF","","APE001","May 10, 2010","A","2018-02-01","green","","Lead Free","partimg/HSMG-C190-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-GREEN-PCB.jpg","3dimg/LED-0603-GREEN-3D.jpg","570 nm","Diffused","0.015 cd","170 degree","","","0.052W");
INSERT INTO diode VALUES("2","HSMH-C190","APE000006-002","LED","","APE001.SchLib","","=Value","Standard","Standard","LED 660NM RED DIFF 0603 SMD","","","","Avago","HSMH-C190","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm","APE001.PcbLib","LED-0603-RED","http://datasheet.octopart.com/HSMH-C190-Avago-datasheet-10307877.pdf","Datasheet","5ns","1.60mm L x 0.80mm W","0.80mm","5v","","18pF","","APE001","May 10, 2010","A","2018-02-01","green","","Lead Free","partimg/HSMH-C190-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-RED-PCB.jpg","3dimg/LED-0603-RED-3D.jpg","660 nm","Diffused","0.017 cd","170 degree","","","0.065W");
INSERT INTO diode VALUES("3","HSMY-C190","APE000006-003","LED","","APE001.SchLib","","=Value","Standard","Standard","LED 589NM YELLOW DIFF 0603 SMD","","","","Avago","HSMY-C190","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm","APE001.PcbLib","LED-0603-YELLOW","http://datasheet.octopart.com/HSMY-C190-Avago-datasheet-10307877.pdf","Datasheet","5ns","1.60mm L x 0.80mm W","0.80mm","5v","","6pF","","APE001","May 10, 2010","A","2018-02-01","green","","Lead Free","partimg/HSMY-C190-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-YELLOW-PCB.jpg","3dimg/LED-0603-YELLOW-3D.jpg","589 nm","Diffused","0.008 cd","170 degree","",""," 0.052W");
INSERT INTO diode VALUES("4","HSMD-C190","APE000006-004","LED","","APE001.SchLib","","=Value","Standard","Standard","LED 604NM ORANGE DIFF 0603 SMD","","","","Avago","HSMD-C190","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm","APE001.PcbLib","LED-0603-ORANGE","http://datasheet.octopart.com/HSMD-C190-Avago-datasheet-10307877.pdf","Datasheet","5ns","1.60mm L x 0.80mm W","0.80mm","5v","","7pf","","APE001","May 10, 2010","A","2018-02-01","green","","Lead Free","partimg/HSMD-C190-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-ORANGE-PCB.jpg","3dimg/LED-0603-ORANGE-3D.jpg","605 nm","Diffused","0.008 cd","170 degree","",""," 0.052W");
INSERT INTO diode VALUES("5","HSMA-C190","APE000006-005","LED","","APE001.SchLib","","=Value","Standard","Standard","LED 595NM AMBER DIFF 0603 SMD","","","","Avago","HSMA-C190","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm","APE001.PcbLib","LED-0603-AMBER","http://datasheet.octopart.com/HSMA-C190-Avago-datasheet-10308043.pdf","Datasheet","5ns","1.60mm L x 0.80mm W","0.80mm","5v","","45pF","","APE001","September 29, 2010","A","2018-02-01","green","","Lead Free","partimg/HSMA-C190-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-AMBER-PCB.jpg","3dimg/LED-0603-AMBER-3D.jpg","595 nm","Diffused","0.09 cd","170 degree","","","0.06W");
INSERT INTO diode VALUES("6","HSMR-C190","APE000006-006","LED","","APE001.SchLib","","=Value","Standard","Standard","LED 473NM BLUE DIFF 0603 SMD","","","","Avago","HSMR-C190","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm","APE001.PcbLib","LED-0603-BLUE","http://datasheet.octopart.com/HSMR-C190-Avago-datasheet-8824602.pdf","Datasheet","","1.60mm L x 0.80mm W","0.80mm","5v","","110c/w","","APE001","April 9, 2010","A","2018-02-01","green","","Lead Free","partimg/HSMR-C190-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-BLUE-PCB.jpg","3dimg/LED-0603-BLUE-3D.jpg","473 nm","Diffused","0.055 cd","140 degree","","","");
INSERT INTO diode VALUES("7","HSMW-C191","APE000006-007","LED","","APE001.SchLib","","=Value","Standard","Standard","LED WHITE DIFF 0603 SMD","","","","Avago","HSMW-C191","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm ","APE001.PcbLib","LED-0603-WHITE","http://datasheet.octopart.com/HSMW-C191-Avago-datasheet-10314750.pdf","Datasheet","5ns","1.60mm L x 0.80mm W","0.60mm","5v","","55pF","","APE001","May 5, 2010","A","2018-02-01","green","","Lead Free","partimg/HSMW-C191-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-WHITE-PCB.jpg","3dimg/LED-0603-WHITE-3D.jpg","","Diffused","0.2 cd","140 degree","","","0.078W");
INSERT INTO diode VALUES("8","1N4148WS-7-F","APE000006-008","DIODE","","APE001.SchLib","","=Value","Standard","Standard","DIODE SWITCHING 75V 0.15A SOD323","","","","Diodes Inc.","1N4148WS-7-F","","","Surface Mount","","","","","SOD323","Tape & Reel","2","Compliant","Active","","","","APE001.PcbLib","SOD-323","http://datasheet.octopart.com/1N4148WS-7-F-Diodes-Inc.-datasheet-11839050.pdf","Datasheet","4.0ns","","","75v","","2.0pF","","APE001","Rev. 20 - 2  march2012","A","2018-02-01","green","","Lead Free","partimg/1N4148WS-7-F-PIC.jpg","partsymbol/DIODE-SCH.jpg","partfootprint/SOD-323-PCB.jpg","3dimg/SOD-323-3D.jpg","","","","","Standard","0.15A","0.2W");
INSERT INTO diode VALUES("9","SS12-E3/61T","APE000006-009","SCHOTTKY","","APE001.SchLib","","=Value","Standard","Standard","DIODE SCHOTTKY 20V 1A DO214C","","","","Vishay Semiconductor","SS12-E3/61T","","","Surface Mount","","","","","DO-214AC","Tape & Reel","2","Compliant","Not Listed by Manufacturer","","","","APE001.PcbLib","DO-214AC","http://datasheet.octopart.com/SS12-E3/61T-Vishay-datasheet-11540009.pdf","Datasheet","","","","20v","","","","APE001","26-Mar-12","A","2018-02-01","green","","Lead Free","partimg/SS12-E3-61T-PIC.jpg","partsymbol/SCHOTTKY-SCH.jpg","partfootprint/DO-214AC-PCB.jpg","3dimg/DO-214AC-3D.jpg","","","","","Standard","1.0A","");
INSERT INTO diode VALUES("10","BAT54S-7-F","APE000006-010","SCHOTTKY","","APE001.SchLib","","=Value","Standard","Standard","DIODE SCHOTTKY 70V 70MA SOT23","","","","Diodes Inc.","BAT54S-7-F","","","Surface Mount","","","","","SOT-23","Tape & Reel","3","Compliant","Active","","","","APE001.PcbLib","SOT-23","http://datasheet.octopart.com/BAT54S-7-F-Diodes-Inc.-datasheet-5320128.pdf","Datasheet","5.0 ns","","","30v","","10pF","","APE001","Rev. 24 - 2  april2009","A","2018-02-01","green","","Lead Free","partimg/BAT54S-7-F-PIC.jpg","partsymbol/SCHOTTKY-SCH.jpg","partfootprint/SOT-23-PCB.jpg","3dimg/SOT-23-3D.jpg","","","","","","0.2A","0.2W");
INSERT INTO diode VALUES("11","BAS70-04-7-F","APE000006-011","SCHOTTKY","","APE001.SchLib","","=Value","Standard","Standard","DIODE ARRAY SCHOTTKY 70V SOT23-3","","","","Diodes Inc.","BAS70-04-7-F","","","Surface Mount","","","","","SOT-23 ","Tape & Reel","3","Compliant","Active","","","","APE001.PcbLib","SOT-23","http://datasheet.octopart.com/BAS70-04-7-F-Diodes-Inc.-datasheet-5395814.pdf","Datasheet","5.0 ns","","","70 v","","2.0pF","","APE001","Rev. 21 - 2  july 2008","A","2018-02-01","green","","Lead Free","partimg/BAS70-04-7-F-PIC.jpg","partsymbol/SCHOTTKY-SCH.jpg","partfootprint/SOT-23-PCB.jpg","3dimg/SOT-23-3D.jpg","","","","","","","0.2W");





DROP TABLE footprint_autofill;

CREATE TABLE `footprint_autofill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autofill_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO footprint_autofill VALUES("1","manual");





DROP TABLE ic;

CREATE TABLE `ic` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `PrePN` varchar(45) DEFAULT NULL,
  `PostPN` varchar(45) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `Supplier_Device_Package` varchar(255) DEFAULT NULL,
  `Minimum_Orde_Quantity` varchar(255) DEFAULT NULL,
  `Reeling` varchar(255) DEFAULT NULL,
  `Product_Type` varchar(255) DEFAULT NULL,
  `Mounting_Style` varchar(255) DEFAULT NULL,
  `Height` varchar(10) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

INSERT INTO ic VALUES("1","UA7805CKTTR","APE000005-001","5","1","UA7805","","APE001.SchLib","","=Value","Standard","Standard","3 Pin 1.5A Fixed 5V Positive Voltage Regulator 3-DDPAK/TO-263 0 to 125","","","Texas Instruments","UA7805CKTTR","","","Surface Mount"," Tape and Reel","3","","","","","","","APE001.PcbLib","TO-263","","","","","","","38.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=055d425&at=physicalpart&hlid=16691236","","APE001","SLVS056O-MAY1976-REVISED AUGUST2012","A","2018-02-01","green","Compliant","Active",""," Contains Lead","partimg/UA7805CKTTR-PIC.jpg","partsymbol/UA7805-SCH.jpg","partfootprint/TO-263-PCB.jpg","");
INSERT INTO ic VALUES("2","LMS8117AMP-3.3/NOPB","APE000005-002","5","2","LMS8117","","APE001.SchLib","","=Value","Standard","Standard","1A Low-Dropout Linear Regulator 4-SOT-223 0 to 125","","","Texas Instruments","LMS8117AMP-3.3/NOPB","","","Surface Mount"," Tape and Reel"," 4","","","","","","","APE001.PcbLib","SOT-223","","","","","","","","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0b88a90&at=physicalpart&hlid=16633772","","APE001","April 2005","A","2018-02-01","green","Non-Compliant","Active",""," Contains Lead","partimg/LMS8117AMP-3.3-NOPB-PIC.jpg","partsymbol/LMS8117-SCH.jpg","partfootprint/SOT-223-PCB.jpg","");
INSERT INTO ic VALUES("3","LM1117IMP-5.0-NOPB","APE000005-003","5","3","LM1117","","APE001.SchLib","","=Value","Standard","Standard","800mA Low-Dropout Linear Regulator 4-SOT-223 -40 to 125","","","Texas Instruments","LM1117IMP-5.0-NOPB","","","Surface Mount"," Tape and Reel","4","","","","","","","APE001.PcbLib","SOT-223","","","","","","","35.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=006ed0b&at=physicalpart&hlid=16624665","","APE001","SNOS412M-FEBRUARY-2000-REVISED-MARCH2013","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/LM1117IMP-5.0-NOPB-PIC.jpg","partsymbol/LM1117-SCH.jpg","partfootprint/SOT-223-PCB.jpg","");
INSERT INTO ic VALUES("4","LP2985-33DBVR","APE000005-004","5","4","LP2985-33","","APE001.SchLib","","=Value","Standard","Standard","Single Output LDO, 150mA, Fixed(3.3V), 1.5% Tolerance, Low Quiescent Current, Low Noise 5-SOT-23 -40 to 125","","","Texas Instruments","LP2985-33DBVR","","","Surface Mount"," Tape and Reel","5","","","","","","","APE001.PcbLib","SOT-23-5","","","","","","","18.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=04af963&at=physicalpart&hlid=16635153","","APE001","SLVS522N-JULY2004-REVISED-JUNE2011","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/LP2985-33DBVR-PIC.jpg","partsymbol/LP2985-33-SCH.jpg","partfootprint/SOT-23-5-PCB.jpg","3dimg/SOT-23-5-3D.jpg");
INSERT INTO ic VALUES("5","LP2985-50DBVR","APE000005-005","5","5","LP2985-50","","APE001.SchLib","","=Value","Standard","Standard","Single Output LDO, 150mA, Fixed(5.0V), 1.5% Tolerance, Low Quiescent Current, Low Noise 5-SOT-23 -40 to 125","","","Texas Instruments","LP2985-50DBVR","","","Surface Mount"," Tape and Reel","5","","","","","","","APE001.PcbLib","SOT-23-5","","","","","","","33.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0553daa&at=physicalpart&hlid=16635159","","APE001","SLVS522N-JULY2004-REVISED-JUNE2011","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/LP2985-50DBVR-PIC.jpg","partsymbol/LP2985-50-SCH.jpg","partfootprint/SOT-23-5-PCB.jpg","3dimg/SOT-23-5-3D.jpg");
INSERT INTO ic VALUES("6","LMR10510YMFE/NOPB","APE000005-006","5","6","LMR10510","","APE001.SchLib","","=Value","Standard","Standard","SIMPLE SWITCHER 5.5Vin, 1A Step-Down Voltage Regulator in SOT-23 5-SOT-23 -40 to 125","","","Texas Instruments","LMR10510YMFE/NOPB","","",""," Tape and Reel","5","","","","","","","APE001.PcbLib","SOT-23-5","","","","","","","35.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0807e8d&at=physicalpart&hlid=16633638","","APE001","SNVS727B-OCTOBER2011-REVISED-APRIL2013","A","2018-02-01","green","Compliant"," Active","","Lead Free","partimg/LMR10510YMFE-NOPB-PIC.jpg","partsymbol/LMR10510-SCH.jpg","partfootprint/SOT-23-5-PCB.jpg","3dimg/SOT-23-5-3D.jpg");
INSERT INTO ic VALUES("7","LM2576HVS-5.0/NOPB","APE000005-007","5","7","LM2576","","APE001.SchLib","","=Value","Standard","Standard","SIMPLE SWITCHER® 3A Step-Down Voltage Regulator 5-DDPAK/TO-263 -40 to 125","","","Texas Instruments","LM2576HVS-5.0/NOPB","",""," Through Hole"," Tape and Reel","5","","","","","","","APE001.PcbLib","TO-263","","","","","","","45.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=02a3fe5&at=physicalpart&hlid=16625726","","APE001","SNVS107C-JUNE1999-REVISED-APRIL2013","A","2018-02-01","green","Non-Compliant"," Active",""," Contains Lead","partimg/LM2576HVS-5.0-NOPB-PIC.jpg","partsymbol/LM2576-SCH.jpg","partfootprint/TO-263-PCB.jpg","");
INSERT INTO ic VALUES("8","DRV8825PWP","APE000005-008","5","8","DRV8825","","APE001.SchLib","","=Value","Standard","Standard","2.5A Bipolar Stepper Motor Driver with On-Chip 1/32 Microstepping Indexer (Step/Dir Ctrl) 28-HTSSOP -40 to 85","","","Texas Instruments","DRV8825PWP","","",""," Tape and Reel","28 ","","","","","","","APE001.PcbLib","28-HTSSOP","","","","","","","38.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=06288d3&at=physicalpart&hlid=16621369","","APE001","SLVSA73F-APRIL2010-REVISED-JULY2014","A","2018-02-01","green","Compliant"," Active","","Lead Free","partimg/DRV8825PWP-PIC.jpg","partsymbol/DRV8825-SCH.jpg","partfootprint/28-HTSSOP-PCB.jpg","");
INSERT INTO ic VALUES("9","L293DD","APE000005-009","5","9","L293","","APE001.SchLib","","=Value","Standard","Standard","Push-Pull Four Channel Drivers with Diodes","","","STMicroelectronics","L293DD","","","Surface Mount","","20","","","","","","","APE001.PcbLib","SOIC-20","","","","","","","","Datasheet","http://datasheet.octopart.com/L293DD-STMicroelectronics-datasheet-10011.pdf","","APE001","JULY2013","A","2018-02-01","green","Compliant"," Active","","Lead Free","partimg/L293DD-PIC.jpg","partsymbol/L293-SCH.jpg","partfootprint/SOIC-20-PCB.jpg","");
INSERT INTO ic VALUES("10","FT232RL-REEL","APE000005-010","5","10","FT232R","","APE001.SchLib","","=Value","Standard","Standard","USB Full Speed to Serial UART IC, Includes Oscillator and EEPROM - SSOP-28","","","FTDI","FT232RL-REEL","","",""," Tape and Reel","28 ","","","","","","","APE001.PcbLib","SSOP-28","","","","","","","","Datasheet","http://datasheet.octopart.com/FT232RL-REEL-FTDI-datasheet-11992319.pdf","","APE001","Version 2.10 March-2012","A","2018-02-01","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/FT232RL-REEL-PIC.jpg","partsymbol/FT232R-SCH.jpg","partfootprint/SSOP-28-PCB.jpg","");
INSERT INTO ic VALUES("11","MAX232D","APE000005-011","5","11","MAX232","","APE001.SchLib","","=Value","Standard","Standard","Dual EIA-232 Driver/Receiver 16-SOIC 0 to 70","","","Texas Instruments","MAX232D","","","Surface Mount"," Tape and Reel","16","","","","","","","APE001.PcbLib","SOIC-16","","","","","","","28.6mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=060918a&at=physicalpart&hlid=16637997","","APE001","SLLS047M-FEBRUARY1989-REVISED-NOVEMBER2014","A","2018-02-01","green","Compliant"," Active","","Lead Free","partimg/MAX232D-PIC.jpg","partsymbol/MAX232-SCH.jpg","partfootprint/SOIC-16-PCB.jpg","3dimg/SOIC-16-3D.jpg");
INSERT INTO ic VALUES("12","W5100","APE000005-012","5","12","W5100","","APE001.SchLib","","=Value","Standard","Standard","W5100 Series 3.6 V Hardwired TCP/IP Embedded Ethernet Controller","","","WIZnet","W5100","","","PCB, Surface Mount"," Tray","80","","","","","","","APE001.PcbLib","LQFP-80","","","","","","","","Datasheet","http://datasheet.octopart.com/W5100-WIZnet-datasheet-10558856.pdf","","APE001","Unknown","A","2018-02-01","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/W5100-PIC.jpg","partsymbol/W5100-SCH.jpg","partfootprint/LQFP-80-PCB.jpg","");
INSERT INTO ic VALUES("13","HDG104-DN-2","APE000005-013","5","13","HDG104","","APE001.SchLib","","=Value","Standard","Standard","WIFI 802.11B/G SYSTEM 44QFN","","","H&D Wireless","HDG104-DN-2","","","PCB, Surface Mount"," Cut Tape","","","","","","","","APE001.PcbLib","QFN-44","","","","","","","","Datasheet","http://datasheet.octopart.com/HDG104-DN-2-H%26D-Wireless-datasheet-13265248.pdf","","APE001","Unknown","A","2018-02-01","green","Compliant","Not Listed by Manufacturer","","","partimg/HDG104-DN-2-PIC.jpg","partsymbol/HDG104-SCH.jpg","partfootprint/QFN-44-PCB.jpg","");
INSERT INTO ic VALUES("14","CC2540F256RHAR","APE000005-014","5","14","CC2540","","APE001.SchLib","","=Value","Standard","Standard","SimpleLink Bluetooth Smart Wireless MCU with USB 40-VQFN -40 to 85","","","Texas Instruments","CC2540F256RHAR","","","PCB, Surface Mount"," Tape and Reel","40 ","","","","","","","APE001.PcbLib","VQFN-40","","","","","","","28.6mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0cd46d2&at=physicalpart&hlid=16614127","","APE001","SWRS084F-OCTOBER2010-REVISEDJUNE2013","A","2018-02-01","green","Compliant"," Active","","Lead Free","partimg/CC2540F256RHAR-PIC.jpg","partsymbol/CC2540-SCH.jpg","partfootprint/VQFN-40-PCB.jpg","");
INSERT INTO ic VALUES("15","BALNRF01D3","APE000005-015","5","15","BALNRF01D3","","APE001.SchLib","","=Value","Standard","Standard","BALUN 50OHM INPUT","","","STMicroelectronics","BALNRF01D3","","",""," Tape and Reel","","","","","","","","APE001.PcbLib","ECOPACK","","","","","","","","Datasheet","http://datasheet.octopart.com/BAL-NRF01D3-STMicroelectronics-datasheet-16346591.pdf","","APE001","Unknown","A","2018-02-01","green","Compliant"," Active","","Lead Free","partimg/BALNRF01D3-PIC.jpg","partsymbol/BALNRF01D3-SCH.jpg","partfootprint/ECOPACK-PCB.jpg","");
INSERT INTO ic VALUES("16","2450AT07A0100T","APE000005-016","5","16","2450AT07","","APE001.SchLib","","=Value","Standard","Standard","ULTRA MINI 2.45GHZ CHIP ANTENNA / 7 Inch Reel","","","Johanson","2450AT07A0100T","","","Surface Mount"," Tape and Reel","","","","","","","","APE001.PcbLib","2450AT07A0100T","","","","","","","","Datasheet","http://www.johansontechnology.com/datasheets/antennas/2450AT07A0100.pdf","","APE001","Unknown","A","2018-02-01","green","Compliant","Not Listed by Manufacturer","","","partimg/2450AT07A0100T-PIC.jpg","partsymbol/2450AT07-SCH.jpg","partfootprint/2450AT07A0100T-PCB.jpg","");
INSERT INTO ic VALUES("17","CC3000MOD","APE000005-017","5","17","CC3000","","APE001.SchLib","","=Value","Standard","Standard","Wi-Fi Module IEEE 802.11b/g 11000Kbps 46-pin Tray","","","Texas Instruments","CC3000MOD","","","PCB, Surface Mount"," Tape and Reel","46","","","","","","","APE001.PcbLib","CC3000","","","","","","","","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0efcf26&at=physicalpart&hlid=16614171","","APE001","SWRS126-NOVEMBER2012","A","2018-02-01","green","Non-Compliant"," Active","","Contains Lead","partimg/CC3000MOD-PIC.jpg","partsymbol/CC3000-SCH.jpg","partfootprint/CC3000-PCB.jpg","");
INSERT INTO ic VALUES("18","2500AT44M0400E","APE000005-018","5","18","2500AT44","","APE001.SchLib","","=Value","Standard","Standard","ANT CHIP WIMAX 2.3-2.7GHZ / 7 Inch Reel","","","Johanson","2500AT44M0400E","","","Surface Mount"," Tape and Reel","","","","","","","","APE001.PcbLib","2500AT44M0400E","","","","","","","1.00mm","Datasheet","http://datasheet.octopart.com/2500AT44M0400E-Johanson-datasheet-27138377.pdf","","APE001","5/24/2013","A","2018-02-01","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/2500AT44M0400E-PIC.jpg","partsymbol/2500AT44-SCH.jpg","partfootprint/2500AT44M0400E-PCB.jpg","");
INSERT INTO ic VALUES("19","NRF905","APE000005-019","5","19","NRF905","","APE001.SchLib","","=Value","Standard","Standard","IC TXRX 433/68/915MHZ 32QFN","","","Nordic Power","NRF905","","","PCB, Surface Mount"," Tray ","32","","","","","","","APE001.PcbLib","QFN-32","","","","","","","","Datasheet","http://datasheet.octopart.com/NRF905-Nordic-Semiconductor-datasheet-10669630.pdf","","APE001","Unknown","A","2018-02-01","green","Compliant","Not Listed by Manufacturer","","","partimg/NRF905-PIC.jpg","partsymbol/NRF905-SCH.jpg","partfootprint/QFN-32-PCB.jpg","");
INSERT INTO ic VALUES("20","MCP73832T-2ACI/OT","APE000005-020","5","20","MCP73832T","","APE001.SchLib","","=Value","Standard","Standard","500 mA Single Cell Li-Ion/Li-Polymer Charge Mgmt controller","","","Microchip","MCP73832T-2ACI/OT","","","Surface Mount"," Tape and Reel","5","","","","","","","APE001.PcbLib","SOT-23-5","","","","","","","","Datasheet","http://datasheet.octopart.com/MCP73832T-2ACI/OT-Microchip-datasheet-8814540.pdf","","APE001","Revision E (September 2008)  (A TO E)","A","2018-02-01","green","Compliant"," Active","","Lead Free","partimg/MCP73832T-2ACI-OT-PIC.jpg","partsymbol/MCP73832T-SCH.jpg","partfootprint/SOT-23-5-PCB.jpg","3dimg/SOT-23-5-3D.jpg");
INSERT INTO ic VALUES("21","LM358DR","APE000005-021","5","21","LM358","","APE001.SchLib","","=Value","Standard","Standard","Dual Operational Amplifiers 8-SOIC 0 to 70","","","Texas Instruments","LM358DR","","","Surface Mount"," Tape and Reel","8","","","","","","","APE001.PcbLib","SOIC-8","","","","","","","20.6mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0a03d13&at=physicalpart&hlid=16628525","","APE001","SLOS068S-JUNE1976-REVISED-MAY2013","A","2018-02-01","green","Compliant"," Active","","Lead Free","partimg/LM358DR-PIC.jpg","partsymbol/LM358-SCH.jpg","partfootprint/SOIC-8-PCB.jpg","");
INSERT INTO ic VALUES("22","LMV324IDR","APE000005-022","5","22","LMV324","","APE001.SchLib","","=Value","Standard","Standard","Quad Low-Voltage Rail-to-Rail Output Operational Amplifier 14-SOIC -40 to 125","","","Texas Instruments","LMV324IDR","","","Surface Mount"," Tape and Reel","14 ","","","","","","","APE001.PcbLib","SOIC-14","","","","","","","28.6mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=00dc390&at=physicalpart&hlid=16633906","","APE001","SLOS263W-AUGUST1999-REVISED-OCTOBER2014","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/LMV324IDR-PIC.jpg","partsymbol/LMV324-SCH.jpg","partfootprint/SOIC-14-PCB.jpg","3dimg/SOIC-14-3D.jpg");
INSERT INTO ic VALUES("23","FAN4174IS5X","APE000005-023","5","23","AMP","","APE001.SchLib","","=Value","Standard","Standard","The FAN4174 (single) and FAN4274 (dual) are voltage feedback amplifiers with CMOS inputs that consume only 200 µA of supply current per amplifier, while providing ±33 mA of output short-circuit current. These amplifiers are designed to operate 5 V suppl","","","Fairchild Semiconductor","FAN4174IS5X","","","Surface Mount"," Tape and Reel","5","","","","","","","APE001.PcbLib","SOT-23-5","","","","","","","","Datasheet","http://datasheet.octopart.com/FAN4174IS5X-Fairchild-Semiconductor-datasheet-12512810.pdf","","APE001","FAN4174/FAN4274  Rev. 1.0.8","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/FAN4174IS5X-PIC.jpg","partsymbol/AMP-SCH.jpg","partfootprint/SOT-23-5-PCB.jpg","3dimg/SOT-23-5-3D.jpg");
INSERT INTO ic VALUES("24","DS1307ZN+","APE000005-024","5","24","DS1307","","APE001.SchLib","","=Value","Standard","Standard","RTC SERIAL 64X8, SMD, 1307, SOIC8; Date Format:Day; Date; Month; Year; Clock Format:hh:mm:ss; Clock IC Type:RTC; Interface Type:I2C, Serial; Memory Configuration:64 x 8; Supply Voltage Range:4.5V to 5.5V; Digital IC Case Style:SOIC; No. of Pins:8; Operati","","","Maxim Integrated","DS1307ZN+","","","Surface Mount"," Tube","8","","","","","","","APE001.PcbLib","SOIC-8","","","","","","","","Datasheet","http://datasheet.octopart.com/DS1307ZN%2B-Dallas-Semiconductor-datasheet-11987049.pdf","","APE001"," REV:100208 ","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/DS1307ZN+-PIC.jpg","partsymbol/DS1307-SCH.jpg","partfootprint/SOIC-8-PCB.jpg","");
INSERT INTO ic VALUES("25","NE555DR","APE000005-025","5","25","NE555","","APE001.SchLib","","=Value","Standard","Standard","Single Precision Timer 8-SOIC 0 to 70","","","Texas Instruments","NE555DR","","","Surface Mount"," Tape and Reel","8","","","","","","","APE001.PcbLib","SOIC-8","","","","","","","35.0","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0031693&at=physicalpart&hlid=16640884","","APE001","SLFS022I-SEPTEMBER1973-REVISED-SEPTEMBER2014","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/NE555DR-PIC.jpg","partsymbol/NE555-SCH.jpg","partfootprint/SOIC-8-PCB.jpg","");
INSERT INTO ic VALUES("26","S25FL204K0TMFI011","APE000005-026","5","26","S25FL204","","APE001.SchLib","","=Value","Standard","Standard","IC, FLASH MEM, 4 MBIT, 85 MHZ, 8-SOIC; Memory Type:Flash - NOR; Memory Size:4Mbit; Memory Configuration:-; Supply Voltage Min:2.7V; Supply Voltage Max:3.6V; Memory Case Style:SOIC; No. of Pins:8; Clock Frequency:85MHz ;RoHS Compliant: Yes","","","Spansion","S25FL204K0TMFI011","","",""," Tube","8","","","","","","","APE001.PcbLib","SOIC-8","","","","","","","","Datasheet","http://datasheet.octopart.com/S25FL204K0TMFI011-Spansion-datasheet-16486155.pdf","","APE001","Revision:06 Issue Date:January 17, 2013","A","2018-02-01","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/S25FL204K0TMFI011-PIC.jpg","partsymbol/S25FL204-SCH.jpg","partfootprint/SOIC-8-PCB.jpg","");
INSERT INTO ic VALUES("27","24AA02T-I/OT","APE000005-027","5","27","24AA02T","","APE001.SchLib","","=Value","Standard","Standard","2K 256 X 8 1.8V SERIAL EE IND","","","Microchip","24AA02T-I/OT","","","Surface Mount","","5","","","","","","","APE001.PcbLib","SOT-23-5","","","","","","","","Datasheet","http://datasheet.octopart.com/24AA02T-I/OT-Microchip-datasheet-7546411.pdf","","APE001","02/04/09","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/24AA02T-I-OT-PIC.jpg","partsymbol/24AA02T-SCH.jpg","partfootprint/SOT-23-5-PCB.jpg","3dimg/SOT-23-5-3D.jpg");
INSERT INTO ic VALUES("28","CAT24C32WI-GT3","APE000005-028","5","28","CAT24C32WI","","APE001.SchLib","","=Value","Standard","Standard","CAT24C32 Series 32 Kb (4K X 8) 1.8 V - 5.5 V I2C CMOS Serial EEPROM - SOIC-8","","","ON Semiconductor","CAT24C32WI-GT3","","","Surface Mount"," Tape and Reel","8","","","","","","","APE001.PcbLib","SOIC-8","","","","","","","","Datasheet","http://datasheet.octopart.com/CAT24C32WI-GT3-ON-Semiconductor-datasheet-14714113.pdf","","APE001"," March, 2013?Rev. 16","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/CAT24C32WI-GT3-PIC.jpg","partsymbol/CAT24C32WI-SCH.jpg","partfootprint/SOIC-8-PCB.jpg","");
INSERT INTO ic VALUES("29","ATMEGA328P-AU","APE000005-029","5","29","ATMEGA328P","","APE001.SchLib","","=Value","Standard","Standard","ATmega Series 20 MHz 32 KB Flash 2 KB SRAM 8-Bit Microcontroller - TQFP-32","","","Atmel","ATMEGA328P-AU","","",""," Tube","32","","","","","","","APE001.PcbLib","TQFP-32","","","","","","","","Datasheet","http://datasheet.octopart.com/ATMEGA328P-AU-Atmel-datasheet-14421303.pdf","","APE001"," 8271GS-AVR-02/201","A","2018-02-01","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/ATMEGA328P-AU-PIC.jpg","partsymbol/ATMEGA328P-SCH.jpg","partfootprint/TQFP-32-PCB.jpg","");
INSERT INTO ic VALUES("30","ATMEGA328P-PU","APE000005-030","5","30","ATMEGA328P-PU","","APE001.SchLib","","=Value","Standard","Standard","ATmega Series 20 MHz 32 KB Flash 2 KB SRAM 8-Bit Microcontroller - DIP-28","","","Atmel","ATMEGA328P-PU","","",""," Tape and Reel","32","","","","","","","APE001.PcbLib","DIP-28","","","","","","","","Datasheet","http://datasheet.octopart.com/ATMEGA328P-PU-Atmel-datasheet-14421303.pdf","","APE001","Rev.: 8271GS-AVR-02/2013","A","2018-02-01","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/ATMEGA328P-PU-PIC.jpg","partsymbol/ATMEGA328P-PU-SCH.jpg","partfootprint/DIP-28-PCB.jpg","");
INSERT INTO ic VALUES("31","ATMEGA1284P-AU","APE000005-031","5","31","ATMEGA1284P","","APE001.SchLib","","=Value","Standard","Standard","ATmega Series 20 MHz 128 KB Flash 16 KB SRAM 8-Bit Microcontroller - TQFP-44","","","Atmel","ATMEGA1284P-AU","","",""," Tape and Reel","44","","","","","","","APE001.PcbLib","TQFP-44","","","","","","","","Datasheet","http://datasheet.octopart.com/ATMEGA1284P-AU-Atmel-datasheet-13381179.pdf","","APE001","8272D-AVR-05/12","A","2018-02-01","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/ATMEGA1284P-AU-PIC.jpg","partsymbol/ATMEGA1284P-SCH.jpg","partfootprint/TQFP-44-PCB.jpg","");
INSERT INTO ic VALUES("32","ATMEGA2560V8AU","APE000005-032","5","32","ATMEGA2560","","APE001.SchLib","","=Value","Standard","Standard","ATmega Series 8 MHz 256 KB Flash 8 KB SRAM 8-Bit Microcontroller - TQFP-100","","","Atmel","ATMEGA2560V8AU","","","Surface Mount"," Tray ","100","","","","","","","APE001.PcbLib","TQFP-100","","","","","","","","Datasheet","http://datasheet.octopart.com/ATMEGA2560V8AU-Atmel-datasheet-24967521.pdf","","APE001","2549QS-AVR-02/201","A","2018-02-01","green"," Compliant","Not Listed by Manufacturer","","Lead Free","partimg/ATMEGA2560V8AU-PIC.jpg","partsymbol/ATMEGA2560-SCH.jpg","partfootprint/TQFP-100-PCB.jpg","");
INSERT INTO ic VALUES("33","ATTINY85-20PU","APE000005-033","5","33","ATTINY85","","APE001.SchLib","","=Value","Standard","Standard","AVR, 4KB FLASH, 256B SRAM, ADC, 2 TIMERS - 5V, 20MHZ, PDIP, IND TEMP, GREEN","","","Atmel","ATTINY85-20PU","","","Through Hole"," Tube","8","","","","","","","APE001.PcbLib","DIP-8","","","","","","","","Datasheet","http://datasheet.octopart.com/ATTINY85-20PU-Atmel-datasheet-10223956.pdf","","APE001","2586N-AVR-04/11","A","2018-02-01","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/ATTINY85-20PU-PIC.jpg","partsymbol/ATTINY85-SCH.jpg","partfootprint/DIP-8-PCB.jpg","");
INSERT INTO ic VALUES("34","LPC1768FBD100,551","APE000005-034","5","34","LPC1768","","APE001.SchLib","","=Value","Standard","Standard","LPC1768FBD100","","","NXP Semiconductors","LPC1768FBD100,551","","",""," Tray","100","","","","","","","APE001.PcbLib","LQFP-100","","","","","","","","Datasheet","http://datasheet.octopart.com/LPC1768FBD100%2C551-NXP-Semiconductors-datasheet-10855012.pdf","","APE001","Rev. 8 - 14 November 2011","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/LPC1768FBD100,551-PIC.jpg","partsymbol/LPC1768-SCH.jpg","partfootprint/LQFP-100-PCB.jpg","");
INSERT INTO ic VALUES("35","STM32F103CBT6","APE000005-035","5","35","STM32F103CBT6","","APE001.SchLib","","=Value","Standard","Standard","Mainstream Performance line, ARM Cortex-M3 MCU with 128 Kbytes Flash, 72 MHz CPU, motor control, USB and CAN","","","STMicroelectronics","STM32F103CBT6","","","Surface Mount"," Tray","48","","","","","","","APE001.PcbLib","LQFP-48","","","","","","","","Datasheet","http://datasheet.octopart.com/STM32F103CBT6-STMicroelectronics-datasheet-10330614.pdf","","APE001","April 2011 Doc ID 13587 Rev 13 ","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/STM32F103CBT6-PIC.jpg","partsymbol/STM32F103CBT6-SCH.jpg","partfootprint/LQFP-48-PCB.jpg","");
INSERT INTO ic VALUES("36","STM32F103RBT6","APE000005-036","5","36","STM32F103RBT6","","APE001.SchLib","","=Value","Standard","Standard","Mainstream Performance line, ARM Cortex-M3 MCU with 128 Kbytes Flash, 72 MHz CPU, motor control, USB and CAN","","","STMicroelectronics","STM32F103RBT6","","","Surface Mount"," Tray","64","","","","","","","APE001.PcbLib","LQFP-64","","","","","","","","Datasheet","http://datasheet.octopart.com/STM32F103RBT6-STMicroelectronics-datasheet-500013.pdf","","APE001","September 2008 Rev 9 ","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/STM32F103RBT6-PIC.jpg","partsymbol/STM32F103RBT6-SCH.jpg","partfootprint/LQFP-64-PCB.jpg","3dimg/LQFP-64-3D.jpg");
INSERT INTO ic VALUES("37","MK22FN1M0VLH12","APE000005-037","5","37","MK22FN1","","APE001.SchLib","","=Value","Standard","Standard","MCU 32-bit Kinetis K22 ARM Cortex M4 1024KB Flash 3.3V 64-Pin LQFP Tray","","","Freescale Semiconductor","MK22FN1M0VLH12","","","","Tray","64","","","","","","","APE001.PcbLib","LQFP-64","","","","","","","","Datasheet","http://datasheet.octopart.com/MK22FN1M0VLH12-Freescale-Semiconductor-datasheet-16471319.pdf","","APE001","Rev. 2, 05/2013","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/MK22FN1M0VLH12-PIC.jpg","partsymbol/MK22FN1-SCH.jpg","partfootprint/LQFP-64-PCB.jpg","3dimg/LQFP-64-3D.jpg");
INSERT INTO ic VALUES("38","MSP430F2132IPW","APE000005-038","5","38","MSP430F","","APE001.SchLib","","=Value","Standard","Standard","16-bit Ultra-Low-Power Microcontroller, 8kB Flash, 512B RAM, 10 bit ADC, 1 USCI 28-TSSOP -40 to 85","","","Texas Instruments","MSP430F2132IPW","","","","Tape and Reel","28 ","","","","","","","APE001.PcbLib","TSSOP-28","","","","","","","35.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0252055&at=physicalpart&hlid=16639135","","APE001","SLAS578J-NOVEMBER2007-REVISED-JANUARY2012","A","2018-02-01","green","Compliant","Active","","Lead Free","partimg/MSP430F2132IPW-PIC.jpg","partsymbol/MSP430F-SCH.jpg","partfootprint/TSSOP-28-PCB.jpg","");
INSERT INTO ic VALUES("39","SPU0409HD5H-PB","APE000005-039","5","39","SPU0409HD5H","","APE001.SchLib","","=Value","Standard","Standard","; Transducer Function:Microphone; Device Type:UltraMini Microphone; Directivity:Omni Directional; Sensitivity (dB):-42dB; Output Impedance:300ohm; Length:3.76mm; Sensitivity:-42 dB","","","Knowles Acoustics","SPU0409HD5H-PB","","","Surface Mount"," Tape and Reel","4","","","","","","","APE001.PcbLib","SPU-4","","","","","","","1.00mm","Datasheet","http://datasheet.octopart.com/SPU0409HD5H-PB-Knowles-Acoustics-datasheet-17019172.pdf","","APE001","Revision:F 3/27/2013","A","2018-02-01","green","Compliant","Not Listed by Manufacturer","","","partimg/SPU0409HD5H-PB-PIC.jpg","partsymbol/SPU0409HD5H-SCH.jpg","partfootprint/SPU-4-PCB.jpg","");
INSERT INTO ic VALUES("40","MMA8452QR1","APE000005-040","5","40","MMA8452Q","","APE001.SchLib","","=Value","Standard","Standard","IC, DIGITAL ACCELERATION SENSOR, ±2G / ±4G / ±8G, 12BIT, QFN-16; Acceleration Range:± 2g, ± 4g, ± 8g; No. of Axes:3; IC Interface Type:I2C; Sensor Case Style:QFN; No. of Pins:16; Supply Voltage Range:1.95V to 3.6V ;RoHS Compliant: Yes","","","Freescale Semiconductor","MMA8452QR1","","","Surface Mount","Tape and Reel","16","","","","","","","APE001.PcbLib","QFN-16","","","","","","","","Datasheet","http://datasheet.octopart.com/MMA8452QR1-Freescale-Semiconductor-datasheet-21199847.pdf","","APE001","Rev. 8.1, 10/2013","A","2018-02-01","green","Compliant","Active","","Contains Lead","partimg/MMA8452QR1-PIC.jpg","partsymbol/MMA8452Q-SCH.jpg","partfootprint/QFN-16-PCB.jpg","3dimg/QFN-16-3D.jpg");
INSERT INTO ic VALUES("41","DS18B20U+","APE000005-041","5","41","DS18B20U+","","APE001.SchLib","","=Value","Standard","Standard","THERMOMETER, DIG, PROG, SMD, 18B20; IC Output Type:Digital; Sensing Accuracy Range:± 0.5°C; Temperature Sensing Range:-55°C to +125°C; Supply Current:1mA; Supply Voltage Range:3V to 5.5V; Resolution (Bits):12bit; Sensor Case Style:MSOP; No. of Pins:8;","","","Maxim Integrated","DS18B20U+","","","Surface Mount","Tape and Reel","8","","","","","","","APE001.PcbLib","MSOP-8","","","","","","","","Datasheet","http://datasheet.octopart.com/DS18B20U%2B-Dallas-Semiconductor-datasheet-11984949.pdf","","APE001","REV:042208 ","A","2018-02-01","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/DS18B20U+-PIC.jpg","partsymbol/DS18B20U+-SCH.jpg","partfootprint/MSOP-8-PCB.jpg","3dimg/MSOP-8-3D.jpg");





DROP TABLE misc;

CREATE TABLE `misc` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacture_Part_Number1` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(255) DEFAULT NULL,
  `Operating_Temperature` varchar(255) DEFAULT NULL,
  `Package_Case` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `Height` varchar(10) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO misc VALUES("2","1825289-1","APE000003-002","MHS222","APE001.SchLib","","=Value","Standard","Standard","Switch Slide DPDT Top Slide 0.3A 125VAC PC Pins Thru-Hole Tray","","MHS222","TE Connectivity / Alcoswitch","1825289-1","","","Through Hole","- 20 �C to + 85 �C","MHS222","Bulk","","Compliant","Active","","","","","","","APE001.PcbLib","MHS222","","Datasheet","http://sigma.octopart.com/14475282/technical_drawing/Tyco-Electronics-1825289-1.pdf","","green","","APE001","REVISED PER ECO-11-004587","A","2018-02-01","Lead Free","partimg/1825289-1-PIC.jpg","partsymbol/MHS222-SCH.jpg","partfootprint/MHS222-PCB.jpg","");
INSERT INTO misc VALUES("3","B3U-3000P","APE000003-003","B3U","APE001.SchLib","","=Value","Standard","Standard","TACTILE SWITCH, SIDE ACTUATED, SMD; Contact Configuration:SPST-NO; Operating Force:1.59N; Contact Voltage DC Nom:12V; Contact Current Max:50mA; Actuation Type:Side; Switch Terminals:Surface Mount; SVHC:No SVHC (20-Jun-2011); Actuator Type:Side Actuated; C","","B3U","Omron","B3U-3000P","","","Surface Mount, Right Angle","�25  �C to +70�C","B3U"," Tape & Reel","","Compliant","Not Listed by Manufacturer","","","","","","","APE001.PcbLib","B3U","","Datasheet","http://datasheet.octopart.com/B3U-3000P-Omron-datasheet-10910421.pdf","","green","","APE001","Unknown","A","2018-02-01","Lead Free","partimg/B3U-3000P-PIC.jpg","partsymbol/B3U-SCH.jpg","partfootprint/B3U-PCB.jpg","");
INSERT INTO misc VALUES("4","1812L050PR","APE000003-004","FUSE_SMT","APE001.SchLib","","=Value","Standard","Standard","FUSE, RESETTABLE, 1812, 15V, 500MA; Tripping Current:1A; Carrying Current Max:100A; Initial Resistance Max:1.0ohm; Operating Voltage:15VDC; Series:1812L; SVHC:No SVHC (20-Jun-2011); Approval Bodies:TUV; Approval Category:UL Recognised; External Depth:3.41","","1812","Littelfuse","1812L050PR","","","Surface Mounting","- 40 �C to + 85 �C","1812"," Tape & Reel","","Compliant","Not Listed by Manufacturer","","","","","","","APE001.PcbLib","1812","","Datasheet","http://datasheet.octopart.com/1812L050PR-Littelfuse-datasheet-8701984.pdf","","green","","APE001","Revised: June 28, 2010 ","A","2018-02-01","Lead Free","partimg/1812L050PR-PIC.jpg","partsymbol/FUSE_SMT-SCH.jpg","partfootprint/1812-PCB.jpg","3dimg/1812.jpg");
INSERT INTO misc VALUES("5","1812L075PR","APE000003-005","FUSE_SMT","APE001.SchLib","","=Value","Standard","Standard","FUSE, RESETTABLE, 1812, 13.2V, 750MA; Tripping Current:1.5A; Carrying Current Max:100A; Initial Resistance Max:0.45ohm; Operating Voltage:13.2VDC; Series:1812L; SVHC:No SVHC (20-Jun-2011); Approval Bodies:TUV; Approval Category:UL Recognised; External Dep","","1812","Littelfuse","1812L075PR","","","Surface Mounting","- 40 �C to + 85 �C","1812"," Tape & Reel","","Compliant","Not Listed by Manufacturer","","","","","","","APE001.PcbLib","1812","","Datasheet","http://datasheet.octopart.com/1812L075PR-Littelfuse-datasheet-24577145.pdf","","green","","APE001","Revised: 04/01/14 ","A","2018-02-01","Lead Free","partimg/1812L075PR-PIC.jpg","partsymbol/FUSE_SMT-SCH.jpg","partfootprint/1812-PCB.jpg","3dimg/1812.jpg");
INSERT INTO misc VALUES("6","1812L110/33MR","APE000003-006","FUSE_SMT","APE001.SchLib","","=Value","Standard","Standard","FUSE, PTC RESET, 33V, 1.1A, 1812; Holding Current:1.1A; Tripping Current:1.95A; Initial Resistance Max:0.2ohm; Operating Voltage:33V; Series:POLYFUSE; PTC Fuse Case:1812; External Depth:2mm; Initial Resistance Min:0.06ohm ;RoHS Compliant: Yes","","1812","Littelfuse","1812L110/33MR","","","Surface Mounting","- 40 �C to + 85 �C","1812"," Tape & Reel","","Compliant","Not Listed by Manufacturer","","","","","","","APE001.PcbLib","1812","","Datasheet","http://datasheet.octopart.com/1812L110/33MR-Littelfuse-datasheet-8701984.pdf","","green","","APE001","Revised: June 28, 2011","A","2018-02-01","Lead Free","partimg/1812L110-33MR-PIC.jpg","partsymbol/FUSE_SMT-SCH.jpg","partfootprint/1812-PCB.jpg","3dimg/1812.jpg");
INSERT INTO misc VALUES("7","1812L160/12DR","APE000003-007","FUSE_SMT","APE001.SchLib","","=Value","Standard","Standard","POLYFUSE 1.6A 12V SMD 1812 RoHSconf","","1812","Littelfuse","1812L160/12DR","","","Surface Mounting","- 40 �C to + 85 �C","1812"," Tape & Reel","","Compliant","Not Listed by Manufacturer","","","","","","","APE001.PcbLib","1812","","Datasheet","http://datasheet.octopart.com/1812L160-12DR-Littelfuse-datasheet-8701984.pdf","","green","","APE001","Revised: June 28, 2010","A","2018-02-01","Lead Free","partimg/1812L160-12DR-PIC.jpg","partsymbol/FUSE_SMT-SCH.jpg","partfootprint/1812-PCB.jpg","3dimg/1812.jpg");





DROP TABLE new_part;

CREATE TABLE `new_part` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(225) DEFAULT NULL,
  `CPN` varchar(225) DEFAULT NULL,
  `Description` varchar(225) DEFAULT NULL,
  `Manufacturer` varchar(225) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `table_name` varchar(45) DEFAULT NULL,
  `Library_Ref` varchar(225) DEFAULT NULL,
  `Library_Path` varchar(45) DEFAULT NULL,
  `Category` varchar(45) DEFAULT NULL,
  `Comment` varchar(45) DEFAULT NULL,
  `Component_Kind` varchar(45) DEFAULT NULL,
  `Component_Type` varchar(45) DEFAULT NULL,
  `Designator` varchar(45) DEFAULT NULL,
  `Footprint` varchar(45) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Packaging` varchar(45) DEFAULT NULL,
  `Pin_Count` varchar(45) DEFAULT NULL,
  `Signal_Integrity` varchar(45) DEFAULT NULL,
  `Simulation` varchar(45) DEFAULT NULL,
  `Supplier` varchar(45) DEFAULT NULL,
  `Supplier_Part_Number` varchar(45) DEFAULT NULL,
  `Footprint_Path` varchar(45) DEFAULT NULL,
  `Footprint_Ref` varchar(45) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(45) DEFAULT NULL,
  `Supplier1` varchar(45) DEFAULT NULL,
  `ComponentLink1Description` varchar(45) DEFAULT NULL,
  `ComponentLink1URL` varchar(225) DEFAULT NULL,
  `PrePN` varchar(45) DEFAULT NULL,
  `PostPN` varchar(45) DEFAULT NULL,
  `TEMPCO` varchar(45) DEFAULT NULL,
  `Tolerance` varchar(45) DEFAULT NULL,
  `Value` varchar(45) DEFAULT NULL,
  `Height` varchar(45) DEFAULT NULL,
  `Size_Dimension` varchar(45) DEFAULT NULL,
  `Composition` varchar(45) DEFAULT NULL,
  `Number_of_Terminations` varchar(45) DEFAULT NULL,
  `Power_Watts` varchar(45) DEFAULT NULL,
  `Resistance_Ohms` varchar(45) DEFAULT NULL,
  `Operating_Temperature` varchar(45) DEFAULT NULL,
  `Package_Case` varchar(45) DEFAULT NULL,
  `Capacitance` varchar(45) DEFAULT NULL,
  `Thickness_Max` varchar(45) DEFAULT NULL,
  `Voltage_Rated` varchar(45) DEFAULT NULL,
  `Lead_Spacing` varchar(45) DEFAULT NULL,
  `RoHS` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(45) DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL,
  `Supplier_Device_Package` varchar(45) DEFAULT NULL,
  `Minimum_Orde_Quantity` varchar(45) DEFAULT NULL,
  `Reeling` varchar(45) DEFAULT NULL,
  `Product_Type` varchar(45) DEFAULT NULL,
  `Reverse_Recovery_Time_trr` varchar(45) DEFAULT NULL,
  `ESR_Equivalent_Series_Resistance` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `starter` varchar(45) DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `noti_status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE oscillator;

CREATE TABLE `oscillator` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(255) DEFAULT NULL,
  `Operating_Temperature` varchar(255) DEFAULT NULL,
  `Package_Case` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `Height` varchar(10) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO oscillator VALUES("1","CSTCE16M0V53-R0","APE000008-001","RESONATOR","APE001.SchLib","","=Value","Standard","Standard","RESONATOR, SMD, 16MHZ; Frequency:16MHz; Frequency Tolerance:� 0.5%; Frequency Stability:� 0.3%; Resonant Impedance:40ohm; Oscillator Mounting:SMD; Operating Temperature Range:-20�C to +80�C; SVHC:No SVHC (19-Dec-2011)","","","Murata","CSTCE16M0V53-R0","","","Surface Mount","-20�C to +80�C","3-SMD, Non-Standard","Tape & Reel ","3","","","","","APE001.PcbLib","3-SMD","1.00mm","http://datasheet.octopart.com/CSTCE16M0V53-R0-Murata-datasheet-5335552.pdf","Datasheet","","","","APE001","08.3.3","A","2018-02-01","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/CSTCE16M0V53-R0-PIC.jpg","partsymbol/RESONATOR-SCH.jpg","partfootprint/3-SMD-PCB.jpg","3dimg/3-SMD-3D.jpg");
INSERT INTO oscillator VALUES("2","ABM3B-8.000MHZ-B2-T","APE000008-002","CRYSTAL4","APE001.SchLib","","=Value","Standard","Standard","CRYSTAL, 8MHZ, 18PF, SMD; Frequency:8MHz; Load Capacitance:18pF; Frequency Tolerance:� 20ppm; Operating Temperature Range:-20�C to +70�C; Crystal Mounting Type:SMD; No. of Pins:4; Frequency Stability:50ppm ;RoHS Compliant: Yes","","","Abracon","ABM3B-8.000MHZ-B2-T","","","Surface Mount","-20�C to +70�C","4-SMD, No Lead","Tape and Reel","4","","","","","APE001.PcbLib","4-SMD","1.1mm","http://datasheet.octopart.com/ABM3B-8.000MHZ-B2-T-Abracon-datasheet-5300259.pdf","Datasheet","","","","APE001","Revised (O): 03.11.08","A","2018-02-01","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/ABM3B-8.000MHZ-B2-T-PIC.jpg","partsymbol/CRYSTAL4-SCH.jpg","partfootprint/4-SMD-PCB.jpg","3dimg/4-SMD-3D.jpg");
INSERT INTO oscillator VALUES("3","ABM3B-12.000MHZ-B2-T","APE000008-003","CRYSTAL4","APE001.SchLib","","=Value","Standard","Standard","CRYSTAL, 12M, 18PF CL, 5X3.2 SMT; Frequency:12MHz; Frequency Tolerance:� 50ppm; Load Capacitance:18pF; Operating Temperature Range:-20�C to +70�C; Crystal Mounting Type:SMD; Crystal Case Type:5mm x 3.2mm; No. of Pins:4; SVHC:No SVHC (20-Jun-2011); Accu","","","Abracon","ABM3B-12.000MHZ-B2-T","","","Surface Mount","-20�C to +70�C","4-SMD, No Lead","Tape and Reel","4","","","","","APE001.PcbLib","4-SMD","1.1mm","http://datasheet.octopart.com/ABM3B-12.000MHZ-B2-T-Abracon-datasheet-5300259.pdf","Datasheet","","","","APE001","Revised (O): 03.11.08","A","2018-02-01","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/ABM3B-12.000MHZ-B2-T-PIC.jpg","partsymbol/CRYSTAL4-SCH.jpg","partfootprint/4-SMD-PCB.jpg","3dimg/4-SMD-3D.jpg");
INSERT INTO oscillator VALUES("4","ABM3B-12.288MHZ-B2-T","APE000008-004","CRYSTAL4","APE001.SchLib","","=Value","Standard","Standard","MICROP. CRYSTAL, 12.288MHZ; Frequency:12.288MHz; Frequency Tolerance:� 20ppm; Load Capacitance:18pF; Frequency Stability:� 50ppm; Operating Temperature Range:-20�C to +70�C; Crystal Mounting Type:SMD; No. of Pins:4; SVHC:No SVHC (20-Jun-2011); Output ","","","Abracon","ABM3B-12.288MHZ-B2-T","","","Surface Mount","-20�C to +70�C","4-SMD, No Lead","Tape and Reel","4","","","","","APE001.PcbLib","4-SMD","1.1mm","http://datasheet.octopart.com/ABM3B-12.288MHZ-B2-T-Abracon-datasheet-5300259.pdf","Datasheet","","","","APE001","Revised (O): 03.11.08","A","2018-02-01","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/ABM3B-12.288MHZ-B2-T-PIC.jpg","partsymbol/CRYSTAL4-SCH.jpg","partfootprint/4-SMD-PCB.jpg","3dimg/4-SMD-3D.jpg");
INSERT INTO oscillator VALUES("5","ABM3B-16.000MHZ-B2-T","APE000008-005","CRYSTAL4","APE001.SchLib","","=Value","Standard","Standard","CRYSTAL, 16M, 18PF CL, 5X3.2 SMT; Frequency:16MHz; Frequency Tolerance:� 50ppm; Load Capacitance:18pF; Operating Temperature Range:-20�C to +70�C; Crystal Mounting Type:SMD; Crystal Case Type:5mm x 3.2mm; No. of Pins:4; SVHC:No SVHC (20-Jun-2011); Accu","","","Abracon","ABM3B-16.000MHZ-B2-T","","","Surface Mount","-20�C to +70�C","4-SMD, No Lead","Tape and Reel","4","","","","","APE001.PcbLib","4-SMD","1.1mm","http://datasheet.octopart.com/ABM3B-16.000MHZ-B2-T-Abracon-datasheet-5300259.pdf","Datasheet","","","","APE001","Revised (O): 03.11.08","A","2018-02-01","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/ABM3B-16.000MHZ-B2-T-PIC.jpg","partsymbol/CRYSTAL4-SCH.jpg","partfootprint/4-SMD-PCB.jpg","3dimg/4-SMD-3D.jpg");
INSERT INTO oscillator VALUES("6","ABM3B-20.000MHZ-B2-T","APE000008-006","CRYSTAL4","APE001.SchLib","","=Value","Standard","Standard","CRYSTAL, 20M, 18PF CL, 5X3.2 SMT; Frequency:20MHz; Frequency Tolerance:� 50ppm; Load Capacitance:18pF; Operating Temperature Range:-20�C to +70�C; Crystal Mounting Type:SMD; Crystal Case Type:5mm x 3.2mm; No. of Pins:4; SVHC:No SVHC (20-Jun-2011); Accu","","","Abracon","ABM3B-20.000MHZ-B2-T","","","Surface Mount","-20�C to +70�C","4-SMD, No Lead","Tape and Reel","4","","","","","APE001.PcbLib","4-SMD","1.1mm","http://datasheet.octopart.com/ABM3B-20.000MHZ-B2-T-Abracon-datasheet-5300259.pdf","Datasheet","","","","APE001","Revised (O): 03.11.08","A","2018-02-01","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/ABM3B-20.000MHZ-B2-T-PIC.jpg","partsymbol/CRYSTAL4-SCH.jpg","partfootprint/4-SMD-PCB.jpg","3dimg/4-SMD-3D.jpg");
INSERT INTO oscillator VALUES("7","ABM3B-24.000MHZ-B2-T","APE000008-007","CRYSTAL4","APE001.SchLib","","=Value","Standard","Standard","Crystal; Frequency:24MHz; Frequency Tolerance:� 20ppm; Operating Temperature Range:-20�C to +70�C; Load Capacitance:18pF; Crystal Case:Ceramic; Crystal Terminals:Surface Mount (SMD, SMT); Crystal Type:Standard; ESR:50ohm ;RoHS Compliant: Yes","","","Abracon","ABM3B-24.000MHZ-B2-T","","","Surface Mount","-20�C to +70�C","4-SMD, No Lead","Tape and Reel","4","","","","","APE001.PcbLib","4-SMD","1.1mm","http://datasheet.octopart.com/ABM3B-24.000MHZ-B2-T-Abracon-datasheet-5300259.pdf","Datasheet","","","","APE001","Revised (O): 03.11.08","A","2018-02-01","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/ABM3B-24.000MHZ-B2-T-PIC.jpg","partsymbol/CRYSTAL4-SCH.jpg","partfootprint/4-SMD-PCB.jpg","3dimg/4-SMD-3D.jpg");





DROP TABLE parts_log;

CREATE TABLE `parts_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_name` varchar(225) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `user` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE resistor;

CREATE TABLE `resistor` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `PrePN` int(11) DEFAULT NULL,
  `PostPN` int(11) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `TEMPCO` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Tolerance` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Height` varchar(255) DEFAULT NULL,
  `Size_Dimension` varchar(255) DEFAULT NULL,
  `Composition` varchar(255) DEFAULT NULL,
  `Number_of_Terminations` varchar(255) DEFAULT NULL,
  `Power_Watts` varchar(255) DEFAULT NULL,
  `Resistance_Ohms` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

INSERT INTO resistor VALUES("2","10.0 - �1% - 0603","RES","APE000001-002","1","2","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 10.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF10R0V","","","Taping","","","","","","","","�1 %","APE001.PcbLib","R0603","10.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","10.0","Datasheet","http://datasheet.octopart.com/ERJ3EKF10R0V-Panasonic-datasheet-5313456.pdf","","APE001","Unknown","A","2018-02-01","green","","Compliant","Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF10R0V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("3","12.0 - �1% - 0603","RES","APE000001-003","1","3","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 12.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF12R0V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","12.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","12.0","Datasheet","http://datasheet.octopart.com/ERJ3EKF12R0V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF12R0V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("4","15.0 - �1% - 0603","RES","APE000001-004","1","4","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 15.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF15R0V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","15.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","15.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF15R0V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF15R0V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("5","22.0 - �1% - 0603","RES","APE000001-005","1","5","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 22.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF22R0V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","22.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","22.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF22R0V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF22R0V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("6","33.2 - �1% - 0603","RES","APE000001-006","1","6","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 33.2 - �1% - 0.1","","","Panasonic","ERJ-3EKF33R2V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","33.2","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","33.2","Datasheet","http://datasheet.octopart.com/ERJ-3EKF33R2V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF33R2V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("7","47.0 - �1% - 0603","RES","APE000001-007","1","7","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 47.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF47R0V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","47.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","47.0","Datasheet","http://datasheet.octopart.com/ERJ3EKF47R0V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF47R0V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("8","49.9 - �1% - 0603","RES","APE000001-008","1","8","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 49.9 - �1% - 0.1","","","Panasonic","ERJ-3EKF49R9V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","49.9","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","49.9","Datasheet","http://datasheet.octopart.com/ERJ-3EKF49R9V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF49R9V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("9","56.2 - �1% - 0603","RES","APE000001-009","1","9","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 56.2 - �1% - 0.1","","","Panasonic","ERJ-3EKF56R2V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","56.2","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","56.2","Datasheet","http://datasheet.octopart.com/ERJ-3EKF56R2V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF56R2V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("10","68.1 - �1% - 0603","RES","APE000001-010","1","10","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 68.1 - �1% - 0.1","","","Panasonic","ERJ-3EKF68R1V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","68.1","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","68.1","Datasheet","http://datasheet.octopart.com/ERJ-3EKF68R1V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF68R1V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("11","75.0 - �1% - 0603","RES","APE000001-011","1","11","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 75.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF75R0V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","75.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","75.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF75R0V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF75R0V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("12","82.5 - �1% - 0603","RES","APE000001-012","1","12","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 82.5 - �1% - 0.1","","","Panasonic","ERJ-3EKF82R5V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","82.5","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","82.5","Datasheet","http://datasheet.octopart.com/ERJ-3EKF82R5V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF82R5V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("13","100.0 - �1% - 0603","RES","APE000001-013","1","13","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 100.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF1000V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","100.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","100.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF1000V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1000V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("14","120.0 - �1% - 0603","RES","APE000001-014","1","14","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 120.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF1200V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","120.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","120.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF1200V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1200V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("15","150.0 - �1% - 0603","RES","APE000001-015","1","15","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 150.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF1500V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","150.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","150.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF1500V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1500V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("16","220.0 - �1% - 0603","RES","APE000001-016","1","16","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 220.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF2200V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","220.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","220.0","Datasheet","http://datasheet.octopart.com/ERJ3EKF2200V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF2200V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("17","390.0 - �1% - 0603","RES","APE000001-017","1","17","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 390.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF3900V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","390.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","390.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF3900V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF3900V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("18","470.0 - �1% - 0603","RES","APE000001-018","1","18","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 470.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF4700V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","470.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","470.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF4700V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF4700V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("19","560.0 - �1% - 0603","RES","APE000001-019","1","19","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 560.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF5600V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","560.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","560.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF5600V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF5600V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("20","681.0 - �1% - 0603","RES","APE000001-020","1","20","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 681.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF6810V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","681.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","681.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF6810V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF6810V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("21","820.0 - �1% - 0603","RES","APE000001-021","1","21","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 820.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF8200V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","820.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","820.0","Datasheet","http://datasheet.octopart.com/ERJ3EKF8200V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012 ","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF8200V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("22","1K - �1% - 0603","RES","APE000001-022","1","22","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 1K - �1% - 0.1","","","Panasonic","ERJ-3EKF1001V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","1K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","1K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1001V-Panasonic-datasheet-5313456.pdf","","APE001","Oct. 2008","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1001V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("23","1.2K - �1% - 0603","RES","APE000001-023","1","23","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 1.2K - �1% - 0.1","","","Panasonic","ERJ-3EKF1201V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","1.2K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","1.2K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1201V-Panasonic-datasheet-13266541.pdf","","APE001","03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1201V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("24","1.5K - �1% - 0603","RES","APE000001-024","1","24","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 1.5K - �1% - 0.1","","","Panasonic","ERJ-3EKF1501V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","1.5K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","1.5K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF1501V-Panasonic-datasheet-13266541.pdf","","APE001","03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1501V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("25","2K - �1% - 0603","RES","APE000001-025","1","25","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 2K - �1% - 0.1","","","Panasonic","ERJ-3EKF2001V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","2K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","2K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF2001V-Panasonic-datasheet-13266541.pdf","","APE001","03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF2001V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("26","3.6K - �1% - 0603","RES","APE000001-026","1","26","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 3.6K - �1% - 0.1","","","Panasonic","ERJ-3EKF3601V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","3.6K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","3.6K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF3601V-Panasonic-datasheet-13293574.pdf","","APE001","03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF3601V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("27","4.7K - �1% - 0603","RES","APE000001-027","1","27","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 4.7K - �1% - 0.1","","","Panasonic","ERJ-3EKF4701V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","4.7K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","4.7K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF4701V-Panasonic-datasheet-5313456.pdf","","APE001","Oct. 2008","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF4701V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("28","680.0 - �1% - 0603","RES","APE000001-028","1","28","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 680.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF6800V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","680.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","680.0","Datasheet","http://datasheet.octopart.com/ERJ3EKF6800V-Panasonic-datasheet-13266541.pdf","","APE001","03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF6800V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("29","10K - �1% - 0603","RES","APE000001-029","1","29","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 10K - �1% - 0.1","","","Panasonic","ERJ-3EKF1002V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","10K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","10K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1002V-Panasonic-datasheet-5313456.pdf","","APE001","Oct. 2008","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1002V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("30","12K - �1% - 0603","RES","APE000001-030","1","30","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 12K - �1% - 0.1","","","Panasonic","ERJ-3EKF1202V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","12K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","12K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1202V-Panasonic-datasheet-13266541.pdf","","APE001","03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1202V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("31","15K - �1% - 0603","RES","APE000001-031","1","31","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 15K - �1% - 0.1","","","Panasonic","ERJ-3EKF1502V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","15K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","15K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1502V-Panasonic-datasheet-5313456.pdf","","APE001","Oct. 2008","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1502V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("32","22K - �1% - 0603","RES","APE000001-032","1","32","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 22K - �1% - 0.1","","","Panasonic","ERJ-3EKF2202V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","22K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","22K","Datasheet","http://datasheet.octopart.com/ERJ3EKF2202V-Panasonic-datasheet-13266541.pdf","","APE001","03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF2202V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("33","33.2K - �1% - 0603","RES","APE000001-033","1","33","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 33.2K - �1% - 0.1","","","Panasonic","ERJ-3EKF3322V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","33.2K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","33.2K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF3322V-Panasonic-datasheet-13266541.pdf","","APE001","03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF3322V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("34","47K - �1% - 0603","RES","APE000001-034","1","34","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 47K - �1% - 0.1","","","Panasonic","ERJ-3EKF4702V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","47K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","47K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF4702V-Panasonic-datasheet-5313456.pdf","","APE001","Oct. 2008","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF4702V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("35","56.2K - �1% - 0603","RES","APE000001-035","1","35","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 56.2K - �1% - 0.1","","","Panasonic","ERJ-3EKF5622V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","56.2K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","56.2K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF5622V-Panasonic-datasheet-13266541.pdf","","APE001","03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF5622V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("36","68.1K - �1% - 0603","RES","APE000001-036","1","36","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 68.1K - �1% - 0.1","","","Panasonic","ERJ-3EKF6812V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","68.1K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","68.1K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF6812V-Panasonic---ECG-datasheet-13266541.pdf","","APE001","03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF6812V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("37","82.5K - �1% - 0603","RES","APE000001-037","1","37","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 82.5K - �1% - 0.1","","","Panasonic","ERJ-3EKF8252V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","82.5K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","82.5K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF8252V-Panasonic-datasheet-13266541.pdf","","APE001","03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF8252V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("38","100K - �1% - 0603","RES","APE000001-038","1","38","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 100K - �1% - 0.1","","","Panasonic","ERJ-3EKF1003V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","100K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","100K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1003V-Panasonic-datasheet-5313456.pdf","","APE001","Oct. 2008","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1003V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("39","120K - �1% - 0603","RES","APE000001-039","1","39","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 120K - �1% - 0.1","","","Panasonic","ERJ-3EKF1203V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","120K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","120K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1203V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1203V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("40","150K - �1% - 0603","RES","APE000001-040","1","40","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 150K - �1% - 0.1","","","Panasonic","ERJ-3EKF1503V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","150K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","150K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF1503V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1503V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("41","220K - �1% - 0603","RES","APE000001-041","1","41","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 220K - �1% - 0.1","","","Panasonic","ERJ-3EKF2203V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","220K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","220K","Datasheet","http://datasheet.octopart.com/ERJ3EKF2203V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF2203V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("42","332K - �1% - 0603","RES","APE000001-042","1","42","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 332K - �1% - 0.1","","","Panasonic","ERJ-3EKF3323V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","332K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","332K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF3323V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF3323V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("43","470K - �1% - 0603","RES","APE000001-043","1","43","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 470K - �1% - 0.1","","","Panasonic","ERJ-3EKF4703V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","470K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","470K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF4703V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF4703V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("44","562K - �1% - 0603","RES","APE000001-044","1","44","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 562K - �1% - 0.1","","","Panasonic","ERJ-3EKF5623V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","562K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","562K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF5623V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF5623V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("45","681K - �1% - 0603","RES","APE000001-045","1","45","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 681K - �1% - 0.1","","","Panasonic","ERJ-3EKF6813V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","681K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","681K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF6813V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF6813V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("46","825K - �1% - 0603","RES","APE000001-046","1","46","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 825K - �1% - 0.1","","","Panasonic","ERJ-3EKF8253V","","","Taping","","","","","","","","�1%","APE001.PcbLib","R0603","825K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","825K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF8253V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF8253V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("47","1M - �1% - 0603","RES","APE000001-047","1","47","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 1M - �1% - 0.1","","","Panasonic","ERJ-3EKF1004V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","1M","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","1M","Datasheet","http://datasheet.octopart.com/ERJ-3EKF1004V-Panasonic-datasheet-13266541.pdf","","APE001"," 03-AUG-2012","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1004V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("48","10K - �25% - TC33","POT","APE000001-048","1","48","APE001.SchLib","","=Value","Standard","Standard","�250ppm/�C","RES 10K - �25% - 0.1","","","Bourns","TC33X-2-103E","",""," Reel","","","","","","","","�25 %","APE001.PcbLib","TC33","10K","","3.80mm x 3.60mm x 1.20mm","","Single","0.1W","10K","Datasheet","http://datasheet.octopart.com/TC33X-2-103E-Bourns-datasheet-26822215.pdf","","APE001","REV. 04/14","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/TC33X-2-103E-PIC.jpg","partsymbol/POT-SCH.jpg","partfootprint/TC33-PCB.jpg","3dimg/TC33-3D.jpg");
INSERT INTO resistor VALUES("49","330.0 - �1% - 0603","RES","APE000001-049","1","49","APE001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 330.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF3300V","","","Taping","2","","","","","","","�1%","APE001.PcbLib","R0603","330.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","330.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF3300V-Panasonic-datasheet-5313456.pdf","","APE001","Oct. 2008","A","2018-02-01","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF3300V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");





DROP TABLE revision;

CREATE TABLE `revision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RevNo` varchar(45) DEFAULT NULL,
  `manu` varchar(225) DEFAULT NULL,
  `userID` varchar(225) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `notes` varchar(225) DEFAULT NULL,
  `CPN` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE subscription_plan;

CREATE TABLE `subscription_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Subscription_name` varchar(45) DEFAULT NULL,
  `Price` varchar(45) DEFAULT NULL,
  `Parts_package` varchar(45) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `purchase` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE symbols_default;

CREATE TABLE `symbols_default` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `column_val` varchar(200) NOT NULL,
  `category` varchar(200) NOT NULL,
  `col_type` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE transistor;

CREATE TABLE `transistor` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Impedance_Max_Zzt` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Height` varchar(10) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO transistor VALUES("1","FQD13N06LTM","APE000007-001","NFET","APE001.SchLib","","=Value","","Standard","Standard","This N-Channel enhancement mode power MOSFET is produced using Fairchild Semiconductor’s proprietary planar stripe and DMOS technology. This advanced MOSFET technology has been especially tailored to reduce on-state resistance, and to provide superior s","","","","Fairchild Semiconductor","FQD13N06LTM","","","Surface Mount"," Reel and tape","3","Compliant","Active","","","","","","","APE001.PcbLib","TO-252","Datasheet","http://datasheet.octopart.com/FQD13N06LTM-Fairchild-datasheet-8331961.pdf","","","APE001","Unknown","A","2018-02-01","green","","Lead Free","partimg/FQD13N06LTM-PIC.jpg","partsymbol/NFET-SCH.jpg","partfootprint/TO-252-PCB.jpg","3dimg/TO-252-3D.jpg");
INSERT INTO transistor VALUES("2","BSS84","APE000007-002","PFET","APE001.SchLib","","=Value","","Standard","Standard","This P-channel enhancement-mode field-effect transistor is produced using Fairchild’s proprietary, high cell density, DMOS technology. This very high density process minimizes on-state resistance and to provide rugged and reliable performance and fast s","","","","Fairchild Semiconductor","BSS84","","","Surface Mount"," Reel and tape","3","Compliant","Active","","","","","","","APE001.PcbLib","SOT-23","Datasheet","http://datasheet.octopart.com/BSS84-Fairchild-datasheet-62054.pdf","","","APE001","BSS84 Rev B(W)","A","2018-02-01","green","","Lead Free","partimg/BSS84-PIC.jpg","partsymbol/PFET-SCH.jpg","partfootprint/SOT-23-PCB.jpg","3dimg/SOT-23-3D.jpg");
INSERT INTO transistor VALUES("3","MMBT3906","APE000007-003","PNP","APE001.SchLib","","=Value","","Standard","Standard","This device is designed for general purpose amplifier and switching applications at collector currents of 10 µA to 100 mA.","","","","Fairchild Semiconductor","MMBT3906","","","Surface Mount"," Reel and tape","3","Compliant","Active","","","","","","","APE001.PcbLib","SOT-23","Datasheet","http://datasheet.octopart.com/MMBT3906-Fairchild-datasheet-23394.pdf","","","APE001","2N3906/MMBT3906/PZT3906, Rev A","A","2018-02-01","green","","Lead Free","partimg/MMBT3906-PIC.jpg","partsymbol/PNP-SCH.jpg","partfootprint/SOT-23-PCB.jpg","3dimg/SOT-23-3D.jpg");
INSERT INTO transistor VALUES("4","MMBT3904","APE000007-004","NPN","APE001.SchLib","","=Value","","Standard","Standard","This device is designed as a general purpose amplifier and switch. The useful dynamic range extends to 100 mA as a switch and to 100 MHz as an amplifier.","","","","Fairchild Semiconductor","MMBT3904","","","Surface Mount"," Reel and tape","3","Compliant","Active","","","","","","","APE001.PcbLib","SOT-23","Datasheet","http://datasheet.octopart.com/MMBT3904-Fairchild-datasheet-5999.pdf","","","APE001","2N3904/MMBT3904/PZT3904, Rev A","A","2018-02-01","green","","Lead Free","partimg/MMBT3904-PIC.jpg","partsymbol/NPN-SCH.jpg","partfootprint/SOT-23-PCB.jpg","3dimg/SOT-23-3D.jpg");





DROP TABLE uploads;

CREATE TABLE `uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(225) DEFAULT NULL,
  `sch_name` varchar(225) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE vendor_list;

CREATE TABLE `vendor_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `com_id` varchar(45) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE visible;

CREATE TABLE `visible` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `column1` varchar(45) DEFAULT NULL,
  `column2` varchar(45) DEFAULT NULL,
  `column3` varchar(45) DEFAULT NULL,
  `column4` varchar(45) DEFAULT NULL,
  `column5` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO visible VALUES("1","Part_Number","CPN","Description","Manufacturer","Manufacturer_Part_Number");
INSERT INTO visible VALUES("2","Part_Number","CPN","Manufacturer","Value","Manufacturer_Part_Number");
INSERT INTO visible VALUES("3","Part_Number","CPN","Description","Manufacturer","Manufacturer_Part_Number");
INSERT INTO visible VALUES("4","Part_Number","CPN","Description","Manufacturer","Manufacturer_Part_Number");
INSERT INTO visible VALUES("5","Part_Number","CPN","Description","Manufacturer","Manufacturer_Part_Number");
INSERT INTO visible VALUES("6","Part_Number","CPN","Description","Manufacturer","Manufacturer_Part_Number");
INSERT INTO visible VALUES("7","Part_Number","CPN","Description","Manufacturer","Manufacturer_Part_Number");
INSERT INTO visible VALUES("8","Part_Number","CPN","Description","Manufacturer","Manufacturer_Part_Number");
INSERT INTO visible VALUES("9","Part_Number","CPN","Description","Manufacturer","Manufacturer_Part_Number");



