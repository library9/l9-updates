create database CUS001;

DROP TABLE bom;

CREATE TABLE `bom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bom_name` varchar(45) DEFAULT NULL,
  `reference_designator` varchar(225) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `manufacture` varchar(225) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `mpn` varchar(225) DEFAULT NULL,
  `price` varchar(225) DEFAULT NULL,
  `priceat` varchar(225) DEFAULT NULL,
  `vendor` varchar(225) DEFAULT NULL,
  `sku` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE bom_filename;

CREATE TABLE `bom_filename` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `bom_name` varchar(45) DEFAULT NULL,
  `filename` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE capacitor;

CREATE TABLE `capacitor` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Library_Reference` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(45) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Manufacturer Part Number` varchar(255) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `Part Number` varchar(225) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `rohs` varchar(225) DEFAULT NULL,
  `Life_cycle` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  `custom1` varchar(255) DEFAULT NULL,
  `custom2` varchar(255) DEFAULT NULL,
  `custom3` varchar(255) DEFAULT NULL,
  `custom4` varchar(45) DEFAULT NULL,
  `custom5` varchar(45) DEFAULT NULL,
  `custom6` varchar(255) DEFAULT NULL,
  `custom7` varchar(255) DEFAULT NULL,
  `custom8` varchar(255) DEFAULT NULL,
  `custom9` varchar(255) DEFAULT NULL,
  `custom10` varchar(255) DEFAULT NULL,
  `custom11` varchar(255) DEFAULT NULL,
  `custom12` varchar(255) DEFAULT NULL,
  `custom13` varchar(45) DEFAULT NULL,
  `custom14` varchar(255) DEFAULT NULL,
  `custom15` varchar(255) DEFAULT NULL,
  `custom16` varchar(45) DEFAULT NULL,
  `custom17` varchar(255) DEFAULT NULL,
  `custom18` varchar(255) DEFAULT NULL,
  `custom19` varchar(255) DEFAULT NULL,
  `custom20` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=543 DEFAULT CHARSET=latin1;

INSERT INTO capacitor VALUES("522","3.873 - ","ESC01-001","","CUS001SchLib","","=Value","Standard","Standard","CAP 3.873 - ","","","","Vishay Dale","RS02B5R000FE70","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/RS02B5R000FE70-Vishay-datasheet-13315611.pdf","","green","","2013-01-22T03:35:17Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("523","50.0 - C0603","ESC01-002","","CUS001SchLib","","=Value","Standard","Standard","CAP 50.0 - ","","","","Yageo","RC0603FR-0763K4L","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/RC0603FR-0763K4L-Yageo-datasheet-73766026.pdf","","green","","2014-07-14T06:00:28Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("524","50.0 - C0402","ESC01-003","","CUS001SchLib","","=Value","Standard","Standard","CAP 50.0 - ","","","","Yageo","RC0402JR-0768RL","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/RC0402JR-0768RL-Yageo-datasheet-10408619.pdf","","green","","2011-01-06T01:55:34Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("525","50.0 - C0603","ESC01-004","","CUS001SchLib","","=Value","Standard","Standard","CAP 50.0 - ","","","","Yageo","RC0603JR-0768RL","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/RC0603JR-0768RL-Yageo-datasheet-8328172.pdf","","green","","2009-04-24T02:31:26Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("526","50.0 - C0603","ESC01-005","","CUS001SchLib","","=Value","Standard","Standard","CAP 50.0 - ","","","","Yageo","RC0603FR-076K34L","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/RC0603FR-076K34L-Yageo-datasheet-73766026.pdf","","green","","2014-07-14T06:00:28Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("527","50.0 - C0603","ESC01-006","","CUS001SchLib","","=Value","Standard","Standard","CAP 50.0 - ","","","","Yageo","RC0603FR-076K65L","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/RC0603FR-076K65L-Yageo-datasheet-73766026.pdf","","green","","2014-07-14T06:00:28Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("528","200.0 - C1206","ESC01-007","","CUS001SchLib","","=Value","Standard","Standard","CAP 200.0 - ","","","","Vishay Dale","CRCW12066R80JNEA","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/CRCW12066R80JNEA-Vishay-datasheet-33013972.pdf","","green","","2014-05-29T02:31:17Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("529","50.0 - C0603","ESC01-008","","CUS001SchLib","","=Value","Standard","Standard","CAP 50.0 - ","","","","Yageo","RC0603FR-07768RL","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/RC0603FR-07768RL-Yageo-datasheet-73766026.pdf","","green","","2014-07-14T06:00:28Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("530","75.0 - C0603","ESC01-009","","CUS001SchLib","","=Value","Standard","Standard","CAP 75.0 - ","","","","Panasonic","ERJ-3EKF7321V","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/ERJ-3EKF7321V-Panasonic-datasheet-82288794.pdf","","green","","2017-05-10T02:26:57Z","A","2014-12-08","Compliant","Not Listed by Manufacturer","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("531","150.0 - C0805","ESC01-010","","CUS001SchLib","","=Value","Standard","Standard","CAP 150.0 - ","","","","Vishay Dale","CRCW08057K87FKEA","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/CRCW08057K87FKEA-Vishay-datasheet-66077789.pdf","","green","","2016-07-26T02:32:39Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("532","75.0 - C0603","ESC01-011","","CUS001SchLib","","=Value","Standard","Standard","CAP 75.0 - ","","","","Yageo","RC0603FR-07820RL","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/RC0603FR-07820RL-Yageo-datasheet-8328172.pdf","","green","","2009-04-24T02:31:26Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("533","200.0 - C1206","ESC01-012","","CUS001SchLib","","=Value","Standard","Standard","CAP 200.0 - ","","","","Yageo","RC1206JR-0791RL","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/RC1206JR-0791RL-Yageo-datasheet-8359522.pdf","","green","","2009-07-17T05:32:36Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("534","150.0 - C0805","ESC01-013","","CUS001SchLib","","=Value","Standard","Standard","CAP 150.0 - ","","","","Vishay","CRCW0805953RFKEA","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/CRCW0805953RFKEA-Vishay-datasheet-66077789.pdf","","green","","2016-07-26T02:32:39Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("535","100.0 - ","ESC01-014","","CUS001SchLib","","=Value","Standard","Standard","CAP 100.0 - ","","","","Bourns","3306F-1-103","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/3306F-1-103-Bourns-datasheet-8877134.pdf","","green","","2010-11-22T19:26:26Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("536","PV36X103C01B00","ESC01-015","","CUS001SchLib","","=Value","Standard","Standard","","","","","Bourns","PV36X103C01B00","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/3306F-1-103-Bourns-datasheet-8877134.pdf","","green","","2010-11-22T19:26:26Z","A","2014-12-08","","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("537","500.0 - ","ESC01-016","","CUS001SchLib","","=Value","Standard","Standard","CAP 500.0 - ","","","","Bourns","3214G-1-103E","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/3214G-1-103E-Bourns-datasheet-68298440.pdf","","green","","2016-12-14T18:17:18Z","A","2014-12-08","Non-Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("538","64Y-102","ESC01-017","","CUS001SchLib","","=Value","Standard","Standard","","","","","Vishay Dale","64Y-102","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/64Y-102-Vishay-datasheet-12541187.pdf","","green","","2012-11-08T03:42:38Z","A","2014-12-08","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("539","PV36Y102C01B00","ESC01-018","","CUS001SchLib","","=Value","Standard","Standard","","","","","Bourns","PV36Y102C01B00","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/3214G-1-103E-Bourns-datasheet-68298440.pdf","","green","","2016-12-14T18:17:18Z","A","2014-12-08","","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("540","PV36X203C01B00","ESC01-019","","CUS001SchLib","","=Value","Standard","Standard","","","","","Bourns","PV36X203C01B00","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/RC0603FR-07768RL-Yageo-datasheet-73766026.pdf","","green","","2014-07-14T06:00:28Z","A","2014-12-08","","","","","","","","","","","","","","","","","","","","","","","","","","");
INSERT INTO capacitor VALUES("541","PV36X202C01B00","ESC01-020","","CUS001SchLib","","=Value","Standard","Standard","","","","","Bourns","PV36X202C01B00","","","","","","","","","","","CUS001PcbLib","","Datasheet","http://datasheet.octopart.com/64Y-102-Vishay-datasheet-12541187.pdf","","green","","2012-11-08T03:42:38Z","A","2014-12-08","","","","","","","","","","","","","","","","","","","","","","","","","","");





DROP TABLE categories;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO categories VALUES("5","capacitor");





DROP TABLE custom_alias;

CREATE TABLE `custom_alias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(45) DEFAULT NULL,
  `cat_name` varchar(45) DEFAULT NULL,
  `alias_for_field` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;






DROP TABLE custom_pn;

CREATE TABLE `custom_pn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `postfix` varchar(45) DEFAULT NULL,
  `prefix` varchar(45) DEFAULT NULL,
  `CPN` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO custom_pn VALUES("5","capacitor","001","ESC01","ESC01-001");





DROP TABLE default_values;

CREATE TABLE `default_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value_type` varchar(200) NOT NULL,
  `category` varchar(200) NOT NULL,
  `default_val` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE footprint_autofill;

CREATE TABLE `footprint_autofill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autofill_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO footprint_autofill VALUES("1","manual");





DROP TABLE new_part;

CREATE TABLE `new_part` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(225) DEFAULT NULL,
  `CPN` varchar(225) DEFAULT NULL,
  `Description` varchar(225) DEFAULT NULL,
  `Manufacturer` varchar(225) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `table_name` varchar(45) DEFAULT NULL,
  `Library_Ref` varchar(225) DEFAULT NULL,
  `Library_Path` varchar(45) DEFAULT NULL,
  `Category` varchar(45) DEFAULT NULL,
  `Comment` varchar(45) DEFAULT NULL,
  `Component_Kind` varchar(45) DEFAULT NULL,
  `Component_Type` varchar(45) DEFAULT NULL,
  `Designator` varchar(45) DEFAULT NULL,
  `Footprint` varchar(45) DEFAULT NULL,
  `Library_Reference` varchar(45) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Packaging` varchar(45) DEFAULT NULL,
  `Pin_Count` varchar(45) DEFAULT NULL,
  `Signal_Integrity` varchar(45) DEFAULT NULL,
  `Simulation` varchar(45) DEFAULT NULL,
  `Supplier` varchar(45) DEFAULT NULL,
  `Supplier_Part_Number` varchar(45) DEFAULT NULL,
  `Footprint_Path` varchar(45) DEFAULT NULL,
  `Footprint_Ref` varchar(45) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(45) DEFAULT NULL,
  `Supplier1` varchar(45) DEFAULT NULL,
  `ComponentLink1Description` varchar(45) DEFAULT NULL,
  `ComponentLink1URL` varchar(225) DEFAULT NULL,
  `ITEM` int(11) DEFAULT NULL,
  `PrePN` varchar(45) DEFAULT NULL,
  `PostPN` varchar(45) DEFAULT NULL,
  `TEMPCO` varchar(45) DEFAULT NULL,
  `Tolerance` varchar(45) DEFAULT NULL,
  `Value` varchar(45) DEFAULT NULL,
  `Height` varchar(45) DEFAULT NULL,
  `Size_Dimension` varchar(45) DEFAULT NULL,
  `Composition` varchar(45) DEFAULT NULL,
  `Number_of_Terminations` varchar(45) DEFAULT NULL,
  `Power_Watts` varchar(45) DEFAULT NULL,
  `Resistance_Ohms` varchar(45) DEFAULT NULL,
  `Operating_Temperature` varchar(45) DEFAULT NULL,
  `Package_Case` varchar(45) DEFAULT NULL,
  `Capacitance` varchar(45) DEFAULT NULL,
  `Thickness_Max` varchar(45) DEFAULT NULL,
  `Voltage_Rated` varchar(45) DEFAULT NULL,
  `Lead_Spacing` varchar(45) DEFAULT NULL,
  `RoHS` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(45) DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL,
  `Supplier_Device_Package` varchar(45) DEFAULT NULL,
  `Minimum_Orde_Quantity` varchar(45) DEFAULT NULL,
  `Reeling` varchar(45) DEFAULT NULL,
  `Product_Type` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `Reverse_Recovery_Time_trr` varchar(45) DEFAULT NULL,
  `ESR_Equivalent_Series_Resistance` varchar(45) DEFAULT NULL,
  `table_names` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE parts_log;

CREATE TABLE `parts_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_name` varchar(225) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `user` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE revision;

CREATE TABLE `revision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RevNo` varchar(45) DEFAULT NULL,
  `manu` varchar(225) DEFAULT NULL,
  `userID` varchar(225) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `notes` varchar(225) DEFAULT NULL,
  `CPN` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=latin1;

INSERT INTO revision VALUES("1","A","RS02B5R000FE70","custom","2017-10-11 08:04:15","Orignated","");
INSERT INTO revision VALUES("2","A","C1608X7R1H103K","custom","2017-10-11 08:06:28","Orignated","");
INSERT INTO revision VALUES("3","A","RC0603FR-076K34L","custom","2017-10-11 08:10:46","Orignated","");
INSERT INTO revision VALUES("4","A","CRCW08057K87FKEA","custom","2017-10-11 08:10:46","Orignated","");
INSERT INTO revision VALUES("5","A","3306F-1-103","custom","2017-10-11 08:25:40","Orignated","");
INSERT INTO revision VALUES("6","A","RC1206JR-0791RL","custom","2017-10-11 08:25:41","Orignated","");
INSERT INTO revision VALUES("7","A","RC0402JR-0768RL","custom","2017-10-11 08:25:42","Orignated","");
INSERT INTO revision VALUES("8","A","64Y-102","custom","2017-10-11 08:25:43","Orignated","");
INSERT INTO revision VALUES("9","A","RS02B5R000FE70","custom","2017-10-11 08:25:43","Orignated","");
INSERT INTO revision VALUES("10","A","RC0603FR-07768RL","custom","2017-10-11 08:25:44","Orignated","");
INSERT INTO revision VALUES("11","A","RC0603FR-076K65L","custom","2017-10-11 08:25:45","Orignated","");
INSERT INTO revision VALUES("12","A","RC0603FR-0763K4L","custom","2017-10-11 08:25:45","Orignated","");
INSERT INTO revision VALUES("13","A","ERJ-3EKF7321V","custom","2017-10-11 08:25:46","Orignated","");
INSERT INTO revision VALUES("14","A","CRCW0805953RFKEA","custom","2017-10-11 08:25:47","Orignated","");
INSERT INTO revision VALUES("15","A","CRCW08057K87FKEA","custom","2017-10-11 08:25:48","Orignated","");
INSERT INTO revision VALUES("16","A","PV36Y102C01B00","custom","2017-10-11 08:25:48","Orignated","");
INSERT INTO revision VALUES("17","A","CRCW12066R80JNEA","custom","2017-10-11 08:25:49","Orignated","");
INSERT INTO revision VALUES("18","A","PV36X202C01B00","custom","2017-10-11 08:25:50","Orignated","");
INSERT INTO revision VALUES("19","A","RC0603JR-0768RL","custom","2017-10-11 08:25:50","Orignated","");
INSERT INTO revision VALUES("20","A","PV36X203C01B00","custom","2017-10-11 08:25:51","Orignated","");
INSERT INTO revision VALUES("21","A","3214G-1-103E","custom","2017-10-11 08:25:51","Orignated","");
INSERT INTO revision VALUES("22","A","RC0603FR-07820RL","custom","2017-10-11 08:25:52","Orignated","");
INSERT INTO revision VALUES("23","A","PV36X103C01B00","custom","2017-10-11 08:25:53","Orignated","");
INSERT INTO revision VALUES("24","A","RC0603FR-076K34L","custom","2017-10-11 08:25:54","Orignated","");
INSERT INTO revision VALUES("25","A","RS02B5R000FE70","custom","2017-10-11 08:27:56","Orignated","");
INSERT INTO revision VALUES("26","A","ERJ-3EKF7321V","custom","2017-10-11 08:28:48","Orignated","");
INSERT INTO revision VALUES("27","A","RC0603JR-0768RL","custom","2017-10-11 08:28:49","Orignated","");
INSERT INTO revision VALUES("28","A","RS02B5R000FE70","custom","2017-10-11 08:28:50","Orignated","");
INSERT INTO revision VALUES("29","A","64Y-102","custom","2017-10-11 08:28:51","Orignated","");
INSERT INTO revision VALUES("30","A","CRCW08057K87FKEA","custom","2017-10-11 08:28:51","Orignated","");
INSERT INTO revision VALUES("31","A","RC1206JR-0791RL","custom","2017-10-11 08:28:52","Orignated","");
INSERT INTO revision VALUES("32","A","RC0603FR-07768RL","custom","2017-10-11 08:28:52","Orignated","");
INSERT INTO revision VALUES("33","A","RC0402JR-0768RL","custom","2017-10-11 08:28:53","Orignated","");
INSERT INTO revision VALUES("34","A","RC0603FR-076K65L","custom","2017-10-11 08:28:54","Orignated","");
INSERT INTO revision VALUES("35","A","RC0603FR-076K34L","custom","2017-10-11 08:28:54","Orignated","");
INSERT INTO revision VALUES("36","A","PV36X103C01B00","custom","2017-10-11 08:28:55","Orignated","");
INSERT INTO revision VALUES("37","A","3306F-1-103","custom","2017-10-11 08:28:56","Orignated","");
INSERT INTO revision VALUES("38","A","PV36X203C01B00","custom","2017-10-11 08:28:56","Orignated","");
INSERT INTO revision VALUES("39","A","RC0603FR-07820RL","custom","2017-10-11 08:28:57","Orignated","");
INSERT INTO revision VALUES("40","A","PV36Y102C01B00","custom","2017-10-11 08:28:58","Orignated","");
INSERT INTO revision VALUES("41","A","CRCW12066R80JNEA","custom","2017-10-11 08:28:58","Orignated","");
INSERT INTO revision VALUES("42","A","PV36X202C01B00","custom","2017-10-11 08:28:59","Orignated","");
INSERT INTO revision VALUES("43","A","3214G-1-103E","custom","2017-10-11 08:28:59","Orignated","");
INSERT INTO revision VALUES("44","A","CRCW0805953RFKEA","custom","2017-10-11 08:29:00","Orignated","");
INSERT INTO revision VALUES("45","A","RC0603FR-0763K4L","custom","2017-10-11 08:29:01","Orignated","");
INSERT INTO revision VALUES("46","A","C1608X7R1H103K","custom","2017-10-11 08:30:57","Orignated","");
INSERT INTO revision VALUES("47","A","C1206C106K4RACTU","custom","2017-10-11 08:30:57","Orignated","");
INSERT INTO revision VALUES("48","A","CC0402JRNPO9BN101","custom","2017-10-11 08:30:58","Orignated","");
INSERT INTO revision VALUES("49","A","C1608X7R1C105K","custom","2017-10-11 08:30:59","Orignated","");
INSERT INTO revision VALUES("50","A","C0603C121K5GACTU","custom","2017-10-11 08:31:00","Orignated","");
INSERT INTO revision VALUES("51","A","GCM188R72A102KA37D","custom","2017-10-11 08:31:00","Orignated","");
INSERT INTO revision VALUES("52","A","04026D105KAT2A","custom","2017-10-11 08:31:02","Orignated","");
INSERT INTO revision VALUES("53","A","0402YD104KAT2A","custom","2017-10-11 08:31:02","Orignated","");
INSERT INTO revision VALUES("54","A","GRM219R71E224KA01D","custom","2017-10-11 08:31:03","Orignated","");
INSERT INTO revision VALUES("55","A","06035C104KAT2A","custom","2017-10-11 08:31:03","Orignated","");
INSERT INTO revision VALUES("56","A","UMK325BJ106MM-T","custom","2017-10-11 08:31:04","Orignated","");
INSERT INTO revision VALUES("57","A","0402YC103KAT2A","custom","2017-10-11 08:31:05","Orignated","");
INSERT INTO revision VALUES("58","A","C3225X5R0J226M/2.00","custom","2017-10-11 08:31:05","Orignated","");
INSERT INTO revision VALUES("59","A","04025A181JAT2A","custom","2017-10-11 08:31:06","Orignated","");
INSERT INTO revision VALUES("60","A","C0805C104K5RACTU","custom","2017-10-11 08:31:07","Orignated","");
INSERT INTO revision VALUES("61","A","0402YC102KAT2A","custom","2017-10-11 08:31:08","Orignated","");
INSERT INTO revision VALUES("62","A","C1608X7R1C224K","custom","2017-10-11 08:31:08","Orignated","");
INSERT INTO revision VALUES("63","A","C1206C106K8PACTU","custom","2017-10-11 08:31:09","Orignated","");
INSERT INTO revision VALUES("64","A","0805YC474KAT2A","custom","2017-10-11 08:31:10","Orignated","");
INSERT INTO revision VALUES("65","A","CC0805KKX7R7BB105","custom","2017-10-11 08:31:11","Orignated","");
INSERT INTO revision VALUES("66","A","CC0603JRNPO9BN181","custom","2017-10-11 08:31:11","Orignated","");
INSERT INTO revision VALUES("67","A","C0603C151K5GACTU","custom","2017-10-11 08:31:12","Orignated","");
INSERT INTO revision VALUES("68","A","06035A101FAT2A","custom","2017-10-11 08:31:13","Orignated","");
INSERT INTO revision VALUES("69","A","C1206C181K5GACTU","custom","2017-10-11 08:31:14","Orignated","");
INSERT INTO revision VALUES("70","A","C3216C0G1H104J160AA","custom","2017-10-11 08:31:15","Orignated","");
INSERT INTO revision VALUES("71","A","08055C182KAT2A","custom","2017-10-11 08:31:16","Orignated","");
INSERT INTO revision VALUES("72","A","0805ZD106KAT2A","custom","2017-10-11 08:31:16","Orignated","");
INSERT INTO revision VALUES("73","A","06031A180GAT2A","custom","2017-10-11 08:31:17","Orignated","");
INSERT INTO revision VALUES("74","A","GRM1885C1H100FA01J","custom","2017-10-11 08:31:18","Orignated","");
INSERT INTO revision VALUES("75","A","C0805C334K3RACTU","custom","2017-10-11 08:31:19","Orignated","");
INSERT INTO revision VALUES("76","A","ERJ-3EKF7321V","custom","2017-10-12 04:55:49","Orignated","");
INSERT INTO revision VALUES("77","A","RC1206JR-0791RL","custom","2017-10-12 04:55:51","Orignated","");
INSERT INTO revision VALUES("78","A","CRCW12066R80JNEA","custom","2017-10-12 04:55:53","Orignated","");
INSERT INTO revision VALUES("79","A","RC0603FR-07768RL","custom","2017-10-12 04:55:55","Orignated","");
INSERT INTO revision VALUES("80","A","CRCW08057K87FKEA","custom","2017-10-12 04:55:56","Orignated","");
INSERT INTO revision VALUES("81","A","RC0402JR-0768RL","custom","2017-10-12 04:55:58","Orignated","");
INSERT INTO revision VALUES("82","A","3214G-1-103E","custom","2017-10-12 04:56:00","Orignated","");
INSERT INTO revision VALUES("83","A","PV36X202C01B00","custom","2017-10-12 04:56:01","Orignated","");
INSERT INTO revision VALUES("84","A","PV36Y102C01B00","custom","2017-10-12 04:56:03","Orignated","");
INSERT INTO revision VALUES("85","A","PV36X203C01B00","custom","2017-10-12 04:56:04","Orignated","");
INSERT INTO revision VALUES("86","A","64Y-102","custom","2017-10-12 04:56:06","Orignated","");
INSERT INTO revision VALUES("87","A","RC0603FR-0763K4L","custom","2017-10-12 04:56:08","Orignated","");
INSERT INTO revision VALUES("88","A","3306F-1-103","custom","2017-10-12 04:56:09","Orignated","");
INSERT INTO revision VALUES("89","A","RC0603FR-076K34L","custom","2017-10-12 04:56:11","Orignated","");
INSERT INTO revision VALUES("90","A","RC0603FR-076K65L","custom","2017-10-12 04:56:12","Orignated","");
INSERT INTO revision VALUES("91","A","RC0603JR-0768RL","custom","2017-10-12 04:56:13","Orignated","");
INSERT INTO revision VALUES("92","A","PV36X103C01B00","custom","2017-10-12 04:56:15","Orignated","");
INSERT INTO revision VALUES("93","A","RC0603FR-07820RL","custom","2017-10-12 04:56:16","Orignated","");
INSERT INTO revision VALUES("94","A","RS02B5R000FE70","custom","2017-10-12 04:56:18","Orignated","");
INSERT INTO revision VALUES("95","A","CRCW0805953RFKEA","custom","2017-10-12 04:56:19","Orignated","");
INSERT INTO revision VALUES("96","A","PV36X202C01B00","custom","2017-10-12 04:57:52","Orignated","");
INSERT INTO revision VALUES("97","A","64Y-102","custom","2017-10-12 04:59:41","Orignated","");
INSERT INTO revision VALUES("98","A","PV36X202C01B00","custom","2017-10-12 04:59:43","Orignated","");
INSERT INTO revision VALUES("99","A","CRCW12066R80JNEA","custom","2017-10-12 04:59:44","Orignated","");
INSERT INTO revision VALUES("100","A","RC0603JR-0768RL","custom","2017-10-12 04:59:46","Orignated","");
INSERT INTO revision VALUES("101","A","RC0402JR-0768RL","custom","2017-10-12 04:59:47","Orignated","");
INSERT INTO revision VALUES("102","A","ERJ-3EKF7321V","custom","2017-10-12 04:59:48","Orignated","");
INSERT INTO revision VALUES("103","A","CRCW08057K87FKEA","custom","2017-10-12 04:59:49","Orignated","");
INSERT INTO revision VALUES("104","A","3306F-1-103","custom","2017-10-12 04:59:51","Orignated","");
INSERT INTO revision VALUES("105","A","PV36X103C01B00","custom","2017-10-12 04:59:52","Orignated","");
INSERT INTO revision VALUES("106","A","RC0603FR-07820RL","custom","2017-10-12 04:59:54","Orignated","");
INSERT INTO revision VALUES("107","A","RC1206JR-0791RL","custom","2017-10-12 04:59:55","Orignated","");
INSERT INTO revision VALUES("108","A","3214G-1-103E","custom","2017-10-12 04:59:56","Orignated","");
INSERT INTO revision VALUES("109","A","PV36Y102C01B00","custom","2017-10-12 04:59:58","Orignated","");
INSERT INTO revision VALUES("110","A","RS02B5R000FE70","custom","2017-10-12 04:59:59","Orignated","");
INSERT INTO revision VALUES("111","A","CRCW0805953RFKEA","custom","2017-10-12 05:00:01","Orignated","");
INSERT INTO revision VALUES("112","A","RC0603FR-07768RL","custom","2017-10-12 05:00:02","Orignated","");
INSERT INTO revision VALUES("113","A","PV36X203C01B00","custom","2017-10-12 05:00:03","Orignated","");
INSERT INTO revision VALUES("114","A","RC0603FR-076K34L","custom","2017-10-12 05:00:05","Orignated","");
INSERT INTO revision VALUES("115","A","RC0603FR-0763K4L","custom","2017-10-12 05:00:06","Orignated","");
INSERT INTO revision VALUES("116","A","RC0603FR-076K65L","custom","2017-10-12 05:00:07","Orignated","");





DROP TABLE subscription_plan;

CREATE TABLE `subscription_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Subscription_name` varchar(45) DEFAULT NULL,
  `Price` varchar(45) DEFAULT NULL,
  `Parts_package` varchar(45) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `purchase` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE symbols_default;

CREATE TABLE `symbols_default` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `column_val` varchar(200) NOT NULL,
  `category` varchar(200) NOT NULL,
  `col_type` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE uploads;

CREATE TABLE `uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(225) DEFAULT NULL,
  `sch_name` varchar(225) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE vendor_list;

CREATE TABLE `vendor_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `com_id` varchar(45) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE visible;

CREATE TABLE `visible` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `column1` varchar(45) DEFAULT NULL,
  `column2` varchar(45) DEFAULT NULL,
  `column3` varchar(45) DEFAULT NULL,
  `column4` varchar(45) DEFAULT NULL,
  `column5` varchar(45) DEFAULT NULL,
  `cat_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO visible VALUES("1","Part_Number","CPN","Manufacturer","Description","Manufacturer_Part_Number","all");
INSERT INTO visible VALUES("2","Part_Number","CPN","Description","Manufacturer","Manufacturer_Part_Number","capacitor");



