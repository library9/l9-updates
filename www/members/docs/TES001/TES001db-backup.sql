create database TES001;

DROP TABLE bom;

CREATE TABLE `bom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bom_name` varchar(45) DEFAULT NULL,
  `reference_designator` varchar(225) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `manufacture` varchar(225) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `mpn` varchar(225) DEFAULT NULL,
  `price` varchar(225) DEFAULT NULL,
  `priceat` varchar(225) DEFAULT NULL,
  `vendor` varchar(225) DEFAULT NULL,
  `sku` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE bom_filename;

CREATE TABLE `bom_filename` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `bom_name` varchar(45) DEFAULT NULL,
  `filename` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE categories;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;






DROP TABLE custom_alias;

CREATE TABLE `custom_alias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(45) DEFAULT NULL,
  `cat_name` varchar(45) DEFAULT NULL,
  `alias_for_field` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;






DROP TABLE custom_pn;

CREATE TABLE `custom_pn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `postfix` varchar(45) DEFAULT NULL,
  `prefix` varchar(45) DEFAULT NULL,
  `CPN` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;






DROP TABLE default_values;

CREATE TABLE `default_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value_type` varchar(200) NOT NULL,
  `category` varchar(200) NOT NULL,
  `default_val` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE footprint_autofill;

CREATE TABLE `footprint_autofill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autofill_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO footprint_autofill VALUES("1","manual");





DROP TABLE new_part;

CREATE TABLE `new_part` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(225) DEFAULT NULL,
  `CPN` varchar(225) DEFAULT NULL,
  `Description` varchar(225) DEFAULT NULL,
  `Manufacturer` varchar(225) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `table_name` varchar(45) DEFAULT NULL,
  `Library_Ref` varchar(225) DEFAULT NULL,
  `Library_Path` varchar(45) DEFAULT NULL,
  `Category` varchar(45) DEFAULT NULL,
  `Comment` varchar(45) DEFAULT NULL,
  `Component_Kind` varchar(45) DEFAULT NULL,
  `Component_Type` varchar(45) DEFAULT NULL,
  `Designator` varchar(45) DEFAULT NULL,
  `Footprint` varchar(45) DEFAULT NULL,
  `Library_Reference` varchar(45) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Packaging` varchar(45) DEFAULT NULL,
  `Pin_Count` varchar(45) DEFAULT NULL,
  `Signal_Integrity` varchar(45) DEFAULT NULL,
  `Simulation` varchar(45) DEFAULT NULL,
  `Supplier` varchar(45) DEFAULT NULL,
  `Supplier_Part_Number` varchar(45) DEFAULT NULL,
  `Footprint_Path` varchar(45) DEFAULT NULL,
  `Footprint_Ref` varchar(45) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(45) DEFAULT NULL,
  `Supplier1` varchar(45) DEFAULT NULL,
  `ComponentLink1Description` varchar(45) DEFAULT NULL,
  `ComponentLink1URL` varchar(225) DEFAULT NULL,
  `ITEM` int(11) DEFAULT NULL,
  `PrePN` varchar(45) DEFAULT NULL,
  `PostPN` varchar(45) DEFAULT NULL,
  `TEMPCO` varchar(45) DEFAULT NULL,
  `Tolerance` varchar(45) DEFAULT NULL,
  `Value` varchar(45) DEFAULT NULL,
  `Height` varchar(45) DEFAULT NULL,
  `Size_Dimension` varchar(45) DEFAULT NULL,
  `Composition` varchar(45) DEFAULT NULL,
  `Number_of_Terminations` varchar(45) DEFAULT NULL,
  `Power_Watts` varchar(45) DEFAULT NULL,
  `Resistance_Ohms` varchar(45) DEFAULT NULL,
  `Operating_Temperature` varchar(45) DEFAULT NULL,
  `Package_Case` varchar(45) DEFAULT NULL,
  `Capacitance` varchar(45) DEFAULT NULL,
  `Thickness_Max` varchar(45) DEFAULT NULL,
  `Voltage_Rated` varchar(45) DEFAULT NULL,
  `Lead_Spacing` varchar(45) DEFAULT NULL,
  `RoHS` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(45) DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL,
  `Supplier_Device_Package` varchar(45) DEFAULT NULL,
  `Minimum_Orde_Quantity` varchar(45) DEFAULT NULL,
  `Reeling` varchar(45) DEFAULT NULL,
  `Product_Type` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `Reverse_Recovery_Time_trr` varchar(45) DEFAULT NULL,
  `ESR_Equivalent_Series_Resistance` varchar(45) DEFAULT NULL,
  `table_names` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE parts_log;

CREATE TABLE `parts_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_name` varchar(225) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `user` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE revision;

CREATE TABLE `revision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RevNo` varchar(45) DEFAULT NULL,
  `manu` varchar(225) DEFAULT NULL,
  `userID` varchar(225) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `notes` varchar(225) DEFAULT NULL,
  `CPN` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE subscription_plan;

CREATE TABLE `subscription_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Subscription_name` varchar(45) DEFAULT NULL,
  `Price` varchar(45) DEFAULT NULL,
  `Parts_package` varchar(45) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `purchase` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE symbols_default;

CREATE TABLE `symbols_default` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `column_val` varchar(200) NOT NULL,
  `category` varchar(200) NOT NULL,
  `col_type` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE uploads;

CREATE TABLE `uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(225) DEFAULT NULL,
  `sch_name` varchar(225) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE vendor_list;

CREATE TABLE `vendor_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `com_id` varchar(45) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE visible;

CREATE TABLE `visible` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `column1` varchar(45) DEFAULT NULL,
  `column2` varchar(45) DEFAULT NULL,
  `column3` varchar(45) DEFAULT NULL,
  `column4` varchar(45) DEFAULT NULL,
  `column5` varchar(45) DEFAULT NULL,
  `cat_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO visible VALUES("1","Part_Number","CPN","Manufacturer","Description","Manufacturer_Part_Number","all");



