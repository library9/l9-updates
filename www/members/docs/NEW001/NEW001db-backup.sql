create database NEW001;

DROP TABLE bom;

CREATE TABLE `bom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bom_name` varchar(45) DEFAULT NULL,
  `reference_designator` varchar(225) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `manufacture` varchar(225) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `mpn` varchar(225) DEFAULT NULL,
  `price` varchar(225) DEFAULT NULL,
  `priceat` varchar(225) DEFAULT NULL,
  `vendor` varchar(225) DEFAULT NULL,
  `sku` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

INSERT INTO bom VALUES("1","NewBom1","C1","","","1","GRM188R71H104KA93D","0.00","","","");
INSERT INTO bom VALUES("2","NewBom1","C4, C5","","","2","C1608C0G1H080C080AA","0.00","","","");
INSERT INTO bom VALUES("3","NewBom1","RN2, RN3","","","2","ERJ-3GEYJ220V","0.00","","","");
INSERT INTO bom VALUES("4","NewBom1","R1, R2, R3, R4","","","4","ERJ-3EKF22R0V","0.00","","","");
INSERT INTO bom VALUES("5","NewBom1","R5, R6","","","2","ERJ-3EKF75R0V","0.00","","","");
INSERT INTO bom VALUES("6","NewBom1","C2, C3, C6, C7, C8, C10","","","6","06035C104JAT2A","0.00","","","");
INSERT INTO bom VALUES("7","Test","C1","","","1","GRM188R71H104KA93D","0.00","","","");
INSERT INTO bom VALUES("8","Test","C4, C5","","","2","C1608C0G1H080C080AA","0.00","","","");
INSERT INTO bom VALUES("9","Test","RN2, RN3","","","2","ERJ-3GEYJ220V","0.00","","","");
INSERT INTO bom VALUES("10","Test","R1, R2, R3, R4","","","4","ERJ-3EKF22R0V","0.00","","","");
INSERT INTO bom VALUES("11","Test","R5, R6","","","2","ERJ-3EKF75R0V","0.00","","","");
INSERT INTO bom VALUES("12","Test","C2, C3, C6, C7, C8, C10","","","6","06035C104JAT2A","0.00","","","");
INSERT INTO bom VALUES("13","","C1","","","1","GRM188R71H104KA93D","0.00","","","");
INSERT INTO bom VALUES("14","","C4, C5","","","2","C1608C0G1H080C080AA","0.00","","","");
INSERT INTO bom VALUES("15","","RN2, RN3","","","2","ERJ-3GEYJ220V","0.00","","","");
INSERT INTO bom VALUES("16","","R1, R2, R3, R4","","","4","ERJ-3EKF22R0V","0.00","","","");
INSERT INTO bom VALUES("17","","R5, R6","","","2","ERJ-3EKF75R0V","0.00","","","");
INSERT INTO bom VALUES("18","","C2, C3, C6, C7, C8, C10","","","6","06035C104JAT2A","0.00","","","");
INSERT INTO bom VALUES("19","asdf","C1","","","1","GRM188R71H104KA93D","0.00","","","");
INSERT INTO bom VALUES("20","asdf","C4, C5","","","2","C1608C0G1H080C080AA","0.00","","","");
INSERT INTO bom VALUES("21","asdf","RN2, RN3","","","2","ERJ-3GEYJ220V","0.00","","","");
INSERT INTO bom VALUES("22","asdf","R1, R2, R3, R4","","","4","ERJ-3EKF22R0V","0.00","","","");
INSERT INTO bom VALUES("23","asdf","R5, R6","","","2","ERJ-3EKF75R0V","0.00","","","");
INSERT INTO bom VALUES("24","asdf","C2, C3, C6, C7, C8, C10","","","6","06035C104JAT2A","0.00","","","");
INSERT INTO bom VALUES("25","asdfasdf","C1","","","1","GRM188R71H104KA93D","0.00","","","");
INSERT INTO bom VALUES("26","asdfasdf","C4, C5","","","2","C1608C0G1H080C080AA","0.00","","","");
INSERT INTO bom VALUES("27","asdfasdf","RN2, RN3","","","2","ERJ-3GEYJ220V","0.00","","","");
INSERT INTO bom VALUES("28","asdfasdf","R1, R2, R3, R4","","","4","ERJ-3EKF22R0V","0.00","","","");
INSERT INTO bom VALUES("29","asdfasdf","R5, R6","","","2","ERJ-3EKF75R0V","0.00","","","");
INSERT INTO bom VALUES("30","asdfasdf","C2, C3, C6, C7, C8, C10","","","6","06035C104JAT2A","0.00","","","");
INSERT INTO bom VALUES("31","","C1","","","1","GRM188R71H104KA93D","0.00","","","");
INSERT INTO bom VALUES("32","","C4, C5","","","2","C1608C0G1H080C080AA","0.00","","","");
INSERT INTO bom VALUES("33","","RN2, RN3","","","2","ERJ-3GEYJ220V","0.00","","","");
INSERT INTO bom VALUES("34","","R1, R2, R3, R4","","","4","ERJ-3EKF22R0V","0.00","","","");
INSERT INTO bom VALUES("35","","R5, R6","","","2","ERJ-3EKF75R0V","0.00","","","");
INSERT INTO bom VALUES("36","","C2, C3, C6, C7, C8, C10","","","6","06035C104JAT2A","0.00","","","");
INSERT INTO bom VALUES("37","asdf","C1","","","1","GRM188R71H104KA93D","0.00","","","");
INSERT INTO bom VALUES("38","asdf","C4, C5","","","2","C1608C0G1H080C080AA","0.00","","","");
INSERT INTO bom VALUES("39","asdf","RN2, RN3","","","2","ERJ-3GEYJ220V","0.00","","","");
INSERT INTO bom VALUES("40","asdf","R1, R2, R3, R4","","","4","ERJ-3EKF22R0V","0.00","","","");
INSERT INTO bom VALUES("41","asdf","R5, R6","","","2","ERJ-3EKF75R0V","0.00","","","");
INSERT INTO bom VALUES("42","asdf","C2, C3, C6, C7, C8, C10","","","6","06035C104JAT2A","0.00","","","");





DROP TABLE bom_filename;

CREATE TABLE `bom_filename` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `bom_name` varchar(45) DEFAULT NULL,
  `filename` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO bom_filename VALUES("7","2018-02-27 11:40:43","asdf","20180227114043bom.xls");





DROP TABLE capacitor;

CREATE TABLE `capacitor` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `PrePN` int(11) DEFAULT NULL,
  `PostPN` int(11) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Operating_Temperature` varchar(255) DEFAULT NULL,
  `Package_Case` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(45) DEFAULT NULL,
  `Tolerance` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Capacitance` varchar(255) DEFAULT NULL,
  `Size_Dimension` varchar(255) DEFAULT NULL,
  `Thickness_Max` varchar(255) DEFAULT NULL,
  `Voltage_Rated` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Height` varchar(255) DEFAULT NULL,
  `Lead_Spacing` varchar(255) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `Dielectric_Material` varchar(225) DEFAULT NULL,
  `Dielectric_Characteristic` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

INSERT INTO capacitor VALUES("1","8pF - 50.0 - �5% - 0603","NEW000002-001","2","1","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 8pF - 50.0 - �5% - Ceramic","","","Kemet","C0603C829J5GACTU","","","-55�C to +125�C ","C0603","Reel Unmarked","","","","","","",""," �5%","NEW001.PcbLib","C0603","8pF","8pF","1.60mm x 0.80mm","0.80 � 0.07mm"," 50V","Datasheet","http://datasheet.octopart.com/C0603C829J5GACTU-Kemet-datasheet-10540709.pdf","","","green","Compliant","Not Listed by Manufacturer","NEW001","CC1003_C0G   4/30/201","A","2018-02-18","Lead Free","Ceramic","C0G/NP0","partimg/C0603C829J5GACTU-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("2","18pF - 50.0 - �5% - 0603","NEW000002-002","2","2","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 18pF - 50.0 - �5% - Ceramic","","","Kemet","C0603C180J5GACTU","","","-55�C to +125�C ","C0603","Reel Unmarked","2","","","","","",""," �5%","NEW001.PcbLib","C0603","18pF","18pF","1.60mm x 0.80mm","0.80 � 0.07mm"," 50 V","Datasheet","http://datasheet.octopart.com/C0603C180J5GACTU-Kemet-datasheet-22084244.pdf","","","green","Compliant","Not Listed by Manufacturer","NEW001","CC101_COMM_SMD  11/25/2013 ","A","2018-02-18","Lead Free","Ceramic","C0G/NP0","partimg/C0603C180J5GACTU-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("3","22pF - 50.0 - �5% - 0603","NEW000002-003","2","3","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 22pF - 50.0 - �5% - Ceramic","","","Kemet","C0603C220J5GACTU","","","-55�C to +125�C ","C0603","Reel Unmarked","2","","","","","",""," �5%","NEW001.PcbLib","C0603","22pF","22pF","1.60mm x 0.80mm","0.80 � 0.07mm"," 50 V","Datasheet","http://datasheet.octopart.com/C0603C220J5GACTU-Kemet-datasheet-22084244.pdf","0.0008mm","","green","Compliant","Not Listed by Manufacturer","NEW001","CC101_COMM_SMD 11/25/2013","A","2018-02-18","Lead Free","Ceramic","C0G/NP0","partimg/C0603C220J5GACTU-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("4","27pF - 50.0 - �5% - 0603","NEW000002-004","2","4","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 27pF - 50.0 - �5% - Ceramic","","","Kemet","C0603C270J5GACTU","","","-55�C to +125�C ","0603","Tape & Reel ","2","","","","","",""," �5%","NEW001.PcbLib","C0603","27pF","27pF","1.60mm x 0.80mm","0.87mm"," 50 V","Datasheet","http://datasheet.octopart.com/C0603C270J5GACTU-Kemet-datasheet-22084244.pdf"," 0.0008mm","","green","Compliant","Not Listed by Manufacturer","NEW001","CC101_COMM_SMD 11/25/2013","A","2018-02-18","Lead Free","Ceramic","C0G/NP0","partimg/C0603C270J5GACTU-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("5","0.001uF - 50.0 - �5% - 0603","NEW000002-005","2","5","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 0.001uF - 50.0 - �5% - Ceramic","","","Kemet","C0603C102J5GACTU","","","-55�C to +125�C ","0603","Tape & Reel ","2","","","","","",""," �5%","NEW001.PcbLib","C0603","0.001uF","0.001uF","1.60mm x 0.80mm","0.87mm"," 50 V","Datasheet","http://datasheet.octopart.com/C0603C102J5GACTU-Kemet-datasheet-22084244.pdf","0.0008mm","","green","Compliant","Not Listed by Manufacturer","NEW001","CC101_COMM_SMD  11/25/2013","A","2018-02-18","Lead Free","Ceramic","C0G/NP0","partimg/C0603C102J5GACTU-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("6","0.0047uF - 50.0 - �5% - 0603","NEW000002-006","2","6","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 0.0047uF - 50.0 - �5% - ","","","AVX Interconnect / Elco","06035C472JAT2A","","","-55�C to +125�C ","0603","Tape & Reel ","2","","","","","",""," �5%","NEW001.PcbLib","C0603","0.0047uF","0.0047uF","1.60mm x 0.81mm"," 0.90mm","50V","Datasheet","http://datasheet.octopart.com/06035C472JAT2A-AVX-datasheet-12509786.pdf","0.0008mm","","green","Compliant","Not Listed by Manufacturer","NEW001","Unknown","A","2018-02-18","Lead Free","","X7R","partimg/06035C472JAT2A-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("7","0.01uF - 50.0 - �5% - 0603","NEW000002-007","2","7","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 0.01uF - 50.0 - �5% - ","","","AVX Interconnect / Elco","06035C103JAT2A","","","-55�C to +125�C ","0603","Tape & Reel ","2","","","","","",""," �5%","NEW001.PcbLib","C0603","0.01uF","0.01uF","1.60mm x 0.81mm"," 0.90mm","50V","Datasheet","http://datasheet.octopart.com/06035C103JAT2A-AVX-datasheet-26446220.pdf","0.0008mm","","green","Compliant","Not Listed by Manufacturer","NEW001","Unknown","A","2018-02-18","Lead Free",""," X7R","partimg/06035C103JAT2A-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("8","0.047uF - 50.0 - �5% - 0603","NEW000002-008","2","8","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 0.047uF - 50.0 - �5% - ","","","AVX Interconnect / Elco","06035C473JAT2A","","","-55�C to +125�C ","0603","Tape & Reel ","","","","","","",""," �5%","NEW001.PcbLib","C0603","0.047uF","0.047uF","1.60mm x 0.81mm"," 0.90mm","50V","Datasheet","http://datasheet.octopart.com/Commercial-Surface-Mount-Chips---X7R-Dielectric-AVX-datasheet-family-100.pdf","","","green","Compliant","Active","NEW001","Unknown","A","2018-02-18","Lead Free",""," X7R","partimg/06035C473JAT2A-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("9","0.1uF - 50.0 - �5% - 0603","NEW000002-009","2","9","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 0.1uF - 50.0 - �5% - Ceramic Multilayer","","","AVX Interconnect / Elco","06035C104JAT2A","","","-55�C to +125�C ","0603","Tape & Reel ","2","","","","","",""," �5%","NEW001.PcbLib","C0603","0.1uF","0.1uF","1.60mm x 0.81mm"," 0.90mm","50V","Datasheet","http://datasheet.octopart.com/06035C104JAT2A-AVX-datasheet-26446220.pdf","","","green","Compliant","Active","NEW001","Unknown","A","2018-02-18","Lead Free"," Ceramic Multilayer"," X7R","partimg/06035C104JAT2A-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("10","1uF - 16.0 - �10% - 0603","NEW000002-010","2","10","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 1uF - 16.0 - �10% - Ceramic Multilayer","","","AVX Interconnect / Elco","0603YC105KAT2A","","","-55�C to +125�C ","0603","Tape & Reel ","2","","","","","",""," �10%","NEW001.PcbLib","C0603","1uF","1uF","1.60mm x 0.81mm"," 0.90mm","16V","Datasheet","http://datasheet.octopart.com/0603YC105KAT2A-AVX-datasheet-26446220.pdf","","","green","Compliant","Active","NEW001","Unknown","A","2018-02-18","Lead Free","Ceramic Multilayer"," X7R","partimg/0603YC105KAT2A-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C0603-PCB.jpg","3dimg/C0603-3D.jpg");
INSERT INTO capacitor VALUES("11","10uF - 16.0 - �10% - 1206","NEW000002-011","2","11","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 10uF - 16.0 - �10% - Ceramic Multilayer","","","AVX Interconnect / Elco","1206YC106KAT2A","","","-55�C to +125�C ","1206","Tape & Reel ","2","","","","","",""," �10%","NEW001.PcbLib","C1206","10uF","10uF","3.20mm x 1.60mm","1.78mm","16V ","Datasheet","http://datasheet.octopart.com/1206YC106KAT2A-AVX-datasheet-12587061.pdf","","","green","Compliant","Active","NEW001","Unknown","A","2018-02-18","Lead Free","Ceramic Multilayer","X7R","partimg/1206YC106KAT2A-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C1206-PCB.jpg","3dimg/C1206-3D.jpg");
INSERT INTO capacitor VALUES("12","22uF - 25.0 - �10% - 1210","NEW000002-012","2","12","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 22uF - 25.0 - �10% - Ceramic Multilayer","","","Murata","GRM32ER71E226KE15L","","","-55�C to +125�C ","1210","Tape & Reel ","2","","","","","",""," �10%","NEW001.PcbLib","C1210","22uF","22uF","3.20mm x 2.50mm","2.70mm","25V","Datasheet","http://datasheet.octopart.com/GRM32ER71E226KE15L-Murata-datasheet-22122223.pdf","2.5mm","","green","Compliant","Active","NEW001","Unknown","A","2018-02-18","Lead Free","Ceramic Multilayer"," X7R","partimg/GRM32ER71E226KE15L-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C1210-PCB.jpg","3dimg/C1210-3D.jpg");
INSERT INTO capacitor VALUES("13","47uF - 10.0 - �10% - 1210","NEW000002-013","2","13","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 47uF - 10.0 - �10% - Ceramic Multilayer","","","Murata","GRM32ER71A476KE15L","","","-55�C to +125�C ","1210","Tape & Reel ","2","","","","","",""," �10%","NEW001.PcbLib","C1210","47uF","47uF","3.20mm x 2.50mm","2.70mm","10V","Datasheet","http://datasheet.octopart.com/GRM32ER71A476KE15L-Murata-datasheet-8814637.pdf","2.5mm","","green","Compliant","Active","NEW001","FM7500U-092-NOV09      muRata GRM Hi-Cap 2009_11","A","2018-02-18","Lead Free","Ceramic Multilayer"," X7R","partimg/GRM32ER71A476KE15L-PIC.jpg","partsymbol/CAP-SCH.jpg","partfootprint/C1210-PCB.jpg","3dimg/C1210-3D.jpg");
INSERT INTO capacitor VALUES("14","1uF - 35.0 - �10% - 1206","NEW000002-014","2","14","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 1uF - 35.0 - �10% - Tantalum","","","AVX Interconnect / Elco","TAJA105K035RNJ","","","-55�C to +125�C ","1206","Tape & Reel ","2","","","","","",""," �10%","NEW001.PcbLib","C1206","1uF","1uF","3.20mm x1.60mm","","35V","Datasheet","http://datasheet.octopart.com/TAJA105K035RNJ-AVX-datasheet-12511273.pdf","1.80 mm","","green","Compliant","Active","NEW001","Unknown","A","2018-02-18","Lead Free","Tantalum","","partimg/TAJA105K035RNJ-PIC.jpg","partsymbol/CAPPOL-SCH.jpg","partfootprint/C1206-PCB.jpg","3dimg/C1206-3D.jpg");
INSERT INTO capacitor VALUES("15","22uF - 2.0 - �10% - 7343-31","NEW000002-015","2","15","CAP","NEW001.SchLib","","=Value","Standard","Standard","CAP 22uF - 2.0 - �10% - Tantalum","","","Sprague","293D226X9020D2TE3","","","- 55 �C to + 85 �C","7343-31","Tape & Reel ","2","","","","","",""," �10%","NEW001.PcbLib","C7343-31","22uF","22uF","7.30mm x 4.30mm","","6.3 V","Datasheet","http://datasheet.octopart.com/293D226X9020D2TE3-Vishay-datasheet-12545925.pdf","3.10mm","","green","Compliant","Active","NEW001","Revision: 30-Aug-12","A","2018-02-18","Lead Free","Tantalum","","partimg/293D226X9020D2TE3-PIC.jpg","partsymbol/CAPPOL-SCH.jpg","partfootprint/C7343-31-PCB.jpg","3dimg/C7343-31-3D.jpg");





DROP TABLE categories;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO categories VALUES("5","resistors");





DROP TABLE connector;

CREATE TABLE `connector` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(255) DEFAULT NULL,
  `Package_Case` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Height` varchar(10) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

INSERT INTO connector VALUES("1","RASM722X","NEW000004-001","RASM722X","NEW001.SchLib","","=Value","Standard","Standard","JACK SOCKET, DC; Gender:Receptacle; Current Rating:5A; Connector Mounting:PCB; Contact Termination:Solder; SVHC:No SVHC (19-Dec-2011); Connector Mounting Orientation:PCB; Connector Type:Power Entry; Mounting Type:PC Board; No. of Outlets:1; Termination Me","","","Switchcraft / Conxall","RASM722X","","","Surface Mount","","Bulk","","","","","","","","NEW001.PcbLib","RASM722X","Datasheet","http://datasheet.octopart.com/RASM722X-Switchcraft-datasheet-11630130.pdf","","","","green","Compliant","Not Listed by Manufacturer","","NEW001"," 05/02/2006 ","A","2018-02-18","Lead Free","partimg/RASM722X-PIC.jpg","partsymbol/RASM722X-SCH.jpg","partfootprint/RASM722X-PCB.jpg","3dimg/RASM722X-3D.jpg");
INSERT INTO connector VALUES("4","UX60-MB-5ST","NEW000004-004","UX60-MB-5ST","NEW001.SchLib","","=Value","Standard","Standard","Conn USB 2.0 Type A RCP 5 POS 0.8mm Solder RA SMD 5 Terminal 1 Port T/R","","","Hirose","UX60-MB-5ST","","","Surface Mount","","Tape and Reel","","","","","","","","NEW001.PcbLib","UX60-MB-5ST","Datasheet","http://datasheet.octopart.com/UX60-MB-5ST-Hirose-datasheet-8329595.pdf","","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Unknown","A","2018-02-18","Lead Free","partimg/UX60-MB-5ST-PIC.jpg","partsymbol/UX60-MB-5ST-SCH.jpg","partfootprint/UX60-MB-5ST-PCB.jpg","");
INSERT INTO connector VALUES("5","10103594-0001LF","NEW000004-005","10103594-0001LF","NEW001.SchLib","","=Value","Standard","Standard","CONN RCPT STD MICRO USB TYPE B","","","Framatome Connectors","10103594-0001LF","","","Surface Mount","","Tape and Reel","","","","","","","","NEW001.PcbLib","10103594-0001LF","Datasheet","http://datasheet.octopart.com/10103594-0001LF-FCI-datasheet-10887844.pdf","","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Unknown","A","2018-02-18","Lead Free","partimg/10103594-0001LF-PIC.jpg","partsymbol/10103594-0001LF-SCH.jpg","partfootprint/10103594-0001LF-PCB.jpg","");
INSERT INTO connector VALUES("6","480-37-1000","NEW000004-006","480-37-1000","NEW001.SchLib","","=Value","Standard","Standard","Universal Serial Bus (USB) Shielded I/O Plug, Type A, Right Angle, Surface Mount, 0.76µm Gold (Au) Plating, PCB Thickness 1.20mm-1.60mm, Tray Package, Lead-Free","","","Molex","480-37-1000","","","Surface Mount","","Tray","","","","","","","","NEW001.PcbLib","480-37-1000","Datasheet","http://datasheet.octopart.com/48037-1000-Molex-datasheet-142259.pdf","","","","green","Compliant","Active","","NEW001","GC.DGN REV A 96/06/12 LEVEL 3 ","A","2018-02-18","Lead Free","partimg/480-37-1000-PIC.jpg","partsymbol/480-37-1000-SCH.jpg","partfootprint/480-37-1000-PCB.jpg","");
INSERT INTO connector VALUES("7","61729-1011BLF","NEW000004-007","61729-1011BLF","NEW001.SchLib","","=Value","Standard","Standard","Conn USB Type B RCP 4 POS 2.5mm Solder RA Thru-Hole 4 Terminal 1 Port Tube","","","FCI","61729-1011BLF","","","Through Hole","","Tubes","","","","","","","","NEW001.PcbLib","61729-1011BLF","Datasheet","http://datasheet.octopart.com/61729-1011BLF-Areva-datasheet-8782292.pdf","","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Jul 28, 2010","A","2018-02-18","Lead Free","partimg/61729-1011BLF-PIC.jpg","partsymbol/61729-1011BLF-SCH.jpg","partfootprint/61729-1011BLF-PCB.jpg","");
INSERT INTO connector VALUES("8","1747981-1","NEW000004-008","1747981-1","NEW001.SchLib","","=Value","Standard","Standard","HDMI Connectors Product Highlights: Size = Standard Type A Series Receptacle 1 Ports Terminate To Printed Circuit Board","","","TE Connectivity / AMP","1747981-1","","","Surface Mount","","Tape and Reel","","","","","","","","NEW001.PcbLib","1747981-1","Datasheet","http://datasheet.octopart.com/1747981-1-Tyco-Electronics-datasheet-10742577.pdf","","","","green","Compliant","Not Listed by Manufacturer","","NEW001","ECO-11-005033 ","A","2018-02-18","Lead Free","partimg/1747981-1-PIC.jpg","partsymbol/1747981-1-SCH.jpg","partfootprint/1747981-1-PCB.jpg","");
INSERT INTO connector VALUES("9","PJRAN1X1U01X","NEW000004-009","PJRAN1X1U01X","NEW001.SchLib","","=Value","Standard","Standard","CONNECTOR, PHONO, JACK, 1WAY; Series:-;; CONNECTOR, PHONO, JACK, 1WAY; Series:-; Gender:Jack; No. of Contacts:1; Connector Color:Black; Contact Termination:Solder; Connector Body Material:Metal; Contact Plating:Tin; Color:Black; Connector Mounting:PCB","","","Switchcraft","PJRAN1X1U01X","","","Through Hole","","","","","","","","","","NEW001.PcbLib","PJRAN1X1U01X","Datasheet","http://datasheet.octopart.com/PJRAN1X1U01X-Switchcraft-datasheet-10098733.pdf","","","","green","Compliant","Not Listed by Manufacturer","","NEW001","ECO-25181 1-13-2006","A","2018-02-18","Lead Free","partimg/PJRAN1X1U01X-PIC.jpg","partsymbol/PJRAN1X1U01X-SCH.jpg","partfootprint/PJRAN1X1U01X-PCB.jpg","");
INSERT INTO connector VALUES("10","5555764-1","NEW000004-010","5555764-1","NEW001.SchLib","","=Value","Standard","Standard","Conn RJ-45 F 8 POS 1.27mm Solder RA SMD 8 Terminal 1 Port Tray Cat 3","","","TE Connectivity / AMP","5555764-1","","","Surface Mount","","Tray","","","","","","","","NEW001.PcbLib","5555764-1","Datasheet","http://datasheet.octopart.com/5555764-1-TE-Connectivity---AMP-datasheet-21774921.pdf","12.70mm","","","green","Compliant","Active","","NEW001","Unknown","A","2018-02-18","Lead Free","partimg/5555764-1-PIC.jpg","partsymbol/5555764-1-SCH.jpg","partfootprint/5555764-1-PCB.jpg","");
INSERT INTO connector VALUES("11","U.FL-R-SMT(10)","NEW000004-011","U.FL","NEW001.SchLib","","=Value","Standard","Standard","U.FL Series 50 Ohm Receptacle Male Pins Ultra Small SMT Coaxial Connector","","","Hirose","U.FL-R-SMT(10)","","","Surface Mount","","Tape and Reel","","","","","","","","NEW001.PcbLib","U.FL-R-SMT(10)","Datasheet","http://datasheet.octopart.com/U.FL-R-SMT%2810%29-Hirose-datasheet-8324213.pdf","1.9mm or 2","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Unknown","A","2018-02-18","Lead Free","partimg/U.FL-R-SMT(10)-PIC.jpg","partsymbol/U.FL-SCH.jpg","partfootprint/U.FL-R-SMT(10)-PCB.jpg","");
INSERT INTO connector VALUES("13","5-534237-8","NEW000004-013","5-534237-8","NEW001.SchLib","","=Value","Standard","Standard","Conn Socket Strip SKT 10 POS 2.54mm Solder ST Thru-Hole Tube","","","TE Connectivity / AMP","5-534237-8","","","Through Hole","","Tube","","","","","","","","NEW001.PcbLib","5-534237-8","Datasheet","http://datasheet.octopart.com/5-534237-8-Tyco-Electronics-datasheet-11077762.pdf","","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Revised 8-08 ","A","2018-02-18","Lead Free","partimg/5-534237-8-PIC.jpg","partsymbol/5-534237-8-SCH.jpg","partfootprint/5-534237-8-PCB.jpg","3dimg/5-534237-8-3D.jpg");
INSERT INTO connector VALUES("14","9-146281-0","NEW000004-014","9-146281-0","NEW001.SchLib","","=Value","Standard","Standard","AMPMODU 40 Position Through-Hole Single Row Straight 2.54 mm Breakaway Header","","","TE Connectivity / AMP","9-146281-0","","","Through Hole","","Bulk","","","","","","","","NEW001.PcbLib","9-146281-0","Datasheet","http://datasheet.octopart.com/9-146281-0-Tyco-Electronics-datasheet-11077762.pdf","","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Revised 8-08 ","A","2018-02-18","Lead Free","partimg/9-146281-0-PIC.jpg","partsymbol/9-146281-0-SCH.jpg","partfootprint/9-146281-0-PCB.jpg","3dimg/9-146281-0-3D.jpg");
INSERT INTO connector VALUES("15","95278-101A06LF","NEW000004-015","95278-101A06LF","NEW001.SchLib","","=Value","Standard","Standard","Unshrouded Header, SMT, Double Row, 6 Positions, 2.54 mm Pitch, Vertical","","","FCI","95278-101A06LF","","","Surface Mount","","Tape and Reel","","","","","","","","NEW001.PcbLib","95278-101A06LF","Datasheet","http://datasheet.octopart.com/95278-101A06LF-Areva-datasheet-8459731.pdf","","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Dec 12, 2008","A","2018-02-18","Lead Free","partimg/95278-101A06LF-PIC.jpg","partsymbol/95278-101A06LF-SCH.jpg","partfootprint/95278-101A06LF-PCB.jpg","");
INSERT INTO connector VALUES("16","BA2032SM","NEW000004-016","BA2032SM","NEW001.SchLib","","=Value","Standard","Standard","HOLDER COIN CELL 2032 EJECT SMD","","","MPD","BA2032SM","","","PCB, Surface Mount","","","","","","","","","","NEW001.PcbLib","BA2032SM","Datasheet","http://datasheet.octopart.com/BA2032SM-MPD-datasheet-14434172.pdf","6.70mm","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Unknown","A","2018-02-18","Lead Free","partimg/BA2032SM-PIC.jpg","partsymbol/BA2032SM-SCH.jpg","partfootprint/BA2032SM-PCB.jpg","");
INSERT INTO connector VALUES("17","B2P-VH(LF)(SN)","NEW000004-017","B2P-VH(LF)(SN)","NEW001.SchLib","","=Value","Standard","Standard","HEADER, PCB, VERTICAL, 2WAY; Connector Type:Wire to Board; Series:VH; Contact Termination:Crimp; Gender:Header; No. of Contacts:2; No. of Rows:1; Pitch Spacing:3.96mm; Contact Plating:Tin; Contact Material:Brass; SVHC:No SVHC (15-Dec-2010); Approval Bodie","","","JST","B2P-VH(LF)(SN)","","","Through Hole","","Bulk","","","","","","","","NEW001.PcbLib","B2P-VH(LF)(SN)","Datasheet","http://datasheet.octopart.com/B2P-VH%28LF%29%28SN%29-JST-datasheet-540220.pdf","16.5mm","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Unknown","A","2018-02-18","Lead Free","partimg/B2P-VH(LF)(SN)-PIC.jpg","partsymbol/B2P-VH(LF)(SN)-SCH.jpg","partfootprint/B2P-VH(LF)(SN)-PCB.jpg","3dimg/B2P-VH(LF)(SN)-3D.jpg");
INSERT INTO connector VALUES("19","B3P-VH(LF)(SN)","NEW000004-019","B3P-VH(LF)(SN)","NEW001.SchLib","","=Value","Standard","Standard","HEADER, PCB, VERTICAL, 3WAY; Connector Type:Wire to Board; Series:VH; Contact Termination:Through Hole Vertical; Gender:Header; No. of Contacts:3; No. of Rows:1; Pitch Spacing:3.96mm; Contact Plating:Tin; Contact Material:Brass; SVHC:No SVHC (15-Dec-2010)","","","JST","B3P-VH(LF)(SN)","","","Through Hole","","Bulk","","","","","","","","NEW001.PcbLib","B3P-VH(LF)(SN)","Datasheet","http://datasheet.octopart.com/B3P-VH%28LF%29%28SN%29-JST-datasheet-5332680.pdf","9.4mm","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Unknown","A","2018-02-18","Lead Free","partimg/B3P-VH(LF)(SN)-PIC.jpg","partsymbol/B3P-VH(LF)(SN)-SCH.jpg","partfootprint/B3P-VH(LF)(SN)-PCB.jpg","3dimg/B3P-VH-3D.jpg");
INSERT INTO connector VALUES("22","S2B-XH-A(LF)(SN)","NEW000004-022","S2B-XH-A(LF)(SN)","NEW001.SchLib","","=Value","Standard","Standard","XH Series 2.5mm 2 Position Through Hole Shrouded Header Connector","","","JST","S2B-XH-A(LF)(SN)","","","Through Hole, Right Angle","","Bulk","","","","","","","","NEW001.PcbLib","S2B-XH-A(LF)(SN)","Datasheet","http://datasheet.octopart.com/S2B-XH-A%28LF%29%28SN%29-JST-datasheet-8325564.pdf","9.8mm","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Unknown","A","2018-02-18","Lead Free","partimg/S2B-XH-A(LF)(SN)-PIC.jpg","partsymbol/S2B-XH-A(LF)(SN)-SCH.jpg","partfootprint/S2B-XH-A(LF)(SN)-PCB.jpg","3dimg/S2B-XH-A-3D.jpg");
INSERT INTO connector VALUES("24","S3B-XH-A(LF)(SN)","NEW000004-024","S3B-XH-A(LF)(SN)","NEW001.SchLib","","=Value","Standard","Standard","XH Series 3 Position 2.5 mm Pitch Through Hole Side Entry Shrouded Header","","","JST","S3B-XH-A(LF)(SN)","","","Through Hole, Right Angle","","Bulk","","","","","","","","NEW001.PcbLib","S3B-XH-A(LF)(SN)","Datasheet","http://datasheet.octopart.com/S3B-XH-A%28LF%29%28SN%29-JST-datasheet-8325564.pdf","9.8mm","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Unknown","A","2018-02-18","Lead Free","partimg/S3B-XH-A(LF)(SN)-PIC.jpg","partsymbol/S3B-XH-A(LF)(SN)-SCH.jpg","partfootprint/S3B-XH-A(LF)(SN)-PCB.jpg","3dimg/S3B-XH-A-3D.jpg");
INSERT INTO connector VALUES("26","S4B-XH-A(LF)(SN)","NEW000004-026","S4B-XH-A(LF)(SN)","NEW001.SchLib","","=Value","Standard","Standard","XH Series 4 Position 2.5 mm Through Hole Right Angle Press Fit Shrouded Header","","","JST","S4B-XH-A(LF)(SN)","","","Through Hole, Right Angle","","Bulk","","","","","","","","NEW001.PcbLib","S4B-XH-A(LF)(SN)","Datasheet","http://datasheet.octopart.com/S4B-XH-A%28LF%29%28SN%29-JST-datasheet-12510042.pdf","9.8mm","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Unknown","A","2018-02-18","Lead Free","partimg/S4B-XH-A(LF)(SN)-PIC.jpg","partsymbol/S4B-XH-A(LF)(SN)-SCH.jpg","partfootprint/S4B-XH-A(LF)(SN)-PCB.jpg","3dimg/S4B-XH-A-1-3D.jpg");
INSERT INTO connector VALUES("29","S2B-PH-K-S(LF)(SN)","NEW000004-029","S2B-PH-K-S(LF)(SN)","NEW001.SchLib","","=Value","Standard","Standard","PH Series 2 Position 2.0 mm Through Hole Side Entry Shrouded Header","","","JST","S2B-PH-K-S(LF)(SN)","","","Through Hole, Right Angle","","Bulk ","","","","","","","","NEW001.PcbLib","S2B-PH-K-S(LF)(SN)","Datasheet","http://datasheet.octopart.com/S2B-PH-K-S%28LF%29%28SN%29-JST-datasheet-8324864.pdf","8.0mm","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Unknown","A","2018-02-18","Lead Free","partimg/S2B-PH-K-S(LF)(SN)-PIC.jpg","partsymbol/S2B-PH-K-S(LF)(SN)-SCH.jpg","partfootprint/S2B-PH-K-S(LF)(SN)-PCB.jpg","3dimg/S2B-PH-K-S-3D.jpg");
INSERT INTO connector VALUES("30","S2B-PH-SM4-TB(LF)(SN)","NEW000004-030","S2B-PH-SM4-TB(LF)(SN)","NEW001.SchLib","","=Value","Standard","Standard","HEADER, SIDE ENTRY, SMD, 2WAY; Connector Type:Wire to Board; Series:PH; Contact Termination:Surface Mount Right Angle; Gender:Header; No. of Contacts:2; No. of Rows:1; Pitch Spacing:2mm; Contact Plating:Tin; Contact Material:Brass; SVHC:No SVHC (15-Dec-20","","","JST","S2B-PH-SM4-TB(LF)(SN)","","","Surface Mount","","Reel and Tape","","","","","","","","NEW001.PcbLib","S2B-PH-SM4-TB(LF)(SN)","Datasheet","http://datasheet.octopart.com/S2B-PH-SM4-TB%28LF%29%28SN%29-JST-datasheet-8324864.pdf","8.0mm","","","green","Compliant","Not Listed by Manufacturer","","NEW001","Unknown","A","2018-02-18","Lead Free","partimg/S2B-PH-SM4-TB(LF)(SN)-PIC.jpg","partsymbol/S2B-PH-SM4-TB(LF)(SN)-SCH.jpg","partfootprint/S2B-PH-SM4-TB(LF)(SN)-PCB.jpg","3dimg/S2B-PH-SM4-TB(LF)(SN)-3D.jpg");
INSERT INTO connector VALUES("34","1803277","NEW000004-034","1803277","NEW001.SchLib","","=Value","Standard","Standard","HEADER, RIGHT ANGLE, 3.81MM, 2WAY; Connector Type:Wire-to-Board; Gender:Pin; Ways, No. of:2; Lead Spacing:3.81mm; Termination Method:Solder; Series:MC; Rows, No. of:2; Current Rating:8A; Pitch:3.81mm; Voltage Rating, AC:160V ;RoHS Compliant: Yes","","","Phoenix Contact","1803277","","","Through Hole","","Bulk","","","","","","","","NEW001.PcbLib","1803277","Datasheet","http://datasheet.octopart.com/1803277-Phoenix-Contact-datasheet-23713020.pdf","11.1mm","","","green","Compliant","Not Listed by Manufacturer","","NEW001","11.11.2013","A","2018-02-18","Lead Free","partimg/1803277-PIC.jpg","partsymbol/1803277-SCH.jpg","partfootprint/1803277-PCB.jpg","3dimg/1803277-3D.jpg");
INSERT INTO connector VALUES("35","35RASMT4BHNTRX","NEW000004-035","35RASMT4BHNTRX","NEW001.SchLib","","=Value","Standard","Standard","Conn 3.5MM Stereo Jack F 3 POS Solder RA SMD 5 Terminal 1 Port T/R","","","Switchcraft","35RASMT4BHNTRX","","","Surface Mount","","Reel and Tape","2","","","","","","","NEW001.PcbLib","35RASMT4BHNTRX","Datasheet","http://datasheet.octopart.com/35RASMT4BHNTRX-Switchcraft-datasheet-17726097.pdf","","","","green","Compliant","Not Listed by Manufacturer","","NEW001","ECO-26972 6-20-2012","A","2018-02-18","Lead Free","partimg/35RASMT4BHNTRX-PIC.jpg","partsymbol/35RASMT4BHNTRX-SCH.jpg","partfootprint/35RASMT4BHNTRX-PCB.jpg","");





DROP TABLE custom_alias;

CREATE TABLE `custom_alias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(45) DEFAULT NULL,
  `cat_name` varchar(45) DEFAULT NULL,
  `alias_for_field` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;






DROP TABLE custom_pn;

CREATE TABLE `custom_pn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `postfix` varchar(45) DEFAULT NULL,
  `prefix` varchar(45) DEFAULT NULL,
  `CPN` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO custom_pn VALUES("5","resistors","00001","150","150-00001");





DROP TABLE default_values;

CREATE TABLE `default_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value_type` varchar(200) NOT NULL,
  `category` varchar(200) NOT NULL,
  `default_val` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO default_values VALUES("1","symbol","resistor","RES");
INSERT INTO default_values VALUES("2","symbol","capacitor","CAP");
INSERT INTO default_values VALUES("3","symbol","misc","FUSE_SMT");
INSERT INTO default_values VALUES("4","symbol","connector","10103594-0001LF");
INSERT INTO default_values VALUES("5","symbol","ic","AMP");
INSERT INTO default_values VALUES("6","symbol","diode","LED");
INSERT INTO default_values VALUES("7","symbol","transistor","NPN");
INSERT INTO default_values VALUES("8","symbol","oscillator","CRYSTAL4");
INSERT INTO default_values VALUES("9","footprint","resistor","R0805");
INSERT INTO default_values VALUES("10","footprint","capacitor","C0603");
INSERT INTO default_values VALUES("11","footprint","misc","B3U");
INSERT INTO default_values VALUES("12","footprint","connector","480-37-1000");
INSERT INTO default_values VALUES("13","footprint","ic","CC3000");
INSERT INTO default_values VALUES("14","footprint","diode","LED-0603-AMBER");
INSERT INTO default_values VALUES("15","footprint","transistor","TO-252");
INSERT INTO default_values VALUES("16","footprint","oscillator","4-SMD");





DROP TABLE diode;

CREATE TABLE `diode` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Impedance_Max_Zzt` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Package_Case` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Tolerance` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `Reverse_Recovery_Time_trr` varchar(255) DEFAULT NULL,
  `Size_Dimension` varchar(255) DEFAULT NULL,
  `Height` varchar(255) DEFAULT NULL,
  `Voltage_Reverse` varchar(255) DEFAULT NULL,
  `ESR_Equivalent_Series_Resistance` varchar(255) DEFAULT NULL,
  `Capacitance` varchar(255) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  `Peak_wavelength` varchar(225) DEFAULT NULL,
  `Lens_type` varchar(225) DEFAULT NULL,
  `Luminous_Intesity` varchar(225) DEFAULT NULL,
  `Viewing_Angle` varchar(225) DEFAULT NULL,
  `Polarity` varchar(225) DEFAULT NULL,
  `Current_Rating` varchar(225) DEFAULT NULL,
  `Power_rating` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

INSERT INTO diode VALUES("1","HSMG-C190","NEW000006-001","LED","","NEW001.SchLib","","=Value","Standard","Standard","LED 570NM GREEN DIFF 0603 SMD","","","","Avago","HSMG-C190","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm","NEW001.PcbLib","LED-0603-GREEN","http://datasheet.octopart.com/HSMG-C190-Avago-datasheet-10307877.pdf","Datasheet","5ns","1.60mm L x 0.80mm W","0.80mm","5v","","9pF","","NEW001","May 10, 2010","A","2018-02-18","green","","Lead Free","partimg/HSMG-C190-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-GREEN-PCB.jpg","3dimg/LED-0603-GREEN-3D.jpg","570 nm","Diffused","0.015 cd","170 degree","","","0.052W");
INSERT INTO diode VALUES("2","HSMH-C190","NEW000006-002","LED","","NEW001.SchLib","","=Value","Standard","Standard","LED 660NM RED DIFF 0603 SMD","","","","Avago","HSMH-C190","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm","NEW001.PcbLib","LED-0603-RED","http://datasheet.octopart.com/HSMH-C190-Avago-datasheet-10307877.pdf","Datasheet","5ns","1.60mm L x 0.80mm W","0.80mm","5v","","18pF","","NEW001","May 10, 2010","A","2018-02-18","green","","Lead Free","partimg/HSMH-C190-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-RED-PCB.jpg","3dimg/LED-0603-RED-3D.jpg","660 nm","Diffused","0.017 cd","170 degree","","","0.065W");
INSERT INTO diode VALUES("3","HSMY-C190","NEW000006-003","LED","","NEW001.SchLib","","=Value","Standard","Standard","LED 589NM YELLOW DIFF 0603 SMD","","","","Avago","HSMY-C190","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm","NEW001.PcbLib","LED-0603-YELLOW","http://datasheet.octopart.com/HSMY-C190-Avago-datasheet-10307877.pdf","Datasheet","5ns","1.60mm L x 0.80mm W","0.80mm","5v","","6pF","","NEW001","May 10, 2010","A","2018-02-18","green","","Lead Free","partimg/HSMY-C190-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-YELLOW-PCB.jpg","3dimg/LED-0603-YELLOW-3D.jpg","589 nm","Diffused","0.008 cd","170 degree","",""," 0.052W");
INSERT INTO diode VALUES("4","HSMD-C190","NEW000006-004","LED","","NEW001.SchLib","","=Value","Standard","Standard","LED 604NM ORANGE DIFF 0603 SMD","","","","Avago","HSMD-C190","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm","NEW001.PcbLib","LED-0603-ORANGE","http://datasheet.octopart.com/HSMD-C190-Avago-datasheet-10307877.pdf","Datasheet","5ns","1.60mm L x 0.80mm W","0.80mm","5v","","7pf","","NEW001","May 10, 2010","A","2018-02-18","green","","Lead Free","partimg/HSMD-C190-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-ORANGE-PCB.jpg","3dimg/LED-0603-ORANGE-3D.jpg","605 nm","Diffused","0.008 cd","170 degree","",""," 0.052W");
INSERT INTO diode VALUES("5","HSMA-C190","NEW000006-005","LED","","NEW001.SchLib","","=Value","Standard","Standard","LED 595NM AMBER DIFF 0603 SMD","","","","Avago","HSMA-C190","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm","NEW001.PcbLib","LED-0603-AMBER","http://datasheet.octopart.com/HSMA-C190-Avago-datasheet-10308043.pdf","Datasheet","5ns","1.60mm L x 0.80mm W","0.80mm","5v","","45pF","","NEW001","September 29, 2010","A","2018-02-18","green","","Lead Free","partimg/HSMA-C190-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-AMBER-PCB.jpg","3dimg/LED-0603-AMBER-3D.jpg","595 nm","Diffused","0.09 cd","170 degree","","","0.06W");
INSERT INTO diode VALUES("6","HSMR-C190","NEW000006-006","LED","","NEW001.SchLib","","=Value","Standard","Standard","LED 473NM BLUE DIFF 0603 SMD","","","","Avago","HSMR-C190","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm","NEW001.PcbLib","LED-0603-BLUE","http://datasheet.octopart.com/HSMR-C190-Avago-datasheet-8824602.pdf","Datasheet","","1.60mm L x 0.80mm W","0.80mm","5v","","110c/w","","NEW001","April 9, 2010","A","2018-02-18","green","","Lead Free","partimg/HSMR-C190-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-BLUE-PCB.jpg","3dimg/LED-0603-BLUE-3D.jpg","473 nm","Diffused","0.055 cd","140 degree","","","");
INSERT INTO diode VALUES("7","HSMW-C191","NEW000006-007","LED","","NEW001.SchLib","","=Value","Standard","Standard","LED WHITE DIFF 0603 SMD","","","","Avago","HSMW-C191","","","Surface Mount ","","","","","0603","Tape & Reel","2","Compliant","Active","","","�0.1 mm ","NEW001.PcbLib","LED-0603-WHITE","http://datasheet.octopart.com/HSMW-C191-Avago-datasheet-10314750.pdf","Datasheet","5ns","1.60mm L x 0.80mm W","0.60mm","5v","","55pF","","NEW001","May 5, 2010","A","2018-02-18","green","","Lead Free","partimg/HSMW-C191-PIC.jpg","partsymbol/LED-SCH.jpg","partfootprint/LED-0603-WHITE-PCB.jpg","3dimg/LED-0603-WHITE-3D.jpg","","Diffused","0.2 cd","140 degree","","","0.078W");
INSERT INTO diode VALUES("8","1N4148WS-7-F","NEW000006-008","DIODE","","NEW001.SchLib","","=Value","Standard","Standard","DIODE SWITCHING 75V 0.15A SOD323","","","","Diodes Inc.","1N4148WS-7-F","","","Surface Mount","","","","","SOD323","Tape & Reel","2","Compliant","Active","","","","NEW001.PcbLib","SOD-323","http://datasheet.octopart.com/1N4148WS-7-F-Diodes-Inc.-datasheet-11839050.pdf","Datasheet","4.0ns","","","75v","","2.0pF","","NEW001","Rev. 20 - 2  march2012","A","2018-02-18","green","","Lead Free","partimg/1N4148WS-7-F-PIC.jpg","partsymbol/DIODE-SCH.jpg","partfootprint/SOD-323-PCB.jpg","3dimg/SOD-323-3D.jpg","","","","","Standard","0.15A","0.2W");
INSERT INTO diode VALUES("9","SS12-E3/61T","NEW000006-009","SCHOTTKY","","NEW001.SchLib","","=Value","Standard","Standard","DIODE SCHOTTKY 20V 1A DO214C","","","","Vishay Semiconductor","SS12-E3/61T","","","Surface Mount","","","","","DO-214AC","Tape & Reel","2","Compliant","Not Listed by Manufacturer","","","","NEW001.PcbLib","DO-214AC","http://datasheet.octopart.com/SS12-E3/61T-Vishay-datasheet-11540009.pdf","Datasheet","","","","20v","","","","NEW001","26-Mar-12","A","2018-02-18","green","","Lead Free","partimg/SS12-E3-61T-PIC.jpg","partsymbol/SCHOTTKY-SCH.jpg","partfootprint/DO-214AC-PCB.jpg","3dimg/DO-214AC-3D.jpg","","","","","Standard","1.0A","");
INSERT INTO diode VALUES("10","BAT54S-7-F","NEW000006-010","SCHOTTKY","","NEW001.SchLib","","=Value","Standard","Standard","DIODE SCHOTTKY 70V 70MA SOT23","","","","Diodes Inc.","BAT54S-7-F","","","Surface Mount","","","","","SOT-23","Tape & Reel","3","Compliant","Active","","","","NEW001.PcbLib","SOT-23","http://datasheet.octopart.com/BAT54S-7-F-Diodes-Inc.-datasheet-5320128.pdf","Datasheet","5.0 ns","","","30v","","10pF","","NEW001","Rev. 24 - 2  april2009","A","2018-02-18","green","","Lead Free","partimg/BAT54S-7-F-PIC.jpg","partsymbol/SCHOTTKY-SCH.jpg","partfootprint/SOT-23-PCB.jpg","3dimg/SOT-23-3D.jpg","","","","","","0.2A","0.2W");
INSERT INTO diode VALUES("11","BAS70-04-7-F","NEW000006-011","SCHOTTKY","","NEW001.SchLib","","=Value","Standard","Standard","DIODE ARRAY SCHOTTKY 70V SOT23-3","","","","Diodes Inc.","BAS70-04-7-F","","","Surface Mount","","","","","SOT-23 ","Tape & Reel","3","Compliant","Active","","","","NEW001.PcbLib","SOT-23","http://datasheet.octopart.com/BAS70-04-7-F-Diodes-Inc.-datasheet-5395814.pdf","Datasheet","5.0 ns","","","70 v","","2.0pF","","NEW001","Rev. 21 - 2  july 2008","A","2018-02-18","green","","Lead Free","partimg/BAS70-04-7-F-PIC.jpg","partsymbol/SCHOTTKY-SCH.jpg","partfootprint/SOT-23-PCB.jpg","3dimg/SOT-23-3D.jpg","","","","","","","0.2W");





DROP TABLE footprint_autofill;

CREATE TABLE `footprint_autofill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autofill_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO footprint_autofill VALUES("1","manual");





DROP TABLE ic;

CREATE TABLE `ic` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `PrePN` varchar(45) DEFAULT NULL,
  `PostPN` varchar(45) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `Supplier_Device_Package` varchar(255) DEFAULT NULL,
  `Minimum_Orde_Quantity` varchar(255) DEFAULT NULL,
  `Reeling` varchar(255) DEFAULT NULL,
  `Product_Type` varchar(255) DEFAULT NULL,
  `Mounting_Style` varchar(255) DEFAULT NULL,
  `Height` varchar(10) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

INSERT INTO ic VALUES("1","UA7805CKTTR","NEW000005-001","5","1","UA7805","","NEW001.SchLib","","=Value","Standard","Standard","3 Pin 1.5A Fixed 5V Positive Voltage Regulator 3-DDPAK/TO-263 0 to 125","","","Texas Instruments","UA7805CKTTR","","","Surface Mount"," Tape and Reel","3","","","","","","","NEW001.PcbLib","TO-263","","","","","","","38.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=055d425&at=physicalpart&hlid=16691236","","NEW001","SLVS056O-MAY1976-REVISED AUGUST2012","A","2018-02-18","green","Compliant","Active",""," Contains Lead","partimg/UA7805CKTTR-PIC.jpg","partsymbol/UA7805-SCH.jpg","partfootprint/TO-263-PCB.jpg","");
INSERT INTO ic VALUES("2","LMS8117AMP-3.3/NOPB","NEW000005-002","5","2","LMS8117","","NEW001.SchLib","","=Value","Standard","Standard","1A Low-Dropout Linear Regulator 4-SOT-223 0 to 125","","","Texas Instruments","LMS8117AMP-3.3/NOPB","","","Surface Mount"," Tape and Reel"," 4","","","","","","","NEW001.PcbLib","SOT-223","","","","","","","","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0b88a90&at=physicalpart&hlid=16633772","","NEW001","April 2005","A","2018-02-18","green","Non-Compliant","Active",""," Contains Lead","partimg/LMS8117AMP-3.3-NOPB-PIC.jpg","partsymbol/LMS8117-SCH.jpg","partfootprint/SOT-223-PCB.jpg","");
INSERT INTO ic VALUES("3","LM1117IMP-5.0-NOPB","NEW000005-003","5","3","LM1117","","NEW001.SchLib","","=Value","Standard","Standard","800mA Low-Dropout Linear Regulator 4-SOT-223 -40 to 125","","","Texas Instruments","LM1117IMP-5.0-NOPB","","","Surface Mount"," Tape and Reel","4","","","","","","","NEW001.PcbLib","SOT-223","","","","","","","35.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=006ed0b&at=physicalpart&hlid=16624665","","NEW001","SNOS412M-FEBRUARY-2000-REVISED-MARCH2013","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/LM1117IMP-5.0-NOPB-PIC.jpg","partsymbol/LM1117-SCH.jpg","partfootprint/SOT-223-PCB.jpg","");
INSERT INTO ic VALUES("4","LP2985-33DBVR","NEW000005-004","5","4","LP2985-33","","NEW001.SchLib","","=Value","Standard","Standard","Single Output LDO, 150mA, Fixed(3.3V), 1.5% Tolerance, Low Quiescent Current, Low Noise 5-SOT-23 -40 to 125","","","Texas Instruments","LP2985-33DBVR","","","Surface Mount"," Tape and Reel","5","","","","","","","NEW001.PcbLib","SOT-23-5","","","","","","","18.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=04af963&at=physicalpart&hlid=16635153","","NEW001","SLVS522N-JULY2004-REVISED-JUNE2011","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/LP2985-33DBVR-PIC.jpg","partsymbol/LP2985-33-SCH.jpg","partfootprint/SOT-23-5-PCB.jpg","3dimg/SOT-23-5-3D.jpg");
INSERT INTO ic VALUES("5","LP2985-50DBVR","NEW000005-005","5","5","LP2985-50","","NEW001.SchLib","","=Value","Standard","Standard","Single Output LDO, 150mA, Fixed(5.0V), 1.5% Tolerance, Low Quiescent Current, Low Noise 5-SOT-23 -40 to 125","","","Texas Instruments","LP2985-50DBVR","","","Surface Mount"," Tape and Reel","5","","","","","","","NEW001.PcbLib","SOT-23-5","","","","","","","33.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0553daa&at=physicalpart&hlid=16635159","","NEW001","SLVS522N-JULY2004-REVISED-JUNE2011","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/LP2985-50DBVR-PIC.jpg","partsymbol/LP2985-50-SCH.jpg","partfootprint/SOT-23-5-PCB.jpg","3dimg/SOT-23-5-3D.jpg");
INSERT INTO ic VALUES("6","LMR10510YMFE/NOPB","NEW000005-006","5","6","LMR10510","","NEW001.SchLib","","=Value","Standard","Standard","SIMPLE SWITCHER 5.5Vin, 1A Step-Down Voltage Regulator in SOT-23 5-SOT-23 -40 to 125","","","Texas Instruments","LMR10510YMFE/NOPB","","",""," Tape and Reel","5","","","","","","","NEW001.PcbLib","SOT-23-5","","","","","","","35.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0807e8d&at=physicalpart&hlid=16633638","","NEW001","SNVS727B-OCTOBER2011-REVISED-APRIL2013","A","2018-02-18","green","Compliant"," Active","","Lead Free","partimg/LMR10510YMFE-NOPB-PIC.jpg","partsymbol/LMR10510-SCH.jpg","partfootprint/SOT-23-5-PCB.jpg","3dimg/SOT-23-5-3D.jpg");
INSERT INTO ic VALUES("7","LM2576HVS-5.0/NOPB","NEW000005-007","5","7","LM2576","","NEW001.SchLib","","=Value","Standard","Standard","SIMPLE SWITCHER® 3A Step-Down Voltage Regulator 5-DDPAK/TO-263 -40 to 125","","","Texas Instruments","LM2576HVS-5.0/NOPB","",""," Through Hole"," Tape and Reel","5","","","","","","","NEW001.PcbLib","TO-263","","","","","","","45.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=02a3fe5&at=physicalpart&hlid=16625726","","NEW001","SNVS107C-JUNE1999-REVISED-APRIL2013","A","2018-02-18","green","Non-Compliant"," Active",""," Contains Lead","partimg/LM2576HVS-5.0-NOPB-PIC.jpg","partsymbol/LM2576-SCH.jpg","partfootprint/TO-263-PCB.jpg","");
INSERT INTO ic VALUES("8","DRV8825PWP","NEW000005-008","5","8","DRV8825","","NEW001.SchLib","","=Value","Standard","Standard","2.5A Bipolar Stepper Motor Driver with On-Chip 1/32 Microstepping Indexer (Step/Dir Ctrl) 28-HTSSOP -40 to 85","","","Texas Instruments","DRV8825PWP","","",""," Tape and Reel","28 ","","","","","","","NEW001.PcbLib","28-HTSSOP","","","","","","","38.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=06288d3&at=physicalpart&hlid=16621369","","NEW001","SLVSA73F-APRIL2010-REVISED-JULY2014","A","2018-02-18","green","Compliant"," Active","","Lead Free","partimg/DRV8825PWP-PIC.jpg","partsymbol/DRV8825-SCH.jpg","partfootprint/28-HTSSOP-PCB.jpg","");
INSERT INTO ic VALUES("9","L293DD","NEW000005-009","5","9","L293","","NEW001.SchLib","","=Value","Standard","Standard","Push-Pull Four Channel Drivers with Diodes","","","STMicroelectronics","L293DD","","","Surface Mount","","20","","","","","","","NEW001.PcbLib","SOIC-20","","","","","","","","Datasheet","http://datasheet.octopart.com/L293DD-STMicroelectronics-datasheet-10011.pdf","","NEW001","JULY2013","A","2018-02-18","green","Compliant"," Active","","Lead Free","partimg/L293DD-PIC.jpg","partsymbol/L293-SCH.jpg","partfootprint/SOIC-20-PCB.jpg","");
INSERT INTO ic VALUES("10","FT232RL-REEL","NEW000005-010","5","10","FT232R","","NEW001.SchLib","","=Value","Standard","Standard","USB Full Speed to Serial UART IC, Includes Oscillator and EEPROM - SSOP-28","","","FTDI","FT232RL-REEL","","",""," Tape and Reel","28 ","","","","","","","NEW001.PcbLib","SSOP-28","","","","","","","","Datasheet","http://datasheet.octopart.com/FT232RL-REEL-FTDI-datasheet-11992319.pdf","","NEW001","Version 2.10 March-2012","A","2018-02-18","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/FT232RL-REEL-PIC.jpg","partsymbol/FT232R-SCH.jpg","partfootprint/SSOP-28-PCB.jpg","");
INSERT INTO ic VALUES("11","MAX232D","NEW000005-011","5","11","MAX232","","NEW001.SchLib","","=Value","Standard","Standard","Dual EIA-232 Driver/Receiver 16-SOIC 0 to 70","","","Texas Instruments","MAX232D","","","Surface Mount"," Tape and Reel","16","","","","","","","NEW001.PcbLib","SOIC-16","","","","","","","28.6mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=060918a&at=physicalpart&hlid=16637997","","NEW001","SLLS047M-FEBRUARY1989-REVISED-NOVEMBER2014","A","2018-02-18","green","Compliant"," Active","","Lead Free","partimg/MAX232D-PIC.jpg","partsymbol/MAX232-SCH.jpg","partfootprint/SOIC-16-PCB.jpg","3dimg/SOIC-16-3D.jpg");
INSERT INTO ic VALUES("12","W5100","NEW000005-012","5","12","W5100","","NEW001.SchLib","","=Value","Standard","Standard","W5100 Series 3.6 V Hardwired TCP/IP Embedded Ethernet Controller","","","WIZnet","W5100","","","PCB, Surface Mount"," Tray","80","","","","","","","NEW001.PcbLib","LQFP-80","","","","","","","","Datasheet","http://datasheet.octopart.com/W5100-WIZnet-datasheet-10558856.pdf","","NEW001","Unknown","A","2018-02-18","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/W5100-PIC.jpg","partsymbol/W5100-SCH.jpg","partfootprint/LQFP-80-PCB.jpg","");
INSERT INTO ic VALUES("13","HDG104-DN-2","NEW000005-013","5","13","HDG104","","NEW001.SchLib","","=Value","Standard","Standard","WIFI 802.11B/G SYSTEM 44QFN","","","H&D Wireless","HDG104-DN-2","","","PCB, Surface Mount"," Cut Tape","","","","","","","","NEW001.PcbLib","QFN-44","","","","","","","","Datasheet","http://datasheet.octopart.com/HDG104-DN-2-H%26D-Wireless-datasheet-13265248.pdf","","NEW001","Unknown","A","2018-02-18","green","Compliant","Not Listed by Manufacturer","","","partimg/HDG104-DN-2-PIC.jpg","partsymbol/HDG104-SCH.jpg","partfootprint/QFN-44-PCB.jpg","");
INSERT INTO ic VALUES("14","CC2540F256RHAR","NEW000005-014","5","14","CC2540","","NEW001.SchLib","","=Value","Standard","Standard","SimpleLink Bluetooth Smart Wireless MCU with USB 40-VQFN -40 to 85","","","Texas Instruments","CC2540F256RHAR","","","PCB, Surface Mount"," Tape and Reel","40 ","","","","","","","NEW001.PcbLib","VQFN-40","","","","","","","28.6mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0cd46d2&at=physicalpart&hlid=16614127","","NEW001","SWRS084F-OCTOBER2010-REVISEDJUNE2013","A","2018-02-18","green","Compliant"," Active","","Lead Free","partimg/CC2540F256RHAR-PIC.jpg","partsymbol/CC2540-SCH.jpg","partfootprint/VQFN-40-PCB.jpg","");
INSERT INTO ic VALUES("15","BALNRF01D3","NEW000005-015","5","15","BALNRF01D3","","NEW001.SchLib","","=Value","Standard","Standard","BALUN 50OHM INPUT","","","STMicroelectronics","BALNRF01D3","","",""," Tape and Reel","","","","","","","","NEW001.PcbLib","ECOPACK","","","","","","","","Datasheet","http://datasheet.octopart.com/BAL-NRF01D3-STMicroelectronics-datasheet-16346591.pdf","","NEW001","Unknown","A","2018-02-18","green","Compliant"," Active","","Lead Free","partimg/BALNRF01D3-PIC.jpg","partsymbol/BALNRF01D3-SCH.jpg","partfootprint/ECOPACK-PCB.jpg","");
INSERT INTO ic VALUES("16","2450AT07A0100T","NEW000005-016","5","16","2450AT07","","NEW001.SchLib","","=Value","Standard","Standard","ULTRA MINI 2.45GHZ CHIP ANTENNA / 7 Inch Reel","","","Johanson","2450AT07A0100T","","","Surface Mount"," Tape and Reel","","","","","","","","NEW001.PcbLib","2450AT07A0100T","","","","","","","","Datasheet","http://www.johansontechnology.com/datasheets/antennas/2450AT07A0100.pdf","","NEW001","Unknown","A","2018-02-18","green","Compliant","Not Listed by Manufacturer","","","partimg/2450AT07A0100T-PIC.jpg","partsymbol/2450AT07-SCH.jpg","partfootprint/2450AT07A0100T-PCB.jpg","");
INSERT INTO ic VALUES("17","CC3000MOD","NEW000005-017","5","17","CC3000","","NEW001.SchLib","","=Value","Standard","Standard","Wi-Fi Module IEEE 802.11b/g 11000Kbps 46-pin Tray","","","Texas Instruments","CC3000MOD","","","PCB, Surface Mount"," Tape and Reel","46","","","","","","","NEW001.PcbLib","CC3000","","","","","","","","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0efcf26&at=physicalpart&hlid=16614171","","NEW001","SWRS126-NOVEMBER2012","A","2018-02-18","green","Non-Compliant"," Active","","Contains Lead","partimg/CC3000MOD-PIC.jpg","partsymbol/CC3000-SCH.jpg","partfootprint/CC3000-PCB.jpg","");
INSERT INTO ic VALUES("18","2500AT44M0400E","NEW000005-018","5","18","2500AT44","","NEW001.SchLib","","=Value","Standard","Standard","ANT CHIP WIMAX 2.3-2.7GHZ / 7 Inch Reel","","","Johanson","2500AT44M0400E","","","Surface Mount"," Tape and Reel","","","","","","","","NEW001.PcbLib","2500AT44M0400E","","","","","","","1.00mm","Datasheet","http://datasheet.octopart.com/2500AT44M0400E-Johanson-datasheet-27138377.pdf","","NEW001","5/24/2013","A","2018-02-18","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/2500AT44M0400E-PIC.jpg","partsymbol/2500AT44-SCH.jpg","partfootprint/2500AT44M0400E-PCB.jpg","");
INSERT INTO ic VALUES("19","NRF905","NEW000005-019","5","19","NRF905","","NEW001.SchLib","","=Value","Standard","Standard","IC TXRX 433/68/915MHZ 32QFN","","","Nordic Power","NRF905","","","PCB, Surface Mount"," Tray ","32","","","","","","","NEW001.PcbLib","QFN-32","","","","","","","","Datasheet","http://datasheet.octopart.com/NRF905-Nordic-Semiconductor-datasheet-10669630.pdf","","NEW001","Unknown","A","2018-02-18","green","Compliant","Not Listed by Manufacturer","","","partimg/NRF905-PIC.jpg","partsymbol/NRF905-SCH.jpg","partfootprint/QFN-32-PCB.jpg","");
INSERT INTO ic VALUES("20","MCP73832T-2ACI/OT","NEW000005-020","5","20","MCP73832T","","NEW001.SchLib","","=Value","Standard","Standard","500 mA Single Cell Li-Ion/Li-Polymer Charge Mgmt controller","","","Microchip","MCP73832T-2ACI/OT","","","Surface Mount"," Tape and Reel","5","","","","","","","NEW001.PcbLib","SOT-23-5","","","","","","","","Datasheet","http://datasheet.octopart.com/MCP73832T-2ACI/OT-Microchip-datasheet-8814540.pdf","","NEW001","Revision E (September 2008)  (A TO E)","A","2018-02-18","green","Compliant"," Active","","Lead Free","partimg/MCP73832T-2ACI-OT-PIC.jpg","partsymbol/MCP73832T-SCH.jpg","partfootprint/SOT-23-5-PCB.jpg","3dimg/SOT-23-5-3D.jpg");
INSERT INTO ic VALUES("21","LM358DR","NEW000005-021","5","21","LM358","","NEW001.SchLib","","=Value","Standard","Standard","Dual Operational Amplifiers 8-SOIC 0 to 70","","","Texas Instruments","LM358DR","","","Surface Mount"," Tape and Reel","8","","","","","","","NEW001.PcbLib","SOIC-8","","","","","","","20.6mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0a03d13&at=physicalpart&hlid=16628525","","NEW001","SLOS068S-JUNE1976-REVISED-MAY2013","A","2018-02-18","green","Compliant"," Active","","Lead Free","partimg/LM358DR-PIC.jpg","partsymbol/LM358-SCH.jpg","partfootprint/SOIC-8-PCB.jpg","");
INSERT INTO ic VALUES("22","LMV324IDR","NEW000005-022","5","22","LMV324","","NEW001.SchLib","","=Value","Standard","Standard","Quad Low-Voltage Rail-to-Rail Output Operational Amplifier 14-SOIC -40 to 125","","","Texas Instruments","LMV324IDR","","","Surface Mount"," Tape and Reel","14 ","","","","","","","NEW001.PcbLib","SOIC-14","","","","","","","28.6mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=00dc390&at=physicalpart&hlid=16633906","","NEW001","SLOS263W-AUGUST1999-REVISED-OCTOBER2014","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/LMV324IDR-PIC.jpg","partsymbol/LMV324-SCH.jpg","partfootprint/SOIC-14-PCB.jpg","3dimg/SOIC-14-3D.jpg");
INSERT INTO ic VALUES("23","FAN4174IS5X","NEW000005-023","5","23","AMP","","NEW001.SchLib","","=Value","Standard","Standard","The FAN4174 (single) and FAN4274 (dual) are voltage feedback amplifiers with CMOS inputs that consume only 200 µA of supply current per amplifier, while providing ±33 mA of output short-circuit current. These amplifiers are designed to operate 5 V suppl","","","Fairchild Semiconductor","FAN4174IS5X","","","Surface Mount"," Tape and Reel","5","","","","","","","NEW001.PcbLib","SOT-23-5","","","","","","","","Datasheet","http://datasheet.octopart.com/FAN4174IS5X-Fairchild-Semiconductor-datasheet-12512810.pdf","","NEW001","FAN4174/FAN4274  Rev. 1.0.8","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/FAN4174IS5X-PIC.jpg","partsymbol/AMP-SCH.jpg","partfootprint/SOT-23-5-PCB.jpg","3dimg/SOT-23-5-3D.jpg");
INSERT INTO ic VALUES("24","DS1307ZN+","NEW000005-024","5","24","DS1307","","NEW001.SchLib","","=Value","Standard","Standard","RTC SERIAL 64X8, SMD, 1307, SOIC8; Date Format:Day; Date; Month; Year; Clock Format:hh:mm:ss; Clock IC Type:RTC; Interface Type:I2C, Serial; Memory Configuration:64 x 8; Supply Voltage Range:4.5V to 5.5V; Digital IC Case Style:SOIC; No. of Pins:8; Operati","","","Maxim Integrated","DS1307ZN+","","","Surface Mount"," Tube","8","","","","","","","NEW001.PcbLib","SOIC-8","","","","","","","","Datasheet","http://datasheet.octopart.com/DS1307ZN%2B-Dallas-Semiconductor-datasheet-11987049.pdf","","NEW001"," REV:100208 ","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/DS1307ZN+-PIC.jpg","partsymbol/DS1307-SCH.jpg","partfootprint/SOIC-8-PCB.jpg","");
INSERT INTO ic VALUES("25","NE555DR","NEW000005-025","5","25","NE555","","NEW001.SchLib","","=Value","Standard","Standard","Single Precision Timer 8-SOIC 0 to 70","","","Texas Instruments","NE555DR","","","Surface Mount"," Tape and Reel","8","","","","","","","NEW001.PcbLib","SOIC-8","","","","","","","35.0","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0031693&at=physicalpart&hlid=16640884","","NEW001","SLFS022I-SEPTEMBER1973-REVISED-SEPTEMBER2014","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/NE555DR-PIC.jpg","partsymbol/NE555-SCH.jpg","partfootprint/SOIC-8-PCB.jpg","");
INSERT INTO ic VALUES("26","S25FL204K0TMFI011","NEW000005-026","5","26","S25FL204","","NEW001.SchLib","","=Value","Standard","Standard","IC, FLASH MEM, 4 MBIT, 85 MHZ, 8-SOIC; Memory Type:Flash - NOR; Memory Size:4Mbit; Memory Configuration:-; Supply Voltage Min:2.7V; Supply Voltage Max:3.6V; Memory Case Style:SOIC; No. of Pins:8; Clock Frequency:85MHz ;RoHS Compliant: Yes","","","Spansion","S25FL204K0TMFI011","","",""," Tube","8","","","","","","","NEW001.PcbLib","SOIC-8","","","","","","","","Datasheet","http://datasheet.octopart.com/S25FL204K0TMFI011-Spansion-datasheet-16486155.pdf","","NEW001","Revision:06 Issue Date:January 17, 2013","A","2018-02-18","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/S25FL204K0TMFI011-PIC.jpg","partsymbol/S25FL204-SCH.jpg","partfootprint/SOIC-8-PCB.jpg","");
INSERT INTO ic VALUES("27","24AA02T-I/OT","NEW000005-027","5","27","24AA02T","","NEW001.SchLib","","=Value","Standard","Standard","2K 256 X 8 1.8V SERIAL EE IND","","","Microchip","24AA02T-I/OT","","","Surface Mount","","5","","","","","","","NEW001.PcbLib","SOT-23-5","","","","","","","","Datasheet","http://datasheet.octopart.com/24AA02T-I/OT-Microchip-datasheet-7546411.pdf","","NEW001","02/04/09","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/24AA02T-I-OT-PIC.jpg","partsymbol/24AA02T-SCH.jpg","partfootprint/SOT-23-5-PCB.jpg","3dimg/SOT-23-5-3D.jpg");
INSERT INTO ic VALUES("28","CAT24C32WI-GT3","NEW000005-028","5","28","CAT24C32WI","","NEW001.SchLib","","=Value","Standard","Standard","CAT24C32 Series 32 Kb (4K X 8) 1.8 V - 5.5 V I2C CMOS Serial EEPROM - SOIC-8","","","ON Semiconductor","CAT24C32WI-GT3","","","Surface Mount"," Tape and Reel","8","","","","","","","NEW001.PcbLib","SOIC-8","","","","","","","","Datasheet","http://datasheet.octopart.com/CAT24C32WI-GT3-ON-Semiconductor-datasheet-14714113.pdf","","NEW001"," March, 2013?Rev. 16","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/CAT24C32WI-GT3-PIC.jpg","partsymbol/CAT24C32WI-SCH.jpg","partfootprint/SOIC-8-PCB.jpg","");
INSERT INTO ic VALUES("29","ATMEGA328P-AU","NEW000005-029","5","29","ATMEGA328P","","NEW001.SchLib","","=Value","Standard","Standard","ATmega Series 20 MHz 32 KB Flash 2 KB SRAM 8-Bit Microcontroller - TQFP-32","","","Atmel","ATMEGA328P-AU","","",""," Tube","32","","","","","","","NEW001.PcbLib","TQFP-32","","","","","","","","Datasheet","http://datasheet.octopart.com/ATMEGA328P-AU-Atmel-datasheet-14421303.pdf","","NEW001"," 8271GS-AVR-02/201","A","2018-02-18","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/ATMEGA328P-AU-PIC.jpg","partsymbol/ATMEGA328P-SCH.jpg","partfootprint/TQFP-32-PCB.jpg","");
INSERT INTO ic VALUES("30","ATMEGA328P-PU","NEW000005-030","5","30","ATMEGA328P-PU","","NEW001.SchLib","","=Value","Standard","Standard","ATmega Series 20 MHz 32 KB Flash 2 KB SRAM 8-Bit Microcontroller - DIP-28","","","Atmel","ATMEGA328P-PU","","",""," Tape and Reel","32","","","","","","","NEW001.PcbLib","DIP-28","","","","","","","","Datasheet","http://datasheet.octopart.com/ATMEGA328P-PU-Atmel-datasheet-14421303.pdf","","NEW001","Rev.: 8271GS-AVR-02/2013","A","2018-02-18","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/ATMEGA328P-PU-PIC.jpg","partsymbol/ATMEGA328P-PU-SCH.jpg","partfootprint/DIP-28-PCB.jpg","");
INSERT INTO ic VALUES("31","ATMEGA1284P-AU","NEW000005-031","5","31","ATMEGA1284P","","NEW001.SchLib","","=Value","Standard","Standard","ATmega Series 20 MHz 128 KB Flash 16 KB SRAM 8-Bit Microcontroller - TQFP-44","","","Atmel","ATMEGA1284P-AU","","",""," Tape and Reel","44","","","","","","","NEW001.PcbLib","TQFP-44","","","","","","","","Datasheet","http://datasheet.octopart.com/ATMEGA1284P-AU-Atmel-datasheet-13381179.pdf","","NEW001","8272D-AVR-05/12","A","2018-02-18","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/ATMEGA1284P-AU-PIC.jpg","partsymbol/ATMEGA1284P-SCH.jpg","partfootprint/TQFP-44-PCB.jpg","");
INSERT INTO ic VALUES("32","ATMEGA2560V8AU","NEW000005-032","5","32","ATMEGA2560","","NEW001.SchLib","","=Value","Standard","Standard","ATmega Series 8 MHz 256 KB Flash 8 KB SRAM 8-Bit Microcontroller - TQFP-100","","","Atmel","ATMEGA2560V8AU","","","Surface Mount"," Tray ","100","","","","","","","NEW001.PcbLib","TQFP-100","","","","","","","","Datasheet","http://datasheet.octopart.com/ATMEGA2560V8AU-Atmel-datasheet-24967521.pdf","","NEW001","2549QS-AVR-02/201","A","2018-02-18","green"," Compliant","Not Listed by Manufacturer","","Lead Free","partimg/ATMEGA2560V8AU-PIC.jpg","partsymbol/ATMEGA2560-SCH.jpg","partfootprint/TQFP-100-PCB.jpg","");
INSERT INTO ic VALUES("33","ATTINY85-20PU","NEW000005-033","5","33","ATTINY85","","NEW001.SchLib","","=Value","Standard","Standard","AVR, 4KB FLASH, 256B SRAM, ADC, 2 TIMERS - 5V, 20MHZ, PDIP, IND TEMP, GREEN","","","Atmel","ATTINY85-20PU","","","Through Hole"," Tube","8","","","","","","","NEW001.PcbLib","DIP-8","","","","","","","","Datasheet","http://datasheet.octopart.com/ATTINY85-20PU-Atmel-datasheet-10223956.pdf","","NEW001","2586N-AVR-04/11","A","2018-02-18","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/ATTINY85-20PU-PIC.jpg","partsymbol/ATTINY85-SCH.jpg","partfootprint/DIP-8-PCB.jpg","");
INSERT INTO ic VALUES("34","LPC1768FBD100,551","NEW000005-034","5","34","LPC1768","","NEW001.SchLib","","=Value","Standard","Standard","LPC1768FBD100","","","NXP Semiconductors","LPC1768FBD100,551","","",""," Tray","100","","","","","","","NEW001.PcbLib","LQFP-100","","","","","","","","Datasheet","http://datasheet.octopart.com/LPC1768FBD100%2C551-NXP-Semiconductors-datasheet-10855012.pdf","","NEW001","Rev. 8 - 14 November 2011","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/LPC1768FBD100,551-PIC.jpg","partsymbol/LPC1768-SCH.jpg","partfootprint/LQFP-100-PCB.jpg","");
INSERT INTO ic VALUES("35","STM32F103CBT6","NEW000005-035","5","35","STM32F103CBT6","","NEW001.SchLib","","=Value","Standard","Standard","Mainstream Performance line, ARM Cortex-M3 MCU with 128 Kbytes Flash, 72 MHz CPU, motor control, USB and CAN","","","STMicroelectronics","STM32F103CBT6","","","Surface Mount"," Tray","48","","","","","","","NEW001.PcbLib","LQFP-48","","","","","","","","Datasheet","http://datasheet.octopart.com/STM32F103CBT6-STMicroelectronics-datasheet-10330614.pdf","","NEW001","April 2011 Doc ID 13587 Rev 13 ","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/STM32F103CBT6-PIC.jpg","partsymbol/STM32F103CBT6-SCH.jpg","partfootprint/LQFP-48-PCB.jpg","");
INSERT INTO ic VALUES("36","STM32F103RBT6","NEW000005-036","5","36","STM32F103RBT6","","NEW001.SchLib","","=Value","Standard","Standard","Mainstream Performance line, ARM Cortex-M3 MCU with 128 Kbytes Flash, 72 MHz CPU, motor control, USB and CAN","","","STMicroelectronics","STM32F103RBT6","","","Surface Mount"," Tray","64","","","","","","","NEW001.PcbLib","LQFP-64","","","","","","","","Datasheet","http://datasheet.octopart.com/STM32F103RBT6-STMicroelectronics-datasheet-500013.pdf","","NEW001","September 2008 Rev 9 ","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/STM32F103RBT6-PIC.jpg","partsymbol/STM32F103RBT6-SCH.jpg","partfootprint/LQFP-64-PCB.jpg","3dimg/LQFP-64-3D.jpg");
INSERT INTO ic VALUES("37","MK22FN1M0VLH12","NEW000005-037","5","37","MK22FN1","","NEW001.SchLib","","=Value","Standard","Standard","MCU 32-bit Kinetis K22 ARM Cortex M4 1024KB Flash 3.3V 64-Pin LQFP Tray","","","Freescale Semiconductor","MK22FN1M0VLH12","","","","Tray","64","","","","","","","NEW001.PcbLib","LQFP-64","","","","","","","","Datasheet","http://datasheet.octopart.com/MK22FN1M0VLH12-Freescale-Semiconductor-datasheet-16471319.pdf","","NEW001","Rev. 2, 05/2013","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/MK22FN1M0VLH12-PIC.jpg","partsymbol/MK22FN1-SCH.jpg","partfootprint/LQFP-64-PCB.jpg","3dimg/LQFP-64-3D.jpg");
INSERT INTO ic VALUES("38","MSP430F2132IPW","NEW000005-038","5","38","MSP430F","","NEW001.SchLib","","=Value","Standard","Standard","16-bit Ultra-Low-Power Microcontroller, 8kB Flash, 512B RAM, 10 bit ADC, 1 USCI 28-TSSOP -40 to 85","","","Texas Instruments","MSP430F2132IPW","","","","Tape and Reel","28 ","","","","","","","NEW001.PcbLib","TSSOP-28","","","","","","","35.0mm","Datasheet","http://octopart-clicks.com/click/hltrack?ak=5d89adf6&sig=0252055&at=physicalpart&hlid=16639135","","NEW001","SLAS578J-NOVEMBER2007-REVISED-JANUARY2012","A","2018-02-18","green","Compliant","Active","","Lead Free","partimg/MSP430F2132IPW-PIC.jpg","partsymbol/MSP430F-SCH.jpg","partfootprint/TSSOP-28-PCB.jpg","");
INSERT INTO ic VALUES("39","SPU0409HD5H-PB","NEW000005-039","5","39","SPU0409HD5H","","NEW001.SchLib","","=Value","Standard","Standard","; Transducer Function:Microphone; Device Type:UltraMini Microphone; Directivity:Omni Directional; Sensitivity (dB):-42dB; Output Impedance:300ohm; Length:3.76mm; Sensitivity:-42 dB","","","Knowles Acoustics","SPU0409HD5H-PB","","","Surface Mount"," Tape and Reel","4","","","","","","","NEW001.PcbLib","SPU-4","","","","","","","1.00mm","Datasheet","http://datasheet.octopart.com/SPU0409HD5H-PB-Knowles-Acoustics-datasheet-17019172.pdf","","NEW001","Revision:F 3/27/2013","A","2018-02-18","green","Compliant","Not Listed by Manufacturer","","","partimg/SPU0409HD5H-PB-PIC.jpg","partsymbol/SPU0409HD5H-SCH.jpg","partfootprint/SPU-4-PCB.jpg","");
INSERT INTO ic VALUES("40","MMA8452QR1","NEW000005-040","5","40","MMA8452Q","","NEW001.SchLib","","=Value","Standard","Standard","IC, DIGITAL ACCELERATION SENSOR, ±2G / ±4G / ±8G, 12BIT, QFN-16; Acceleration Range:± 2g, ± 4g, ± 8g; No. of Axes:3; IC Interface Type:I2C; Sensor Case Style:QFN; No. of Pins:16; Supply Voltage Range:1.95V to 3.6V ;RoHS Compliant: Yes","","","Freescale Semiconductor","MMA8452QR1","","","Surface Mount","Tape and Reel","16","","","","","","","NEW001.PcbLib","QFN-16","","","","","","","","Datasheet","http://datasheet.octopart.com/MMA8452QR1-Freescale-Semiconductor-datasheet-21199847.pdf","","NEW001","Rev. 8.1, 10/2013","A","2018-02-18","green","Compliant","Active","","Contains Lead","partimg/MMA8452QR1-PIC.jpg","partsymbol/MMA8452Q-SCH.jpg","partfootprint/QFN-16-PCB.jpg","3dimg/QFN-16-3D.jpg");
INSERT INTO ic VALUES("41","DS18B20U+","NEW000005-041","5","41","DS18B20U+","","NEW001.SchLib","","=Value","Standard","Standard","THERMOMETER, DIG, PROG, SMD, 18B20; IC Output Type:Digital; Sensing Accuracy Range:± 0.5°C; Temperature Sensing Range:-55°C to +125°C; Supply Current:1mA; Supply Voltage Range:3V to 5.5V; Resolution (Bits):12bit; Sensor Case Style:MSOP; No. of Pins:8;","","","Maxim Integrated","DS18B20U+","","","Surface Mount","Tape and Reel","8","","","","","","","NEW001.PcbLib","MSOP-8","","","","","","","","Datasheet","http://datasheet.octopart.com/DS18B20U%2B-Dallas-Semiconductor-datasheet-11984949.pdf","","NEW001","REV:042208 ","A","2018-02-18","green","Compliant","Not Listed by Manufacturer","","Lead Free","partimg/DS18B20U+-PIC.jpg","partsymbol/DS18B20U+-SCH.jpg","partfootprint/MSOP-8-PCB.jpg","3dimg/MSOP-8-3D.jpg");





DROP TABLE misc;

CREATE TABLE `misc` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacture_Part_Number1` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(255) DEFAULT NULL,
  `Operating_Temperature` varchar(255) DEFAULT NULL,
  `Package_Case` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `Height` varchar(10) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO misc VALUES("2","1825289-1","NEW000003-002","MHS222","NEW001.SchLib","","=Value","Standard","Standard","Switch Slide DPDT Top Slide 0.3A 125VAC PC Pins Thru-Hole Tray","","MHS222","TE Connectivity / Alcoswitch","1825289-1","","","Through Hole","- 20 �C to + 85 �C","MHS222","Bulk","","Compliant","Active","","","","","","","NEW001.PcbLib","MHS222","","Datasheet","http://sigma.octopart.com/14475282/technical_drawing/Tyco-Electronics-1825289-1.pdf","","green","","NEW001","REVISED PER ECO-11-004587","A","2018-02-18","Lead Free","partimg/1825289-1-PIC.jpg","partsymbol/MHS222-SCH.jpg","partfootprint/MHS222-PCB.jpg","");
INSERT INTO misc VALUES("3","B3U-3000P","NEW000003-003","B3U","NEW001.SchLib","","=Value","Standard","Standard","TACTILE SWITCH, SIDE ACTUATED, SMD; Contact Configuration:SPST-NO; Operating Force:1.59N; Contact Voltage DC Nom:12V; Contact Current Max:50mA; Actuation Type:Side; Switch Terminals:Surface Mount; SVHC:No SVHC (20-Jun-2011); Actuator Type:Side Actuated; C","","B3U","Omron","B3U-3000P","","","Surface Mount, Right Angle","�25  �C to +70�C","B3U"," Tape & Reel","","Compliant","Not Listed by Manufacturer","","","","","","","NEW001.PcbLib","B3U","","Datasheet","http://datasheet.octopart.com/B3U-3000P-Omron-datasheet-10910421.pdf","","green","","NEW001","Unknown","A","2018-02-18","Lead Free","partimg/B3U-3000P-PIC.jpg","partsymbol/B3U-SCH.jpg","partfootprint/B3U-PCB.jpg","");
INSERT INTO misc VALUES("4","1812L050PR","NEW000003-004","FUSE_SMT","NEW001.SchLib","","=Value","Standard","Standard","FUSE, RESETTABLE, 1812, 15V, 500MA; Tripping Current:1A; Carrying Current Max:100A; Initial Resistance Max:1.0ohm; Operating Voltage:15VDC; Series:1812L; SVHC:No SVHC (20-Jun-2011); Approval Bodies:TUV; Approval Category:UL Recognised; External Depth:3.41","","1812","Littelfuse","1812L050PR","","","Surface Mounting","- 40 �C to + 85 �C","1812"," Tape & Reel","","Compliant","Not Listed by Manufacturer","","","","","","","NEW001.PcbLib","1812","","Datasheet","http://datasheet.octopart.com/1812L050PR-Littelfuse-datasheet-8701984.pdf","","green","","NEW001","Revised: June 28, 2010 ","A","2018-02-18","Lead Free","partimg/1812L050PR-PIC.jpg","partsymbol/FUSE_SMT-SCH.jpg","partfootprint/1812-PCB.jpg","3dimg/1812.jpg");
INSERT INTO misc VALUES("5","1812L075PR","NEW000003-005","FUSE_SMT","NEW001.SchLib","","=Value","Standard","Standard","FUSE, RESETTABLE, 1812, 13.2V, 750MA; Tripping Current:1.5A; Carrying Current Max:100A; Initial Resistance Max:0.45ohm; Operating Voltage:13.2VDC; Series:1812L; SVHC:No SVHC (20-Jun-2011); Approval Bodies:TUV; Approval Category:UL Recognised; External Dep","","1812","Littelfuse","1812L075PR","","","Surface Mounting","- 40 �C to + 85 �C","1812"," Tape & Reel","","Compliant","Not Listed by Manufacturer","","","","","","","NEW001.PcbLib","1812","","Datasheet","http://datasheet.octopart.com/1812L075PR-Littelfuse-datasheet-24577145.pdf","","green","","NEW001","Revised: 04/01/14 ","A","2018-02-18","Lead Free","partimg/1812L075PR-PIC.jpg","partsymbol/FUSE_SMT-SCH.jpg","partfootprint/1812-PCB.jpg","3dimg/1812.jpg");
INSERT INTO misc VALUES("6","1812L110/33MR","NEW000003-006","FUSE_SMT","NEW001.SchLib","","=Value","Standard","Standard","FUSE, PTC RESET, 33V, 1.1A, 1812; Holding Current:1.1A; Tripping Current:1.95A; Initial Resistance Max:0.2ohm; Operating Voltage:33V; Series:POLYFUSE; PTC Fuse Case:1812; External Depth:2mm; Initial Resistance Min:0.06ohm ;RoHS Compliant: Yes","","1812","Littelfuse","1812L110/33MR","","","Surface Mounting","- 40 �C to + 85 �C","1812"," Tape & Reel","","Compliant","Not Listed by Manufacturer","","","","","","","NEW001.PcbLib","1812","","Datasheet","http://datasheet.octopart.com/1812L110/33MR-Littelfuse-datasheet-8701984.pdf","","green","","NEW001","Revised: June 28, 2011","A","2018-02-18","Lead Free","partimg/1812L110-33MR-PIC.jpg","partsymbol/FUSE_SMT-SCH.jpg","partfootprint/1812-PCB.jpg","3dimg/1812.jpg");
INSERT INTO misc VALUES("7","1812L160/12DR","NEW000003-007","FUSE_SMT","NEW001.SchLib","","=Value","Standard","Standard","POLYFUSE 1.6A 12V SMD 1812 RoHSconf","","1812","Littelfuse","1812L160/12DR","","","Surface Mounting","- 40 �C to + 85 �C","1812"," Tape & Reel","","Compliant","Not Listed by Manufacturer","","","","","","","NEW001.PcbLib","1812","","Datasheet","http://datasheet.octopart.com/1812L160-12DR-Littelfuse-datasheet-8701984.pdf","","green","","NEW001","Revised: June 28, 2010","A","2018-02-18","Lead Free","partimg/1812L160-12DR-PIC.jpg","partsymbol/FUSE_SMT-SCH.jpg","partfootprint/1812-PCB.jpg","3dimg/1812.jpg");





DROP TABLE new_part;

CREATE TABLE `new_part` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(225) DEFAULT NULL,
  `CPN` varchar(225) DEFAULT NULL,
  `Description` varchar(225) DEFAULT NULL,
  `Manufacturer` varchar(225) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `table_name` varchar(45) DEFAULT NULL,
  `Library_Ref` varchar(225) DEFAULT NULL,
  `Library_Path` varchar(45) DEFAULT NULL,
  `Category` varchar(45) DEFAULT NULL,
  `Comment` varchar(45) DEFAULT NULL,
  `Component_Kind` varchar(45) DEFAULT NULL,
  `Component_Type` varchar(45) DEFAULT NULL,
  `Designator` varchar(45) DEFAULT NULL,
  `Footprint` varchar(45) DEFAULT NULL,
  `Library_Reference` varchar(45) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Packaging` varchar(45) DEFAULT NULL,
  `Pin_Count` varchar(45) DEFAULT NULL,
  `Signal_Integrity` varchar(45) DEFAULT NULL,
  `Simulation` varchar(45) DEFAULT NULL,
  `Supplier` varchar(45) DEFAULT NULL,
  `Supplier_Part_Number` varchar(45) DEFAULT NULL,
  `Footprint_Path` varchar(45) DEFAULT NULL,
  `Footprint_Ref` varchar(45) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(45) DEFAULT NULL,
  `Supplier1` varchar(45) DEFAULT NULL,
  `ComponentLink1Description` varchar(45) DEFAULT NULL,
  `ComponentLink1URL` varchar(225) DEFAULT NULL,
  `ITEM` int(11) DEFAULT NULL,
  `PrePN` varchar(45) DEFAULT NULL,
  `PostPN` varchar(45) DEFAULT NULL,
  `TEMPCO` varchar(45) DEFAULT NULL,
  `Tolerance` varchar(45) DEFAULT NULL,
  `Value` varchar(45) DEFAULT NULL,
  `Height` varchar(45) DEFAULT NULL,
  `Size_Dimension` varchar(45) DEFAULT NULL,
  `Composition` varchar(45) DEFAULT NULL,
  `Number_of_Terminations` varchar(45) DEFAULT NULL,
  `Power_Watts` varchar(45) DEFAULT NULL,
  `Resistance_Ohms` varchar(45) DEFAULT NULL,
  `Operating_Temperature` varchar(45) DEFAULT NULL,
  `Package_Case` varchar(45) DEFAULT NULL,
  `Capacitance` varchar(45) DEFAULT NULL,
  `Thickness_Max` varchar(45) DEFAULT NULL,
  `Voltage_Rated` varchar(45) DEFAULT NULL,
  `Lead_Spacing` varchar(45) DEFAULT NULL,
  `RoHS` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(45) DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL,
  `Supplier_Device_Package` varchar(45) DEFAULT NULL,
  `Minimum_Orde_Quantity` varchar(45) DEFAULT NULL,
  `Reeling` varchar(45) DEFAULT NULL,
  `Product_Type` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `Reverse_Recovery_Time_trr` varchar(45) DEFAULT NULL,
  `ESR_Equivalent_Series_Resistance` varchar(45) DEFAULT NULL,
  `table_names` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

INSERT INTO new_part VALUES("10","100.0 - �1% - R0805","NEW000001-079","RES 100.0 - �1% - 0.05","Panasonic","ERJ-1GEF1000C","yellow","resistor","RES","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","R0805","","","Datasheet","http://datasheet.octopart.com/ERJ-1GEF1000C-Panasonic-datasheet-5313456.pdf","","1","79","","","100.0","","","","","","","","","","","","","","","","","","","","NEW001","Kirti Chandel","","","");
INSERT INTO new_part VALUES("11","1000K - �1% - R0805","NEW000001-078","RES 1000K - �1% - 0.05","Panasonic","ERJ-1GEF1004C","yellow","resistor","RES","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","R0805","","","Datasheet","http://datasheet.octopart.com/ERJ-1GEF1004C-Panasonic-datasheet-5313456.pdf","","1","78","","","1000K","","","","","","","","","","","","","","","","","","","","NEW001","Kirti Chandel","","","");
INSERT INTO new_part VALUES("12","40.2 - �1% - R0805","NEW000001-081","RES 40.2 - �1% - 0.05","Panasonic","ERJ-1GEF40R2C","yellow","resistor","RES","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","R0805","","","Datasheet","http://datasheet.octopart.com/ERJ-1GEF40R2C-Panasonic-datasheet-13266541.pdf","","1","81","","","40.2","","","","","","","","","","","","","","","","","","","","NEW001","Kirti Chandel","","","");
INSERT INTO new_part VALUES("13","0.0 - �5% - ","NEW000001-082","RES 0.0 - �5% - 0.125","Panasonic","ERJ-6GEY0R00V","yellow","resistor","RES","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","","","","Datasheet","http://datasheet.octopart.com/ERJ-6GEY0R00V-Panasonic-datasheet-13266872.pdf","","1","82","","","ZERO","","","","","","","","","","","","","","","","","","","","NEW001","Kirti Chandel","","","");
INSERT INTO new_part VALUES("14","20K - �0.1% - R0805","NEW000001-087","RES 20K - �0.1% - 0.063","Panasonic","ERA-3YEB203V","yellow","resistor","RES","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","R0805","","","Datasheet","http://datasheet.octopart.com/ERA-3YEB203V-Panasonic-datasheet-11719046.pdf","","1","87","","","20K","","","","","","","","","","","","","","","","","","","","NEW001","Kirti Chandel","","","");
INSERT INTO new_part VALUES("18","0.047 - �1% - R0805","","RES 0.047 - �1% - 0.125","Panasonic","ERJ-L06KF47MV","orange","resistor","","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","R0805","","","Datasheet","http://datasheet.octopart.com/ERJ-L06KF47MV-Panasonic-datasheet-13271236.pdf","","1","2","","","","","","","","","","","","","","","","","","","","","","","","Kirti Chandel","","","");
INSERT INTO new_part VALUES("19","�0.1% - R0805","","RES �0.1% - ","Stackpole Electronics","RNCS0805BKE10R0","orange","resistor","RES","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","R0805","","","Datasheet","http://datasheet.octopart.com/RNCS0805BKE10R0-Stackpole-Electronics-datasheet-12540111.pdf","","1","2","","","","","","","","","","","","","","","","","","","","","","","","Kirti Chandel","","","");
INSERT INTO new_part VALUES("20","R0612","","","Susumu","KRL1632E-M-R033-F-T5","orange","resistor","","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","R0612","","","Datasheet","http://datasheet.octopart.com/KRL1632E-M-R033-F-T5-Susumu-datasheet-17122291.pdf","","","","","","","","","","","","","","","","","","","","","","","","","","","Kirti Chandel","","","");
INSERT INTO new_part VALUES("21","0.04 - �1% - R1005","","","Vishay","WSL2512R0400FEA","orange","resistor","RES","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","R1005","","","Datasheet","http://datasheet.octopart.com/WSL2512R0400FEA-Vishay-datasheet-87136251.pdf","","","","","","","","","","","","","","","","","","","","","","","","","","","Kirti Chandel","","","");
INSERT INTO new_part VALUES("22","0.05 - �1% - R1005","","","Vishay","WSL2512R0500FEA","orange","resistor","RES","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","R1005","","","Datasheet","http://datasheet.octopart.com/WSL2512R0500FEA-Vishay-datasheet-8360116.pdf","","","","","","","","","","","","","","","","","","","","","","","","","","","Kirti Chandel","","","");
INSERT INTO new_part VALUES("23","0.03 - �1% - R1005","","","Vishay","WSL2512R0300FEA","orange","resistor","RES","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","R1005","","","Datasheet","http://datasheet.octopart.com/WSL2512R0300FEA-Vishay-datasheet-87136251.pdf","","","","","","","","","","","","","","","","","","","","","","","","","","","Kirti Chandel","","","");
INSERT INTO new_part VALUES("24","0.015 - �1% - R1005","N","N","Vishay","WSL2512R0150FEA","orange","resistor","RES","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","R1005","","","Datasheet","http://datasheet.octopart.com/WSL2512R0150FEA-Vishay-datasheet-87136251.pdf","","","","","","","","","","","","","","","","","","","","","","","","","","","Kirti Chandel","","","");
INSERT INTO new_part VALUES("25","WSHM2818R0600FEA","NEW000001-110","","Vishay","WSHM2818R0600FEA","orange","resistor","","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","","","","Datasheet","#","","","","","","","","","","","","","","","","","","","","","","","","","","","Kirti Chandel","","","");
INSERT INTO new_part VALUES("26","1K - �0.1% - R1206","","","Panasonic","ERA8ARB102V","orange","resistor","RES","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","R1206","","","Datasheet","http://datasheet.octopart.com/ERA8ARB102V-Panasonic-datasheet-46744048.pdf","","","","","","","","","","","","","","","","","","","","","","","","","","","Kirti Chandel","","","");
INSERT INTO new_part VALUES("28","�1% - ","NEW000001-113","RES �1% - ","Stackpole Electronics","CSS2725FTL250","orange","resistor","RES","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","","","","Datasheet","http://datasheet.octopart.com/CSS2725FTL250-Stackpole-Electronics-datasheet-12517701.pdf","","","","","","","","","","","","","","","","","","","","","","","","","","","Kirti Chandel","","","");
INSERT INTO new_part VALUES("29","10.0 - �1% - R2515","NEW000001-114","RES 10.0 - �1% - 1.0","Vishay","WSC251510R00FEA","orange","resistor","RES","NEW001.SchLib","","=Value","Standard","Standard","","","","","","","","","","","","NEW001.PcbLib","R2515","","","Datasheet","http://datasheet.octopart.com/WSC251510R00FEA-Vishay-datasheet-80324201.pdf","","","","","","","","","","","","","","","","","","","","","","","","","","","Kirti Chandel","","","");





DROP TABLE oscillator;

CREATE TABLE `oscillator` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(255) DEFAULT NULL,
  `Operating_Temperature` varchar(255) DEFAULT NULL,
  `Package_Case` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `Height` varchar(10) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO oscillator VALUES("1","CSTCE16M0V53-R0","NEW000008-001","RESONATOR","NEW001.SchLib","","=Value","Standard","Standard","RESONATOR, SMD, 16MHZ; Frequency:16MHz; Frequency Tolerance:� 0.5%; Frequency Stability:� 0.3%; Resonant Impedance:40ohm; Oscillator Mounting:SMD; Operating Temperature Range:-20�C to +80�C; SVHC:No SVHC (19-Dec-2011)","","","Murata","CSTCE16M0V53-R0","","","Surface Mount","-20�C to +80�C","3-SMD, Non-Standard","Tape & Reel ","3","","","","","NEW001.PcbLib","3-SMD","1.00mm","http://datasheet.octopart.com/CSTCE16M0V53-R0-Murata-datasheet-5335552.pdf","Datasheet","","","","NEW001","08.3.3","A","2018-02-18","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/CSTCE16M0V53-R0-PIC.jpg","partsymbol/RESONATOR-SCH.jpg","partfootprint/3-SMD-PCB.jpg","3dimg/3-SMD-3D.jpg");
INSERT INTO oscillator VALUES("2","ABM3B-8.000MHZ-B2-T","NEW000008-002","CRYSTAL4","NEW001.SchLib","","=Value","Standard","Standard","CRYSTAL, 8MHZ, 18PF, SMD; Frequency:8MHz; Load Capacitance:18pF; Frequency Tolerance:� 20ppm; Operating Temperature Range:-20�C to +70�C; Crystal Mounting Type:SMD; No. of Pins:4; Frequency Stability:50ppm ;RoHS Compliant: Yes","","","Abracon","ABM3B-8.000MHZ-B2-T","","","Surface Mount","-20�C to +70�C","4-SMD, No Lead","Tape and Reel","4","","","","","NEW001.PcbLib","4-SMD","1.1mm","http://datasheet.octopart.com/ABM3B-8.000MHZ-B2-T-Abracon-datasheet-5300259.pdf","Datasheet","","","","NEW001","Revised (O): 03.11.08","A","2018-02-18","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/ABM3B-8.000MHZ-B2-T-PIC.jpg","partsymbol/CRYSTAL4-SCH.jpg","partfootprint/4-SMD-PCB.jpg","3dimg/4-SMD-3D.jpg");
INSERT INTO oscillator VALUES("3","ABM3B-12.000MHZ-B2-T","NEW000008-003","CRYSTAL4","NEW001.SchLib","","=Value","Standard","Standard","CRYSTAL, 12M, 18PF CL, 5X3.2 SMT; Frequency:12MHz; Frequency Tolerance:� 50ppm; Load Capacitance:18pF; Operating Temperature Range:-20�C to +70�C; Crystal Mounting Type:SMD; Crystal Case Type:5mm x 3.2mm; No. of Pins:4; SVHC:No SVHC (20-Jun-2011); Accu","","","Abracon","ABM3B-12.000MHZ-B2-T","","","Surface Mount","-20�C to +70�C","4-SMD, No Lead","Tape and Reel","4","","","","","NEW001.PcbLib","4-SMD","1.1mm","http://datasheet.octopart.com/ABM3B-12.000MHZ-B2-T-Abracon-datasheet-5300259.pdf","Datasheet","","","","NEW001","Revised (O): 03.11.08","A","2018-02-18","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/ABM3B-12.000MHZ-B2-T-PIC.jpg","partsymbol/CRYSTAL4-SCH.jpg","partfootprint/4-SMD-PCB.jpg","3dimg/4-SMD-3D.jpg");
INSERT INTO oscillator VALUES("4","ABM3B-12.288MHZ-B2-T","NEW000008-004","CRYSTAL4","NEW001.SchLib","","=Value","Standard","Standard","MICROP. CRYSTAL, 12.288MHZ; Frequency:12.288MHz; Frequency Tolerance:� 20ppm; Load Capacitance:18pF; Frequency Stability:� 50ppm; Operating Temperature Range:-20�C to +70�C; Crystal Mounting Type:SMD; No. of Pins:4; SVHC:No SVHC (20-Jun-2011); Output ","","","Abracon","ABM3B-12.288MHZ-B2-T","","","Surface Mount","-20�C to +70�C","4-SMD, No Lead","Tape and Reel","4","","","","","NEW001.PcbLib","4-SMD","1.1mm","http://datasheet.octopart.com/ABM3B-12.288MHZ-B2-T-Abracon-datasheet-5300259.pdf","Datasheet","","","","NEW001","Revised (O): 03.11.08","A","2018-02-18","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/ABM3B-12.288MHZ-B2-T-PIC.jpg","partsymbol/CRYSTAL4-SCH.jpg","partfootprint/4-SMD-PCB.jpg","3dimg/4-SMD-3D.jpg");
INSERT INTO oscillator VALUES("5","ABM3B-16.000MHZ-B2-T","NEW000008-005","CRYSTAL4","NEW001.SchLib","","=Value","Standard","Standard","CRYSTAL, 16M, 18PF CL, 5X3.2 SMT; Frequency:16MHz; Frequency Tolerance:� 50ppm; Load Capacitance:18pF; Operating Temperature Range:-20�C to +70�C; Crystal Mounting Type:SMD; Crystal Case Type:5mm x 3.2mm; No. of Pins:4; SVHC:No SVHC (20-Jun-2011); Accu","","","Abracon","ABM3B-16.000MHZ-B2-T","","","Surface Mount","-20�C to +70�C","4-SMD, No Lead","Tape and Reel","4","","","","","NEW001.PcbLib","4-SMD","1.1mm","http://datasheet.octopart.com/ABM3B-16.000MHZ-B2-T-Abracon-datasheet-5300259.pdf","Datasheet","","","","NEW001","Revised (O): 03.11.08","A","2018-02-18","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/ABM3B-16.000MHZ-B2-T-PIC.jpg","partsymbol/CRYSTAL4-SCH.jpg","partfootprint/4-SMD-PCB.jpg","3dimg/4-SMD-3D.jpg");
INSERT INTO oscillator VALUES("6","ABM3B-20.000MHZ-B2-T","NEW000008-006","CRYSTAL4","NEW001.SchLib","","=Value","Standard","Standard","CRYSTAL, 20M, 18PF CL, 5X3.2 SMT; Frequency:20MHz; Frequency Tolerance:� 50ppm; Load Capacitance:18pF; Operating Temperature Range:-20�C to +70�C; Crystal Mounting Type:SMD; Crystal Case Type:5mm x 3.2mm; No. of Pins:4; SVHC:No SVHC (20-Jun-2011); Accu","","","Abracon","ABM3B-20.000MHZ-B2-T","","","Surface Mount","-20�C to +70�C","4-SMD, No Lead","Tape and Reel","4","","","","","NEW001.PcbLib","4-SMD","1.1mm","http://datasheet.octopart.com/ABM3B-20.000MHZ-B2-T-Abracon-datasheet-5300259.pdf","Datasheet","","","","NEW001","Revised (O): 03.11.08","A","2018-02-18","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/ABM3B-20.000MHZ-B2-T-PIC.jpg","partsymbol/CRYSTAL4-SCH.jpg","partfootprint/4-SMD-PCB.jpg","3dimg/4-SMD-3D.jpg");
INSERT INTO oscillator VALUES("7","ABM3B-24.000MHZ-B2-T","NEW000008-007","CRYSTAL4","NEW001.SchLib","","=Value","Standard","Standard","Crystal; Frequency:24MHz; Frequency Tolerance:� 20ppm; Operating Temperature Range:-20�C to +70�C; Load Capacitance:18pF; Crystal Case:Ceramic; Crystal Terminals:Surface Mount (SMD, SMT); Crystal Type:Standard; ESR:50ohm ;RoHS Compliant: Yes","","","Abracon","ABM3B-24.000MHZ-B2-T","","","Surface Mount","-20�C to +70�C","4-SMD, No Lead","Tape and Reel","4","","","","","NEW001.PcbLib","4-SMD","1.1mm","http://datasheet.octopart.com/ABM3B-24.000MHZ-B2-T-Abracon-datasheet-5300259.pdf","Datasheet","","","","NEW001","Revised (O): 03.11.08","A","2018-02-18","green","Compliance","Not Listed by Manufacturer","","Lead Free","partimg/ABM3B-24.000MHZ-B2-T-PIC.jpg","partsymbol/CRYSTAL4-SCH.jpg","partfootprint/4-SMD-PCB.jpg","3dimg/4-SMD-3D.jpg");





DROP TABLE parts_log;

CREATE TABLE `parts_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_name` varchar(225) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `user` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE resistor;

CREATE TABLE `resistor` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `PrePN` int(11) DEFAULT NULL,
  `PostPN` int(11) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `TEMPCO` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Tolerance` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Height` varchar(255) DEFAULT NULL,
  `Size_Dimension` varchar(255) DEFAULT NULL,
  `Composition` varchar(255) DEFAULT NULL,
  `Number_of_Terminations` varchar(255) DEFAULT NULL,
  `Power_Watts` varchar(255) DEFAULT NULL,
  `Resistance_Ohms` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;

INSERT INTO resistor VALUES("2","10.0 - �1% - 0603","RES","NEW000001-002","1","2","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 10.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF10R0V","","","Taping","","","","","","","","�1 %","NEW001.PcbLib","R0603","10.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","10.0","Datasheet","http://datasheet.octopart.com/ERJ3EKF10R0V-Panasonic-datasheet-5313456.pdf","","NEW001","Unknown","A","2018-02-18","green","","Compliant","Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF10R0V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("3","12.0 - �1% - 0603","RES","NEW000001-003","1","3","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 12.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF12R0V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","12.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","12.0","Datasheet","http://datasheet.octopart.com/ERJ3EKF12R0V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF12R0V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("4","15.0 - �1% - 0603","RES","NEW000001-004","1","4","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 15.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF15R0V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","15.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","15.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF15R0V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF15R0V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("5","22.0 - �1% - 0603","RES","NEW000001-005","1","5","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 22.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF22R0V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","22.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","22.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF22R0V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF22R0V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("6","33.2 - �1% - 0603","RES","NEW000001-006","1","6","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 33.2 - �1% - 0.1","","","Panasonic","ERJ-3EKF33R2V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","33.2","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","33.2","Datasheet","http://datasheet.octopart.com/ERJ-3EKF33R2V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF33R2V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("7","47.0 - �1% - 0603","RES","NEW000001-007","1","7","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 47.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF47R0V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","47.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","47.0","Datasheet","http://datasheet.octopart.com/ERJ3EKF47R0V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF47R0V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("8","49.9 - �1% - 0603","RES","NEW000001-008","1","8","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 49.9 - �1% - 0.1","","","Panasonic","ERJ-3EKF49R9V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","49.9","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","49.9","Datasheet","http://datasheet.octopart.com/ERJ-3EKF49R9V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF49R9V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("9","56.2 - �1% - 0603","RES","NEW000001-009","1","9","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 56.2 - �1% - 0.1","","","Panasonic","ERJ-3EKF56R2V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","56.2","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","56.2","Datasheet","http://datasheet.octopart.com/ERJ-3EKF56R2V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF56R2V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("10","68.1 - �1% - 0603","RES","NEW000001-010","1","10","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 68.1 - �1% - 0.1","","","Panasonic","ERJ-3EKF68R1V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","68.1","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","68.1","Datasheet","http://datasheet.octopart.com/ERJ-3EKF68R1V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF68R1V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("11","75.0 - �1% - 0603","RES","NEW000001-011","1","11","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 75.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF75R0V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","75.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","75.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF75R0V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF75R0V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("12","82.5 - �1% - 0603","RES","NEW000001-012","1","12","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 82.5 - �1% - 0.1","","","Panasonic","ERJ-3EKF82R5V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","82.5","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","82.5","Datasheet","http://datasheet.octopart.com/ERJ-3EKF82R5V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF82R5V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("13","100.0 - �1% - 0603","RES","NEW000001-013","1","13","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 100.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF1000V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","100.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","100.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF1000V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1000V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("14","120.0 - �1% - 0603","RES","NEW000001-014","1","14","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 120.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF1200V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","120.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","120.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF1200V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1200V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("15","150.0 - �1% - 0603","RES","NEW000001-015","1","15","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 150.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF1500V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","150.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","150.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF1500V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1500V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("16","220.0 - �1% - 0603","RES","NEW000001-016","1","16","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 220.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF2200V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","220.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","220.0","Datasheet","http://datasheet.octopart.com/ERJ3EKF2200V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF2200V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("17","390.0 - �1% - 0603","RES","NEW000001-017","1","17","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 390.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF3900V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","390.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","390.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF3900V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF3900V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("18","470.0 - �1% - 0603","RES","NEW000001-018","1","18","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 470.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF4700V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","470.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","470.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF4700V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF4700V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("19","560.0 - �1% - 0603","RES","NEW000001-019","1","19","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 560.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF5600V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","560.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","560.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF5600V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF5600V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("20","681.0 - �1% - 0603","RES","NEW000001-020","1","20","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 681.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF6810V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","681.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","681.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF6810V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF6810V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("21","820.0 - �1% - 0603","RES","NEW000001-021","1","21","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 820.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF8200V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","820.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","820.0","Datasheet","http://datasheet.octopart.com/ERJ3EKF8200V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012 ","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF8200V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("22","1K - �1% - 0603","RES","NEW000001-022","1","22","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 1K - �1% - 0.1","","","Panasonic","ERJ-3EKF1001V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","1K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","1K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1001V-Panasonic-datasheet-5313456.pdf","","NEW001","Oct. 2008","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1001V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("23","1.2K - �1% - 0603","RES","NEW000001-023","1","23","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 1.2K - �1% - 0.1","","","Panasonic","ERJ-3EKF1201V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","1.2K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","1.2K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1201V-Panasonic-datasheet-13266541.pdf","","NEW001","03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1201V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("24","1.5K - �1% - 0603","RES","NEW000001-024","1","24","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 1.5K - �1% - 0.1","","","Panasonic","ERJ-3EKF1501V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","1.5K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","1.5K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF1501V-Panasonic-datasheet-13266541.pdf","","NEW001","03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1501V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("25","2K - �1% - 0603","RES","NEW000001-025","1","25","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 2K - �1% - 0.1","","","Panasonic","ERJ-3EKF2001V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","2K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","2K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF2001V-Panasonic-datasheet-13266541.pdf","","NEW001","03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF2001V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("26","3.6K - �1% - 0603","RES","NEW000001-026","1","26","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 3.6K - �1% - 0.1","","","Panasonic","ERJ-3EKF3601V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","3.6K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","3.6K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF3601V-Panasonic-datasheet-13293574.pdf","","NEW001","03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF3601V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("27","4.7K - �1% - 0603","RES","NEW000001-027","1","27","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 4.7K - �1% - 0.1","","","Panasonic","ERJ-3EKF4701V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","4.7K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","4.7K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF4701V-Panasonic-datasheet-5313456.pdf","","NEW001","Oct. 2008","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF4701V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("28","680.0 - �1% - 0603","RES","NEW000001-028","1","28","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 680.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF6800V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","680.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","680.0","Datasheet","http://datasheet.octopart.com/ERJ3EKF6800V-Panasonic-datasheet-13266541.pdf","","NEW001","03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF6800V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("29","10K - �1% - 0603","RES","NEW000001-029","1","29","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 10K - �1% - 0.1","","","Panasonic","ERJ-3EKF1002V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","10K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","10K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1002V-Panasonic-datasheet-5313456.pdf","","NEW001","Oct. 2008","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1002V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("30","12K - �1% - 0603","RES","NEW000001-030","1","30","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 12K - �1% - 0.1","","","Panasonic","ERJ-3EKF1202V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","12K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","12K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1202V-Panasonic-datasheet-13266541.pdf","","NEW001","03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1202V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("31","15K - �1% - 0603","RES","NEW000001-031","1","31","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 15K - �1% - 0.1","","","Panasonic","ERJ-3EKF1502V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","15K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","15K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1502V-Panasonic-datasheet-5313456.pdf","","NEW001","Oct. 2008","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1502V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("32","22K - �1% - 0603","RES","NEW000001-032","1","32","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 22K - �1% - 0.1","","","Panasonic","ERJ-3EKF2202V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","22K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","22K","Datasheet","http://datasheet.octopart.com/ERJ3EKF2202V-Panasonic-datasheet-13266541.pdf","","NEW001","03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF2202V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("33","33.2K - �1% - 0603","RES","NEW000001-033","1","33","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 33.2K - �1% - 0.1","","","Panasonic","ERJ-3EKF3322V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","33.2K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","33.2K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF3322V-Panasonic-datasheet-13266541.pdf","","NEW001","03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF3322V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("34","47K - �1% - 0603","RES","NEW000001-034","1","34","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 47K - �1% - 0.1","","","Panasonic","ERJ-3EKF4702V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","47K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","47K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF4702V-Panasonic-datasheet-5313456.pdf","","NEW001","Oct. 2008","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF4702V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("35","56.2K - �1% - 0603","RES","NEW000001-035","1","35","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 56.2K - �1% - 0.1","","","Panasonic","ERJ-3EKF5622V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","56.2K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","56.2K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF5622V-Panasonic-datasheet-13266541.pdf","","NEW001","03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF5622V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("36","68.1K - �1% - 0603","RES","NEW000001-036","1","36","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 68.1K - �1% - 0.1","","","Panasonic","ERJ-3EKF6812V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","68.1K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","68.1K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF6812V-Panasonic---ECG-datasheet-13266541.pdf","","NEW001","03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF6812V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("37","82.5K - �1% - 0603","RES","NEW000001-037","1","37","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 82.5K - �1% - 0.1","","","Panasonic","ERJ-3EKF8252V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","82.5K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","82.5K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF8252V-Panasonic-datasheet-13266541.pdf","","NEW001","03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF8252V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("38","100K - �1% - 0603","RES","NEW000001-038","1","38","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 100K - �1% - 0.1","","","Panasonic","ERJ-3EKF1003V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","100K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","100K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1003V-Panasonic-datasheet-5313456.pdf","","NEW001","Oct. 2008","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1003V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("39","120K - �1% - 0603","RES","NEW000001-039","1","39","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 120K - �1% - 0.1","","","Panasonic","ERJ-3EKF1203V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","120K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","120K","Datasheet","http://datasheet.octopart.com/ERJ3EKF1203V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1203V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("40","150K - �1% - 0603","RES","NEW000001-040","1","40","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 150K - �1% - 0.1","","","Panasonic","ERJ-3EKF1503V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","150K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","150K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF1503V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1503V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("41","220K - �1% - 0603","RES","NEW000001-041","1","41","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 220K - �1% - 0.1","","","Panasonic","ERJ-3EKF2203V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","220K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","220K","Datasheet","http://datasheet.octopart.com/ERJ3EKF2203V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF2203V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("42","332K - �1% - 0603","RES","NEW000001-042","1","42","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 332K - �1% - 0.1","","","Panasonic","ERJ-3EKF3323V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","332K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","332K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF3323V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF3323V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("43","470K - �1% - 0603","RES","NEW000001-043","1","43","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 470K - �1% - 0.1","","","Panasonic","ERJ-3EKF4703V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","470K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","470K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF4703V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF4703V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("44","562K - �1% - 0603","RES","NEW000001-044","1","44","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 562K - �1% - 0.1","","","Panasonic","ERJ-3EKF5623V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","562K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","562K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF5623V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF5623V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("45","681K - �1% - 0603","RES","NEW000001-045","1","45","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 681K - �1% - 0.1","","","Panasonic","ERJ-3EKF6813V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","681K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","681K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF6813V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF6813V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("46","825K - �1% - 0603","RES","NEW000001-046","1","46","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 825K - �1% - 0.1","","","Panasonic","ERJ-3EKF8253V","","","Taping","","","","","","","","�1%","NEW001.PcbLib","R0603","825K","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","825K","Datasheet","http://datasheet.octopart.com/ERJ-3EKF8253V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF8253V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("47","1M - �1% - 0603","RES","NEW000001-047","1","47","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 1M - �1% - 0.1","","","Panasonic","ERJ-3EKF1004V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","1M","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","1M","Datasheet","http://datasheet.octopart.com/ERJ-3EKF1004V-Panasonic-datasheet-13266541.pdf","","NEW001"," 03-AUG-2012","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF1004V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("48","10K - �25% - TC33","POT","NEW000001-048","1","48","NEW001.SchLib","","=Value","Standard","Standard","�250ppm/�C","RES 10K - �25% - 0.1","","","Bourns","TC33X-2-103E","",""," Reel","","","","","","","","�25 %","NEW001.PcbLib","TC33","10K","","3.80mm x 3.60mm x 1.20mm","","Single","0.1W","10K","Datasheet","http://datasheet.octopart.com/TC33X-2-103E-Bourns-datasheet-26822215.pdf","","NEW001","REV. 04/14","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/TC33X-2-103E-PIC.jpg","partsymbol/POT-SCH.jpg","partfootprint/TC33-PCB.jpg","3dimg/TC33-3D.jpg");
INSERT INTO resistor VALUES("49","330.0 - �1% - 0603","RES","NEW000001-049","1","49","NEW001.SchLib","","=Value","Standard","Standard","�100ppm/�C","RES 330.0 - �1% - 0.1","","","Panasonic","ERJ-3EKF3300V","","","Taping","2","","","","","","","�1%","NEW001.PcbLib","R0603","330.0","0.55mm","1.60mm x 0.80mm","Thick Film","2","0.1W","330.0","Datasheet","http://datasheet.octopart.com/ERJ-3EKF3300V-Panasonic-datasheet-5313456.pdf","","NEW001","Oct. 2008","A","2018-02-18","green","","Compliant"," Not Listed by Manufacturer","Lead Free","partimg/ERJ-3EKF3300V-PIC.jpg","partsymbol/RES-SCH.jpg","partfootprint/R0603-PCB.jpg","3dimg/R0603-3D.jpg");
INSERT INTO resistor VALUES("52","22.0 - 5% - ","RES","NEW000001-050","1","50","NEW001.SchLib","","=Value","Standard","Standard","","RES 22.0 - �5% - 0.1","","","Panasonic","ERJ-3GEYJ220V","","","","","","","","","","","","NEW001.PcbLib","R0603","22.0","","","","","","22.0","Datasheet","http://datasheet.octopart.com/ERJ-3GEYJ220V-Panasonic-datasheet-61114972.pdf","","NEW001","unknown","O","2018-02-18","yellow","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("53","5K - 0.01% - ","RES","NEW000001-053","1","53","NEW001.SchLib","","=Value","Standard","Standard","","RES 5K - �0.01% - 0.2","","","Vishay","Y16245K00000T9R","","","","","","","","","","","","NEW001.PcbLib","R0603","5K","","","","","","5K","Datasheet","http://datasheet.octopart.com/Y16245K00000T9R-Vishay-datasheet-7548670.pdf","","NEW001","2009-04-01T00:01:01Z","O","2018-02-18","yellow","","Compliant","","","","","","");
INSERT INTO resistor VALUES("54","91K - 0.1% - 0603","RES","NEW000001-054","1","54","NEW001.SchLib","","=Value","Standard","Standard","","RES 91K - �0.1% - 0.1","","","Yageo","RT0603BRD0791KL","","","","","","","","","","","","NEW001.PcbLib","R0603","91K","","","","","","91K","Datasheet","http://datasheet.octopart.com/RT0603BRD0791KL-Yageo-datasheet-96443642.pdf","","NEW001","2017-09-12T06:49:18Z","O","2018-02-18","yellow","","Compliant","","","","","","");
INSERT INTO resistor VALUES("55","22K - 0.1% - ","RES","NEW000001-055","1","55","NEW001.SchLib","","=Value","Standard","Standard","","RES 22K - �0.1% - 0.1","","","Panasonic","ERA-3AEB223V","","","","","","","","","","","","NEW001.PcbLib","","22K","","","","","","22K","Datasheet","http://datasheet.octopart.com/ERA-3AEB223V-Panasonic-datasheet-62287448.pdf","","NEW001","2016-04-11T06:49:32Z","O","2018-02-18","yellow","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("56","56K - 0.1% - ","RES","NEW000001-056","1","56","NEW001.SchLib","","=Value","Standard","Standard","","RES 56K - �0.1% - 0.063","","","Panasonic","ERA-2AEB563X","","","","","","","","","","","","NEW001.PcbLib","R0805","56K","","","","","","56K","Datasheet","http://datasheet.octopart.com/ERA-2AEB563X-Panasonic-datasheet-13266200.pdf","","NEW001","unknown","O","2018-02-19","yellow","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("58","1K - �5% - ","","NEW000001-058","","","NEW001SchLib","","=Value","Standard","Standard","","RES 1K - �5% - 0.25","","","Stackpole Electronics","CF14JT1K00","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/CF14JT1K00-Stackpole-Electronics-datasheet-12509918.pdf","","NEW001","2012-10-26T15:59:26Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("59","CFR-25JR-52-10K","","NEW000001-059","","","NEW001SchLib","","=Value","Standard","Standard","","RES 10K - �5% - 0.25","","","Yageo","CFR-25JR-52-10K","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/CFR-25JR-52-10K-Yageo-datasheet-12509909.pdf","","NEW001","2010-12-31T08:31:20Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("60","0.0 - �5% - R0402","","NEW000001-060","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.0 - �5% - 0.0625","","","Yageo","RC0402JR-070RL","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/RC0402JR-070RL-Yageo-datasheet-10408619.pdf","","NEW001","2011-01-06T01:55:34Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("61","R0402","","NEW000001-061","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.0625","","","Samsung","RC1005J000CS","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/RC1005J000CS-Samsung-Electro-Mechanics-datasheet-81440.pdf","","NEW001","2006-01-28T11:58:39Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("62","0.0 - �5% - R0603","","NEW000001-062","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.0 - �5% - 0.1","","","Yageo","RC0603JR-070RL","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/RC0603JR-070RL-Yageo-datasheet-8328172.pdf","","NEW001","2009-04-24T02:31:26Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("63","0.0 - �1% - R0402","","NEW000001-063","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.0 - �1% - ","","","Yageo","RC0402FR-070RL","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/RC0402FR-070RL-Yageo-datasheet-8324527.pdf","","NEW001","2008-07-18T01:47:51Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("66","0.0 - R0805","RES","NEW000001-066","1","66","NEW001.SchLib","","=Value","Standard","Standard","","RES 0.0 - 0.05","","","Panasonic","ERJ-1GN0R00C","","","","","","","","","","","","NEW001.PcbLib","R0805","ZERO","","","","","","ZERO","Datasheet","http://datasheet.octopart.com/ERJ-1GN0R00C-Panasonic-datasheet-5314550.pdf","","NEW001","2008-08-07T02:57:13Z","A","2018-02-24","green","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("68","0.0 - �5% - R0201","","NEW000001-068","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.0 - �5% - 0.05","","","Panasonic","ERJ-1GE0R00C","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/ERJ-1GE0R00C-Panasonic-datasheet-13266872.pdf","","NEW001","unknown","A","2014-12-08","orange","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("69","0.0 - �5% - R0201","","NEW000001-069","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.0 - �5% - 0.5","","","Yageo","RC0201JR-070RL","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/RC0201JR-070RL-Yageo-datasheet-98239111.pdf","","NEW001","2017-07-10T03:35:06Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("70","0.0 - �1% - R0201","","NEW000001-070","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.0 - �1% - ","","","Yageo","RC0201FR-070RL","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/RC0201FR-070RL-Yageo-datasheet-98239111.pdf","","NEW001","2017-07-10T03:35:06Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("71","0.0 - �5% - R0603","","NEW000001-071","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.0 - �5% - 0.1","","","Panasonic","ERJ-3GEY0R00V","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/ERJ-3GEY0R00V-Panasonic-datasheet-13266872.pdf","","NEW001","unknown","O","2014-12-08","orange","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("72","1K - �1% - R0201","","NEW000001-072","","","NEW001SchLib","","=Value","Standard","Standard","","RES 1K - �1% - 0.05","","","Yageo","RC0201FR-071KL","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/RC0201FR-071KL-Yageo-datasheet-98239111.pdf","","NEW001","2017-07-10T03:35:06Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("73","100K - �1% - R0201","","NEW000001-073","","","NEW001SchLib","","=Value","Standard","Standard","","RES 100K - �1% - 0.5","","","Yageo","RC0201FR-07100KL","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/RC0201FR-07100KL-Yageo-datasheet-98239111.pdf","","NEW001","2017-07-10T03:35:06Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("74","100K - �1% - R0201","","NEW000001-074","","","NEW001SchLib","","=Value","Standard","Standard","","RES 100K - �1% - 0.05","","","Panasonic","ERJ-1GEF1003C","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/ERJ-1GEF1003C-Panasonic-datasheet-5313456.pdf","","NEW001","2008-10-30T05:36:34Z","O","2014-12-08","orange","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("75","4.7K - �1% - R0805","RES","NEW000001-068","1","68","NEW001.SchLib","","=Value","Standard","Standard","","RES 4.7K - �1% - 0.05","","","Panasonic","ERJ-1GEF4701C","","","","","","","","","","","","NEW001.PcbLib","R0805","4.7K","","","","","","4.7K","Datasheet","http://datasheet.octopart.com/ERJ-1GEF4701C-Panasonic-datasheet-5313456.pdf","","NEW001","2008-10-30T05:36:34Z","A","2018-02-24","green","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("77","200K - �1% - R0805","RES","NEW000001-077","1","77","NEW001.SchLib","","=Value","Standard","Standard","","RES 200K - �1% - 0.05","","","Panasonic","ERJ-1GEF2003C","","","","","","","","","","","","NEW001.PcbLib","R0805","200K","","","","","","200K","Datasheet","http://datasheet.octopart.com/ERJ-1GEF2003C-Panasonic-datasheet-5313456.pdf","","NEW001","2008-10-30T05:36:34Z","A","2018-02-24","green","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("78","49.9 - �1% - R0201","","NEW000001-078","","","NEW001SchLib","","=Value","Standard","Standard","","RES 49.9 - �1% - 0.05","","","Panasonic","ERJ-1GEF49R9C","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/ERJ-1GEF49R9C-Panasonic-datasheet-13266872.pdf","","NEW001","unknown","O","2014-12-08","orange","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("79","100.0 - 1% - R0805","RES","NEW000001-079","1","79","NEW001.SchLib","","=Value","Standard","Standard","","RES 100.0 - �1% - 0.05","","","Panasonic","ERJ-1GEF1000C","","","","","","","","","","","","NEW001.PcbLib","R0805","100.0","","","","","","100.0","Datasheet","http://datasheet.octopart.com/ERJ-1GEF1000C-Panasonic-datasheet-5313456.pdf","","NEW001","2008-10-30T05:36:34Z","O","2018-02-24","yellow","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("80","1000K - 1% - R0805","RES","NEW000001-078","1","78","NEW001.SchLib","","=Value","Standard","Standard","","RES 1000K - �1% - 0.05","","","Panasonic","ERJ-1GEF1004C","","","","","","","","","","","","NEW001.PcbLib","R0805","1000K","","","","","","1000K","Datasheet","http://datasheet.octopart.com/ERJ-1GEF1004C-Panasonic-datasheet-5313456.pdf","","NEW001","2008-10-30T05:36:34Z","O","2018-02-24","yellow","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("81","40.2 - 1% - R0805","RES","NEW000001-081","1","81","NEW001.SchLib","","=Value","Standard","Standard","","RES 40.2 - �1% - 0.05","","","Panasonic","ERJ-1GEF40R2C","","","","","","","","","","","","NEW001.PcbLib","R0805","40.2","","","","","","40.2","Datasheet","http://datasheet.octopart.com/ERJ-1GEF40R2C-Panasonic-datasheet-13266541.pdf","","NEW001","unknown","O","2018-02-24","yellow","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("82","0.0 - 5% - ","RES","NEW000001-082","1","82","NEW001.SchLib","","=Value","Standard","Standard","","RES 0.0 - �5% - 0.125","","","Panasonic","ERJ-6GEY0R00V","","","","","","","","","","","","NEW001.PcbLib","","ZERO","","","","","","ZERO","Datasheet","http://datasheet.octopart.com/ERJ-6GEY0R00V-Panasonic-datasheet-13266872.pdf","","NEW001","unknown","O","2018-02-24","yellow","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("83","10K - �0.5% - R0402","","NEW000001-083","","","NEW001SchLib","","=Value","Standard","Standard","","RES 10K - �0.5% - 0.063","","","Susumu","RR0510P-103-D","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/RR0510P-103-D-Susumu-datasheet-5302462.pdf","","NEW001","2008-10-23T23:58:17Z","A","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("84","1K - �0.5% - R0402","","NEW000001-084","","","NEW001SchLib","","=Value","Standard","Standard","","RES 1K - �0.5% - 0.063","","","Susumu","RR0510P-102-D","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/RR0510P-102-D-Susumu-datasheet-5302462.pdf","","NEW001","2008-10-23T23:58:17Z","A","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("85","10K - �1% - R0201","","NEW000001-085","","","NEW001SchLib","","=Value","Standard","Standard","","RES 10K - �1% - 0.05","","","Vishay","CRCW020110K0FNED","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/CRCW020110K0FNED-Vishay-datasheet-21268116.pdf","","NEW001","2013-04-19T02:35:33Z","A","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("86","10K - �0.5% - R2012","","NEW000001-086","","","NEW001SchLib","","=Value","Standard","Standard","","RES 10K - �0.5% - 0.1","","","Susumu","RR1220P-103-D","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/RR1220P-103-D-Susumu-datasheet-5302462.pdf","","NEW001","2008-10-23T23:58:17Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("87","20K - 0.1% - R0805","RES","NEW000001-087","1","87","NEW001.SchLib","","=Value","Standard","Standard","","RES 20K - �0.1% - 0.063","","","Panasonic","ERA-3YEB203V","","","","","","","","","","","","NEW001.PcbLib","R0805","20K","","","","","","20K","Datasheet","http://datasheet.octopart.com/ERA-3YEB203V-Panasonic-datasheet-11719046.pdf","","NEW001","2008-03-26T02:25:18Z","O","2018-02-24","yellow","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("88","15K - �0.1% - R0603","","NEW000001-088","","","NEW001SchLib","","=Value","Standard","Standard","","RES 15K - �0.1% - 0.063","","","Panasonic","ERA-3YEB153V","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/ERA-3YEB153V-Panasonic-datasheet-11719046.pdf","","NEW001","2008-03-26T02:25:18Z","O","2014-12-08","orange","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("89","2.2K - �0.1% - R0603","","NEW000001-089","","","NEW001SchLib","","=Value","Standard","Standard","","RES 2.2K - �0.1% - 0.063","","","Panasonic","ERA-3YEB222V","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/ERA-3YEB222V-Panasonic-datasheet-11544494.pdf","","NEW001","2011-03-30T02:15:20Z","O","2014-12-08","orange","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("90","30K - �0.1% - R0603","","NEW000001-090","","","NEW001SchLib","","=Value","Standard","Standard","","RES 30K - �0.1% - 0.063","","","Panasonic","ERA-3YEB303V","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/ERA-3YEB303V-Panasonic-datasheet-11544494.pdf","","NEW001","2011-03-30T02:15:20Z","O","2014-12-08","orange","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("91","R0508","","NEW000001-091","","","NEW001SchLib","","=Value","Standard","Standard","","","","","Susumu","KRL1220E-M-R010-F-T5","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/KRL1220E-M-R010-F-T5-Susumu-datasheet-17122291.pdf","","NEW001","2012-10-18T20:27:59Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("92","R0805","","NEW000001-092","","","NEW001SchLib","","=Value","Standard","Standard","","","","","Susumu","KRL1220E-M-R100-F-T5","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/KRL1220E-M-R100-F-T5-Susumu-datasheet-17122291.pdf","","NEW001","2012-10-18T20:27:59Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("93","R0805","","NEW000001-093","","","NEW001SchLib","","=Value","Standard","Standard","","","","","Susumu","KRL1220E-M-R012-F-T5","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","#","","NEW001","unknown","O","2014-12-08","orange","","","","","","","","");
INSERT INTO resistor VALUES("94","R0402","","NEW000001-094","","","NEW001SchLib","","=Value","Standard","Standard","","","","","Stackpole Electronics","HCJ0402ZT0R00","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/HCJ0402ZT0R00-Stackpole-Electronics-datasheet-45185688.pdf","","NEW001","2015-05-11T16:31:52Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("95","0.1 - �1% - R3216","","NEW000001-095","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.1 - �1% - 0.5","","","Bourns","CRM1206-FX-R100ELF","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/CRM1206-FX-R100ELF-Bourns-datasheet-21196278.pdf","","NEW001","2013-01-25T16:58:46Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("96","0.1 - �1% - R0805","","NEW000001-096","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.1 - �1% - ","","","Yageo","PT0805FR-7W0R1L","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/PT0805FR-7W0R1L-Yageo-datasheet-14431055.pdf","","NEW001","2011-04-07T01:00:54Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("97","10K - �0.1% - R0603","","NEW000001-097","","","NEW001SchLib","","=Value","Standard","Standard","","RES 10K - �0.1% - 0.1","","","Bourns","CRT0603-BY-1002ELF","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/CRT0603-BY-1002ELF-Bourns-datasheet-17018289.pdf","","NEW001","2013-04-16T17:51:58Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("98","10.0 - �0.1% - R0603","","NEW000001-098","","","NEW001SchLib","","=Value","Standard","Standard","","RES 10.0 - �0.1% - 0.1","","","Bourns","CRT0603-BY-10R0ELF","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/CRT0603-BY-10R0ELF-Bourns-datasheet-8360809.pdf","","NEW001","2009-03-12T15:39:30Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("99","�1% - ","","NEW000001-099","","","NEW001SchLib","","=Value","Standard","Standard","","RES �1% - ","","","Stackpole Electronics","CSRN2512FKR680","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/CSRN2512FKR680-Stackpole-Electronics-datasheet-12522144.pdf","","NEW001","2012-09-19T12:59:13Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("100","0.047 - �1% - R0805","","NEW000001-100","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.047 - �1% - 0.125","","","Panasonic","ERJ-L06KF47MV","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/ERJ-L06KF47MV-Panasonic-datasheet-13271236.pdf","","NEW001","unknown","O","2014-12-08","orange","","Compliant","Not Listed by Manufacturer","","","","","");
INSERT INTO resistor VALUES("101","ERJ-L06KF10CV","","NEW000001-101","","","NEW001SchLib","","","","","","","","","","ERJ-L06KF10CV","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","","","","","","","","orange","","","","","","","","");
INSERT INTO resistor VALUES("102","ERJ-B2CFR02V","","NEW000001-102","","","NEW001SchLib","","","","","","","","","","ERJ-B2CFR02V","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","","","","","","","","orange","","","","","","","","");
INSERT INTO resistor VALUES("103","�0.1% - R0805","","NEW000001-103","","","NEW001SchLib","","=Value","Standard","Standard","","RES �0.1% - ","","","Stackpole Electronics","RNCS0805BKE10R0","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/RNCS0805BKE10R0-Stackpole-Electronics-datasheet-12540111.pdf","","NEW001","2012-08-01T12:01:25Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("105","0.04 - �1% - R1005","","NEW000001-104","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.04 - �1% - 1.0","","","Vishay","WSL2512R0400FEA","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/WSL2512R0400FEA-Vishay-datasheet-87136251.pdf","","NEW001","2017-07-18T03:10:53Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("106","0.05 - �1% - R1005","","NEW000001-106","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.05 - �1% - 1.0","","","Vishay","WSL2512R0500FEA","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/WSL2512R0500FEA-Vishay-datasheet-8360116.pdf","","NEW001","2009-09-25T16:31:08Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("107","0.03 - �1% - R1005","","NEW000001-107","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.03 - �1% - 1.0","","","Vishay","WSL2512R0300FEA","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/WSL2512R0300FEA-Vishay-datasheet-87136251.pdf","","NEW001","2017-07-18T03:10:53Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("108","0.015 - �1% - R1005","","NEW000001-108","","","NEW001SchLib","","=Value","Standard","Standard","","RES 0.015 - �1% - 1.0","","","Vishay","WSL2512R0150FEA","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/WSL2512R0150FEA-Vishay-datasheet-87136251.pdf","","NEW001","2017-07-18T03:10:53Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("109","WSHM2818R0150FEA","","NEW000001-109","","","NEW001SchLib","","=Value","Standard","Standard","","","","","Vishay","WSHM2818R0150FEA","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/WSHM2818R0150FEA-Vishay-datasheet-79543305.pdf","","NEW001","2017-03-24T03:11:31Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("110","WSHM2818R0600FEA","","NEW000001-110","","","NEW001SchLib","","=Value","Standard","Standard","","","","","Vishay","WSHM2818R0600FEA","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","#","","NEW001","unknown","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("111","ERA-8ARB102V","","NEW000001-111","","","NEW001SchLib","","","","","","","","","","ERA-8ARB102V","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","","","","","","","","orange","","","","","","","","");
INSERT INTO resistor VALUES("112","0.02 - �1% - R0603","RES","NEW000001-112","","","NEW001.SchLib","","=Value","Standard","Standard","","RES 0.02 - �1% - 0.25","","","Ohmite","FC4L16R020FER","","","","","","","","","","","","NEW001.PcbLib","R0603","","","","","","","","Datasheet","http://datasheet.octopart.com/FC4L16R020FER-Ohmite-datasheet-27120570.pdf","","NEW001","2014-05-08T15:40:07Z","A","2014-12-08","green","","Compliant","","","","","","");
INSERT INTO resistor VALUES("113","�1% - ","","NEW000001-113","","","NEW001SchLib","","=Value","Standard","Standard","","RES �1% - ","","","Stackpole Electronics","CSS2725FTL250","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/CSS2725FTL250-Stackpole-Electronics-datasheet-12517701.pdf","","NEW001","2012-06-06T15:57:42Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("114","10.0 - �1% - R2515","","NEW000001-114","","","NEW001SchLib","","=Value","Standard","Standard","","RES 10.0 - �1% - 1.0","","","Vishay","WSC251510R00FEA","","","","","","","","","","","","NEW001PcbLib","","","","","","","","","Datasheet","http://datasheet.octopart.com/WSC251510R00FEA-Vishay-datasheet-80324201.pdf","","NEW001","2017-04-08T03:11:28Z","O","2014-12-08","orange","","Compliant","","","","","","");
INSERT INTO resistor VALUES("115","2.0 - �1% - R2515","RES","NEW000001-115","","","NEW001.SchLib","","=Value","Standard","Standard","","RES 2.0 - �1% - 1.0","","","Vishay","WSC25152R000FEA","","","","","","","","","","","","NEW001.PcbLib","R2515","","","","","","","","Datasheet","http://datasheet.octopart.com/WSC25152R000FEA-Vishay-datasheet-5314438.pdf","","NEW001","2008-07-29T21:02:24Z","A","2014-12-08","green","","Compliant","","","","","","");
INSERT INTO resistor VALUES("116","0.0 - �1% - R3216","RES","NEW000001-116","","","NEW001.SchLib","","=Value","Standard","Standard","","RES 0.0 - �1% - 0.25","","","Vishay","WSL12065L000FEA18","","","","","","","","","","","","NEW001.PcbLib","R3216","","","","","","","","Datasheet","http://datasheet.octopart.com/WSL12065L000FEA18-Vishay-datasheet-86082047.pdf","","NEW001","2017-06-29T03:12:02Z","A","2014-12-08","green","","Compliant","","","","","","");
INSERT INTO resistor VALUES("117","1.0 - �1% - R2515","RES","NEW000001-117","","","NEW001.SchLib","","=Value","Standard","Standard","","RES 1.0 - �1% - 1.0","","","Vishay","WSC25151R000FEA","","","","","","","","","","","","NEW001.PcbLib","R2515","","","","","","","","Datasheet","http://datasheet.octopart.com/WSC25151R000FEA-Vishay-datasheet-80324201.pdf","","NEW001","2017-04-08T03:11:28Z","A","2014-12-08","green","","Compliant","","","","","","");
INSERT INTO resistor VALUES("118","0.0 - �1% - R2010","RES","NEW000001-118","","","NEW001.SchLib","","=Value","Standard","Standard","","RES 0.0 - �1% - 0.5","","","Vishay","WSL20104L000FEA","","","","","","","","","","","","NEW001.PcbLib","R2010","","","","","","","","Datasheet","http://datasheet.octopart.com/WSL20104L000FEA-Vishay-datasheet-21271709.pdf","","NEW001","2013-09-22T02:35:27Z","A","2014-12-08","green","","Compliant","","","","","","");





DROP TABLE resistors;

CREATE TABLE `resistors` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Library_Reference` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(45) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Manufacturer Part Number` varchar(255) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `Part Number` varchar(225) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `rohs` varchar(225) DEFAULT NULL,
  `Life_cycle` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  `custom1` varchar(255) DEFAULT NULL,
  `custom2` varchar(255) DEFAULT NULL,
  `custom3` varchar(255) DEFAULT NULL,
  `custom4` varchar(45) DEFAULT NULL,
  `custom5` varchar(45) DEFAULT NULL,
  `custom6` varchar(255) DEFAULT NULL,
  `custom7` varchar(255) DEFAULT NULL,
  `custom8` varchar(255) DEFAULT NULL,
  `custom9` varchar(255) DEFAULT NULL,
  `custom10` varchar(255) DEFAULT NULL,
  `custom11` varchar(255) DEFAULT NULL,
  `custom12` varchar(255) DEFAULT NULL,
  `custom13` varchar(45) DEFAULT NULL,
  `custom14` varchar(255) DEFAULT NULL,
  `custom15` varchar(255) DEFAULT NULL,
  `custom16` varchar(45) DEFAULT NULL,
  `custom17` varchar(255) DEFAULT NULL,
  `custom18` varchar(255) DEFAULT NULL,
  `custom19` varchar(255) DEFAULT NULL,
  `custom20` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO resistors VALUES("1","TNPW080510K0BEEA","150-00001","","NEW001.SchLib","","","","","RES 10K OHM 0.1% 1/5W 0805","","","","Vishay","TNPW080510K0BEEA","","","","","","","","","","","NEW001.PcbLib","","","http://datasheet.octopart.com/TNPW080510K0BEEA-Vishay-datasheet-542802.pdf","TNPW080510K0BEEA","green","TNPW080510K0BEEA","2008-07-29T21:05:53Z","A","2018-02-18","Compliant","","","","","","","","","","","","","","","","","","","","","","","","","");





DROP TABLE revision;

CREATE TABLE `revision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RevNo` varchar(45) DEFAULT NULL,
  `manu` varchar(225) DEFAULT NULL,
  `userID` varchar(225) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `notes` varchar(225) DEFAULT NULL,
  `CPN` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

INSERT INTO revision VALUES("1","O","TNPW080510K0BEEA","Tim Leiker","2018-02-18 05:56:11","Orignated","150-00001");
INSERT INTO revision VALUES("2","A","TNPW080510K0BEEA","Tim Leiker","2018-02-18 05:57:08","Approved","150-00001");
INSERT INTO revision VALUES("3","O","","The Manager","2018-02-18 09:55:03","Orignated","NEW000001-050");
INSERT INTO revision VALUES("4","O","ERJ-3GEYJ220V","The Manager","2018-02-18 09:55:53","Orignated","NEW000001-051");
INSERT INTO revision VALUES("5","O","ERJ-3GEYJ220V","The Manager","2018-02-18 09:57:54","Orignated","NEW000001-050");
INSERT INTO revision VALUES("6","O","Y16245K00000T9R","The Manager","2018-02-18 09:59:18","Orignated","NEW000001-053");
INSERT INTO revision VALUES("7","O","RT0603BRD0791KL","Kirti Chandel","2018-02-18 19:04:19","Orignated","NEW000001-054");
INSERT INTO revision VALUES("8","O","ERA-3AEB223V","Kirti Chandel","2018-02-18 19:11:56","Orignated","NEW000001-055");
INSERT INTO revision VALUES("9","O","ERA-2AEB563X","Kirti Chandel","2018-02-19 22:29:57","Orignated","NEW000001-056");
INSERT INTO revision VALUES("10","O","CF14JT10K0","Kirti Chandel","2018-02-24 02:51:40","Orignated","NEW000001-057");
INSERT INTO revision VALUES("11","O","CF14JT10K0","Kirti Chandel","2018-02-24 05:46:33","Orignated","");
INSERT INTO revision VALUES("12","O","CFR-25JR-52-10K","Kirti Chandel","2018-02-24 05:46:38","Orignated","");
INSERT INTO revision VALUES("13","O","CF14JT1K00","Kirti Chandel","2018-02-24 05:46:40","Orignated","");
INSERT INTO revision VALUES("14","O","RC1005J000CS","Kirti Chandel","2018-02-24 05:49:12","Orignated","");
INSERT INTO revision VALUES("15","O","RC0402JR-070RL","Kirti Chandel","2018-02-24 05:49:12","Orignated","");
INSERT INTO revision VALUES("16","O","RC0402FR-070RL","Kirti Chandel","2018-02-24 05:51:49","Orignated","");
INSERT INTO revision VALUES("17","O","RC0603JR-070RL","Kirti Chandel","2018-02-24 05:51:50","Orignated","");
INSERT INTO revision VALUES("18","O","RC0805JR-070RL","Kirti Chandel","2018-02-24 05:52:21","Orignated","NEW000001-064");
INSERT INTO revision VALUES("19","O","ERJ-2GE0R00X","Kirti Chandel","2018-02-24 05:52:55","Orignated","NEW000001-065");
INSERT INTO revision VALUES("20","O","ERJ-1GN0R00C","Kirti Chandel","2018-02-24 05:56:55","Orignated","NEW000001-066");
INSERT INTO revision VALUES("21","A","ERJ-1GN0R00C","Kirti Chandel","2018-02-24 05:59:27","Approved","NEW000001-066");
INSERT INTO revision VALUES("22","O","RC1206JR-070RL","Kirti Chandel","2018-02-24 06:00:21","Orignated","NEW000001-067");
INSERT INTO revision VALUES("23","O","ERJ-1GE0R00C","Kirti Chandel","2018-02-24 06:02:09","Orignated","");
INSERT INTO revision VALUES("24","O","RC0201JR-070RL","Kirti Chandel","2018-02-24 06:05:39","Orignated","");
INSERT INTO revision VALUES("25","O","RC0201FR-071KL","Kirti Chandel","2018-02-24 06:06:16","Orignated","");
INSERT INTO revision VALUES("26","O","RC0201FR-070RL","Kirti Chandel","2018-02-24 06:06:17","Orignated","");
INSERT INTO revision VALUES("27","O","ERJ-3GEY0R00V","Kirti Chandel","2018-02-24 06:06:17","Orignated","");
INSERT INTO revision VALUES("28","O","RC0201FR-07100KL","Kirti Chandel","2018-02-24 06:07:26","Orignated","");
INSERT INTO revision VALUES("29","O","ERJ-1GEF1003C","Kirti Chandel","2018-02-24 06:17:02","Orignated","");
INSERT INTO revision VALUES("30","O","ERJ-1GEF4701C","Kirti Chandel","2018-02-24 06:20:41","Orignated","NEW000001-068");
INSERT INTO revision VALUES("31","O","ERJ-1GEF1500C","Kirti Chandel","2018-02-24 06:21:54","Orignated","NEW000001-076");
INSERT INTO revision VALUES("32","O","ERJ-1GEF2003C","Kirti Chandel","2018-02-24 06:22:11","Orignated","NEW000001-077");
INSERT INTO revision VALUES("33","A","ERJ-1GEF2003C","Kirti Chandel","2018-02-24 06:25:30","Approved","NEW000001-077");
INSERT INTO revision VALUES("34","O","ERJ-1GEF49R9C","Kirti Chandel","2018-02-24 06:29:10","Orignated","");
INSERT INTO revision VALUES("35","O","ERJ-1GEF1000C","Kirti Chandel","2018-02-24 06:31:15","Orignated","NEW000001-079");
INSERT INTO revision VALUES("36","O","ERJ-1GEF1004C","Kirti Chandel","2018-02-24 06:34:28","Orignated","NEW000001-078");
INSERT INTO revision VALUES("37","O","ERJ-1GEF40R2C","Kirti Chandel","2018-02-24 06:34:51","Orignated","NEW000001-081");
INSERT INTO revision VALUES("38","O","ERJ-6GEY0R00V","Kirti Chandel","2018-02-24 06:40:06","Orignated","NEW000001-082");
INSERT INTO revision VALUES("39","A","RR0510P-103-D","Kirti Chandel","2018-02-24 06:42:05","Orignated","");
INSERT INTO revision VALUES("40","A","RR0510P-102-D","Kirti Chandel","2018-02-24 06:42:06","Orignated","");
INSERT INTO revision VALUES("41","A","CRCW020110K0FNED","Kirti Chandel","2018-02-24 06:43:31","Orignated","");
INSERT INTO revision VALUES("42","O","RR1220P-103-D","Kirti Chandel","2018-02-24 06:48:08","Orignated","");
INSERT INTO revision VALUES("43","A","ERJ-1GEF4701C","Kirti Chandel","2018-02-24 06:50:35","Approved","NEW000001-068");
INSERT INTO revision VALUES("44","O","ERA-3YEB203V","Kirti Chandel","2018-02-24 07:27:30","Orignated","NEW000001-087");
INSERT INTO revision VALUES("45","O","ERA-3YEB153V","Kirti Chandel","2018-02-24 07:28:00","Orignated","");
INSERT INTO revision VALUES("46","O","ERA-3YEB222V","Kirti Chandel","2018-02-24 07:36:36","Orignated","");
INSERT INTO revision VALUES("47","O","ERA-3YEB303V","Kirti Chandel","2018-02-24 07:38:39","Orignated","");
INSERT INTO revision VALUES("48","O","KRL1220E-M-R010-F-T5","Kirti Chandel","2018-02-24 07:41:38","Orignated","");
INSERT INTO revision VALUES("49","O","KRL1220E-M-R100-F-T5","Kirti Chandel","2018-02-24 07:44:04","Orignated","");
INSERT INTO revision VALUES("50","O","KRL1220E-M-R012-F-T5","Kirti Chandel","2018-02-24 07:47:27","Orignated","");
INSERT INTO revision VALUES("51","O","HCJ0402ZT0R00","Kirti Chandel","2018-02-24 07:48:38","Orignated","");
INSERT INTO revision VALUES("52","O","CRM1206-FX-R100ELF","Kirti Chandel","2018-02-24 07:53:54","Orignated","");
INSERT INTO revision VALUES("53","O","PT0805FR-7W0R1L","Kirti Chandel","2018-02-24 08:02:52","Orignated","");
INSERT INTO revision VALUES("54","O","CRT0603-BY-1002ELF","Kirti Chandel","2018-02-24 08:04:01","Orignated","");
INSERT INTO revision VALUES("55","O","CRT0603-BY-10R0ELF","Kirti Chandel","2018-02-24 08:04:36","Orignated","");
INSERT INTO revision VALUES("56","O","CSRN2512FKR680","Kirti Chandel","2018-02-24 08:06:42","Orignated","");
INSERT INTO revision VALUES("57","O","ERJ-L06KF47MV","Kirti Chandel","2018-02-24 08:07:24","Orignated","");
INSERT INTO revision VALUES("58","O","RNCS0805BKE10R0","Kirti Chandel","2018-02-24 08:16:06","Orignated","");
INSERT INTO revision VALUES("59","O","KRL1632E-M-R033-F-T5","Kirti Chandel","2018-02-24 08:18:42","Orignated","");
INSERT INTO revision VALUES("60","O","WSL2512R0400FEA","Kirti Chandel","2018-02-24 08:24:36","Orignated","");
INSERT INTO revision VALUES("61","O","WSL2512R0500FEA","Kirti Chandel","2018-02-24 08:31:58","Orignated","");
INSERT INTO revision VALUES("62","O","WSL2512R0300FEA","Kirti Chandel","2018-02-24 08:35:00","Orignated","");
INSERT INTO revision VALUES("63","O","WSL2512R0150FEA","Kirti Chandel","2018-02-24 08:38:19","Orignated","");
INSERT INTO revision VALUES("64","O","WSHM2818R0150FEA","Kirti Chandel","2018-02-24 08:39:37","Orignated","");
INSERT INTO revision VALUES("65","O","WSHM2818R0600FEA","Kirti Chandel","2018-02-24 08:41:31","Orignated","");
INSERT INTO revision VALUES("66","O","ERA8ARB102V","Kirti Chandel","2018-02-24 08:42:17","Orignated","");
INSERT INTO revision VALUES("67","O","FC4L16R020FER","Kirti Chandel","2018-02-24 08:43:29","Orignated","");
INSERT INTO revision VALUES("68","O","FC4L16R020FER","Kirti Chandel","2018-02-24 08:44:40","Orignated","");
INSERT INTO revision VALUES("69","O","CSS2725FTL250","Kirti Chandel","2018-02-24 08:44:41","Orignated","");
INSERT INTO revision VALUES("70","O","WSC251510R00FEA","Kirti Chandel","2018-02-24 08:44:41","Orignated","");
INSERT INTO revision VALUES("71","O","WSC25151R000FEA","Kirti Chandel","2018-02-24 08:44:42","Orignated","");
INSERT INTO revision VALUES("72","O","WSC25152R000FEA","Kirti Chandel","2018-02-24 08:44:43","Orignated","");
INSERT INTO revision VALUES("73","O","WSL20104L000FEA","Kirti Chandel","2018-02-24 08:44:43","Orignated","");
INSERT INTO revision VALUES("74","O","WSL12065L000FEA18","Kirti Chandel","2018-02-24 08:44:44","Orignated","");
INSERT INTO revision VALUES("75","A","WSL12065L000FEA18","Kirti Chandel","2018-02-24 19:16:42","Approved","NEW000001-116");
INSERT INTO revision VALUES("76","A","WSL20104L000FEA","Kirti Chandel","2018-02-24 19:17:03","Approved","NEW000001-118");
INSERT INTO revision VALUES("77","A","WSC25152R000FEA","Kirti Chandel","2018-02-24 19:17:24","Approved","NEW000001-115");
INSERT INTO revision VALUES("78","A","WSC25151R000FEA","Kirti Chandel","2018-02-24 19:19:08","Approved","NEW000001-117");
INSERT INTO revision VALUES("79","A","FC4L16R020FER","Kirti Chandel","2018-02-27 11:52:34","Approved","NEW000001-112");





DROP TABLE subscription_plan;

CREATE TABLE `subscription_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Subscription_name` varchar(45) DEFAULT NULL,
  `Price` varchar(45) DEFAULT NULL,
  `Parts_package` varchar(45) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `purchase` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE symbols_default;

CREATE TABLE `symbols_default` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `column_val` varchar(200) NOT NULL,
  `category` varchar(200) NOT NULL,
  `col_type` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO symbols_default VALUES("1","R0805","resistor","footprint");





DROP TABLE transistor;

CREATE TABLE `transistor` (
  `ITEM` int(5) NOT NULL AUTO_INCREMENT,
  `Part_Number` varchar(255) DEFAULT NULL,
  `CPN` varchar(255) DEFAULT NULL,
  `Library_Ref` varchar(255) DEFAULT NULL,
  `Library_Path` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Component_Kind` varchar(255) DEFAULT NULL,
  `Component_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Designator` varchar(255) DEFAULT NULL,
  `Footprint` varchar(255) DEFAULT NULL,
  `Impedance_Max_Zzt` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Manufacturer_Part_Number` varchar(255) DEFAULT NULL,
  `Manufacturer1` varchar(45) DEFAULT NULL,
  `Manufacturer_Part_Number1` varchar(45) DEFAULT NULL,
  `Mounting_Type` varchar(255) DEFAULT NULL,
  `Packaging` varchar(255) DEFAULT NULL,
  `Pin_Count` varchar(255) DEFAULT NULL,
  `rohs` varchar(255) DEFAULT NULL,
  `lifecycle` varchar(255) DEFAULT NULL,
  `Signal_Integrity` varchar(255) DEFAULT NULL,
  `Simulation` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number` varchar(255) DEFAULT NULL,
  `Supplier1` varchar(255) DEFAULT NULL,
  `Supplier_Part_Number1` varchar(255) DEFAULT NULL,
  `Footprint_Path` varchar(255) DEFAULT NULL,
  `Footprint_Ref` varchar(255) DEFAULT NULL,
  `ComponentLink1Description` varchar(255) DEFAULT NULL,
  `ComponentLink1URL` varchar(255) DEFAULT NULL,
  `Height` varchar(10) DEFAULT NULL,
  `Temp_Qual` varchar(45) DEFAULT NULL,
  `company_id` varchar(45) DEFAULT NULL,
  `revise_datasheet` varchar(225) DEFAULT NULL,
  `revision` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `Temp-Qual` varchar(45) DEFAULT NULL,
  `Lead_free` varchar(225) DEFAULT NULL,
  `part_img` varchar(500) DEFAULT NULL,
  `sym_img` varchar(500) DEFAULT NULL,
  `pcb_img` varchar(500) DEFAULT NULL,
  `threeD_img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ITEM`),
  UNIQUE KEY `Manufacturer_Part_Number_UNIQUE` (`Manufacturer_Part_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO transistor VALUES("1","FQD13N06LTM","NEW000007-001","NFET","NEW001.SchLib","","=Value","","Standard","Standard","This N-Channel enhancement mode power MOSFET is produced using Fairchild Semiconductor’s proprietary planar stripe and DMOS technology. This advanced MOSFET technology has been especially tailored to reduce on-state resistance, and to provide superior s","","","","Fairchild Semiconductor","FQD13N06LTM","","","Surface Mount"," Reel and tape","3","Compliant","Active","","","","","","","NEW001.PcbLib","TO-252","Datasheet","http://datasheet.octopart.com/FQD13N06LTM-Fairchild-datasheet-8331961.pdf","","","NEW001","Unknown","A","2018-02-18","green","","Lead Free","partimg/FQD13N06LTM-PIC.jpg","partsymbol/NFET-SCH.jpg","partfootprint/TO-252-PCB.jpg","3dimg/TO-252-3D.jpg");
INSERT INTO transistor VALUES("2","BSS84","NEW000007-002","PFET","NEW001.SchLib","","=Value","","Standard","Standard","This P-channel enhancement-mode field-effect transistor is produced using Fairchild’s proprietary, high cell density, DMOS technology. This very high density process minimizes on-state resistance and to provide rugged and reliable performance and fast s","","","","Fairchild Semiconductor","BSS84","","","Surface Mount"," Reel and tape","3","Compliant","Active","","","","","","","NEW001.PcbLib","SOT-23","Datasheet","http://datasheet.octopart.com/BSS84-Fairchild-datasheet-62054.pdf","","","NEW001","BSS84 Rev B(W)","A","2018-02-18","green","","Lead Free","partimg/BSS84-PIC.jpg","partsymbol/PFET-SCH.jpg","partfootprint/SOT-23-PCB.jpg","3dimg/SOT-23-3D.jpg");
INSERT INTO transistor VALUES("3","MMBT3906","NEW000007-003","PNP","NEW001.SchLib","","=Value","","Standard","Standard","This device is designed for general purpose amplifier and switching applications at collector currents of 10 µA to 100 mA.","","","","Fairchild Semiconductor","MMBT3906","","","Surface Mount"," Reel and tape","3","Compliant","Active","","","","","","","NEW001.PcbLib","SOT-23","Datasheet","http://datasheet.octopart.com/MMBT3906-Fairchild-datasheet-23394.pdf","","","NEW001","2N3906/MMBT3906/PZT3906, Rev A","A","2018-02-18","green","","Lead Free","partimg/MMBT3906-PIC.jpg","partsymbol/PNP-SCH.jpg","partfootprint/SOT-23-PCB.jpg","3dimg/SOT-23-3D.jpg");
INSERT INTO transistor VALUES("4","MMBT3904","NEW000007-004","NPN","NEW001.SchLib","","=Value","","Standard","Standard","This device is designed as a general purpose amplifier and switch. The useful dynamic range extends to 100 mA as a switch and to 100 MHz as an amplifier.","","","","Fairchild Semiconductor","MMBT3904","","","Surface Mount"," Reel and tape","3","Compliant","Active","","","","","","","NEW001.PcbLib","SOT-23","Datasheet","http://datasheet.octopart.com/MMBT3904-Fairchild-datasheet-5999.pdf","","","NEW001","2N3904/MMBT3904/PZT3904, Rev A","A","2018-02-18","green","","Lead Free","partimg/MMBT3904-PIC.jpg","partsymbol/NPN-SCH.jpg","partfootprint/SOT-23-PCB.jpg","3dimg/SOT-23-3D.jpg");





DROP TABLE uploads;

CREATE TABLE `uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(225) DEFAULT NULL,
  `sch_name` varchar(225) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE vendor_list;

CREATE TABLE `vendor_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `com_id` varchar(45) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






DROP TABLE visible;

CREATE TABLE `visible` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `column1` varchar(45) DEFAULT NULL,
  `column2` varchar(45) DEFAULT NULL,
  `column3` varchar(45) DEFAULT NULL,
  `column4` varchar(45) DEFAULT NULL,
  `column5` varchar(45) DEFAULT NULL,
  `cat_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO visible VALUES("1","Part_Number","CPN","Manufacturer","Description","Manufacturer_Part_Number","all");
INSERT INTO visible VALUES("2","Part_Number","CPN","Description","Manufacturer","Manufacturer_Part_Number","resistors");



