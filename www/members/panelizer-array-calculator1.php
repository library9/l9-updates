<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en-US">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" href="http://bayareacircuits.com/favicon.ico" />
<!-- Style sheets -->
<link rel="stylesheet" type="text/css" href="http://bayareacircuits.com/wp-content/themes/bayareacircuits/style.css?v=1" media="screen" />
<style type="text/css">
	body {
		background-color: #6E6E6E;
	}
	body {
		font-size: 13px;
	}
	.very-top {
		color: #7D7F82;
	}
	.very-top #pages-nav a {
		color: #FFFFFF;
	}
	.very-top #pages-nav .topnav a:hover, .very-top #pages-nav .topnav li.current_page_item, .very-top #pages-nav .topnav li.current_page_parent, .very-top #pages-nav .topnav li.current-menu-item, .very-top #pages-nav .topnav li.current-menu-parent,
.very-top #pages-nav .topnav li.current-cat {
		color: #7D7F82;
	}
	.menu ul li.current-menu-item a, .menu ul li.current-menu-parent ul li.current-menu-item a, .menu ul li.current-cat a, .menu ul li.current_page_item a, .menu ul li.current_page_parent ul li.current_page_item a, .menu ul li.current-cat-parent ul li.current-cat a {
		color: #FFFFFF;
	}
	.menu ul li.current-cat>a, .menu ul li.current_page_item>a, .menu ul li.current-menu-item>a {
		background-color: #949AA5;
	}
	.menu ul li a {
		color: #FFFFFF;
	}
	.menu ul li a:hover {
		color: #FFFFFF;
	}
	.slideshow .desc h2 a {
		color: #ffffff;
	}
	.slideshow .desc .cont {
		color: #ffffff;
	}
	.note.module h2 {
		color: #ffffff;
	}
	.note.module p {
		color: #ffffff;
	}
	.note.module a {
		color: #000000;
	}
	.note.module a:hover {
		color: #ffffff;
	}
	#featured .grid h2 a, #featured .grid h2 {
		color: #3a3a3a;
	}
	#featured .grid {
		color: #000000;
	}
	#featured .grid a {
		color: #e06900;
	}
	#featured .grid a:hover {
		color: #000000;
	}
	.post-head h2, .post-head h2 a, .main-title h2, .main-title h2 a, #respond h2, h2.comment-title {
		color: #696969;
	}
	.post, #respond, .post .entry {
		color: #000000;
	}
	.post a, .post .entry a {
		color: #e06900;
	}
	.post a:hover, .post .postmetadata a.comments-link:hover, .post .postmetadata .category a:hover, .post .postmetadata small .author, .tags a:hover, .portfolio-format .main-title span a:hover {
		color: #000000;
	}
	.widget h2 {
		color: #3a3a3a;
	}
	.widget {
		color: #696969;
	}
	.widget a {
		color: #A82F55;
	}
	.widget a:hover {
		color: #000000;
	}
	.footer ul li, .footer {
		color: #7d7f82;
	}
	.footer ul li a, .footer a {
		color: #7d7f82;
	}
	.footer ul li a:hover, .footer a:hover {
		color: #ffffff;
	}
</style>

<link rel="stylesheet" type="text/css" href="http://bayareacircuits.com/wp-content/themes/Comodo/include/fancybox/fancy.css" media="screen" />

<!--[if lt IE 7]>
  <link rel="stylesheet" type="text/css" href="http://bayareacircuits.com/wp-content/themes/Comodo/ie6.css" />
    <script type="text/javascript" src="http://bayareacircuits.com/wp-content/themes/Comodo/js/pngFix.min.js"></script>
		<script type="text/javascript"> 
			jQuery(document).ready(function(){ 
				$j(document.body).supersleight();
			}); 
		</script>    
	<![endif]-->
  <!--[if IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="http://bayareacircuits.com/wp-content/themes/Comodo/ie7.css" />
<![endif]-->
<style type="text/css">
body {background-image: url("http://bayareacircuits.com/wp-content/uploads/2011/09/html2.png"); background-repeat: repeat}</style>
<title>PCB Array Calculator | Bay Area Circuits</title>
<!-- Feed link -->
<link rel="alternate" type="application/rss+xml" title="Bay Area Circuits RSS Feed" href="http://bayareacircuits.com/feed/" />
<link rel="pingback" href="http://bayareacircuits.com/xmlrpc.php" />
<link rel="bookmark" type="text/html" title="Best wordpress themes portfolio club" href="http://www.nattywp.com" />
<link rel="alternate" type="application/rss+xml" title="Bay Area Circuits &raquo; Feed" href="http://bayareacircuits.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Bay Area Circuits &raquo; Comments Feed" href="http://bayareacircuits.com/comments/feed/" />
<link rel='stylesheet' id='colorbox-theme1-css'  href='http://bayareacircuits.com/wp-content/plugins/jquery-colorbox/themes/theme1/colorbox.css' type='text/css' media='screen' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://bayareacircuits.com/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' />
<link rel='stylesheet' id='bayareacircuits-css'  href='http://bayareacircuits.com/wp-content/themes/bayareacircuits/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='panelizer2-css'  href='http://bayareacircuits.com/wp-content/plugins/panelizer2/css/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='fancybox-css'  href='http://bayareacircuits.com/wp-content/plugins/panelizer2/fancybox/jquery.fancybox-1.3.4.css' type='text/css' media='all' />
<link rel='stylesheet' id='magiczoom-css'  href='http://bayareacircuits.com/wp-content/plugins/panelizer2/css/magiczoom.css' type='text/css' media='all' />
<link rel='stylesheet' id='A2A_SHARE_SAVE-css'  href='http://bayareacircuits.com/wp-content/plugins/add-to-any/addtoany.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='uds-billboard-css'  href='http://bayareacircuits.com/wp-content/plugins/uBillboard/css/billboard.min.css' type='text/css' media='screen' />
<script type='text/javascript' src='http://bayareacircuits.com/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript' src='http://bayareacircuits.com/wp-includes/js/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var jQueryColorboxSettingsArray = {"jQueryColorboxVersion":"4.6","colorboxInline":"false","colorboxIframe":"false","colorboxGroupId":"","colorboxTitle":"","colorboxWidth":"false","colorboxHeight":"false","colorboxMaxWidth":"false","colorboxMaxHeight":"false","colorboxSlideshow":"false","colorboxSlideshowAuto":"false","colorboxScalePhotos":"false","colorboxPreloading":"false","colorboxOverlayClose":"false","colorboxLoop":"true","colorboxEscKey":"true","colorboxArrowKey":"true","colorboxScrolling":"true","colorboxOpacity":"0.85","colorboxTransition":"elastic","colorboxSpeed":"350","colorboxSlideshowSpeed":"2500","colorboxClose":"close","colorboxNext":"next","colorboxPrevious":"previous","colorboxSlideshowStart":"start slideshow","colorboxSlideshowStop":"stop slideshow","colorboxCurrent":"{current} of {total} images","colorboxXhrError":"This content failed to load.","colorboxImgError":"This image failed to load.","colorboxImageMaxWidth":"false","colorboxImageMaxHeight":"false","colorboxImageHeight":"false","colorboxImageWidth":"false","colorboxLinkHeight":"false","colorboxLinkWidth":"false","colorboxInitialHeight":"100","colorboxInitialWidth":"300","autoColorboxJavaScript":"","autoHideFlash":"","autoColorbox":"true","autoColorboxGalleries":"","addZoomOverlay":"","useGoogleJQuery":"","colorboxAddClassToLinks":""};
/* ]]> */
</script>
<script type='text/javascript' src='http://bayareacircuits.com/wp-content/plugins/jquery-colorbox/js/jquery.colorbox-min.js'></script>
<script type='text/javascript' src='http://bayareacircuits.com/wp-content/plugins/jquery-colorbox/js/jquery-colorbox-wrapper-min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var panelizer2 = {"ajaxurl":"http:\/\/bayareacircuits.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='http://bayareacircuits.com/wp-content/plugins/panelizer2/js/panelizer2.js'></script>
<script type='text/javascript' src='http://cdnjs.cloudflare.com/ajax/libs/cufon/1.09i/cufon-yui.js'></script>
<script type='text/javascript' src='http://bayareacircuits.com/wp-content/plugins/panelizer2/js/magiczoom.js'></script>
<script type='text/javascript' src='http://bayareacircuits.com/wp-content/plugins/panelizer2/fancybox/jquery.fancybox-1.3.4.pack.js'></script>
<script type='text/javascript' src='http://bayareacircuits.com/wp-content/plugins/panelizer2/fancybox/jquery.easing-1.3.pack.js'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://bayareacircuits.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://bayareacircuits.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.0.1" />
<link rel='shortlink' href='http://bayareacircuits.com/?p=1044' />

<script type="text/javascript"><!--
var a2a_config=a2a_config||{},wpa2a={done:false,html_done:false,script_ready:false,script_load:function(){var a=document.createElement('script'),s=document.getElementsByTagName('script')[0];a.type='text/javascript';a.async=true;a.src='http://static.addtoany.com/menu/page.js';s.parentNode.insertBefore(a,s);wpa2a.script_load=function(){};},script_onready:function(){if(a2a.type=='page'){wpa2a.script_ready=true;if(wpa2a.html_done)wpa2a.init();}},init:function(){for(var i=0,el,target,targets=wpa2a.targets,length=targets.length;i<length;i++){el=document.getElementById('wpa2a_'+(i+1));target=targets[i];a2a_config.linkname=target.title;a2a_config.linkurl=target.url;if(el){a2a.init('page',{target:el});el.id='';}wpa2a.done=true;}wpa2a.targets=[];}};a2a_config.tracking_callback=['ready',wpa2a.script_onready];
//--></script>
<link href='http://bayareacircuits.com/wp-content/plugins/arrayCalc/css/array_pcb.css' type='text/css' rel='stylesheet' /><script type='text/javascript' src='http://bayareacircuits.com/wp-content/plugins/arrayCalc/js/javaArrayFunctions.js' ></script><script type='text/javascript' src='http://bayareacircuits.com/wp-content/plugins/panelizer/js/class.pcb.js' ></script><script type='text/javascript' src='http://bayareacircuits.com/wp-content/plugins/panelizer/js/pcb.functions.js' ></script><script type='text/javascript'  >ajaxurl = 'http://bayareacircuits.com/wp-admin/admin-ajax.php'; overlayImage = 'http://bayareacircuits.com/wp-content/plugins/panelizer/images/online-panelizer-tool.png'</script><link href='http://bayareacircuits.com/wp-content/plugins/panelizer/css/pcb.css' type='text/css' rel='stylesheet' /><link rel="stylesheet" href="http://bayareacircuits.com/wp-content/plugins/shortcodedeluxe/scd.css" media="screen" /><script type="text/javascript" src="http://bayareacircuits.com/wp-content/plugins/shortcodedeluxe/jscolor/jscolor.js"></script><link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,700,300' rel='stylesheet' type='text/css'>
<meta name="template" content="Comodo 3.1.3" />
<meta name="generator" content="NattyWP Framework Version 2.0" />
<meta name="description" content="Visit the Bay Area Circuits website We have been serving the PCB Manufacturing needs of CEMs, OEMs and Design Engineers in California\\\'s Silicon Valley for more than 37 years" />
<link rel="icon" href="http://bayareacircuits.com/wp-content/themes/Comodo/images/icons/favicon.ico" /><link rel="shortcut icon" href="http://bayareacircuits.com/wp-content/themes/Comodo/images/icons/favicon.ico" />
<link rel="stylesheet" type="text/css" href="http://bayareacircuits.com/wp-content/themes/Comodo/functions/css/shortcodes.css" media="screen" /><script type="text/javascript" src="http://bayareacircuits.com/wp-content/themes/Comodo/functions/js/shortcode.js"></script>
<!-- All in One SEO Pack 2.2.3.1 by Michael Torbert of Semper Fi Web Design[3582,3625] -->
<link rel="author" href="https://plus.google.com/101085786939645213212" />
<meta name="description"  content="Use our PCB Array Calculator to quickly determine how many individual boards can be designed on an array for assembly." />

<meta name="keywords"  content="pcb array calculator, pcb manufacturing" />

<link rel="canonical" href="http://bayareacircuits.com/pcb-array-calculator/" />
<meta property="og:title" content="PCB Array Calculator | Bay Area Circuits" />
<meta property="og:type" content="activity" />
<meta property="og:url" content="http://bayareacircuits.com/pcb-array-calculator/" />
<meta property="og:site_name" content="Bay Area Circuits" />
<meta property="og:description" content="Use our PCB Array Calculator to quickly determine how many individual boards can be designed on an array for assembly." />
<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="Use our PCB Array Calculator to quickly determine how many individual boards can be designed on an array for assembly." />
<!-- /all in one seo pack -->
<!-- jQuery utilities -->
<script type="text/javascript">
		var themePath = 'http://bayareacircuits.com/wp-content/themes/Comodo/'; // for js functions 
	</script>
<script type="text/javascript">var $j = jQuery.noConflict();</script>
<script type="text/javascript" src="http://bayareacircuits.com/wp-content/themes/Comodo/js/jquery-ui-1.7.2.min.js"></script>
<script type="text/javascript" src="http://bayareacircuits.com/wp-content/themes/Comodo/js/jquery.cycle.all.min.js"></script>
<script type="text/javascript" src="http://bayareacircuits.com/wp-content/themes/Comodo/js/superfish.min.js"></script>
<script type="text/javascript" src="http://bayareacircuits.com/wp-content/themes/Comodo/js/supersubs.min.js"></script>
<script type="text/javascript" src="http://bayareacircuits.com/wp-content/themes/Comodo/js/jquery.overlabel.min.js"></script>
<script type="text/javascript" src="http://bayareacircuits.com/wp-content/themes/Comodo/js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
<link rel="stylesheet" href="http://bayareacircuits.com/wp-content/themes/Comodo/js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
<script type="text/javascript" src="http://bayareacircuits.com/wp-content/themes/Comodo/js/load.js"></script>
</head>
<!-- template-full.php --><body class="page page-id-1044 page-template page-template-template-full-php">

<!-- BEGIN LivePerson Button Code -->
<div id="lpButDivID-1320094632886"></div>
<script type="text/javascript" charset="UTF-8" src="https://server.iad.liveperson.net/hc/91548726/?cmd=mTagRepstate&site=91548726&buttonID=12&divID=lpButDivID-1320094632886&bt=1&c=1"></script>
<!-- END LivePerson Button code -->

<!-- BEGIN LivePerson Monitor. -->
<script language='javascript'> var lpMTagConfig = {'lpServer' : "server.iad.liveperson.net",'lpNumber' : "91548726",'lpProtocol' : (document.location.toString().indexOf('https:')==0) ? 'https' : 'http'}; function lpAddMonitorTag(src){if(typeof(src)=='undefined'||typeof(src)=='object'){src=lpMTagConfig.lpMTagSrc?lpMTagConfig.lpMTagSrc:'/hcp/html/mTag.js';}if(src.indexOf('http')!=0){src=lpMTagConfig.lpProtocol+"://"+lpMTagConfig.lpServer+src+'?site='+lpMTagConfig.lpNumber;}else{if(src.indexOf('site=')<0){if(src.indexOf('?')<0)src=src+'?';else src=src+'&';src=src+'site='+lpMTagConfig.lpNumber;}};var s=document.createElement('script');s.setAttribute('type','text/javascript');s.setAttribute('charset','iso-8859-1');s.setAttribute('src',src);document.getElementsByTagName('head').item(0).appendChild(s);} if (window.attachEvent) window.attachEvent('onload',lpAddMonitorTag); else window.addEventListener("load",lpAddMonitorTag,false);</script><!-- END LivePerson Monitor. -->

<div class="inner-wrap rounded module">

  <div id="header">	
    <div class="logo">
       <a href="http://bayareacircuits.com"><img src="http://bayareacircuits.com/wp-content/uploads/2013/11/logo.jpg" border="0" class="png" alt="Bay Area Circuits" /></a>
    </div> 
    <div class="social-icons">
		<ul>
			<li><a href="http://twitter.com/bayareacircuits"><img src="http://bayareacircuits.com/wp-content/themes/bayareacircuits/images/twitter.jpg"></a></li>
			<li><a href="https://www.facebook.com/BayAreaCircuits"><img src="http://bayareacircuits.com/wp-content/themes/bayareacircuits/images/facebook.jpg"></a></li>
			<li><a href="http://www.linkedin.com/company/bay-area-circuits-inc./"><img src="http://bayareacircuits.com/wp-content/themes/bayareacircuits/images/linkedin.jpg"></a></li>
			<li><a href="https://www.youtube.com/user/thepcbmanufacturer"><img src="http://bayareacircuits.com/wp-content/themes/bayareacircuits/images/youtube.jpg"></a></li>
			<li><a href="https://plus.google.com/+Bayareacircuits/about/" rel="publisher"><img src="http://bayareacircuits.com/wp-content/themes/bayareacircuits/images/google-plus.jpg"></a></li>
			<li><a href="http://feeds.feedburner.com/BayAreaCircuits"><img src="http://bayareacircuits.com/wp-content/themes/bayareacircuits/images/rss.jpg"></a></li>
		</ul>
	</div>
    <div class="clear"></div>
  </div> <!-- END Header -->
		<div class="left fullwidth">
             									
			<div class="post page">
        <div class="main-title">
          <h1><a href="http://bayareacircuits.com/pcb-array-calculator/" rel="bookmark" title="Array Calculator">Array Calculator</a></h1>     			
        </div>     
         
				<div class="entry">
  
<div class='panelizer2-wrapper' >
	<form action='slika.php' method='post' class='panelizer2-form' >
		<div class='panelizer-heading' >Dimensions</div>
		<div class='panelizer-input-wrap' >
			<fieldset>
				<label for='o1' >Array Dimension 1</label>
				<br clear='all' />
				<input type='text' name='o1' id='o1' value='7.8' />
			</fieldset>
			<fieldset>
				<label for='o2' >Array Dimension 2</label>
				<br clear='all' />
				<input type='text' name='o2' id='o2' value='10.8' />
			</fieldset>
			<br clear='all' />
			<fieldset>
				<label for='d1' >Part Dimension 1</label>
				<br clear='all' />
				<input type='text' name='d1' id='d1' value='1' />
			</fieldset>
			<fieldset>
				<label for='d2' >Part Dimension 2</label>
				<br clear='all' />
				<input type='text' name='d2' id='d2' value='1' />
			</fieldset>
		</div>
		<div class='panelizer-heading' >Settings</div>
		<div class='panelizer-input-wrap' >
			<fieldset>
				<label for='fab' >Fab</label>
				<br clear='all' />
				<select type='text' name='fab' id='fab' >
					<option value='1'>V Score</option>
					<option value='0' selected='selected' >Tab Route</option>
				</select>
			</fieldset>
			<fieldset>
				<label for='rsize' >Rail Size</label>
				<br clear='all' />
				<select type='text' name='rsize' id='rsize' >
					<option value='0.5' selected='selected' >0.5</option>
					<option value='0.375' >0.375</option>
					<option value='0.25' >0.25</option>
					<option value='0' >0</option>
				</select>
			</fieldset>
			<fieldset>
				<label for='rsides' >Rail Sides</label>
				<br clear='all' />
				<select type='text' name='rsides' id='rsides' >
					<option value='4' selected='selected' >4</option>
					<option value='2' >2</option>
				</select>
			</fieldset>
			<br clear='all' />
			<fieldset>
				<label for='tholes' >Tooling Holes</label>
				<br clear='all' />
				<select type='text' name='tholes' id='tholes' >
					<option value='0'>NA</option>
					<option value='0.125' >0.125</option>
					<option value='0.250' selected='selected' >0.250</option>
				</select>
			</fieldset>
			<fieldset>
				<label for='fid' >Fiducials</label>
				<br clear='all' />
				<select type='text' name='fid' id='fid' >
					<option value='0' >No</option>
					<option value='1' selected='selected' >Yes</option>
				</select>
			</fieldset>
			<fieldset id='panelizer-mbites'  >
				<label for='mbites' >Mouse Bites</label>
				<br clear='all' />
				<select type='text' name='mbites' id='mbites' >
					<option value='0' >NA</option>
					<option value='1' selected='selected' >Yes</option>
				</select>
			</fieldset>
		</div>
		<fieldset id='panelizer-submit'>
			<input type='submit' value='Update Image' />
		</fieldset>
	</form>
	<div id='result'></div>
</div>



</div>  
           
        </div>   
        
				                 
			</div><!-- END Post -->	  

			                      
       	 	
        </div><!-- END left -->           
                 
</div><!-- END Content -->
 </div><!-- wrapper -->  

<script type="text/javascript">
wpa2a.targets=[];
wpa2a.html_done=true;if(wpa2a.script_ready&&!wpa2a.done)wpa2a.init();wpa2a.script_load();
</script>
        
		<script type="text/javascript">
       
        var disqus_shortname = 'bayareacircuits';
        (function () {
            var nodes = document.getElementsByTagName('span');
            for (var i = 0, url; i < nodes.length; i++) {
                if (nodes[i].className.indexOf('dsq-postid') != -1) {
                    nodes[i].parentNode.setAttribute('data-disqus-identifier', nodes[i].getAttribute('rel'));
                    url = nodes[i].parentNode.href.split('#', 1);
                    if (url.length == 1) { url = url[0]; }
                    else { url = url[1]; }
                    nodes[i].parentNode.href = url + '#disqus_thread';
                }
            }
            var s = document.createElement('script'); 
            s.async = true;
            s.type = 'text/javascript';
            s.src = '//' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
        }());
        
        </script>
        
<script type='text/javascript' src='http://bayareacircuits.com/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js'></script>
<script type='text/javascript'>
var _wpcf7 = {"loaderUrl":"http:\/\/bayareacircuits.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
</script>
<script type='text/javascript' src='http://bayareacircuits.com/wp-content/plugins/contact-form-7/includes/js/scripts.js'></script>
<script type='text/javascript' src='http://bayareacircuits.com/wp-content/plugins/uBillboard/js/billboard.min.js'></script>

<script type="text/javascript">
function trackOutboundLink(link, category, action) { 
try { 
_gaq.push(['_trackEvent', category , action]); 
} catch(err){}
 
setTimeout(function() {
document.location.href = link.href;
}, 100);
}
</script>


</body>
</html>   