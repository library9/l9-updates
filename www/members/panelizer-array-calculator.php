
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="http://actpcb.com/sites/default/files/favicon.ico" type="image/vnd.microsoft.icon" />
<meta content="" about="/panelizer-array-calculator" property="dc:title" />
<link rel="canonical" href="/panelizer-array-calculator" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="shortlink" href="/node/125" />
  <title>Panelizer &amp; Array Calculator | American Circuit Technology</title>
  <style type="text/css" media="all">@import url("http://actpcb.com/modules/system/system.base.css?m1xc9w");
@import url("http://actpcb.com/modules/system/system.menus.css?m1xc9w");
@import url("http://actpcb.com/modules/system/system.messages.css?m1xc9w");
@import url("http://actpcb.com/modules/system/system.theme.css?m1xc9w");</style>
<style type="text/css" media="all">@import url("http://actpcb.com/modules/book/book.css?m1xc9w");
@import url("http://actpcb.com/modules/comment/comment.css?m1xc9w");
@import url("http://actpcb.com/modules/field/theme/field.css?m1xc9w");
@import url("http://actpcb.com/modules/node/node.css?m1xc9w");
@import url("http://actpcb.com/modules/search/search.css?m1xc9w");
@import url("http://actpcb.com/modules/user/user.css?m1xc9w");
@import url("http://actpcb.com/sites/all/modules/views/css/views.css?m1xc9w");</style>
<style type="text/css" media="all">@import url("http://actpcb.com/sites/all/modules/simplenews/simplenews.css?m1xc9w");</style>
<style type="text/css" media="all">@import url("http://actpcb.com/sites/all/themes/circuit/style.css?m1xc9w");</style>
<script type="text/javascript" src="../js/p1.js"></script>
<script type="text/javascript" src="../js/p2.js"></script>
<script type="text/javascript" src="../js/p3.js"></script>
<script type="text/javascript" src="../js/p4.js"></script>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(["_setAccount", "UA-9414211-5"]);
_gaq.push(["_trackPageview"]);
(function() {var ga = document.createElement("script");
ga.type = "text/javascript";
ga.async = true;
ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ga, s);})();

</script>
</head>

<body>


    
        <!--white bg start-->
       
		<div class="whiteWrapper floatLeft">
			
			
			<div class="page-content">

       
<div class="field-item even" property="content:encoded"><link rel="stylesheet" type="text/css" href="/calc/css/pcb.css" />

<!--script type="text/javascript" src="/calc/js/jquery.min.js"></script-->
<script type="text/javascript" src="/calc/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/calc/js/jquery.nano.js"></script>
<script type="text/javascript" src="/calc/js/pcb.js"></script>

<script type="pcb-template" id="pcb-default-template">
  <dt>Size</dt>
    <dd>Panel: {panel.width} × {panel.height}</dd>
    <dd>Part:  {part.width} × {part.height}</dd>
  <dt>Panel Yield</dt>
    <dd>{part.total} Parts Max Total</dd>
    <dd>{panel.utilization}% Panel Utilization</dd>
  <dt>Matrix</dt>
    <dd>On Panel: {matrix.width} × {matrix.height}</dd>
  <dt>Spacing</dt>
    <dd>{panel.spacing} × {panel.spacing}</dd>
  <dt>Panel Borders</dt>
    <dd>Left: {panel.left}; Right: {panel.right}</dd>
    <dd>Top: {panel.top}; Bottom: {panel.bottom}</dd>
</script>

<script type="pcb-template" id="pcb-array-template">
  <dt>Size</dt>
    <dd>Panel: {panel.width} × {panel.height}</dd>
    <dd>Array: {array.width} × {array.height}</dd>
    <dd>Part:  {part.width} × {part.height}</dd>
  <dt>Panel Yield</dt>
    <dd>{array.total} Arrays of {array.parts} Parts</dd>
    <dd>{part.total} Parts Max Total</dd>
    <dd>{panel.utilization}% Panel Utilization</dd>
    <dd>{array.utilization}% Array Utilization</dd>        
  <dt>Matrix</dt>
    <dd>On Panel: {matrix.width} × {matrix.height}</dd>
    <dd>On Array: {array_matrix.width} × {array_matrix.height}</dd>
  <dt>Spacing</dt>
    <dd>{panel.spacing} × {panel.spacing}</dd>
  <dt>Panel Borders</dt>
    <dd>Left: {panel.left}; Right: {panel.right}</dd>
    <dd>Top: {panel.top}; Bottom: {panel.bottom}</dd>

  <dt>Array Borders</dt>
    <dd>Left: {array.left}; Right: {array.right}</dd>
    <dd>Top: {array.top}; Bottom: {array.bottom}</dd>
</script>

<script type="pcb-template" id="pcb-multiple-template">
  <dt>Size</dt>
    <dd>Panel: {panel.width} × {panel.height}</dd>
    {sizes}

  <dt>Panel Yield</dt>
    {parts.totals}

    <dd>{panel.total} Parts Max Total</dd>
    <dd>{panel.utilization}% Panel Utilization</dd>

  <dt>Matrix</dt>
    {parts.matrices}

  <dt>Spacing</dt>
    <dd>{panel.spacing} × {panel.spacing}</dd>
  <dt>Panel Borders</dt>
    <dd>Left: {panel.left}; Right: {panel.right}</dd>
    <dd>Top: {panel.top}; Bottom: {panel.bottom}</dd>
</script>

<form id="pcb-form" class="pcb-form" method="get" action="">
  <h4>Panel Size</h4>
  <div class="row">
    <label>
      Width:
      <input type="text" id="panel-width" name="panel-width" value="50" />
    </label>

    <label>
      Height:
      <input type="text" id="panel-height" name="panel-height" value="25" />
    </label>
  </div>

  <h4>Panel Margins &amp; Space Between Boards</h4>
  <div class="row">
    <label>
      Left:
      <input type="text" id="panel-left" name="panel-left" value="1" />
    </label>
    <label>
      Right:
      <input type="text" id="panel-right" name="panel-right" value="1" />
    </label>
  </div>

  <div class="row">
    <label>
      Top:
      <input type="text" id="panel-top" name="panel-top" value="1" />
    </label>

    <label>
      Bottom:
      <input type="text" id="panel-bottom" name="panel-bottom" value="1" />
    </label>
  </div>

  <div class="row">
    <label>
      Spacing:
      <input type="text" id="panel-spacing" name="panel-spacing" value="1" />
    </label>
  </div>

  <h4>Calculate Type</h4>
  <div class="row">
    <label>
      Panel Type:
      <select name="type" id="pcb-type">
        <option value="default">Default</option>
        <option value="array">Panel with Arrays</option>
        <option value="multiple">Panel with Multiple Parts</option>
      </select>
    </label>
  </div>

  <div id="tabs">
    <div class="tab" rel="array" style="display: none">
      <h4>Array Size</h4>
      <div class="row">
        <label>
          Width:
          <input type="text" id="array-width" name="array-width" value="10" />
        </label>

        <label>
          Height:
          <input type="text" id="array-height" name="array-height" value="15" />
        </label>
      </div>

      <h4>Array Margins &amp; Space Between Boards</h4>
      <div class="row">
        <label>
          Left:
          <input type="text" id="array-left" name="array-left" value="1" />
        </label>
        <label>
          Right:
          <input type="text" id="array-right" name="array-right" value="1" />
        </label>
      </div>

      <div class="row">
        <label>
          Top:
          <input type="text" id="array-top" name="array-top" value="1" />
        </label>

        <label>
          Bottom:
          <input type="text" id="array-bottom" name="array-bottom" value="1" />
        </label>
      </div>

      <div class="row">
        <label>
          Spacing:
          <input type="text" id="array-spacing" name="array-spacing" value="1" />
        </label>
      </div>
    </div>
  
    <div class="tab" rel="default array">
      <h4>Part Size</h4>
      <div class="row">
        <label>
          Width:
          <input type="text" id="part-width" name="part-width" value="2.5" />
        </label>

        <label>
          Height:
          <input type="text" id="part-height" name="part-height" value="3.6" />
        </label>
      </div>
    </div>

    <div class="tab" rel="multiple" style="display: none">
      <h4>Part Sizes</h4>
      <div class="row">
        <label>
          Width:
          <input type="text" id="part-1-width" name="part-1-width" value="1" />
        </label>

        <label>
          Height:
          <input type="text" id="part-1-height" name="part-1-height" value="1" />
        </label>
      </div>

      <div class="row">
        <label>
          Width:
          <input type="text" id="part-2-width" name="part-2-width" value="2" />
        </label>

        <label>
          Height:
          <input type="text" id="part-2-height" name="part-2-height" value="2" />
        </label>
      </div>

      <div class="row">
        <label>
          Width:
          <input type="text" id="part-3-width" name="part-3-width" value="3" />
        </label>

        <label>
          Height:
          <input type="text" id="part-3-height" name="part-3-height" value="3" />
        </label>
      </div>

      <div class="row">
        <label>
          Width:
          <input type="text" id="part-4-width" name="part-4-width" value="4" />
        </label>

        <label>
          Height:
          <input type="text" id="part-4-height" name="part-4-height" value="4" />
        </label>
      </div>
    </div>
  </div>

  <input type="submit" value="Calculate" />
</form>

<div id="pcb-results">
  <canvas id="pcb-canvas" width="640" height="360"></canvas>
  <div id="pcb-output"></div>
  <div id="pcb-reportform">  
  </div>
</div>
</div> 

        <!--white bg finish-->
        
        
            
            
       
         </body>
</html>
