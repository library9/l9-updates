<?php
include("header.php");
include("sidebar.php");
include("menu.php");

?>
		<div class="side_work">	
			<div class="working_area">
<div class="status_panel">
	<div class="status_sec">

			<div class="statusbar"><p>Current Page - <strong>Glossary</strong></p></div>
			<div class="returnstat"><a href="index.php" class="menu_click">Return</a></div>

	</div>
</div>

	
<div class="app_gloss">
<div id="body-container">
  <div class="glossary-container">
    <ul class="firstUL">
      <li id="a" class="selected">A</li>
      <li id="b">B</li>
      <li id="c">C</li>
      <li id="d">D</li>
      <li id="e">E</li>
      <li id="f">F</li>
      <li id="g">G</li>
      <li id="h">H</li>
      <li id="i">I</li>
      <li id="j">J</li>
      <li id="k">K</li>
      <li id="l">L</li>
      <li id="m">M</li>
      <li id="n">N</li>
      <li id="o">O</li>
      <li id="p">P</li>
	   <li id="q">Q</li>	  
      <li id="r">R</li>
      <li id="s">S</li>
      <li id="t">T</li>
      <li id="u">U</li>
      <li id="v">V</li>
      <li id="w">W</li>
      <li id="x">X</li>
      <li id="y">Y</li>
      <li id="z">Z</li>
    </ul>
  </div>
  <div class="content-container">
    <!-- A -->
    <div id="content-for-a" style="background-color:#e9e9e9">
      <h2>A</h2>
      <p class="heading">Active Component</p>
      <p>1. A component which adds energy to the signal it passes.<br />
        2.  A device that requires an external source of power to operate upon its input signal(s). <br />
        3. Any device that switches or amplifies by the application of low-level signals.   Examples of active devices which fit one or more of the above definitions: transistors, rectifiers, diodes, amplifiers, oscillators, mechanical relays and almost all IC's (Contrast with passive component).</p>
      <p class="heading">Additive Processt</p>
      <p>Deposition or addition of conductive material onto clad or unclad base material.</p>
      <p class="heading">Ambient</p>
      <p>The surrounding environment coming into contact with the system or component in question</p>
      <p class="heading">Annular Rings</p>
      <p>The copper pad area that remains after a hole is drilled through the pad, measured from edge of hole to edge of pad.</p>
      <p>Your inner and outer layer pads should be at least 0.018" larger than the finish hole size (0.010 for vias). If your design has any pad to trace junction minimum requirement, add that to the above numbers [0.018" pad + 0.002" junction should have 0.020" pad]. This will provide a 9mil annular ring for component pins, 5mil annular ring for vias.</p>
      <p class="heading">Aperature Information</p>
      <p>This is a text file describing the size and shape of each element on the PCB printed circuit boards. These are also known as D-code lists. These lists are not necessary if your files are saved as Extended Gerber with embedded Apertures (RS274X).</p>
      <p class="heading">Aspect Ratio</p>
      <p>The ratio of the length or depth of a hole to its pre-plated diameter <br>
        (Example. 0.062" thick board 0.0135" drill = aspect ratio of 4.59:1).<br>
        <br>
        Maximum aspect ratio allowed for Sunstone Circuits manufacturing is 6:1.<br>
        <br>
        Board Thickness	Smallest Drill Size<br>
        0.031"	0.010"<br>
        0.042"	0.010"<br>
        0.062"	0.010"<br>
        0.093"	0.017"<br>
        0.125"	0.019"<br>
        Designer tip: Minimizing the aspect ratio of the holes improves through hole plating quality and minimizes the chance of via failures.</p>
      <p class="return-to-top">Return to Top</p>
    </div>
    <!-- B -->
    <div id="content-for-b">
      <h2>B</h2>
      <p class="heading">Ball Grid Array</p>
      <p>A flip-chip type of package in which the internal die terminals form a grid-style array, and are in contact with solder balls ( solder bumps ), which carry the electrical connection to the outside of the package. The PCB footprint will have round landing pads to which the solder balls will be soldered when the package and PCB are heated in a reflow oven. Advantages of the ball grid array package are (1) that its size is compact and (2) its leads do not get damaged in handling (unlike the formed "gull-wing" leads of a QFP ' ) and thus has a long shelf life. Disadvantages of the BGA are 1) they, or their solder joints, are subject to stress-related failure. For example, the intense vibration of rocket-powered space vehicles can pop them right off the PCB, 2) they can not be hand-soldered (they require a reflow oven), making first-article prototypes a bit more expensive to stuff, 3) except for the outer rows, the solder joints can not be visually inspected and 4) they are difficult to rework.</p>
      <p class="heading">Board Thickness</p>
      <p>The standard base thickness is 1/16 inch (0.062"). The following thicknesses are also available through Full Feature Product: 0.031", 0.047", 0.093", and 0.125". Overall thickness tolerance is within +/- 10% of the given thickness (for 2 and 4 layer boards) and 14% for 6 layer boards.</p>
      <p class="return-to-top">Return to Top</p>
    </div>
    <!-- C -->
    <div id="content-for-c">
      <h2>C</h2>
      <p class="heading">C4</p>
      <p>Controlled Collapsed Chip Connect. A type of flip-chip technology which is used in Intel's Pentium III and in Motorola's PowerPC 603 and PowerPC 604 RISC Microprocessors. Here is an Friday, February 07, 2003 introduction to the C4/CBGA interconnect technology by Kromann, Gerke and Huang of Motorola's Advanced Packaging Technology Division.</p>
      <p class="heading">CAD</p>
      <p>Computer Aided Design. A system where engineers create a design and see the proposed product in front of them on a graphics screen or in the form of a computer printout or plot. In electronics, the result would be a printed circuit layout.</p>
      <p class="heading">Component Library</p>
      <p>A representation of components as decals, stored in a computer data file which can be accessed by a PCB CAD program.</p>
      <p class="return-to-top">Return to Top</p>
    </div>
    <!-- D -->
    <div id="content-for-d">
      <h2>D</h2>
      <p class="heading">D Code</p>
      <p>Draft code.  1.  A datum in a Gerber file which acts as a command to a photoplotter . A D code in a Gerber file takes the form of a number prefixed by the letter D, e.g. "D20". However, in some aperture lists the D is dropped. In aperture lists of Cadstar, the column heading "Position" actually refers to D code, and the D prefix is dropped. 2.  D codes have multiple purposes. The first is to control the state of the light being on or off. Valid codes for light state are D01, D02, and D03.
        
        D01 - Light on for next move.
        D02 - Light off for next move.
        D03 - Flash (Light On, Light Off) after move (effect is limited to block in which appears, i.e. non-modal). You can also think of a D03 as D02, D01, D02 series of commands linked together.
        D codes with values of 10 or greater represent the aperture's position on the list or wheel. It is very important to understand that there is no universal "D10" or "D30". Unlike the D01 , D02, and D03 counterparts which have a fixed meaning ( draw , move, flash), D10 and higher values have aperture shapes and dimensions assigned to them by each individual user. Hence, one job's D10 could be a 10 mil Round, when another job's D10 could be a 45 mil Square.
        
        There are two distinct ways to number an aperture list. The traditional 24 aperture system started with D10 - D19, jumping suddenly to D70 - D71, then back to D20 - D29, ending with D72 -D73. This is still a common format for output for CAD packages, and is still mandatory for old 24 aperture Gerber vector Photoplotters .
        
        It is now common to start with D10, then increase numerically in steps of 1 (D10, D11, etc.) continuing up to D70 and beyond, rarely beyond 1000 individual apertures.  From Advanced Microcomputer Systems CAE Glossary</p>
      <p class="heading">Dielectric Constant</p>
      <p>The ratio of the capacitance of a capacitor with the given dielectric to the capacitance of a capacitor having air for its dielectric but otherwise identical.</p>
      <p class="heading">Differential Signaling</p>
      <p>A method of signal transmission through two wires which always have opposite states. The signal data is the polarity difference between the wires:   Whenever either is high, the other is low. Neither wire is grounded. </p>
      <p class="return-to-top">Return to Top</p>
    </div>
    <!-- E -->
    <div id="content-for-e">
      <h2>E</h2>
      <p class="heading">Excellon</p>
      <p>NC Drill file format. An ASCII format used in a file which drives an NC Drill machine. The earliest NC Drill machines were made by Excellon Automation Company. The format is in broad use, although the company has been sold.</p>
      <p class="return-to-top">Return to Top</p>
    </div>
    <!-- F -->
    <div id="content-for-f">
      <h2>F</h2>
      <p class="heading">Fabrication Drawing</p>
      <p>A drawing used to aid the construction of a printed board. It shows all of the locations of the holes to be drilled, their sizes and tolerances, dimensions of the board edges, and notes on the materials and methods to be used. Called "fab drawing" for short. It relates the board edge to at least on hole location as a reference point so that the NC Drill file can be properly lined up.</p>

      <p class="return-to-top">Return to Top</p>
    </div>
    <!-- G -->
    <div id="content-for-g">
      <h2>G</h2>
      <p class="heading">Gerber File</p>
      <p>ASCII data file used to control a photoplotter . Named after H. Joseph Gerber, founder of Gerber Scientific Co., who invented the original vector photoplotter .</p>
 <p class="return-to-top">Return to Top</p>
    </div>
    <!-- H -->
    <div id="content-for-h">
      <h2>H</h2>
      <p class="heading">HPGL</p>
  <p>Hewlett-Packard Graphics Language, a text-based data structure of pen-plot files which are used to drive Hewlett-Packard pen plotters. Although Hewlett-Packard no longer makes pen plotters, the large-format dot matrix printers which replaced them can also be driven by HPGL.</p>

	   <p class="return-to-top">Return to Top</p>
    </div>
    <!-- I -->
    <div id="content-for-i">
      <h2>I</h2>
      <p class="heading">Integrated Circuit</p>
<p>1)miniaturized electronic circuit that has been manufactured as a chip ( die ). 2)A packaged chip.</p>
      <p class="heading">Internal Signal Layers</p>
      <p>Routing layers composed of signal traces that are contained below the surface layers of your board. These layers are connected to the surface layers by plated through holes and/or vias.

We may remove any pads that are not connected to traces on your internal signal layers in order to expedite the imaging process on Full-Featured Orders. Many customers upload their files with these non-functional pads already removed, which expedites the tooling process. If for any reason, you do not wish these pads to be removed, please clarify this by listing it as a drawing note, or in a "readme" file.</p>
     <p class="heading">IPC</p>
      <p>The Institute for Interconnecting and Packaging Electronic Circuits, the final American authority on how to design and manufacture printed wiring.   In 1999, IPC changed its name from Institute of Interconnecting and Packaging Electronic Circuits to IPC. The new name is accompanied with an identity statement, Association Connecting Electronics Industries. </p>
   <p class="return-to-top">Return to Top</p>	  
    </div>
    <!-- J -->
    <div id="content-for-j">
      <h2>J</h2>
      <p class="heading">Jumper Wire</p>
      <p>An electrical connection formed by wire between two points on a printed board added after the intended conductive pattern is formed.</p>
	     <p class="return-to-top">Return to Top</p>

    </div>
    <!-- K -->
    <div id="content-for-k">
      <h2>K</h2>
      <p class="heading">KGB</p>
<p>(Known Good Board)- A board or assembly that is verified to be free of defects. Also known as a Golden Board.</p>
      <p class="heading">Kerf</p>
    <p>A widening of the rout path as may be called out on the blueprint. Allows extra space for hardware to be attached to the board.</p>
		     <p class="return-to-top">Return to Top</p>
    </div>
    <!-- L -->
    <div id="content-for-l">
      <h2>L</h2>
      <p class="heading">Laminate</p>
<p>A product made by bonding together two or more layers of material.</p>
      <p class="heading">LPI</p>
<p>Liquid Photo Imageable. Refers to liquid photo imagable solder mask.</p>
  <p class="return-to-top">Return to Top</p>
    </div>
    <!-- M -->
    <div id="content-for-m">
      <h2>M</h2>
      <p class="heading">Mask</p>
      <p>The measurement of physical properties, usually including pressure, temperature and well bore trajectory in three-dimensional space, while extending a well bore. The measurements are made downhole and transmitted to the surface. Data transmission methods vary, but usually involve digitally encoding data and transmitting to the surface as pressure pulses in the mud system. These pressures may be positive, negative or continuous sine waves. Some MWD tools have the ability to store the measurements for later retrieval when the tool is tripped out of the hole if the data transmission link fails.</p>
      <p class="heading">Microvia</p>
  <p>A via used to make connection between two adjacent layers, typically less than 6 mils in diameter. May be formed by laser ablation, plasma etching, or photo processing.</p>
      <p class="heading">Minimum Annular Ring</p>
  <p>The minimum metal width, at the narrowest point, between the circumference of the hole and the outer circumference of the land. This measurement is made to the drilled hole on internal layers of multilayer printed circuit boards and to the edge of the plating on outside layers of multilayer boards and double-sided boards.</p>
      <p class="heading">Multilayer Circuit Board</p>
  <p>The general term for completely processed printed circuit configurations consisting of alternate layers of conductive patterns and insulating materials bonded together in more than two layers.</p>    
	    <p class="return-to-top">Return to Top</p>
    </div>
    <!-- N -->
    <div id="content-for-n">
      <h2>N</h2>
      <p class="heading">Negative</p>
      <p>The flared condition of copper on the inner conductor layers of a multilayer board caused by hole drilling.</p>
      <p class="heading">Netlist</p>
      <p>List of names of symbols or parts and their connection points which are logically connected in each net of a circuit. A netlist can be "captured" (extracted electronically on a computer) from a properly prepared CAE schematic.</p>
      <p class="heading">Nomenclature</p>
      <p>Identification symbols applied to the board by means of screen printing ink jetting see legend.</p>	  
	    <p class="return-to-top">Return to Top</p>
    </div>
    <!-- O -->
    <div id="content-for-o">
      <h2>O</h2>
      <p class="heading">Outer Layer</p>
<p>The top and bottom sides of any type of circuit board.</p>
      <p class="heading">Outgassing</p>
      <p>Deaeration or other gaseous emission from a printed circuit board when exposed to the soldering operation or to vacuum.</p>
      <p class="heading">Oxide</p>
      <p>A chemical treatment to inner layers prior to lamination, for the purpose of increasing the roughness of clad copper to improve laminate bond strength.</p>	  
	    <p class="return-to-top">Return to Top</p>
    </div>
    <!-- P -->
    <div id="content-for-p">
      <h2>P</h2>
      <p class="heading">Pad</p>
      <p>The portion of the conductive pattern on printed circuits designated for the mounting or attachment of components. Also called Land.</p>
      <p class="heading">Panel</p>
<p>The square or rectangular base material containing one or more circuit patterns that passes successively through the production sequence and from which printed circuit boards are extracted. See Back Planes and Panels .typically 12" by 18" or 18" by 24"</p>

      <p class="heading">Panelize</p>
<p>1. To lay up more than one (usually identical)printed circuits on a pans. Individual printed circuits on a panel need a margin between them of 0.3". Some board houses permit less separation. 2. Lay up multiple printed circuits (called modules) into a sub-panel so that the sub-panel can be assembled as a unit. The modules can then be separated after assembly into discrete printed circuits.</p>

      <p class="heading">Photo mask</p>
<p>A silver halide or diazo image on a transparent substrate that is used to either block or pass light.</p>
      <p class="heading">Plated-Through Hole</p>
<p>(PTH) A hole in a circuit board that that been plated with metal (usually copper) on its sides to provide electrical connections between conductive patterns layers of a printed circuit board.</p>
      <p class="heading">Pre-preg</p>
<p>Sheet material consisting of the base material impregnated with a synthetic resin, such as epoxy or Polymide, partially cured to the B-stage (an intermediate stage). Short for pre-impregnated. See also B-stage.</p>
	    <p class="return-to-top">Return to Top</p>
    </div>
    <!-- Q -->
    <div id="content-for-q">
      <h2>Q</h2>
      <p class="heading">QFP</p>
      <p>Quad Flat Pack, a fine-pitch SMT package that is rectangular or square with gull-wing shaped leads on all four sides. The lead pitch of a QFP is typically either 0.8mm or 0.65mm, although there are variations on this theme with smaller lead pitches: TQFP also 0.8mm; PQFP tooled at either 0.65mm (0.026") or 0.025" and SQFP at 0.5mm (0.020"). Any of these packages can have a wide variety of lead counts from 44 leads on up to 240 or more. Although these terms are descriptive, there are no industry- wide standards for sizes. Any printed circuit designer will need a spec sheet for the particular manufacturer's part, as a brief descrition like "PQFP-160" is inadequate to define the mechanical size and lead pitch of the part.</p>

	    <p class="return-to-top">Return to Top</p>
    </div>
    <!-- R -->
    <div id="content-for-r">
      <h2>R</h2>
      <p class="heading">Registration</p>
<p>The degree of conformity of the true position of a pattern with its intended position or with that of any other conductor layer of a board.</p>
      <p class="heading">Reverse Image</p>
   <p>The resist pattern on a printed circuit board enabling the exposure of conductive areas for subsequent plating</p>
	    <p class="return-to-top">Return to Top</p>
    </div>
    <!-- S -->
    <div id="content-for-s">
      <h2>S</h2>
      <p class="heading">Schematic Diagram</p>
<p>A drawing that shows, by means of graphic symbols, the electrical connections, components, and functions of an electronic circuit.</p>
      <p class="heading">Selective Plate</p>
      <p>A process for plating unique features with a different metal than the remaining features will have. Created by imaging, exposing, and plating selected area and then repeating the process for the remainder of the board.</p>
      <p class="heading">SMOBC</p>
      <p>Solder mask over bare copper a method of fabricating a printed circuit board which results in final metallization under the solder mask being copper with no protective metal, The non coated areas are coated by solder resist, exposing only the component terminal areas, this eliminates tin lead under the components.</p>
      <p class="heading">Solder Mask</p>
      <p>A coating applied to a circuit board to prevent solder from flowing onto any areas where it`s not desired or from bridging across closely spaced conductors.</p>
      <p class="heading">Stacked vias</p>
      <p>Micro vias in HDI that are stacked one upon each other.</p>	  	  	  
	    <p class="return-to-top">Return to Top</p>
    </div>
    <!-- T -->
    <div id="content-for-t">
      <h2>T</h2>
      <p class="heading">T/g</p>
      <p>Glass transition temperature, in degree C the point at which the material starts to become soft and plastic like, also the point where the z axis starts to expand in linearly.</p>
      <p class="heading">Tented via</p>
      <p>A via with dry film solder mask completely covering both its pad and its plated-thru hole. This completely insulates the via from foreign objects, thus protecting against accidental shorts, but it also renders the via unusable as a test point. Sometimes vias are tented on the top side of the board and left uncovered on the bottom side to permit probing from that side only with a test fixture.</p>
      <p class="heading">Tooling Holes</p>
      <p>Two specified holes on a printed circuit board used to position the board in order to mount components accurately.</p>	  
	    <p class="return-to-top">Return to Top</p>
    </div>
    <!-- U -->
    <div id="content-for-u">
      <h2>U</h2>
      <p class="heading">Underwriters Symbol</p>
      <p>A logotype denoting that the product has been recognized by Underwriters Laboratory, Inc. (UL).</p>

	    <p class="return-to-top">Return to Top</p>
    </div>
    <!-- V -->
    <div id="content-for-v">
      <h2>V</h2>
      <p class="heading">Via</p>
      <p>A plated thru-hole that is used as an inner-layer connection but doesn`t have component lead or other reinforcing material in it. Vias can make an electrical connection between layers on a PCB.</p>
      <p class="heading">Void</p>
      <p>The absence of substances in a localized area (e.g., air bubbles).</p>
	    <p class="return-to-top">Return to Top</p>
    </div>
    <!-- W -->
    <div id="content-for-w">
      <h2>W</h2>
      <p class="heading">Wave Soldering</p>
      <p>A process wherein assembled printed boards are brought in contact with a continuously flowing and circulating mass of solder.</p>
      <p class="heading">Wicking</p>
      <p>Migration of conductive copper chemicals into the glass fibers of the insulating material around a drilled hole.</p>
	    <p class="return-to-top">Return to Top</p>
    </div>
    <!-- X -->
    <div id="content-for-x">
      <h2>X</h2>
      <p class="heading">X-Axis</p>
      <p>The horizontal or left-to-right direction in a two-dimensional system of coordinates.</p>

	    <p class="return-to-top">Return to Top</p>
    </div>
    <!-- y -->
    <div id="content-for-y">
      <h2>Y</h2>
      <p class="heading">Y-Axis</p>
      <p>The vertical or bottom-to-top direction in a two-dimensional system of coordinates.</p>

	    <p class="return-to-top">Return to Top</p>
    </div>
    <!-- V -->
    <div id="content-for-z">
      <h2>Z</h2>
      <p class="heading">Zero Defects Sampling</p>
      <p> statistical based attribute sampling plan (C = O) where a given sample of parts is inspected and any defects found are cause for rejection of the entire lot.</p>

	    <p class="return-to-top">Return to Top</p>
    </div>
  </div>
</div>

<!-- jQuery -->
<script language="javascript" type="text/javascript" src="js/jquery1.js"></script>
<!-- scrollTo Plugin -->
<script language="javascript" type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	 //below code is for high-lighting the link and scroll to particular DOM Element as well
 	 $(".firstUL li").each(function() { 
		$(this).click(function() { //On click of any Alphabet
			$(".firstUL li").removeClass("selected"); //Initially remove "selected" class if any
			$(this).addClass("selected"); //Add "selected" class for the clicked one
			elementClick = $(this).attr("id"); //get respective 'Id' for example 'a','b','c'.. etc.,
			$(".content-container").scrollTo($("#content-for-"+elementClick), 800); //scrollTo particular DOM Element
			$(".content-container div").css({'background-color' : '#e9e9e9'}); //set the background color to default, that is white
			$(".content-container #content-for-"+elementClick).css({'background-color' : '#ffffff'}); //set the background color to light-blue to that div
		});
	 });
	 
	 //When "Return to Top" is clicked highlight the first Alphabet that 'A' and scroll to top.
	 $('.return-to-top').click(function(){
		$(".firstUL li").each(function() { 
			$(".firstUL li").removeClass("selected"); //Remove classname "selected"
		});
		$("#a").addClass("selected"); //Add a class named "selected" to the first Alphabet
	 	$(".content-container").scrollTo($("#content-for-a"), 800); //This is for scrolling to particular element that is "A" here...
		$(".content-container div").css({'background-color' : '#e9e9e9'}); //set the background color to default, that is white
		$(".content-container #content-for-a").css({'background-color' : '#ffffff'}); //set the background color to light-blue to that div
	 });
});
</script>

<!-- end content -->
</div>
</div>
</div>
