<?php
$groupswithaccess="PUBLIC";
$loginpage="http://library9.com";

$logoutpage="index.php"; 
$loginredirect=2;
require_once("slpw/sitelokpw.php");
//test comment
 
?>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Library9.com</title>
<link rel="shortcut icon" href="/images/favicon.png"/>
<link href="styles.css" rel="stylesheet" type="text/css" media="screen" />
<!--<script type="text/javascript" src="javascript/jquery-1.6.1.min.js"></script>-->
  
    <script src="js/jquery.min.js"></script>

	  
<style type="text/css">
	* {
		margin: 0;
		padding: 0;
	}
	/* @group Customer Login Form */
	/* customer login boxes */
	h1.loginboxcust {
		padding: 0 0 0 0;
		margin: -10px 0 20px 0;
		color: #ebebeb;
		font: bold 30px Arial,Helvetica,sans-serif;
		text-align: left;
	}
	label.loginboxcust {
		display: inline-block;
		position: relative;
		width: 96px;
		text-align: left;
		margin: 0 0 0 0;
		padding: 10px 10px 0 0;
		vertical-align: middle;luser
		text-transform: capitalize;
		font-variant: small-caps;
		font-family: "Lucida Grande",Lucida,Verdana,sans-serif;
		color: #646464;
	}
	input.loginboxcust {
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 2px 3px 3px #0f0f0f;
		-moz-box-shadow: 2px 3px 3px #0f0f0f;
		box-shadow: 2px 3px 3px #0f0f0f;
		display: inline-block;
		position: relative;
		font-size: 14px;
		width: 204px;
		height: 20px;
		line-height: 20px;
		padding: 2px;
		margin: 10px 0 0 0;
		border: 1px solid #0d2c52;
		background-color: #c4c4c4;
		font-size: 16px;
		color: #444545;
		vertical-align: middle;
	}
	input.loginboxcust:focus {
		border: 1px solid #00b1ed;
		background-color: #eff2f1;
	}
	/* end login boxes for customer page */
	

	/* @group LogoutButton	 */
		a.buttonlogout {
			-moz-box-shadow:inset 0px 1px 0px 0px #f29c93;
			-webkit-box-shadow:inset 0px 1px 0px 0px #f29c93;
			box-shadow:inset 0px 1px 0px 0px #f29c93;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #fe1a00), color-stop(1, #ce0100) );
			background:-moz-linear-gradient( center top, #fe1a00 5%, #ce0100 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fe1a00', endColorstr='#ce0100');
			background-color:#fe1a00;
			-moz-border-radius:6px;
			-webkit-border-radius:6px;
			border-radius:6px;
			border:1px solid #d83526;
			display:inline-block;
			color:#ffffff;
			font-family:arial;
			font-size:14px;
			font-weight:bold;
			padding:6px 24px;
			text-decoration:none;
			text-shadow:1px 1px 0px #b23e35;
		}
		a.buttonlogout:hover {
			background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ce0100), color-stop(1, #fe1a00) );
			background:-moz-linear-gradient( center top, #ce0100 5%, #fe1a00 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ce0100', endColorstr='#fe1a00');
			background-color:#ce0100;
		}
		a.buttonlogout:active {
			position:relative;
			top:1px;
		}
			a.buttonpassword {
			-moz-box-shadow:inset 0px 1px 0px 0px #f29c93;
			-webkit-box-shadow:inset 0px 1px 0px 0px #f29c93;
			box-shadow:inset 0px 1px 0px 0px #f29c93;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #fe1a00), color-stop(1, #ce0100) );
			background:-moz-linear-gradient( center top, #fe1a00 5%, #ce0100 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fe1a00', endColorstr='#ce0100');
			background-color:#fe1a00;
			-moz-border-radius:6px;
			-webkit-border-radius:6px;
			border-radius:6px;
			border:1px solid #d83526;
			display:inline-block;
			color:#ffffff;
			font-family:arial;
			font-size:11px;
			font-weight:bold;
			height: 15px;
			line-height: 15px;
			padding: 4px 10px;
			text-decoration:none;
			text-shadow:1px 1px 0px #b23e35;
			margin-top: 5px;
		margin-left: 108px;
		}
		a.buttonpassword:hover {

		}
		a.buttonpassword:active {
			position:relative;
			top:1px;
		}

	input[type="submit"] {
	
		-moz-border-radius:4px;
		-webkit-border-radius:4px;
		border-radius:4px;
		border:1px solid #84bbf3;
		display:inline-block;
		color:#fff;
		font-family:arial;
		font-size:14px;
		font-weight:bold;
		padding:8px 0px;
		text-decoration:none;

		margin-top: 10px;
		margin-left:0px;
		width:210px;
	}
	input[type="submit"]:hover {

		background-color:#3792bf;
	}
	input[type="submit"]:active {
		position:relative;
		top:1px;
		}
	/* @end */
	</style>
<style type="text/css">
#content{
	height:467px;
}
button, input[type="button"] {
    background-color: #eeeeee;
    border: 1px solid #999999;
    border-radius: 5px;
    box-shadow: 1px 1px 3px #cccccc;
    color: #333333;
    cursor: pointer;
    font-family: tahoma,sans-serif;
    font-size: 21px;
    height: 65px;
    margin: 111px 300px;
width: 200px;
}
    
</style> 
</head>
<body>
<div id="header-ff">
<div id="headercontainer">


    </div><!-- end navigation -->
</div><!-- end headercontainer -->

<div id="content">
<div id="logoutcontentcontainer">
<br>
<div id="droplineMenu">
<ul id="menuOuter">
	<div class="menu_logo">
	<a href="index.php"><img src="/images/nav_logo.png" alt="Demo Library" border="0" width="65" height="60" style="margin-top:10px;"></a>
	
	</div>
 

	
	</div>
	
	</ul>
</div>	

