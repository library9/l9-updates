$gv_.currentprovider="";// (C) Copyright 2000 - 2007 Morfik Technology

function geteventtype     (a){return a.type}
function runscript        (a){eval(a)}
function lEngTH           (a){return a.length}
function createactivex    (c){try{return new ActiveXObject(c)}catch(e){return null}}
function createNSXMLDoc   (d) {return d.implementation.createDocument("", "", null)}
function canCreateNSXMLDoc(d) {return (d.implementation && d.implementation.createDocument) }
function getclasstype     (a){return a.zc}
function getclassname     (a){return a.zb}
function parseDate        (s){var m=Date.parse(s); if(isNaN(m))m=-1; return m}
function CreateDateTimeObj(){return new Date()}
function createRegExp     (s,f){return new RegExp(s,f)}
function defined          (c){return typeof(c)!="undefined";}
function debuggerBreak    (){debugger}
function createHash       (){var r = {}; return r;}

function free(o){if (typeof(o)=="object"&&o&&o.zd)o.zd();}

function getSafeDomain(d){
   var k = d.split(".");
   var b = false;
   for(var i = k.length - 1; i >= 0; i--){
      if(i == k.length - 1){
         d = k[i]
      }else{
         d = k[i] + "." + d;
      }
      if(!b){
         b=(".com.org.net.co.biz".search(k[i]) != -1);
      }else{
         return d;
      }
   }
   return d;
}

function assigned(c){
   if(typeof(c)=="undefined")return false;
   if(c!=void(0)){
      if(typeof(c._o)=="undefined")return true;
      return(c._o&&c._p);
   }else return false;
}

//using the amazing javascript closure technique!!
function getMethodPointer(o,m){
     if(m&&o)
      return function(){
                var bi=-1,ri=-1;

                var source=this;
                if (assigned(window.event))
                   if(assigned(window.event.srcElement))
                      source=window.event.srcElement;

                if (assigned(source.bandindex))
                   bi=source.bandindex

                if (assigned(source.repeaterindex))
                   ri=source.repeaterindex

                if (assigned(source.wrapper))
                   if(assigned(source.wrapper.bandindex))
                      if(source.id.toLowerCase().search('maindiv')==-1)
                        {source.wrapper.bandindex=bi;source.wrapper.repeaterindex=ri;}

                //firefox
                if(arguments.length>0){
                   if(arguments[0]!=null)
                      {arguments[0].bandindex=bi;arguments[0].repeaterindex=ri;}
                   return m.apply(o,arguments);
                }
                else
                //IE
                if (assigned(window.event)){
                   window.event.bandindex=bi;window.event.repeaterindex=ri;
                   return m.apply(o,[window.event]);
                }
                else
                   return m.apply(o,[this]);
             }
     else
      return null;
}


function createXMLHTTPRequest(){
   if (window.XMLHttpRequest)
       return new XMLHttpRequest();
   if (window.ActiveXObject)
       return new ActiveXObject('Microsoft.XMLHTTP');
   return null;
}

function getEvt(t,e){
    if(e)return e;
    function WE(w){if(!w)return null;return w.event}   
    e=WE(t.ownerwin); 
    if(!e)e=WE(t.parentWindow); 
    if(!e)if(t.document!=void(0))e=WE(t.document.parentWindow);
    if(!e)e=WE(_mw());
    return e;
}

function TBrowser(){
    var n=0;var majv=0;var minv=0;
    var UA=navigator.userAgent.toUpperCase();
    this.engine       = '';
    this.engineid     = '';
    this.version      = 0;
    this.minorVersion = 0;
    this.platform     = '';

    function readVersion(m){
        var a,b;
        while ((n<UA.length)&&                    (" ./"       .indexOf(UA.charAt(n))!=-1)) n+=1; a=n; 
        while ((n<UA.length)&&((m==-1)||(n-a<m))&&("0123456789".indexOf(UA.charAt(n))!=-1)) n+=1;
        if((m!=-1)&&(n-a>m))b=a+m; else b=n;
        try {return parseInt(UA.slice(a,b));} catch(x) {return 0;}    
    } 

    function checkEngine(s,m1,m2) {
        n = UA.indexOf(s);
        if(n==-1) return false;
        n = n + s.length + 1;
        majv = readVersion(m1);
        minv = readVersion(m2);
        return true;
    }

    if (UA.indexOf ('WIN'  )>0) this.platform = 'WIN'; else
    if (UA.indexOf ('MAC'  )>0) this.platform = 'MAC'; else
    if (UA.indexOf ('LINUX')>0) this.platform = 'LNX';
    if (checkEngine('SAFARI'   ,-1,-1)){this.engine = 'SAFARI';   this.engineid = 'SF';} else
    if (checkEngine('OPERA'    ,-1,-1)){this.engine = 'OPERA';    this.engineid = 'OP';} else
    if (checkEngine('MSIE'     ,-1,-1)){this.engine = 'MSIE';     this.engineid = 'MS';} else
    if (checkEngine('KONQUEROR',-1,-1)){this.engine = 'KONQUEROR';this.engineid = 'KQ';} else
    if (checkEngine('GECKO'    ,4 ,2 )){this.engine = 'GECKO';    this.engineid = 'GK';}
    this.version  = majv;
    this.minorVersion = minv;
    this.identifier = this.engine + '(' + this.engineid + ') ' + this.version + '.' + this.minorVersion + ' ' + this.platform;

    this.IsIE     = (this.engineid == 'MS');
    this.IsGecko  = (this.engineid == 'GK');
    this.IsOpera  = (this.engineid == 'OP');
    this.IsSafari = (this.engineid == 'SF');
    this.IsChrome = UA.indexOf("CHROME")>0;
    var p = navigator.platform?navigator.platform:"";
    this.IsiPad   = (p == "iPad");
    this.IsiPod   = (p == "iPod");
    this.IsiPhone = (p == "iPhone");  
    this.IsMobileSafari = this.IsSafari && (UA.indexOf('MOBILE')>0);
}
var bRoWsEr=new TBrowser();

function dbgEval(o,f){
  var s=""; 
  for(k in o){
      if(typeof(o[k])=="function"){
         if(f){s += k + "~~function\n"}
      }else{
         s += k + "~~" + o[k] + "\n"
      }
  }
  return s;
}

function functionname(func) {
  if ( typeof func == "function" || typeof func == "object" )
     var fName = (""+func).match(/function\s*([\w\$]*)\s*\(/);
  if ( fName !== null ) return fName[1];
}

var IsListRegistered=false;
function tlist(){}
function CreateList(InitFn,AssignFn,CreateFn,ConstructFn,IsDual){
    if (!IsListRegistered){
       tlist.prototype=q=new Object();
       q.grow        =tlgrow;
       q.setcapacity =tlsetcapacity;
       q.setcount    =tlsetcount;
       q.put         =tlput1;
       q.get         =tlget1;
       q.add         =tladd;
       q.checkindex  =tlcheckindex;
       q.clear       =tlclear;
       q.Destroy     =tldestroy;
       q.free        =tlfree;
       q.delete_     =tldelete;
       q.indexof     =tlindexof;
       q.insert      =tlinsert;
       q.pack        =tlpack;
       q.remove      =tlremove;
       q.sort        =tlsort;
       q.find        =tlfind;
       q.addnewitem  =tladdnewitem;
       q.EOF         =tlEOF;
       q.first       =tlfirst;
       q.last        =tllast;
       q.next        =tlnext;
       q.previous    =tlprevious;
       q.currentitem =tlcurrentitem;
       q.addressof   =tladdressof;

       q.fcapacity          = 0;
       q.fcount             = 0;
       q.fitemindex         = -1;
       q.fmemberInit        = null;
       q.fmemberAssign      = null;
       q.fmemberCreate      = null;
       q.fmemberConstructor = null;
       q.fIsDual            = true;
       q.fastMembers        = null;
       q.members            = null;

       IsListRegistered=true;
    }

    var p = new tlist();

    p.fcapacity          = 0;
    p.fcount             = 0;
    p.fitemindex         = -1;

    p.fmemberInit        = null;
    p.fmemberAssign      = null;
    p.fmemberCreate      = null;
    p.fmemberConstructor = null;
    p.fIsDual            = true;

    p.members            = new Object();
    p.members.v          = new Array ();
    p.fastMembers        = p.members.v;
    if(InitFn     !=void(0)){p.fmemberInit        =InitFn}
    if(AssignFn   !=void(0)){p.fmemberAssign      =AssignFn}
    if(CreateFn   !=void(0)){p.fmemberCreate      =CreateFn}
    if(ConstructFn!=void(0)){p.fmemberConstructor =ConstructFn}
    if(IsDual     !=void(0)){p.fIsDual            =IsDual}
    if(!p.fIsDual){
        p.put=tlput2;
        p.get=tlget2;
    }
    return p
}

function createfastlist(){
   return CreateList(null,null,null,null,false);
}

function setlength(a,l,InitFn,dual){
  var lth=a.length;
  if(l==lth)return;

  if(dual==void(0))dual=false;

  if(InitFn!=void(0)){
     for(var i=lth;i<l;i++){
        a[i]=InitFn(dual);
     }
     return
  }

  if(dual){
     for(var i=lth;i<l;i++){
           a[i]  =new Object();
           a[i].v=null;//new Object();
     }
     return
  }

  if ("IsArrayOfRecords" in a){
      for(var i=lth;i<l;i++){
          a[i]={};
      }
      return
  }

  a.length=l;
}

function tlsetcapacity(newCapacity){
    if(newCapacity<this.fcount)return
    if(newCapacity!=this.fcapacity){
        setlength(this.fastMembers,newCapacity,this.fmemberInit,this.fIsDual);
        this.fcapacity=newCapacity;
    }
}

function tlgrow(){
    this.setcapacity(this.fcapacity+1);
}

function tlsetcount(newCount){
    if(newCount == this.fcount   )return
    if(newCount <  0             )return
    if(newCount >  this.fcapacity){this.setcapacity(newCount)}
//    this.fastMembers.length=newCount
    if(newCount < this.fcount){
       for(var i=newCount;i<this.fcount;i++){
          this.put(i,null);
       }
    }
    this.fcount = newCount;
}

function tlput1(Inx,Item){
    if (this.fmemberAssign!=null){this.fmemberAssign(Item,this.fastMembers[Inx])}
                            else {this.fastMembers[Inx].v=Item}
}

function tlget1(Inx){
    return this.fastMembers[Inx].v
}

function tlput2(Inx,Item){
   if (this.fmemberAssign!=null){this.fmemberAssign(Item,this.fastMembers[Inx])}
                           else {this.fastMembers[Inx]=Item}
}

function tlget2(Inx){
   return this.fastMembers[Inx]
}

function tladd(Item){
    with(this){
        if(fcount==fcapacity)grow()
        if(fmemberAssign!=null){fmemberAssign(Item,get(fcount))}
                          else {put(fcount,Item)}
        fcount+=1;
    }
}

function tlcheckindex(i){
    return ((i >= 0) && (i < this.fcount));
}

function tlclear(){
    this.setcount(0);
}

function tlfree(){
  this.clear();
  this.Destroy();
}

function tldestroy(){
//     if(this.fmemberAssign!=null){for(var i=0;i<this.fcapacity;i++){this.fmemberAssign(null,this.fmemberAssign[i]);}} 
//                            else {for(var i=0;i<this.fcapacity;i++){this.fastMembers[i].v=null;}}
     this.fastMembers   = null;
     this.members.v     = null;
     this.members       = null;
}

function tldelete(Inx){
    if(!this.checkindex(Inx)){return}

    if (this.fitemindex >= Inx){
       this.fitemindex--;
    }

    while(Inx < this.fcount-1){
       if (this.fmemberAssign!=null)
          this.fmemberAssign(this.fastMembers[Inx+1],this.fastMembers[Inx])
       else
       if (this.fIsDual)
           this.fastMembers[Inx].v = this.fastMembers[Inx+1].v;
       else
           this.fastMembers[Inx] = this.fastMembers[Inx+1];
       Inx=Inx+1;
    }
    this.fcount=this.fcount-1;
}

function tlindexof(Item){
    var result=null;
    result=0;
    while((result < this.fcount) && (this.get(result) != Item)){
        result=result + 1;
    }
    if(result == this.fcount){
        result=-1;
    }
    return result;
}

function tlinsert(Inx,Item){
    var i=null;
    if((Inx < 0) || (Inx > this.fcount)){
        return;
    }
    if(this.fcount == this.fcapacity){
        this.grow();
    }
    if(Inx < this.fcount){
        i=this.fcount;
        while(i > Inx){
            this.put(i,this.get(i-1));
            i=i-1;
        }
    }
    if (this.fmemberAssign!=null){this.fmemberAssign(Item,this.get(Inx))}
                            else {this.put(Inx,Item)}

    this.fcount=this.fcount + 1;
}

function tlpack(){
    var arrayhasholes=false;
    var i=0;
    while(i < this.fcount){
        if(this.fastMembers[i] == null){
            arrayhasholes=true;
            if(i < this.fcount - 1){
/*
                  if (this.fmemberAssign!=null){
                    this.fmemberAssign(this.get(i+1),this.get(i  ));
                    this.fmemberAssign(null         ,this.get(i+1));
                  } else {
                    this.put(i,this.get(i+1));
                    this.put(i+1,null);
                  }
*/
                  this.put(i,this.get(i+1));
                  this.put(i+1,null);
            }
        }
        i=i + 1;
    }
    if(!arrayhasholes){
        return;
    }
    i=0;
    while(i < this.fcount){
        if(this.get(i) == null){
            this.fcount=i;
            return;
        }else
            i=i + 1;
    }
}

function tlremove(Item){
    var result=null;
    result=this.indexof(Item);
    this.delete_(result);
    return result;
}


function tlsort(compare){
    function dosort(a,l,r){
        var i,j,k,p,t=null;
        do{
            i=l;
            j=r;
            k=(l+r) >> 1;
            p=a.get(k);
            do{
                while(compare(a.get(i),p) < 0){
                    i=i+1;
                }
                while(compare(a.get(j),p) > 0){
                    j=j-1;
                }
                if(i<=j){
                    t=a.get(i);
                    a.put(i,a.get(j));
                    a.put(j,t);
                    i=i+1;
                    j=j-1;
                }
            }while(!(i>j))
            if(l<j){
                dosort(a,l,j);
            }
            l=i;
        }while(!(i>=r))
    }

    if(this.fcount > 0){
        dosort(this,0,(this.fcount - 1));
    }
}

function tlfind(Item,compare,Inx){
    var L,H,I,C=null;
    L      = 0;
    H      = this.fcount - 1;
    while(L<=H){
        I = (L + H) >> 1;
        C = compare(this.get(I), Item);
        if(C<0){
           L=I+1
        } else {
            H=I-1;
            if(C==0){
              L     =I;
              Inx.v =I;
              return true;
            }
        }
    }
    Inx.v = L;
    return false;
}


function tladdnewitem(){
   if(this.fcount == this.fcapacity){this.grow()}
   if (this.fmemberCreate !=null){
      var m=this.fmemberCreate();
      if (this.fmemberConstructor!=null){
         m.x = this.fmemberConstructor;
         m.x();
         m.x=null;
      }
      this.add(m)
   } else{
      this.setcount(this.fcount+1);
   }
}

function tlEOF(){
    if (this.fcount==0){
      return true;
    }
    if (this.fitemindex >= this.fcount){
      return true;
    }
    return false;
}

function tlfirst(){
    if (this.fcount==0){
      this.fitemindex = -1;
      return;
    }
    this.fitemindex = 0;
}

function tllast(){
    if (this.fcount==0){
      this.fitemindex = -1;
      return;
    }
    this.fitemindex = this.fcount-1;
}

function tlnext(){
    if (this.fcount==0){
      this.fitemindex = -1;
      return;
    }
    this.fitemindex = this.fitemindex+1;
}

function tlprevious(){
    if (this.fcount==0){
      this.fitemindex = -1;
      return;
    }
    this.fitemindex = this.fitemindex-1;
    if (this.fitemindex<0){
      this.fitemindex=0;
    }
}

function tlcurrentitem(){
    if (this.fcount==0){
      return null;
    }
    return this.get(this.fitemindex);
}

function tladdressof(Inx){
    return this.fastMembers[Inx]
}

function XB_insertAdjacentHTML(element,position,html){

   if (element.insertAdjacentHTML!=void(0)){
       element.insertAdjacentHTML(position,html);
   }
   else
   if (document.createRange!=void(0)){
       var fragment;
       var parent;
       var range;
       var s;

       range = document.createRange();
       range.selectNode(element);

       fragment = range.createContextualFragment(html);
       parent   = element.parentNode;
       s        = position.toLowerCase();

       if (s == 'beforebegin'){return parent .insertBefore(fragment,element              )}
       if (s == 'afterbegin' ){return element.insertBefore(fragment,element.childNodes[0])}
       if (s == 'beforeend'  ){return element.appendChild (fragment                      )}
       if (s == 'afterend'   ){return parent .insertBefore(fragment,element.nextSibling  )}
   }
}

function getDomainSuffix(d){
   var k=d.split("."); var b=false;
   for(var i=k.length-1;i>=0;i--){
      if(i==k.length-1){d=k[i]}else{d=k[i]+"."+d};
      if(!b){b=(".com.org.net.co.biz".search(k[i])!=-1)}else{return d}}
   return d
}

if (!Array.prototype.filter)
{
  Array.prototype.filter = function(fun /*, thisp*/)
  {
    var len = this.length;
    if (typeof fun != "function")
      throw new TypeError();

    var res = new Array();
    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in this)
      {
        var val = this[i]; // in case fun mutates this
        if (fun.call(thisp, val, i, this))
          res.push(val);
      }
    }

    return res;
  };
}

if (!Array.prototype.every)
{
  Array.prototype.every = function(fun /*, thisp*/)
  {
    var len = this.length;
    if (typeof fun != "function")
      throw new TypeError();

    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in this &&
          !fun.call(thisp, this[i], i, this))
        return false;
    }

    return true;
  };
}

if (!Array.prototype.forEach)
{
  Array.prototype.forEach = function(fun /*, thisp*/)
  {
    var len = this.length;
    if (typeof fun != "function")
      throw new TypeError();

    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in this)
        fun.call(thisp, this[i], i, this);
    }
  };
}

if (!Array.prototype.lastIndexOf)
{
  Array.prototype.lastIndexOf = function(elt /*, from*/)
  {
    var len = this.length;

    var from = Number(arguments[1]);
    if (isNaN(from))
    {
      from = len - 1;
    }
    else
    {
      from = (from < 0)
           ? Math.ceil(from)
           : Math.floor(from);
      if (from < 0)
        from += len;
      else if (from >= len)
        from = len - 1;
    }

    for (; from > -1; from--)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

if (!Array.prototype.map)
{
  Array.prototype.map = function(fun /*, thisp*/)
  {
    var len = this.length;
    if (typeof fun != "function")
      throw new TypeError();

    var res = new Array(len);
    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in this)
        res[i] = fun.call(thisp, this[i], i, this);
    }

    return res;
  };
}

if (!Array.prototype.some)
{
  Array.prototype.some = function(fun /*, thisp*/)
  {
    var len = this.length;
    if (typeof fun != "function")
      throw new TypeError();

    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in this &&
          fun.call(thisp, this[i], i, this))
        return true;
    }

    return false;
  };
}

if (!Array.indexOf)
   Array.prototype.indexOf = function(obj,from){
      var len = this.length;
      for (var i = (from < 0) ? Math.max(0, len + from) : from || 0; i < len; i++){
          if (this[i] === obj) return i;
      }
      return -1;
   }

var SWFUpload;

Array.prototype.remove = 
function(a,b){if(typeof(b)=='undefined'){var i=this.indexOf(a);if(i>=0)this.splice(i,1);}else{this.splice(a,b-a+1);}}

// By Douglas Crockford
String.prototype.supplant = function (o) {
    return this.replace(/{([^{}]*)}/g,
        function (a, b) {
            var r = o[b];
            return typeof r === 'string' || typeof r === 'number' ? r : a;
        }
    );
};


/*
 * Date Format 1.2.2
 * (c) 2007-2008 Steven Levithan <stevenlevithan.com>
 * MIT license
 * Includes enhancements by Scott Trenda <scott.trenda.net> and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */
var jsFormatDateTime = function () {
        var     token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
                timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
                timezoneClip = /[^-+\dA-Z]/g,
                pad = function (val, len) {
                        val = String(val);
                        len = len || 2;
                        while (val.length < len) val = "0" + val;
                        return val;
                };

        // Regexes and supporting functions are cached through closure
        return function (date, mask, utc) {
                var dF = jsFormatDateTime;

                // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
                if (arguments.length == 1 && (typeof date == "string" || date instanceof String) && !/\d/.test(date)) {
                        mask = date;
                        date = undefined;
                }

                // Passing date through Date applies Date.parse, if necessary
                date = date ? new Date(date) : new Date();
                if (isNaN(date)) throw new SyntaxError("invalid date");

                mask = String(dF.masks[mask] || mask || dF.masks["default"]);

                // Allow setting the utc argument via the mask
                if (mask.slice(0, 4) == "UTC:") {
                        mask = mask.slice(4);
                        utc = true;
                }

                var     _ = utc ? "getUTC" : "get",
                        d = date[_ + "Date"](),
                        D = date[_ + "Day"](),
                        m = date[_ + "Month"](),
                        y = date[_ + "FullYear"](),
                        H = date[_ + "Hours"](),
                        M = date[_ + "Minutes"](),
                        s = date[_ + "Seconds"](),
                        L = date[_ + "Milliseconds"](),
                        o = utc ? 0 : date.getTimezoneOffset(),
                        flags = {
                                d:    d,
                                dd:   pad(d),
                                ddd:  dF.i18n.dayNames[D],
                                dddd: dF.i18n.dayNames[D + 7],
                                m:    m + 1,
                                mm:   pad(m + 1),
                                mmm:  dF.i18n.monthNames[m],
                                mmmm: dF.i18n.monthNames[m + 12],
                                yy:   String(y).slice(2),
                                yyyy: y,
                                h:    H % 12 || 12,
                                hh:   pad(H % 12 || 12),
                                H:    H,
                                HH:   pad(H),
                                M:    M,
                                MM:   pad(M),
                                s:    s,
                                ss:   pad(s),
                                l:    pad(L, 3),
                                L:    pad(L > 99 ? Math.round(L / 10) : L),
                                t:    H < 12 ? "a"  : "p",
                                tt:   H < 12 ? "am" : "pm",
                                T:    H < 12 ? "A"  : "P",
                                TT:   H < 12 ? "AM" : "PM",
                                Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                                o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                                S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
                        };

                return mask.replace(token, function ($0) {
                        return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
                });
        };
}();

// Some common format strings
jsFormatDateTime.masks = {
        "default":      "ddd mmm dd yyyy HH:MM:ss",
        shortDate:      "m/d/yy",
        mediumDate:     "mmm d, yyyy",
        longDate:       "mmmm d, yyyy",
        fullDate:       "dddd, mmmm d, yyyy",
        shortTime:      "h:MM TT",
        mediumTime:     "h:MM:ss TT",
        longTime:       "h:MM:ss TT Z",
        isoDate:        "yyyy-mm-dd",
        isoTime:        "HH:MM:ss",
        isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
        isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
jsFormatDateTime.i18n = {
        dayNames: [
                "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
                "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
        ],
        monthNames: [
                "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
                "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
        ]
};

//from http://www.json.org/json2.js
if(!this.JSON){this.JSON={}}(function(){function f(n){return n<10?'0'+n:n}if(typeof Date.prototype.toJSON!=='function'){Date.prototype.toJSON=function(key){return isFinite(this.valueOf())?this.getUTCFullYear()+'-'+f(this.getUTCMonth()+1)+'-'+f(this.getUTCDate())+'T'+f(this.getUTCHours())+':'+f(this.getUTCMinutes())+':'+f(this.getUTCSeconds())+'Z':null};String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(key){return this.valueOf()}}var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={'\b':'\\b','\t':'\\t','\n':'\\n','\f':'\\f','\r':'\\r','"':'\\"','\\':'\\\\'},rep;function quote(string){escapable.lastIndex=0;return escapable.test(string)?'"'+string.replace(escapable,function(a){var c=meta[a];return typeof c==='string'?c:'\\u'+('0000'+a.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+string+'"'}function str(key,holder){var i,k,v,length,mind=gap,partial,value=holder[key];if(value&&typeof value==='object'&&typeof value.toJSON==='function'){value=value.toJSON(key)}if(typeof rep==='function'){value=rep.call(holder,key,value)}switch(typeof value){case'string':return quote(value);case'number':return isFinite(value)?String(value):'null';case'boolean':case'null':return String(value);case'object':if(!value){return'null'}gap+=indent;partial=[];if(Object.prototype.toString.apply(value)==='[object Array]'){length=value.length;for(i=0;i<length;i+=1){partial[i]=str(i,value)||'null'}v=partial.length===0?'[]':gap?'[\n'+gap+partial.join(',\n'+gap)+'\n'+mind+']':'['+partial.join(',')+']';gap=mind;return v}if(rep&&typeof rep==='object'){length=rep.length;for(i=0;i<length;i+=1){k=rep[i];if(typeof k==='string'){v=str(k,value);if(v){partial.push(quote(k)+(gap?': ':':')+v)}}}}else{for(k in value){if(Object.hasOwnProperty.call(value,k)){v=str(k,value);if(v){partial.push(quote(k)+(gap?': ':':')+v)}}}}v=partial.length===0?'{}':gap?'{\n'+gap+partial.join(',\n'+gap)+'\n'+mind+'}':'{'+partial.join(',')+'}';gap=mind;return v}}if(typeof JSON.stringify!=='function'){JSON.stringify=function(value,replacer,space){var i;gap='';indent='';if(typeof space==='number'){for(i=0;i<space;i+=1){indent+=' '}}else if(typeof space==='string'){indent=space}rep=replacer;if(replacer&&typeof replacer!=='function'&&(typeof replacer!=='object'||typeof replacer.length!=='number')){throw new Error('JSON.stringify');}return str('',{'':value})}}if(typeof JSON.parse!=='function'){JSON.parse=function(text,reviver){var j;function walk(holder,key){var k,v,value=holder[key];if(value&&typeof value==='object'){for(k in value){if(Object.hasOwnProperty.call(value,k)){v=walk(value,k);if(v!==undefined){value[k]=v}else{delete value[k]}}}}return reviver.call(holder,key,value)}text=String(text);cx.lastIndex=0;if(cx.test(text)){text=text.replace(cx,function(a){return'\\u'+('0000'+a.charCodeAt(0).toString(16)).slice(-4)})}if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,'@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,']').replace(/(?:^|:|,)(?:\s*\[)+/g,''))){j=eval('('+text+')');return typeof reviver==='function'?walk({'':j},''):j}throw new SyntaxError('JSON.parse');}}}());

function jsonDateReviver(key, value)
{
    var a;
    if (typeof value === 'string') 
    {
        a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
        if (a) 
        {
            return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6]));
        }
    }
    return value;
};
$gv_.serverexception="";