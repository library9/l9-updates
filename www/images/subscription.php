<?php include 'menu.php'; ?>
<div class="subscription_pg">
	<h2>Subscription Pricing</h2>
	
	<div  class="pricing_table">
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<th>Service/Product</th>
				<th>
					<span class="planblue">Cloud9</span><br />
					<span  style="font-size:20px;">$1249/<sup>Year</sup></span>
				</th>
				<th>
					<span class="planblue">USB9</span><br />
					<span  style="font-size:20px;">$1749/<sup>Year</sup></span>				
				
				</th>
				<th>
					<span class="planblue">DVD9</span><br />
					<span  style="font-size:20px;">$2249/<sup>Year</sup></span>				
				</th>												
			</tr>
			<tr>
				<td align="right">Component Database</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>													
			</tr>
			<tr>
				<td align="right">Multiple Accounts</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">BOM Storage</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">BOM Storage with Price &nbsp; Availability Service</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick1.png" width="28" height="25" /></td>
				<td><img src="images/planstick1.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">Where Used System</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>													
			</tr>
			<tr>
				<td align="right">L9 Dynamic Datasheets</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick1.png" width="28" height="25" /></td>
				<td><img src="images/planstick1.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">Custom Part Number System</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">Component Revision System</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>												
			</tr>			
			<tr>
				<td align="right">Custom Column Visibility System</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>													
			</tr>
			<tr>
				<td align="right">L9 Datasheet Download System</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick1.png" width="28" height="25" /></td>
				<td><img src="images/planstick1.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">RoHs and Lifecycle Data</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick1.png" width="28" height="25" /></td>
				<td><img src="images/planstick1.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">Component Verification Service</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick2.png" width="28" height="25" /></td>
				<td><img src="images/planstick2.png" width="28" height="25" /></td>												
			</tr>			
			<tr>
				<td align="right">L9 Custom Library System</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>													
			</tr>
			<tr>
				<td align="right">Custom Component Additional Database Field System</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">Component Editor System</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">Auto Parameter Data Fillin Service</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick1.png" width="28" height="25" /></td>
				<td><img src="images/planstick1.png" width="28" height="25" /></td>												
			</tr>
						<tr>
				<td align="right">User Management System</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>													
			</tr>
			<tr>
				<td align="right">Vendor Accounts</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">Logo Design Service</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">L9 Basic Library System</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">Part Creation Service</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>													
			</tr>
			<tr>
				<td align="right">Upload Area for Vendor Use</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">Online Tools (download, calculators etc...)</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">StandAlone System Architecture</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>												
			</tr>
			<tr>
				<td align="right">Portable Architecture</td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>
				<td><img src="images/planstick.png" width="28" height="25" /></td>												
			</tr>						
			<tr>
				<td>&nbsp;</td>
				<td><a href="#" class="plansignup">Sign Up Now</a></td>
				<td><a href="#" class="plansignup">Sign Up Now</a></td>
				<td><a href="#" class="plansignup">Sign Up Now</a></td>
			</tr>
			<tr>
				<td colspan="4" align="left">
				Maintence Fee per year: <span class="planblue">$449</span><br />
				Auto data fillin service with, BOM Pricing/Availability System, and L9 Datasheets: <span class="planblue">Individual: $29 per month / Company: $99 per month</span><br />
				Add L9 part Data verification service: <span class="planblue">Individual: $29 per month / Company: $99 per month</span><br />
				Part Building Service: <br />
					<span class="planblue">1 part - $12 each<br />
					20 parts - $199 per year<br />
					50 parts - $449 per year<br />
					100 parts - $799 per year<br />
					200 parts - $1199 per year<br />
					500 parts - $2499 per year, $5 per part after 500</span><br />
				</td>



			</tr>																								
		</table>
	</div>
	
</div>


	
	
			
		

       
    
    
</div><!-- end contentcontainer -->
</div>
<!-- end content -->
         
 <?php include 'footer.php'; ?>